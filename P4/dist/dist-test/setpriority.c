#include "types.h"
#include "user.h"

int
main(int argc, char* argv[]) {
  int pid;
  int rc = 0;

  if(argc != 2) {
    printf(2, "Enter a valid PID of a running process to run tests.\n");
    exit();
  }

  pid = atoi(argv[1]);

  printf(1, "\nSetting priority to -1.\n");
  rc = setpriority(pid, -1);
  if(rc != 0) {
    printf(1, "Priority value is invalid. Did not change the current value so PASS!\n\n");
  } 
  printf(1, "Setting priority to 2.\n");
  rc = setpriority(pid, 2);
  if(rc == 0) {
    printf(1, "Priority value is valid. Priority value wass changed to 2 PASS!\n\n");
  } 
  printf(1, "Setting priority to 100.\n");
  rc = setpriority(pid, 100);
  if(rc != 0) {
    printf(1, "Priority value is invalid. Did not change the current value so PASS!\n\n");
  } 

  printf(1, "\nNow testing an invalid PID.\nSetting priority to 2 and using invalid PID -1.\n");
  rc = setpriority(-1, 2);
  if(rc == 0) {
    printf(1, "Priority value is valid. Priority value wass changed to 2 PASS!\n\n");
  } 
  exit();
}
