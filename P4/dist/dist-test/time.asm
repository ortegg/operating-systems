
_time:     file format elf32-i386


Disassembly of section .text:

00000000 <main>:
#include "types.h"
#include "user.h"

int main(int argc, char *argv [])
{
   0:	8d 4c 24 04          	lea    0x4(%esp),%ecx
   4:	83 e4 f0             	and    $0xfffffff0,%esp
   7:	ff 71 fc             	pushl  -0x4(%ecx)
   a:	55                   	push   %ebp
   b:	89 e5                	mov    %esp,%ebp
   d:	56                   	push   %esi
   e:	53                   	push   %ebx
   f:	51                   	push   %ecx
  10:	83 ec 1c             	sub    $0x1c,%esp
  13:	89 cb                	mov    %ecx,%ebx
  int t1, t2;  //ticks before and after
  int pid;

  argv++;
  15:	83 43 04 04          	addl   $0x4,0x4(%ebx)
  t1 = uptime(); //get the current clock tick
  19:	e8 cd 03 00 00       	call   3eb <uptime>
  1e:	89 45 e4             	mov    %eax,-0x1c(%ebp)

  pid = fork();
  21:	e8 25 03 00 00       	call   34b <fork>
  26:	89 45 e0             	mov    %eax,-0x20(%ebp)
  if (pid < 0) //invalid pid
  29:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
  2d:	79 24                	jns    53 <main+0x53>
  {
    printf(2, "Error: fork failed. %s at line %d, for command %s\n", __FILE__, __LINE__, argv[0]);
  2f:	8b 43 04             	mov    0x4(%ebx),%eax
  32:	8b 00                	mov    (%eax),%eax
  34:	83 ec 0c             	sub    $0xc,%esp
  37:	50                   	push   %eax
  38:	6a 0f                	push   $0xf
  3a:	68 c8 08 00 00       	push   $0x8c8
  3f:	68 d0 08 00 00       	push   $0x8d0
  44:	6a 02                	push   $0x2
  46:	e8 c7 04 00 00       	call   512 <printf>
  4b:	83 c4 20             	add    $0x20,%esp
    exit();
  4e:	e8 00 03 00 00       	call   353 <exit>
  }
  else if (pid == 0) // is child (error)
  53:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
  57:	75 38                	jne    91 <main+0x91>
  {
    exec(argv[0], argv);
  59:	8b 43 04             	mov    0x4(%ebx),%eax
  5c:	8b 00                	mov    (%eax),%eax
  5e:	83 ec 08             	sub    $0x8,%esp
  61:	ff 73 04             	pushl  0x4(%ebx)
  64:	50                   	push   %eax
  65:	e8 21 03 00 00       	call   38b <exec>
  6a:	83 c4 10             	add    $0x10,%esp
    printf(2, "Error: exec failed. %s at line %d, for command %s\n", __FILE__, __LINE__, argv[0]);
  6d:	8b 43 04             	mov    0x4(%ebx),%eax
  70:	8b 00                	mov    (%eax),%eax
  72:	83 ec 0c             	sub    $0xc,%esp
  75:	50                   	push   %eax
  76:	6a 15                	push   $0x15
  78:	68 c8 08 00 00       	push   $0x8c8
  7d:	68 04 09 00 00       	push   $0x904
  82:	6a 02                	push   $0x2
  84:	e8 89 04 00 00       	call   512 <printf>
  89:	83 c4 20             	add    $0x20,%esp
    exit();
  8c:	e8 c2 02 00 00       	call   353 <exit>
  }
  else //is parent (good)
  { 
    wait(); // wait for it to exit
  91:	e8 c5 02 00 00       	call   35b <wait>
    t2 = uptime(); //get the clock at the end of the program
  96:	e8 50 03 00 00       	call   3eb <uptime>
  9b:	89 45 dc             	mov    %eax,-0x24(%ebp)
  }

  // output to the time program
  printf(1, "%s ran in %d.%d seconds.\n", argv[0], (t2-t1)/100, (t2-t1)%100);
  9e:	8b 45 dc             	mov    -0x24(%ebp),%eax
  a1:	2b 45 e4             	sub    -0x1c(%ebp),%eax
  a4:	89 c6                	mov    %eax,%esi
  a6:	ba 1f 85 eb 51       	mov    $0x51eb851f,%edx
  ab:	89 f0                	mov    %esi,%eax
  ad:	f7 ea                	imul   %edx
  af:	c1 fa 05             	sar    $0x5,%edx
  b2:	89 f0                	mov    %esi,%eax
  b4:	c1 f8 1f             	sar    $0x1f,%eax
  b7:	89 d1                	mov    %edx,%ecx
  b9:	29 c1                	sub    %eax,%ecx
  bb:	6b c1 64             	imul   $0x64,%ecx,%eax
  be:	29 c6                	sub    %eax,%esi
  c0:	89 f1                	mov    %esi,%ecx
  c2:	8b 45 dc             	mov    -0x24(%ebp),%eax
  c5:	2b 45 e4             	sub    -0x1c(%ebp),%eax
  c8:	89 c6                	mov    %eax,%esi
  ca:	ba 1f 85 eb 51       	mov    $0x51eb851f,%edx
  cf:	89 f0                	mov    %esi,%eax
  d1:	f7 ea                	imul   %edx
  d3:	c1 fa 05             	sar    $0x5,%edx
  d6:	89 f0                	mov    %esi,%eax
  d8:	c1 f8 1f             	sar    $0x1f,%eax
  db:	29 c2                	sub    %eax,%edx
  dd:	8b 43 04             	mov    0x4(%ebx),%eax
  e0:	8b 00                	mov    (%eax),%eax
  e2:	83 ec 0c             	sub    $0xc,%esp
  e5:	51                   	push   %ecx
  e6:	52                   	push   %edx
  e7:	50                   	push   %eax
  e8:	68 37 09 00 00       	push   $0x937
  ed:	6a 01                	push   $0x1
  ef:	e8 1e 04 00 00       	call   512 <printf>
  f4:	83 c4 20             	add    $0x20,%esp

  exit();
  f7:	e8 57 02 00 00       	call   353 <exit>

000000fc <stosb>:
               "cc");
}

static inline void
stosb(void *addr, int data, int cnt)
{
  fc:	55                   	push   %ebp
  fd:	89 e5                	mov    %esp,%ebp
  ff:	57                   	push   %edi
 100:	53                   	push   %ebx
  asm volatile("cld; rep stosb" :
 101:	8b 4d 08             	mov    0x8(%ebp),%ecx
 104:	8b 55 10             	mov    0x10(%ebp),%edx
 107:	8b 45 0c             	mov    0xc(%ebp),%eax
 10a:	89 cb                	mov    %ecx,%ebx
 10c:	89 df                	mov    %ebx,%edi
 10e:	89 d1                	mov    %edx,%ecx
 110:	fc                   	cld    
 111:	f3 aa                	rep stos %al,%es:(%edi)
 113:	89 ca                	mov    %ecx,%edx
 115:	89 fb                	mov    %edi,%ebx
 117:	89 5d 08             	mov    %ebx,0x8(%ebp)
 11a:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
 11d:	90                   	nop
 11e:	5b                   	pop    %ebx
 11f:	5f                   	pop    %edi
 120:	5d                   	pop    %ebp
 121:	c3                   	ret    

00000122 <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, char *t)
{
 122:	55                   	push   %ebp
 123:	89 e5                	mov    %esp,%ebp
 125:	83 ec 10             	sub    $0x10,%esp
  char *os;

  os = s;
 128:	8b 45 08             	mov    0x8(%ebp),%eax
 12b:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while((*s++ = *t++) != 0)
 12e:	90                   	nop
 12f:	8b 45 08             	mov    0x8(%ebp),%eax
 132:	8d 50 01             	lea    0x1(%eax),%edx
 135:	89 55 08             	mov    %edx,0x8(%ebp)
 138:	8b 55 0c             	mov    0xc(%ebp),%edx
 13b:	8d 4a 01             	lea    0x1(%edx),%ecx
 13e:	89 4d 0c             	mov    %ecx,0xc(%ebp)
 141:	0f b6 12             	movzbl (%edx),%edx
 144:	88 10                	mov    %dl,(%eax)
 146:	0f b6 00             	movzbl (%eax),%eax
 149:	84 c0                	test   %al,%al
 14b:	75 e2                	jne    12f <strcpy+0xd>
    ;
  return os;
 14d:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 150:	c9                   	leave  
 151:	c3                   	ret    

00000152 <strcmp>:

int
strcmp(const char *p, const char *q)
{
 152:	55                   	push   %ebp
 153:	89 e5                	mov    %esp,%ebp
  while(*p && *p == *q)
 155:	eb 08                	jmp    15f <strcmp+0xd>
    p++, q++;
 157:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 15b:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
  while(*p && *p == *q)
 15f:	8b 45 08             	mov    0x8(%ebp),%eax
 162:	0f b6 00             	movzbl (%eax),%eax
 165:	84 c0                	test   %al,%al
 167:	74 10                	je     179 <strcmp+0x27>
 169:	8b 45 08             	mov    0x8(%ebp),%eax
 16c:	0f b6 10             	movzbl (%eax),%edx
 16f:	8b 45 0c             	mov    0xc(%ebp),%eax
 172:	0f b6 00             	movzbl (%eax),%eax
 175:	38 c2                	cmp    %al,%dl
 177:	74 de                	je     157 <strcmp+0x5>
    p++, q++;
  return (uchar)*p - (uchar)*q;
 179:	8b 45 08             	mov    0x8(%ebp),%eax
 17c:	0f b6 00             	movzbl (%eax),%eax
 17f:	0f b6 d0             	movzbl %al,%edx
 182:	8b 45 0c             	mov    0xc(%ebp),%eax
 185:	0f b6 00             	movzbl (%eax),%eax
 188:	0f b6 c0             	movzbl %al,%eax
 18b:	29 c2                	sub    %eax,%edx
 18d:	89 d0                	mov    %edx,%eax
}
 18f:	5d                   	pop    %ebp
 190:	c3                   	ret    

00000191 <strlen>:

uint
strlen(char *s)
{
 191:	55                   	push   %ebp
 192:	89 e5                	mov    %esp,%ebp
 194:	83 ec 10             	sub    $0x10,%esp
  int n;

  for(n = 0; s[n]; n++)
 197:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
 19e:	eb 04                	jmp    1a4 <strlen+0x13>
 1a0:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
 1a4:	8b 55 fc             	mov    -0x4(%ebp),%edx
 1a7:	8b 45 08             	mov    0x8(%ebp),%eax
 1aa:	01 d0                	add    %edx,%eax
 1ac:	0f b6 00             	movzbl (%eax),%eax
 1af:	84 c0                	test   %al,%al
 1b1:	75 ed                	jne    1a0 <strlen+0xf>
    ;
  return n;
 1b3:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 1b6:	c9                   	leave  
 1b7:	c3                   	ret    

000001b8 <memset>:

void*
memset(void *dst, int c, uint n)
{
 1b8:	55                   	push   %ebp
 1b9:	89 e5                	mov    %esp,%ebp
  stosb(dst, c, n);
 1bb:	8b 45 10             	mov    0x10(%ebp),%eax
 1be:	50                   	push   %eax
 1bf:	ff 75 0c             	pushl  0xc(%ebp)
 1c2:	ff 75 08             	pushl  0x8(%ebp)
 1c5:	e8 32 ff ff ff       	call   fc <stosb>
 1ca:	83 c4 0c             	add    $0xc,%esp
  return dst;
 1cd:	8b 45 08             	mov    0x8(%ebp),%eax
}
 1d0:	c9                   	leave  
 1d1:	c3                   	ret    

000001d2 <strchr>:

char*
strchr(const char *s, char c)
{
 1d2:	55                   	push   %ebp
 1d3:	89 e5                	mov    %esp,%ebp
 1d5:	83 ec 04             	sub    $0x4,%esp
 1d8:	8b 45 0c             	mov    0xc(%ebp),%eax
 1db:	88 45 fc             	mov    %al,-0x4(%ebp)
  for(; *s; s++)
 1de:	eb 14                	jmp    1f4 <strchr+0x22>
    if(*s == c)
 1e0:	8b 45 08             	mov    0x8(%ebp),%eax
 1e3:	0f b6 00             	movzbl (%eax),%eax
 1e6:	3a 45 fc             	cmp    -0x4(%ebp),%al
 1e9:	75 05                	jne    1f0 <strchr+0x1e>
      return (char*)s;
 1eb:	8b 45 08             	mov    0x8(%ebp),%eax
 1ee:	eb 13                	jmp    203 <strchr+0x31>
}

char*
strchr(const char *s, char c)
{
  for(; *s; s++)
 1f0:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 1f4:	8b 45 08             	mov    0x8(%ebp),%eax
 1f7:	0f b6 00             	movzbl (%eax),%eax
 1fa:	84 c0                	test   %al,%al
 1fc:	75 e2                	jne    1e0 <strchr+0xe>
    if(*s == c)
      return (char*)s;
  return 0;
 1fe:	b8 00 00 00 00       	mov    $0x0,%eax
}
 203:	c9                   	leave  
 204:	c3                   	ret    

00000205 <gets>:

char*
gets(char *buf, int max)
{
 205:	55                   	push   %ebp
 206:	89 e5                	mov    %esp,%ebp
 208:	83 ec 18             	sub    $0x18,%esp
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 20b:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 212:	eb 42                	jmp    256 <gets+0x51>
    cc = read(0, &c, 1);
 214:	83 ec 04             	sub    $0x4,%esp
 217:	6a 01                	push   $0x1
 219:	8d 45 ef             	lea    -0x11(%ebp),%eax
 21c:	50                   	push   %eax
 21d:	6a 00                	push   $0x0
 21f:	e8 47 01 00 00       	call   36b <read>
 224:	83 c4 10             	add    $0x10,%esp
 227:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if(cc < 1)
 22a:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 22e:	7e 33                	jle    263 <gets+0x5e>
      break;
    buf[i++] = c;
 230:	8b 45 f4             	mov    -0xc(%ebp),%eax
 233:	8d 50 01             	lea    0x1(%eax),%edx
 236:	89 55 f4             	mov    %edx,-0xc(%ebp)
 239:	89 c2                	mov    %eax,%edx
 23b:	8b 45 08             	mov    0x8(%ebp),%eax
 23e:	01 c2                	add    %eax,%edx
 240:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 244:	88 02                	mov    %al,(%edx)
    if(c == '\n' || c == '\r')
 246:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 24a:	3c 0a                	cmp    $0xa,%al
 24c:	74 16                	je     264 <gets+0x5f>
 24e:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 252:	3c 0d                	cmp    $0xd,%al
 254:	74 0e                	je     264 <gets+0x5f>
gets(char *buf, int max)
{
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 256:	8b 45 f4             	mov    -0xc(%ebp),%eax
 259:	83 c0 01             	add    $0x1,%eax
 25c:	3b 45 0c             	cmp    0xc(%ebp),%eax
 25f:	7c b3                	jl     214 <gets+0xf>
 261:	eb 01                	jmp    264 <gets+0x5f>
    cc = read(0, &c, 1);
    if(cc < 1)
      break;
 263:	90                   	nop
    buf[i++] = c;
    if(c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 264:	8b 55 f4             	mov    -0xc(%ebp),%edx
 267:	8b 45 08             	mov    0x8(%ebp),%eax
 26a:	01 d0                	add    %edx,%eax
 26c:	c6 00 00             	movb   $0x0,(%eax)
  return buf;
 26f:	8b 45 08             	mov    0x8(%ebp),%eax
}
 272:	c9                   	leave  
 273:	c3                   	ret    

00000274 <stat>:

int
stat(char *n, struct stat *st)
{
 274:	55                   	push   %ebp
 275:	89 e5                	mov    %esp,%ebp
 277:	83 ec 18             	sub    $0x18,%esp
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 27a:	83 ec 08             	sub    $0x8,%esp
 27d:	6a 00                	push   $0x0
 27f:	ff 75 08             	pushl  0x8(%ebp)
 282:	e8 0c 01 00 00       	call   393 <open>
 287:	83 c4 10             	add    $0x10,%esp
 28a:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(fd < 0)
 28d:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 291:	79 07                	jns    29a <stat+0x26>
    return -1;
 293:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 298:	eb 25                	jmp    2bf <stat+0x4b>
  r = fstat(fd, st);
 29a:	83 ec 08             	sub    $0x8,%esp
 29d:	ff 75 0c             	pushl  0xc(%ebp)
 2a0:	ff 75 f4             	pushl  -0xc(%ebp)
 2a3:	e8 03 01 00 00       	call   3ab <fstat>
 2a8:	83 c4 10             	add    $0x10,%esp
 2ab:	89 45 f0             	mov    %eax,-0x10(%ebp)
  close(fd);
 2ae:	83 ec 0c             	sub    $0xc,%esp
 2b1:	ff 75 f4             	pushl  -0xc(%ebp)
 2b4:	e8 c2 00 00 00       	call   37b <close>
 2b9:	83 c4 10             	add    $0x10,%esp
  return r;
 2bc:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
 2bf:	c9                   	leave  
 2c0:	c3                   	ret    

000002c1 <atoi>:

int
atoi(const char *s)
{
 2c1:	55                   	push   %ebp
 2c2:	89 e5                	mov    %esp,%ebp
 2c4:	83 ec 10             	sub    $0x10,%esp
  int n;

  n = 0;
 2c7:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  while('0' <= *s && *s <= '9')
 2ce:	eb 25                	jmp    2f5 <atoi+0x34>
    n = n*10 + *s++ - '0';
 2d0:	8b 55 fc             	mov    -0x4(%ebp),%edx
 2d3:	89 d0                	mov    %edx,%eax
 2d5:	c1 e0 02             	shl    $0x2,%eax
 2d8:	01 d0                	add    %edx,%eax
 2da:	01 c0                	add    %eax,%eax
 2dc:	89 c1                	mov    %eax,%ecx
 2de:	8b 45 08             	mov    0x8(%ebp),%eax
 2e1:	8d 50 01             	lea    0x1(%eax),%edx
 2e4:	89 55 08             	mov    %edx,0x8(%ebp)
 2e7:	0f b6 00             	movzbl (%eax),%eax
 2ea:	0f be c0             	movsbl %al,%eax
 2ed:	01 c8                	add    %ecx,%eax
 2ef:	83 e8 30             	sub    $0x30,%eax
 2f2:	89 45 fc             	mov    %eax,-0x4(%ebp)
atoi(const char *s)
{
  int n;

  n = 0;
  while('0' <= *s && *s <= '9')
 2f5:	8b 45 08             	mov    0x8(%ebp),%eax
 2f8:	0f b6 00             	movzbl (%eax),%eax
 2fb:	3c 2f                	cmp    $0x2f,%al
 2fd:	7e 0a                	jle    309 <atoi+0x48>
 2ff:	8b 45 08             	mov    0x8(%ebp),%eax
 302:	0f b6 00             	movzbl (%eax),%eax
 305:	3c 39                	cmp    $0x39,%al
 307:	7e c7                	jle    2d0 <atoi+0xf>
    n = n*10 + *s++ - '0';
  return n;
 309:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 30c:	c9                   	leave  
 30d:	c3                   	ret    

0000030e <memmove>:

void*
memmove(void *vdst, void *vsrc, int n)
{
 30e:	55                   	push   %ebp
 30f:	89 e5                	mov    %esp,%ebp
 311:	83 ec 10             	sub    $0x10,%esp
  char *dst, *src;
  
  dst = vdst;
 314:	8b 45 08             	mov    0x8(%ebp),%eax
 317:	89 45 fc             	mov    %eax,-0x4(%ebp)
  src = vsrc;
 31a:	8b 45 0c             	mov    0xc(%ebp),%eax
 31d:	89 45 f8             	mov    %eax,-0x8(%ebp)
  while(n-- > 0)
 320:	eb 17                	jmp    339 <memmove+0x2b>
    *dst++ = *src++;
 322:	8b 45 fc             	mov    -0x4(%ebp),%eax
 325:	8d 50 01             	lea    0x1(%eax),%edx
 328:	89 55 fc             	mov    %edx,-0x4(%ebp)
 32b:	8b 55 f8             	mov    -0x8(%ebp),%edx
 32e:	8d 4a 01             	lea    0x1(%edx),%ecx
 331:	89 4d f8             	mov    %ecx,-0x8(%ebp)
 334:	0f b6 12             	movzbl (%edx),%edx
 337:	88 10                	mov    %dl,(%eax)
{
  char *dst, *src;
  
  dst = vdst;
  src = vsrc;
  while(n-- > 0)
 339:	8b 45 10             	mov    0x10(%ebp),%eax
 33c:	8d 50 ff             	lea    -0x1(%eax),%edx
 33f:	89 55 10             	mov    %edx,0x10(%ebp)
 342:	85 c0                	test   %eax,%eax
 344:	7f dc                	jg     322 <memmove+0x14>
    *dst++ = *src++;
  return vdst;
 346:	8b 45 08             	mov    0x8(%ebp),%eax
}
 349:	c9                   	leave  
 34a:	c3                   	ret    

0000034b <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
 34b:	b8 01 00 00 00       	mov    $0x1,%eax
 350:	cd 40                	int    $0x40
 352:	c3                   	ret    

00000353 <exit>:
SYSCALL(exit)
 353:	b8 02 00 00 00       	mov    $0x2,%eax
 358:	cd 40                	int    $0x40
 35a:	c3                   	ret    

0000035b <wait>:
SYSCALL(wait)
 35b:	b8 03 00 00 00       	mov    $0x3,%eax
 360:	cd 40                	int    $0x40
 362:	c3                   	ret    

00000363 <pipe>:
SYSCALL(pipe)
 363:	b8 04 00 00 00       	mov    $0x4,%eax
 368:	cd 40                	int    $0x40
 36a:	c3                   	ret    

0000036b <read>:
SYSCALL(read)
 36b:	b8 05 00 00 00       	mov    $0x5,%eax
 370:	cd 40                	int    $0x40
 372:	c3                   	ret    

00000373 <write>:
SYSCALL(write)
 373:	b8 10 00 00 00       	mov    $0x10,%eax
 378:	cd 40                	int    $0x40
 37a:	c3                   	ret    

0000037b <close>:
SYSCALL(close)
 37b:	b8 15 00 00 00       	mov    $0x15,%eax
 380:	cd 40                	int    $0x40
 382:	c3                   	ret    

00000383 <kill>:
SYSCALL(kill)
 383:	b8 06 00 00 00       	mov    $0x6,%eax
 388:	cd 40                	int    $0x40
 38a:	c3                   	ret    

0000038b <exec>:
SYSCALL(exec)
 38b:	b8 07 00 00 00       	mov    $0x7,%eax
 390:	cd 40                	int    $0x40
 392:	c3                   	ret    

00000393 <open>:
SYSCALL(open)
 393:	b8 0f 00 00 00       	mov    $0xf,%eax
 398:	cd 40                	int    $0x40
 39a:	c3                   	ret    

0000039b <mknod>:
SYSCALL(mknod)
 39b:	b8 11 00 00 00       	mov    $0x11,%eax
 3a0:	cd 40                	int    $0x40
 3a2:	c3                   	ret    

000003a3 <unlink>:
SYSCALL(unlink)
 3a3:	b8 12 00 00 00       	mov    $0x12,%eax
 3a8:	cd 40                	int    $0x40
 3aa:	c3                   	ret    

000003ab <fstat>:
SYSCALL(fstat)
 3ab:	b8 08 00 00 00       	mov    $0x8,%eax
 3b0:	cd 40                	int    $0x40
 3b2:	c3                   	ret    

000003b3 <link>:
SYSCALL(link)
 3b3:	b8 13 00 00 00       	mov    $0x13,%eax
 3b8:	cd 40                	int    $0x40
 3ba:	c3                   	ret    

000003bb <mkdir>:
SYSCALL(mkdir)
 3bb:	b8 14 00 00 00       	mov    $0x14,%eax
 3c0:	cd 40                	int    $0x40
 3c2:	c3                   	ret    

000003c3 <chdir>:
SYSCALL(chdir)
 3c3:	b8 09 00 00 00       	mov    $0x9,%eax
 3c8:	cd 40                	int    $0x40
 3ca:	c3                   	ret    

000003cb <dup>:
SYSCALL(dup)
 3cb:	b8 0a 00 00 00       	mov    $0xa,%eax
 3d0:	cd 40                	int    $0x40
 3d2:	c3                   	ret    

000003d3 <getpid>:
SYSCALL(getpid)
 3d3:	b8 0b 00 00 00       	mov    $0xb,%eax
 3d8:	cd 40                	int    $0x40
 3da:	c3                   	ret    

000003db <sbrk>:
SYSCALL(sbrk)
 3db:	b8 0c 00 00 00       	mov    $0xc,%eax
 3e0:	cd 40                	int    $0x40
 3e2:	c3                   	ret    

000003e3 <sleep>:
SYSCALL(sleep)
 3e3:	b8 0d 00 00 00       	mov    $0xd,%eax
 3e8:	cd 40                	int    $0x40
 3ea:	c3                   	ret    

000003eb <uptime>:
SYSCALL(uptime)
 3eb:	b8 0e 00 00 00       	mov    $0xe,%eax
 3f0:	cd 40                	int    $0x40
 3f2:	c3                   	ret    

000003f3 <halt>:
SYSCALL(halt)
 3f3:	b8 16 00 00 00       	mov    $0x16,%eax
 3f8:	cd 40                	int    $0x40
 3fa:	c3                   	ret    

000003fb <date>:
SYSCALL(date)
 3fb:	b8 17 00 00 00       	mov    $0x17,%eax
 400:	cd 40                	int    $0x40
 402:	c3                   	ret    

00000403 <getuid>:
SYSCALL(getuid)
 403:	b8 18 00 00 00       	mov    $0x18,%eax
 408:	cd 40                	int    $0x40
 40a:	c3                   	ret    

0000040b <getgid>:
SYSCALL(getgid)
 40b:	b8 19 00 00 00       	mov    $0x19,%eax
 410:	cd 40                	int    $0x40
 412:	c3                   	ret    

00000413 <getppid>:
SYSCALL(getppid)
 413:	b8 1a 00 00 00       	mov    $0x1a,%eax
 418:	cd 40                	int    $0x40
 41a:	c3                   	ret    

0000041b <setuid>:
SYSCALL(setuid)
 41b:	b8 1b 00 00 00       	mov    $0x1b,%eax
 420:	cd 40                	int    $0x40
 422:	c3                   	ret    

00000423 <setgid>:
SYSCALL(setgid)
 423:	b8 1c 00 00 00       	mov    $0x1c,%eax
 428:	cd 40                	int    $0x40
 42a:	c3                   	ret    

0000042b <getprocs>:
SYSCALL(getprocs)
 42b:	b8 1d 00 00 00       	mov    $0x1d,%eax
 430:	cd 40                	int    $0x40
 432:	c3                   	ret    

00000433 <setpriority>:
SYSCALL(setpriority)
 433:	b8 1e 00 00 00       	mov    $0x1e,%eax
 438:	cd 40                	int    $0x40
 43a:	c3                   	ret    

0000043b <putc>:
#include "stat.h"
#include "user.h"

static void
putc(int fd, char c)
{
 43b:	55                   	push   %ebp
 43c:	89 e5                	mov    %esp,%ebp
 43e:	83 ec 18             	sub    $0x18,%esp
 441:	8b 45 0c             	mov    0xc(%ebp),%eax
 444:	88 45 f4             	mov    %al,-0xc(%ebp)
  write(fd, &c, 1);
 447:	83 ec 04             	sub    $0x4,%esp
 44a:	6a 01                	push   $0x1
 44c:	8d 45 f4             	lea    -0xc(%ebp),%eax
 44f:	50                   	push   %eax
 450:	ff 75 08             	pushl  0x8(%ebp)
 453:	e8 1b ff ff ff       	call   373 <write>
 458:	83 c4 10             	add    $0x10,%esp
}
 45b:	90                   	nop
 45c:	c9                   	leave  
 45d:	c3                   	ret    

0000045e <printint>:

static void
printint(int fd, int xx, int base, int sgn)
{
 45e:	55                   	push   %ebp
 45f:	89 e5                	mov    %esp,%ebp
 461:	53                   	push   %ebx
 462:	83 ec 24             	sub    $0x24,%esp
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 465:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  if(sgn && xx < 0){
 46c:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
 470:	74 17                	je     489 <printint+0x2b>
 472:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
 476:	79 11                	jns    489 <printint+0x2b>
    neg = 1;
 478:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
    x = -xx;
 47f:	8b 45 0c             	mov    0xc(%ebp),%eax
 482:	f7 d8                	neg    %eax
 484:	89 45 ec             	mov    %eax,-0x14(%ebp)
 487:	eb 06                	jmp    48f <printint+0x31>
  } else {
    x = xx;
 489:	8b 45 0c             	mov    0xc(%ebp),%eax
 48c:	89 45 ec             	mov    %eax,-0x14(%ebp)
  }

  i = 0;
 48f:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  do{
    buf[i++] = digits[x % base];
 496:	8b 4d f4             	mov    -0xc(%ebp),%ecx
 499:	8d 41 01             	lea    0x1(%ecx),%eax
 49c:	89 45 f4             	mov    %eax,-0xc(%ebp)
 49f:	8b 5d 10             	mov    0x10(%ebp),%ebx
 4a2:	8b 45 ec             	mov    -0x14(%ebp),%eax
 4a5:	ba 00 00 00 00       	mov    $0x0,%edx
 4aa:	f7 f3                	div    %ebx
 4ac:	89 d0                	mov    %edx,%eax
 4ae:	0f b6 80 a8 0b 00 00 	movzbl 0xba8(%eax),%eax
 4b5:	88 44 0d dc          	mov    %al,-0x24(%ebp,%ecx,1)
  }while((x /= base) != 0);
 4b9:	8b 5d 10             	mov    0x10(%ebp),%ebx
 4bc:	8b 45 ec             	mov    -0x14(%ebp),%eax
 4bf:	ba 00 00 00 00       	mov    $0x0,%edx
 4c4:	f7 f3                	div    %ebx
 4c6:	89 45 ec             	mov    %eax,-0x14(%ebp)
 4c9:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 4cd:	75 c7                	jne    496 <printint+0x38>
  if(neg)
 4cf:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 4d3:	74 2d                	je     502 <printint+0xa4>
    buf[i++] = '-';
 4d5:	8b 45 f4             	mov    -0xc(%ebp),%eax
 4d8:	8d 50 01             	lea    0x1(%eax),%edx
 4db:	89 55 f4             	mov    %edx,-0xc(%ebp)
 4de:	c6 44 05 dc 2d       	movb   $0x2d,-0x24(%ebp,%eax,1)

  while(--i >= 0)
 4e3:	eb 1d                	jmp    502 <printint+0xa4>
    putc(fd, buf[i]);
 4e5:	8d 55 dc             	lea    -0x24(%ebp),%edx
 4e8:	8b 45 f4             	mov    -0xc(%ebp),%eax
 4eb:	01 d0                	add    %edx,%eax
 4ed:	0f b6 00             	movzbl (%eax),%eax
 4f0:	0f be c0             	movsbl %al,%eax
 4f3:	83 ec 08             	sub    $0x8,%esp
 4f6:	50                   	push   %eax
 4f7:	ff 75 08             	pushl  0x8(%ebp)
 4fa:	e8 3c ff ff ff       	call   43b <putc>
 4ff:	83 c4 10             	add    $0x10,%esp
    buf[i++] = digits[x % base];
  }while((x /= base) != 0);
  if(neg)
    buf[i++] = '-';

  while(--i >= 0)
 502:	83 6d f4 01          	subl   $0x1,-0xc(%ebp)
 506:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 50a:	79 d9                	jns    4e5 <printint+0x87>
    putc(fd, buf[i]);
}
 50c:	90                   	nop
 50d:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 510:	c9                   	leave  
 511:	c3                   	ret    

00000512 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, char *fmt, ...)
{
 512:	55                   	push   %ebp
 513:	89 e5                	mov    %esp,%ebp
 515:	83 ec 28             	sub    $0x28,%esp
  char *s;
  int c, i, state;
  uint *ap;

  state = 0;
 518:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  ap = (uint*)(void*)&fmt + 1;
 51f:	8d 45 0c             	lea    0xc(%ebp),%eax
 522:	83 c0 04             	add    $0x4,%eax
 525:	89 45 e8             	mov    %eax,-0x18(%ebp)
  for(i = 0; fmt[i]; i++){
 528:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 52f:	e9 59 01 00 00       	jmp    68d <printf+0x17b>
    c = fmt[i] & 0xff;
 534:	8b 55 0c             	mov    0xc(%ebp),%edx
 537:	8b 45 f0             	mov    -0x10(%ebp),%eax
 53a:	01 d0                	add    %edx,%eax
 53c:	0f b6 00             	movzbl (%eax),%eax
 53f:	0f be c0             	movsbl %al,%eax
 542:	25 ff 00 00 00       	and    $0xff,%eax
 547:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    if(state == 0){
 54a:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 54e:	75 2c                	jne    57c <printf+0x6a>
      if(c == '%'){
 550:	83 7d e4 25          	cmpl   $0x25,-0x1c(%ebp)
 554:	75 0c                	jne    562 <printf+0x50>
        state = '%';
 556:	c7 45 ec 25 00 00 00 	movl   $0x25,-0x14(%ebp)
 55d:	e9 27 01 00 00       	jmp    689 <printf+0x177>
      } else {
        putc(fd, c);
 562:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 565:	0f be c0             	movsbl %al,%eax
 568:	83 ec 08             	sub    $0x8,%esp
 56b:	50                   	push   %eax
 56c:	ff 75 08             	pushl  0x8(%ebp)
 56f:	e8 c7 fe ff ff       	call   43b <putc>
 574:	83 c4 10             	add    $0x10,%esp
 577:	e9 0d 01 00 00       	jmp    689 <printf+0x177>
      }
    } else if(state == '%'){
 57c:	83 7d ec 25          	cmpl   $0x25,-0x14(%ebp)
 580:	0f 85 03 01 00 00    	jne    689 <printf+0x177>
      if(c == 'd'){
 586:	83 7d e4 64          	cmpl   $0x64,-0x1c(%ebp)
 58a:	75 1e                	jne    5aa <printf+0x98>
        printint(fd, *ap, 10, 1);
 58c:	8b 45 e8             	mov    -0x18(%ebp),%eax
 58f:	8b 00                	mov    (%eax),%eax
 591:	6a 01                	push   $0x1
 593:	6a 0a                	push   $0xa
 595:	50                   	push   %eax
 596:	ff 75 08             	pushl  0x8(%ebp)
 599:	e8 c0 fe ff ff       	call   45e <printint>
 59e:	83 c4 10             	add    $0x10,%esp
        ap++;
 5a1:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 5a5:	e9 d8 00 00 00       	jmp    682 <printf+0x170>
      } else if(c == 'x' || c == 'p'){
 5aa:	83 7d e4 78          	cmpl   $0x78,-0x1c(%ebp)
 5ae:	74 06                	je     5b6 <printf+0xa4>
 5b0:	83 7d e4 70          	cmpl   $0x70,-0x1c(%ebp)
 5b4:	75 1e                	jne    5d4 <printf+0xc2>
        printint(fd, *ap, 16, 0);
 5b6:	8b 45 e8             	mov    -0x18(%ebp),%eax
 5b9:	8b 00                	mov    (%eax),%eax
 5bb:	6a 00                	push   $0x0
 5bd:	6a 10                	push   $0x10
 5bf:	50                   	push   %eax
 5c0:	ff 75 08             	pushl  0x8(%ebp)
 5c3:	e8 96 fe ff ff       	call   45e <printint>
 5c8:	83 c4 10             	add    $0x10,%esp
        ap++;
 5cb:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 5cf:	e9 ae 00 00 00       	jmp    682 <printf+0x170>
      } else if(c == 's'){
 5d4:	83 7d e4 73          	cmpl   $0x73,-0x1c(%ebp)
 5d8:	75 43                	jne    61d <printf+0x10b>
        s = (char*)*ap;
 5da:	8b 45 e8             	mov    -0x18(%ebp),%eax
 5dd:	8b 00                	mov    (%eax),%eax
 5df:	89 45 f4             	mov    %eax,-0xc(%ebp)
        ap++;
 5e2:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
        if(s == 0)
 5e6:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 5ea:	75 25                	jne    611 <printf+0xff>
          s = "(null)";
 5ec:	c7 45 f4 51 09 00 00 	movl   $0x951,-0xc(%ebp)
        while(*s != 0){
 5f3:	eb 1c                	jmp    611 <printf+0xff>
          putc(fd, *s);
 5f5:	8b 45 f4             	mov    -0xc(%ebp),%eax
 5f8:	0f b6 00             	movzbl (%eax),%eax
 5fb:	0f be c0             	movsbl %al,%eax
 5fe:	83 ec 08             	sub    $0x8,%esp
 601:	50                   	push   %eax
 602:	ff 75 08             	pushl  0x8(%ebp)
 605:	e8 31 fe ff ff       	call   43b <putc>
 60a:	83 c4 10             	add    $0x10,%esp
          s++;
 60d:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
      } else if(c == 's'){
        s = (char*)*ap;
        ap++;
        if(s == 0)
          s = "(null)";
        while(*s != 0){
 611:	8b 45 f4             	mov    -0xc(%ebp),%eax
 614:	0f b6 00             	movzbl (%eax),%eax
 617:	84 c0                	test   %al,%al
 619:	75 da                	jne    5f5 <printf+0xe3>
 61b:	eb 65                	jmp    682 <printf+0x170>
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 61d:	83 7d e4 63          	cmpl   $0x63,-0x1c(%ebp)
 621:	75 1d                	jne    640 <printf+0x12e>
        putc(fd, *ap);
 623:	8b 45 e8             	mov    -0x18(%ebp),%eax
 626:	8b 00                	mov    (%eax),%eax
 628:	0f be c0             	movsbl %al,%eax
 62b:	83 ec 08             	sub    $0x8,%esp
 62e:	50                   	push   %eax
 62f:	ff 75 08             	pushl  0x8(%ebp)
 632:	e8 04 fe ff ff       	call   43b <putc>
 637:	83 c4 10             	add    $0x10,%esp
        ap++;
 63a:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 63e:	eb 42                	jmp    682 <printf+0x170>
      } else if(c == '%'){
 640:	83 7d e4 25          	cmpl   $0x25,-0x1c(%ebp)
 644:	75 17                	jne    65d <printf+0x14b>
        putc(fd, c);
 646:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 649:	0f be c0             	movsbl %al,%eax
 64c:	83 ec 08             	sub    $0x8,%esp
 64f:	50                   	push   %eax
 650:	ff 75 08             	pushl  0x8(%ebp)
 653:	e8 e3 fd ff ff       	call   43b <putc>
 658:	83 c4 10             	add    $0x10,%esp
 65b:	eb 25                	jmp    682 <printf+0x170>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 65d:	83 ec 08             	sub    $0x8,%esp
 660:	6a 25                	push   $0x25
 662:	ff 75 08             	pushl  0x8(%ebp)
 665:	e8 d1 fd ff ff       	call   43b <putc>
 66a:	83 c4 10             	add    $0x10,%esp
        putc(fd, c);
 66d:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 670:	0f be c0             	movsbl %al,%eax
 673:	83 ec 08             	sub    $0x8,%esp
 676:	50                   	push   %eax
 677:	ff 75 08             	pushl  0x8(%ebp)
 67a:	e8 bc fd ff ff       	call   43b <putc>
 67f:	83 c4 10             	add    $0x10,%esp
      }
      state = 0;
 682:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
 689:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
 68d:	8b 55 0c             	mov    0xc(%ebp),%edx
 690:	8b 45 f0             	mov    -0x10(%ebp),%eax
 693:	01 d0                	add    %edx,%eax
 695:	0f b6 00             	movzbl (%eax),%eax
 698:	84 c0                	test   %al,%al
 69a:	0f 85 94 fe ff ff    	jne    534 <printf+0x22>
        putc(fd, c);
      }
      state = 0;
    }
  }
}
 6a0:	90                   	nop
 6a1:	c9                   	leave  
 6a2:	c3                   	ret    

000006a3 <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 6a3:	55                   	push   %ebp
 6a4:	89 e5                	mov    %esp,%ebp
 6a6:	83 ec 10             	sub    $0x10,%esp
  Header *bp, *p;

  bp = (Header*)ap - 1;
 6a9:	8b 45 08             	mov    0x8(%ebp),%eax
 6ac:	83 e8 08             	sub    $0x8,%eax
 6af:	89 45 f8             	mov    %eax,-0x8(%ebp)
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 6b2:	a1 c4 0b 00 00       	mov    0xbc4,%eax
 6b7:	89 45 fc             	mov    %eax,-0x4(%ebp)
 6ba:	eb 24                	jmp    6e0 <free+0x3d>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 6bc:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6bf:	8b 00                	mov    (%eax),%eax
 6c1:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 6c4:	77 12                	ja     6d8 <free+0x35>
 6c6:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6c9:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 6cc:	77 24                	ja     6f2 <free+0x4f>
 6ce:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6d1:	8b 00                	mov    (%eax),%eax
 6d3:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 6d6:	77 1a                	ja     6f2 <free+0x4f>
free(void *ap)
{
  Header *bp, *p;

  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 6d8:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6db:	8b 00                	mov    (%eax),%eax
 6dd:	89 45 fc             	mov    %eax,-0x4(%ebp)
 6e0:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6e3:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 6e6:	76 d4                	jbe    6bc <free+0x19>
 6e8:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6eb:	8b 00                	mov    (%eax),%eax
 6ed:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 6f0:	76 ca                	jbe    6bc <free+0x19>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if(bp + bp->s.size == p->s.ptr){
 6f2:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6f5:	8b 40 04             	mov    0x4(%eax),%eax
 6f8:	8d 14 c5 00 00 00 00 	lea    0x0(,%eax,8),%edx
 6ff:	8b 45 f8             	mov    -0x8(%ebp),%eax
 702:	01 c2                	add    %eax,%edx
 704:	8b 45 fc             	mov    -0x4(%ebp),%eax
 707:	8b 00                	mov    (%eax),%eax
 709:	39 c2                	cmp    %eax,%edx
 70b:	75 24                	jne    731 <free+0x8e>
    bp->s.size += p->s.ptr->s.size;
 70d:	8b 45 f8             	mov    -0x8(%ebp),%eax
 710:	8b 50 04             	mov    0x4(%eax),%edx
 713:	8b 45 fc             	mov    -0x4(%ebp),%eax
 716:	8b 00                	mov    (%eax),%eax
 718:	8b 40 04             	mov    0x4(%eax),%eax
 71b:	01 c2                	add    %eax,%edx
 71d:	8b 45 f8             	mov    -0x8(%ebp),%eax
 720:	89 50 04             	mov    %edx,0x4(%eax)
    bp->s.ptr = p->s.ptr->s.ptr;
 723:	8b 45 fc             	mov    -0x4(%ebp),%eax
 726:	8b 00                	mov    (%eax),%eax
 728:	8b 10                	mov    (%eax),%edx
 72a:	8b 45 f8             	mov    -0x8(%ebp),%eax
 72d:	89 10                	mov    %edx,(%eax)
 72f:	eb 0a                	jmp    73b <free+0x98>
  } else
    bp->s.ptr = p->s.ptr;
 731:	8b 45 fc             	mov    -0x4(%ebp),%eax
 734:	8b 10                	mov    (%eax),%edx
 736:	8b 45 f8             	mov    -0x8(%ebp),%eax
 739:	89 10                	mov    %edx,(%eax)
  if(p + p->s.size == bp){
 73b:	8b 45 fc             	mov    -0x4(%ebp),%eax
 73e:	8b 40 04             	mov    0x4(%eax),%eax
 741:	8d 14 c5 00 00 00 00 	lea    0x0(,%eax,8),%edx
 748:	8b 45 fc             	mov    -0x4(%ebp),%eax
 74b:	01 d0                	add    %edx,%eax
 74d:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 750:	75 20                	jne    772 <free+0xcf>
    p->s.size += bp->s.size;
 752:	8b 45 fc             	mov    -0x4(%ebp),%eax
 755:	8b 50 04             	mov    0x4(%eax),%edx
 758:	8b 45 f8             	mov    -0x8(%ebp),%eax
 75b:	8b 40 04             	mov    0x4(%eax),%eax
 75e:	01 c2                	add    %eax,%edx
 760:	8b 45 fc             	mov    -0x4(%ebp),%eax
 763:	89 50 04             	mov    %edx,0x4(%eax)
    p->s.ptr = bp->s.ptr;
 766:	8b 45 f8             	mov    -0x8(%ebp),%eax
 769:	8b 10                	mov    (%eax),%edx
 76b:	8b 45 fc             	mov    -0x4(%ebp),%eax
 76e:	89 10                	mov    %edx,(%eax)
 770:	eb 08                	jmp    77a <free+0xd7>
  } else
    p->s.ptr = bp;
 772:	8b 45 fc             	mov    -0x4(%ebp),%eax
 775:	8b 55 f8             	mov    -0x8(%ebp),%edx
 778:	89 10                	mov    %edx,(%eax)
  freep = p;
 77a:	8b 45 fc             	mov    -0x4(%ebp),%eax
 77d:	a3 c4 0b 00 00       	mov    %eax,0xbc4
}
 782:	90                   	nop
 783:	c9                   	leave  
 784:	c3                   	ret    

00000785 <morecore>:

static Header*
morecore(uint nu)
{
 785:	55                   	push   %ebp
 786:	89 e5                	mov    %esp,%ebp
 788:	83 ec 18             	sub    $0x18,%esp
  char *p;
  Header *hp;

  if(nu < 4096)
 78b:	81 7d 08 ff 0f 00 00 	cmpl   $0xfff,0x8(%ebp)
 792:	77 07                	ja     79b <morecore+0x16>
    nu = 4096;
 794:	c7 45 08 00 10 00 00 	movl   $0x1000,0x8(%ebp)
  p = sbrk(nu * sizeof(Header));
 79b:	8b 45 08             	mov    0x8(%ebp),%eax
 79e:	c1 e0 03             	shl    $0x3,%eax
 7a1:	83 ec 0c             	sub    $0xc,%esp
 7a4:	50                   	push   %eax
 7a5:	e8 31 fc ff ff       	call   3db <sbrk>
 7aa:	83 c4 10             	add    $0x10,%esp
 7ad:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(p == (char*)-1)
 7b0:	83 7d f4 ff          	cmpl   $0xffffffff,-0xc(%ebp)
 7b4:	75 07                	jne    7bd <morecore+0x38>
    return 0;
 7b6:	b8 00 00 00 00       	mov    $0x0,%eax
 7bb:	eb 26                	jmp    7e3 <morecore+0x5e>
  hp = (Header*)p;
 7bd:	8b 45 f4             	mov    -0xc(%ebp),%eax
 7c0:	89 45 f0             	mov    %eax,-0x10(%ebp)
  hp->s.size = nu;
 7c3:	8b 45 f0             	mov    -0x10(%ebp),%eax
 7c6:	8b 55 08             	mov    0x8(%ebp),%edx
 7c9:	89 50 04             	mov    %edx,0x4(%eax)
  free((void*)(hp + 1));
 7cc:	8b 45 f0             	mov    -0x10(%ebp),%eax
 7cf:	83 c0 08             	add    $0x8,%eax
 7d2:	83 ec 0c             	sub    $0xc,%esp
 7d5:	50                   	push   %eax
 7d6:	e8 c8 fe ff ff       	call   6a3 <free>
 7db:	83 c4 10             	add    $0x10,%esp
  return freep;
 7de:	a1 c4 0b 00 00       	mov    0xbc4,%eax
}
 7e3:	c9                   	leave  
 7e4:	c3                   	ret    

000007e5 <malloc>:

void*
malloc(uint nbytes)
{
 7e5:	55                   	push   %ebp
 7e6:	89 e5                	mov    %esp,%ebp
 7e8:	83 ec 18             	sub    $0x18,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 7eb:	8b 45 08             	mov    0x8(%ebp),%eax
 7ee:	83 c0 07             	add    $0x7,%eax
 7f1:	c1 e8 03             	shr    $0x3,%eax
 7f4:	83 c0 01             	add    $0x1,%eax
 7f7:	89 45 ec             	mov    %eax,-0x14(%ebp)
  if((prevp = freep) == 0){
 7fa:	a1 c4 0b 00 00       	mov    0xbc4,%eax
 7ff:	89 45 f0             	mov    %eax,-0x10(%ebp)
 802:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 806:	75 23                	jne    82b <malloc+0x46>
    base.s.ptr = freep = prevp = &base;
 808:	c7 45 f0 bc 0b 00 00 	movl   $0xbbc,-0x10(%ebp)
 80f:	8b 45 f0             	mov    -0x10(%ebp),%eax
 812:	a3 c4 0b 00 00       	mov    %eax,0xbc4
 817:	a1 c4 0b 00 00       	mov    0xbc4,%eax
 81c:	a3 bc 0b 00 00       	mov    %eax,0xbbc
    base.s.size = 0;
 821:	c7 05 c0 0b 00 00 00 	movl   $0x0,0xbc0
 828:	00 00 00 
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 82b:	8b 45 f0             	mov    -0x10(%ebp),%eax
 82e:	8b 00                	mov    (%eax),%eax
 830:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if(p->s.size >= nunits){
 833:	8b 45 f4             	mov    -0xc(%ebp),%eax
 836:	8b 40 04             	mov    0x4(%eax),%eax
 839:	3b 45 ec             	cmp    -0x14(%ebp),%eax
 83c:	72 4d                	jb     88b <malloc+0xa6>
      if(p->s.size == nunits)
 83e:	8b 45 f4             	mov    -0xc(%ebp),%eax
 841:	8b 40 04             	mov    0x4(%eax),%eax
 844:	3b 45 ec             	cmp    -0x14(%ebp),%eax
 847:	75 0c                	jne    855 <malloc+0x70>
        prevp->s.ptr = p->s.ptr;
 849:	8b 45 f4             	mov    -0xc(%ebp),%eax
 84c:	8b 10                	mov    (%eax),%edx
 84e:	8b 45 f0             	mov    -0x10(%ebp),%eax
 851:	89 10                	mov    %edx,(%eax)
 853:	eb 26                	jmp    87b <malloc+0x96>
      else {
        p->s.size -= nunits;
 855:	8b 45 f4             	mov    -0xc(%ebp),%eax
 858:	8b 40 04             	mov    0x4(%eax),%eax
 85b:	2b 45 ec             	sub    -0x14(%ebp),%eax
 85e:	89 c2                	mov    %eax,%edx
 860:	8b 45 f4             	mov    -0xc(%ebp),%eax
 863:	89 50 04             	mov    %edx,0x4(%eax)
        p += p->s.size;
 866:	8b 45 f4             	mov    -0xc(%ebp),%eax
 869:	8b 40 04             	mov    0x4(%eax),%eax
 86c:	c1 e0 03             	shl    $0x3,%eax
 86f:	01 45 f4             	add    %eax,-0xc(%ebp)
        p->s.size = nunits;
 872:	8b 45 f4             	mov    -0xc(%ebp),%eax
 875:	8b 55 ec             	mov    -0x14(%ebp),%edx
 878:	89 50 04             	mov    %edx,0x4(%eax)
      }
      freep = prevp;
 87b:	8b 45 f0             	mov    -0x10(%ebp),%eax
 87e:	a3 c4 0b 00 00       	mov    %eax,0xbc4
      return (void*)(p + 1);
 883:	8b 45 f4             	mov    -0xc(%ebp),%eax
 886:	83 c0 08             	add    $0x8,%eax
 889:	eb 3b                	jmp    8c6 <malloc+0xe1>
    }
    if(p == freep)
 88b:	a1 c4 0b 00 00       	mov    0xbc4,%eax
 890:	39 45 f4             	cmp    %eax,-0xc(%ebp)
 893:	75 1e                	jne    8b3 <malloc+0xce>
      if((p = morecore(nunits)) == 0)
 895:	83 ec 0c             	sub    $0xc,%esp
 898:	ff 75 ec             	pushl  -0x14(%ebp)
 89b:	e8 e5 fe ff ff       	call   785 <morecore>
 8a0:	83 c4 10             	add    $0x10,%esp
 8a3:	89 45 f4             	mov    %eax,-0xc(%ebp)
 8a6:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 8aa:	75 07                	jne    8b3 <malloc+0xce>
        return 0;
 8ac:	b8 00 00 00 00       	mov    $0x0,%eax
 8b1:	eb 13                	jmp    8c6 <malloc+0xe1>
  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
  if((prevp = freep) == 0){
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 8b3:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8b6:	89 45 f0             	mov    %eax,-0x10(%ebp)
 8b9:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8bc:	8b 00                	mov    (%eax),%eax
 8be:	89 45 f4             	mov    %eax,-0xc(%ebp)
      return (void*)(p + 1);
    }
    if(p == freep)
      if((p = morecore(nunits)) == 0)
        return 0;
  }
 8c1:	e9 6d ff ff ff       	jmp    833 <malloc+0x4e>
}
 8c6:	c9                   	leave  
 8c7:	c3                   	ret    
