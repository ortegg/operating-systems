
_setpriority:     file format elf32-i386


Disassembly of section .text:

00000000 <main>:
#include "types.h"
#include "user.h"

int
main(int argc, char* argv[]) {
   0:	8d 4c 24 04          	lea    0x4(%esp),%ecx
   4:	83 e4 f0             	and    $0xfffffff0,%esp
   7:	ff 71 fc             	pushl  -0x4(%ecx)
   a:	55                   	push   %ebp
   b:	89 e5                	mov    %esp,%ebp
   d:	51                   	push   %ecx
   e:	83 ec 14             	sub    $0x14,%esp
  11:	89 c8                	mov    %ecx,%eax
  int pid;
  int rc = 0;
  13:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

  if(argc != 2) {
  1a:	83 38 02             	cmpl   $0x2,(%eax)
  1d:	74 17                	je     36 <main+0x36>
    printf(2, "Enter a valid PID of a running process to run tests.\n");
  1f:	83 ec 08             	sub    $0x8,%esp
  22:	68 14 09 00 00       	push   $0x914
  27:	6a 02                	push   $0x2
  29:	e8 2d 05 00 00       	call   55b <printf>
  2e:	83 c4 10             	add    $0x10,%esp
    exit();
  31:	e8 66 03 00 00       	call   39c <exit>
  }

  pid = atoi(argv[1]);
  36:	8b 40 04             	mov    0x4(%eax),%eax
  39:	83 c0 04             	add    $0x4,%eax
  3c:	8b 00                	mov    (%eax),%eax
  3e:	83 ec 0c             	sub    $0xc,%esp
  41:	50                   	push   %eax
  42:	e8 c3 02 00 00       	call   30a <atoi>
  47:	83 c4 10             	add    $0x10,%esp
  4a:	89 45 f0             	mov    %eax,-0x10(%ebp)

  printf(1, "\nSetting priority to -1.\n");
  4d:	83 ec 08             	sub    $0x8,%esp
  50:	68 4a 09 00 00       	push   $0x94a
  55:	6a 01                	push   $0x1
  57:	e8 ff 04 00 00       	call   55b <printf>
  5c:	83 c4 10             	add    $0x10,%esp
  rc = setpriority(pid, -1);
  5f:	83 ec 08             	sub    $0x8,%esp
  62:	6a ff                	push   $0xffffffff
  64:	ff 75 f0             	pushl  -0x10(%ebp)
  67:	e8 10 04 00 00       	call   47c <setpriority>
  6c:	83 c4 10             	add    $0x10,%esp
  6f:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(rc != 0) {
  72:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
  76:	74 12                	je     8a <main+0x8a>
    printf(1, "Priority value is invalid. Did not change the current value so PASS!\n\n");
  78:	83 ec 08             	sub    $0x8,%esp
  7b:	68 64 09 00 00       	push   $0x964
  80:	6a 01                	push   $0x1
  82:	e8 d4 04 00 00       	call   55b <printf>
  87:	83 c4 10             	add    $0x10,%esp
  } 
  printf(1, "Setting priority to 2.\n");
  8a:	83 ec 08             	sub    $0x8,%esp
  8d:	68 ab 09 00 00       	push   $0x9ab
  92:	6a 01                	push   $0x1
  94:	e8 c2 04 00 00       	call   55b <printf>
  99:	83 c4 10             	add    $0x10,%esp
  rc = setpriority(pid, 2);
  9c:	83 ec 08             	sub    $0x8,%esp
  9f:	6a 02                	push   $0x2
  a1:	ff 75 f0             	pushl  -0x10(%ebp)
  a4:	e8 d3 03 00 00       	call   47c <setpriority>
  a9:	83 c4 10             	add    $0x10,%esp
  ac:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(rc == 0) {
  af:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
  b3:	75 12                	jne    c7 <main+0xc7>
    printf(1, "Priority value is valid. Priority value wass changed to 2 PASS!\n\n");
  b5:	83 ec 08             	sub    $0x8,%esp
  b8:	68 c4 09 00 00       	push   $0x9c4
  bd:	6a 01                	push   $0x1
  bf:	e8 97 04 00 00       	call   55b <printf>
  c4:	83 c4 10             	add    $0x10,%esp
  } 
  printf(1, "Setting priority to 100.\n");
  c7:	83 ec 08             	sub    $0x8,%esp
  ca:	68 06 0a 00 00       	push   $0xa06
  cf:	6a 01                	push   $0x1
  d1:	e8 85 04 00 00       	call   55b <printf>
  d6:	83 c4 10             	add    $0x10,%esp
  rc = setpriority(pid, 100);
  d9:	83 ec 08             	sub    $0x8,%esp
  dc:	6a 64                	push   $0x64
  de:	ff 75 f0             	pushl  -0x10(%ebp)
  e1:	e8 96 03 00 00       	call   47c <setpriority>
  e6:	83 c4 10             	add    $0x10,%esp
  e9:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(rc != 0) {
  ec:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
  f0:	74 12                	je     104 <main+0x104>
    printf(1, "Priority value is invalid. Did not change the current value so PASS!\n\n");
  f2:	83 ec 08             	sub    $0x8,%esp
  f5:	68 64 09 00 00       	push   $0x964
  fa:	6a 01                	push   $0x1
  fc:	e8 5a 04 00 00       	call   55b <printf>
 101:	83 c4 10             	add    $0x10,%esp
  } 

  printf(1, "\nNow testing an invalid PID.\nSetting priority to 2 and using invalid PID -1.\n");
 104:	83 ec 08             	sub    $0x8,%esp
 107:	68 20 0a 00 00       	push   $0xa20
 10c:	6a 01                	push   $0x1
 10e:	e8 48 04 00 00       	call   55b <printf>
 113:	83 c4 10             	add    $0x10,%esp
  rc = setpriority(-1, 2);
 116:	83 ec 08             	sub    $0x8,%esp
 119:	6a 02                	push   $0x2
 11b:	6a ff                	push   $0xffffffff
 11d:	e8 5a 03 00 00       	call   47c <setpriority>
 122:	83 c4 10             	add    $0x10,%esp
 125:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(rc == 0) {
 128:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 12c:	75 12                	jne    140 <main+0x140>
    printf(1, "Priority value is valid. Priority value wass changed to 2 PASS!\n\n");
 12e:	83 ec 08             	sub    $0x8,%esp
 131:	68 c4 09 00 00       	push   $0x9c4
 136:	6a 01                	push   $0x1
 138:	e8 1e 04 00 00       	call   55b <printf>
 13d:	83 c4 10             	add    $0x10,%esp
  } 
  exit();
 140:	e8 57 02 00 00       	call   39c <exit>

00000145 <stosb>:
               "cc");
}

static inline void
stosb(void *addr, int data, int cnt)
{
 145:	55                   	push   %ebp
 146:	89 e5                	mov    %esp,%ebp
 148:	57                   	push   %edi
 149:	53                   	push   %ebx
  asm volatile("cld; rep stosb" :
 14a:	8b 4d 08             	mov    0x8(%ebp),%ecx
 14d:	8b 55 10             	mov    0x10(%ebp),%edx
 150:	8b 45 0c             	mov    0xc(%ebp),%eax
 153:	89 cb                	mov    %ecx,%ebx
 155:	89 df                	mov    %ebx,%edi
 157:	89 d1                	mov    %edx,%ecx
 159:	fc                   	cld    
 15a:	f3 aa                	rep stos %al,%es:(%edi)
 15c:	89 ca                	mov    %ecx,%edx
 15e:	89 fb                	mov    %edi,%ebx
 160:	89 5d 08             	mov    %ebx,0x8(%ebp)
 163:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
 166:	90                   	nop
 167:	5b                   	pop    %ebx
 168:	5f                   	pop    %edi
 169:	5d                   	pop    %ebp
 16a:	c3                   	ret    

0000016b <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, char *t)
{
 16b:	55                   	push   %ebp
 16c:	89 e5                	mov    %esp,%ebp
 16e:	83 ec 10             	sub    $0x10,%esp
  char *os;

  os = s;
 171:	8b 45 08             	mov    0x8(%ebp),%eax
 174:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while((*s++ = *t++) != 0)
 177:	90                   	nop
 178:	8b 45 08             	mov    0x8(%ebp),%eax
 17b:	8d 50 01             	lea    0x1(%eax),%edx
 17e:	89 55 08             	mov    %edx,0x8(%ebp)
 181:	8b 55 0c             	mov    0xc(%ebp),%edx
 184:	8d 4a 01             	lea    0x1(%edx),%ecx
 187:	89 4d 0c             	mov    %ecx,0xc(%ebp)
 18a:	0f b6 12             	movzbl (%edx),%edx
 18d:	88 10                	mov    %dl,(%eax)
 18f:	0f b6 00             	movzbl (%eax),%eax
 192:	84 c0                	test   %al,%al
 194:	75 e2                	jne    178 <strcpy+0xd>
    ;
  return os;
 196:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 199:	c9                   	leave  
 19a:	c3                   	ret    

0000019b <strcmp>:

int
strcmp(const char *p, const char *q)
{
 19b:	55                   	push   %ebp
 19c:	89 e5                	mov    %esp,%ebp
  while(*p && *p == *q)
 19e:	eb 08                	jmp    1a8 <strcmp+0xd>
    p++, q++;
 1a0:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 1a4:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
  while(*p && *p == *q)
 1a8:	8b 45 08             	mov    0x8(%ebp),%eax
 1ab:	0f b6 00             	movzbl (%eax),%eax
 1ae:	84 c0                	test   %al,%al
 1b0:	74 10                	je     1c2 <strcmp+0x27>
 1b2:	8b 45 08             	mov    0x8(%ebp),%eax
 1b5:	0f b6 10             	movzbl (%eax),%edx
 1b8:	8b 45 0c             	mov    0xc(%ebp),%eax
 1bb:	0f b6 00             	movzbl (%eax),%eax
 1be:	38 c2                	cmp    %al,%dl
 1c0:	74 de                	je     1a0 <strcmp+0x5>
    p++, q++;
  return (uchar)*p - (uchar)*q;
 1c2:	8b 45 08             	mov    0x8(%ebp),%eax
 1c5:	0f b6 00             	movzbl (%eax),%eax
 1c8:	0f b6 d0             	movzbl %al,%edx
 1cb:	8b 45 0c             	mov    0xc(%ebp),%eax
 1ce:	0f b6 00             	movzbl (%eax),%eax
 1d1:	0f b6 c0             	movzbl %al,%eax
 1d4:	29 c2                	sub    %eax,%edx
 1d6:	89 d0                	mov    %edx,%eax
}
 1d8:	5d                   	pop    %ebp
 1d9:	c3                   	ret    

000001da <strlen>:

uint
strlen(char *s)
{
 1da:	55                   	push   %ebp
 1db:	89 e5                	mov    %esp,%ebp
 1dd:	83 ec 10             	sub    $0x10,%esp
  int n;

  for(n = 0; s[n]; n++)
 1e0:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
 1e7:	eb 04                	jmp    1ed <strlen+0x13>
 1e9:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
 1ed:	8b 55 fc             	mov    -0x4(%ebp),%edx
 1f0:	8b 45 08             	mov    0x8(%ebp),%eax
 1f3:	01 d0                	add    %edx,%eax
 1f5:	0f b6 00             	movzbl (%eax),%eax
 1f8:	84 c0                	test   %al,%al
 1fa:	75 ed                	jne    1e9 <strlen+0xf>
    ;
  return n;
 1fc:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 1ff:	c9                   	leave  
 200:	c3                   	ret    

00000201 <memset>:

void*
memset(void *dst, int c, uint n)
{
 201:	55                   	push   %ebp
 202:	89 e5                	mov    %esp,%ebp
  stosb(dst, c, n);
 204:	8b 45 10             	mov    0x10(%ebp),%eax
 207:	50                   	push   %eax
 208:	ff 75 0c             	pushl  0xc(%ebp)
 20b:	ff 75 08             	pushl  0x8(%ebp)
 20e:	e8 32 ff ff ff       	call   145 <stosb>
 213:	83 c4 0c             	add    $0xc,%esp
  return dst;
 216:	8b 45 08             	mov    0x8(%ebp),%eax
}
 219:	c9                   	leave  
 21a:	c3                   	ret    

0000021b <strchr>:

char*
strchr(const char *s, char c)
{
 21b:	55                   	push   %ebp
 21c:	89 e5                	mov    %esp,%ebp
 21e:	83 ec 04             	sub    $0x4,%esp
 221:	8b 45 0c             	mov    0xc(%ebp),%eax
 224:	88 45 fc             	mov    %al,-0x4(%ebp)
  for(; *s; s++)
 227:	eb 14                	jmp    23d <strchr+0x22>
    if(*s == c)
 229:	8b 45 08             	mov    0x8(%ebp),%eax
 22c:	0f b6 00             	movzbl (%eax),%eax
 22f:	3a 45 fc             	cmp    -0x4(%ebp),%al
 232:	75 05                	jne    239 <strchr+0x1e>
      return (char*)s;
 234:	8b 45 08             	mov    0x8(%ebp),%eax
 237:	eb 13                	jmp    24c <strchr+0x31>
}

char*
strchr(const char *s, char c)
{
  for(; *s; s++)
 239:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 23d:	8b 45 08             	mov    0x8(%ebp),%eax
 240:	0f b6 00             	movzbl (%eax),%eax
 243:	84 c0                	test   %al,%al
 245:	75 e2                	jne    229 <strchr+0xe>
    if(*s == c)
      return (char*)s;
  return 0;
 247:	b8 00 00 00 00       	mov    $0x0,%eax
}
 24c:	c9                   	leave  
 24d:	c3                   	ret    

0000024e <gets>:

char*
gets(char *buf, int max)
{
 24e:	55                   	push   %ebp
 24f:	89 e5                	mov    %esp,%ebp
 251:	83 ec 18             	sub    $0x18,%esp
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 254:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 25b:	eb 42                	jmp    29f <gets+0x51>
    cc = read(0, &c, 1);
 25d:	83 ec 04             	sub    $0x4,%esp
 260:	6a 01                	push   $0x1
 262:	8d 45 ef             	lea    -0x11(%ebp),%eax
 265:	50                   	push   %eax
 266:	6a 00                	push   $0x0
 268:	e8 47 01 00 00       	call   3b4 <read>
 26d:	83 c4 10             	add    $0x10,%esp
 270:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if(cc < 1)
 273:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 277:	7e 33                	jle    2ac <gets+0x5e>
      break;
    buf[i++] = c;
 279:	8b 45 f4             	mov    -0xc(%ebp),%eax
 27c:	8d 50 01             	lea    0x1(%eax),%edx
 27f:	89 55 f4             	mov    %edx,-0xc(%ebp)
 282:	89 c2                	mov    %eax,%edx
 284:	8b 45 08             	mov    0x8(%ebp),%eax
 287:	01 c2                	add    %eax,%edx
 289:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 28d:	88 02                	mov    %al,(%edx)
    if(c == '\n' || c == '\r')
 28f:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 293:	3c 0a                	cmp    $0xa,%al
 295:	74 16                	je     2ad <gets+0x5f>
 297:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 29b:	3c 0d                	cmp    $0xd,%al
 29d:	74 0e                	je     2ad <gets+0x5f>
gets(char *buf, int max)
{
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 29f:	8b 45 f4             	mov    -0xc(%ebp),%eax
 2a2:	83 c0 01             	add    $0x1,%eax
 2a5:	3b 45 0c             	cmp    0xc(%ebp),%eax
 2a8:	7c b3                	jl     25d <gets+0xf>
 2aa:	eb 01                	jmp    2ad <gets+0x5f>
    cc = read(0, &c, 1);
    if(cc < 1)
      break;
 2ac:	90                   	nop
    buf[i++] = c;
    if(c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 2ad:	8b 55 f4             	mov    -0xc(%ebp),%edx
 2b0:	8b 45 08             	mov    0x8(%ebp),%eax
 2b3:	01 d0                	add    %edx,%eax
 2b5:	c6 00 00             	movb   $0x0,(%eax)
  return buf;
 2b8:	8b 45 08             	mov    0x8(%ebp),%eax
}
 2bb:	c9                   	leave  
 2bc:	c3                   	ret    

000002bd <stat>:

int
stat(char *n, struct stat *st)
{
 2bd:	55                   	push   %ebp
 2be:	89 e5                	mov    %esp,%ebp
 2c0:	83 ec 18             	sub    $0x18,%esp
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 2c3:	83 ec 08             	sub    $0x8,%esp
 2c6:	6a 00                	push   $0x0
 2c8:	ff 75 08             	pushl  0x8(%ebp)
 2cb:	e8 0c 01 00 00       	call   3dc <open>
 2d0:	83 c4 10             	add    $0x10,%esp
 2d3:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(fd < 0)
 2d6:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 2da:	79 07                	jns    2e3 <stat+0x26>
    return -1;
 2dc:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 2e1:	eb 25                	jmp    308 <stat+0x4b>
  r = fstat(fd, st);
 2e3:	83 ec 08             	sub    $0x8,%esp
 2e6:	ff 75 0c             	pushl  0xc(%ebp)
 2e9:	ff 75 f4             	pushl  -0xc(%ebp)
 2ec:	e8 03 01 00 00       	call   3f4 <fstat>
 2f1:	83 c4 10             	add    $0x10,%esp
 2f4:	89 45 f0             	mov    %eax,-0x10(%ebp)
  close(fd);
 2f7:	83 ec 0c             	sub    $0xc,%esp
 2fa:	ff 75 f4             	pushl  -0xc(%ebp)
 2fd:	e8 c2 00 00 00       	call   3c4 <close>
 302:	83 c4 10             	add    $0x10,%esp
  return r;
 305:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
 308:	c9                   	leave  
 309:	c3                   	ret    

0000030a <atoi>:

int
atoi(const char *s)
{
 30a:	55                   	push   %ebp
 30b:	89 e5                	mov    %esp,%ebp
 30d:	83 ec 10             	sub    $0x10,%esp
  int n;

  n = 0;
 310:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  while('0' <= *s && *s <= '9')
 317:	eb 25                	jmp    33e <atoi+0x34>
    n = n*10 + *s++ - '0';
 319:	8b 55 fc             	mov    -0x4(%ebp),%edx
 31c:	89 d0                	mov    %edx,%eax
 31e:	c1 e0 02             	shl    $0x2,%eax
 321:	01 d0                	add    %edx,%eax
 323:	01 c0                	add    %eax,%eax
 325:	89 c1                	mov    %eax,%ecx
 327:	8b 45 08             	mov    0x8(%ebp),%eax
 32a:	8d 50 01             	lea    0x1(%eax),%edx
 32d:	89 55 08             	mov    %edx,0x8(%ebp)
 330:	0f b6 00             	movzbl (%eax),%eax
 333:	0f be c0             	movsbl %al,%eax
 336:	01 c8                	add    %ecx,%eax
 338:	83 e8 30             	sub    $0x30,%eax
 33b:	89 45 fc             	mov    %eax,-0x4(%ebp)
atoi(const char *s)
{
  int n;

  n = 0;
  while('0' <= *s && *s <= '9')
 33e:	8b 45 08             	mov    0x8(%ebp),%eax
 341:	0f b6 00             	movzbl (%eax),%eax
 344:	3c 2f                	cmp    $0x2f,%al
 346:	7e 0a                	jle    352 <atoi+0x48>
 348:	8b 45 08             	mov    0x8(%ebp),%eax
 34b:	0f b6 00             	movzbl (%eax),%eax
 34e:	3c 39                	cmp    $0x39,%al
 350:	7e c7                	jle    319 <atoi+0xf>
    n = n*10 + *s++ - '0';
  return n;
 352:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 355:	c9                   	leave  
 356:	c3                   	ret    

00000357 <memmove>:

void*
memmove(void *vdst, void *vsrc, int n)
{
 357:	55                   	push   %ebp
 358:	89 e5                	mov    %esp,%ebp
 35a:	83 ec 10             	sub    $0x10,%esp
  char *dst, *src;
  
  dst = vdst;
 35d:	8b 45 08             	mov    0x8(%ebp),%eax
 360:	89 45 fc             	mov    %eax,-0x4(%ebp)
  src = vsrc;
 363:	8b 45 0c             	mov    0xc(%ebp),%eax
 366:	89 45 f8             	mov    %eax,-0x8(%ebp)
  while(n-- > 0)
 369:	eb 17                	jmp    382 <memmove+0x2b>
    *dst++ = *src++;
 36b:	8b 45 fc             	mov    -0x4(%ebp),%eax
 36e:	8d 50 01             	lea    0x1(%eax),%edx
 371:	89 55 fc             	mov    %edx,-0x4(%ebp)
 374:	8b 55 f8             	mov    -0x8(%ebp),%edx
 377:	8d 4a 01             	lea    0x1(%edx),%ecx
 37a:	89 4d f8             	mov    %ecx,-0x8(%ebp)
 37d:	0f b6 12             	movzbl (%edx),%edx
 380:	88 10                	mov    %dl,(%eax)
{
  char *dst, *src;
  
  dst = vdst;
  src = vsrc;
  while(n-- > 0)
 382:	8b 45 10             	mov    0x10(%ebp),%eax
 385:	8d 50 ff             	lea    -0x1(%eax),%edx
 388:	89 55 10             	mov    %edx,0x10(%ebp)
 38b:	85 c0                	test   %eax,%eax
 38d:	7f dc                	jg     36b <memmove+0x14>
    *dst++ = *src++;
  return vdst;
 38f:	8b 45 08             	mov    0x8(%ebp),%eax
}
 392:	c9                   	leave  
 393:	c3                   	ret    

00000394 <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
 394:	b8 01 00 00 00       	mov    $0x1,%eax
 399:	cd 40                	int    $0x40
 39b:	c3                   	ret    

0000039c <exit>:
SYSCALL(exit)
 39c:	b8 02 00 00 00       	mov    $0x2,%eax
 3a1:	cd 40                	int    $0x40
 3a3:	c3                   	ret    

000003a4 <wait>:
SYSCALL(wait)
 3a4:	b8 03 00 00 00       	mov    $0x3,%eax
 3a9:	cd 40                	int    $0x40
 3ab:	c3                   	ret    

000003ac <pipe>:
SYSCALL(pipe)
 3ac:	b8 04 00 00 00       	mov    $0x4,%eax
 3b1:	cd 40                	int    $0x40
 3b3:	c3                   	ret    

000003b4 <read>:
SYSCALL(read)
 3b4:	b8 05 00 00 00       	mov    $0x5,%eax
 3b9:	cd 40                	int    $0x40
 3bb:	c3                   	ret    

000003bc <write>:
SYSCALL(write)
 3bc:	b8 10 00 00 00       	mov    $0x10,%eax
 3c1:	cd 40                	int    $0x40
 3c3:	c3                   	ret    

000003c4 <close>:
SYSCALL(close)
 3c4:	b8 15 00 00 00       	mov    $0x15,%eax
 3c9:	cd 40                	int    $0x40
 3cb:	c3                   	ret    

000003cc <kill>:
SYSCALL(kill)
 3cc:	b8 06 00 00 00       	mov    $0x6,%eax
 3d1:	cd 40                	int    $0x40
 3d3:	c3                   	ret    

000003d4 <exec>:
SYSCALL(exec)
 3d4:	b8 07 00 00 00       	mov    $0x7,%eax
 3d9:	cd 40                	int    $0x40
 3db:	c3                   	ret    

000003dc <open>:
SYSCALL(open)
 3dc:	b8 0f 00 00 00       	mov    $0xf,%eax
 3e1:	cd 40                	int    $0x40
 3e3:	c3                   	ret    

000003e4 <mknod>:
SYSCALL(mknod)
 3e4:	b8 11 00 00 00       	mov    $0x11,%eax
 3e9:	cd 40                	int    $0x40
 3eb:	c3                   	ret    

000003ec <unlink>:
SYSCALL(unlink)
 3ec:	b8 12 00 00 00       	mov    $0x12,%eax
 3f1:	cd 40                	int    $0x40
 3f3:	c3                   	ret    

000003f4 <fstat>:
SYSCALL(fstat)
 3f4:	b8 08 00 00 00       	mov    $0x8,%eax
 3f9:	cd 40                	int    $0x40
 3fb:	c3                   	ret    

000003fc <link>:
SYSCALL(link)
 3fc:	b8 13 00 00 00       	mov    $0x13,%eax
 401:	cd 40                	int    $0x40
 403:	c3                   	ret    

00000404 <mkdir>:
SYSCALL(mkdir)
 404:	b8 14 00 00 00       	mov    $0x14,%eax
 409:	cd 40                	int    $0x40
 40b:	c3                   	ret    

0000040c <chdir>:
SYSCALL(chdir)
 40c:	b8 09 00 00 00       	mov    $0x9,%eax
 411:	cd 40                	int    $0x40
 413:	c3                   	ret    

00000414 <dup>:
SYSCALL(dup)
 414:	b8 0a 00 00 00       	mov    $0xa,%eax
 419:	cd 40                	int    $0x40
 41b:	c3                   	ret    

0000041c <getpid>:
SYSCALL(getpid)
 41c:	b8 0b 00 00 00       	mov    $0xb,%eax
 421:	cd 40                	int    $0x40
 423:	c3                   	ret    

00000424 <sbrk>:
SYSCALL(sbrk)
 424:	b8 0c 00 00 00       	mov    $0xc,%eax
 429:	cd 40                	int    $0x40
 42b:	c3                   	ret    

0000042c <sleep>:
SYSCALL(sleep)
 42c:	b8 0d 00 00 00       	mov    $0xd,%eax
 431:	cd 40                	int    $0x40
 433:	c3                   	ret    

00000434 <uptime>:
SYSCALL(uptime)
 434:	b8 0e 00 00 00       	mov    $0xe,%eax
 439:	cd 40                	int    $0x40
 43b:	c3                   	ret    

0000043c <halt>:
SYSCALL(halt)
 43c:	b8 16 00 00 00       	mov    $0x16,%eax
 441:	cd 40                	int    $0x40
 443:	c3                   	ret    

00000444 <date>:
SYSCALL(date)
 444:	b8 17 00 00 00       	mov    $0x17,%eax
 449:	cd 40                	int    $0x40
 44b:	c3                   	ret    

0000044c <getuid>:
SYSCALL(getuid)
 44c:	b8 18 00 00 00       	mov    $0x18,%eax
 451:	cd 40                	int    $0x40
 453:	c3                   	ret    

00000454 <getgid>:
SYSCALL(getgid)
 454:	b8 19 00 00 00       	mov    $0x19,%eax
 459:	cd 40                	int    $0x40
 45b:	c3                   	ret    

0000045c <getppid>:
SYSCALL(getppid)
 45c:	b8 1a 00 00 00       	mov    $0x1a,%eax
 461:	cd 40                	int    $0x40
 463:	c3                   	ret    

00000464 <setuid>:
SYSCALL(setuid)
 464:	b8 1b 00 00 00       	mov    $0x1b,%eax
 469:	cd 40                	int    $0x40
 46b:	c3                   	ret    

0000046c <setgid>:
SYSCALL(setgid)
 46c:	b8 1c 00 00 00       	mov    $0x1c,%eax
 471:	cd 40                	int    $0x40
 473:	c3                   	ret    

00000474 <getprocs>:
SYSCALL(getprocs)
 474:	b8 1d 00 00 00       	mov    $0x1d,%eax
 479:	cd 40                	int    $0x40
 47b:	c3                   	ret    

0000047c <setpriority>:
SYSCALL(setpriority)
 47c:	b8 1e 00 00 00       	mov    $0x1e,%eax
 481:	cd 40                	int    $0x40
 483:	c3                   	ret    

00000484 <putc>:
#include "stat.h"
#include "user.h"

static void
putc(int fd, char c)
{
 484:	55                   	push   %ebp
 485:	89 e5                	mov    %esp,%ebp
 487:	83 ec 18             	sub    $0x18,%esp
 48a:	8b 45 0c             	mov    0xc(%ebp),%eax
 48d:	88 45 f4             	mov    %al,-0xc(%ebp)
  write(fd, &c, 1);
 490:	83 ec 04             	sub    $0x4,%esp
 493:	6a 01                	push   $0x1
 495:	8d 45 f4             	lea    -0xc(%ebp),%eax
 498:	50                   	push   %eax
 499:	ff 75 08             	pushl  0x8(%ebp)
 49c:	e8 1b ff ff ff       	call   3bc <write>
 4a1:	83 c4 10             	add    $0x10,%esp
}
 4a4:	90                   	nop
 4a5:	c9                   	leave  
 4a6:	c3                   	ret    

000004a7 <printint>:

static void
printint(int fd, int xx, int base, int sgn)
{
 4a7:	55                   	push   %ebp
 4a8:	89 e5                	mov    %esp,%ebp
 4aa:	53                   	push   %ebx
 4ab:	83 ec 24             	sub    $0x24,%esp
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 4ae:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  if(sgn && xx < 0){
 4b5:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
 4b9:	74 17                	je     4d2 <printint+0x2b>
 4bb:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
 4bf:	79 11                	jns    4d2 <printint+0x2b>
    neg = 1;
 4c1:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
    x = -xx;
 4c8:	8b 45 0c             	mov    0xc(%ebp),%eax
 4cb:	f7 d8                	neg    %eax
 4cd:	89 45 ec             	mov    %eax,-0x14(%ebp)
 4d0:	eb 06                	jmp    4d8 <printint+0x31>
  } else {
    x = xx;
 4d2:	8b 45 0c             	mov    0xc(%ebp),%eax
 4d5:	89 45 ec             	mov    %eax,-0x14(%ebp)
  }

  i = 0;
 4d8:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  do{
    buf[i++] = digits[x % base];
 4df:	8b 4d f4             	mov    -0xc(%ebp),%ecx
 4e2:	8d 41 01             	lea    0x1(%ecx),%eax
 4e5:	89 45 f4             	mov    %eax,-0xc(%ebp)
 4e8:	8b 5d 10             	mov    0x10(%ebp),%ebx
 4eb:	8b 45 ec             	mov    -0x14(%ebp),%eax
 4ee:	ba 00 00 00 00       	mov    $0x0,%edx
 4f3:	f7 f3                	div    %ebx
 4f5:	89 d0                	mov    %edx,%eax
 4f7:	0f b6 80 c0 0c 00 00 	movzbl 0xcc0(%eax),%eax
 4fe:	88 44 0d dc          	mov    %al,-0x24(%ebp,%ecx,1)
  }while((x /= base) != 0);
 502:	8b 5d 10             	mov    0x10(%ebp),%ebx
 505:	8b 45 ec             	mov    -0x14(%ebp),%eax
 508:	ba 00 00 00 00       	mov    $0x0,%edx
 50d:	f7 f3                	div    %ebx
 50f:	89 45 ec             	mov    %eax,-0x14(%ebp)
 512:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 516:	75 c7                	jne    4df <printint+0x38>
  if(neg)
 518:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 51c:	74 2d                	je     54b <printint+0xa4>
    buf[i++] = '-';
 51e:	8b 45 f4             	mov    -0xc(%ebp),%eax
 521:	8d 50 01             	lea    0x1(%eax),%edx
 524:	89 55 f4             	mov    %edx,-0xc(%ebp)
 527:	c6 44 05 dc 2d       	movb   $0x2d,-0x24(%ebp,%eax,1)

  while(--i >= 0)
 52c:	eb 1d                	jmp    54b <printint+0xa4>
    putc(fd, buf[i]);
 52e:	8d 55 dc             	lea    -0x24(%ebp),%edx
 531:	8b 45 f4             	mov    -0xc(%ebp),%eax
 534:	01 d0                	add    %edx,%eax
 536:	0f b6 00             	movzbl (%eax),%eax
 539:	0f be c0             	movsbl %al,%eax
 53c:	83 ec 08             	sub    $0x8,%esp
 53f:	50                   	push   %eax
 540:	ff 75 08             	pushl  0x8(%ebp)
 543:	e8 3c ff ff ff       	call   484 <putc>
 548:	83 c4 10             	add    $0x10,%esp
    buf[i++] = digits[x % base];
  }while((x /= base) != 0);
  if(neg)
    buf[i++] = '-';

  while(--i >= 0)
 54b:	83 6d f4 01          	subl   $0x1,-0xc(%ebp)
 54f:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 553:	79 d9                	jns    52e <printint+0x87>
    putc(fd, buf[i]);
}
 555:	90                   	nop
 556:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 559:	c9                   	leave  
 55a:	c3                   	ret    

0000055b <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, char *fmt, ...)
{
 55b:	55                   	push   %ebp
 55c:	89 e5                	mov    %esp,%ebp
 55e:	83 ec 28             	sub    $0x28,%esp
  char *s;
  int c, i, state;
  uint *ap;

  state = 0;
 561:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  ap = (uint*)(void*)&fmt + 1;
 568:	8d 45 0c             	lea    0xc(%ebp),%eax
 56b:	83 c0 04             	add    $0x4,%eax
 56e:	89 45 e8             	mov    %eax,-0x18(%ebp)
  for(i = 0; fmt[i]; i++){
 571:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 578:	e9 59 01 00 00       	jmp    6d6 <printf+0x17b>
    c = fmt[i] & 0xff;
 57d:	8b 55 0c             	mov    0xc(%ebp),%edx
 580:	8b 45 f0             	mov    -0x10(%ebp),%eax
 583:	01 d0                	add    %edx,%eax
 585:	0f b6 00             	movzbl (%eax),%eax
 588:	0f be c0             	movsbl %al,%eax
 58b:	25 ff 00 00 00       	and    $0xff,%eax
 590:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    if(state == 0){
 593:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 597:	75 2c                	jne    5c5 <printf+0x6a>
      if(c == '%'){
 599:	83 7d e4 25          	cmpl   $0x25,-0x1c(%ebp)
 59d:	75 0c                	jne    5ab <printf+0x50>
        state = '%';
 59f:	c7 45 ec 25 00 00 00 	movl   $0x25,-0x14(%ebp)
 5a6:	e9 27 01 00 00       	jmp    6d2 <printf+0x177>
      } else {
        putc(fd, c);
 5ab:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 5ae:	0f be c0             	movsbl %al,%eax
 5b1:	83 ec 08             	sub    $0x8,%esp
 5b4:	50                   	push   %eax
 5b5:	ff 75 08             	pushl  0x8(%ebp)
 5b8:	e8 c7 fe ff ff       	call   484 <putc>
 5bd:	83 c4 10             	add    $0x10,%esp
 5c0:	e9 0d 01 00 00       	jmp    6d2 <printf+0x177>
      }
    } else if(state == '%'){
 5c5:	83 7d ec 25          	cmpl   $0x25,-0x14(%ebp)
 5c9:	0f 85 03 01 00 00    	jne    6d2 <printf+0x177>
      if(c == 'd'){
 5cf:	83 7d e4 64          	cmpl   $0x64,-0x1c(%ebp)
 5d3:	75 1e                	jne    5f3 <printf+0x98>
        printint(fd, *ap, 10, 1);
 5d5:	8b 45 e8             	mov    -0x18(%ebp),%eax
 5d8:	8b 00                	mov    (%eax),%eax
 5da:	6a 01                	push   $0x1
 5dc:	6a 0a                	push   $0xa
 5de:	50                   	push   %eax
 5df:	ff 75 08             	pushl  0x8(%ebp)
 5e2:	e8 c0 fe ff ff       	call   4a7 <printint>
 5e7:	83 c4 10             	add    $0x10,%esp
        ap++;
 5ea:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 5ee:	e9 d8 00 00 00       	jmp    6cb <printf+0x170>
      } else if(c == 'x' || c == 'p'){
 5f3:	83 7d e4 78          	cmpl   $0x78,-0x1c(%ebp)
 5f7:	74 06                	je     5ff <printf+0xa4>
 5f9:	83 7d e4 70          	cmpl   $0x70,-0x1c(%ebp)
 5fd:	75 1e                	jne    61d <printf+0xc2>
        printint(fd, *ap, 16, 0);
 5ff:	8b 45 e8             	mov    -0x18(%ebp),%eax
 602:	8b 00                	mov    (%eax),%eax
 604:	6a 00                	push   $0x0
 606:	6a 10                	push   $0x10
 608:	50                   	push   %eax
 609:	ff 75 08             	pushl  0x8(%ebp)
 60c:	e8 96 fe ff ff       	call   4a7 <printint>
 611:	83 c4 10             	add    $0x10,%esp
        ap++;
 614:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 618:	e9 ae 00 00 00       	jmp    6cb <printf+0x170>
      } else if(c == 's'){
 61d:	83 7d e4 73          	cmpl   $0x73,-0x1c(%ebp)
 621:	75 43                	jne    666 <printf+0x10b>
        s = (char*)*ap;
 623:	8b 45 e8             	mov    -0x18(%ebp),%eax
 626:	8b 00                	mov    (%eax),%eax
 628:	89 45 f4             	mov    %eax,-0xc(%ebp)
        ap++;
 62b:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
        if(s == 0)
 62f:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 633:	75 25                	jne    65a <printf+0xff>
          s = "(null)";
 635:	c7 45 f4 6e 0a 00 00 	movl   $0xa6e,-0xc(%ebp)
        while(*s != 0){
 63c:	eb 1c                	jmp    65a <printf+0xff>
          putc(fd, *s);
 63e:	8b 45 f4             	mov    -0xc(%ebp),%eax
 641:	0f b6 00             	movzbl (%eax),%eax
 644:	0f be c0             	movsbl %al,%eax
 647:	83 ec 08             	sub    $0x8,%esp
 64a:	50                   	push   %eax
 64b:	ff 75 08             	pushl  0x8(%ebp)
 64e:	e8 31 fe ff ff       	call   484 <putc>
 653:	83 c4 10             	add    $0x10,%esp
          s++;
 656:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
      } else if(c == 's'){
        s = (char*)*ap;
        ap++;
        if(s == 0)
          s = "(null)";
        while(*s != 0){
 65a:	8b 45 f4             	mov    -0xc(%ebp),%eax
 65d:	0f b6 00             	movzbl (%eax),%eax
 660:	84 c0                	test   %al,%al
 662:	75 da                	jne    63e <printf+0xe3>
 664:	eb 65                	jmp    6cb <printf+0x170>
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 666:	83 7d e4 63          	cmpl   $0x63,-0x1c(%ebp)
 66a:	75 1d                	jne    689 <printf+0x12e>
        putc(fd, *ap);
 66c:	8b 45 e8             	mov    -0x18(%ebp),%eax
 66f:	8b 00                	mov    (%eax),%eax
 671:	0f be c0             	movsbl %al,%eax
 674:	83 ec 08             	sub    $0x8,%esp
 677:	50                   	push   %eax
 678:	ff 75 08             	pushl  0x8(%ebp)
 67b:	e8 04 fe ff ff       	call   484 <putc>
 680:	83 c4 10             	add    $0x10,%esp
        ap++;
 683:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 687:	eb 42                	jmp    6cb <printf+0x170>
      } else if(c == '%'){
 689:	83 7d e4 25          	cmpl   $0x25,-0x1c(%ebp)
 68d:	75 17                	jne    6a6 <printf+0x14b>
        putc(fd, c);
 68f:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 692:	0f be c0             	movsbl %al,%eax
 695:	83 ec 08             	sub    $0x8,%esp
 698:	50                   	push   %eax
 699:	ff 75 08             	pushl  0x8(%ebp)
 69c:	e8 e3 fd ff ff       	call   484 <putc>
 6a1:	83 c4 10             	add    $0x10,%esp
 6a4:	eb 25                	jmp    6cb <printf+0x170>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 6a6:	83 ec 08             	sub    $0x8,%esp
 6a9:	6a 25                	push   $0x25
 6ab:	ff 75 08             	pushl  0x8(%ebp)
 6ae:	e8 d1 fd ff ff       	call   484 <putc>
 6b3:	83 c4 10             	add    $0x10,%esp
        putc(fd, c);
 6b6:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 6b9:	0f be c0             	movsbl %al,%eax
 6bc:	83 ec 08             	sub    $0x8,%esp
 6bf:	50                   	push   %eax
 6c0:	ff 75 08             	pushl  0x8(%ebp)
 6c3:	e8 bc fd ff ff       	call   484 <putc>
 6c8:	83 c4 10             	add    $0x10,%esp
      }
      state = 0;
 6cb:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
 6d2:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
 6d6:	8b 55 0c             	mov    0xc(%ebp),%edx
 6d9:	8b 45 f0             	mov    -0x10(%ebp),%eax
 6dc:	01 d0                	add    %edx,%eax
 6de:	0f b6 00             	movzbl (%eax),%eax
 6e1:	84 c0                	test   %al,%al
 6e3:	0f 85 94 fe ff ff    	jne    57d <printf+0x22>
        putc(fd, c);
      }
      state = 0;
    }
  }
}
 6e9:	90                   	nop
 6ea:	c9                   	leave  
 6eb:	c3                   	ret    

000006ec <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 6ec:	55                   	push   %ebp
 6ed:	89 e5                	mov    %esp,%ebp
 6ef:	83 ec 10             	sub    $0x10,%esp
  Header *bp, *p;

  bp = (Header*)ap - 1;
 6f2:	8b 45 08             	mov    0x8(%ebp),%eax
 6f5:	83 e8 08             	sub    $0x8,%eax
 6f8:	89 45 f8             	mov    %eax,-0x8(%ebp)
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 6fb:	a1 dc 0c 00 00       	mov    0xcdc,%eax
 700:	89 45 fc             	mov    %eax,-0x4(%ebp)
 703:	eb 24                	jmp    729 <free+0x3d>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 705:	8b 45 fc             	mov    -0x4(%ebp),%eax
 708:	8b 00                	mov    (%eax),%eax
 70a:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 70d:	77 12                	ja     721 <free+0x35>
 70f:	8b 45 f8             	mov    -0x8(%ebp),%eax
 712:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 715:	77 24                	ja     73b <free+0x4f>
 717:	8b 45 fc             	mov    -0x4(%ebp),%eax
 71a:	8b 00                	mov    (%eax),%eax
 71c:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 71f:	77 1a                	ja     73b <free+0x4f>
free(void *ap)
{
  Header *bp, *p;

  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 721:	8b 45 fc             	mov    -0x4(%ebp),%eax
 724:	8b 00                	mov    (%eax),%eax
 726:	89 45 fc             	mov    %eax,-0x4(%ebp)
 729:	8b 45 f8             	mov    -0x8(%ebp),%eax
 72c:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 72f:	76 d4                	jbe    705 <free+0x19>
 731:	8b 45 fc             	mov    -0x4(%ebp),%eax
 734:	8b 00                	mov    (%eax),%eax
 736:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 739:	76 ca                	jbe    705 <free+0x19>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if(bp + bp->s.size == p->s.ptr){
 73b:	8b 45 f8             	mov    -0x8(%ebp),%eax
 73e:	8b 40 04             	mov    0x4(%eax),%eax
 741:	8d 14 c5 00 00 00 00 	lea    0x0(,%eax,8),%edx
 748:	8b 45 f8             	mov    -0x8(%ebp),%eax
 74b:	01 c2                	add    %eax,%edx
 74d:	8b 45 fc             	mov    -0x4(%ebp),%eax
 750:	8b 00                	mov    (%eax),%eax
 752:	39 c2                	cmp    %eax,%edx
 754:	75 24                	jne    77a <free+0x8e>
    bp->s.size += p->s.ptr->s.size;
 756:	8b 45 f8             	mov    -0x8(%ebp),%eax
 759:	8b 50 04             	mov    0x4(%eax),%edx
 75c:	8b 45 fc             	mov    -0x4(%ebp),%eax
 75f:	8b 00                	mov    (%eax),%eax
 761:	8b 40 04             	mov    0x4(%eax),%eax
 764:	01 c2                	add    %eax,%edx
 766:	8b 45 f8             	mov    -0x8(%ebp),%eax
 769:	89 50 04             	mov    %edx,0x4(%eax)
    bp->s.ptr = p->s.ptr->s.ptr;
 76c:	8b 45 fc             	mov    -0x4(%ebp),%eax
 76f:	8b 00                	mov    (%eax),%eax
 771:	8b 10                	mov    (%eax),%edx
 773:	8b 45 f8             	mov    -0x8(%ebp),%eax
 776:	89 10                	mov    %edx,(%eax)
 778:	eb 0a                	jmp    784 <free+0x98>
  } else
    bp->s.ptr = p->s.ptr;
 77a:	8b 45 fc             	mov    -0x4(%ebp),%eax
 77d:	8b 10                	mov    (%eax),%edx
 77f:	8b 45 f8             	mov    -0x8(%ebp),%eax
 782:	89 10                	mov    %edx,(%eax)
  if(p + p->s.size == bp){
 784:	8b 45 fc             	mov    -0x4(%ebp),%eax
 787:	8b 40 04             	mov    0x4(%eax),%eax
 78a:	8d 14 c5 00 00 00 00 	lea    0x0(,%eax,8),%edx
 791:	8b 45 fc             	mov    -0x4(%ebp),%eax
 794:	01 d0                	add    %edx,%eax
 796:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 799:	75 20                	jne    7bb <free+0xcf>
    p->s.size += bp->s.size;
 79b:	8b 45 fc             	mov    -0x4(%ebp),%eax
 79e:	8b 50 04             	mov    0x4(%eax),%edx
 7a1:	8b 45 f8             	mov    -0x8(%ebp),%eax
 7a4:	8b 40 04             	mov    0x4(%eax),%eax
 7a7:	01 c2                	add    %eax,%edx
 7a9:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7ac:	89 50 04             	mov    %edx,0x4(%eax)
    p->s.ptr = bp->s.ptr;
 7af:	8b 45 f8             	mov    -0x8(%ebp),%eax
 7b2:	8b 10                	mov    (%eax),%edx
 7b4:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7b7:	89 10                	mov    %edx,(%eax)
 7b9:	eb 08                	jmp    7c3 <free+0xd7>
  } else
    p->s.ptr = bp;
 7bb:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7be:	8b 55 f8             	mov    -0x8(%ebp),%edx
 7c1:	89 10                	mov    %edx,(%eax)
  freep = p;
 7c3:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7c6:	a3 dc 0c 00 00       	mov    %eax,0xcdc
}
 7cb:	90                   	nop
 7cc:	c9                   	leave  
 7cd:	c3                   	ret    

000007ce <morecore>:

static Header*
morecore(uint nu)
{
 7ce:	55                   	push   %ebp
 7cf:	89 e5                	mov    %esp,%ebp
 7d1:	83 ec 18             	sub    $0x18,%esp
  char *p;
  Header *hp;

  if(nu < 4096)
 7d4:	81 7d 08 ff 0f 00 00 	cmpl   $0xfff,0x8(%ebp)
 7db:	77 07                	ja     7e4 <morecore+0x16>
    nu = 4096;
 7dd:	c7 45 08 00 10 00 00 	movl   $0x1000,0x8(%ebp)
  p = sbrk(nu * sizeof(Header));
 7e4:	8b 45 08             	mov    0x8(%ebp),%eax
 7e7:	c1 e0 03             	shl    $0x3,%eax
 7ea:	83 ec 0c             	sub    $0xc,%esp
 7ed:	50                   	push   %eax
 7ee:	e8 31 fc ff ff       	call   424 <sbrk>
 7f3:	83 c4 10             	add    $0x10,%esp
 7f6:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(p == (char*)-1)
 7f9:	83 7d f4 ff          	cmpl   $0xffffffff,-0xc(%ebp)
 7fd:	75 07                	jne    806 <morecore+0x38>
    return 0;
 7ff:	b8 00 00 00 00       	mov    $0x0,%eax
 804:	eb 26                	jmp    82c <morecore+0x5e>
  hp = (Header*)p;
 806:	8b 45 f4             	mov    -0xc(%ebp),%eax
 809:	89 45 f0             	mov    %eax,-0x10(%ebp)
  hp->s.size = nu;
 80c:	8b 45 f0             	mov    -0x10(%ebp),%eax
 80f:	8b 55 08             	mov    0x8(%ebp),%edx
 812:	89 50 04             	mov    %edx,0x4(%eax)
  free((void*)(hp + 1));
 815:	8b 45 f0             	mov    -0x10(%ebp),%eax
 818:	83 c0 08             	add    $0x8,%eax
 81b:	83 ec 0c             	sub    $0xc,%esp
 81e:	50                   	push   %eax
 81f:	e8 c8 fe ff ff       	call   6ec <free>
 824:	83 c4 10             	add    $0x10,%esp
  return freep;
 827:	a1 dc 0c 00 00       	mov    0xcdc,%eax
}
 82c:	c9                   	leave  
 82d:	c3                   	ret    

0000082e <malloc>:

void*
malloc(uint nbytes)
{
 82e:	55                   	push   %ebp
 82f:	89 e5                	mov    %esp,%ebp
 831:	83 ec 18             	sub    $0x18,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 834:	8b 45 08             	mov    0x8(%ebp),%eax
 837:	83 c0 07             	add    $0x7,%eax
 83a:	c1 e8 03             	shr    $0x3,%eax
 83d:	83 c0 01             	add    $0x1,%eax
 840:	89 45 ec             	mov    %eax,-0x14(%ebp)
  if((prevp = freep) == 0){
 843:	a1 dc 0c 00 00       	mov    0xcdc,%eax
 848:	89 45 f0             	mov    %eax,-0x10(%ebp)
 84b:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 84f:	75 23                	jne    874 <malloc+0x46>
    base.s.ptr = freep = prevp = &base;
 851:	c7 45 f0 d4 0c 00 00 	movl   $0xcd4,-0x10(%ebp)
 858:	8b 45 f0             	mov    -0x10(%ebp),%eax
 85b:	a3 dc 0c 00 00       	mov    %eax,0xcdc
 860:	a1 dc 0c 00 00       	mov    0xcdc,%eax
 865:	a3 d4 0c 00 00       	mov    %eax,0xcd4
    base.s.size = 0;
 86a:	c7 05 d8 0c 00 00 00 	movl   $0x0,0xcd8
 871:	00 00 00 
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 874:	8b 45 f0             	mov    -0x10(%ebp),%eax
 877:	8b 00                	mov    (%eax),%eax
 879:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if(p->s.size >= nunits){
 87c:	8b 45 f4             	mov    -0xc(%ebp),%eax
 87f:	8b 40 04             	mov    0x4(%eax),%eax
 882:	3b 45 ec             	cmp    -0x14(%ebp),%eax
 885:	72 4d                	jb     8d4 <malloc+0xa6>
      if(p->s.size == nunits)
 887:	8b 45 f4             	mov    -0xc(%ebp),%eax
 88a:	8b 40 04             	mov    0x4(%eax),%eax
 88d:	3b 45 ec             	cmp    -0x14(%ebp),%eax
 890:	75 0c                	jne    89e <malloc+0x70>
        prevp->s.ptr = p->s.ptr;
 892:	8b 45 f4             	mov    -0xc(%ebp),%eax
 895:	8b 10                	mov    (%eax),%edx
 897:	8b 45 f0             	mov    -0x10(%ebp),%eax
 89a:	89 10                	mov    %edx,(%eax)
 89c:	eb 26                	jmp    8c4 <malloc+0x96>
      else {
        p->s.size -= nunits;
 89e:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8a1:	8b 40 04             	mov    0x4(%eax),%eax
 8a4:	2b 45 ec             	sub    -0x14(%ebp),%eax
 8a7:	89 c2                	mov    %eax,%edx
 8a9:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8ac:	89 50 04             	mov    %edx,0x4(%eax)
        p += p->s.size;
 8af:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8b2:	8b 40 04             	mov    0x4(%eax),%eax
 8b5:	c1 e0 03             	shl    $0x3,%eax
 8b8:	01 45 f4             	add    %eax,-0xc(%ebp)
        p->s.size = nunits;
 8bb:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8be:	8b 55 ec             	mov    -0x14(%ebp),%edx
 8c1:	89 50 04             	mov    %edx,0x4(%eax)
      }
      freep = prevp;
 8c4:	8b 45 f0             	mov    -0x10(%ebp),%eax
 8c7:	a3 dc 0c 00 00       	mov    %eax,0xcdc
      return (void*)(p + 1);
 8cc:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8cf:	83 c0 08             	add    $0x8,%eax
 8d2:	eb 3b                	jmp    90f <malloc+0xe1>
    }
    if(p == freep)
 8d4:	a1 dc 0c 00 00       	mov    0xcdc,%eax
 8d9:	39 45 f4             	cmp    %eax,-0xc(%ebp)
 8dc:	75 1e                	jne    8fc <malloc+0xce>
      if((p = morecore(nunits)) == 0)
 8de:	83 ec 0c             	sub    $0xc,%esp
 8e1:	ff 75 ec             	pushl  -0x14(%ebp)
 8e4:	e8 e5 fe ff ff       	call   7ce <morecore>
 8e9:	83 c4 10             	add    $0x10,%esp
 8ec:	89 45 f4             	mov    %eax,-0xc(%ebp)
 8ef:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 8f3:	75 07                	jne    8fc <malloc+0xce>
        return 0;
 8f5:	b8 00 00 00 00       	mov    $0x0,%eax
 8fa:	eb 13                	jmp    90f <malloc+0xe1>
  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
  if((prevp = freep) == 0){
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 8fc:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8ff:	89 45 f0             	mov    %eax,-0x10(%ebp)
 902:	8b 45 f4             	mov    -0xc(%ebp),%eax
 905:	8b 00                	mov    (%eax),%eax
 907:	89 45 f4             	mov    %eax,-0xc(%ebp)
      return (void*)(p + 1);
    }
    if(p == freep)
      if((p = morecore(nunits)) == 0)
        return 0;
  }
 90a:	e9 6d ff ff ff       	jmp    87c <malloc+0x4e>
}
 90f:	c9                   	leave  
 910:	c3                   	ret    
