
_ps-test:     file format elf32-i386


Disassembly of section .text:

00000000 <forktest>:
#include "user.h"
#define TPS 100

void
forktest(int N)
{
   0:	55                   	push   %ebp
   1:	89 e5                	mov    %esp,%ebp
   3:	83 ec 18             	sub    $0x18,%esp
  int n, pid;

  printf(1, "fork test\n");
   6:	83 ec 08             	sub    $0x8,%esp
   9:	68 e0 08 00 00       	push   $0x8e0
   e:	6a 01                	push   $0x1
  10:	e8 15 05 00 00       	call   52a <printf>
  15:	83 c4 10             	add    $0x10,%esp

  for(n=0; n<N; n++){
  18:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  1f:	eb 2d                	jmp    4e <forktest+0x4e>
    pid = fork();
  21:	e8 3d 03 00 00       	call   363 <fork>
  26:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if(pid < 0)
  29:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
  2d:	78 29                	js     58 <forktest+0x58>
      break;
    if(pid == 0) {
  2f:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
  33:	75 15                	jne    4a <forktest+0x4a>
      sleep(10*TPS);
  35:	83 ec 0c             	sub    $0xc,%esp
  38:	68 e8 03 00 00       	push   $0x3e8
  3d:	e8 b9 03 00 00       	call   3fb <sleep>
  42:	83 c4 10             	add    $0x10,%esp
      exit();
  45:	e8 21 03 00 00       	call   36b <exit>
{
  int n, pid;

  printf(1, "fork test\n");

  for(n=0; n<N; n++){
  4a:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
  4e:	8b 45 f4             	mov    -0xc(%ebp),%eax
  51:	3b 45 08             	cmp    0x8(%ebp),%eax
  54:	7c cb                	jl     21 <forktest+0x21>
  56:	eb 27                	jmp    7f <forktest+0x7f>
    pid = fork();
    if(pid < 0)
      break;
  58:	90                   	nop
      sleep(10*TPS);
      exit();
    }
  }
  
  for(; n > 0; n--){
  59:	eb 24                	jmp    7f <forktest+0x7f>
    if(wait() < 0){
  5b:	e8 13 03 00 00       	call   373 <wait>
  60:	85 c0                	test   %eax,%eax
  62:	79 17                	jns    7b <forktest+0x7b>
      printf(1, "wait stopped early\n");
  64:	83 ec 08             	sub    $0x8,%esp
  67:	68 eb 08 00 00       	push   $0x8eb
  6c:	6a 01                	push   $0x1
  6e:	e8 b7 04 00 00       	call   52a <printf>
  73:	83 c4 10             	add    $0x10,%esp
      exit();
  76:	e8 f0 02 00 00       	call   36b <exit>
      sleep(10*TPS);
      exit();
    }
  }
  
  for(; n > 0; n--){
  7b:	83 6d f4 01          	subl   $0x1,-0xc(%ebp)
  7f:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
  83:	7f d6                	jg     5b <forktest+0x5b>
      printf(1, "wait stopped early\n");
      exit();
    }
  }
  
  if(wait() != -1){
  85:	e8 e9 02 00 00       	call   373 <wait>
  8a:	83 f8 ff             	cmp    $0xffffffff,%eax
  8d:	74 17                	je     a6 <forktest+0xa6>
    printf(1, "wait got too many\n");
  8f:	83 ec 08             	sub    $0x8,%esp
  92:	68 ff 08 00 00       	push   $0x8ff
  97:	6a 01                	push   $0x1
  99:	e8 8c 04 00 00       	call   52a <printf>
  9e:	83 c4 10             	add    $0x10,%esp
    exit();
  a1:	e8 c5 02 00 00       	call   36b <exit>
  }
  
  printf(1, "fork test OK\n");
  a6:	83 ec 08             	sub    $0x8,%esp
  a9:	68 12 09 00 00       	push   $0x912
  ae:	6a 01                	push   $0x1
  b0:	e8 75 04 00 00       	call   52a <printf>
  b5:	83 c4 10             	add    $0x10,%esp
}
  b8:	90                   	nop
  b9:	c9                   	leave  
  ba:	c3                   	ret    

000000bb <main>:

int
main(int argc, char **argv)
{
  bb:	8d 4c 24 04          	lea    0x4(%esp),%ecx
  bf:	83 e4 f0             	and    $0xfffffff0,%esp
  c2:	ff 71 fc             	pushl  -0x4(%ecx)
  c5:	55                   	push   %ebp
  c6:	89 e5                	mov    %esp,%ebp
  c8:	51                   	push   %ecx
  c9:	83 ec 14             	sub    $0x14,%esp
  cc:	89 c8                	mov    %ecx,%eax
  int N;

  if (argc == 1) {
  ce:	83 38 01             	cmpl   $0x1,(%eax)
  d1:	75 17                	jne    ea <main+0x2f>
    printf(2, "Enter number of processes to create\n");
  d3:	83 ec 08             	sub    $0x8,%esp
  d6:	68 20 09 00 00       	push   $0x920
  db:	6a 02                	push   $0x2
  dd:	e8 48 04 00 00       	call   52a <printf>
  e2:	83 c4 10             	add    $0x10,%esp
    exit();
  e5:	e8 81 02 00 00       	call   36b <exit>
  }

  N = atoi(argv[1]);
  ea:	8b 40 04             	mov    0x4(%eax),%eax
  ed:	83 c0 04             	add    $0x4,%eax
  f0:	8b 00                	mov    (%eax),%eax
  f2:	83 ec 0c             	sub    $0xc,%esp
  f5:	50                   	push   %eax
  f6:	e8 de 01 00 00       	call   2d9 <atoi>
  fb:	83 c4 10             	add    $0x10,%esp
  fe:	89 45 f4             	mov    %eax,-0xc(%ebp)
  forktest(N);
 101:	83 ec 0c             	sub    $0xc,%esp
 104:	ff 75 f4             	pushl  -0xc(%ebp)
 107:	e8 f4 fe ff ff       	call   0 <forktest>
 10c:	83 c4 10             	add    $0x10,%esp
  exit();
 10f:	e8 57 02 00 00       	call   36b <exit>

00000114 <stosb>:
               "cc");
}

static inline void
stosb(void *addr, int data, int cnt)
{
 114:	55                   	push   %ebp
 115:	89 e5                	mov    %esp,%ebp
 117:	57                   	push   %edi
 118:	53                   	push   %ebx
  asm volatile("cld; rep stosb" :
 119:	8b 4d 08             	mov    0x8(%ebp),%ecx
 11c:	8b 55 10             	mov    0x10(%ebp),%edx
 11f:	8b 45 0c             	mov    0xc(%ebp),%eax
 122:	89 cb                	mov    %ecx,%ebx
 124:	89 df                	mov    %ebx,%edi
 126:	89 d1                	mov    %edx,%ecx
 128:	fc                   	cld    
 129:	f3 aa                	rep stos %al,%es:(%edi)
 12b:	89 ca                	mov    %ecx,%edx
 12d:	89 fb                	mov    %edi,%ebx
 12f:	89 5d 08             	mov    %ebx,0x8(%ebp)
 132:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
 135:	90                   	nop
 136:	5b                   	pop    %ebx
 137:	5f                   	pop    %edi
 138:	5d                   	pop    %ebp
 139:	c3                   	ret    

0000013a <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, char *t)
{
 13a:	55                   	push   %ebp
 13b:	89 e5                	mov    %esp,%ebp
 13d:	83 ec 10             	sub    $0x10,%esp
  char *os;

  os = s;
 140:	8b 45 08             	mov    0x8(%ebp),%eax
 143:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while((*s++ = *t++) != 0)
 146:	90                   	nop
 147:	8b 45 08             	mov    0x8(%ebp),%eax
 14a:	8d 50 01             	lea    0x1(%eax),%edx
 14d:	89 55 08             	mov    %edx,0x8(%ebp)
 150:	8b 55 0c             	mov    0xc(%ebp),%edx
 153:	8d 4a 01             	lea    0x1(%edx),%ecx
 156:	89 4d 0c             	mov    %ecx,0xc(%ebp)
 159:	0f b6 12             	movzbl (%edx),%edx
 15c:	88 10                	mov    %dl,(%eax)
 15e:	0f b6 00             	movzbl (%eax),%eax
 161:	84 c0                	test   %al,%al
 163:	75 e2                	jne    147 <strcpy+0xd>
    ;
  return os;
 165:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 168:	c9                   	leave  
 169:	c3                   	ret    

0000016a <strcmp>:

int
strcmp(const char *p, const char *q)
{
 16a:	55                   	push   %ebp
 16b:	89 e5                	mov    %esp,%ebp
  while(*p && *p == *q)
 16d:	eb 08                	jmp    177 <strcmp+0xd>
    p++, q++;
 16f:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 173:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
  while(*p && *p == *q)
 177:	8b 45 08             	mov    0x8(%ebp),%eax
 17a:	0f b6 00             	movzbl (%eax),%eax
 17d:	84 c0                	test   %al,%al
 17f:	74 10                	je     191 <strcmp+0x27>
 181:	8b 45 08             	mov    0x8(%ebp),%eax
 184:	0f b6 10             	movzbl (%eax),%edx
 187:	8b 45 0c             	mov    0xc(%ebp),%eax
 18a:	0f b6 00             	movzbl (%eax),%eax
 18d:	38 c2                	cmp    %al,%dl
 18f:	74 de                	je     16f <strcmp+0x5>
    p++, q++;
  return (uchar)*p - (uchar)*q;
 191:	8b 45 08             	mov    0x8(%ebp),%eax
 194:	0f b6 00             	movzbl (%eax),%eax
 197:	0f b6 d0             	movzbl %al,%edx
 19a:	8b 45 0c             	mov    0xc(%ebp),%eax
 19d:	0f b6 00             	movzbl (%eax),%eax
 1a0:	0f b6 c0             	movzbl %al,%eax
 1a3:	29 c2                	sub    %eax,%edx
 1a5:	89 d0                	mov    %edx,%eax
}
 1a7:	5d                   	pop    %ebp
 1a8:	c3                   	ret    

000001a9 <strlen>:

uint
strlen(char *s)
{
 1a9:	55                   	push   %ebp
 1aa:	89 e5                	mov    %esp,%ebp
 1ac:	83 ec 10             	sub    $0x10,%esp
  int n;

  for(n = 0; s[n]; n++)
 1af:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
 1b6:	eb 04                	jmp    1bc <strlen+0x13>
 1b8:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
 1bc:	8b 55 fc             	mov    -0x4(%ebp),%edx
 1bf:	8b 45 08             	mov    0x8(%ebp),%eax
 1c2:	01 d0                	add    %edx,%eax
 1c4:	0f b6 00             	movzbl (%eax),%eax
 1c7:	84 c0                	test   %al,%al
 1c9:	75 ed                	jne    1b8 <strlen+0xf>
    ;
  return n;
 1cb:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 1ce:	c9                   	leave  
 1cf:	c3                   	ret    

000001d0 <memset>:

void*
memset(void *dst, int c, uint n)
{
 1d0:	55                   	push   %ebp
 1d1:	89 e5                	mov    %esp,%ebp
  stosb(dst, c, n);
 1d3:	8b 45 10             	mov    0x10(%ebp),%eax
 1d6:	50                   	push   %eax
 1d7:	ff 75 0c             	pushl  0xc(%ebp)
 1da:	ff 75 08             	pushl  0x8(%ebp)
 1dd:	e8 32 ff ff ff       	call   114 <stosb>
 1e2:	83 c4 0c             	add    $0xc,%esp
  return dst;
 1e5:	8b 45 08             	mov    0x8(%ebp),%eax
}
 1e8:	c9                   	leave  
 1e9:	c3                   	ret    

000001ea <strchr>:

char*
strchr(const char *s, char c)
{
 1ea:	55                   	push   %ebp
 1eb:	89 e5                	mov    %esp,%ebp
 1ed:	83 ec 04             	sub    $0x4,%esp
 1f0:	8b 45 0c             	mov    0xc(%ebp),%eax
 1f3:	88 45 fc             	mov    %al,-0x4(%ebp)
  for(; *s; s++)
 1f6:	eb 14                	jmp    20c <strchr+0x22>
    if(*s == c)
 1f8:	8b 45 08             	mov    0x8(%ebp),%eax
 1fb:	0f b6 00             	movzbl (%eax),%eax
 1fe:	3a 45 fc             	cmp    -0x4(%ebp),%al
 201:	75 05                	jne    208 <strchr+0x1e>
      return (char*)s;
 203:	8b 45 08             	mov    0x8(%ebp),%eax
 206:	eb 13                	jmp    21b <strchr+0x31>
}

char*
strchr(const char *s, char c)
{
  for(; *s; s++)
 208:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 20c:	8b 45 08             	mov    0x8(%ebp),%eax
 20f:	0f b6 00             	movzbl (%eax),%eax
 212:	84 c0                	test   %al,%al
 214:	75 e2                	jne    1f8 <strchr+0xe>
    if(*s == c)
      return (char*)s;
  return 0;
 216:	b8 00 00 00 00       	mov    $0x0,%eax
}
 21b:	c9                   	leave  
 21c:	c3                   	ret    

0000021d <gets>:

char*
gets(char *buf, int max)
{
 21d:	55                   	push   %ebp
 21e:	89 e5                	mov    %esp,%ebp
 220:	83 ec 18             	sub    $0x18,%esp
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 223:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 22a:	eb 42                	jmp    26e <gets+0x51>
    cc = read(0, &c, 1);
 22c:	83 ec 04             	sub    $0x4,%esp
 22f:	6a 01                	push   $0x1
 231:	8d 45 ef             	lea    -0x11(%ebp),%eax
 234:	50                   	push   %eax
 235:	6a 00                	push   $0x0
 237:	e8 47 01 00 00       	call   383 <read>
 23c:	83 c4 10             	add    $0x10,%esp
 23f:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if(cc < 1)
 242:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 246:	7e 33                	jle    27b <gets+0x5e>
      break;
    buf[i++] = c;
 248:	8b 45 f4             	mov    -0xc(%ebp),%eax
 24b:	8d 50 01             	lea    0x1(%eax),%edx
 24e:	89 55 f4             	mov    %edx,-0xc(%ebp)
 251:	89 c2                	mov    %eax,%edx
 253:	8b 45 08             	mov    0x8(%ebp),%eax
 256:	01 c2                	add    %eax,%edx
 258:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 25c:	88 02                	mov    %al,(%edx)
    if(c == '\n' || c == '\r')
 25e:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 262:	3c 0a                	cmp    $0xa,%al
 264:	74 16                	je     27c <gets+0x5f>
 266:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 26a:	3c 0d                	cmp    $0xd,%al
 26c:	74 0e                	je     27c <gets+0x5f>
gets(char *buf, int max)
{
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 26e:	8b 45 f4             	mov    -0xc(%ebp),%eax
 271:	83 c0 01             	add    $0x1,%eax
 274:	3b 45 0c             	cmp    0xc(%ebp),%eax
 277:	7c b3                	jl     22c <gets+0xf>
 279:	eb 01                	jmp    27c <gets+0x5f>
    cc = read(0, &c, 1);
    if(cc < 1)
      break;
 27b:	90                   	nop
    buf[i++] = c;
    if(c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 27c:	8b 55 f4             	mov    -0xc(%ebp),%edx
 27f:	8b 45 08             	mov    0x8(%ebp),%eax
 282:	01 d0                	add    %edx,%eax
 284:	c6 00 00             	movb   $0x0,(%eax)
  return buf;
 287:	8b 45 08             	mov    0x8(%ebp),%eax
}
 28a:	c9                   	leave  
 28b:	c3                   	ret    

0000028c <stat>:

int
stat(char *n, struct stat *st)
{
 28c:	55                   	push   %ebp
 28d:	89 e5                	mov    %esp,%ebp
 28f:	83 ec 18             	sub    $0x18,%esp
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 292:	83 ec 08             	sub    $0x8,%esp
 295:	6a 00                	push   $0x0
 297:	ff 75 08             	pushl  0x8(%ebp)
 29a:	e8 0c 01 00 00       	call   3ab <open>
 29f:	83 c4 10             	add    $0x10,%esp
 2a2:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(fd < 0)
 2a5:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 2a9:	79 07                	jns    2b2 <stat+0x26>
    return -1;
 2ab:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 2b0:	eb 25                	jmp    2d7 <stat+0x4b>
  r = fstat(fd, st);
 2b2:	83 ec 08             	sub    $0x8,%esp
 2b5:	ff 75 0c             	pushl  0xc(%ebp)
 2b8:	ff 75 f4             	pushl  -0xc(%ebp)
 2bb:	e8 03 01 00 00       	call   3c3 <fstat>
 2c0:	83 c4 10             	add    $0x10,%esp
 2c3:	89 45 f0             	mov    %eax,-0x10(%ebp)
  close(fd);
 2c6:	83 ec 0c             	sub    $0xc,%esp
 2c9:	ff 75 f4             	pushl  -0xc(%ebp)
 2cc:	e8 c2 00 00 00       	call   393 <close>
 2d1:	83 c4 10             	add    $0x10,%esp
  return r;
 2d4:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
 2d7:	c9                   	leave  
 2d8:	c3                   	ret    

000002d9 <atoi>:

int
atoi(const char *s)
{
 2d9:	55                   	push   %ebp
 2da:	89 e5                	mov    %esp,%ebp
 2dc:	83 ec 10             	sub    $0x10,%esp
  int n;

  n = 0;
 2df:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  while('0' <= *s && *s <= '9')
 2e6:	eb 25                	jmp    30d <atoi+0x34>
    n = n*10 + *s++ - '0';
 2e8:	8b 55 fc             	mov    -0x4(%ebp),%edx
 2eb:	89 d0                	mov    %edx,%eax
 2ed:	c1 e0 02             	shl    $0x2,%eax
 2f0:	01 d0                	add    %edx,%eax
 2f2:	01 c0                	add    %eax,%eax
 2f4:	89 c1                	mov    %eax,%ecx
 2f6:	8b 45 08             	mov    0x8(%ebp),%eax
 2f9:	8d 50 01             	lea    0x1(%eax),%edx
 2fc:	89 55 08             	mov    %edx,0x8(%ebp)
 2ff:	0f b6 00             	movzbl (%eax),%eax
 302:	0f be c0             	movsbl %al,%eax
 305:	01 c8                	add    %ecx,%eax
 307:	83 e8 30             	sub    $0x30,%eax
 30a:	89 45 fc             	mov    %eax,-0x4(%ebp)
atoi(const char *s)
{
  int n;

  n = 0;
  while('0' <= *s && *s <= '9')
 30d:	8b 45 08             	mov    0x8(%ebp),%eax
 310:	0f b6 00             	movzbl (%eax),%eax
 313:	3c 2f                	cmp    $0x2f,%al
 315:	7e 0a                	jle    321 <atoi+0x48>
 317:	8b 45 08             	mov    0x8(%ebp),%eax
 31a:	0f b6 00             	movzbl (%eax),%eax
 31d:	3c 39                	cmp    $0x39,%al
 31f:	7e c7                	jle    2e8 <atoi+0xf>
    n = n*10 + *s++ - '0';
  return n;
 321:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 324:	c9                   	leave  
 325:	c3                   	ret    

00000326 <memmove>:

void*
memmove(void *vdst, void *vsrc, int n)
{
 326:	55                   	push   %ebp
 327:	89 e5                	mov    %esp,%ebp
 329:	83 ec 10             	sub    $0x10,%esp
  char *dst, *src;
  
  dst = vdst;
 32c:	8b 45 08             	mov    0x8(%ebp),%eax
 32f:	89 45 fc             	mov    %eax,-0x4(%ebp)
  src = vsrc;
 332:	8b 45 0c             	mov    0xc(%ebp),%eax
 335:	89 45 f8             	mov    %eax,-0x8(%ebp)
  while(n-- > 0)
 338:	eb 17                	jmp    351 <memmove+0x2b>
    *dst++ = *src++;
 33a:	8b 45 fc             	mov    -0x4(%ebp),%eax
 33d:	8d 50 01             	lea    0x1(%eax),%edx
 340:	89 55 fc             	mov    %edx,-0x4(%ebp)
 343:	8b 55 f8             	mov    -0x8(%ebp),%edx
 346:	8d 4a 01             	lea    0x1(%edx),%ecx
 349:	89 4d f8             	mov    %ecx,-0x8(%ebp)
 34c:	0f b6 12             	movzbl (%edx),%edx
 34f:	88 10                	mov    %dl,(%eax)
{
  char *dst, *src;
  
  dst = vdst;
  src = vsrc;
  while(n-- > 0)
 351:	8b 45 10             	mov    0x10(%ebp),%eax
 354:	8d 50 ff             	lea    -0x1(%eax),%edx
 357:	89 55 10             	mov    %edx,0x10(%ebp)
 35a:	85 c0                	test   %eax,%eax
 35c:	7f dc                	jg     33a <memmove+0x14>
    *dst++ = *src++;
  return vdst;
 35e:	8b 45 08             	mov    0x8(%ebp),%eax
}
 361:	c9                   	leave  
 362:	c3                   	ret    

00000363 <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
 363:	b8 01 00 00 00       	mov    $0x1,%eax
 368:	cd 40                	int    $0x40
 36a:	c3                   	ret    

0000036b <exit>:
SYSCALL(exit)
 36b:	b8 02 00 00 00       	mov    $0x2,%eax
 370:	cd 40                	int    $0x40
 372:	c3                   	ret    

00000373 <wait>:
SYSCALL(wait)
 373:	b8 03 00 00 00       	mov    $0x3,%eax
 378:	cd 40                	int    $0x40
 37a:	c3                   	ret    

0000037b <pipe>:
SYSCALL(pipe)
 37b:	b8 04 00 00 00       	mov    $0x4,%eax
 380:	cd 40                	int    $0x40
 382:	c3                   	ret    

00000383 <read>:
SYSCALL(read)
 383:	b8 05 00 00 00       	mov    $0x5,%eax
 388:	cd 40                	int    $0x40
 38a:	c3                   	ret    

0000038b <write>:
SYSCALL(write)
 38b:	b8 10 00 00 00       	mov    $0x10,%eax
 390:	cd 40                	int    $0x40
 392:	c3                   	ret    

00000393 <close>:
SYSCALL(close)
 393:	b8 15 00 00 00       	mov    $0x15,%eax
 398:	cd 40                	int    $0x40
 39a:	c3                   	ret    

0000039b <kill>:
SYSCALL(kill)
 39b:	b8 06 00 00 00       	mov    $0x6,%eax
 3a0:	cd 40                	int    $0x40
 3a2:	c3                   	ret    

000003a3 <exec>:
SYSCALL(exec)
 3a3:	b8 07 00 00 00       	mov    $0x7,%eax
 3a8:	cd 40                	int    $0x40
 3aa:	c3                   	ret    

000003ab <open>:
SYSCALL(open)
 3ab:	b8 0f 00 00 00       	mov    $0xf,%eax
 3b0:	cd 40                	int    $0x40
 3b2:	c3                   	ret    

000003b3 <mknod>:
SYSCALL(mknod)
 3b3:	b8 11 00 00 00       	mov    $0x11,%eax
 3b8:	cd 40                	int    $0x40
 3ba:	c3                   	ret    

000003bb <unlink>:
SYSCALL(unlink)
 3bb:	b8 12 00 00 00       	mov    $0x12,%eax
 3c0:	cd 40                	int    $0x40
 3c2:	c3                   	ret    

000003c3 <fstat>:
SYSCALL(fstat)
 3c3:	b8 08 00 00 00       	mov    $0x8,%eax
 3c8:	cd 40                	int    $0x40
 3ca:	c3                   	ret    

000003cb <link>:
SYSCALL(link)
 3cb:	b8 13 00 00 00       	mov    $0x13,%eax
 3d0:	cd 40                	int    $0x40
 3d2:	c3                   	ret    

000003d3 <mkdir>:
SYSCALL(mkdir)
 3d3:	b8 14 00 00 00       	mov    $0x14,%eax
 3d8:	cd 40                	int    $0x40
 3da:	c3                   	ret    

000003db <chdir>:
SYSCALL(chdir)
 3db:	b8 09 00 00 00       	mov    $0x9,%eax
 3e0:	cd 40                	int    $0x40
 3e2:	c3                   	ret    

000003e3 <dup>:
SYSCALL(dup)
 3e3:	b8 0a 00 00 00       	mov    $0xa,%eax
 3e8:	cd 40                	int    $0x40
 3ea:	c3                   	ret    

000003eb <getpid>:
SYSCALL(getpid)
 3eb:	b8 0b 00 00 00       	mov    $0xb,%eax
 3f0:	cd 40                	int    $0x40
 3f2:	c3                   	ret    

000003f3 <sbrk>:
SYSCALL(sbrk)
 3f3:	b8 0c 00 00 00       	mov    $0xc,%eax
 3f8:	cd 40                	int    $0x40
 3fa:	c3                   	ret    

000003fb <sleep>:
SYSCALL(sleep)
 3fb:	b8 0d 00 00 00       	mov    $0xd,%eax
 400:	cd 40                	int    $0x40
 402:	c3                   	ret    

00000403 <uptime>:
SYSCALL(uptime)
 403:	b8 0e 00 00 00       	mov    $0xe,%eax
 408:	cd 40                	int    $0x40
 40a:	c3                   	ret    

0000040b <halt>:
SYSCALL(halt)
 40b:	b8 16 00 00 00       	mov    $0x16,%eax
 410:	cd 40                	int    $0x40
 412:	c3                   	ret    

00000413 <date>:
SYSCALL(date)
 413:	b8 17 00 00 00       	mov    $0x17,%eax
 418:	cd 40                	int    $0x40
 41a:	c3                   	ret    

0000041b <getuid>:
SYSCALL(getuid)
 41b:	b8 18 00 00 00       	mov    $0x18,%eax
 420:	cd 40                	int    $0x40
 422:	c3                   	ret    

00000423 <getgid>:
SYSCALL(getgid)
 423:	b8 19 00 00 00       	mov    $0x19,%eax
 428:	cd 40                	int    $0x40
 42a:	c3                   	ret    

0000042b <getppid>:
SYSCALL(getppid)
 42b:	b8 1a 00 00 00       	mov    $0x1a,%eax
 430:	cd 40                	int    $0x40
 432:	c3                   	ret    

00000433 <setuid>:
SYSCALL(setuid)
 433:	b8 1b 00 00 00       	mov    $0x1b,%eax
 438:	cd 40                	int    $0x40
 43a:	c3                   	ret    

0000043b <setgid>:
SYSCALL(setgid)
 43b:	b8 1c 00 00 00       	mov    $0x1c,%eax
 440:	cd 40                	int    $0x40
 442:	c3                   	ret    

00000443 <getprocs>:
SYSCALL(getprocs)
 443:	b8 1d 00 00 00       	mov    $0x1d,%eax
 448:	cd 40                	int    $0x40
 44a:	c3                   	ret    

0000044b <setpriority>:
SYSCALL(setpriority)
 44b:	b8 1e 00 00 00       	mov    $0x1e,%eax
 450:	cd 40                	int    $0x40
 452:	c3                   	ret    

00000453 <putc>:
#include "stat.h"
#include "user.h"

static void
putc(int fd, char c)
{
 453:	55                   	push   %ebp
 454:	89 e5                	mov    %esp,%ebp
 456:	83 ec 18             	sub    $0x18,%esp
 459:	8b 45 0c             	mov    0xc(%ebp),%eax
 45c:	88 45 f4             	mov    %al,-0xc(%ebp)
  write(fd, &c, 1);
 45f:	83 ec 04             	sub    $0x4,%esp
 462:	6a 01                	push   $0x1
 464:	8d 45 f4             	lea    -0xc(%ebp),%eax
 467:	50                   	push   %eax
 468:	ff 75 08             	pushl  0x8(%ebp)
 46b:	e8 1b ff ff ff       	call   38b <write>
 470:	83 c4 10             	add    $0x10,%esp
}
 473:	90                   	nop
 474:	c9                   	leave  
 475:	c3                   	ret    

00000476 <printint>:

static void
printint(int fd, int xx, int base, int sgn)
{
 476:	55                   	push   %ebp
 477:	89 e5                	mov    %esp,%ebp
 479:	53                   	push   %ebx
 47a:	83 ec 24             	sub    $0x24,%esp
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 47d:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  if(sgn && xx < 0){
 484:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
 488:	74 17                	je     4a1 <printint+0x2b>
 48a:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
 48e:	79 11                	jns    4a1 <printint+0x2b>
    neg = 1;
 490:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
    x = -xx;
 497:	8b 45 0c             	mov    0xc(%ebp),%eax
 49a:	f7 d8                	neg    %eax
 49c:	89 45 ec             	mov    %eax,-0x14(%ebp)
 49f:	eb 06                	jmp    4a7 <printint+0x31>
  } else {
    x = xx;
 4a1:	8b 45 0c             	mov    0xc(%ebp),%eax
 4a4:	89 45 ec             	mov    %eax,-0x14(%ebp)
  }

  i = 0;
 4a7:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  do{
    buf[i++] = digits[x % base];
 4ae:	8b 4d f4             	mov    -0xc(%ebp),%ecx
 4b1:	8d 41 01             	lea    0x1(%ecx),%eax
 4b4:	89 45 f4             	mov    %eax,-0xc(%ebp)
 4b7:	8b 5d 10             	mov    0x10(%ebp),%ebx
 4ba:	8b 45 ec             	mov    -0x14(%ebp),%eax
 4bd:	ba 00 00 00 00       	mov    $0x0,%edx
 4c2:	f7 f3                	div    %ebx
 4c4:	89 d0                	mov    %edx,%eax
 4c6:	0f b6 80 b4 0b 00 00 	movzbl 0xbb4(%eax),%eax
 4cd:	88 44 0d dc          	mov    %al,-0x24(%ebp,%ecx,1)
  }while((x /= base) != 0);
 4d1:	8b 5d 10             	mov    0x10(%ebp),%ebx
 4d4:	8b 45 ec             	mov    -0x14(%ebp),%eax
 4d7:	ba 00 00 00 00       	mov    $0x0,%edx
 4dc:	f7 f3                	div    %ebx
 4de:	89 45 ec             	mov    %eax,-0x14(%ebp)
 4e1:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 4e5:	75 c7                	jne    4ae <printint+0x38>
  if(neg)
 4e7:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 4eb:	74 2d                	je     51a <printint+0xa4>
    buf[i++] = '-';
 4ed:	8b 45 f4             	mov    -0xc(%ebp),%eax
 4f0:	8d 50 01             	lea    0x1(%eax),%edx
 4f3:	89 55 f4             	mov    %edx,-0xc(%ebp)
 4f6:	c6 44 05 dc 2d       	movb   $0x2d,-0x24(%ebp,%eax,1)

  while(--i >= 0)
 4fb:	eb 1d                	jmp    51a <printint+0xa4>
    putc(fd, buf[i]);
 4fd:	8d 55 dc             	lea    -0x24(%ebp),%edx
 500:	8b 45 f4             	mov    -0xc(%ebp),%eax
 503:	01 d0                	add    %edx,%eax
 505:	0f b6 00             	movzbl (%eax),%eax
 508:	0f be c0             	movsbl %al,%eax
 50b:	83 ec 08             	sub    $0x8,%esp
 50e:	50                   	push   %eax
 50f:	ff 75 08             	pushl  0x8(%ebp)
 512:	e8 3c ff ff ff       	call   453 <putc>
 517:	83 c4 10             	add    $0x10,%esp
    buf[i++] = digits[x % base];
  }while((x /= base) != 0);
  if(neg)
    buf[i++] = '-';

  while(--i >= 0)
 51a:	83 6d f4 01          	subl   $0x1,-0xc(%ebp)
 51e:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 522:	79 d9                	jns    4fd <printint+0x87>
    putc(fd, buf[i]);
}
 524:	90                   	nop
 525:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 528:	c9                   	leave  
 529:	c3                   	ret    

0000052a <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, char *fmt, ...)
{
 52a:	55                   	push   %ebp
 52b:	89 e5                	mov    %esp,%ebp
 52d:	83 ec 28             	sub    $0x28,%esp
  char *s;
  int c, i, state;
  uint *ap;

  state = 0;
 530:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  ap = (uint*)(void*)&fmt + 1;
 537:	8d 45 0c             	lea    0xc(%ebp),%eax
 53a:	83 c0 04             	add    $0x4,%eax
 53d:	89 45 e8             	mov    %eax,-0x18(%ebp)
  for(i = 0; fmt[i]; i++){
 540:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 547:	e9 59 01 00 00       	jmp    6a5 <printf+0x17b>
    c = fmt[i] & 0xff;
 54c:	8b 55 0c             	mov    0xc(%ebp),%edx
 54f:	8b 45 f0             	mov    -0x10(%ebp),%eax
 552:	01 d0                	add    %edx,%eax
 554:	0f b6 00             	movzbl (%eax),%eax
 557:	0f be c0             	movsbl %al,%eax
 55a:	25 ff 00 00 00       	and    $0xff,%eax
 55f:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    if(state == 0){
 562:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 566:	75 2c                	jne    594 <printf+0x6a>
      if(c == '%'){
 568:	83 7d e4 25          	cmpl   $0x25,-0x1c(%ebp)
 56c:	75 0c                	jne    57a <printf+0x50>
        state = '%';
 56e:	c7 45 ec 25 00 00 00 	movl   $0x25,-0x14(%ebp)
 575:	e9 27 01 00 00       	jmp    6a1 <printf+0x177>
      } else {
        putc(fd, c);
 57a:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 57d:	0f be c0             	movsbl %al,%eax
 580:	83 ec 08             	sub    $0x8,%esp
 583:	50                   	push   %eax
 584:	ff 75 08             	pushl  0x8(%ebp)
 587:	e8 c7 fe ff ff       	call   453 <putc>
 58c:	83 c4 10             	add    $0x10,%esp
 58f:	e9 0d 01 00 00       	jmp    6a1 <printf+0x177>
      }
    } else if(state == '%'){
 594:	83 7d ec 25          	cmpl   $0x25,-0x14(%ebp)
 598:	0f 85 03 01 00 00    	jne    6a1 <printf+0x177>
      if(c == 'd'){
 59e:	83 7d e4 64          	cmpl   $0x64,-0x1c(%ebp)
 5a2:	75 1e                	jne    5c2 <printf+0x98>
        printint(fd, *ap, 10, 1);
 5a4:	8b 45 e8             	mov    -0x18(%ebp),%eax
 5a7:	8b 00                	mov    (%eax),%eax
 5a9:	6a 01                	push   $0x1
 5ab:	6a 0a                	push   $0xa
 5ad:	50                   	push   %eax
 5ae:	ff 75 08             	pushl  0x8(%ebp)
 5b1:	e8 c0 fe ff ff       	call   476 <printint>
 5b6:	83 c4 10             	add    $0x10,%esp
        ap++;
 5b9:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 5bd:	e9 d8 00 00 00       	jmp    69a <printf+0x170>
      } else if(c == 'x' || c == 'p'){
 5c2:	83 7d e4 78          	cmpl   $0x78,-0x1c(%ebp)
 5c6:	74 06                	je     5ce <printf+0xa4>
 5c8:	83 7d e4 70          	cmpl   $0x70,-0x1c(%ebp)
 5cc:	75 1e                	jne    5ec <printf+0xc2>
        printint(fd, *ap, 16, 0);
 5ce:	8b 45 e8             	mov    -0x18(%ebp),%eax
 5d1:	8b 00                	mov    (%eax),%eax
 5d3:	6a 00                	push   $0x0
 5d5:	6a 10                	push   $0x10
 5d7:	50                   	push   %eax
 5d8:	ff 75 08             	pushl  0x8(%ebp)
 5db:	e8 96 fe ff ff       	call   476 <printint>
 5e0:	83 c4 10             	add    $0x10,%esp
        ap++;
 5e3:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 5e7:	e9 ae 00 00 00       	jmp    69a <printf+0x170>
      } else if(c == 's'){
 5ec:	83 7d e4 73          	cmpl   $0x73,-0x1c(%ebp)
 5f0:	75 43                	jne    635 <printf+0x10b>
        s = (char*)*ap;
 5f2:	8b 45 e8             	mov    -0x18(%ebp),%eax
 5f5:	8b 00                	mov    (%eax),%eax
 5f7:	89 45 f4             	mov    %eax,-0xc(%ebp)
        ap++;
 5fa:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
        if(s == 0)
 5fe:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 602:	75 25                	jne    629 <printf+0xff>
          s = "(null)";
 604:	c7 45 f4 45 09 00 00 	movl   $0x945,-0xc(%ebp)
        while(*s != 0){
 60b:	eb 1c                	jmp    629 <printf+0xff>
          putc(fd, *s);
 60d:	8b 45 f4             	mov    -0xc(%ebp),%eax
 610:	0f b6 00             	movzbl (%eax),%eax
 613:	0f be c0             	movsbl %al,%eax
 616:	83 ec 08             	sub    $0x8,%esp
 619:	50                   	push   %eax
 61a:	ff 75 08             	pushl  0x8(%ebp)
 61d:	e8 31 fe ff ff       	call   453 <putc>
 622:	83 c4 10             	add    $0x10,%esp
          s++;
 625:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
      } else if(c == 's'){
        s = (char*)*ap;
        ap++;
        if(s == 0)
          s = "(null)";
        while(*s != 0){
 629:	8b 45 f4             	mov    -0xc(%ebp),%eax
 62c:	0f b6 00             	movzbl (%eax),%eax
 62f:	84 c0                	test   %al,%al
 631:	75 da                	jne    60d <printf+0xe3>
 633:	eb 65                	jmp    69a <printf+0x170>
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 635:	83 7d e4 63          	cmpl   $0x63,-0x1c(%ebp)
 639:	75 1d                	jne    658 <printf+0x12e>
        putc(fd, *ap);
 63b:	8b 45 e8             	mov    -0x18(%ebp),%eax
 63e:	8b 00                	mov    (%eax),%eax
 640:	0f be c0             	movsbl %al,%eax
 643:	83 ec 08             	sub    $0x8,%esp
 646:	50                   	push   %eax
 647:	ff 75 08             	pushl  0x8(%ebp)
 64a:	e8 04 fe ff ff       	call   453 <putc>
 64f:	83 c4 10             	add    $0x10,%esp
        ap++;
 652:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 656:	eb 42                	jmp    69a <printf+0x170>
      } else if(c == '%'){
 658:	83 7d e4 25          	cmpl   $0x25,-0x1c(%ebp)
 65c:	75 17                	jne    675 <printf+0x14b>
        putc(fd, c);
 65e:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 661:	0f be c0             	movsbl %al,%eax
 664:	83 ec 08             	sub    $0x8,%esp
 667:	50                   	push   %eax
 668:	ff 75 08             	pushl  0x8(%ebp)
 66b:	e8 e3 fd ff ff       	call   453 <putc>
 670:	83 c4 10             	add    $0x10,%esp
 673:	eb 25                	jmp    69a <printf+0x170>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 675:	83 ec 08             	sub    $0x8,%esp
 678:	6a 25                	push   $0x25
 67a:	ff 75 08             	pushl  0x8(%ebp)
 67d:	e8 d1 fd ff ff       	call   453 <putc>
 682:	83 c4 10             	add    $0x10,%esp
        putc(fd, c);
 685:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 688:	0f be c0             	movsbl %al,%eax
 68b:	83 ec 08             	sub    $0x8,%esp
 68e:	50                   	push   %eax
 68f:	ff 75 08             	pushl  0x8(%ebp)
 692:	e8 bc fd ff ff       	call   453 <putc>
 697:	83 c4 10             	add    $0x10,%esp
      }
      state = 0;
 69a:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
 6a1:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
 6a5:	8b 55 0c             	mov    0xc(%ebp),%edx
 6a8:	8b 45 f0             	mov    -0x10(%ebp),%eax
 6ab:	01 d0                	add    %edx,%eax
 6ad:	0f b6 00             	movzbl (%eax),%eax
 6b0:	84 c0                	test   %al,%al
 6b2:	0f 85 94 fe ff ff    	jne    54c <printf+0x22>
        putc(fd, c);
      }
      state = 0;
    }
  }
}
 6b8:	90                   	nop
 6b9:	c9                   	leave  
 6ba:	c3                   	ret    

000006bb <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 6bb:	55                   	push   %ebp
 6bc:	89 e5                	mov    %esp,%ebp
 6be:	83 ec 10             	sub    $0x10,%esp
  Header *bp, *p;

  bp = (Header*)ap - 1;
 6c1:	8b 45 08             	mov    0x8(%ebp),%eax
 6c4:	83 e8 08             	sub    $0x8,%eax
 6c7:	89 45 f8             	mov    %eax,-0x8(%ebp)
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 6ca:	a1 d0 0b 00 00       	mov    0xbd0,%eax
 6cf:	89 45 fc             	mov    %eax,-0x4(%ebp)
 6d2:	eb 24                	jmp    6f8 <free+0x3d>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 6d4:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6d7:	8b 00                	mov    (%eax),%eax
 6d9:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 6dc:	77 12                	ja     6f0 <free+0x35>
 6de:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6e1:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 6e4:	77 24                	ja     70a <free+0x4f>
 6e6:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6e9:	8b 00                	mov    (%eax),%eax
 6eb:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 6ee:	77 1a                	ja     70a <free+0x4f>
free(void *ap)
{
  Header *bp, *p;

  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 6f0:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6f3:	8b 00                	mov    (%eax),%eax
 6f5:	89 45 fc             	mov    %eax,-0x4(%ebp)
 6f8:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6fb:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 6fe:	76 d4                	jbe    6d4 <free+0x19>
 700:	8b 45 fc             	mov    -0x4(%ebp),%eax
 703:	8b 00                	mov    (%eax),%eax
 705:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 708:	76 ca                	jbe    6d4 <free+0x19>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if(bp + bp->s.size == p->s.ptr){
 70a:	8b 45 f8             	mov    -0x8(%ebp),%eax
 70d:	8b 40 04             	mov    0x4(%eax),%eax
 710:	8d 14 c5 00 00 00 00 	lea    0x0(,%eax,8),%edx
 717:	8b 45 f8             	mov    -0x8(%ebp),%eax
 71a:	01 c2                	add    %eax,%edx
 71c:	8b 45 fc             	mov    -0x4(%ebp),%eax
 71f:	8b 00                	mov    (%eax),%eax
 721:	39 c2                	cmp    %eax,%edx
 723:	75 24                	jne    749 <free+0x8e>
    bp->s.size += p->s.ptr->s.size;
 725:	8b 45 f8             	mov    -0x8(%ebp),%eax
 728:	8b 50 04             	mov    0x4(%eax),%edx
 72b:	8b 45 fc             	mov    -0x4(%ebp),%eax
 72e:	8b 00                	mov    (%eax),%eax
 730:	8b 40 04             	mov    0x4(%eax),%eax
 733:	01 c2                	add    %eax,%edx
 735:	8b 45 f8             	mov    -0x8(%ebp),%eax
 738:	89 50 04             	mov    %edx,0x4(%eax)
    bp->s.ptr = p->s.ptr->s.ptr;
 73b:	8b 45 fc             	mov    -0x4(%ebp),%eax
 73e:	8b 00                	mov    (%eax),%eax
 740:	8b 10                	mov    (%eax),%edx
 742:	8b 45 f8             	mov    -0x8(%ebp),%eax
 745:	89 10                	mov    %edx,(%eax)
 747:	eb 0a                	jmp    753 <free+0x98>
  } else
    bp->s.ptr = p->s.ptr;
 749:	8b 45 fc             	mov    -0x4(%ebp),%eax
 74c:	8b 10                	mov    (%eax),%edx
 74e:	8b 45 f8             	mov    -0x8(%ebp),%eax
 751:	89 10                	mov    %edx,(%eax)
  if(p + p->s.size == bp){
 753:	8b 45 fc             	mov    -0x4(%ebp),%eax
 756:	8b 40 04             	mov    0x4(%eax),%eax
 759:	8d 14 c5 00 00 00 00 	lea    0x0(,%eax,8),%edx
 760:	8b 45 fc             	mov    -0x4(%ebp),%eax
 763:	01 d0                	add    %edx,%eax
 765:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 768:	75 20                	jne    78a <free+0xcf>
    p->s.size += bp->s.size;
 76a:	8b 45 fc             	mov    -0x4(%ebp),%eax
 76d:	8b 50 04             	mov    0x4(%eax),%edx
 770:	8b 45 f8             	mov    -0x8(%ebp),%eax
 773:	8b 40 04             	mov    0x4(%eax),%eax
 776:	01 c2                	add    %eax,%edx
 778:	8b 45 fc             	mov    -0x4(%ebp),%eax
 77b:	89 50 04             	mov    %edx,0x4(%eax)
    p->s.ptr = bp->s.ptr;
 77e:	8b 45 f8             	mov    -0x8(%ebp),%eax
 781:	8b 10                	mov    (%eax),%edx
 783:	8b 45 fc             	mov    -0x4(%ebp),%eax
 786:	89 10                	mov    %edx,(%eax)
 788:	eb 08                	jmp    792 <free+0xd7>
  } else
    p->s.ptr = bp;
 78a:	8b 45 fc             	mov    -0x4(%ebp),%eax
 78d:	8b 55 f8             	mov    -0x8(%ebp),%edx
 790:	89 10                	mov    %edx,(%eax)
  freep = p;
 792:	8b 45 fc             	mov    -0x4(%ebp),%eax
 795:	a3 d0 0b 00 00       	mov    %eax,0xbd0
}
 79a:	90                   	nop
 79b:	c9                   	leave  
 79c:	c3                   	ret    

0000079d <morecore>:

static Header*
morecore(uint nu)
{
 79d:	55                   	push   %ebp
 79e:	89 e5                	mov    %esp,%ebp
 7a0:	83 ec 18             	sub    $0x18,%esp
  char *p;
  Header *hp;

  if(nu < 4096)
 7a3:	81 7d 08 ff 0f 00 00 	cmpl   $0xfff,0x8(%ebp)
 7aa:	77 07                	ja     7b3 <morecore+0x16>
    nu = 4096;
 7ac:	c7 45 08 00 10 00 00 	movl   $0x1000,0x8(%ebp)
  p = sbrk(nu * sizeof(Header));
 7b3:	8b 45 08             	mov    0x8(%ebp),%eax
 7b6:	c1 e0 03             	shl    $0x3,%eax
 7b9:	83 ec 0c             	sub    $0xc,%esp
 7bc:	50                   	push   %eax
 7bd:	e8 31 fc ff ff       	call   3f3 <sbrk>
 7c2:	83 c4 10             	add    $0x10,%esp
 7c5:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(p == (char*)-1)
 7c8:	83 7d f4 ff          	cmpl   $0xffffffff,-0xc(%ebp)
 7cc:	75 07                	jne    7d5 <morecore+0x38>
    return 0;
 7ce:	b8 00 00 00 00       	mov    $0x0,%eax
 7d3:	eb 26                	jmp    7fb <morecore+0x5e>
  hp = (Header*)p;
 7d5:	8b 45 f4             	mov    -0xc(%ebp),%eax
 7d8:	89 45 f0             	mov    %eax,-0x10(%ebp)
  hp->s.size = nu;
 7db:	8b 45 f0             	mov    -0x10(%ebp),%eax
 7de:	8b 55 08             	mov    0x8(%ebp),%edx
 7e1:	89 50 04             	mov    %edx,0x4(%eax)
  free((void*)(hp + 1));
 7e4:	8b 45 f0             	mov    -0x10(%ebp),%eax
 7e7:	83 c0 08             	add    $0x8,%eax
 7ea:	83 ec 0c             	sub    $0xc,%esp
 7ed:	50                   	push   %eax
 7ee:	e8 c8 fe ff ff       	call   6bb <free>
 7f3:	83 c4 10             	add    $0x10,%esp
  return freep;
 7f6:	a1 d0 0b 00 00       	mov    0xbd0,%eax
}
 7fb:	c9                   	leave  
 7fc:	c3                   	ret    

000007fd <malloc>:

void*
malloc(uint nbytes)
{
 7fd:	55                   	push   %ebp
 7fe:	89 e5                	mov    %esp,%ebp
 800:	83 ec 18             	sub    $0x18,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 803:	8b 45 08             	mov    0x8(%ebp),%eax
 806:	83 c0 07             	add    $0x7,%eax
 809:	c1 e8 03             	shr    $0x3,%eax
 80c:	83 c0 01             	add    $0x1,%eax
 80f:	89 45 ec             	mov    %eax,-0x14(%ebp)
  if((prevp = freep) == 0){
 812:	a1 d0 0b 00 00       	mov    0xbd0,%eax
 817:	89 45 f0             	mov    %eax,-0x10(%ebp)
 81a:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 81e:	75 23                	jne    843 <malloc+0x46>
    base.s.ptr = freep = prevp = &base;
 820:	c7 45 f0 c8 0b 00 00 	movl   $0xbc8,-0x10(%ebp)
 827:	8b 45 f0             	mov    -0x10(%ebp),%eax
 82a:	a3 d0 0b 00 00       	mov    %eax,0xbd0
 82f:	a1 d0 0b 00 00       	mov    0xbd0,%eax
 834:	a3 c8 0b 00 00       	mov    %eax,0xbc8
    base.s.size = 0;
 839:	c7 05 cc 0b 00 00 00 	movl   $0x0,0xbcc
 840:	00 00 00 
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 843:	8b 45 f0             	mov    -0x10(%ebp),%eax
 846:	8b 00                	mov    (%eax),%eax
 848:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if(p->s.size >= nunits){
 84b:	8b 45 f4             	mov    -0xc(%ebp),%eax
 84e:	8b 40 04             	mov    0x4(%eax),%eax
 851:	3b 45 ec             	cmp    -0x14(%ebp),%eax
 854:	72 4d                	jb     8a3 <malloc+0xa6>
      if(p->s.size == nunits)
 856:	8b 45 f4             	mov    -0xc(%ebp),%eax
 859:	8b 40 04             	mov    0x4(%eax),%eax
 85c:	3b 45 ec             	cmp    -0x14(%ebp),%eax
 85f:	75 0c                	jne    86d <malloc+0x70>
        prevp->s.ptr = p->s.ptr;
 861:	8b 45 f4             	mov    -0xc(%ebp),%eax
 864:	8b 10                	mov    (%eax),%edx
 866:	8b 45 f0             	mov    -0x10(%ebp),%eax
 869:	89 10                	mov    %edx,(%eax)
 86b:	eb 26                	jmp    893 <malloc+0x96>
      else {
        p->s.size -= nunits;
 86d:	8b 45 f4             	mov    -0xc(%ebp),%eax
 870:	8b 40 04             	mov    0x4(%eax),%eax
 873:	2b 45 ec             	sub    -0x14(%ebp),%eax
 876:	89 c2                	mov    %eax,%edx
 878:	8b 45 f4             	mov    -0xc(%ebp),%eax
 87b:	89 50 04             	mov    %edx,0x4(%eax)
        p += p->s.size;
 87e:	8b 45 f4             	mov    -0xc(%ebp),%eax
 881:	8b 40 04             	mov    0x4(%eax),%eax
 884:	c1 e0 03             	shl    $0x3,%eax
 887:	01 45 f4             	add    %eax,-0xc(%ebp)
        p->s.size = nunits;
 88a:	8b 45 f4             	mov    -0xc(%ebp),%eax
 88d:	8b 55 ec             	mov    -0x14(%ebp),%edx
 890:	89 50 04             	mov    %edx,0x4(%eax)
      }
      freep = prevp;
 893:	8b 45 f0             	mov    -0x10(%ebp),%eax
 896:	a3 d0 0b 00 00       	mov    %eax,0xbd0
      return (void*)(p + 1);
 89b:	8b 45 f4             	mov    -0xc(%ebp),%eax
 89e:	83 c0 08             	add    $0x8,%eax
 8a1:	eb 3b                	jmp    8de <malloc+0xe1>
    }
    if(p == freep)
 8a3:	a1 d0 0b 00 00       	mov    0xbd0,%eax
 8a8:	39 45 f4             	cmp    %eax,-0xc(%ebp)
 8ab:	75 1e                	jne    8cb <malloc+0xce>
      if((p = morecore(nunits)) == 0)
 8ad:	83 ec 0c             	sub    $0xc,%esp
 8b0:	ff 75 ec             	pushl  -0x14(%ebp)
 8b3:	e8 e5 fe ff ff       	call   79d <morecore>
 8b8:	83 c4 10             	add    $0x10,%esp
 8bb:	89 45 f4             	mov    %eax,-0xc(%ebp)
 8be:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 8c2:	75 07                	jne    8cb <malloc+0xce>
        return 0;
 8c4:	b8 00 00 00 00       	mov    $0x0,%eax
 8c9:	eb 13                	jmp    8de <malloc+0xe1>
  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
  if((prevp = freep) == 0){
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 8cb:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8ce:	89 45 f0             	mov    %eax,-0x10(%ebp)
 8d1:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8d4:	8b 00                	mov    (%eax),%eax
 8d6:	89 45 f4             	mov    %eax,-0xc(%ebp)
      return (void*)(p + 1);
    }
    if(p == freep)
      if((p = morecore(nunits)) == 0)
        return 0;
  }
 8d9:	e9 6d ff ff ff       	jmp    84b <malloc+0x4e>
}
 8de:	c9                   	leave  
 8df:	c3                   	ret    
