#include "types.h"
#include "user.h"

int main(int argc, char *argv [])
{
  int t1, t2;  //ticks before and after
  int pid;

  argv++;
  t1 = uptime(); //get the current clock tick

  pid = fork();
  if (pid < 0) //invalid pid
  {
    printf(2, "Error: fork failed. %s at line %d, for command %s\n", __FILE__, __LINE__, argv[0]);
    exit();
  }
  else if (pid == 0) // is child (error)
  {
    exec(argv[0], argv);
    printf(2, "Error: exec failed. %s at line %d, for command %s\n", __FILE__, __LINE__, argv[0]);
    exit();
  }
  else //is parent (good)
  { 
    wait(); // wait for it to exit
    t2 = uptime(); //get the clock at the end of the program
  }

  // output to the time program
  printf(1, "%s ran in %d.%d seconds.\n", argv[0], (t2-t1)/100, (t2-t1)%100);

  exit();
}
