
_testid:     file format elf32-i386


Disassembly of section .text:

00000000 <testvalid>:
#include "types.h"
#include "user.h"

int testvalid(void)
{
   0:	55                   	push   %ebp
   1:	89 e5                	mov    %esp,%ebp
   3:	83 ec 08             	sub    $0x8,%esp
  // set and show new ones
  printf(1, "\nSetting the uid and gid to 3\n");
   6:	83 ec 08             	sub    $0x8,%esp
   9:	68 e0 09 00 00       	push   $0x9e0
   e:	6a 01                	push   $0x1
  10:	e8 13 06 00 00       	call   628 <printf>
  15:	83 c4 10             	add    $0x10,%esp
  if (setuid(3) < 0)
  18:	83 ec 0c             	sub    $0xc,%esp
  1b:	6a 03                	push   $0x3
  1d:	e8 0f 05 00 00       	call   531 <setuid>
  22:	83 c4 10             	add    $0x10,%esp
  25:	85 c0                	test   %eax,%eax
  27:	79 19                	jns    42 <testvalid+0x42>
  {
    printf(2, "SETTING UID TO 3 CAUSED UNEXPECTED ERROR!!!\n");
  29:	83 ec 08             	sub    $0x8,%esp
  2c:	68 00 0a 00 00       	push   $0xa00
  31:	6a 02                	push   $0x2
  33:	e8 f0 05 00 00       	call   628 <printf>
  38:	83 c4 10             	add    $0x10,%esp
    return -1;
  3b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  40:	eb 5f                	jmp    a1 <testvalid+0xa1>
  }
  if (setgid(3) < 0)
  42:	83 ec 0c             	sub    $0xc,%esp
  45:	6a 03                	push   $0x3
  47:	e8 ed 04 00 00       	call   539 <setgid>
  4c:	83 c4 10             	add    $0x10,%esp
  4f:	85 c0                	test   %eax,%eax
  51:	79 19                	jns    6c <testvalid+0x6c>
  {
    printf(2, "SETTING GID TO 3 CAUSED UNEXPECTED ERROR!!!\n");
  53:	83 ec 08             	sub    $0x8,%esp
  56:	68 30 0a 00 00       	push   $0xa30
  5b:	6a 02                	push   $0x2
  5d:	e8 c6 05 00 00       	call   628 <printf>
  62:	83 c4 10             	add    $0x10,%esp
    return -1;
  65:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  6a:	eb 35                	jmp    a1 <testvalid+0xa1>
  }
  printf(1, "The new uid is: %d\n", getuid());
  6c:	e8 a8 04 00 00       	call   519 <getuid>
  71:	83 ec 04             	sub    $0x4,%esp
  74:	50                   	push   %eax
  75:	68 5d 0a 00 00       	push   $0xa5d
  7a:	6a 01                	push   $0x1
  7c:	e8 a7 05 00 00       	call   628 <printf>
  81:	83 c4 10             	add    $0x10,%esp
  printf(1, "The new gid is: %d\n", getgid());
  84:	e8 98 04 00 00       	call   521 <getgid>
  89:	83 ec 04             	sub    $0x4,%esp
  8c:	50                   	push   %eax
  8d:	68 71 0a 00 00       	push   $0xa71
  92:	6a 01                	push   $0x1
  94:	e8 8f 05 00 00       	call   628 <printf>
  99:	83 c4 10             	add    $0x10,%esp
  return 0;
  9c:	b8 00 00 00 00       	mov    $0x0,%eax
}
  a1:	c9                   	leave  
  a2:	c3                   	ret    

000000a3 <testinvalid>:

int testinvalid(void)
{
  a3:	55                   	push   %ebp
  a4:	89 e5                	mov    %esp,%ebp
  a6:	83 ec 08             	sub    $0x8,%esp
  // set and show illegal values
  printf(1, "\nSetting the uid and gid to 32768\n");
  a9:	83 ec 08             	sub    $0x8,%esp
  ac:	68 88 0a 00 00       	push   $0xa88
  b1:	6a 01                	push   $0x1
  b3:	e8 70 05 00 00       	call   628 <printf>
  b8:	83 c4 10             	add    $0x10,%esp
  if (setuid(32768) > 0)
  bb:	83 ec 0c             	sub    $0xc,%esp
  be:	68 00 80 00 00       	push   $0x8000
  c3:	e8 69 04 00 00       	call   531 <setuid>
  c8:	83 c4 10             	add    $0x10,%esp
  cb:	85 c0                	test   %eax,%eax
  cd:	7e 19                	jle    e8 <testinvalid+0x45>
  {
    printf(2, "SETTUNG UID TO 32768 DID NOT CAUSE ERROR!!!\n");
  cf:	83 ec 08             	sub    $0x8,%esp
  d2:	68 ac 0a 00 00       	push   $0xaac
  d7:	6a 02                	push   $0x2
  d9:	e8 4a 05 00 00       	call   628 <printf>
  de:	83 c4 10             	add    $0x10,%esp
    return -1;
  e1:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  e6:	eb 62                	jmp    14a <testinvalid+0xa7>
  }
  if (setuid(32768) > 0)
  e8:	83 ec 0c             	sub    $0xc,%esp
  eb:	68 00 80 00 00       	push   $0x8000
  f0:	e8 3c 04 00 00       	call   531 <setuid>
  f5:	83 c4 10             	add    $0x10,%esp
  f8:	85 c0                	test   %eax,%eax
  fa:	7e 19                	jle    115 <testinvalid+0x72>
  {
    printf(2, "SETTING GID TO 32768 DID NOT CAUSE ERROR\n");
  fc:	83 ec 08             	sub    $0x8,%esp
  ff:	68 dc 0a 00 00       	push   $0xadc
 104:	6a 02                	push   $0x2
 106:	e8 1d 05 00 00       	call   628 <printf>
 10b:	83 c4 10             	add    $0x10,%esp
    return -1;
 10e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 113:	eb 35                	jmp    14a <testinvalid+0xa7>
  }
  printf(1, "The uid is still: %d\n", getuid());
 115:	e8 ff 03 00 00       	call   519 <getuid>
 11a:	83 ec 04             	sub    $0x4,%esp
 11d:	50                   	push   %eax
 11e:	68 06 0b 00 00       	push   $0xb06
 123:	6a 01                	push   $0x1
 125:	e8 fe 04 00 00       	call   628 <printf>
 12a:	83 c4 10             	add    $0x10,%esp
  printf(1, "The gid is still: %d\n", getgid());
 12d:	e8 ef 03 00 00       	call   521 <getgid>
 132:	83 ec 04             	sub    $0x4,%esp
 135:	50                   	push   %eax
 136:	68 1c 0b 00 00       	push   $0xb1c
 13b:	6a 01                	push   $0x1
 13d:	e8 e6 04 00 00       	call   628 <printf>
 142:	83 c4 10             	add    $0x10,%esp
  return 0;
 145:	b8 00 00 00 00       	mov    $0x0,%eax
}
 14a:	c9                   	leave  
 14b:	c3                   	ret    

0000014c <testparent>:

int testparent(void)
{
 14c:	55                   	push   %ebp
 14d:	89 e5                	mov    %esp,%ebp
 14f:	83 ec 08             	sub    $0x8,%esp
  printf(1, "\nGetting the ppid\nThe ppid is: %d\n", getppid());
 152:	e8 d2 03 00 00       	call   529 <getppid>
 157:	83 ec 04             	sub    $0x4,%esp
 15a:	50                   	push   %eax
 15b:	68 34 0b 00 00       	push   $0xb34
 160:	6a 01                	push   $0x1
 162:	e8 c1 04 00 00       	call   628 <printf>
 167:	83 c4 10             	add    $0x10,%esp
  return 0;
 16a:	b8 00 00 00 00       	mov    $0x0,%eax
}
 16f:	c9                   	leave  
 170:	c3                   	ret    

00000171 <test>:

int test(void)
{
 171:	55                   	push   %ebp
 172:	89 e5                	mov    %esp,%ebp
 174:	83 ec 08             	sub    $0x8,%esp
  // get current ids
  printf(1, "The current uid is: %d\n", getuid()); 
 177:	e8 9d 03 00 00       	call   519 <getuid>
 17c:	83 ec 04             	sub    $0x4,%esp
 17f:	50                   	push   %eax
 180:	68 57 0b 00 00       	push   $0xb57
 185:	6a 01                	push   $0x1
 187:	e8 9c 04 00 00       	call   628 <printf>
 18c:	83 c4 10             	add    $0x10,%esp
  printf(1, "The current gid is: %d\n", getgid());
 18f:	e8 8d 03 00 00       	call   521 <getgid>
 194:	83 ec 04             	sub    $0x4,%esp
 197:	50                   	push   %eax
 198:	68 6f 0b 00 00       	push   $0xb6f
 19d:	6a 01                	push   $0x1
 19f:	e8 84 04 00 00       	call   628 <printf>
 1a4:	83 c4 10             	add    $0x10,%esp

  if (testvalid() < 0)
 1a7:	e8 54 fe ff ff       	call   0 <testvalid>
 1ac:	85 c0                	test   %eax,%eax
 1ae:	79 19                	jns    1c9 <test+0x58>
  {
    printf(2, "TEST FAILED.");
 1b0:	83 ec 08             	sub    $0x8,%esp
 1b3:	68 87 0b 00 00       	push   $0xb87
 1b8:	6a 02                	push   $0x2
 1ba:	e8 69 04 00 00       	call   628 <printf>
 1bf:	83 c4 10             	add    $0x10,%esp
    return -1;
 1c2:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 1c7:	eb 2c                	jmp    1f5 <test+0x84>
  }
  if (testinvalid() < 0)
 1c9:	e8 d5 fe ff ff       	call   a3 <testinvalid>
 1ce:	85 c0                	test   %eax,%eax
 1d0:	79 19                	jns    1eb <test+0x7a>
  {
    printf(2, "TEST FAILED.");
 1d2:	83 ec 08             	sub    $0x8,%esp
 1d5:	68 87 0b 00 00       	push   $0xb87
 1da:	6a 02                	push   $0x2
 1dc:	e8 47 04 00 00       	call   628 <printf>
 1e1:	83 c4 10             	add    $0x10,%esp
    return -1;
 1e4:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 1e9:	eb 0a                	jmp    1f5 <test+0x84>
  }
  testparent();
 1eb:	e8 5c ff ff ff       	call   14c <testparent>
  return 0;
 1f0:	b8 00 00 00 00       	mov    $0x0,%eax
}
 1f5:	c9                   	leave  
 1f6:	c3                   	ret    

000001f7 <main>:

int main()
{
 1f7:	8d 4c 24 04          	lea    0x4(%esp),%ecx
 1fb:	83 e4 f0             	and    $0xfffffff0,%esp
 1fe:	ff 71 fc             	pushl  -0x4(%ecx)
 201:	55                   	push   %ebp
 202:	89 e5                	mov    %esp,%ebp
 204:	51                   	push   %ecx
 205:	83 ec 04             	sub    $0x4,%esp
  test();
 208:	e8 64 ff ff ff       	call   171 <test>
  exit();
 20d:	e8 57 02 00 00       	call   469 <exit>

00000212 <stosb>:
               "cc");
}

static inline void
stosb(void *addr, int data, int cnt)
{
 212:	55                   	push   %ebp
 213:	89 e5                	mov    %esp,%ebp
 215:	57                   	push   %edi
 216:	53                   	push   %ebx
  asm volatile("cld; rep stosb" :
 217:	8b 4d 08             	mov    0x8(%ebp),%ecx
 21a:	8b 55 10             	mov    0x10(%ebp),%edx
 21d:	8b 45 0c             	mov    0xc(%ebp),%eax
 220:	89 cb                	mov    %ecx,%ebx
 222:	89 df                	mov    %ebx,%edi
 224:	89 d1                	mov    %edx,%ecx
 226:	fc                   	cld    
 227:	f3 aa                	rep stos %al,%es:(%edi)
 229:	89 ca                	mov    %ecx,%edx
 22b:	89 fb                	mov    %edi,%ebx
 22d:	89 5d 08             	mov    %ebx,0x8(%ebp)
 230:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
 233:	90                   	nop
 234:	5b                   	pop    %ebx
 235:	5f                   	pop    %edi
 236:	5d                   	pop    %ebp
 237:	c3                   	ret    

00000238 <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, char *t)
{
 238:	55                   	push   %ebp
 239:	89 e5                	mov    %esp,%ebp
 23b:	83 ec 10             	sub    $0x10,%esp
  char *os;

  os = s;
 23e:	8b 45 08             	mov    0x8(%ebp),%eax
 241:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while((*s++ = *t++) != 0)
 244:	90                   	nop
 245:	8b 45 08             	mov    0x8(%ebp),%eax
 248:	8d 50 01             	lea    0x1(%eax),%edx
 24b:	89 55 08             	mov    %edx,0x8(%ebp)
 24e:	8b 55 0c             	mov    0xc(%ebp),%edx
 251:	8d 4a 01             	lea    0x1(%edx),%ecx
 254:	89 4d 0c             	mov    %ecx,0xc(%ebp)
 257:	0f b6 12             	movzbl (%edx),%edx
 25a:	88 10                	mov    %dl,(%eax)
 25c:	0f b6 00             	movzbl (%eax),%eax
 25f:	84 c0                	test   %al,%al
 261:	75 e2                	jne    245 <strcpy+0xd>
    ;
  return os;
 263:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 266:	c9                   	leave  
 267:	c3                   	ret    

00000268 <strcmp>:

int
strcmp(const char *p, const char *q)
{
 268:	55                   	push   %ebp
 269:	89 e5                	mov    %esp,%ebp
  while(*p && *p == *q)
 26b:	eb 08                	jmp    275 <strcmp+0xd>
    p++, q++;
 26d:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 271:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
  while(*p && *p == *q)
 275:	8b 45 08             	mov    0x8(%ebp),%eax
 278:	0f b6 00             	movzbl (%eax),%eax
 27b:	84 c0                	test   %al,%al
 27d:	74 10                	je     28f <strcmp+0x27>
 27f:	8b 45 08             	mov    0x8(%ebp),%eax
 282:	0f b6 10             	movzbl (%eax),%edx
 285:	8b 45 0c             	mov    0xc(%ebp),%eax
 288:	0f b6 00             	movzbl (%eax),%eax
 28b:	38 c2                	cmp    %al,%dl
 28d:	74 de                	je     26d <strcmp+0x5>
    p++, q++;
  return (uchar)*p - (uchar)*q;
 28f:	8b 45 08             	mov    0x8(%ebp),%eax
 292:	0f b6 00             	movzbl (%eax),%eax
 295:	0f b6 d0             	movzbl %al,%edx
 298:	8b 45 0c             	mov    0xc(%ebp),%eax
 29b:	0f b6 00             	movzbl (%eax),%eax
 29e:	0f b6 c0             	movzbl %al,%eax
 2a1:	29 c2                	sub    %eax,%edx
 2a3:	89 d0                	mov    %edx,%eax
}
 2a5:	5d                   	pop    %ebp
 2a6:	c3                   	ret    

000002a7 <strlen>:

uint
strlen(char *s)
{
 2a7:	55                   	push   %ebp
 2a8:	89 e5                	mov    %esp,%ebp
 2aa:	83 ec 10             	sub    $0x10,%esp
  int n;

  for(n = 0; s[n]; n++)
 2ad:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
 2b4:	eb 04                	jmp    2ba <strlen+0x13>
 2b6:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
 2ba:	8b 55 fc             	mov    -0x4(%ebp),%edx
 2bd:	8b 45 08             	mov    0x8(%ebp),%eax
 2c0:	01 d0                	add    %edx,%eax
 2c2:	0f b6 00             	movzbl (%eax),%eax
 2c5:	84 c0                	test   %al,%al
 2c7:	75 ed                	jne    2b6 <strlen+0xf>
    ;
  return n;
 2c9:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 2cc:	c9                   	leave  
 2cd:	c3                   	ret    

000002ce <memset>:

void*
memset(void *dst, int c, uint n)
{
 2ce:	55                   	push   %ebp
 2cf:	89 e5                	mov    %esp,%ebp
  stosb(dst, c, n);
 2d1:	8b 45 10             	mov    0x10(%ebp),%eax
 2d4:	50                   	push   %eax
 2d5:	ff 75 0c             	pushl  0xc(%ebp)
 2d8:	ff 75 08             	pushl  0x8(%ebp)
 2db:	e8 32 ff ff ff       	call   212 <stosb>
 2e0:	83 c4 0c             	add    $0xc,%esp
  return dst;
 2e3:	8b 45 08             	mov    0x8(%ebp),%eax
}
 2e6:	c9                   	leave  
 2e7:	c3                   	ret    

000002e8 <strchr>:

char*
strchr(const char *s, char c)
{
 2e8:	55                   	push   %ebp
 2e9:	89 e5                	mov    %esp,%ebp
 2eb:	83 ec 04             	sub    $0x4,%esp
 2ee:	8b 45 0c             	mov    0xc(%ebp),%eax
 2f1:	88 45 fc             	mov    %al,-0x4(%ebp)
  for(; *s; s++)
 2f4:	eb 14                	jmp    30a <strchr+0x22>
    if(*s == c)
 2f6:	8b 45 08             	mov    0x8(%ebp),%eax
 2f9:	0f b6 00             	movzbl (%eax),%eax
 2fc:	3a 45 fc             	cmp    -0x4(%ebp),%al
 2ff:	75 05                	jne    306 <strchr+0x1e>
      return (char*)s;
 301:	8b 45 08             	mov    0x8(%ebp),%eax
 304:	eb 13                	jmp    319 <strchr+0x31>
}

char*
strchr(const char *s, char c)
{
  for(; *s; s++)
 306:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 30a:	8b 45 08             	mov    0x8(%ebp),%eax
 30d:	0f b6 00             	movzbl (%eax),%eax
 310:	84 c0                	test   %al,%al
 312:	75 e2                	jne    2f6 <strchr+0xe>
    if(*s == c)
      return (char*)s;
  return 0;
 314:	b8 00 00 00 00       	mov    $0x0,%eax
}
 319:	c9                   	leave  
 31a:	c3                   	ret    

0000031b <gets>:

char*
gets(char *buf, int max)
{
 31b:	55                   	push   %ebp
 31c:	89 e5                	mov    %esp,%ebp
 31e:	83 ec 18             	sub    $0x18,%esp
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 321:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 328:	eb 42                	jmp    36c <gets+0x51>
    cc = read(0, &c, 1);
 32a:	83 ec 04             	sub    $0x4,%esp
 32d:	6a 01                	push   $0x1
 32f:	8d 45 ef             	lea    -0x11(%ebp),%eax
 332:	50                   	push   %eax
 333:	6a 00                	push   $0x0
 335:	e8 47 01 00 00       	call   481 <read>
 33a:	83 c4 10             	add    $0x10,%esp
 33d:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if(cc < 1)
 340:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 344:	7e 33                	jle    379 <gets+0x5e>
      break;
    buf[i++] = c;
 346:	8b 45 f4             	mov    -0xc(%ebp),%eax
 349:	8d 50 01             	lea    0x1(%eax),%edx
 34c:	89 55 f4             	mov    %edx,-0xc(%ebp)
 34f:	89 c2                	mov    %eax,%edx
 351:	8b 45 08             	mov    0x8(%ebp),%eax
 354:	01 c2                	add    %eax,%edx
 356:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 35a:	88 02                	mov    %al,(%edx)
    if(c == '\n' || c == '\r')
 35c:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 360:	3c 0a                	cmp    $0xa,%al
 362:	74 16                	je     37a <gets+0x5f>
 364:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 368:	3c 0d                	cmp    $0xd,%al
 36a:	74 0e                	je     37a <gets+0x5f>
gets(char *buf, int max)
{
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 36c:	8b 45 f4             	mov    -0xc(%ebp),%eax
 36f:	83 c0 01             	add    $0x1,%eax
 372:	3b 45 0c             	cmp    0xc(%ebp),%eax
 375:	7c b3                	jl     32a <gets+0xf>
 377:	eb 01                	jmp    37a <gets+0x5f>
    cc = read(0, &c, 1);
    if(cc < 1)
      break;
 379:	90                   	nop
    buf[i++] = c;
    if(c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 37a:	8b 55 f4             	mov    -0xc(%ebp),%edx
 37d:	8b 45 08             	mov    0x8(%ebp),%eax
 380:	01 d0                	add    %edx,%eax
 382:	c6 00 00             	movb   $0x0,(%eax)
  return buf;
 385:	8b 45 08             	mov    0x8(%ebp),%eax
}
 388:	c9                   	leave  
 389:	c3                   	ret    

0000038a <stat>:

int
stat(char *n, struct stat *st)
{
 38a:	55                   	push   %ebp
 38b:	89 e5                	mov    %esp,%ebp
 38d:	83 ec 18             	sub    $0x18,%esp
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 390:	83 ec 08             	sub    $0x8,%esp
 393:	6a 00                	push   $0x0
 395:	ff 75 08             	pushl  0x8(%ebp)
 398:	e8 0c 01 00 00       	call   4a9 <open>
 39d:	83 c4 10             	add    $0x10,%esp
 3a0:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(fd < 0)
 3a3:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 3a7:	79 07                	jns    3b0 <stat+0x26>
    return -1;
 3a9:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 3ae:	eb 25                	jmp    3d5 <stat+0x4b>
  r = fstat(fd, st);
 3b0:	83 ec 08             	sub    $0x8,%esp
 3b3:	ff 75 0c             	pushl  0xc(%ebp)
 3b6:	ff 75 f4             	pushl  -0xc(%ebp)
 3b9:	e8 03 01 00 00       	call   4c1 <fstat>
 3be:	83 c4 10             	add    $0x10,%esp
 3c1:	89 45 f0             	mov    %eax,-0x10(%ebp)
  close(fd);
 3c4:	83 ec 0c             	sub    $0xc,%esp
 3c7:	ff 75 f4             	pushl  -0xc(%ebp)
 3ca:	e8 c2 00 00 00       	call   491 <close>
 3cf:	83 c4 10             	add    $0x10,%esp
  return r;
 3d2:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
 3d5:	c9                   	leave  
 3d6:	c3                   	ret    

000003d7 <atoi>:

int
atoi(const char *s)
{
 3d7:	55                   	push   %ebp
 3d8:	89 e5                	mov    %esp,%ebp
 3da:	83 ec 10             	sub    $0x10,%esp
  int n;

  n = 0;
 3dd:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  while('0' <= *s && *s <= '9')
 3e4:	eb 25                	jmp    40b <atoi+0x34>
    n = n*10 + *s++ - '0';
 3e6:	8b 55 fc             	mov    -0x4(%ebp),%edx
 3e9:	89 d0                	mov    %edx,%eax
 3eb:	c1 e0 02             	shl    $0x2,%eax
 3ee:	01 d0                	add    %edx,%eax
 3f0:	01 c0                	add    %eax,%eax
 3f2:	89 c1                	mov    %eax,%ecx
 3f4:	8b 45 08             	mov    0x8(%ebp),%eax
 3f7:	8d 50 01             	lea    0x1(%eax),%edx
 3fa:	89 55 08             	mov    %edx,0x8(%ebp)
 3fd:	0f b6 00             	movzbl (%eax),%eax
 400:	0f be c0             	movsbl %al,%eax
 403:	01 c8                	add    %ecx,%eax
 405:	83 e8 30             	sub    $0x30,%eax
 408:	89 45 fc             	mov    %eax,-0x4(%ebp)
atoi(const char *s)
{
  int n;

  n = 0;
  while('0' <= *s && *s <= '9')
 40b:	8b 45 08             	mov    0x8(%ebp),%eax
 40e:	0f b6 00             	movzbl (%eax),%eax
 411:	3c 2f                	cmp    $0x2f,%al
 413:	7e 0a                	jle    41f <atoi+0x48>
 415:	8b 45 08             	mov    0x8(%ebp),%eax
 418:	0f b6 00             	movzbl (%eax),%eax
 41b:	3c 39                	cmp    $0x39,%al
 41d:	7e c7                	jle    3e6 <atoi+0xf>
    n = n*10 + *s++ - '0';
  return n;
 41f:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 422:	c9                   	leave  
 423:	c3                   	ret    

00000424 <memmove>:

void*
memmove(void *vdst, void *vsrc, int n)
{
 424:	55                   	push   %ebp
 425:	89 e5                	mov    %esp,%ebp
 427:	83 ec 10             	sub    $0x10,%esp
  char *dst, *src;
  
  dst = vdst;
 42a:	8b 45 08             	mov    0x8(%ebp),%eax
 42d:	89 45 fc             	mov    %eax,-0x4(%ebp)
  src = vsrc;
 430:	8b 45 0c             	mov    0xc(%ebp),%eax
 433:	89 45 f8             	mov    %eax,-0x8(%ebp)
  while(n-- > 0)
 436:	eb 17                	jmp    44f <memmove+0x2b>
    *dst++ = *src++;
 438:	8b 45 fc             	mov    -0x4(%ebp),%eax
 43b:	8d 50 01             	lea    0x1(%eax),%edx
 43e:	89 55 fc             	mov    %edx,-0x4(%ebp)
 441:	8b 55 f8             	mov    -0x8(%ebp),%edx
 444:	8d 4a 01             	lea    0x1(%edx),%ecx
 447:	89 4d f8             	mov    %ecx,-0x8(%ebp)
 44a:	0f b6 12             	movzbl (%edx),%edx
 44d:	88 10                	mov    %dl,(%eax)
{
  char *dst, *src;
  
  dst = vdst;
  src = vsrc;
  while(n-- > 0)
 44f:	8b 45 10             	mov    0x10(%ebp),%eax
 452:	8d 50 ff             	lea    -0x1(%eax),%edx
 455:	89 55 10             	mov    %edx,0x10(%ebp)
 458:	85 c0                	test   %eax,%eax
 45a:	7f dc                	jg     438 <memmove+0x14>
    *dst++ = *src++;
  return vdst;
 45c:	8b 45 08             	mov    0x8(%ebp),%eax
}
 45f:	c9                   	leave  
 460:	c3                   	ret    

00000461 <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
 461:	b8 01 00 00 00       	mov    $0x1,%eax
 466:	cd 40                	int    $0x40
 468:	c3                   	ret    

00000469 <exit>:
SYSCALL(exit)
 469:	b8 02 00 00 00       	mov    $0x2,%eax
 46e:	cd 40                	int    $0x40
 470:	c3                   	ret    

00000471 <wait>:
SYSCALL(wait)
 471:	b8 03 00 00 00       	mov    $0x3,%eax
 476:	cd 40                	int    $0x40
 478:	c3                   	ret    

00000479 <pipe>:
SYSCALL(pipe)
 479:	b8 04 00 00 00       	mov    $0x4,%eax
 47e:	cd 40                	int    $0x40
 480:	c3                   	ret    

00000481 <read>:
SYSCALL(read)
 481:	b8 05 00 00 00       	mov    $0x5,%eax
 486:	cd 40                	int    $0x40
 488:	c3                   	ret    

00000489 <write>:
SYSCALL(write)
 489:	b8 10 00 00 00       	mov    $0x10,%eax
 48e:	cd 40                	int    $0x40
 490:	c3                   	ret    

00000491 <close>:
SYSCALL(close)
 491:	b8 15 00 00 00       	mov    $0x15,%eax
 496:	cd 40                	int    $0x40
 498:	c3                   	ret    

00000499 <kill>:
SYSCALL(kill)
 499:	b8 06 00 00 00       	mov    $0x6,%eax
 49e:	cd 40                	int    $0x40
 4a0:	c3                   	ret    

000004a1 <exec>:
SYSCALL(exec)
 4a1:	b8 07 00 00 00       	mov    $0x7,%eax
 4a6:	cd 40                	int    $0x40
 4a8:	c3                   	ret    

000004a9 <open>:
SYSCALL(open)
 4a9:	b8 0f 00 00 00       	mov    $0xf,%eax
 4ae:	cd 40                	int    $0x40
 4b0:	c3                   	ret    

000004b1 <mknod>:
SYSCALL(mknod)
 4b1:	b8 11 00 00 00       	mov    $0x11,%eax
 4b6:	cd 40                	int    $0x40
 4b8:	c3                   	ret    

000004b9 <unlink>:
SYSCALL(unlink)
 4b9:	b8 12 00 00 00       	mov    $0x12,%eax
 4be:	cd 40                	int    $0x40
 4c0:	c3                   	ret    

000004c1 <fstat>:
SYSCALL(fstat)
 4c1:	b8 08 00 00 00       	mov    $0x8,%eax
 4c6:	cd 40                	int    $0x40
 4c8:	c3                   	ret    

000004c9 <link>:
SYSCALL(link)
 4c9:	b8 13 00 00 00       	mov    $0x13,%eax
 4ce:	cd 40                	int    $0x40
 4d0:	c3                   	ret    

000004d1 <mkdir>:
SYSCALL(mkdir)
 4d1:	b8 14 00 00 00       	mov    $0x14,%eax
 4d6:	cd 40                	int    $0x40
 4d8:	c3                   	ret    

000004d9 <chdir>:
SYSCALL(chdir)
 4d9:	b8 09 00 00 00       	mov    $0x9,%eax
 4de:	cd 40                	int    $0x40
 4e0:	c3                   	ret    

000004e1 <dup>:
SYSCALL(dup)
 4e1:	b8 0a 00 00 00       	mov    $0xa,%eax
 4e6:	cd 40                	int    $0x40
 4e8:	c3                   	ret    

000004e9 <getpid>:
SYSCALL(getpid)
 4e9:	b8 0b 00 00 00       	mov    $0xb,%eax
 4ee:	cd 40                	int    $0x40
 4f0:	c3                   	ret    

000004f1 <sbrk>:
SYSCALL(sbrk)
 4f1:	b8 0c 00 00 00       	mov    $0xc,%eax
 4f6:	cd 40                	int    $0x40
 4f8:	c3                   	ret    

000004f9 <sleep>:
SYSCALL(sleep)
 4f9:	b8 0d 00 00 00       	mov    $0xd,%eax
 4fe:	cd 40                	int    $0x40
 500:	c3                   	ret    

00000501 <uptime>:
SYSCALL(uptime)
 501:	b8 0e 00 00 00       	mov    $0xe,%eax
 506:	cd 40                	int    $0x40
 508:	c3                   	ret    

00000509 <halt>:
SYSCALL(halt)
 509:	b8 16 00 00 00       	mov    $0x16,%eax
 50e:	cd 40                	int    $0x40
 510:	c3                   	ret    

00000511 <date>:
SYSCALL(date)
 511:	b8 17 00 00 00       	mov    $0x17,%eax
 516:	cd 40                	int    $0x40
 518:	c3                   	ret    

00000519 <getuid>:
SYSCALL(getuid)
 519:	b8 18 00 00 00       	mov    $0x18,%eax
 51e:	cd 40                	int    $0x40
 520:	c3                   	ret    

00000521 <getgid>:
SYSCALL(getgid)
 521:	b8 19 00 00 00       	mov    $0x19,%eax
 526:	cd 40                	int    $0x40
 528:	c3                   	ret    

00000529 <getppid>:
SYSCALL(getppid)
 529:	b8 1a 00 00 00       	mov    $0x1a,%eax
 52e:	cd 40                	int    $0x40
 530:	c3                   	ret    

00000531 <setuid>:
SYSCALL(setuid)
 531:	b8 1b 00 00 00       	mov    $0x1b,%eax
 536:	cd 40                	int    $0x40
 538:	c3                   	ret    

00000539 <setgid>:
SYSCALL(setgid)
 539:	b8 1c 00 00 00       	mov    $0x1c,%eax
 53e:	cd 40                	int    $0x40
 540:	c3                   	ret    

00000541 <getprocs>:
SYSCALL(getprocs)
 541:	b8 1d 00 00 00       	mov    $0x1d,%eax
 546:	cd 40                	int    $0x40
 548:	c3                   	ret    

00000549 <setpriority>:
SYSCALL(setpriority)
 549:	b8 1e 00 00 00       	mov    $0x1e,%eax
 54e:	cd 40                	int    $0x40
 550:	c3                   	ret    

00000551 <putc>:
#include "stat.h"
#include "user.h"

static void
putc(int fd, char c)
{
 551:	55                   	push   %ebp
 552:	89 e5                	mov    %esp,%ebp
 554:	83 ec 18             	sub    $0x18,%esp
 557:	8b 45 0c             	mov    0xc(%ebp),%eax
 55a:	88 45 f4             	mov    %al,-0xc(%ebp)
  write(fd, &c, 1);
 55d:	83 ec 04             	sub    $0x4,%esp
 560:	6a 01                	push   $0x1
 562:	8d 45 f4             	lea    -0xc(%ebp),%eax
 565:	50                   	push   %eax
 566:	ff 75 08             	pushl  0x8(%ebp)
 569:	e8 1b ff ff ff       	call   489 <write>
 56e:	83 c4 10             	add    $0x10,%esp
}
 571:	90                   	nop
 572:	c9                   	leave  
 573:	c3                   	ret    

00000574 <printint>:

static void
printint(int fd, int xx, int base, int sgn)
{
 574:	55                   	push   %ebp
 575:	89 e5                	mov    %esp,%ebp
 577:	53                   	push   %ebx
 578:	83 ec 24             	sub    $0x24,%esp
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 57b:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  if(sgn && xx < 0){
 582:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
 586:	74 17                	je     59f <printint+0x2b>
 588:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
 58c:	79 11                	jns    59f <printint+0x2b>
    neg = 1;
 58e:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
    x = -xx;
 595:	8b 45 0c             	mov    0xc(%ebp),%eax
 598:	f7 d8                	neg    %eax
 59a:	89 45 ec             	mov    %eax,-0x14(%ebp)
 59d:	eb 06                	jmp    5a5 <printint+0x31>
  } else {
    x = xx;
 59f:	8b 45 0c             	mov    0xc(%ebp),%eax
 5a2:	89 45 ec             	mov    %eax,-0x14(%ebp)
  }

  i = 0;
 5a5:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  do{
    buf[i++] = digits[x % base];
 5ac:	8b 4d f4             	mov    -0xc(%ebp),%ecx
 5af:	8d 41 01             	lea    0x1(%ecx),%eax
 5b2:	89 45 f4             	mov    %eax,-0xc(%ebp)
 5b5:	8b 5d 10             	mov    0x10(%ebp),%ebx
 5b8:	8b 45 ec             	mov    -0x14(%ebp),%eax
 5bb:	ba 00 00 00 00       	mov    $0x0,%edx
 5c0:	f7 f3                	div    %ebx
 5c2:	89 d0                	mov    %edx,%eax
 5c4:	0f b6 80 64 0e 00 00 	movzbl 0xe64(%eax),%eax
 5cb:	88 44 0d dc          	mov    %al,-0x24(%ebp,%ecx,1)
  }while((x /= base) != 0);
 5cf:	8b 5d 10             	mov    0x10(%ebp),%ebx
 5d2:	8b 45 ec             	mov    -0x14(%ebp),%eax
 5d5:	ba 00 00 00 00       	mov    $0x0,%edx
 5da:	f7 f3                	div    %ebx
 5dc:	89 45 ec             	mov    %eax,-0x14(%ebp)
 5df:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 5e3:	75 c7                	jne    5ac <printint+0x38>
  if(neg)
 5e5:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 5e9:	74 2d                	je     618 <printint+0xa4>
    buf[i++] = '-';
 5eb:	8b 45 f4             	mov    -0xc(%ebp),%eax
 5ee:	8d 50 01             	lea    0x1(%eax),%edx
 5f1:	89 55 f4             	mov    %edx,-0xc(%ebp)
 5f4:	c6 44 05 dc 2d       	movb   $0x2d,-0x24(%ebp,%eax,1)

  while(--i >= 0)
 5f9:	eb 1d                	jmp    618 <printint+0xa4>
    putc(fd, buf[i]);
 5fb:	8d 55 dc             	lea    -0x24(%ebp),%edx
 5fe:	8b 45 f4             	mov    -0xc(%ebp),%eax
 601:	01 d0                	add    %edx,%eax
 603:	0f b6 00             	movzbl (%eax),%eax
 606:	0f be c0             	movsbl %al,%eax
 609:	83 ec 08             	sub    $0x8,%esp
 60c:	50                   	push   %eax
 60d:	ff 75 08             	pushl  0x8(%ebp)
 610:	e8 3c ff ff ff       	call   551 <putc>
 615:	83 c4 10             	add    $0x10,%esp
    buf[i++] = digits[x % base];
  }while((x /= base) != 0);
  if(neg)
    buf[i++] = '-';

  while(--i >= 0)
 618:	83 6d f4 01          	subl   $0x1,-0xc(%ebp)
 61c:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 620:	79 d9                	jns    5fb <printint+0x87>
    putc(fd, buf[i]);
}
 622:	90                   	nop
 623:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 626:	c9                   	leave  
 627:	c3                   	ret    

00000628 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, char *fmt, ...)
{
 628:	55                   	push   %ebp
 629:	89 e5                	mov    %esp,%ebp
 62b:	83 ec 28             	sub    $0x28,%esp
  char *s;
  int c, i, state;
  uint *ap;

  state = 0;
 62e:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  ap = (uint*)(void*)&fmt + 1;
 635:	8d 45 0c             	lea    0xc(%ebp),%eax
 638:	83 c0 04             	add    $0x4,%eax
 63b:	89 45 e8             	mov    %eax,-0x18(%ebp)
  for(i = 0; fmt[i]; i++){
 63e:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 645:	e9 59 01 00 00       	jmp    7a3 <printf+0x17b>
    c = fmt[i] & 0xff;
 64a:	8b 55 0c             	mov    0xc(%ebp),%edx
 64d:	8b 45 f0             	mov    -0x10(%ebp),%eax
 650:	01 d0                	add    %edx,%eax
 652:	0f b6 00             	movzbl (%eax),%eax
 655:	0f be c0             	movsbl %al,%eax
 658:	25 ff 00 00 00       	and    $0xff,%eax
 65d:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    if(state == 0){
 660:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 664:	75 2c                	jne    692 <printf+0x6a>
      if(c == '%'){
 666:	83 7d e4 25          	cmpl   $0x25,-0x1c(%ebp)
 66a:	75 0c                	jne    678 <printf+0x50>
        state = '%';
 66c:	c7 45 ec 25 00 00 00 	movl   $0x25,-0x14(%ebp)
 673:	e9 27 01 00 00       	jmp    79f <printf+0x177>
      } else {
        putc(fd, c);
 678:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 67b:	0f be c0             	movsbl %al,%eax
 67e:	83 ec 08             	sub    $0x8,%esp
 681:	50                   	push   %eax
 682:	ff 75 08             	pushl  0x8(%ebp)
 685:	e8 c7 fe ff ff       	call   551 <putc>
 68a:	83 c4 10             	add    $0x10,%esp
 68d:	e9 0d 01 00 00       	jmp    79f <printf+0x177>
      }
    } else if(state == '%'){
 692:	83 7d ec 25          	cmpl   $0x25,-0x14(%ebp)
 696:	0f 85 03 01 00 00    	jne    79f <printf+0x177>
      if(c == 'd'){
 69c:	83 7d e4 64          	cmpl   $0x64,-0x1c(%ebp)
 6a0:	75 1e                	jne    6c0 <printf+0x98>
        printint(fd, *ap, 10, 1);
 6a2:	8b 45 e8             	mov    -0x18(%ebp),%eax
 6a5:	8b 00                	mov    (%eax),%eax
 6a7:	6a 01                	push   $0x1
 6a9:	6a 0a                	push   $0xa
 6ab:	50                   	push   %eax
 6ac:	ff 75 08             	pushl  0x8(%ebp)
 6af:	e8 c0 fe ff ff       	call   574 <printint>
 6b4:	83 c4 10             	add    $0x10,%esp
        ap++;
 6b7:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 6bb:	e9 d8 00 00 00       	jmp    798 <printf+0x170>
      } else if(c == 'x' || c == 'p'){
 6c0:	83 7d e4 78          	cmpl   $0x78,-0x1c(%ebp)
 6c4:	74 06                	je     6cc <printf+0xa4>
 6c6:	83 7d e4 70          	cmpl   $0x70,-0x1c(%ebp)
 6ca:	75 1e                	jne    6ea <printf+0xc2>
        printint(fd, *ap, 16, 0);
 6cc:	8b 45 e8             	mov    -0x18(%ebp),%eax
 6cf:	8b 00                	mov    (%eax),%eax
 6d1:	6a 00                	push   $0x0
 6d3:	6a 10                	push   $0x10
 6d5:	50                   	push   %eax
 6d6:	ff 75 08             	pushl  0x8(%ebp)
 6d9:	e8 96 fe ff ff       	call   574 <printint>
 6de:	83 c4 10             	add    $0x10,%esp
        ap++;
 6e1:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 6e5:	e9 ae 00 00 00       	jmp    798 <printf+0x170>
      } else if(c == 's'){
 6ea:	83 7d e4 73          	cmpl   $0x73,-0x1c(%ebp)
 6ee:	75 43                	jne    733 <printf+0x10b>
        s = (char*)*ap;
 6f0:	8b 45 e8             	mov    -0x18(%ebp),%eax
 6f3:	8b 00                	mov    (%eax),%eax
 6f5:	89 45 f4             	mov    %eax,-0xc(%ebp)
        ap++;
 6f8:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
        if(s == 0)
 6fc:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 700:	75 25                	jne    727 <printf+0xff>
          s = "(null)";
 702:	c7 45 f4 94 0b 00 00 	movl   $0xb94,-0xc(%ebp)
        while(*s != 0){
 709:	eb 1c                	jmp    727 <printf+0xff>
          putc(fd, *s);
 70b:	8b 45 f4             	mov    -0xc(%ebp),%eax
 70e:	0f b6 00             	movzbl (%eax),%eax
 711:	0f be c0             	movsbl %al,%eax
 714:	83 ec 08             	sub    $0x8,%esp
 717:	50                   	push   %eax
 718:	ff 75 08             	pushl  0x8(%ebp)
 71b:	e8 31 fe ff ff       	call   551 <putc>
 720:	83 c4 10             	add    $0x10,%esp
          s++;
 723:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
      } else if(c == 's'){
        s = (char*)*ap;
        ap++;
        if(s == 0)
          s = "(null)";
        while(*s != 0){
 727:	8b 45 f4             	mov    -0xc(%ebp),%eax
 72a:	0f b6 00             	movzbl (%eax),%eax
 72d:	84 c0                	test   %al,%al
 72f:	75 da                	jne    70b <printf+0xe3>
 731:	eb 65                	jmp    798 <printf+0x170>
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 733:	83 7d e4 63          	cmpl   $0x63,-0x1c(%ebp)
 737:	75 1d                	jne    756 <printf+0x12e>
        putc(fd, *ap);
 739:	8b 45 e8             	mov    -0x18(%ebp),%eax
 73c:	8b 00                	mov    (%eax),%eax
 73e:	0f be c0             	movsbl %al,%eax
 741:	83 ec 08             	sub    $0x8,%esp
 744:	50                   	push   %eax
 745:	ff 75 08             	pushl  0x8(%ebp)
 748:	e8 04 fe ff ff       	call   551 <putc>
 74d:	83 c4 10             	add    $0x10,%esp
        ap++;
 750:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 754:	eb 42                	jmp    798 <printf+0x170>
      } else if(c == '%'){
 756:	83 7d e4 25          	cmpl   $0x25,-0x1c(%ebp)
 75a:	75 17                	jne    773 <printf+0x14b>
        putc(fd, c);
 75c:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 75f:	0f be c0             	movsbl %al,%eax
 762:	83 ec 08             	sub    $0x8,%esp
 765:	50                   	push   %eax
 766:	ff 75 08             	pushl  0x8(%ebp)
 769:	e8 e3 fd ff ff       	call   551 <putc>
 76e:	83 c4 10             	add    $0x10,%esp
 771:	eb 25                	jmp    798 <printf+0x170>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 773:	83 ec 08             	sub    $0x8,%esp
 776:	6a 25                	push   $0x25
 778:	ff 75 08             	pushl  0x8(%ebp)
 77b:	e8 d1 fd ff ff       	call   551 <putc>
 780:	83 c4 10             	add    $0x10,%esp
        putc(fd, c);
 783:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 786:	0f be c0             	movsbl %al,%eax
 789:	83 ec 08             	sub    $0x8,%esp
 78c:	50                   	push   %eax
 78d:	ff 75 08             	pushl  0x8(%ebp)
 790:	e8 bc fd ff ff       	call   551 <putc>
 795:	83 c4 10             	add    $0x10,%esp
      }
      state = 0;
 798:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
 79f:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
 7a3:	8b 55 0c             	mov    0xc(%ebp),%edx
 7a6:	8b 45 f0             	mov    -0x10(%ebp),%eax
 7a9:	01 d0                	add    %edx,%eax
 7ab:	0f b6 00             	movzbl (%eax),%eax
 7ae:	84 c0                	test   %al,%al
 7b0:	0f 85 94 fe ff ff    	jne    64a <printf+0x22>
        putc(fd, c);
      }
      state = 0;
    }
  }
}
 7b6:	90                   	nop
 7b7:	c9                   	leave  
 7b8:	c3                   	ret    

000007b9 <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 7b9:	55                   	push   %ebp
 7ba:	89 e5                	mov    %esp,%ebp
 7bc:	83 ec 10             	sub    $0x10,%esp
  Header *bp, *p;

  bp = (Header*)ap - 1;
 7bf:	8b 45 08             	mov    0x8(%ebp),%eax
 7c2:	83 e8 08             	sub    $0x8,%eax
 7c5:	89 45 f8             	mov    %eax,-0x8(%ebp)
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 7c8:	a1 80 0e 00 00       	mov    0xe80,%eax
 7cd:	89 45 fc             	mov    %eax,-0x4(%ebp)
 7d0:	eb 24                	jmp    7f6 <free+0x3d>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 7d2:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7d5:	8b 00                	mov    (%eax),%eax
 7d7:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 7da:	77 12                	ja     7ee <free+0x35>
 7dc:	8b 45 f8             	mov    -0x8(%ebp),%eax
 7df:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 7e2:	77 24                	ja     808 <free+0x4f>
 7e4:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7e7:	8b 00                	mov    (%eax),%eax
 7e9:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 7ec:	77 1a                	ja     808 <free+0x4f>
free(void *ap)
{
  Header *bp, *p;

  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 7ee:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7f1:	8b 00                	mov    (%eax),%eax
 7f3:	89 45 fc             	mov    %eax,-0x4(%ebp)
 7f6:	8b 45 f8             	mov    -0x8(%ebp),%eax
 7f9:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 7fc:	76 d4                	jbe    7d2 <free+0x19>
 7fe:	8b 45 fc             	mov    -0x4(%ebp),%eax
 801:	8b 00                	mov    (%eax),%eax
 803:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 806:	76 ca                	jbe    7d2 <free+0x19>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if(bp + bp->s.size == p->s.ptr){
 808:	8b 45 f8             	mov    -0x8(%ebp),%eax
 80b:	8b 40 04             	mov    0x4(%eax),%eax
 80e:	8d 14 c5 00 00 00 00 	lea    0x0(,%eax,8),%edx
 815:	8b 45 f8             	mov    -0x8(%ebp),%eax
 818:	01 c2                	add    %eax,%edx
 81a:	8b 45 fc             	mov    -0x4(%ebp),%eax
 81d:	8b 00                	mov    (%eax),%eax
 81f:	39 c2                	cmp    %eax,%edx
 821:	75 24                	jne    847 <free+0x8e>
    bp->s.size += p->s.ptr->s.size;
 823:	8b 45 f8             	mov    -0x8(%ebp),%eax
 826:	8b 50 04             	mov    0x4(%eax),%edx
 829:	8b 45 fc             	mov    -0x4(%ebp),%eax
 82c:	8b 00                	mov    (%eax),%eax
 82e:	8b 40 04             	mov    0x4(%eax),%eax
 831:	01 c2                	add    %eax,%edx
 833:	8b 45 f8             	mov    -0x8(%ebp),%eax
 836:	89 50 04             	mov    %edx,0x4(%eax)
    bp->s.ptr = p->s.ptr->s.ptr;
 839:	8b 45 fc             	mov    -0x4(%ebp),%eax
 83c:	8b 00                	mov    (%eax),%eax
 83e:	8b 10                	mov    (%eax),%edx
 840:	8b 45 f8             	mov    -0x8(%ebp),%eax
 843:	89 10                	mov    %edx,(%eax)
 845:	eb 0a                	jmp    851 <free+0x98>
  } else
    bp->s.ptr = p->s.ptr;
 847:	8b 45 fc             	mov    -0x4(%ebp),%eax
 84a:	8b 10                	mov    (%eax),%edx
 84c:	8b 45 f8             	mov    -0x8(%ebp),%eax
 84f:	89 10                	mov    %edx,(%eax)
  if(p + p->s.size == bp){
 851:	8b 45 fc             	mov    -0x4(%ebp),%eax
 854:	8b 40 04             	mov    0x4(%eax),%eax
 857:	8d 14 c5 00 00 00 00 	lea    0x0(,%eax,8),%edx
 85e:	8b 45 fc             	mov    -0x4(%ebp),%eax
 861:	01 d0                	add    %edx,%eax
 863:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 866:	75 20                	jne    888 <free+0xcf>
    p->s.size += bp->s.size;
 868:	8b 45 fc             	mov    -0x4(%ebp),%eax
 86b:	8b 50 04             	mov    0x4(%eax),%edx
 86e:	8b 45 f8             	mov    -0x8(%ebp),%eax
 871:	8b 40 04             	mov    0x4(%eax),%eax
 874:	01 c2                	add    %eax,%edx
 876:	8b 45 fc             	mov    -0x4(%ebp),%eax
 879:	89 50 04             	mov    %edx,0x4(%eax)
    p->s.ptr = bp->s.ptr;
 87c:	8b 45 f8             	mov    -0x8(%ebp),%eax
 87f:	8b 10                	mov    (%eax),%edx
 881:	8b 45 fc             	mov    -0x4(%ebp),%eax
 884:	89 10                	mov    %edx,(%eax)
 886:	eb 08                	jmp    890 <free+0xd7>
  } else
    p->s.ptr = bp;
 888:	8b 45 fc             	mov    -0x4(%ebp),%eax
 88b:	8b 55 f8             	mov    -0x8(%ebp),%edx
 88e:	89 10                	mov    %edx,(%eax)
  freep = p;
 890:	8b 45 fc             	mov    -0x4(%ebp),%eax
 893:	a3 80 0e 00 00       	mov    %eax,0xe80
}
 898:	90                   	nop
 899:	c9                   	leave  
 89a:	c3                   	ret    

0000089b <morecore>:

static Header*
morecore(uint nu)
{
 89b:	55                   	push   %ebp
 89c:	89 e5                	mov    %esp,%ebp
 89e:	83 ec 18             	sub    $0x18,%esp
  char *p;
  Header *hp;

  if(nu < 4096)
 8a1:	81 7d 08 ff 0f 00 00 	cmpl   $0xfff,0x8(%ebp)
 8a8:	77 07                	ja     8b1 <morecore+0x16>
    nu = 4096;
 8aa:	c7 45 08 00 10 00 00 	movl   $0x1000,0x8(%ebp)
  p = sbrk(nu * sizeof(Header));
 8b1:	8b 45 08             	mov    0x8(%ebp),%eax
 8b4:	c1 e0 03             	shl    $0x3,%eax
 8b7:	83 ec 0c             	sub    $0xc,%esp
 8ba:	50                   	push   %eax
 8bb:	e8 31 fc ff ff       	call   4f1 <sbrk>
 8c0:	83 c4 10             	add    $0x10,%esp
 8c3:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(p == (char*)-1)
 8c6:	83 7d f4 ff          	cmpl   $0xffffffff,-0xc(%ebp)
 8ca:	75 07                	jne    8d3 <morecore+0x38>
    return 0;
 8cc:	b8 00 00 00 00       	mov    $0x0,%eax
 8d1:	eb 26                	jmp    8f9 <morecore+0x5e>
  hp = (Header*)p;
 8d3:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8d6:	89 45 f0             	mov    %eax,-0x10(%ebp)
  hp->s.size = nu;
 8d9:	8b 45 f0             	mov    -0x10(%ebp),%eax
 8dc:	8b 55 08             	mov    0x8(%ebp),%edx
 8df:	89 50 04             	mov    %edx,0x4(%eax)
  free((void*)(hp + 1));
 8e2:	8b 45 f0             	mov    -0x10(%ebp),%eax
 8e5:	83 c0 08             	add    $0x8,%eax
 8e8:	83 ec 0c             	sub    $0xc,%esp
 8eb:	50                   	push   %eax
 8ec:	e8 c8 fe ff ff       	call   7b9 <free>
 8f1:	83 c4 10             	add    $0x10,%esp
  return freep;
 8f4:	a1 80 0e 00 00       	mov    0xe80,%eax
}
 8f9:	c9                   	leave  
 8fa:	c3                   	ret    

000008fb <malloc>:

void*
malloc(uint nbytes)
{
 8fb:	55                   	push   %ebp
 8fc:	89 e5                	mov    %esp,%ebp
 8fe:	83 ec 18             	sub    $0x18,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 901:	8b 45 08             	mov    0x8(%ebp),%eax
 904:	83 c0 07             	add    $0x7,%eax
 907:	c1 e8 03             	shr    $0x3,%eax
 90a:	83 c0 01             	add    $0x1,%eax
 90d:	89 45 ec             	mov    %eax,-0x14(%ebp)
  if((prevp = freep) == 0){
 910:	a1 80 0e 00 00       	mov    0xe80,%eax
 915:	89 45 f0             	mov    %eax,-0x10(%ebp)
 918:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 91c:	75 23                	jne    941 <malloc+0x46>
    base.s.ptr = freep = prevp = &base;
 91e:	c7 45 f0 78 0e 00 00 	movl   $0xe78,-0x10(%ebp)
 925:	8b 45 f0             	mov    -0x10(%ebp),%eax
 928:	a3 80 0e 00 00       	mov    %eax,0xe80
 92d:	a1 80 0e 00 00       	mov    0xe80,%eax
 932:	a3 78 0e 00 00       	mov    %eax,0xe78
    base.s.size = 0;
 937:	c7 05 7c 0e 00 00 00 	movl   $0x0,0xe7c
 93e:	00 00 00 
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 941:	8b 45 f0             	mov    -0x10(%ebp),%eax
 944:	8b 00                	mov    (%eax),%eax
 946:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if(p->s.size >= nunits){
 949:	8b 45 f4             	mov    -0xc(%ebp),%eax
 94c:	8b 40 04             	mov    0x4(%eax),%eax
 94f:	3b 45 ec             	cmp    -0x14(%ebp),%eax
 952:	72 4d                	jb     9a1 <malloc+0xa6>
      if(p->s.size == nunits)
 954:	8b 45 f4             	mov    -0xc(%ebp),%eax
 957:	8b 40 04             	mov    0x4(%eax),%eax
 95a:	3b 45 ec             	cmp    -0x14(%ebp),%eax
 95d:	75 0c                	jne    96b <malloc+0x70>
        prevp->s.ptr = p->s.ptr;
 95f:	8b 45 f4             	mov    -0xc(%ebp),%eax
 962:	8b 10                	mov    (%eax),%edx
 964:	8b 45 f0             	mov    -0x10(%ebp),%eax
 967:	89 10                	mov    %edx,(%eax)
 969:	eb 26                	jmp    991 <malloc+0x96>
      else {
        p->s.size -= nunits;
 96b:	8b 45 f4             	mov    -0xc(%ebp),%eax
 96e:	8b 40 04             	mov    0x4(%eax),%eax
 971:	2b 45 ec             	sub    -0x14(%ebp),%eax
 974:	89 c2                	mov    %eax,%edx
 976:	8b 45 f4             	mov    -0xc(%ebp),%eax
 979:	89 50 04             	mov    %edx,0x4(%eax)
        p += p->s.size;
 97c:	8b 45 f4             	mov    -0xc(%ebp),%eax
 97f:	8b 40 04             	mov    0x4(%eax),%eax
 982:	c1 e0 03             	shl    $0x3,%eax
 985:	01 45 f4             	add    %eax,-0xc(%ebp)
        p->s.size = nunits;
 988:	8b 45 f4             	mov    -0xc(%ebp),%eax
 98b:	8b 55 ec             	mov    -0x14(%ebp),%edx
 98e:	89 50 04             	mov    %edx,0x4(%eax)
      }
      freep = prevp;
 991:	8b 45 f0             	mov    -0x10(%ebp),%eax
 994:	a3 80 0e 00 00       	mov    %eax,0xe80
      return (void*)(p + 1);
 999:	8b 45 f4             	mov    -0xc(%ebp),%eax
 99c:	83 c0 08             	add    $0x8,%eax
 99f:	eb 3b                	jmp    9dc <malloc+0xe1>
    }
    if(p == freep)
 9a1:	a1 80 0e 00 00       	mov    0xe80,%eax
 9a6:	39 45 f4             	cmp    %eax,-0xc(%ebp)
 9a9:	75 1e                	jne    9c9 <malloc+0xce>
      if((p = morecore(nunits)) == 0)
 9ab:	83 ec 0c             	sub    $0xc,%esp
 9ae:	ff 75 ec             	pushl  -0x14(%ebp)
 9b1:	e8 e5 fe ff ff       	call   89b <morecore>
 9b6:	83 c4 10             	add    $0x10,%esp
 9b9:	89 45 f4             	mov    %eax,-0xc(%ebp)
 9bc:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 9c0:	75 07                	jne    9c9 <malloc+0xce>
        return 0;
 9c2:	b8 00 00 00 00       	mov    $0x0,%eax
 9c7:	eb 13                	jmp    9dc <malloc+0xe1>
  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
  if((prevp = freep) == 0){
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 9c9:	8b 45 f4             	mov    -0xc(%ebp),%eax
 9cc:	89 45 f0             	mov    %eax,-0x10(%ebp)
 9cf:	8b 45 f4             	mov    -0xc(%ebp),%eax
 9d2:	8b 00                	mov    (%eax),%eax
 9d4:	89 45 f4             	mov    %eax,-0xc(%ebp)
      return (void*)(p + 1);
    }
    if(p == freep)
      if((p = morecore(nunits)) == 0)
        return 0;
  }
 9d7:	e9 6d ff ff ff       	jmp    949 <malloc+0x4e>
}
 9dc:	c9                   	leave  
 9dd:	c3                   	ret    
