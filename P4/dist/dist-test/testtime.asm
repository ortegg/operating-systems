
_testtime:     file format elf32-i386


Disassembly of section .text:

00000000 <main>:
#include "types.h"
#include "user.h"

int main()
{
   0:	8d 4c 24 04          	lea    0x4(%esp),%ecx
   4:	83 e4 f0             	and    $0xfffffff0,%esp
   7:	ff 71 fc             	pushl  -0x4(%ecx)
   a:	55                   	push   %ebp
   b:	89 e5                	mov    %esp,%ebp
   d:	51                   	push   %ecx
   e:	83 ec 04             	sub    $0x4,%esp
  printf(1, "Track my time with PS or CTRL-P. I'm going to sleep for awhile. Goodnight."); 
  11:	83 ec 08             	sub    $0x8,%esp
  14:	68 0c 08 00 00       	push   $0x80c
  19:	6a 01                	push   $0x1
  1b:	e8 36 04 00 00       	call   456 <printf>
  20:	83 c4 10             	add    $0x10,%esp
  sleep(5000);
  23:	83 ec 0c             	sub    $0xc,%esp
  26:	68 88 13 00 00       	push   $0x1388
  2b:	e8 f7 02 00 00       	call   327 <sleep>
  30:	83 c4 10             	add    $0x10,%esp
  return 0;
  33:	b8 00 00 00 00       	mov    $0x0,%eax
}
  38:	8b 4d fc             	mov    -0x4(%ebp),%ecx
  3b:	c9                   	leave  
  3c:	8d 61 fc             	lea    -0x4(%ecx),%esp
  3f:	c3                   	ret    

00000040 <stosb>:
               "cc");
}

static inline void
stosb(void *addr, int data, int cnt)
{
  40:	55                   	push   %ebp
  41:	89 e5                	mov    %esp,%ebp
  43:	57                   	push   %edi
  44:	53                   	push   %ebx
  asm volatile("cld; rep stosb" :
  45:	8b 4d 08             	mov    0x8(%ebp),%ecx
  48:	8b 55 10             	mov    0x10(%ebp),%edx
  4b:	8b 45 0c             	mov    0xc(%ebp),%eax
  4e:	89 cb                	mov    %ecx,%ebx
  50:	89 df                	mov    %ebx,%edi
  52:	89 d1                	mov    %edx,%ecx
  54:	fc                   	cld    
  55:	f3 aa                	rep stos %al,%es:(%edi)
  57:	89 ca                	mov    %ecx,%edx
  59:	89 fb                	mov    %edi,%ebx
  5b:	89 5d 08             	mov    %ebx,0x8(%ebp)
  5e:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
  61:	90                   	nop
  62:	5b                   	pop    %ebx
  63:	5f                   	pop    %edi
  64:	5d                   	pop    %ebp
  65:	c3                   	ret    

00000066 <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, char *t)
{
  66:	55                   	push   %ebp
  67:	89 e5                	mov    %esp,%ebp
  69:	83 ec 10             	sub    $0x10,%esp
  char *os;

  os = s;
  6c:	8b 45 08             	mov    0x8(%ebp),%eax
  6f:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while((*s++ = *t++) != 0)
  72:	90                   	nop
  73:	8b 45 08             	mov    0x8(%ebp),%eax
  76:	8d 50 01             	lea    0x1(%eax),%edx
  79:	89 55 08             	mov    %edx,0x8(%ebp)
  7c:	8b 55 0c             	mov    0xc(%ebp),%edx
  7f:	8d 4a 01             	lea    0x1(%edx),%ecx
  82:	89 4d 0c             	mov    %ecx,0xc(%ebp)
  85:	0f b6 12             	movzbl (%edx),%edx
  88:	88 10                	mov    %dl,(%eax)
  8a:	0f b6 00             	movzbl (%eax),%eax
  8d:	84 c0                	test   %al,%al
  8f:	75 e2                	jne    73 <strcpy+0xd>
    ;
  return os;
  91:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  94:	c9                   	leave  
  95:	c3                   	ret    

00000096 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  96:	55                   	push   %ebp
  97:	89 e5                	mov    %esp,%ebp
  while(*p && *p == *q)
  99:	eb 08                	jmp    a3 <strcmp+0xd>
    p++, q++;
  9b:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  9f:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
  while(*p && *p == *q)
  a3:	8b 45 08             	mov    0x8(%ebp),%eax
  a6:	0f b6 00             	movzbl (%eax),%eax
  a9:	84 c0                	test   %al,%al
  ab:	74 10                	je     bd <strcmp+0x27>
  ad:	8b 45 08             	mov    0x8(%ebp),%eax
  b0:	0f b6 10             	movzbl (%eax),%edx
  b3:	8b 45 0c             	mov    0xc(%ebp),%eax
  b6:	0f b6 00             	movzbl (%eax),%eax
  b9:	38 c2                	cmp    %al,%dl
  bb:	74 de                	je     9b <strcmp+0x5>
    p++, q++;
  return (uchar)*p - (uchar)*q;
  bd:	8b 45 08             	mov    0x8(%ebp),%eax
  c0:	0f b6 00             	movzbl (%eax),%eax
  c3:	0f b6 d0             	movzbl %al,%edx
  c6:	8b 45 0c             	mov    0xc(%ebp),%eax
  c9:	0f b6 00             	movzbl (%eax),%eax
  cc:	0f b6 c0             	movzbl %al,%eax
  cf:	29 c2                	sub    %eax,%edx
  d1:	89 d0                	mov    %edx,%eax
}
  d3:	5d                   	pop    %ebp
  d4:	c3                   	ret    

000000d5 <strlen>:

uint
strlen(char *s)
{
  d5:	55                   	push   %ebp
  d6:	89 e5                	mov    %esp,%ebp
  d8:	83 ec 10             	sub    $0x10,%esp
  int n;

  for(n = 0; s[n]; n++)
  db:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  e2:	eb 04                	jmp    e8 <strlen+0x13>
  e4:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
  e8:	8b 55 fc             	mov    -0x4(%ebp),%edx
  eb:	8b 45 08             	mov    0x8(%ebp),%eax
  ee:	01 d0                	add    %edx,%eax
  f0:	0f b6 00             	movzbl (%eax),%eax
  f3:	84 c0                	test   %al,%al
  f5:	75 ed                	jne    e4 <strlen+0xf>
    ;
  return n;
  f7:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  fa:	c9                   	leave  
  fb:	c3                   	ret    

000000fc <memset>:

void*
memset(void *dst, int c, uint n)
{
  fc:	55                   	push   %ebp
  fd:	89 e5                	mov    %esp,%ebp
  stosb(dst, c, n);
  ff:	8b 45 10             	mov    0x10(%ebp),%eax
 102:	50                   	push   %eax
 103:	ff 75 0c             	pushl  0xc(%ebp)
 106:	ff 75 08             	pushl  0x8(%ebp)
 109:	e8 32 ff ff ff       	call   40 <stosb>
 10e:	83 c4 0c             	add    $0xc,%esp
  return dst;
 111:	8b 45 08             	mov    0x8(%ebp),%eax
}
 114:	c9                   	leave  
 115:	c3                   	ret    

00000116 <strchr>:

char*
strchr(const char *s, char c)
{
 116:	55                   	push   %ebp
 117:	89 e5                	mov    %esp,%ebp
 119:	83 ec 04             	sub    $0x4,%esp
 11c:	8b 45 0c             	mov    0xc(%ebp),%eax
 11f:	88 45 fc             	mov    %al,-0x4(%ebp)
  for(; *s; s++)
 122:	eb 14                	jmp    138 <strchr+0x22>
    if(*s == c)
 124:	8b 45 08             	mov    0x8(%ebp),%eax
 127:	0f b6 00             	movzbl (%eax),%eax
 12a:	3a 45 fc             	cmp    -0x4(%ebp),%al
 12d:	75 05                	jne    134 <strchr+0x1e>
      return (char*)s;
 12f:	8b 45 08             	mov    0x8(%ebp),%eax
 132:	eb 13                	jmp    147 <strchr+0x31>
}

char*
strchr(const char *s, char c)
{
  for(; *s; s++)
 134:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 138:	8b 45 08             	mov    0x8(%ebp),%eax
 13b:	0f b6 00             	movzbl (%eax),%eax
 13e:	84 c0                	test   %al,%al
 140:	75 e2                	jne    124 <strchr+0xe>
    if(*s == c)
      return (char*)s;
  return 0;
 142:	b8 00 00 00 00       	mov    $0x0,%eax
}
 147:	c9                   	leave  
 148:	c3                   	ret    

00000149 <gets>:

char*
gets(char *buf, int max)
{
 149:	55                   	push   %ebp
 14a:	89 e5                	mov    %esp,%ebp
 14c:	83 ec 18             	sub    $0x18,%esp
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 14f:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 156:	eb 42                	jmp    19a <gets+0x51>
    cc = read(0, &c, 1);
 158:	83 ec 04             	sub    $0x4,%esp
 15b:	6a 01                	push   $0x1
 15d:	8d 45 ef             	lea    -0x11(%ebp),%eax
 160:	50                   	push   %eax
 161:	6a 00                	push   $0x0
 163:	e8 47 01 00 00       	call   2af <read>
 168:	83 c4 10             	add    $0x10,%esp
 16b:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if(cc < 1)
 16e:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 172:	7e 33                	jle    1a7 <gets+0x5e>
      break;
    buf[i++] = c;
 174:	8b 45 f4             	mov    -0xc(%ebp),%eax
 177:	8d 50 01             	lea    0x1(%eax),%edx
 17a:	89 55 f4             	mov    %edx,-0xc(%ebp)
 17d:	89 c2                	mov    %eax,%edx
 17f:	8b 45 08             	mov    0x8(%ebp),%eax
 182:	01 c2                	add    %eax,%edx
 184:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 188:	88 02                	mov    %al,(%edx)
    if(c == '\n' || c == '\r')
 18a:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 18e:	3c 0a                	cmp    $0xa,%al
 190:	74 16                	je     1a8 <gets+0x5f>
 192:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 196:	3c 0d                	cmp    $0xd,%al
 198:	74 0e                	je     1a8 <gets+0x5f>
gets(char *buf, int max)
{
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 19a:	8b 45 f4             	mov    -0xc(%ebp),%eax
 19d:	83 c0 01             	add    $0x1,%eax
 1a0:	3b 45 0c             	cmp    0xc(%ebp),%eax
 1a3:	7c b3                	jl     158 <gets+0xf>
 1a5:	eb 01                	jmp    1a8 <gets+0x5f>
    cc = read(0, &c, 1);
    if(cc < 1)
      break;
 1a7:	90                   	nop
    buf[i++] = c;
    if(c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 1a8:	8b 55 f4             	mov    -0xc(%ebp),%edx
 1ab:	8b 45 08             	mov    0x8(%ebp),%eax
 1ae:	01 d0                	add    %edx,%eax
 1b0:	c6 00 00             	movb   $0x0,(%eax)
  return buf;
 1b3:	8b 45 08             	mov    0x8(%ebp),%eax
}
 1b6:	c9                   	leave  
 1b7:	c3                   	ret    

000001b8 <stat>:

int
stat(char *n, struct stat *st)
{
 1b8:	55                   	push   %ebp
 1b9:	89 e5                	mov    %esp,%ebp
 1bb:	83 ec 18             	sub    $0x18,%esp
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 1be:	83 ec 08             	sub    $0x8,%esp
 1c1:	6a 00                	push   $0x0
 1c3:	ff 75 08             	pushl  0x8(%ebp)
 1c6:	e8 0c 01 00 00       	call   2d7 <open>
 1cb:	83 c4 10             	add    $0x10,%esp
 1ce:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(fd < 0)
 1d1:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 1d5:	79 07                	jns    1de <stat+0x26>
    return -1;
 1d7:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 1dc:	eb 25                	jmp    203 <stat+0x4b>
  r = fstat(fd, st);
 1de:	83 ec 08             	sub    $0x8,%esp
 1e1:	ff 75 0c             	pushl  0xc(%ebp)
 1e4:	ff 75 f4             	pushl  -0xc(%ebp)
 1e7:	e8 03 01 00 00       	call   2ef <fstat>
 1ec:	83 c4 10             	add    $0x10,%esp
 1ef:	89 45 f0             	mov    %eax,-0x10(%ebp)
  close(fd);
 1f2:	83 ec 0c             	sub    $0xc,%esp
 1f5:	ff 75 f4             	pushl  -0xc(%ebp)
 1f8:	e8 c2 00 00 00       	call   2bf <close>
 1fd:	83 c4 10             	add    $0x10,%esp
  return r;
 200:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
 203:	c9                   	leave  
 204:	c3                   	ret    

00000205 <atoi>:

int
atoi(const char *s)
{
 205:	55                   	push   %ebp
 206:	89 e5                	mov    %esp,%ebp
 208:	83 ec 10             	sub    $0x10,%esp
  int n;

  n = 0;
 20b:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  while('0' <= *s && *s <= '9')
 212:	eb 25                	jmp    239 <atoi+0x34>
    n = n*10 + *s++ - '0';
 214:	8b 55 fc             	mov    -0x4(%ebp),%edx
 217:	89 d0                	mov    %edx,%eax
 219:	c1 e0 02             	shl    $0x2,%eax
 21c:	01 d0                	add    %edx,%eax
 21e:	01 c0                	add    %eax,%eax
 220:	89 c1                	mov    %eax,%ecx
 222:	8b 45 08             	mov    0x8(%ebp),%eax
 225:	8d 50 01             	lea    0x1(%eax),%edx
 228:	89 55 08             	mov    %edx,0x8(%ebp)
 22b:	0f b6 00             	movzbl (%eax),%eax
 22e:	0f be c0             	movsbl %al,%eax
 231:	01 c8                	add    %ecx,%eax
 233:	83 e8 30             	sub    $0x30,%eax
 236:	89 45 fc             	mov    %eax,-0x4(%ebp)
atoi(const char *s)
{
  int n;

  n = 0;
  while('0' <= *s && *s <= '9')
 239:	8b 45 08             	mov    0x8(%ebp),%eax
 23c:	0f b6 00             	movzbl (%eax),%eax
 23f:	3c 2f                	cmp    $0x2f,%al
 241:	7e 0a                	jle    24d <atoi+0x48>
 243:	8b 45 08             	mov    0x8(%ebp),%eax
 246:	0f b6 00             	movzbl (%eax),%eax
 249:	3c 39                	cmp    $0x39,%al
 24b:	7e c7                	jle    214 <atoi+0xf>
    n = n*10 + *s++ - '0';
  return n;
 24d:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 250:	c9                   	leave  
 251:	c3                   	ret    

00000252 <memmove>:

void*
memmove(void *vdst, void *vsrc, int n)
{
 252:	55                   	push   %ebp
 253:	89 e5                	mov    %esp,%ebp
 255:	83 ec 10             	sub    $0x10,%esp
  char *dst, *src;
  
  dst = vdst;
 258:	8b 45 08             	mov    0x8(%ebp),%eax
 25b:	89 45 fc             	mov    %eax,-0x4(%ebp)
  src = vsrc;
 25e:	8b 45 0c             	mov    0xc(%ebp),%eax
 261:	89 45 f8             	mov    %eax,-0x8(%ebp)
  while(n-- > 0)
 264:	eb 17                	jmp    27d <memmove+0x2b>
    *dst++ = *src++;
 266:	8b 45 fc             	mov    -0x4(%ebp),%eax
 269:	8d 50 01             	lea    0x1(%eax),%edx
 26c:	89 55 fc             	mov    %edx,-0x4(%ebp)
 26f:	8b 55 f8             	mov    -0x8(%ebp),%edx
 272:	8d 4a 01             	lea    0x1(%edx),%ecx
 275:	89 4d f8             	mov    %ecx,-0x8(%ebp)
 278:	0f b6 12             	movzbl (%edx),%edx
 27b:	88 10                	mov    %dl,(%eax)
{
  char *dst, *src;
  
  dst = vdst;
  src = vsrc;
  while(n-- > 0)
 27d:	8b 45 10             	mov    0x10(%ebp),%eax
 280:	8d 50 ff             	lea    -0x1(%eax),%edx
 283:	89 55 10             	mov    %edx,0x10(%ebp)
 286:	85 c0                	test   %eax,%eax
 288:	7f dc                	jg     266 <memmove+0x14>
    *dst++ = *src++;
  return vdst;
 28a:	8b 45 08             	mov    0x8(%ebp),%eax
}
 28d:	c9                   	leave  
 28e:	c3                   	ret    

0000028f <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
 28f:	b8 01 00 00 00       	mov    $0x1,%eax
 294:	cd 40                	int    $0x40
 296:	c3                   	ret    

00000297 <exit>:
SYSCALL(exit)
 297:	b8 02 00 00 00       	mov    $0x2,%eax
 29c:	cd 40                	int    $0x40
 29e:	c3                   	ret    

0000029f <wait>:
SYSCALL(wait)
 29f:	b8 03 00 00 00       	mov    $0x3,%eax
 2a4:	cd 40                	int    $0x40
 2a6:	c3                   	ret    

000002a7 <pipe>:
SYSCALL(pipe)
 2a7:	b8 04 00 00 00       	mov    $0x4,%eax
 2ac:	cd 40                	int    $0x40
 2ae:	c3                   	ret    

000002af <read>:
SYSCALL(read)
 2af:	b8 05 00 00 00       	mov    $0x5,%eax
 2b4:	cd 40                	int    $0x40
 2b6:	c3                   	ret    

000002b7 <write>:
SYSCALL(write)
 2b7:	b8 10 00 00 00       	mov    $0x10,%eax
 2bc:	cd 40                	int    $0x40
 2be:	c3                   	ret    

000002bf <close>:
SYSCALL(close)
 2bf:	b8 15 00 00 00       	mov    $0x15,%eax
 2c4:	cd 40                	int    $0x40
 2c6:	c3                   	ret    

000002c7 <kill>:
SYSCALL(kill)
 2c7:	b8 06 00 00 00       	mov    $0x6,%eax
 2cc:	cd 40                	int    $0x40
 2ce:	c3                   	ret    

000002cf <exec>:
SYSCALL(exec)
 2cf:	b8 07 00 00 00       	mov    $0x7,%eax
 2d4:	cd 40                	int    $0x40
 2d6:	c3                   	ret    

000002d7 <open>:
SYSCALL(open)
 2d7:	b8 0f 00 00 00       	mov    $0xf,%eax
 2dc:	cd 40                	int    $0x40
 2de:	c3                   	ret    

000002df <mknod>:
SYSCALL(mknod)
 2df:	b8 11 00 00 00       	mov    $0x11,%eax
 2e4:	cd 40                	int    $0x40
 2e6:	c3                   	ret    

000002e7 <unlink>:
SYSCALL(unlink)
 2e7:	b8 12 00 00 00       	mov    $0x12,%eax
 2ec:	cd 40                	int    $0x40
 2ee:	c3                   	ret    

000002ef <fstat>:
SYSCALL(fstat)
 2ef:	b8 08 00 00 00       	mov    $0x8,%eax
 2f4:	cd 40                	int    $0x40
 2f6:	c3                   	ret    

000002f7 <link>:
SYSCALL(link)
 2f7:	b8 13 00 00 00       	mov    $0x13,%eax
 2fc:	cd 40                	int    $0x40
 2fe:	c3                   	ret    

000002ff <mkdir>:
SYSCALL(mkdir)
 2ff:	b8 14 00 00 00       	mov    $0x14,%eax
 304:	cd 40                	int    $0x40
 306:	c3                   	ret    

00000307 <chdir>:
SYSCALL(chdir)
 307:	b8 09 00 00 00       	mov    $0x9,%eax
 30c:	cd 40                	int    $0x40
 30e:	c3                   	ret    

0000030f <dup>:
SYSCALL(dup)
 30f:	b8 0a 00 00 00       	mov    $0xa,%eax
 314:	cd 40                	int    $0x40
 316:	c3                   	ret    

00000317 <getpid>:
SYSCALL(getpid)
 317:	b8 0b 00 00 00       	mov    $0xb,%eax
 31c:	cd 40                	int    $0x40
 31e:	c3                   	ret    

0000031f <sbrk>:
SYSCALL(sbrk)
 31f:	b8 0c 00 00 00       	mov    $0xc,%eax
 324:	cd 40                	int    $0x40
 326:	c3                   	ret    

00000327 <sleep>:
SYSCALL(sleep)
 327:	b8 0d 00 00 00       	mov    $0xd,%eax
 32c:	cd 40                	int    $0x40
 32e:	c3                   	ret    

0000032f <uptime>:
SYSCALL(uptime)
 32f:	b8 0e 00 00 00       	mov    $0xe,%eax
 334:	cd 40                	int    $0x40
 336:	c3                   	ret    

00000337 <halt>:
SYSCALL(halt)
 337:	b8 16 00 00 00       	mov    $0x16,%eax
 33c:	cd 40                	int    $0x40
 33e:	c3                   	ret    

0000033f <date>:
SYSCALL(date)
 33f:	b8 17 00 00 00       	mov    $0x17,%eax
 344:	cd 40                	int    $0x40
 346:	c3                   	ret    

00000347 <getuid>:
SYSCALL(getuid)
 347:	b8 18 00 00 00       	mov    $0x18,%eax
 34c:	cd 40                	int    $0x40
 34e:	c3                   	ret    

0000034f <getgid>:
SYSCALL(getgid)
 34f:	b8 19 00 00 00       	mov    $0x19,%eax
 354:	cd 40                	int    $0x40
 356:	c3                   	ret    

00000357 <getppid>:
SYSCALL(getppid)
 357:	b8 1a 00 00 00       	mov    $0x1a,%eax
 35c:	cd 40                	int    $0x40
 35e:	c3                   	ret    

0000035f <setuid>:
SYSCALL(setuid)
 35f:	b8 1b 00 00 00       	mov    $0x1b,%eax
 364:	cd 40                	int    $0x40
 366:	c3                   	ret    

00000367 <setgid>:
SYSCALL(setgid)
 367:	b8 1c 00 00 00       	mov    $0x1c,%eax
 36c:	cd 40                	int    $0x40
 36e:	c3                   	ret    

0000036f <getprocs>:
SYSCALL(getprocs)
 36f:	b8 1d 00 00 00       	mov    $0x1d,%eax
 374:	cd 40                	int    $0x40
 376:	c3                   	ret    

00000377 <setpriority>:
SYSCALL(setpriority)
 377:	b8 1e 00 00 00       	mov    $0x1e,%eax
 37c:	cd 40                	int    $0x40
 37e:	c3                   	ret    

0000037f <putc>:
#include "stat.h"
#include "user.h"

static void
putc(int fd, char c)
{
 37f:	55                   	push   %ebp
 380:	89 e5                	mov    %esp,%ebp
 382:	83 ec 18             	sub    $0x18,%esp
 385:	8b 45 0c             	mov    0xc(%ebp),%eax
 388:	88 45 f4             	mov    %al,-0xc(%ebp)
  write(fd, &c, 1);
 38b:	83 ec 04             	sub    $0x4,%esp
 38e:	6a 01                	push   $0x1
 390:	8d 45 f4             	lea    -0xc(%ebp),%eax
 393:	50                   	push   %eax
 394:	ff 75 08             	pushl  0x8(%ebp)
 397:	e8 1b ff ff ff       	call   2b7 <write>
 39c:	83 c4 10             	add    $0x10,%esp
}
 39f:	90                   	nop
 3a0:	c9                   	leave  
 3a1:	c3                   	ret    

000003a2 <printint>:

static void
printint(int fd, int xx, int base, int sgn)
{
 3a2:	55                   	push   %ebp
 3a3:	89 e5                	mov    %esp,%ebp
 3a5:	53                   	push   %ebx
 3a6:	83 ec 24             	sub    $0x24,%esp
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 3a9:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  if(sgn && xx < 0){
 3b0:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
 3b4:	74 17                	je     3cd <printint+0x2b>
 3b6:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
 3ba:	79 11                	jns    3cd <printint+0x2b>
    neg = 1;
 3bc:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
    x = -xx;
 3c3:	8b 45 0c             	mov    0xc(%ebp),%eax
 3c6:	f7 d8                	neg    %eax
 3c8:	89 45 ec             	mov    %eax,-0x14(%ebp)
 3cb:	eb 06                	jmp    3d3 <printint+0x31>
  } else {
    x = xx;
 3cd:	8b 45 0c             	mov    0xc(%ebp),%eax
 3d0:	89 45 ec             	mov    %eax,-0x14(%ebp)
  }

  i = 0;
 3d3:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  do{
    buf[i++] = digits[x % base];
 3da:	8b 4d f4             	mov    -0xc(%ebp),%ecx
 3dd:	8d 41 01             	lea    0x1(%ecx),%eax
 3e0:	89 45 f4             	mov    %eax,-0xc(%ebp)
 3e3:	8b 5d 10             	mov    0x10(%ebp),%ebx
 3e6:	8b 45 ec             	mov    -0x14(%ebp),%eax
 3e9:	ba 00 00 00 00       	mov    $0x0,%edx
 3ee:	f7 f3                	div    %ebx
 3f0:	89 d0                	mov    %edx,%eax
 3f2:	0f b6 80 b0 0a 00 00 	movzbl 0xab0(%eax),%eax
 3f9:	88 44 0d dc          	mov    %al,-0x24(%ebp,%ecx,1)
  }while((x /= base) != 0);
 3fd:	8b 5d 10             	mov    0x10(%ebp),%ebx
 400:	8b 45 ec             	mov    -0x14(%ebp),%eax
 403:	ba 00 00 00 00       	mov    $0x0,%edx
 408:	f7 f3                	div    %ebx
 40a:	89 45 ec             	mov    %eax,-0x14(%ebp)
 40d:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 411:	75 c7                	jne    3da <printint+0x38>
  if(neg)
 413:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 417:	74 2d                	je     446 <printint+0xa4>
    buf[i++] = '-';
 419:	8b 45 f4             	mov    -0xc(%ebp),%eax
 41c:	8d 50 01             	lea    0x1(%eax),%edx
 41f:	89 55 f4             	mov    %edx,-0xc(%ebp)
 422:	c6 44 05 dc 2d       	movb   $0x2d,-0x24(%ebp,%eax,1)

  while(--i >= 0)
 427:	eb 1d                	jmp    446 <printint+0xa4>
    putc(fd, buf[i]);
 429:	8d 55 dc             	lea    -0x24(%ebp),%edx
 42c:	8b 45 f4             	mov    -0xc(%ebp),%eax
 42f:	01 d0                	add    %edx,%eax
 431:	0f b6 00             	movzbl (%eax),%eax
 434:	0f be c0             	movsbl %al,%eax
 437:	83 ec 08             	sub    $0x8,%esp
 43a:	50                   	push   %eax
 43b:	ff 75 08             	pushl  0x8(%ebp)
 43e:	e8 3c ff ff ff       	call   37f <putc>
 443:	83 c4 10             	add    $0x10,%esp
    buf[i++] = digits[x % base];
  }while((x /= base) != 0);
  if(neg)
    buf[i++] = '-';

  while(--i >= 0)
 446:	83 6d f4 01          	subl   $0x1,-0xc(%ebp)
 44a:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 44e:	79 d9                	jns    429 <printint+0x87>
    putc(fd, buf[i]);
}
 450:	90                   	nop
 451:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 454:	c9                   	leave  
 455:	c3                   	ret    

00000456 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, char *fmt, ...)
{
 456:	55                   	push   %ebp
 457:	89 e5                	mov    %esp,%ebp
 459:	83 ec 28             	sub    $0x28,%esp
  char *s;
  int c, i, state;
  uint *ap;

  state = 0;
 45c:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  ap = (uint*)(void*)&fmt + 1;
 463:	8d 45 0c             	lea    0xc(%ebp),%eax
 466:	83 c0 04             	add    $0x4,%eax
 469:	89 45 e8             	mov    %eax,-0x18(%ebp)
  for(i = 0; fmt[i]; i++){
 46c:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 473:	e9 59 01 00 00       	jmp    5d1 <printf+0x17b>
    c = fmt[i] & 0xff;
 478:	8b 55 0c             	mov    0xc(%ebp),%edx
 47b:	8b 45 f0             	mov    -0x10(%ebp),%eax
 47e:	01 d0                	add    %edx,%eax
 480:	0f b6 00             	movzbl (%eax),%eax
 483:	0f be c0             	movsbl %al,%eax
 486:	25 ff 00 00 00       	and    $0xff,%eax
 48b:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    if(state == 0){
 48e:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 492:	75 2c                	jne    4c0 <printf+0x6a>
      if(c == '%'){
 494:	83 7d e4 25          	cmpl   $0x25,-0x1c(%ebp)
 498:	75 0c                	jne    4a6 <printf+0x50>
        state = '%';
 49a:	c7 45 ec 25 00 00 00 	movl   $0x25,-0x14(%ebp)
 4a1:	e9 27 01 00 00       	jmp    5cd <printf+0x177>
      } else {
        putc(fd, c);
 4a6:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 4a9:	0f be c0             	movsbl %al,%eax
 4ac:	83 ec 08             	sub    $0x8,%esp
 4af:	50                   	push   %eax
 4b0:	ff 75 08             	pushl  0x8(%ebp)
 4b3:	e8 c7 fe ff ff       	call   37f <putc>
 4b8:	83 c4 10             	add    $0x10,%esp
 4bb:	e9 0d 01 00 00       	jmp    5cd <printf+0x177>
      }
    } else if(state == '%'){
 4c0:	83 7d ec 25          	cmpl   $0x25,-0x14(%ebp)
 4c4:	0f 85 03 01 00 00    	jne    5cd <printf+0x177>
      if(c == 'd'){
 4ca:	83 7d e4 64          	cmpl   $0x64,-0x1c(%ebp)
 4ce:	75 1e                	jne    4ee <printf+0x98>
        printint(fd, *ap, 10, 1);
 4d0:	8b 45 e8             	mov    -0x18(%ebp),%eax
 4d3:	8b 00                	mov    (%eax),%eax
 4d5:	6a 01                	push   $0x1
 4d7:	6a 0a                	push   $0xa
 4d9:	50                   	push   %eax
 4da:	ff 75 08             	pushl  0x8(%ebp)
 4dd:	e8 c0 fe ff ff       	call   3a2 <printint>
 4e2:	83 c4 10             	add    $0x10,%esp
        ap++;
 4e5:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 4e9:	e9 d8 00 00 00       	jmp    5c6 <printf+0x170>
      } else if(c == 'x' || c == 'p'){
 4ee:	83 7d e4 78          	cmpl   $0x78,-0x1c(%ebp)
 4f2:	74 06                	je     4fa <printf+0xa4>
 4f4:	83 7d e4 70          	cmpl   $0x70,-0x1c(%ebp)
 4f8:	75 1e                	jne    518 <printf+0xc2>
        printint(fd, *ap, 16, 0);
 4fa:	8b 45 e8             	mov    -0x18(%ebp),%eax
 4fd:	8b 00                	mov    (%eax),%eax
 4ff:	6a 00                	push   $0x0
 501:	6a 10                	push   $0x10
 503:	50                   	push   %eax
 504:	ff 75 08             	pushl  0x8(%ebp)
 507:	e8 96 fe ff ff       	call   3a2 <printint>
 50c:	83 c4 10             	add    $0x10,%esp
        ap++;
 50f:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 513:	e9 ae 00 00 00       	jmp    5c6 <printf+0x170>
      } else if(c == 's'){
 518:	83 7d e4 73          	cmpl   $0x73,-0x1c(%ebp)
 51c:	75 43                	jne    561 <printf+0x10b>
        s = (char*)*ap;
 51e:	8b 45 e8             	mov    -0x18(%ebp),%eax
 521:	8b 00                	mov    (%eax),%eax
 523:	89 45 f4             	mov    %eax,-0xc(%ebp)
        ap++;
 526:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
        if(s == 0)
 52a:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 52e:	75 25                	jne    555 <printf+0xff>
          s = "(null)";
 530:	c7 45 f4 57 08 00 00 	movl   $0x857,-0xc(%ebp)
        while(*s != 0){
 537:	eb 1c                	jmp    555 <printf+0xff>
          putc(fd, *s);
 539:	8b 45 f4             	mov    -0xc(%ebp),%eax
 53c:	0f b6 00             	movzbl (%eax),%eax
 53f:	0f be c0             	movsbl %al,%eax
 542:	83 ec 08             	sub    $0x8,%esp
 545:	50                   	push   %eax
 546:	ff 75 08             	pushl  0x8(%ebp)
 549:	e8 31 fe ff ff       	call   37f <putc>
 54e:	83 c4 10             	add    $0x10,%esp
          s++;
 551:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
      } else if(c == 's'){
        s = (char*)*ap;
        ap++;
        if(s == 0)
          s = "(null)";
        while(*s != 0){
 555:	8b 45 f4             	mov    -0xc(%ebp),%eax
 558:	0f b6 00             	movzbl (%eax),%eax
 55b:	84 c0                	test   %al,%al
 55d:	75 da                	jne    539 <printf+0xe3>
 55f:	eb 65                	jmp    5c6 <printf+0x170>
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 561:	83 7d e4 63          	cmpl   $0x63,-0x1c(%ebp)
 565:	75 1d                	jne    584 <printf+0x12e>
        putc(fd, *ap);
 567:	8b 45 e8             	mov    -0x18(%ebp),%eax
 56a:	8b 00                	mov    (%eax),%eax
 56c:	0f be c0             	movsbl %al,%eax
 56f:	83 ec 08             	sub    $0x8,%esp
 572:	50                   	push   %eax
 573:	ff 75 08             	pushl  0x8(%ebp)
 576:	e8 04 fe ff ff       	call   37f <putc>
 57b:	83 c4 10             	add    $0x10,%esp
        ap++;
 57e:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 582:	eb 42                	jmp    5c6 <printf+0x170>
      } else if(c == '%'){
 584:	83 7d e4 25          	cmpl   $0x25,-0x1c(%ebp)
 588:	75 17                	jne    5a1 <printf+0x14b>
        putc(fd, c);
 58a:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 58d:	0f be c0             	movsbl %al,%eax
 590:	83 ec 08             	sub    $0x8,%esp
 593:	50                   	push   %eax
 594:	ff 75 08             	pushl  0x8(%ebp)
 597:	e8 e3 fd ff ff       	call   37f <putc>
 59c:	83 c4 10             	add    $0x10,%esp
 59f:	eb 25                	jmp    5c6 <printf+0x170>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 5a1:	83 ec 08             	sub    $0x8,%esp
 5a4:	6a 25                	push   $0x25
 5a6:	ff 75 08             	pushl  0x8(%ebp)
 5a9:	e8 d1 fd ff ff       	call   37f <putc>
 5ae:	83 c4 10             	add    $0x10,%esp
        putc(fd, c);
 5b1:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 5b4:	0f be c0             	movsbl %al,%eax
 5b7:	83 ec 08             	sub    $0x8,%esp
 5ba:	50                   	push   %eax
 5bb:	ff 75 08             	pushl  0x8(%ebp)
 5be:	e8 bc fd ff ff       	call   37f <putc>
 5c3:	83 c4 10             	add    $0x10,%esp
      }
      state = 0;
 5c6:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
 5cd:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
 5d1:	8b 55 0c             	mov    0xc(%ebp),%edx
 5d4:	8b 45 f0             	mov    -0x10(%ebp),%eax
 5d7:	01 d0                	add    %edx,%eax
 5d9:	0f b6 00             	movzbl (%eax),%eax
 5dc:	84 c0                	test   %al,%al
 5de:	0f 85 94 fe ff ff    	jne    478 <printf+0x22>
        putc(fd, c);
      }
      state = 0;
    }
  }
}
 5e4:	90                   	nop
 5e5:	c9                   	leave  
 5e6:	c3                   	ret    

000005e7 <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 5e7:	55                   	push   %ebp
 5e8:	89 e5                	mov    %esp,%ebp
 5ea:	83 ec 10             	sub    $0x10,%esp
  Header *bp, *p;

  bp = (Header*)ap - 1;
 5ed:	8b 45 08             	mov    0x8(%ebp),%eax
 5f0:	83 e8 08             	sub    $0x8,%eax
 5f3:	89 45 f8             	mov    %eax,-0x8(%ebp)
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 5f6:	a1 cc 0a 00 00       	mov    0xacc,%eax
 5fb:	89 45 fc             	mov    %eax,-0x4(%ebp)
 5fe:	eb 24                	jmp    624 <free+0x3d>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 600:	8b 45 fc             	mov    -0x4(%ebp),%eax
 603:	8b 00                	mov    (%eax),%eax
 605:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 608:	77 12                	ja     61c <free+0x35>
 60a:	8b 45 f8             	mov    -0x8(%ebp),%eax
 60d:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 610:	77 24                	ja     636 <free+0x4f>
 612:	8b 45 fc             	mov    -0x4(%ebp),%eax
 615:	8b 00                	mov    (%eax),%eax
 617:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 61a:	77 1a                	ja     636 <free+0x4f>
free(void *ap)
{
  Header *bp, *p;

  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 61c:	8b 45 fc             	mov    -0x4(%ebp),%eax
 61f:	8b 00                	mov    (%eax),%eax
 621:	89 45 fc             	mov    %eax,-0x4(%ebp)
 624:	8b 45 f8             	mov    -0x8(%ebp),%eax
 627:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 62a:	76 d4                	jbe    600 <free+0x19>
 62c:	8b 45 fc             	mov    -0x4(%ebp),%eax
 62f:	8b 00                	mov    (%eax),%eax
 631:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 634:	76 ca                	jbe    600 <free+0x19>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if(bp + bp->s.size == p->s.ptr){
 636:	8b 45 f8             	mov    -0x8(%ebp),%eax
 639:	8b 40 04             	mov    0x4(%eax),%eax
 63c:	8d 14 c5 00 00 00 00 	lea    0x0(,%eax,8),%edx
 643:	8b 45 f8             	mov    -0x8(%ebp),%eax
 646:	01 c2                	add    %eax,%edx
 648:	8b 45 fc             	mov    -0x4(%ebp),%eax
 64b:	8b 00                	mov    (%eax),%eax
 64d:	39 c2                	cmp    %eax,%edx
 64f:	75 24                	jne    675 <free+0x8e>
    bp->s.size += p->s.ptr->s.size;
 651:	8b 45 f8             	mov    -0x8(%ebp),%eax
 654:	8b 50 04             	mov    0x4(%eax),%edx
 657:	8b 45 fc             	mov    -0x4(%ebp),%eax
 65a:	8b 00                	mov    (%eax),%eax
 65c:	8b 40 04             	mov    0x4(%eax),%eax
 65f:	01 c2                	add    %eax,%edx
 661:	8b 45 f8             	mov    -0x8(%ebp),%eax
 664:	89 50 04             	mov    %edx,0x4(%eax)
    bp->s.ptr = p->s.ptr->s.ptr;
 667:	8b 45 fc             	mov    -0x4(%ebp),%eax
 66a:	8b 00                	mov    (%eax),%eax
 66c:	8b 10                	mov    (%eax),%edx
 66e:	8b 45 f8             	mov    -0x8(%ebp),%eax
 671:	89 10                	mov    %edx,(%eax)
 673:	eb 0a                	jmp    67f <free+0x98>
  } else
    bp->s.ptr = p->s.ptr;
 675:	8b 45 fc             	mov    -0x4(%ebp),%eax
 678:	8b 10                	mov    (%eax),%edx
 67a:	8b 45 f8             	mov    -0x8(%ebp),%eax
 67d:	89 10                	mov    %edx,(%eax)
  if(p + p->s.size == bp){
 67f:	8b 45 fc             	mov    -0x4(%ebp),%eax
 682:	8b 40 04             	mov    0x4(%eax),%eax
 685:	8d 14 c5 00 00 00 00 	lea    0x0(,%eax,8),%edx
 68c:	8b 45 fc             	mov    -0x4(%ebp),%eax
 68f:	01 d0                	add    %edx,%eax
 691:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 694:	75 20                	jne    6b6 <free+0xcf>
    p->s.size += bp->s.size;
 696:	8b 45 fc             	mov    -0x4(%ebp),%eax
 699:	8b 50 04             	mov    0x4(%eax),%edx
 69c:	8b 45 f8             	mov    -0x8(%ebp),%eax
 69f:	8b 40 04             	mov    0x4(%eax),%eax
 6a2:	01 c2                	add    %eax,%edx
 6a4:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6a7:	89 50 04             	mov    %edx,0x4(%eax)
    p->s.ptr = bp->s.ptr;
 6aa:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6ad:	8b 10                	mov    (%eax),%edx
 6af:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6b2:	89 10                	mov    %edx,(%eax)
 6b4:	eb 08                	jmp    6be <free+0xd7>
  } else
    p->s.ptr = bp;
 6b6:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6b9:	8b 55 f8             	mov    -0x8(%ebp),%edx
 6bc:	89 10                	mov    %edx,(%eax)
  freep = p;
 6be:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6c1:	a3 cc 0a 00 00       	mov    %eax,0xacc
}
 6c6:	90                   	nop
 6c7:	c9                   	leave  
 6c8:	c3                   	ret    

000006c9 <morecore>:

static Header*
morecore(uint nu)
{
 6c9:	55                   	push   %ebp
 6ca:	89 e5                	mov    %esp,%ebp
 6cc:	83 ec 18             	sub    $0x18,%esp
  char *p;
  Header *hp;

  if(nu < 4096)
 6cf:	81 7d 08 ff 0f 00 00 	cmpl   $0xfff,0x8(%ebp)
 6d6:	77 07                	ja     6df <morecore+0x16>
    nu = 4096;
 6d8:	c7 45 08 00 10 00 00 	movl   $0x1000,0x8(%ebp)
  p = sbrk(nu * sizeof(Header));
 6df:	8b 45 08             	mov    0x8(%ebp),%eax
 6e2:	c1 e0 03             	shl    $0x3,%eax
 6e5:	83 ec 0c             	sub    $0xc,%esp
 6e8:	50                   	push   %eax
 6e9:	e8 31 fc ff ff       	call   31f <sbrk>
 6ee:	83 c4 10             	add    $0x10,%esp
 6f1:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(p == (char*)-1)
 6f4:	83 7d f4 ff          	cmpl   $0xffffffff,-0xc(%ebp)
 6f8:	75 07                	jne    701 <morecore+0x38>
    return 0;
 6fa:	b8 00 00 00 00       	mov    $0x0,%eax
 6ff:	eb 26                	jmp    727 <morecore+0x5e>
  hp = (Header*)p;
 701:	8b 45 f4             	mov    -0xc(%ebp),%eax
 704:	89 45 f0             	mov    %eax,-0x10(%ebp)
  hp->s.size = nu;
 707:	8b 45 f0             	mov    -0x10(%ebp),%eax
 70a:	8b 55 08             	mov    0x8(%ebp),%edx
 70d:	89 50 04             	mov    %edx,0x4(%eax)
  free((void*)(hp + 1));
 710:	8b 45 f0             	mov    -0x10(%ebp),%eax
 713:	83 c0 08             	add    $0x8,%eax
 716:	83 ec 0c             	sub    $0xc,%esp
 719:	50                   	push   %eax
 71a:	e8 c8 fe ff ff       	call   5e7 <free>
 71f:	83 c4 10             	add    $0x10,%esp
  return freep;
 722:	a1 cc 0a 00 00       	mov    0xacc,%eax
}
 727:	c9                   	leave  
 728:	c3                   	ret    

00000729 <malloc>:

void*
malloc(uint nbytes)
{
 729:	55                   	push   %ebp
 72a:	89 e5                	mov    %esp,%ebp
 72c:	83 ec 18             	sub    $0x18,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 72f:	8b 45 08             	mov    0x8(%ebp),%eax
 732:	83 c0 07             	add    $0x7,%eax
 735:	c1 e8 03             	shr    $0x3,%eax
 738:	83 c0 01             	add    $0x1,%eax
 73b:	89 45 ec             	mov    %eax,-0x14(%ebp)
  if((prevp = freep) == 0){
 73e:	a1 cc 0a 00 00       	mov    0xacc,%eax
 743:	89 45 f0             	mov    %eax,-0x10(%ebp)
 746:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 74a:	75 23                	jne    76f <malloc+0x46>
    base.s.ptr = freep = prevp = &base;
 74c:	c7 45 f0 c4 0a 00 00 	movl   $0xac4,-0x10(%ebp)
 753:	8b 45 f0             	mov    -0x10(%ebp),%eax
 756:	a3 cc 0a 00 00       	mov    %eax,0xacc
 75b:	a1 cc 0a 00 00       	mov    0xacc,%eax
 760:	a3 c4 0a 00 00       	mov    %eax,0xac4
    base.s.size = 0;
 765:	c7 05 c8 0a 00 00 00 	movl   $0x0,0xac8
 76c:	00 00 00 
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 76f:	8b 45 f0             	mov    -0x10(%ebp),%eax
 772:	8b 00                	mov    (%eax),%eax
 774:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if(p->s.size >= nunits){
 777:	8b 45 f4             	mov    -0xc(%ebp),%eax
 77a:	8b 40 04             	mov    0x4(%eax),%eax
 77d:	3b 45 ec             	cmp    -0x14(%ebp),%eax
 780:	72 4d                	jb     7cf <malloc+0xa6>
      if(p->s.size == nunits)
 782:	8b 45 f4             	mov    -0xc(%ebp),%eax
 785:	8b 40 04             	mov    0x4(%eax),%eax
 788:	3b 45 ec             	cmp    -0x14(%ebp),%eax
 78b:	75 0c                	jne    799 <malloc+0x70>
        prevp->s.ptr = p->s.ptr;
 78d:	8b 45 f4             	mov    -0xc(%ebp),%eax
 790:	8b 10                	mov    (%eax),%edx
 792:	8b 45 f0             	mov    -0x10(%ebp),%eax
 795:	89 10                	mov    %edx,(%eax)
 797:	eb 26                	jmp    7bf <malloc+0x96>
      else {
        p->s.size -= nunits;
 799:	8b 45 f4             	mov    -0xc(%ebp),%eax
 79c:	8b 40 04             	mov    0x4(%eax),%eax
 79f:	2b 45 ec             	sub    -0x14(%ebp),%eax
 7a2:	89 c2                	mov    %eax,%edx
 7a4:	8b 45 f4             	mov    -0xc(%ebp),%eax
 7a7:	89 50 04             	mov    %edx,0x4(%eax)
        p += p->s.size;
 7aa:	8b 45 f4             	mov    -0xc(%ebp),%eax
 7ad:	8b 40 04             	mov    0x4(%eax),%eax
 7b0:	c1 e0 03             	shl    $0x3,%eax
 7b3:	01 45 f4             	add    %eax,-0xc(%ebp)
        p->s.size = nunits;
 7b6:	8b 45 f4             	mov    -0xc(%ebp),%eax
 7b9:	8b 55 ec             	mov    -0x14(%ebp),%edx
 7bc:	89 50 04             	mov    %edx,0x4(%eax)
      }
      freep = prevp;
 7bf:	8b 45 f0             	mov    -0x10(%ebp),%eax
 7c2:	a3 cc 0a 00 00       	mov    %eax,0xacc
      return (void*)(p + 1);
 7c7:	8b 45 f4             	mov    -0xc(%ebp),%eax
 7ca:	83 c0 08             	add    $0x8,%eax
 7cd:	eb 3b                	jmp    80a <malloc+0xe1>
    }
    if(p == freep)
 7cf:	a1 cc 0a 00 00       	mov    0xacc,%eax
 7d4:	39 45 f4             	cmp    %eax,-0xc(%ebp)
 7d7:	75 1e                	jne    7f7 <malloc+0xce>
      if((p = morecore(nunits)) == 0)
 7d9:	83 ec 0c             	sub    $0xc,%esp
 7dc:	ff 75 ec             	pushl  -0x14(%ebp)
 7df:	e8 e5 fe ff ff       	call   6c9 <morecore>
 7e4:	83 c4 10             	add    $0x10,%esp
 7e7:	89 45 f4             	mov    %eax,-0xc(%ebp)
 7ea:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 7ee:	75 07                	jne    7f7 <malloc+0xce>
        return 0;
 7f0:	b8 00 00 00 00       	mov    $0x0,%eax
 7f5:	eb 13                	jmp    80a <malloc+0xe1>
  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
  if((prevp = freep) == 0){
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 7f7:	8b 45 f4             	mov    -0xc(%ebp),%eax
 7fa:	89 45 f0             	mov    %eax,-0x10(%ebp)
 7fd:	8b 45 f4             	mov    -0xc(%ebp),%eax
 800:	8b 00                	mov    (%eax),%eax
 802:	89 45 f4             	mov    %eax,-0xc(%ebp)
      return (void*)(p + 1);
    }
    if(p == freep)
      if((p = morecore(nunits)) == 0)
        return 0;
  }
 805:	e9 6d ff ff ff       	jmp    777 <malloc+0x4e>
}
 80a:	c9                   	leave  
 80b:	c3                   	ret    
