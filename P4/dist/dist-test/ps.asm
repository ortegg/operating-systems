
_ps:     file format elf32-i386


Disassembly of section .text:

00000000 <printTableEntry>:
int MAXCOUNT = 4;

// Helper function to print a page table entry
void
printTableEntry(struct uproc* t)
{
   0:	55                   	push   %ebp
   1:	89 e5                	mov    %esp,%ebp
   3:	57                   	push   %edi
   4:	56                   	push   %esi
   5:	53                   	push   %ebx
   6:	83 ec 2c             	sub    $0x2c,%esp
  int i, len;
  static int MAXNAME = 12;  // only print up to this many chars of name

  len = strlen(t->name);
   9:	8b 45 08             	mov    0x8(%ebp),%eax
   c:	83 c0 40             	add    $0x40,%eax
   f:	83 ec 0c             	sub    $0xc,%esp
  12:	50                   	push   %eax
  13:	e8 d3 02 00 00       	call   2eb <strlen>
  18:	83 c4 10             	add    $0x10,%esp
  1b:	89 45 e0             	mov    %eax,-0x20(%ebp)
  if (len > MAXNAME) {
  1e:	a1 8c 0d 00 00       	mov    0xd8c,%eax
  23:	39 45 e0             	cmp    %eax,-0x20(%ebp)
  26:	7e 15                	jle    3d <printTableEntry+0x3d>
    t->name[MAXNAME] = '\0';
  28:	a1 8c 0d 00 00       	mov    0xd8c,%eax
  2d:	8b 55 08             	mov    0x8(%ebp),%edx
  30:	c6 44 02 40 00       	movb   $0x0,0x40(%edx,%eax,1)
    len = MAXNAME;
  35:	a1 8c 0d 00 00       	mov    0xd8c,%eax
  3a:	89 45 e0             	mov    %eax,-0x20(%ebp)
  }
  printf(1, "%d\t%s", t->pid, t->name);
  3d:	8b 45 08             	mov    0x8(%ebp),%eax
  40:	8d 50 40             	lea    0x40(%eax),%edx
  43:	8b 45 08             	mov    0x8(%ebp),%eax
  46:	8b 00                	mov    (%eax),%eax
  48:	52                   	push   %edx
  49:	50                   	push   %eax
  4a:	68 24 0a 00 00       	push   $0xa24
  4f:	6a 01                	push   $0x1
  51:	e8 16 06 00 00       	call   66c <printf>
  56:	83 c4 10             	add    $0x10,%esp
  for (i=len; i<=MAXNAME; i++) printf(1, " ");
  59:	8b 45 e0             	mov    -0x20(%ebp),%eax
  5c:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  5f:	eb 16                	jmp    77 <printTableEntry+0x77>
  61:	83 ec 08             	sub    $0x8,%esp
  64:	68 2a 0a 00 00       	push   $0xa2a
  69:	6a 01                	push   $0x1
  6b:	e8 fc 05 00 00       	call   66c <printf>
  70:	83 c4 10             	add    $0x10,%esp
  73:	83 45 e4 01          	addl   $0x1,-0x1c(%ebp)
  77:	a1 8c 0d 00 00       	mov    0xd8c,%eax
  7c:	39 45 e4             	cmp    %eax,-0x1c(%ebp)
  7f:	7e e0                	jle    61 <printTableEntry+0x61>
  printf(1, "%d", t->uid);
  81:	8b 45 08             	mov    0x8(%ebp),%eax
  84:	8b 40 04             	mov    0x4(%eax),%eax
  87:	83 ec 04             	sub    $0x4,%esp
  8a:	50                   	push   %eax
  8b:	68 2c 0a 00 00       	push   $0xa2c
  90:	6a 01                	push   $0x1
  92:	e8 d5 05 00 00       	call   66c <printf>
  97:	83 c4 10             	add    $0x10,%esp
  if (t->uid <100) printf(1, "\t\t");
  9a:	8b 45 08             	mov    0x8(%ebp),%eax
  9d:	8b 40 04             	mov    0x4(%eax),%eax
  a0:	83 f8 63             	cmp    $0x63,%eax
  a3:	77 14                	ja     b9 <printTableEntry+0xb9>
  a5:	83 ec 08             	sub    $0x8,%esp
  a8:	68 2f 0a 00 00       	push   $0xa2f
  ad:	6a 01                	push   $0x1
  af:	e8 b8 05 00 00       	call   66c <printf>
  b4:	83 c4 10             	add    $0x10,%esp
  b7:	eb 12                	jmp    cb <printTableEntry+0xcb>
  else printf(1, "\t");
  b9:	83 ec 08             	sub    $0x8,%esp
  bc:	68 32 0a 00 00       	push   $0xa32
  c1:	6a 01                	push   $0x1
  c3:	e8 a4 05 00 00       	call   66c <printf>
  c8:	83 c4 10             	add    $0x10,%esp
  printf(1, "%d\t%d\t%d\t%d.%d\t%d.%d\t%s\t%d\n",
  cb:	8b 45 08             	mov    0x8(%ebp),%eax
  ce:	8b 78 3c             	mov    0x3c(%eax),%edi
	t->gid, t->ppid, t->priority, (t->elapsed_ticks/100),
	 (t->elapsed_ticks%100), (t->CPU_total_ticks/100), (t->CPU_total_ticks%100),
	 t->state, t->size);
  d1:	8b 45 08             	mov    0x8(%ebp),%eax
  d4:	83 c0 1c             	add    $0x1c,%eax
  d7:	89 45 d4             	mov    %eax,-0x2c(%ebp)
  printf(1, "%d", t->uid);
  if (t->uid <100) printf(1, "\t\t");
  else printf(1, "\t");
  printf(1, "%d\t%d\t%d\t%d.%d\t%d.%d\t%s\t%d\n",
	t->gid, t->ppid, t->priority, (t->elapsed_ticks/100),
	 (t->elapsed_ticks%100), (t->CPU_total_ticks/100), (t->CPU_total_ticks%100),
  da:	8b 45 08             	mov    0x8(%ebp),%eax
  dd:	8b 48 14             	mov    0x14(%eax),%ecx
  printf(1, "%d\t%s", t->pid, t->name);
  for (i=len; i<=MAXNAME; i++) printf(1, " ");
  printf(1, "%d", t->uid);
  if (t->uid <100) printf(1, "\t\t");
  else printf(1, "\t");
  printf(1, "%d\t%d\t%d\t%d.%d\t%d.%d\t%s\t%d\n",
  e0:	ba 1f 85 eb 51       	mov    $0x51eb851f,%edx
  e5:	89 c8                	mov    %ecx,%eax
  e7:	f7 e2                	mul    %edx
  e9:	89 d6                	mov    %edx,%esi
  eb:	c1 ee 05             	shr    $0x5,%esi
  ee:	6b c6 64             	imul   $0x64,%esi,%eax
  f1:	89 ce                	mov    %ecx,%esi
  f3:	29 c6                	sub    %eax,%esi
	t->gid, t->ppid, t->priority, (t->elapsed_ticks/100),
	 (t->elapsed_ticks%100), (t->CPU_total_ticks/100), (t->CPU_total_ticks%100),
  f5:	8b 45 08             	mov    0x8(%ebp),%eax
  f8:	8b 40 14             	mov    0x14(%eax),%eax
  printf(1, "%d\t%s", t->pid, t->name);
  for (i=len; i<=MAXNAME; i++) printf(1, " ");
  printf(1, "%d", t->uid);
  if (t->uid <100) printf(1, "\t\t");
  else printf(1, "\t");
  printf(1, "%d\t%d\t%d\t%d.%d\t%d.%d\t%s\t%d\n",
  fb:	ba 1f 85 eb 51       	mov    $0x51eb851f,%edx
 100:	f7 e2                	mul    %edx
 102:	c1 ea 05             	shr    $0x5,%edx
 105:	89 55 d0             	mov    %edx,-0x30(%ebp)
	t->gid, t->ppid, t->priority, (t->elapsed_ticks/100),
	 (t->elapsed_ticks%100), (t->CPU_total_ticks/100), (t->CPU_total_ticks%100),
 108:	8b 45 08             	mov    0x8(%ebp),%eax
 10b:	8b 48 10             	mov    0x10(%eax),%ecx
  printf(1, "%d\t%s", t->pid, t->name);
  for (i=len; i<=MAXNAME; i++) printf(1, " ");
  printf(1, "%d", t->uid);
  if (t->uid <100) printf(1, "\t\t");
  else printf(1, "\t");
  printf(1, "%d\t%d\t%d\t%d.%d\t%d.%d\t%s\t%d\n",
 10e:	ba 1f 85 eb 51       	mov    $0x51eb851f,%edx
 113:	89 c8                	mov    %ecx,%eax
 115:	f7 e2                	mul    %edx
 117:	89 d3                	mov    %edx,%ebx
 119:	c1 eb 05             	shr    $0x5,%ebx
 11c:	6b c3 64             	imul   $0x64,%ebx,%eax
 11f:	89 cb                	mov    %ecx,%ebx
 121:	29 c3                	sub    %eax,%ebx
	t->gid, t->ppid, t->priority, (t->elapsed_ticks/100),
 123:	8b 45 08             	mov    0x8(%ebp),%eax
 126:	8b 40 10             	mov    0x10(%eax),%eax
  printf(1, "%d\t%s", t->pid, t->name);
  for (i=len; i<=MAXNAME; i++) printf(1, " ");
  printf(1, "%d", t->uid);
  if (t->uid <100) printf(1, "\t\t");
  else printf(1, "\t");
  printf(1, "%d\t%d\t%d\t%d.%d\t%d.%d\t%s\t%d\n",
 129:	ba 1f 85 eb 51       	mov    $0x51eb851f,%edx
 12e:	f7 e2                	mul    %edx
 130:	89 d0                	mov    %edx,%eax
 132:	c1 e8 05             	shr    $0x5,%eax
 135:	89 45 cc             	mov    %eax,-0x34(%ebp)
 138:	8b 45 08             	mov    0x8(%ebp),%eax
 13b:	8b 48 18             	mov    0x18(%eax),%ecx
 13e:	8b 45 08             	mov    0x8(%ebp),%eax
 141:	8b 50 0c             	mov    0xc(%eax),%edx
 144:	8b 45 08             	mov    0x8(%ebp),%eax
 147:	8b 40 08             	mov    0x8(%eax),%eax
 14a:	83 ec 04             	sub    $0x4,%esp
 14d:	57                   	push   %edi
 14e:	ff 75 d4             	pushl  -0x2c(%ebp)
 151:	56                   	push   %esi
 152:	ff 75 d0             	pushl  -0x30(%ebp)
 155:	53                   	push   %ebx
 156:	ff 75 cc             	pushl  -0x34(%ebp)
 159:	51                   	push   %ecx
 15a:	52                   	push   %edx
 15b:	50                   	push   %eax
 15c:	68 34 0a 00 00       	push   $0xa34
 161:	6a 01                	push   $0x1
 163:	e8 04 05 00 00       	call   66c <printf>
 168:	83 c4 30             	add    $0x30,%esp
	t->gid, t->ppid, t->priority, (t->elapsed_ticks/100),
	 (t->elapsed_ticks%100), (t->CPU_total_ticks/100), (t->CPU_total_ticks%100),
	 t->state, t->size);
  return;
 16b:	90                   	nop
}
 16c:	8d 65 f4             	lea    -0xc(%ebp),%esp
 16f:	5b                   	pop    %ebx
 170:	5e                   	pop    %esi
 171:	5f                   	pop    %edi
 172:	5d                   	pop    %ebp
 173:	c3                   	ret    

00000174 <main>:


int
main(int argc, char* argv[]) {
 174:	8d 4c 24 04          	lea    0x4(%esp),%ecx
 178:	83 e4 f0             	and    $0xfffffff0,%esp
 17b:	ff 71 fc             	pushl  -0x4(%ecx)
 17e:	55                   	push   %ebp
 17f:	89 e5                	mov    %esp,%ebp
 181:	51                   	push   %ecx
 182:	83 ec 14             	sub    $0x14,%esp
  int MAX = 64;  // just happens to be the same as NPROC
 185:	c7 45 f0 40 00 00 00 	movl   $0x40,-0x10(%ebp)
  int i, rc;
  struct uproc *table;

  table = malloc(MAX * sizeof(struct uproc)); //malloc the table
 18c:	8b 55 f0             	mov    -0x10(%ebp),%edx
 18f:	89 d0                	mov    %edx,%eax
 191:	01 c0                	add    %eax,%eax
 193:	01 d0                	add    %edx,%eax
 195:	c1 e0 05             	shl    $0x5,%eax
 198:	83 ec 0c             	sub    $0xc,%esp
 19b:	50                   	push   %eax
 19c:	e8 9e 07 00 00       	call   93f <malloc>
 1a1:	83 c4 10             	add    $0x10,%esp
 1a4:	89 45 ec             	mov    %eax,-0x14(%ebp)
  if (!table) { //error check malloc
 1a7:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 1ab:	75 1b                	jne    1c8 <main+0x54>
    printf(2, "Error: malloc call failed. %s at line %d\n", __FILE__, __LINE__);
 1ad:	6a 29                	push   $0x29
 1af:	68 50 0a 00 00       	push   $0xa50
 1b4:	68 58 0a 00 00       	push   $0xa58
 1b9:	6a 02                	push   $0x2
 1bb:	e8 ac 04 00 00       	call   66c <printf>
 1c0:	83 c4 10             	add    $0x10,%esp
    exit();
 1c3:	e8 e5 02 00 00       	call   4ad <exit>
  }
  rc = getprocs(MAX, table); //calling the systemcall
 1c8:	83 ec 08             	sub    $0x8,%esp
 1cb:	ff 75 ec             	pushl  -0x14(%ebp)
 1ce:	ff 75 f0             	pushl  -0x10(%ebp)
 1d1:	e8 af 03 00 00       	call   585 <getprocs>
 1d6:	83 c4 10             	add    $0x10,%esp
 1d9:	89 45 e8             	mov    %eax,-0x18(%ebp)
  if (rc < 0) { //error check system call
 1dc:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
 1e0:	79 1b                	jns    1fd <main+0x89>
    printf(1, "Error: getprocs call failed. %s at line %d\n", __FILE__, __LINE__);
 1e2:	6a 2e                	push   $0x2e
 1e4:	68 50 0a 00 00       	push   $0xa50
 1e9:	68 84 0a 00 00       	push   $0xa84
 1ee:	6a 01                	push   $0x1
 1f0:	e8 77 04 00 00       	call   66c <printf>
 1f5:	83 c4 10             	add    $0x10,%esp
    exit();
 1f8:	e8 b0 02 00 00       	call   4ad <exit>
  }
  printf(1, "\nPID\tName        UID        GID     PPID\tPrio\tElapsed\tCPU\tState\tSize\n");
 1fd:	83 ec 08             	sub    $0x8,%esp
 200:	68 b0 0a 00 00       	push   $0xab0
 205:	6a 01                	push   $0x1
 207:	e8 60 04 00 00       	call   66c <printf>
 20c:	83 c4 10             	add    $0x10,%esp
  for (i=0; i<rc; i++)
 20f:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 216:	eb 23                	jmp    23b <main+0xc7>
    printTableEntry(&table[i]); //print each page table entry
 218:	8b 55 f4             	mov    -0xc(%ebp),%edx
 21b:	89 d0                	mov    %edx,%eax
 21d:	01 c0                	add    %eax,%eax
 21f:	01 d0                	add    %edx,%eax
 221:	c1 e0 05             	shl    $0x5,%eax
 224:	89 c2                	mov    %eax,%edx
 226:	8b 45 ec             	mov    -0x14(%ebp),%eax
 229:	01 d0                	add    %edx,%eax
 22b:	83 ec 0c             	sub    $0xc,%esp
 22e:	50                   	push   %eax
 22f:	e8 cc fd ff ff       	call   0 <printTableEntry>
 234:	83 c4 10             	add    $0x10,%esp
  if (rc < 0) { //error check system call
    printf(1, "Error: getprocs call failed. %s at line %d\n", __FILE__, __LINE__);
    exit();
  }
  printf(1, "\nPID\tName        UID        GID     PPID\tPrio\tElapsed\tCPU\tState\tSize\n");
  for (i=0; i<rc; i++)
 237:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
 23b:	8b 45 f4             	mov    -0xc(%ebp),%eax
 23e:	3b 45 e8             	cmp    -0x18(%ebp),%eax
 241:	7c d5                	jl     218 <main+0xa4>
    printTableEntry(&table[i]); //print each page table entry
  free(table); //release the memory for the table
 243:	83 ec 0c             	sub    $0xc,%esp
 246:	ff 75 ec             	pushl  -0x14(%ebp)
 249:	e8 af 05 00 00       	call   7fd <free>
 24e:	83 c4 10             	add    $0x10,%esp

  exit();
 251:	e8 57 02 00 00       	call   4ad <exit>

00000256 <stosb>:
               "cc");
}

static inline void
stosb(void *addr, int data, int cnt)
{
 256:	55                   	push   %ebp
 257:	89 e5                	mov    %esp,%ebp
 259:	57                   	push   %edi
 25a:	53                   	push   %ebx
  asm volatile("cld; rep stosb" :
 25b:	8b 4d 08             	mov    0x8(%ebp),%ecx
 25e:	8b 55 10             	mov    0x10(%ebp),%edx
 261:	8b 45 0c             	mov    0xc(%ebp),%eax
 264:	89 cb                	mov    %ecx,%ebx
 266:	89 df                	mov    %ebx,%edi
 268:	89 d1                	mov    %edx,%ecx
 26a:	fc                   	cld    
 26b:	f3 aa                	rep stos %al,%es:(%edi)
 26d:	89 ca                	mov    %ecx,%edx
 26f:	89 fb                	mov    %edi,%ebx
 271:	89 5d 08             	mov    %ebx,0x8(%ebp)
 274:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
 277:	90                   	nop
 278:	5b                   	pop    %ebx
 279:	5f                   	pop    %edi
 27a:	5d                   	pop    %ebp
 27b:	c3                   	ret    

0000027c <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, char *t)
{
 27c:	55                   	push   %ebp
 27d:	89 e5                	mov    %esp,%ebp
 27f:	83 ec 10             	sub    $0x10,%esp
  char *os;

  os = s;
 282:	8b 45 08             	mov    0x8(%ebp),%eax
 285:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while((*s++ = *t++) != 0)
 288:	90                   	nop
 289:	8b 45 08             	mov    0x8(%ebp),%eax
 28c:	8d 50 01             	lea    0x1(%eax),%edx
 28f:	89 55 08             	mov    %edx,0x8(%ebp)
 292:	8b 55 0c             	mov    0xc(%ebp),%edx
 295:	8d 4a 01             	lea    0x1(%edx),%ecx
 298:	89 4d 0c             	mov    %ecx,0xc(%ebp)
 29b:	0f b6 12             	movzbl (%edx),%edx
 29e:	88 10                	mov    %dl,(%eax)
 2a0:	0f b6 00             	movzbl (%eax),%eax
 2a3:	84 c0                	test   %al,%al
 2a5:	75 e2                	jne    289 <strcpy+0xd>
    ;
  return os;
 2a7:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 2aa:	c9                   	leave  
 2ab:	c3                   	ret    

000002ac <strcmp>:

int
strcmp(const char *p, const char *q)
{
 2ac:	55                   	push   %ebp
 2ad:	89 e5                	mov    %esp,%ebp
  while(*p && *p == *q)
 2af:	eb 08                	jmp    2b9 <strcmp+0xd>
    p++, q++;
 2b1:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 2b5:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
  while(*p && *p == *q)
 2b9:	8b 45 08             	mov    0x8(%ebp),%eax
 2bc:	0f b6 00             	movzbl (%eax),%eax
 2bf:	84 c0                	test   %al,%al
 2c1:	74 10                	je     2d3 <strcmp+0x27>
 2c3:	8b 45 08             	mov    0x8(%ebp),%eax
 2c6:	0f b6 10             	movzbl (%eax),%edx
 2c9:	8b 45 0c             	mov    0xc(%ebp),%eax
 2cc:	0f b6 00             	movzbl (%eax),%eax
 2cf:	38 c2                	cmp    %al,%dl
 2d1:	74 de                	je     2b1 <strcmp+0x5>
    p++, q++;
  return (uchar)*p - (uchar)*q;
 2d3:	8b 45 08             	mov    0x8(%ebp),%eax
 2d6:	0f b6 00             	movzbl (%eax),%eax
 2d9:	0f b6 d0             	movzbl %al,%edx
 2dc:	8b 45 0c             	mov    0xc(%ebp),%eax
 2df:	0f b6 00             	movzbl (%eax),%eax
 2e2:	0f b6 c0             	movzbl %al,%eax
 2e5:	29 c2                	sub    %eax,%edx
 2e7:	89 d0                	mov    %edx,%eax
}
 2e9:	5d                   	pop    %ebp
 2ea:	c3                   	ret    

000002eb <strlen>:

uint
strlen(char *s)
{
 2eb:	55                   	push   %ebp
 2ec:	89 e5                	mov    %esp,%ebp
 2ee:	83 ec 10             	sub    $0x10,%esp
  int n;

  for(n = 0; s[n]; n++)
 2f1:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
 2f8:	eb 04                	jmp    2fe <strlen+0x13>
 2fa:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
 2fe:	8b 55 fc             	mov    -0x4(%ebp),%edx
 301:	8b 45 08             	mov    0x8(%ebp),%eax
 304:	01 d0                	add    %edx,%eax
 306:	0f b6 00             	movzbl (%eax),%eax
 309:	84 c0                	test   %al,%al
 30b:	75 ed                	jne    2fa <strlen+0xf>
    ;
  return n;
 30d:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 310:	c9                   	leave  
 311:	c3                   	ret    

00000312 <memset>:

void*
memset(void *dst, int c, uint n)
{
 312:	55                   	push   %ebp
 313:	89 e5                	mov    %esp,%ebp
  stosb(dst, c, n);
 315:	8b 45 10             	mov    0x10(%ebp),%eax
 318:	50                   	push   %eax
 319:	ff 75 0c             	pushl  0xc(%ebp)
 31c:	ff 75 08             	pushl  0x8(%ebp)
 31f:	e8 32 ff ff ff       	call   256 <stosb>
 324:	83 c4 0c             	add    $0xc,%esp
  return dst;
 327:	8b 45 08             	mov    0x8(%ebp),%eax
}
 32a:	c9                   	leave  
 32b:	c3                   	ret    

0000032c <strchr>:

char*
strchr(const char *s, char c)
{
 32c:	55                   	push   %ebp
 32d:	89 e5                	mov    %esp,%ebp
 32f:	83 ec 04             	sub    $0x4,%esp
 332:	8b 45 0c             	mov    0xc(%ebp),%eax
 335:	88 45 fc             	mov    %al,-0x4(%ebp)
  for(; *s; s++)
 338:	eb 14                	jmp    34e <strchr+0x22>
    if(*s == c)
 33a:	8b 45 08             	mov    0x8(%ebp),%eax
 33d:	0f b6 00             	movzbl (%eax),%eax
 340:	3a 45 fc             	cmp    -0x4(%ebp),%al
 343:	75 05                	jne    34a <strchr+0x1e>
      return (char*)s;
 345:	8b 45 08             	mov    0x8(%ebp),%eax
 348:	eb 13                	jmp    35d <strchr+0x31>
}

char*
strchr(const char *s, char c)
{
  for(; *s; s++)
 34a:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 34e:	8b 45 08             	mov    0x8(%ebp),%eax
 351:	0f b6 00             	movzbl (%eax),%eax
 354:	84 c0                	test   %al,%al
 356:	75 e2                	jne    33a <strchr+0xe>
    if(*s == c)
      return (char*)s;
  return 0;
 358:	b8 00 00 00 00       	mov    $0x0,%eax
}
 35d:	c9                   	leave  
 35e:	c3                   	ret    

0000035f <gets>:

char*
gets(char *buf, int max)
{
 35f:	55                   	push   %ebp
 360:	89 e5                	mov    %esp,%ebp
 362:	83 ec 18             	sub    $0x18,%esp
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 365:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 36c:	eb 42                	jmp    3b0 <gets+0x51>
    cc = read(0, &c, 1);
 36e:	83 ec 04             	sub    $0x4,%esp
 371:	6a 01                	push   $0x1
 373:	8d 45 ef             	lea    -0x11(%ebp),%eax
 376:	50                   	push   %eax
 377:	6a 00                	push   $0x0
 379:	e8 47 01 00 00       	call   4c5 <read>
 37e:	83 c4 10             	add    $0x10,%esp
 381:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if(cc < 1)
 384:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 388:	7e 33                	jle    3bd <gets+0x5e>
      break;
    buf[i++] = c;
 38a:	8b 45 f4             	mov    -0xc(%ebp),%eax
 38d:	8d 50 01             	lea    0x1(%eax),%edx
 390:	89 55 f4             	mov    %edx,-0xc(%ebp)
 393:	89 c2                	mov    %eax,%edx
 395:	8b 45 08             	mov    0x8(%ebp),%eax
 398:	01 c2                	add    %eax,%edx
 39a:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 39e:	88 02                	mov    %al,(%edx)
    if(c == '\n' || c == '\r')
 3a0:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 3a4:	3c 0a                	cmp    $0xa,%al
 3a6:	74 16                	je     3be <gets+0x5f>
 3a8:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 3ac:	3c 0d                	cmp    $0xd,%al
 3ae:	74 0e                	je     3be <gets+0x5f>
gets(char *buf, int max)
{
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 3b0:	8b 45 f4             	mov    -0xc(%ebp),%eax
 3b3:	83 c0 01             	add    $0x1,%eax
 3b6:	3b 45 0c             	cmp    0xc(%ebp),%eax
 3b9:	7c b3                	jl     36e <gets+0xf>
 3bb:	eb 01                	jmp    3be <gets+0x5f>
    cc = read(0, &c, 1);
    if(cc < 1)
      break;
 3bd:	90                   	nop
    buf[i++] = c;
    if(c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 3be:	8b 55 f4             	mov    -0xc(%ebp),%edx
 3c1:	8b 45 08             	mov    0x8(%ebp),%eax
 3c4:	01 d0                	add    %edx,%eax
 3c6:	c6 00 00             	movb   $0x0,(%eax)
  return buf;
 3c9:	8b 45 08             	mov    0x8(%ebp),%eax
}
 3cc:	c9                   	leave  
 3cd:	c3                   	ret    

000003ce <stat>:

int
stat(char *n, struct stat *st)
{
 3ce:	55                   	push   %ebp
 3cf:	89 e5                	mov    %esp,%ebp
 3d1:	83 ec 18             	sub    $0x18,%esp
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 3d4:	83 ec 08             	sub    $0x8,%esp
 3d7:	6a 00                	push   $0x0
 3d9:	ff 75 08             	pushl  0x8(%ebp)
 3dc:	e8 0c 01 00 00       	call   4ed <open>
 3e1:	83 c4 10             	add    $0x10,%esp
 3e4:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(fd < 0)
 3e7:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 3eb:	79 07                	jns    3f4 <stat+0x26>
    return -1;
 3ed:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 3f2:	eb 25                	jmp    419 <stat+0x4b>
  r = fstat(fd, st);
 3f4:	83 ec 08             	sub    $0x8,%esp
 3f7:	ff 75 0c             	pushl  0xc(%ebp)
 3fa:	ff 75 f4             	pushl  -0xc(%ebp)
 3fd:	e8 03 01 00 00       	call   505 <fstat>
 402:	83 c4 10             	add    $0x10,%esp
 405:	89 45 f0             	mov    %eax,-0x10(%ebp)
  close(fd);
 408:	83 ec 0c             	sub    $0xc,%esp
 40b:	ff 75 f4             	pushl  -0xc(%ebp)
 40e:	e8 c2 00 00 00       	call   4d5 <close>
 413:	83 c4 10             	add    $0x10,%esp
  return r;
 416:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
 419:	c9                   	leave  
 41a:	c3                   	ret    

0000041b <atoi>:

int
atoi(const char *s)
{
 41b:	55                   	push   %ebp
 41c:	89 e5                	mov    %esp,%ebp
 41e:	83 ec 10             	sub    $0x10,%esp
  int n;

  n = 0;
 421:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  while('0' <= *s && *s <= '9')
 428:	eb 25                	jmp    44f <atoi+0x34>
    n = n*10 + *s++ - '0';
 42a:	8b 55 fc             	mov    -0x4(%ebp),%edx
 42d:	89 d0                	mov    %edx,%eax
 42f:	c1 e0 02             	shl    $0x2,%eax
 432:	01 d0                	add    %edx,%eax
 434:	01 c0                	add    %eax,%eax
 436:	89 c1                	mov    %eax,%ecx
 438:	8b 45 08             	mov    0x8(%ebp),%eax
 43b:	8d 50 01             	lea    0x1(%eax),%edx
 43e:	89 55 08             	mov    %edx,0x8(%ebp)
 441:	0f b6 00             	movzbl (%eax),%eax
 444:	0f be c0             	movsbl %al,%eax
 447:	01 c8                	add    %ecx,%eax
 449:	83 e8 30             	sub    $0x30,%eax
 44c:	89 45 fc             	mov    %eax,-0x4(%ebp)
atoi(const char *s)
{
  int n;

  n = 0;
  while('0' <= *s && *s <= '9')
 44f:	8b 45 08             	mov    0x8(%ebp),%eax
 452:	0f b6 00             	movzbl (%eax),%eax
 455:	3c 2f                	cmp    $0x2f,%al
 457:	7e 0a                	jle    463 <atoi+0x48>
 459:	8b 45 08             	mov    0x8(%ebp),%eax
 45c:	0f b6 00             	movzbl (%eax),%eax
 45f:	3c 39                	cmp    $0x39,%al
 461:	7e c7                	jle    42a <atoi+0xf>
    n = n*10 + *s++ - '0';
  return n;
 463:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 466:	c9                   	leave  
 467:	c3                   	ret    

00000468 <memmove>:

void*
memmove(void *vdst, void *vsrc, int n)
{
 468:	55                   	push   %ebp
 469:	89 e5                	mov    %esp,%ebp
 46b:	83 ec 10             	sub    $0x10,%esp
  char *dst, *src;
  
  dst = vdst;
 46e:	8b 45 08             	mov    0x8(%ebp),%eax
 471:	89 45 fc             	mov    %eax,-0x4(%ebp)
  src = vsrc;
 474:	8b 45 0c             	mov    0xc(%ebp),%eax
 477:	89 45 f8             	mov    %eax,-0x8(%ebp)
  while(n-- > 0)
 47a:	eb 17                	jmp    493 <memmove+0x2b>
    *dst++ = *src++;
 47c:	8b 45 fc             	mov    -0x4(%ebp),%eax
 47f:	8d 50 01             	lea    0x1(%eax),%edx
 482:	89 55 fc             	mov    %edx,-0x4(%ebp)
 485:	8b 55 f8             	mov    -0x8(%ebp),%edx
 488:	8d 4a 01             	lea    0x1(%edx),%ecx
 48b:	89 4d f8             	mov    %ecx,-0x8(%ebp)
 48e:	0f b6 12             	movzbl (%edx),%edx
 491:	88 10                	mov    %dl,(%eax)
{
  char *dst, *src;
  
  dst = vdst;
  src = vsrc;
  while(n-- > 0)
 493:	8b 45 10             	mov    0x10(%ebp),%eax
 496:	8d 50 ff             	lea    -0x1(%eax),%edx
 499:	89 55 10             	mov    %edx,0x10(%ebp)
 49c:	85 c0                	test   %eax,%eax
 49e:	7f dc                	jg     47c <memmove+0x14>
    *dst++ = *src++;
  return vdst;
 4a0:	8b 45 08             	mov    0x8(%ebp),%eax
}
 4a3:	c9                   	leave  
 4a4:	c3                   	ret    

000004a5 <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
 4a5:	b8 01 00 00 00       	mov    $0x1,%eax
 4aa:	cd 40                	int    $0x40
 4ac:	c3                   	ret    

000004ad <exit>:
SYSCALL(exit)
 4ad:	b8 02 00 00 00       	mov    $0x2,%eax
 4b2:	cd 40                	int    $0x40
 4b4:	c3                   	ret    

000004b5 <wait>:
SYSCALL(wait)
 4b5:	b8 03 00 00 00       	mov    $0x3,%eax
 4ba:	cd 40                	int    $0x40
 4bc:	c3                   	ret    

000004bd <pipe>:
SYSCALL(pipe)
 4bd:	b8 04 00 00 00       	mov    $0x4,%eax
 4c2:	cd 40                	int    $0x40
 4c4:	c3                   	ret    

000004c5 <read>:
SYSCALL(read)
 4c5:	b8 05 00 00 00       	mov    $0x5,%eax
 4ca:	cd 40                	int    $0x40
 4cc:	c3                   	ret    

000004cd <write>:
SYSCALL(write)
 4cd:	b8 10 00 00 00       	mov    $0x10,%eax
 4d2:	cd 40                	int    $0x40
 4d4:	c3                   	ret    

000004d5 <close>:
SYSCALL(close)
 4d5:	b8 15 00 00 00       	mov    $0x15,%eax
 4da:	cd 40                	int    $0x40
 4dc:	c3                   	ret    

000004dd <kill>:
SYSCALL(kill)
 4dd:	b8 06 00 00 00       	mov    $0x6,%eax
 4e2:	cd 40                	int    $0x40
 4e4:	c3                   	ret    

000004e5 <exec>:
SYSCALL(exec)
 4e5:	b8 07 00 00 00       	mov    $0x7,%eax
 4ea:	cd 40                	int    $0x40
 4ec:	c3                   	ret    

000004ed <open>:
SYSCALL(open)
 4ed:	b8 0f 00 00 00       	mov    $0xf,%eax
 4f2:	cd 40                	int    $0x40
 4f4:	c3                   	ret    

000004f5 <mknod>:
SYSCALL(mknod)
 4f5:	b8 11 00 00 00       	mov    $0x11,%eax
 4fa:	cd 40                	int    $0x40
 4fc:	c3                   	ret    

000004fd <unlink>:
SYSCALL(unlink)
 4fd:	b8 12 00 00 00       	mov    $0x12,%eax
 502:	cd 40                	int    $0x40
 504:	c3                   	ret    

00000505 <fstat>:
SYSCALL(fstat)
 505:	b8 08 00 00 00       	mov    $0x8,%eax
 50a:	cd 40                	int    $0x40
 50c:	c3                   	ret    

0000050d <link>:
SYSCALL(link)
 50d:	b8 13 00 00 00       	mov    $0x13,%eax
 512:	cd 40                	int    $0x40
 514:	c3                   	ret    

00000515 <mkdir>:
SYSCALL(mkdir)
 515:	b8 14 00 00 00       	mov    $0x14,%eax
 51a:	cd 40                	int    $0x40
 51c:	c3                   	ret    

0000051d <chdir>:
SYSCALL(chdir)
 51d:	b8 09 00 00 00       	mov    $0x9,%eax
 522:	cd 40                	int    $0x40
 524:	c3                   	ret    

00000525 <dup>:
SYSCALL(dup)
 525:	b8 0a 00 00 00       	mov    $0xa,%eax
 52a:	cd 40                	int    $0x40
 52c:	c3                   	ret    

0000052d <getpid>:
SYSCALL(getpid)
 52d:	b8 0b 00 00 00       	mov    $0xb,%eax
 532:	cd 40                	int    $0x40
 534:	c3                   	ret    

00000535 <sbrk>:
SYSCALL(sbrk)
 535:	b8 0c 00 00 00       	mov    $0xc,%eax
 53a:	cd 40                	int    $0x40
 53c:	c3                   	ret    

0000053d <sleep>:
SYSCALL(sleep)
 53d:	b8 0d 00 00 00       	mov    $0xd,%eax
 542:	cd 40                	int    $0x40
 544:	c3                   	ret    

00000545 <uptime>:
SYSCALL(uptime)
 545:	b8 0e 00 00 00       	mov    $0xe,%eax
 54a:	cd 40                	int    $0x40
 54c:	c3                   	ret    

0000054d <halt>:
SYSCALL(halt)
 54d:	b8 16 00 00 00       	mov    $0x16,%eax
 552:	cd 40                	int    $0x40
 554:	c3                   	ret    

00000555 <date>:
SYSCALL(date)
 555:	b8 17 00 00 00       	mov    $0x17,%eax
 55a:	cd 40                	int    $0x40
 55c:	c3                   	ret    

0000055d <getuid>:
SYSCALL(getuid)
 55d:	b8 18 00 00 00       	mov    $0x18,%eax
 562:	cd 40                	int    $0x40
 564:	c3                   	ret    

00000565 <getgid>:
SYSCALL(getgid)
 565:	b8 19 00 00 00       	mov    $0x19,%eax
 56a:	cd 40                	int    $0x40
 56c:	c3                   	ret    

0000056d <getppid>:
SYSCALL(getppid)
 56d:	b8 1a 00 00 00       	mov    $0x1a,%eax
 572:	cd 40                	int    $0x40
 574:	c3                   	ret    

00000575 <setuid>:
SYSCALL(setuid)
 575:	b8 1b 00 00 00       	mov    $0x1b,%eax
 57a:	cd 40                	int    $0x40
 57c:	c3                   	ret    

0000057d <setgid>:
SYSCALL(setgid)
 57d:	b8 1c 00 00 00       	mov    $0x1c,%eax
 582:	cd 40                	int    $0x40
 584:	c3                   	ret    

00000585 <getprocs>:
SYSCALL(getprocs)
 585:	b8 1d 00 00 00       	mov    $0x1d,%eax
 58a:	cd 40                	int    $0x40
 58c:	c3                   	ret    

0000058d <setpriority>:
SYSCALL(setpriority)
 58d:	b8 1e 00 00 00       	mov    $0x1e,%eax
 592:	cd 40                	int    $0x40
 594:	c3                   	ret    

00000595 <putc>:
#include "stat.h"
#include "user.h"

static void
putc(int fd, char c)
{
 595:	55                   	push   %ebp
 596:	89 e5                	mov    %esp,%ebp
 598:	83 ec 18             	sub    $0x18,%esp
 59b:	8b 45 0c             	mov    0xc(%ebp),%eax
 59e:	88 45 f4             	mov    %al,-0xc(%ebp)
  write(fd, &c, 1);
 5a1:	83 ec 04             	sub    $0x4,%esp
 5a4:	6a 01                	push   $0x1
 5a6:	8d 45 f4             	lea    -0xc(%ebp),%eax
 5a9:	50                   	push   %eax
 5aa:	ff 75 08             	pushl  0x8(%ebp)
 5ad:	e8 1b ff ff ff       	call   4cd <write>
 5b2:	83 c4 10             	add    $0x10,%esp
}
 5b5:	90                   	nop
 5b6:	c9                   	leave  
 5b7:	c3                   	ret    

000005b8 <printint>:

static void
printint(int fd, int xx, int base, int sgn)
{
 5b8:	55                   	push   %ebp
 5b9:	89 e5                	mov    %esp,%ebp
 5bb:	53                   	push   %ebx
 5bc:	83 ec 24             	sub    $0x24,%esp
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 5bf:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  if(sgn && xx < 0){
 5c6:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
 5ca:	74 17                	je     5e3 <printint+0x2b>
 5cc:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
 5d0:	79 11                	jns    5e3 <printint+0x2b>
    neg = 1;
 5d2:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
    x = -xx;
 5d9:	8b 45 0c             	mov    0xc(%ebp),%eax
 5dc:	f7 d8                	neg    %eax
 5de:	89 45 ec             	mov    %eax,-0x14(%ebp)
 5e1:	eb 06                	jmp    5e9 <printint+0x31>
  } else {
    x = xx;
 5e3:	8b 45 0c             	mov    0xc(%ebp),%eax
 5e6:	89 45 ec             	mov    %eax,-0x14(%ebp)
  }

  i = 0;
 5e9:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  do{
    buf[i++] = digits[x % base];
 5f0:	8b 4d f4             	mov    -0xc(%ebp),%ecx
 5f3:	8d 41 01             	lea    0x1(%ecx),%eax
 5f6:	89 45 f4             	mov    %eax,-0xc(%ebp)
 5f9:	8b 5d 10             	mov    0x10(%ebp),%ebx
 5fc:	8b 45 ec             	mov    -0x14(%ebp),%eax
 5ff:	ba 00 00 00 00       	mov    $0x0,%edx
 604:	f7 f3                	div    %ebx
 606:	89 d0                	mov    %edx,%eax
 608:	0f b6 80 90 0d 00 00 	movzbl 0xd90(%eax),%eax
 60f:	88 44 0d dc          	mov    %al,-0x24(%ebp,%ecx,1)
  }while((x /= base) != 0);
 613:	8b 5d 10             	mov    0x10(%ebp),%ebx
 616:	8b 45 ec             	mov    -0x14(%ebp),%eax
 619:	ba 00 00 00 00       	mov    $0x0,%edx
 61e:	f7 f3                	div    %ebx
 620:	89 45 ec             	mov    %eax,-0x14(%ebp)
 623:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 627:	75 c7                	jne    5f0 <printint+0x38>
  if(neg)
 629:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 62d:	74 2d                	je     65c <printint+0xa4>
    buf[i++] = '-';
 62f:	8b 45 f4             	mov    -0xc(%ebp),%eax
 632:	8d 50 01             	lea    0x1(%eax),%edx
 635:	89 55 f4             	mov    %edx,-0xc(%ebp)
 638:	c6 44 05 dc 2d       	movb   $0x2d,-0x24(%ebp,%eax,1)

  while(--i >= 0)
 63d:	eb 1d                	jmp    65c <printint+0xa4>
    putc(fd, buf[i]);
 63f:	8d 55 dc             	lea    -0x24(%ebp),%edx
 642:	8b 45 f4             	mov    -0xc(%ebp),%eax
 645:	01 d0                	add    %edx,%eax
 647:	0f b6 00             	movzbl (%eax),%eax
 64a:	0f be c0             	movsbl %al,%eax
 64d:	83 ec 08             	sub    $0x8,%esp
 650:	50                   	push   %eax
 651:	ff 75 08             	pushl  0x8(%ebp)
 654:	e8 3c ff ff ff       	call   595 <putc>
 659:	83 c4 10             	add    $0x10,%esp
    buf[i++] = digits[x % base];
  }while((x /= base) != 0);
  if(neg)
    buf[i++] = '-';

  while(--i >= 0)
 65c:	83 6d f4 01          	subl   $0x1,-0xc(%ebp)
 660:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 664:	79 d9                	jns    63f <printint+0x87>
    putc(fd, buf[i]);
}
 666:	90                   	nop
 667:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 66a:	c9                   	leave  
 66b:	c3                   	ret    

0000066c <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, char *fmt, ...)
{
 66c:	55                   	push   %ebp
 66d:	89 e5                	mov    %esp,%ebp
 66f:	83 ec 28             	sub    $0x28,%esp
  char *s;
  int c, i, state;
  uint *ap;

  state = 0;
 672:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  ap = (uint*)(void*)&fmt + 1;
 679:	8d 45 0c             	lea    0xc(%ebp),%eax
 67c:	83 c0 04             	add    $0x4,%eax
 67f:	89 45 e8             	mov    %eax,-0x18(%ebp)
  for(i = 0; fmt[i]; i++){
 682:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 689:	e9 59 01 00 00       	jmp    7e7 <printf+0x17b>
    c = fmt[i] & 0xff;
 68e:	8b 55 0c             	mov    0xc(%ebp),%edx
 691:	8b 45 f0             	mov    -0x10(%ebp),%eax
 694:	01 d0                	add    %edx,%eax
 696:	0f b6 00             	movzbl (%eax),%eax
 699:	0f be c0             	movsbl %al,%eax
 69c:	25 ff 00 00 00       	and    $0xff,%eax
 6a1:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    if(state == 0){
 6a4:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 6a8:	75 2c                	jne    6d6 <printf+0x6a>
      if(c == '%'){
 6aa:	83 7d e4 25          	cmpl   $0x25,-0x1c(%ebp)
 6ae:	75 0c                	jne    6bc <printf+0x50>
        state = '%';
 6b0:	c7 45 ec 25 00 00 00 	movl   $0x25,-0x14(%ebp)
 6b7:	e9 27 01 00 00       	jmp    7e3 <printf+0x177>
      } else {
        putc(fd, c);
 6bc:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 6bf:	0f be c0             	movsbl %al,%eax
 6c2:	83 ec 08             	sub    $0x8,%esp
 6c5:	50                   	push   %eax
 6c6:	ff 75 08             	pushl  0x8(%ebp)
 6c9:	e8 c7 fe ff ff       	call   595 <putc>
 6ce:	83 c4 10             	add    $0x10,%esp
 6d1:	e9 0d 01 00 00       	jmp    7e3 <printf+0x177>
      }
    } else if(state == '%'){
 6d6:	83 7d ec 25          	cmpl   $0x25,-0x14(%ebp)
 6da:	0f 85 03 01 00 00    	jne    7e3 <printf+0x177>
      if(c == 'd'){
 6e0:	83 7d e4 64          	cmpl   $0x64,-0x1c(%ebp)
 6e4:	75 1e                	jne    704 <printf+0x98>
        printint(fd, *ap, 10, 1);
 6e6:	8b 45 e8             	mov    -0x18(%ebp),%eax
 6e9:	8b 00                	mov    (%eax),%eax
 6eb:	6a 01                	push   $0x1
 6ed:	6a 0a                	push   $0xa
 6ef:	50                   	push   %eax
 6f0:	ff 75 08             	pushl  0x8(%ebp)
 6f3:	e8 c0 fe ff ff       	call   5b8 <printint>
 6f8:	83 c4 10             	add    $0x10,%esp
        ap++;
 6fb:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 6ff:	e9 d8 00 00 00       	jmp    7dc <printf+0x170>
      } else if(c == 'x' || c == 'p'){
 704:	83 7d e4 78          	cmpl   $0x78,-0x1c(%ebp)
 708:	74 06                	je     710 <printf+0xa4>
 70a:	83 7d e4 70          	cmpl   $0x70,-0x1c(%ebp)
 70e:	75 1e                	jne    72e <printf+0xc2>
        printint(fd, *ap, 16, 0);
 710:	8b 45 e8             	mov    -0x18(%ebp),%eax
 713:	8b 00                	mov    (%eax),%eax
 715:	6a 00                	push   $0x0
 717:	6a 10                	push   $0x10
 719:	50                   	push   %eax
 71a:	ff 75 08             	pushl  0x8(%ebp)
 71d:	e8 96 fe ff ff       	call   5b8 <printint>
 722:	83 c4 10             	add    $0x10,%esp
        ap++;
 725:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 729:	e9 ae 00 00 00       	jmp    7dc <printf+0x170>
      } else if(c == 's'){
 72e:	83 7d e4 73          	cmpl   $0x73,-0x1c(%ebp)
 732:	75 43                	jne    777 <printf+0x10b>
        s = (char*)*ap;
 734:	8b 45 e8             	mov    -0x18(%ebp),%eax
 737:	8b 00                	mov    (%eax),%eax
 739:	89 45 f4             	mov    %eax,-0xc(%ebp)
        ap++;
 73c:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
        if(s == 0)
 740:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 744:	75 25                	jne    76b <printf+0xff>
          s = "(null)";
 746:	c7 45 f4 f6 0a 00 00 	movl   $0xaf6,-0xc(%ebp)
        while(*s != 0){
 74d:	eb 1c                	jmp    76b <printf+0xff>
          putc(fd, *s);
 74f:	8b 45 f4             	mov    -0xc(%ebp),%eax
 752:	0f b6 00             	movzbl (%eax),%eax
 755:	0f be c0             	movsbl %al,%eax
 758:	83 ec 08             	sub    $0x8,%esp
 75b:	50                   	push   %eax
 75c:	ff 75 08             	pushl  0x8(%ebp)
 75f:	e8 31 fe ff ff       	call   595 <putc>
 764:	83 c4 10             	add    $0x10,%esp
          s++;
 767:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
      } else if(c == 's'){
        s = (char*)*ap;
        ap++;
        if(s == 0)
          s = "(null)";
        while(*s != 0){
 76b:	8b 45 f4             	mov    -0xc(%ebp),%eax
 76e:	0f b6 00             	movzbl (%eax),%eax
 771:	84 c0                	test   %al,%al
 773:	75 da                	jne    74f <printf+0xe3>
 775:	eb 65                	jmp    7dc <printf+0x170>
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 777:	83 7d e4 63          	cmpl   $0x63,-0x1c(%ebp)
 77b:	75 1d                	jne    79a <printf+0x12e>
        putc(fd, *ap);
 77d:	8b 45 e8             	mov    -0x18(%ebp),%eax
 780:	8b 00                	mov    (%eax),%eax
 782:	0f be c0             	movsbl %al,%eax
 785:	83 ec 08             	sub    $0x8,%esp
 788:	50                   	push   %eax
 789:	ff 75 08             	pushl  0x8(%ebp)
 78c:	e8 04 fe ff ff       	call   595 <putc>
 791:	83 c4 10             	add    $0x10,%esp
        ap++;
 794:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 798:	eb 42                	jmp    7dc <printf+0x170>
      } else if(c == '%'){
 79a:	83 7d e4 25          	cmpl   $0x25,-0x1c(%ebp)
 79e:	75 17                	jne    7b7 <printf+0x14b>
        putc(fd, c);
 7a0:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 7a3:	0f be c0             	movsbl %al,%eax
 7a6:	83 ec 08             	sub    $0x8,%esp
 7a9:	50                   	push   %eax
 7aa:	ff 75 08             	pushl  0x8(%ebp)
 7ad:	e8 e3 fd ff ff       	call   595 <putc>
 7b2:	83 c4 10             	add    $0x10,%esp
 7b5:	eb 25                	jmp    7dc <printf+0x170>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 7b7:	83 ec 08             	sub    $0x8,%esp
 7ba:	6a 25                	push   $0x25
 7bc:	ff 75 08             	pushl  0x8(%ebp)
 7bf:	e8 d1 fd ff ff       	call   595 <putc>
 7c4:	83 c4 10             	add    $0x10,%esp
        putc(fd, c);
 7c7:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 7ca:	0f be c0             	movsbl %al,%eax
 7cd:	83 ec 08             	sub    $0x8,%esp
 7d0:	50                   	push   %eax
 7d1:	ff 75 08             	pushl  0x8(%ebp)
 7d4:	e8 bc fd ff ff       	call   595 <putc>
 7d9:	83 c4 10             	add    $0x10,%esp
      }
      state = 0;
 7dc:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
 7e3:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
 7e7:	8b 55 0c             	mov    0xc(%ebp),%edx
 7ea:	8b 45 f0             	mov    -0x10(%ebp),%eax
 7ed:	01 d0                	add    %edx,%eax
 7ef:	0f b6 00             	movzbl (%eax),%eax
 7f2:	84 c0                	test   %al,%al
 7f4:	0f 85 94 fe ff ff    	jne    68e <printf+0x22>
        putc(fd, c);
      }
      state = 0;
    }
  }
}
 7fa:	90                   	nop
 7fb:	c9                   	leave  
 7fc:	c3                   	ret    

000007fd <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 7fd:	55                   	push   %ebp
 7fe:	89 e5                	mov    %esp,%ebp
 800:	83 ec 10             	sub    $0x10,%esp
  Header *bp, *p;

  bp = (Header*)ap - 1;
 803:	8b 45 08             	mov    0x8(%ebp),%eax
 806:	83 e8 08             	sub    $0x8,%eax
 809:	89 45 f8             	mov    %eax,-0x8(%ebp)
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 80c:	a1 ac 0d 00 00       	mov    0xdac,%eax
 811:	89 45 fc             	mov    %eax,-0x4(%ebp)
 814:	eb 24                	jmp    83a <free+0x3d>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 816:	8b 45 fc             	mov    -0x4(%ebp),%eax
 819:	8b 00                	mov    (%eax),%eax
 81b:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 81e:	77 12                	ja     832 <free+0x35>
 820:	8b 45 f8             	mov    -0x8(%ebp),%eax
 823:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 826:	77 24                	ja     84c <free+0x4f>
 828:	8b 45 fc             	mov    -0x4(%ebp),%eax
 82b:	8b 00                	mov    (%eax),%eax
 82d:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 830:	77 1a                	ja     84c <free+0x4f>
free(void *ap)
{
  Header *bp, *p;

  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 832:	8b 45 fc             	mov    -0x4(%ebp),%eax
 835:	8b 00                	mov    (%eax),%eax
 837:	89 45 fc             	mov    %eax,-0x4(%ebp)
 83a:	8b 45 f8             	mov    -0x8(%ebp),%eax
 83d:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 840:	76 d4                	jbe    816 <free+0x19>
 842:	8b 45 fc             	mov    -0x4(%ebp),%eax
 845:	8b 00                	mov    (%eax),%eax
 847:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 84a:	76 ca                	jbe    816 <free+0x19>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if(bp + bp->s.size == p->s.ptr){
 84c:	8b 45 f8             	mov    -0x8(%ebp),%eax
 84f:	8b 40 04             	mov    0x4(%eax),%eax
 852:	8d 14 c5 00 00 00 00 	lea    0x0(,%eax,8),%edx
 859:	8b 45 f8             	mov    -0x8(%ebp),%eax
 85c:	01 c2                	add    %eax,%edx
 85e:	8b 45 fc             	mov    -0x4(%ebp),%eax
 861:	8b 00                	mov    (%eax),%eax
 863:	39 c2                	cmp    %eax,%edx
 865:	75 24                	jne    88b <free+0x8e>
    bp->s.size += p->s.ptr->s.size;
 867:	8b 45 f8             	mov    -0x8(%ebp),%eax
 86a:	8b 50 04             	mov    0x4(%eax),%edx
 86d:	8b 45 fc             	mov    -0x4(%ebp),%eax
 870:	8b 00                	mov    (%eax),%eax
 872:	8b 40 04             	mov    0x4(%eax),%eax
 875:	01 c2                	add    %eax,%edx
 877:	8b 45 f8             	mov    -0x8(%ebp),%eax
 87a:	89 50 04             	mov    %edx,0x4(%eax)
    bp->s.ptr = p->s.ptr->s.ptr;
 87d:	8b 45 fc             	mov    -0x4(%ebp),%eax
 880:	8b 00                	mov    (%eax),%eax
 882:	8b 10                	mov    (%eax),%edx
 884:	8b 45 f8             	mov    -0x8(%ebp),%eax
 887:	89 10                	mov    %edx,(%eax)
 889:	eb 0a                	jmp    895 <free+0x98>
  } else
    bp->s.ptr = p->s.ptr;
 88b:	8b 45 fc             	mov    -0x4(%ebp),%eax
 88e:	8b 10                	mov    (%eax),%edx
 890:	8b 45 f8             	mov    -0x8(%ebp),%eax
 893:	89 10                	mov    %edx,(%eax)
  if(p + p->s.size == bp){
 895:	8b 45 fc             	mov    -0x4(%ebp),%eax
 898:	8b 40 04             	mov    0x4(%eax),%eax
 89b:	8d 14 c5 00 00 00 00 	lea    0x0(,%eax,8),%edx
 8a2:	8b 45 fc             	mov    -0x4(%ebp),%eax
 8a5:	01 d0                	add    %edx,%eax
 8a7:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 8aa:	75 20                	jne    8cc <free+0xcf>
    p->s.size += bp->s.size;
 8ac:	8b 45 fc             	mov    -0x4(%ebp),%eax
 8af:	8b 50 04             	mov    0x4(%eax),%edx
 8b2:	8b 45 f8             	mov    -0x8(%ebp),%eax
 8b5:	8b 40 04             	mov    0x4(%eax),%eax
 8b8:	01 c2                	add    %eax,%edx
 8ba:	8b 45 fc             	mov    -0x4(%ebp),%eax
 8bd:	89 50 04             	mov    %edx,0x4(%eax)
    p->s.ptr = bp->s.ptr;
 8c0:	8b 45 f8             	mov    -0x8(%ebp),%eax
 8c3:	8b 10                	mov    (%eax),%edx
 8c5:	8b 45 fc             	mov    -0x4(%ebp),%eax
 8c8:	89 10                	mov    %edx,(%eax)
 8ca:	eb 08                	jmp    8d4 <free+0xd7>
  } else
    p->s.ptr = bp;
 8cc:	8b 45 fc             	mov    -0x4(%ebp),%eax
 8cf:	8b 55 f8             	mov    -0x8(%ebp),%edx
 8d2:	89 10                	mov    %edx,(%eax)
  freep = p;
 8d4:	8b 45 fc             	mov    -0x4(%ebp),%eax
 8d7:	a3 ac 0d 00 00       	mov    %eax,0xdac
}
 8dc:	90                   	nop
 8dd:	c9                   	leave  
 8de:	c3                   	ret    

000008df <morecore>:

static Header*
morecore(uint nu)
{
 8df:	55                   	push   %ebp
 8e0:	89 e5                	mov    %esp,%ebp
 8e2:	83 ec 18             	sub    $0x18,%esp
  char *p;
  Header *hp;

  if(nu < 4096)
 8e5:	81 7d 08 ff 0f 00 00 	cmpl   $0xfff,0x8(%ebp)
 8ec:	77 07                	ja     8f5 <morecore+0x16>
    nu = 4096;
 8ee:	c7 45 08 00 10 00 00 	movl   $0x1000,0x8(%ebp)
  p = sbrk(nu * sizeof(Header));
 8f5:	8b 45 08             	mov    0x8(%ebp),%eax
 8f8:	c1 e0 03             	shl    $0x3,%eax
 8fb:	83 ec 0c             	sub    $0xc,%esp
 8fe:	50                   	push   %eax
 8ff:	e8 31 fc ff ff       	call   535 <sbrk>
 904:	83 c4 10             	add    $0x10,%esp
 907:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(p == (char*)-1)
 90a:	83 7d f4 ff          	cmpl   $0xffffffff,-0xc(%ebp)
 90e:	75 07                	jne    917 <morecore+0x38>
    return 0;
 910:	b8 00 00 00 00       	mov    $0x0,%eax
 915:	eb 26                	jmp    93d <morecore+0x5e>
  hp = (Header*)p;
 917:	8b 45 f4             	mov    -0xc(%ebp),%eax
 91a:	89 45 f0             	mov    %eax,-0x10(%ebp)
  hp->s.size = nu;
 91d:	8b 45 f0             	mov    -0x10(%ebp),%eax
 920:	8b 55 08             	mov    0x8(%ebp),%edx
 923:	89 50 04             	mov    %edx,0x4(%eax)
  free((void*)(hp + 1));
 926:	8b 45 f0             	mov    -0x10(%ebp),%eax
 929:	83 c0 08             	add    $0x8,%eax
 92c:	83 ec 0c             	sub    $0xc,%esp
 92f:	50                   	push   %eax
 930:	e8 c8 fe ff ff       	call   7fd <free>
 935:	83 c4 10             	add    $0x10,%esp
  return freep;
 938:	a1 ac 0d 00 00       	mov    0xdac,%eax
}
 93d:	c9                   	leave  
 93e:	c3                   	ret    

0000093f <malloc>:

void*
malloc(uint nbytes)
{
 93f:	55                   	push   %ebp
 940:	89 e5                	mov    %esp,%ebp
 942:	83 ec 18             	sub    $0x18,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 945:	8b 45 08             	mov    0x8(%ebp),%eax
 948:	83 c0 07             	add    $0x7,%eax
 94b:	c1 e8 03             	shr    $0x3,%eax
 94e:	83 c0 01             	add    $0x1,%eax
 951:	89 45 ec             	mov    %eax,-0x14(%ebp)
  if((prevp = freep) == 0){
 954:	a1 ac 0d 00 00       	mov    0xdac,%eax
 959:	89 45 f0             	mov    %eax,-0x10(%ebp)
 95c:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 960:	75 23                	jne    985 <malloc+0x46>
    base.s.ptr = freep = prevp = &base;
 962:	c7 45 f0 a4 0d 00 00 	movl   $0xda4,-0x10(%ebp)
 969:	8b 45 f0             	mov    -0x10(%ebp),%eax
 96c:	a3 ac 0d 00 00       	mov    %eax,0xdac
 971:	a1 ac 0d 00 00       	mov    0xdac,%eax
 976:	a3 a4 0d 00 00       	mov    %eax,0xda4
    base.s.size = 0;
 97b:	c7 05 a8 0d 00 00 00 	movl   $0x0,0xda8
 982:	00 00 00 
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 985:	8b 45 f0             	mov    -0x10(%ebp),%eax
 988:	8b 00                	mov    (%eax),%eax
 98a:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if(p->s.size >= nunits){
 98d:	8b 45 f4             	mov    -0xc(%ebp),%eax
 990:	8b 40 04             	mov    0x4(%eax),%eax
 993:	3b 45 ec             	cmp    -0x14(%ebp),%eax
 996:	72 4d                	jb     9e5 <malloc+0xa6>
      if(p->s.size == nunits)
 998:	8b 45 f4             	mov    -0xc(%ebp),%eax
 99b:	8b 40 04             	mov    0x4(%eax),%eax
 99e:	3b 45 ec             	cmp    -0x14(%ebp),%eax
 9a1:	75 0c                	jne    9af <malloc+0x70>
        prevp->s.ptr = p->s.ptr;
 9a3:	8b 45 f4             	mov    -0xc(%ebp),%eax
 9a6:	8b 10                	mov    (%eax),%edx
 9a8:	8b 45 f0             	mov    -0x10(%ebp),%eax
 9ab:	89 10                	mov    %edx,(%eax)
 9ad:	eb 26                	jmp    9d5 <malloc+0x96>
      else {
        p->s.size -= nunits;
 9af:	8b 45 f4             	mov    -0xc(%ebp),%eax
 9b2:	8b 40 04             	mov    0x4(%eax),%eax
 9b5:	2b 45 ec             	sub    -0x14(%ebp),%eax
 9b8:	89 c2                	mov    %eax,%edx
 9ba:	8b 45 f4             	mov    -0xc(%ebp),%eax
 9bd:	89 50 04             	mov    %edx,0x4(%eax)
        p += p->s.size;
 9c0:	8b 45 f4             	mov    -0xc(%ebp),%eax
 9c3:	8b 40 04             	mov    0x4(%eax),%eax
 9c6:	c1 e0 03             	shl    $0x3,%eax
 9c9:	01 45 f4             	add    %eax,-0xc(%ebp)
        p->s.size = nunits;
 9cc:	8b 45 f4             	mov    -0xc(%ebp),%eax
 9cf:	8b 55 ec             	mov    -0x14(%ebp),%edx
 9d2:	89 50 04             	mov    %edx,0x4(%eax)
      }
      freep = prevp;
 9d5:	8b 45 f0             	mov    -0x10(%ebp),%eax
 9d8:	a3 ac 0d 00 00       	mov    %eax,0xdac
      return (void*)(p + 1);
 9dd:	8b 45 f4             	mov    -0xc(%ebp),%eax
 9e0:	83 c0 08             	add    $0x8,%eax
 9e3:	eb 3b                	jmp    a20 <malloc+0xe1>
    }
    if(p == freep)
 9e5:	a1 ac 0d 00 00       	mov    0xdac,%eax
 9ea:	39 45 f4             	cmp    %eax,-0xc(%ebp)
 9ed:	75 1e                	jne    a0d <malloc+0xce>
      if((p = morecore(nunits)) == 0)
 9ef:	83 ec 0c             	sub    $0xc,%esp
 9f2:	ff 75 ec             	pushl  -0x14(%ebp)
 9f5:	e8 e5 fe ff ff       	call   8df <morecore>
 9fa:	83 c4 10             	add    $0x10,%esp
 9fd:	89 45 f4             	mov    %eax,-0xc(%ebp)
 a00:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 a04:	75 07                	jne    a0d <malloc+0xce>
        return 0;
 a06:	b8 00 00 00 00       	mov    $0x0,%eax
 a0b:	eb 13                	jmp    a20 <malloc+0xe1>
  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
  if((prevp = freep) == 0){
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 a0d:	8b 45 f4             	mov    -0xc(%ebp),%eax
 a10:	89 45 f0             	mov    %eax,-0x10(%ebp)
 a13:	8b 45 f4             	mov    -0xc(%ebp),%eax
 a16:	8b 00                	mov    (%eax),%eax
 a18:	89 45 f4             	mov    %eax,-0xc(%ebp)
      return (void*)(p + 1);
    }
    if(p == freep)
      if((p = morecore(nunits)) == 0)
        return 0;
  }
 a1b:	e9 6d ff ff ff       	jmp    98d <malloc+0x4e>
}
 a20:	c9                   	leave  
 a21:	c3                   	ret    
