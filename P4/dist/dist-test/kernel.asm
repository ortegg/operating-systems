
kernel:     file format elf32-i386


Disassembly of section .text:

80100000 <multiboot_header>:
80100000:	02 b0 ad 1b 00 00    	add    0x1bad(%eax),%dh
80100006:	00 00                	add    %al,(%eax)
80100008:	fe 4f 52             	decb   0x52(%edi)
8010000b:	e4                   	.byte 0xe4

8010000c <entry>:

# Entering xv6 on boot processor, with paging off.
.globl entry
entry:
  # Turn on page size extension for 4Mbyte pages
  movl    %cr4, %eax
8010000c:	0f 20 e0             	mov    %cr4,%eax
  orl     $(CR4_PSE), %eax
8010000f:	83 c8 10             	or     $0x10,%eax
  movl    %eax, %cr4
80100012:	0f 22 e0             	mov    %eax,%cr4
  # Set page directory
  movl    $(V2P_WO(entrypgdir)), %eax
80100015:	b8 00 c0 10 00       	mov    $0x10c000,%eax
  movl    %eax, %cr3
8010001a:	0f 22 d8             	mov    %eax,%cr3
  # Turn on paging.
  movl    %cr0, %eax
8010001d:	0f 20 c0             	mov    %cr0,%eax
  orl     $(CR0_PG|CR0_WP), %eax
80100020:	0d 00 00 01 80       	or     $0x80010000,%eax
  movl    %eax, %cr0
80100025:	0f 22 c0             	mov    %eax,%cr0

  # Set up the stack pointer.
  movl $(stack + KSTACKSIZE), %esp
80100028:	bc 70 e6 10 80       	mov    $0x8010e670,%esp

  # Jump to main(), and switch to executing at
  # high addresses. The indirect call is needed because
  # the assembler produces a PC-relative instruction
  # for a direct jump.
  mov $main, %eax
8010002d:	b8 2d 39 10 80       	mov    $0x8010392d,%eax
  jmp *%eax
80100032:	ff e0                	jmp    *%eax

80100034 <binit>:
  struct buf head;
} bcache;

void
binit(void)
{
80100034:	55                   	push   %ebp
80100035:	89 e5                	mov    %esp,%ebp
80100037:	83 ec 18             	sub    $0x18,%esp
  struct buf *b;

  initlock(&bcache.lock, "bcache");
8010003a:	83 ec 08             	sub    $0x8,%esp
8010003d:	68 f0 a0 10 80       	push   $0x8010a0f0
80100042:	68 80 e6 10 80       	push   $0x8010e680
80100047:	e8 c7 69 00 00       	call   80106a13 <initlock>
8010004c:	83 c4 10             	add    $0x10,%esp

  // Create linked list of buffers
  bcache.head.prev = &bcache.head;
8010004f:	c7 05 90 25 11 80 84 	movl   $0x80112584,0x80112590
80100056:	25 11 80 
  bcache.head.next = &bcache.head;
80100059:	c7 05 94 25 11 80 84 	movl   $0x80112584,0x80112594
80100060:	25 11 80 
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
80100063:	c7 45 f4 b4 e6 10 80 	movl   $0x8010e6b4,-0xc(%ebp)
8010006a:	eb 3a                	jmp    801000a6 <binit+0x72>
    b->next = bcache.head.next;
8010006c:	8b 15 94 25 11 80    	mov    0x80112594,%edx
80100072:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100075:	89 50 10             	mov    %edx,0x10(%eax)
    b->prev = &bcache.head;
80100078:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010007b:	c7 40 0c 84 25 11 80 	movl   $0x80112584,0xc(%eax)
    b->dev = -1;
80100082:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100085:	c7 40 04 ff ff ff ff 	movl   $0xffffffff,0x4(%eax)
    bcache.head.next->prev = b;
8010008c:	a1 94 25 11 80       	mov    0x80112594,%eax
80100091:	8b 55 f4             	mov    -0xc(%ebp),%edx
80100094:	89 50 0c             	mov    %edx,0xc(%eax)
    bcache.head.next = b;
80100097:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010009a:	a3 94 25 11 80       	mov    %eax,0x80112594
  initlock(&bcache.lock, "bcache");

  // Create linked list of buffers
  bcache.head.prev = &bcache.head;
  bcache.head.next = &bcache.head;
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
8010009f:	81 45 f4 18 02 00 00 	addl   $0x218,-0xc(%ebp)
801000a6:	b8 84 25 11 80       	mov    $0x80112584,%eax
801000ab:	39 45 f4             	cmp    %eax,-0xc(%ebp)
801000ae:	72 bc                	jb     8010006c <binit+0x38>
    b->prev = &bcache.head;
    b->dev = -1;
    bcache.head.next->prev = b;
    bcache.head.next = b;
  }
}
801000b0:	90                   	nop
801000b1:	c9                   	leave  
801000b2:	c3                   	ret    

801000b3 <bget>:
// Look through buffer cache for block on device dev.
// If not found, allocate a buffer.
// In either case, return B_BUSY buffer.
static struct buf*
bget(uint dev, uint blockno)
{
801000b3:	55                   	push   %ebp
801000b4:	89 e5                	mov    %esp,%ebp
801000b6:	83 ec 18             	sub    $0x18,%esp
  struct buf *b;

  acquire(&bcache.lock);
801000b9:	83 ec 0c             	sub    $0xc,%esp
801000bc:	68 80 e6 10 80       	push   $0x8010e680
801000c1:	e8 6f 69 00 00       	call   80106a35 <acquire>
801000c6:	83 c4 10             	add    $0x10,%esp

 loop:
  // Is the block already cached?
  for(b = bcache.head.next; b != &bcache.head; b = b->next){
801000c9:	a1 94 25 11 80       	mov    0x80112594,%eax
801000ce:	89 45 f4             	mov    %eax,-0xc(%ebp)
801000d1:	eb 67                	jmp    8010013a <bget+0x87>
    if(b->dev == dev && b->blockno == blockno){
801000d3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801000d6:	8b 40 04             	mov    0x4(%eax),%eax
801000d9:	3b 45 08             	cmp    0x8(%ebp),%eax
801000dc:	75 53                	jne    80100131 <bget+0x7e>
801000de:	8b 45 f4             	mov    -0xc(%ebp),%eax
801000e1:	8b 40 08             	mov    0x8(%eax),%eax
801000e4:	3b 45 0c             	cmp    0xc(%ebp),%eax
801000e7:	75 48                	jne    80100131 <bget+0x7e>
      if(!(b->flags & B_BUSY)){
801000e9:	8b 45 f4             	mov    -0xc(%ebp),%eax
801000ec:	8b 00                	mov    (%eax),%eax
801000ee:	83 e0 01             	and    $0x1,%eax
801000f1:	85 c0                	test   %eax,%eax
801000f3:	75 27                	jne    8010011c <bget+0x69>
        b->flags |= B_BUSY;
801000f5:	8b 45 f4             	mov    -0xc(%ebp),%eax
801000f8:	8b 00                	mov    (%eax),%eax
801000fa:	83 c8 01             	or     $0x1,%eax
801000fd:	89 c2                	mov    %eax,%edx
801000ff:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100102:	89 10                	mov    %edx,(%eax)
        release(&bcache.lock);
80100104:	83 ec 0c             	sub    $0xc,%esp
80100107:	68 80 e6 10 80       	push   $0x8010e680
8010010c:	e8 8b 69 00 00       	call   80106a9c <release>
80100111:	83 c4 10             	add    $0x10,%esp
        return b;
80100114:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100117:	e9 98 00 00 00       	jmp    801001b4 <bget+0x101>
      }
      sleep(b, &bcache.lock);
8010011c:	83 ec 08             	sub    $0x8,%esp
8010011f:	68 80 e6 10 80       	push   $0x8010e680
80100124:	ff 75 f4             	pushl  -0xc(%ebp)
80100127:	e8 c0 52 00 00       	call   801053ec <sleep>
8010012c:	83 c4 10             	add    $0x10,%esp
      goto loop;
8010012f:	eb 98                	jmp    801000c9 <bget+0x16>

  acquire(&bcache.lock);

 loop:
  // Is the block already cached?
  for(b = bcache.head.next; b != &bcache.head; b = b->next){
80100131:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100134:	8b 40 10             	mov    0x10(%eax),%eax
80100137:	89 45 f4             	mov    %eax,-0xc(%ebp)
8010013a:	81 7d f4 84 25 11 80 	cmpl   $0x80112584,-0xc(%ebp)
80100141:	75 90                	jne    801000d3 <bget+0x20>
  }

  // Not cached; recycle some non-busy and clean buffer.
  // "clean" because B_DIRTY and !B_BUSY means log.c
  // hasn't yet committed the changes to the buffer.
  for(b = bcache.head.prev; b != &bcache.head; b = b->prev){
80100143:	a1 90 25 11 80       	mov    0x80112590,%eax
80100148:	89 45 f4             	mov    %eax,-0xc(%ebp)
8010014b:	eb 51                	jmp    8010019e <bget+0xeb>
    if((b->flags & B_BUSY) == 0 && (b->flags & B_DIRTY) == 0){
8010014d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100150:	8b 00                	mov    (%eax),%eax
80100152:	83 e0 01             	and    $0x1,%eax
80100155:	85 c0                	test   %eax,%eax
80100157:	75 3c                	jne    80100195 <bget+0xe2>
80100159:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010015c:	8b 00                	mov    (%eax),%eax
8010015e:	83 e0 04             	and    $0x4,%eax
80100161:	85 c0                	test   %eax,%eax
80100163:	75 30                	jne    80100195 <bget+0xe2>
      b->dev = dev;
80100165:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100168:	8b 55 08             	mov    0x8(%ebp),%edx
8010016b:	89 50 04             	mov    %edx,0x4(%eax)
      b->blockno = blockno;
8010016e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100171:	8b 55 0c             	mov    0xc(%ebp),%edx
80100174:	89 50 08             	mov    %edx,0x8(%eax)
      b->flags = B_BUSY;
80100177:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010017a:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
      release(&bcache.lock);
80100180:	83 ec 0c             	sub    $0xc,%esp
80100183:	68 80 e6 10 80       	push   $0x8010e680
80100188:	e8 0f 69 00 00       	call   80106a9c <release>
8010018d:	83 c4 10             	add    $0x10,%esp
      return b;
80100190:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100193:	eb 1f                	jmp    801001b4 <bget+0x101>
  }

  // Not cached; recycle some non-busy and clean buffer.
  // "clean" because B_DIRTY and !B_BUSY means log.c
  // hasn't yet committed the changes to the buffer.
  for(b = bcache.head.prev; b != &bcache.head; b = b->prev){
80100195:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100198:	8b 40 0c             	mov    0xc(%eax),%eax
8010019b:	89 45 f4             	mov    %eax,-0xc(%ebp)
8010019e:	81 7d f4 84 25 11 80 	cmpl   $0x80112584,-0xc(%ebp)
801001a5:	75 a6                	jne    8010014d <bget+0x9a>
      b->flags = B_BUSY;
      release(&bcache.lock);
      return b;
    }
  }
  panic("bget: no buffers");
801001a7:	83 ec 0c             	sub    $0xc,%esp
801001aa:	68 f7 a0 10 80       	push   $0x8010a0f7
801001af:	e8 b2 03 00 00       	call   80100566 <panic>
}
801001b4:	c9                   	leave  
801001b5:	c3                   	ret    

801001b6 <bread>:

// Return a B_BUSY buf with the contents of the indicated block.
struct buf*
bread(uint dev, uint blockno)
{
801001b6:	55                   	push   %ebp
801001b7:	89 e5                	mov    %esp,%ebp
801001b9:	83 ec 18             	sub    $0x18,%esp
  struct buf *b;

  b = bget(dev, blockno);
801001bc:	83 ec 08             	sub    $0x8,%esp
801001bf:	ff 75 0c             	pushl  0xc(%ebp)
801001c2:	ff 75 08             	pushl  0x8(%ebp)
801001c5:	e8 e9 fe ff ff       	call   801000b3 <bget>
801001ca:	83 c4 10             	add    $0x10,%esp
801001cd:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(!(b->flags & B_VALID)) {
801001d0:	8b 45 f4             	mov    -0xc(%ebp),%eax
801001d3:	8b 00                	mov    (%eax),%eax
801001d5:	83 e0 02             	and    $0x2,%eax
801001d8:	85 c0                	test   %eax,%eax
801001da:	75 0e                	jne    801001ea <bread+0x34>
    iderw(b);
801001dc:	83 ec 0c             	sub    $0xc,%esp
801001df:	ff 75 f4             	pushl  -0xc(%ebp)
801001e2:	e8 c4 27 00 00       	call   801029ab <iderw>
801001e7:	83 c4 10             	add    $0x10,%esp
  }
  return b;
801001ea:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
801001ed:	c9                   	leave  
801001ee:	c3                   	ret    

801001ef <bwrite>:

// Write b's contents to disk.  Must be B_BUSY.
void
bwrite(struct buf *b)
{
801001ef:	55                   	push   %ebp
801001f0:	89 e5                	mov    %esp,%ebp
801001f2:	83 ec 08             	sub    $0x8,%esp
  if((b->flags & B_BUSY) == 0)
801001f5:	8b 45 08             	mov    0x8(%ebp),%eax
801001f8:	8b 00                	mov    (%eax),%eax
801001fa:	83 e0 01             	and    $0x1,%eax
801001fd:	85 c0                	test   %eax,%eax
801001ff:	75 0d                	jne    8010020e <bwrite+0x1f>
    panic("bwrite");
80100201:	83 ec 0c             	sub    $0xc,%esp
80100204:	68 08 a1 10 80       	push   $0x8010a108
80100209:	e8 58 03 00 00       	call   80100566 <panic>
  b->flags |= B_DIRTY;
8010020e:	8b 45 08             	mov    0x8(%ebp),%eax
80100211:	8b 00                	mov    (%eax),%eax
80100213:	83 c8 04             	or     $0x4,%eax
80100216:	89 c2                	mov    %eax,%edx
80100218:	8b 45 08             	mov    0x8(%ebp),%eax
8010021b:	89 10                	mov    %edx,(%eax)
  iderw(b);
8010021d:	83 ec 0c             	sub    $0xc,%esp
80100220:	ff 75 08             	pushl  0x8(%ebp)
80100223:	e8 83 27 00 00       	call   801029ab <iderw>
80100228:	83 c4 10             	add    $0x10,%esp
}
8010022b:	90                   	nop
8010022c:	c9                   	leave  
8010022d:	c3                   	ret    

8010022e <brelse>:

// Release a B_BUSY buffer.
// Move to the head of the MRU list.
void
brelse(struct buf *b)
{
8010022e:	55                   	push   %ebp
8010022f:	89 e5                	mov    %esp,%ebp
80100231:	83 ec 08             	sub    $0x8,%esp
  if((b->flags & B_BUSY) == 0)
80100234:	8b 45 08             	mov    0x8(%ebp),%eax
80100237:	8b 00                	mov    (%eax),%eax
80100239:	83 e0 01             	and    $0x1,%eax
8010023c:	85 c0                	test   %eax,%eax
8010023e:	75 0d                	jne    8010024d <brelse+0x1f>
    panic("brelse");
80100240:	83 ec 0c             	sub    $0xc,%esp
80100243:	68 0f a1 10 80       	push   $0x8010a10f
80100248:	e8 19 03 00 00       	call   80100566 <panic>

  acquire(&bcache.lock);
8010024d:	83 ec 0c             	sub    $0xc,%esp
80100250:	68 80 e6 10 80       	push   $0x8010e680
80100255:	e8 db 67 00 00       	call   80106a35 <acquire>
8010025a:	83 c4 10             	add    $0x10,%esp

  b->next->prev = b->prev;
8010025d:	8b 45 08             	mov    0x8(%ebp),%eax
80100260:	8b 40 10             	mov    0x10(%eax),%eax
80100263:	8b 55 08             	mov    0x8(%ebp),%edx
80100266:	8b 52 0c             	mov    0xc(%edx),%edx
80100269:	89 50 0c             	mov    %edx,0xc(%eax)
  b->prev->next = b->next;
8010026c:	8b 45 08             	mov    0x8(%ebp),%eax
8010026f:	8b 40 0c             	mov    0xc(%eax),%eax
80100272:	8b 55 08             	mov    0x8(%ebp),%edx
80100275:	8b 52 10             	mov    0x10(%edx),%edx
80100278:	89 50 10             	mov    %edx,0x10(%eax)
  b->next = bcache.head.next;
8010027b:	8b 15 94 25 11 80    	mov    0x80112594,%edx
80100281:	8b 45 08             	mov    0x8(%ebp),%eax
80100284:	89 50 10             	mov    %edx,0x10(%eax)
  b->prev = &bcache.head;
80100287:	8b 45 08             	mov    0x8(%ebp),%eax
8010028a:	c7 40 0c 84 25 11 80 	movl   $0x80112584,0xc(%eax)
  bcache.head.next->prev = b;
80100291:	a1 94 25 11 80       	mov    0x80112594,%eax
80100296:	8b 55 08             	mov    0x8(%ebp),%edx
80100299:	89 50 0c             	mov    %edx,0xc(%eax)
  bcache.head.next = b;
8010029c:	8b 45 08             	mov    0x8(%ebp),%eax
8010029f:	a3 94 25 11 80       	mov    %eax,0x80112594

  b->flags &= ~B_BUSY;
801002a4:	8b 45 08             	mov    0x8(%ebp),%eax
801002a7:	8b 00                	mov    (%eax),%eax
801002a9:	83 e0 fe             	and    $0xfffffffe,%eax
801002ac:	89 c2                	mov    %eax,%edx
801002ae:	8b 45 08             	mov    0x8(%ebp),%eax
801002b1:	89 10                	mov    %edx,(%eax)
  wakeup(b);
801002b3:	83 ec 0c             	sub    $0xc,%esp
801002b6:	ff 75 08             	pushl  0x8(%ebp)
801002b9:	e8 d7 52 00 00       	call   80105595 <wakeup>
801002be:	83 c4 10             	add    $0x10,%esp

  release(&bcache.lock);
801002c1:	83 ec 0c             	sub    $0xc,%esp
801002c4:	68 80 e6 10 80       	push   $0x8010e680
801002c9:	e8 ce 67 00 00       	call   80106a9c <release>
801002ce:	83 c4 10             	add    $0x10,%esp
}
801002d1:	90                   	nop
801002d2:	c9                   	leave  
801002d3:	c3                   	ret    

801002d4 <inb>:

// end of CS333 added routines

static inline uchar
inb(ushort port)
{
801002d4:	55                   	push   %ebp
801002d5:	89 e5                	mov    %esp,%ebp
801002d7:	83 ec 14             	sub    $0x14,%esp
801002da:	8b 45 08             	mov    0x8(%ebp),%eax
801002dd:	66 89 45 ec          	mov    %ax,-0x14(%ebp)
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801002e1:	0f b7 45 ec          	movzwl -0x14(%ebp),%eax
801002e5:	89 c2                	mov    %eax,%edx
801002e7:	ec                   	in     (%dx),%al
801002e8:	88 45 ff             	mov    %al,-0x1(%ebp)
  return data;
801002eb:	0f b6 45 ff          	movzbl -0x1(%ebp),%eax
}
801002ef:	c9                   	leave  
801002f0:	c3                   	ret    

801002f1 <outb>:
               "memory", "cc");
}

static inline void
outb(ushort port, uchar data)
{
801002f1:	55                   	push   %ebp
801002f2:	89 e5                	mov    %esp,%ebp
801002f4:	83 ec 08             	sub    $0x8,%esp
801002f7:	8b 55 08             	mov    0x8(%ebp),%edx
801002fa:	8b 45 0c             	mov    0xc(%ebp),%eax
801002fd:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
80100301:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80100304:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
80100308:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
8010030c:	ee                   	out    %al,(%dx)
}
8010030d:	90                   	nop
8010030e:	c9                   	leave  
8010030f:	c3                   	ret    

80100310 <cli>:
  asm volatile("movw %0, %%gs" : : "r" (v));
}

static inline void
cli(void)
{
80100310:	55                   	push   %ebp
80100311:	89 e5                	mov    %esp,%ebp
  asm volatile("cli");
80100313:	fa                   	cli    
}
80100314:	90                   	nop
80100315:	5d                   	pop    %ebp
80100316:	c3                   	ret    

80100317 <printint>:
  int locking;
} cons;

static void
printint(int xx, int base, int sign)
{
80100317:	55                   	push   %ebp
80100318:	89 e5                	mov    %esp,%ebp
8010031a:	53                   	push   %ebx
8010031b:	83 ec 24             	sub    $0x24,%esp
  static char digits[] = "0123456789abcdef";
  char buf[16];
  int i;
  uint x;

  if(sign && (sign = xx < 0))
8010031e:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80100322:	74 1c                	je     80100340 <printint+0x29>
80100324:	8b 45 08             	mov    0x8(%ebp),%eax
80100327:	c1 e8 1f             	shr    $0x1f,%eax
8010032a:	0f b6 c0             	movzbl %al,%eax
8010032d:	89 45 10             	mov    %eax,0x10(%ebp)
80100330:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80100334:	74 0a                	je     80100340 <printint+0x29>
    x = -xx;
80100336:	8b 45 08             	mov    0x8(%ebp),%eax
80100339:	f7 d8                	neg    %eax
8010033b:	89 45 f0             	mov    %eax,-0x10(%ebp)
8010033e:	eb 06                	jmp    80100346 <printint+0x2f>
  else
    x = xx;
80100340:	8b 45 08             	mov    0x8(%ebp),%eax
80100343:	89 45 f0             	mov    %eax,-0x10(%ebp)

  i = 0;
80100346:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  do{
    buf[i++] = digits[x % base];
8010034d:	8b 4d f4             	mov    -0xc(%ebp),%ecx
80100350:	8d 41 01             	lea    0x1(%ecx),%eax
80100353:	89 45 f4             	mov    %eax,-0xc(%ebp)
80100356:	8b 5d 0c             	mov    0xc(%ebp),%ebx
80100359:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010035c:	ba 00 00 00 00       	mov    $0x0,%edx
80100361:	f7 f3                	div    %ebx
80100363:	89 d0                	mov    %edx,%eax
80100365:	0f b6 80 04 b0 10 80 	movzbl -0x7fef4ffc(%eax),%eax
8010036c:	88 44 0d e0          	mov    %al,-0x20(%ebp,%ecx,1)
  }while((x /= base) != 0);
80100370:	8b 5d 0c             	mov    0xc(%ebp),%ebx
80100373:	8b 45 f0             	mov    -0x10(%ebp),%eax
80100376:	ba 00 00 00 00       	mov    $0x0,%edx
8010037b:	f7 f3                	div    %ebx
8010037d:	89 45 f0             	mov    %eax,-0x10(%ebp)
80100380:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80100384:	75 c7                	jne    8010034d <printint+0x36>

  if(sign)
80100386:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
8010038a:	74 2a                	je     801003b6 <printint+0x9f>
    buf[i++] = '-';
8010038c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010038f:	8d 50 01             	lea    0x1(%eax),%edx
80100392:	89 55 f4             	mov    %edx,-0xc(%ebp)
80100395:	c6 44 05 e0 2d       	movb   $0x2d,-0x20(%ebp,%eax,1)

  while(--i >= 0)
8010039a:	eb 1a                	jmp    801003b6 <printint+0x9f>
    consputc(buf[i]);
8010039c:	8d 55 e0             	lea    -0x20(%ebp),%edx
8010039f:	8b 45 f4             	mov    -0xc(%ebp),%eax
801003a2:	01 d0                	add    %edx,%eax
801003a4:	0f b6 00             	movzbl (%eax),%eax
801003a7:	0f be c0             	movsbl %al,%eax
801003aa:	83 ec 0c             	sub    $0xc,%esp
801003ad:	50                   	push   %eax
801003ae:	e8 df 03 00 00       	call   80100792 <consputc>
801003b3:	83 c4 10             	add    $0x10,%esp
  }while((x /= base) != 0);

  if(sign)
    buf[i++] = '-';

  while(--i >= 0)
801003b6:	83 6d f4 01          	subl   $0x1,-0xc(%ebp)
801003ba:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801003be:	79 dc                	jns    8010039c <printint+0x85>
    consputc(buf[i]);
}
801003c0:	90                   	nop
801003c1:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801003c4:	c9                   	leave  
801003c5:	c3                   	ret    

801003c6 <cprintf>:

// Print to the console. only understands %d, %x, %p, %s.
void
cprintf(char *fmt, ...)
{
801003c6:	55                   	push   %ebp
801003c7:	89 e5                	mov    %esp,%ebp
801003c9:	83 ec 28             	sub    $0x28,%esp
  int i, c, locking;
  uint *argp;
  char *s;

  locking = cons.locking;
801003cc:	a1 14 d6 10 80       	mov    0x8010d614,%eax
801003d1:	89 45 e8             	mov    %eax,-0x18(%ebp)
  if(locking)
801003d4:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
801003d8:	74 10                	je     801003ea <cprintf+0x24>
    acquire(&cons.lock);
801003da:	83 ec 0c             	sub    $0xc,%esp
801003dd:	68 e0 d5 10 80       	push   $0x8010d5e0
801003e2:	e8 4e 66 00 00       	call   80106a35 <acquire>
801003e7:	83 c4 10             	add    $0x10,%esp

  if (fmt == 0)
801003ea:	8b 45 08             	mov    0x8(%ebp),%eax
801003ed:	85 c0                	test   %eax,%eax
801003ef:	75 0d                	jne    801003fe <cprintf+0x38>
    panic("null fmt");
801003f1:	83 ec 0c             	sub    $0xc,%esp
801003f4:	68 16 a1 10 80       	push   $0x8010a116
801003f9:	e8 68 01 00 00       	call   80100566 <panic>

  argp = (uint*)(void*)(&fmt + 1);
801003fe:	8d 45 0c             	lea    0xc(%ebp),%eax
80100401:	89 45 f0             	mov    %eax,-0x10(%ebp)
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
80100404:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
8010040b:	e9 1a 01 00 00       	jmp    8010052a <cprintf+0x164>
    if(c != '%'){
80100410:	83 7d e4 25          	cmpl   $0x25,-0x1c(%ebp)
80100414:	74 13                	je     80100429 <cprintf+0x63>
      consputc(c);
80100416:	83 ec 0c             	sub    $0xc,%esp
80100419:	ff 75 e4             	pushl  -0x1c(%ebp)
8010041c:	e8 71 03 00 00       	call   80100792 <consputc>
80100421:	83 c4 10             	add    $0x10,%esp
      continue;
80100424:	e9 fd 00 00 00       	jmp    80100526 <cprintf+0x160>
    }
    c = fmt[++i] & 0xff;
80100429:	8b 55 08             	mov    0x8(%ebp),%edx
8010042c:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80100430:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100433:	01 d0                	add    %edx,%eax
80100435:	0f b6 00             	movzbl (%eax),%eax
80100438:	0f be c0             	movsbl %al,%eax
8010043b:	25 ff 00 00 00       	and    $0xff,%eax
80100440:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    if(c == 0)
80100443:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
80100447:	0f 84 ff 00 00 00    	je     8010054c <cprintf+0x186>
      break;
    switch(c){
8010044d:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100450:	83 f8 70             	cmp    $0x70,%eax
80100453:	74 47                	je     8010049c <cprintf+0xd6>
80100455:	83 f8 70             	cmp    $0x70,%eax
80100458:	7f 13                	jg     8010046d <cprintf+0xa7>
8010045a:	83 f8 25             	cmp    $0x25,%eax
8010045d:	0f 84 98 00 00 00    	je     801004fb <cprintf+0x135>
80100463:	83 f8 64             	cmp    $0x64,%eax
80100466:	74 14                	je     8010047c <cprintf+0xb6>
80100468:	e9 9d 00 00 00       	jmp    8010050a <cprintf+0x144>
8010046d:	83 f8 73             	cmp    $0x73,%eax
80100470:	74 47                	je     801004b9 <cprintf+0xf3>
80100472:	83 f8 78             	cmp    $0x78,%eax
80100475:	74 25                	je     8010049c <cprintf+0xd6>
80100477:	e9 8e 00 00 00       	jmp    8010050a <cprintf+0x144>
    case 'd':
      printint(*argp++, 10, 1);
8010047c:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010047f:	8d 50 04             	lea    0x4(%eax),%edx
80100482:	89 55 f0             	mov    %edx,-0x10(%ebp)
80100485:	8b 00                	mov    (%eax),%eax
80100487:	83 ec 04             	sub    $0x4,%esp
8010048a:	6a 01                	push   $0x1
8010048c:	6a 0a                	push   $0xa
8010048e:	50                   	push   %eax
8010048f:	e8 83 fe ff ff       	call   80100317 <printint>
80100494:	83 c4 10             	add    $0x10,%esp
      break;
80100497:	e9 8a 00 00 00       	jmp    80100526 <cprintf+0x160>
    case 'x':
    case 'p':
      printint(*argp++, 16, 0);
8010049c:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010049f:	8d 50 04             	lea    0x4(%eax),%edx
801004a2:	89 55 f0             	mov    %edx,-0x10(%ebp)
801004a5:	8b 00                	mov    (%eax),%eax
801004a7:	83 ec 04             	sub    $0x4,%esp
801004aa:	6a 00                	push   $0x0
801004ac:	6a 10                	push   $0x10
801004ae:	50                   	push   %eax
801004af:	e8 63 fe ff ff       	call   80100317 <printint>
801004b4:	83 c4 10             	add    $0x10,%esp
      break;
801004b7:	eb 6d                	jmp    80100526 <cprintf+0x160>
    case 's':
      if((s = (char*)*argp++) == 0)
801004b9:	8b 45 f0             	mov    -0x10(%ebp),%eax
801004bc:	8d 50 04             	lea    0x4(%eax),%edx
801004bf:	89 55 f0             	mov    %edx,-0x10(%ebp)
801004c2:	8b 00                	mov    (%eax),%eax
801004c4:	89 45 ec             	mov    %eax,-0x14(%ebp)
801004c7:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
801004cb:	75 22                	jne    801004ef <cprintf+0x129>
        s = "(null)";
801004cd:	c7 45 ec 1f a1 10 80 	movl   $0x8010a11f,-0x14(%ebp)
      for(; *s; s++)
801004d4:	eb 19                	jmp    801004ef <cprintf+0x129>
        consputc(*s);
801004d6:	8b 45 ec             	mov    -0x14(%ebp),%eax
801004d9:	0f b6 00             	movzbl (%eax),%eax
801004dc:	0f be c0             	movsbl %al,%eax
801004df:	83 ec 0c             	sub    $0xc,%esp
801004e2:	50                   	push   %eax
801004e3:	e8 aa 02 00 00       	call   80100792 <consputc>
801004e8:	83 c4 10             	add    $0x10,%esp
      printint(*argp++, 16, 0);
      break;
    case 's':
      if((s = (char*)*argp++) == 0)
        s = "(null)";
      for(; *s; s++)
801004eb:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
801004ef:	8b 45 ec             	mov    -0x14(%ebp),%eax
801004f2:	0f b6 00             	movzbl (%eax),%eax
801004f5:	84 c0                	test   %al,%al
801004f7:	75 dd                	jne    801004d6 <cprintf+0x110>
        consputc(*s);
      break;
801004f9:	eb 2b                	jmp    80100526 <cprintf+0x160>
    case '%':
      consputc('%');
801004fb:	83 ec 0c             	sub    $0xc,%esp
801004fe:	6a 25                	push   $0x25
80100500:	e8 8d 02 00 00       	call   80100792 <consputc>
80100505:	83 c4 10             	add    $0x10,%esp
      break;
80100508:	eb 1c                	jmp    80100526 <cprintf+0x160>
    default:
      // Print unknown % sequence to draw attention.
      consputc('%');
8010050a:	83 ec 0c             	sub    $0xc,%esp
8010050d:	6a 25                	push   $0x25
8010050f:	e8 7e 02 00 00       	call   80100792 <consputc>
80100514:	83 c4 10             	add    $0x10,%esp
      consputc(c);
80100517:	83 ec 0c             	sub    $0xc,%esp
8010051a:	ff 75 e4             	pushl  -0x1c(%ebp)
8010051d:	e8 70 02 00 00       	call   80100792 <consputc>
80100522:	83 c4 10             	add    $0x10,%esp
      break;
80100525:	90                   	nop

  if (fmt == 0)
    panic("null fmt");

  argp = (uint*)(void*)(&fmt + 1);
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
80100526:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
8010052a:	8b 55 08             	mov    0x8(%ebp),%edx
8010052d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100530:	01 d0                	add    %edx,%eax
80100532:	0f b6 00             	movzbl (%eax),%eax
80100535:	0f be c0             	movsbl %al,%eax
80100538:	25 ff 00 00 00       	and    $0xff,%eax
8010053d:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80100540:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
80100544:	0f 85 c6 fe ff ff    	jne    80100410 <cprintf+0x4a>
8010054a:	eb 01                	jmp    8010054d <cprintf+0x187>
      consputc(c);
      continue;
    }
    c = fmt[++i] & 0xff;
    if(c == 0)
      break;
8010054c:	90                   	nop
      consputc(c);
      break;
    }
  }

  if(locking)
8010054d:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
80100551:	74 10                	je     80100563 <cprintf+0x19d>
    release(&cons.lock);
80100553:	83 ec 0c             	sub    $0xc,%esp
80100556:	68 e0 d5 10 80       	push   $0x8010d5e0
8010055b:	e8 3c 65 00 00       	call   80106a9c <release>
80100560:	83 c4 10             	add    $0x10,%esp
}
80100563:	90                   	nop
80100564:	c9                   	leave  
80100565:	c3                   	ret    

80100566 <panic>:

void
panic(char *s)
{
80100566:	55                   	push   %ebp
80100567:	89 e5                	mov    %esp,%ebp
80100569:	83 ec 38             	sub    $0x38,%esp
  int i;
  uint pcs[10];
  
  cli();
8010056c:	e8 9f fd ff ff       	call   80100310 <cli>
  cons.locking = 0;
80100571:	c7 05 14 d6 10 80 00 	movl   $0x0,0x8010d614
80100578:	00 00 00 
  cprintf("cpu%d: panic: ", cpu->id);
8010057b:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80100581:	0f b6 00             	movzbl (%eax),%eax
80100584:	0f b6 c0             	movzbl %al,%eax
80100587:	83 ec 08             	sub    $0x8,%esp
8010058a:	50                   	push   %eax
8010058b:	68 26 a1 10 80       	push   $0x8010a126
80100590:	e8 31 fe ff ff       	call   801003c6 <cprintf>
80100595:	83 c4 10             	add    $0x10,%esp
  cprintf(s);
80100598:	8b 45 08             	mov    0x8(%ebp),%eax
8010059b:	83 ec 0c             	sub    $0xc,%esp
8010059e:	50                   	push   %eax
8010059f:	e8 22 fe ff ff       	call   801003c6 <cprintf>
801005a4:	83 c4 10             	add    $0x10,%esp
  cprintf("\n");
801005a7:	83 ec 0c             	sub    $0xc,%esp
801005aa:	68 35 a1 10 80       	push   $0x8010a135
801005af:	e8 12 fe ff ff       	call   801003c6 <cprintf>
801005b4:	83 c4 10             	add    $0x10,%esp
  getcallerpcs(&s, pcs);
801005b7:	83 ec 08             	sub    $0x8,%esp
801005ba:	8d 45 cc             	lea    -0x34(%ebp),%eax
801005bd:	50                   	push   %eax
801005be:	8d 45 08             	lea    0x8(%ebp),%eax
801005c1:	50                   	push   %eax
801005c2:	e8 27 65 00 00       	call   80106aee <getcallerpcs>
801005c7:	83 c4 10             	add    $0x10,%esp
  for(i=0; i<10; i++)
801005ca:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
801005d1:	eb 1c                	jmp    801005ef <panic+0x89>
    cprintf(" %p", pcs[i]);
801005d3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801005d6:	8b 44 85 cc          	mov    -0x34(%ebp,%eax,4),%eax
801005da:	83 ec 08             	sub    $0x8,%esp
801005dd:	50                   	push   %eax
801005de:	68 37 a1 10 80       	push   $0x8010a137
801005e3:	e8 de fd ff ff       	call   801003c6 <cprintf>
801005e8:	83 c4 10             	add    $0x10,%esp
  cons.locking = 0;
  cprintf("cpu%d: panic: ", cpu->id);
  cprintf(s);
  cprintf("\n");
  getcallerpcs(&s, pcs);
  for(i=0; i<10; i++)
801005eb:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
801005ef:	83 7d f4 09          	cmpl   $0x9,-0xc(%ebp)
801005f3:	7e de                	jle    801005d3 <panic+0x6d>
    cprintf(" %p", pcs[i]);
  panicked = 1; // freeze other CPU
801005f5:	c7 05 c0 d5 10 80 01 	movl   $0x1,0x8010d5c0
801005fc:	00 00 00 
  for(;;)
    ;
801005ff:	eb fe                	jmp    801005ff <panic+0x99>

80100601 <cgaputc>:
#define CRTPORT 0x3d4
static ushort *crt = (ushort*)P2V(0xb8000);  // CGA memory

static void
cgaputc(int c)
{
80100601:	55                   	push   %ebp
80100602:	89 e5                	mov    %esp,%ebp
80100604:	83 ec 18             	sub    $0x18,%esp
  int pos;
  
  // Cursor position: col + 80*row.
  outb(CRTPORT, 14);
80100607:	6a 0e                	push   $0xe
80100609:	68 d4 03 00 00       	push   $0x3d4
8010060e:	e8 de fc ff ff       	call   801002f1 <outb>
80100613:	83 c4 08             	add    $0x8,%esp
  pos = inb(CRTPORT+1) << 8;
80100616:	68 d5 03 00 00       	push   $0x3d5
8010061b:	e8 b4 fc ff ff       	call   801002d4 <inb>
80100620:	83 c4 04             	add    $0x4,%esp
80100623:	0f b6 c0             	movzbl %al,%eax
80100626:	c1 e0 08             	shl    $0x8,%eax
80100629:	89 45 f4             	mov    %eax,-0xc(%ebp)
  outb(CRTPORT, 15);
8010062c:	6a 0f                	push   $0xf
8010062e:	68 d4 03 00 00       	push   $0x3d4
80100633:	e8 b9 fc ff ff       	call   801002f1 <outb>
80100638:	83 c4 08             	add    $0x8,%esp
  pos |= inb(CRTPORT+1);
8010063b:	68 d5 03 00 00       	push   $0x3d5
80100640:	e8 8f fc ff ff       	call   801002d4 <inb>
80100645:	83 c4 04             	add    $0x4,%esp
80100648:	0f b6 c0             	movzbl %al,%eax
8010064b:	09 45 f4             	or     %eax,-0xc(%ebp)

  if(c == '\n')
8010064e:	83 7d 08 0a          	cmpl   $0xa,0x8(%ebp)
80100652:	75 30                	jne    80100684 <cgaputc+0x83>
    pos += 80 - pos%80;
80100654:	8b 4d f4             	mov    -0xc(%ebp),%ecx
80100657:	ba 67 66 66 66       	mov    $0x66666667,%edx
8010065c:	89 c8                	mov    %ecx,%eax
8010065e:	f7 ea                	imul   %edx
80100660:	c1 fa 05             	sar    $0x5,%edx
80100663:	89 c8                	mov    %ecx,%eax
80100665:	c1 f8 1f             	sar    $0x1f,%eax
80100668:	29 c2                	sub    %eax,%edx
8010066a:	89 d0                	mov    %edx,%eax
8010066c:	c1 e0 02             	shl    $0x2,%eax
8010066f:	01 d0                	add    %edx,%eax
80100671:	c1 e0 04             	shl    $0x4,%eax
80100674:	29 c1                	sub    %eax,%ecx
80100676:	89 ca                	mov    %ecx,%edx
80100678:	b8 50 00 00 00       	mov    $0x50,%eax
8010067d:	29 d0                	sub    %edx,%eax
8010067f:	01 45 f4             	add    %eax,-0xc(%ebp)
80100682:	eb 34                	jmp    801006b8 <cgaputc+0xb7>
  else if(c == BACKSPACE){
80100684:	81 7d 08 00 01 00 00 	cmpl   $0x100,0x8(%ebp)
8010068b:	75 0c                	jne    80100699 <cgaputc+0x98>
    if(pos > 0) --pos;
8010068d:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80100691:	7e 25                	jle    801006b8 <cgaputc+0xb7>
80100693:	83 6d f4 01          	subl   $0x1,-0xc(%ebp)
80100697:	eb 1f                	jmp    801006b8 <cgaputc+0xb7>
  } else
    crt[pos++] = (c&0xff) | 0x0700;  // black on white
80100699:	8b 0d 00 b0 10 80    	mov    0x8010b000,%ecx
8010069f:	8b 45 f4             	mov    -0xc(%ebp),%eax
801006a2:	8d 50 01             	lea    0x1(%eax),%edx
801006a5:	89 55 f4             	mov    %edx,-0xc(%ebp)
801006a8:	01 c0                	add    %eax,%eax
801006aa:	01 c8                	add    %ecx,%eax
801006ac:	8b 55 08             	mov    0x8(%ebp),%edx
801006af:	0f b6 d2             	movzbl %dl,%edx
801006b2:	80 ce 07             	or     $0x7,%dh
801006b5:	66 89 10             	mov    %dx,(%eax)

  if(pos < 0 || pos > 25*80)
801006b8:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801006bc:	78 09                	js     801006c7 <cgaputc+0xc6>
801006be:	81 7d f4 d0 07 00 00 	cmpl   $0x7d0,-0xc(%ebp)
801006c5:	7e 0d                	jle    801006d4 <cgaputc+0xd3>
    panic("pos under/overflow");
801006c7:	83 ec 0c             	sub    $0xc,%esp
801006ca:	68 3b a1 10 80       	push   $0x8010a13b
801006cf:	e8 92 fe ff ff       	call   80100566 <panic>
  
  if((pos/80) >= 24){  // Scroll up.
801006d4:	81 7d f4 7f 07 00 00 	cmpl   $0x77f,-0xc(%ebp)
801006db:	7e 4c                	jle    80100729 <cgaputc+0x128>
    memmove(crt, crt+80, sizeof(crt[0])*23*80);
801006dd:	a1 00 b0 10 80       	mov    0x8010b000,%eax
801006e2:	8d 90 a0 00 00 00    	lea    0xa0(%eax),%edx
801006e8:	a1 00 b0 10 80       	mov    0x8010b000,%eax
801006ed:	83 ec 04             	sub    $0x4,%esp
801006f0:	68 60 0e 00 00       	push   $0xe60
801006f5:	52                   	push   %edx
801006f6:	50                   	push   %eax
801006f7:	e8 5b 66 00 00       	call   80106d57 <memmove>
801006fc:	83 c4 10             	add    $0x10,%esp
    pos -= 80;
801006ff:	83 6d f4 50          	subl   $0x50,-0xc(%ebp)
    memset(crt+pos, 0, sizeof(crt[0])*(24*80 - pos));
80100703:	b8 80 07 00 00       	mov    $0x780,%eax
80100708:	2b 45 f4             	sub    -0xc(%ebp),%eax
8010070b:	8d 14 00             	lea    (%eax,%eax,1),%edx
8010070e:	a1 00 b0 10 80       	mov    0x8010b000,%eax
80100713:	8b 4d f4             	mov    -0xc(%ebp),%ecx
80100716:	01 c9                	add    %ecx,%ecx
80100718:	01 c8                	add    %ecx,%eax
8010071a:	83 ec 04             	sub    $0x4,%esp
8010071d:	52                   	push   %edx
8010071e:	6a 00                	push   $0x0
80100720:	50                   	push   %eax
80100721:	e8 72 65 00 00       	call   80106c98 <memset>
80100726:	83 c4 10             	add    $0x10,%esp
  }
  
  outb(CRTPORT, 14);
80100729:	83 ec 08             	sub    $0x8,%esp
8010072c:	6a 0e                	push   $0xe
8010072e:	68 d4 03 00 00       	push   $0x3d4
80100733:	e8 b9 fb ff ff       	call   801002f1 <outb>
80100738:	83 c4 10             	add    $0x10,%esp
  outb(CRTPORT+1, pos>>8);
8010073b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010073e:	c1 f8 08             	sar    $0x8,%eax
80100741:	0f b6 c0             	movzbl %al,%eax
80100744:	83 ec 08             	sub    $0x8,%esp
80100747:	50                   	push   %eax
80100748:	68 d5 03 00 00       	push   $0x3d5
8010074d:	e8 9f fb ff ff       	call   801002f1 <outb>
80100752:	83 c4 10             	add    $0x10,%esp
  outb(CRTPORT, 15);
80100755:	83 ec 08             	sub    $0x8,%esp
80100758:	6a 0f                	push   $0xf
8010075a:	68 d4 03 00 00       	push   $0x3d4
8010075f:	e8 8d fb ff ff       	call   801002f1 <outb>
80100764:	83 c4 10             	add    $0x10,%esp
  outb(CRTPORT+1, pos);
80100767:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010076a:	0f b6 c0             	movzbl %al,%eax
8010076d:	83 ec 08             	sub    $0x8,%esp
80100770:	50                   	push   %eax
80100771:	68 d5 03 00 00       	push   $0x3d5
80100776:	e8 76 fb ff ff       	call   801002f1 <outb>
8010077b:	83 c4 10             	add    $0x10,%esp
  crt[pos] = ' ' | 0x0700;
8010077e:	a1 00 b0 10 80       	mov    0x8010b000,%eax
80100783:	8b 55 f4             	mov    -0xc(%ebp),%edx
80100786:	01 d2                	add    %edx,%edx
80100788:	01 d0                	add    %edx,%eax
8010078a:	66 c7 00 20 07       	movw   $0x720,(%eax)
}
8010078f:	90                   	nop
80100790:	c9                   	leave  
80100791:	c3                   	ret    

80100792 <consputc>:

void
consputc(int c)
{
80100792:	55                   	push   %ebp
80100793:	89 e5                	mov    %esp,%ebp
80100795:	83 ec 08             	sub    $0x8,%esp
  if(panicked){
80100798:	a1 c0 d5 10 80       	mov    0x8010d5c0,%eax
8010079d:	85 c0                	test   %eax,%eax
8010079f:	74 07                	je     801007a8 <consputc+0x16>
    cli();
801007a1:	e8 6a fb ff ff       	call   80100310 <cli>
    for(;;)
      ;
801007a6:	eb fe                	jmp    801007a6 <consputc+0x14>
  }

  if(c == BACKSPACE){
801007a8:	81 7d 08 00 01 00 00 	cmpl   $0x100,0x8(%ebp)
801007af:	75 29                	jne    801007da <consputc+0x48>
    uartputc('\b'); uartputc(' '); uartputc('\b');
801007b1:	83 ec 0c             	sub    $0xc,%esp
801007b4:	6a 08                	push   $0x8
801007b6:	e8 bd 7f 00 00       	call   80108778 <uartputc>
801007bb:	83 c4 10             	add    $0x10,%esp
801007be:	83 ec 0c             	sub    $0xc,%esp
801007c1:	6a 20                	push   $0x20
801007c3:	e8 b0 7f 00 00       	call   80108778 <uartputc>
801007c8:	83 c4 10             	add    $0x10,%esp
801007cb:	83 ec 0c             	sub    $0xc,%esp
801007ce:	6a 08                	push   $0x8
801007d0:	e8 a3 7f 00 00       	call   80108778 <uartputc>
801007d5:	83 c4 10             	add    $0x10,%esp
801007d8:	eb 0e                	jmp    801007e8 <consputc+0x56>
  } else
    uartputc(c);
801007da:	83 ec 0c             	sub    $0xc,%esp
801007dd:	ff 75 08             	pushl  0x8(%ebp)
801007e0:	e8 93 7f 00 00       	call   80108778 <uartputc>
801007e5:	83 c4 10             	add    $0x10,%esp
  cgaputc(c);
801007e8:	83 ec 0c             	sub    $0xc,%esp
801007eb:	ff 75 08             	pushl  0x8(%ebp)
801007ee:	e8 0e fe ff ff       	call   80100601 <cgaputc>
801007f3:	83 c4 10             	add    $0x10,%esp
}
801007f6:	90                   	nop
801007f7:	c9                   	leave  
801007f8:	c3                   	ret    

801007f9 <consoleintr>:

#define C(x)  ((x)-'@')  // Control-x

void
consoleintr(int (*getc)(void))
{
801007f9:	55                   	push   %ebp
801007fa:	89 e5                	mov    %esp,%ebp
801007fc:	83 ec 28             	sub    $0x28,%esp
  int c, doprocdump = 0;
801007ff:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  #ifdef CS333_P3P4
  int countFL = 0, countRL = 0, countSL = 0, countZL = 0;
80100806:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
8010080d:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
80100814:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)
8010081b:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
  #endif

  acquire(&cons.lock);
80100822:	83 ec 0c             	sub    $0xc,%esp
80100825:	68 e0 d5 10 80       	push   $0x8010d5e0
8010082a:	e8 06 62 00 00       	call   80106a35 <acquire>
8010082f:	83 c4 10             	add    $0x10,%esp
  while((c = getc()) >= 0){
80100832:	e9 9a 01 00 00       	jmp    801009d1 <consoleintr+0x1d8>
    switch(c){
80100837:	8b 45 e0             	mov    -0x20(%ebp),%eax
8010083a:	83 f8 12             	cmp    $0x12,%eax
8010083d:	74 5c                	je     8010089b <consoleintr+0xa2>
8010083f:	83 f8 12             	cmp    $0x12,%eax
80100842:	7f 18                	jg     8010085c <consoleintr+0x63>
80100844:	83 f8 08             	cmp    $0x8,%eax
80100847:	0f 84 bd 00 00 00    	je     8010090a <consoleintr+0x111>
8010084d:	83 f8 10             	cmp    $0x10,%eax
80100850:	74 31                	je     80100883 <consoleintr+0x8a>
80100852:	83 f8 06             	cmp    $0x6,%eax
80100855:	74 38                	je     8010088f <consoleintr+0x96>
80100857:	e9 e3 00 00 00       	jmp    8010093f <consoleintr+0x146>
8010085c:	83 f8 15             	cmp    $0x15,%eax
8010085f:	74 7b                	je     801008dc <consoleintr+0xe3>
80100861:	83 f8 15             	cmp    $0x15,%eax
80100864:	7f 0a                	jg     80100870 <consoleintr+0x77>
80100866:	83 f8 13             	cmp    $0x13,%eax
80100869:	74 3c                	je     801008a7 <consoleintr+0xae>
8010086b:	e9 cf 00 00 00       	jmp    8010093f <consoleintr+0x146>
80100870:	83 f8 1a             	cmp    $0x1a,%eax
80100873:	74 3e                	je     801008b3 <consoleintr+0xba>
80100875:	83 f8 7f             	cmp    $0x7f,%eax
80100878:	0f 84 8c 00 00 00    	je     8010090a <consoleintr+0x111>
8010087e:	e9 bc 00 00 00       	jmp    8010093f <consoleintr+0x146>
    case C('P'):  // Process listing.
      doprocdump = 1;   // procdump() locks cons.lock indirectly; invoke later
80100883:	c7 45 f4 01 00 00 00 	movl   $0x1,-0xc(%ebp)
      break;
8010088a:	e9 42 01 00 00       	jmp    801009d1 <consoleintr+0x1d8>
    #ifdef CS333_P3P4
    case C('F'):
      countFL = 1;
8010088f:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
      break;
80100896:	e9 36 01 00 00       	jmp    801009d1 <consoleintr+0x1d8>
    case C('R'):
      countRL = 1;
8010089b:	c7 45 ec 01 00 00 00 	movl   $0x1,-0x14(%ebp)
      break;
801008a2:	e9 2a 01 00 00       	jmp    801009d1 <consoleintr+0x1d8>
    case C('S'):
      countSL = 1;
801008a7:	c7 45 e8 01 00 00 00 	movl   $0x1,-0x18(%ebp)
      break;
801008ae:	e9 1e 01 00 00       	jmp    801009d1 <consoleintr+0x1d8>
    case C('Z'):
      countZL = 1;
801008b3:	c7 45 e4 01 00 00 00 	movl   $0x1,-0x1c(%ebp)
      break;
801008ba:	e9 12 01 00 00       	jmp    801009d1 <consoleintr+0x1d8>
    #endif
    case C('U'):  // Kill line.
      while(input.e != input.w &&
            input.buf[(input.e-1) % INPUT_BUF] != '\n'){
        input.e--;
801008bf:	a1 28 28 11 80       	mov    0x80112828,%eax
801008c4:	83 e8 01             	sub    $0x1,%eax
801008c7:	a3 28 28 11 80       	mov    %eax,0x80112828
        consputc(BACKSPACE);
801008cc:	83 ec 0c             	sub    $0xc,%esp
801008cf:	68 00 01 00 00       	push   $0x100
801008d4:	e8 b9 fe ff ff       	call   80100792 <consputc>
801008d9:	83 c4 10             	add    $0x10,%esp
    case C('Z'):
      countZL = 1;
      break;
    #endif
    case C('U'):  // Kill line.
      while(input.e != input.w &&
801008dc:	8b 15 28 28 11 80    	mov    0x80112828,%edx
801008e2:	a1 24 28 11 80       	mov    0x80112824,%eax
801008e7:	39 c2                	cmp    %eax,%edx
801008e9:	0f 84 e2 00 00 00    	je     801009d1 <consoleintr+0x1d8>
            input.buf[(input.e-1) % INPUT_BUF] != '\n'){
801008ef:	a1 28 28 11 80       	mov    0x80112828,%eax
801008f4:	83 e8 01             	sub    $0x1,%eax
801008f7:	83 e0 7f             	and    $0x7f,%eax
801008fa:	0f b6 80 a0 27 11 80 	movzbl -0x7feed860(%eax),%eax
    case C('Z'):
      countZL = 1;
      break;
    #endif
    case C('U'):  // Kill line.
      while(input.e != input.w &&
80100901:	3c 0a                	cmp    $0xa,%al
80100903:	75 ba                	jne    801008bf <consoleintr+0xc6>
            input.buf[(input.e-1) % INPUT_BUF] != '\n'){
        input.e--;
        consputc(BACKSPACE);
      }
      break;
80100905:	e9 c7 00 00 00       	jmp    801009d1 <consoleintr+0x1d8>
    case C('H'): case '\x7f':  // Backspace
      if(input.e != input.w){
8010090a:	8b 15 28 28 11 80    	mov    0x80112828,%edx
80100910:	a1 24 28 11 80       	mov    0x80112824,%eax
80100915:	39 c2                	cmp    %eax,%edx
80100917:	0f 84 b4 00 00 00    	je     801009d1 <consoleintr+0x1d8>
        input.e--;
8010091d:	a1 28 28 11 80       	mov    0x80112828,%eax
80100922:	83 e8 01             	sub    $0x1,%eax
80100925:	a3 28 28 11 80       	mov    %eax,0x80112828
        consputc(BACKSPACE);
8010092a:	83 ec 0c             	sub    $0xc,%esp
8010092d:	68 00 01 00 00       	push   $0x100
80100932:	e8 5b fe ff ff       	call   80100792 <consputc>
80100937:	83 c4 10             	add    $0x10,%esp
      }
      break;
8010093a:	e9 92 00 00 00       	jmp    801009d1 <consoleintr+0x1d8>
    default:
      if(c != 0 && input.e-input.r < INPUT_BUF){
8010093f:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
80100943:	0f 84 87 00 00 00    	je     801009d0 <consoleintr+0x1d7>
80100949:	8b 15 28 28 11 80    	mov    0x80112828,%edx
8010094f:	a1 20 28 11 80       	mov    0x80112820,%eax
80100954:	29 c2                	sub    %eax,%edx
80100956:	89 d0                	mov    %edx,%eax
80100958:	83 f8 7f             	cmp    $0x7f,%eax
8010095b:	77 73                	ja     801009d0 <consoleintr+0x1d7>
        c = (c == '\r') ? '\n' : c;
8010095d:	83 7d e0 0d          	cmpl   $0xd,-0x20(%ebp)
80100961:	74 05                	je     80100968 <consoleintr+0x16f>
80100963:	8b 45 e0             	mov    -0x20(%ebp),%eax
80100966:	eb 05                	jmp    8010096d <consoleintr+0x174>
80100968:	b8 0a 00 00 00       	mov    $0xa,%eax
8010096d:	89 45 e0             	mov    %eax,-0x20(%ebp)
        input.buf[input.e++ % INPUT_BUF] = c;
80100970:	a1 28 28 11 80       	mov    0x80112828,%eax
80100975:	8d 50 01             	lea    0x1(%eax),%edx
80100978:	89 15 28 28 11 80    	mov    %edx,0x80112828
8010097e:	83 e0 7f             	and    $0x7f,%eax
80100981:	8b 55 e0             	mov    -0x20(%ebp),%edx
80100984:	88 90 a0 27 11 80    	mov    %dl,-0x7feed860(%eax)
        consputc(c);
8010098a:	83 ec 0c             	sub    $0xc,%esp
8010098d:	ff 75 e0             	pushl  -0x20(%ebp)
80100990:	e8 fd fd ff ff       	call   80100792 <consputc>
80100995:	83 c4 10             	add    $0x10,%esp
        if(c == '\n' || c == C('D') || input.e == input.r+INPUT_BUF){
80100998:	83 7d e0 0a          	cmpl   $0xa,-0x20(%ebp)
8010099c:	74 18                	je     801009b6 <consoleintr+0x1bd>
8010099e:	83 7d e0 04          	cmpl   $0x4,-0x20(%ebp)
801009a2:	74 12                	je     801009b6 <consoleintr+0x1bd>
801009a4:	a1 28 28 11 80       	mov    0x80112828,%eax
801009a9:	8b 15 20 28 11 80    	mov    0x80112820,%edx
801009af:	83 ea 80             	sub    $0xffffff80,%edx
801009b2:	39 d0                	cmp    %edx,%eax
801009b4:	75 1a                	jne    801009d0 <consoleintr+0x1d7>
          input.w = input.e;
801009b6:	a1 28 28 11 80       	mov    0x80112828,%eax
801009bb:	a3 24 28 11 80       	mov    %eax,0x80112824
          wakeup(&input.r);
801009c0:	83 ec 0c             	sub    $0xc,%esp
801009c3:	68 20 28 11 80       	push   $0x80112820
801009c8:	e8 c8 4b 00 00       	call   80105595 <wakeup>
801009cd:	83 c4 10             	add    $0x10,%esp
        }
      }
      break;
801009d0:	90                   	nop
  #ifdef CS333_P3P4
  int countFL = 0, countRL = 0, countSL = 0, countZL = 0;
  #endif

  acquire(&cons.lock);
  while((c = getc()) >= 0){
801009d1:	8b 45 08             	mov    0x8(%ebp),%eax
801009d4:	ff d0                	call   *%eax
801009d6:	89 45 e0             	mov    %eax,-0x20(%ebp)
801009d9:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
801009dd:	0f 89 54 fe ff ff    	jns    80100837 <consoleintr+0x3e>
        }
      }
      break;
    }
  }
  release(&cons.lock);
801009e3:	83 ec 0c             	sub    $0xc,%esp
801009e6:	68 e0 d5 10 80       	push   $0x8010d5e0
801009eb:	e8 ac 60 00 00       	call   80106a9c <release>
801009f0:	83 c4 10             	add    $0x10,%esp
  if(doprocdump) {
801009f3:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801009f7:	74 05                	je     801009fe <consoleintr+0x205>
    procdump();  // now call procdump() wo. cons.lock held
801009f9:	e8 a0 4d 00 00       	call   8010579e <procdump>
  }
  #ifdef CS333_P3P4
  if(countFL) countFreeList();
801009fe:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80100a02:	74 05                	je     80100a09 <consoleintr+0x210>
80100a04:	e8 3c 55 00 00       	call   80105f45 <countFreeList>
  if(countRL) countReadyList();
80100a09:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
80100a0d:	74 05                	je     80100a14 <consoleintr+0x21b>
80100a0f:	e8 37 56 00 00       	call   8010604b <countReadyList>
  if(countSL) countSleepList();
80100a14:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
80100a18:	74 05                	je     80100a1f <consoleintr+0x226>
80100a1a:	e8 3d 57 00 00       	call   8010615c <countSleepList>
  if(countZL) countZombieList();
80100a1f:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
80100a23:	74 05                	je     80100a2a <consoleintr+0x231>
80100a25:	e8 b8 59 00 00       	call   801063e2 <countZombieList>
  #endif 
}
80100a2a:	90                   	nop
80100a2b:	c9                   	leave  
80100a2c:	c3                   	ret    

80100a2d <consoleread>:

int
consoleread(struct inode *ip, char *dst, int n)
{
80100a2d:	55                   	push   %ebp
80100a2e:	89 e5                	mov    %esp,%ebp
80100a30:	83 ec 18             	sub    $0x18,%esp
  uint target;
  int c;

  iunlock(ip);
80100a33:	83 ec 0c             	sub    $0xc,%esp
80100a36:	ff 75 08             	pushl  0x8(%ebp)
80100a39:	e8 28 11 00 00       	call   80101b66 <iunlock>
80100a3e:	83 c4 10             	add    $0x10,%esp
  target = n;
80100a41:	8b 45 10             	mov    0x10(%ebp),%eax
80100a44:	89 45 f4             	mov    %eax,-0xc(%ebp)
  acquire(&cons.lock);
80100a47:	83 ec 0c             	sub    $0xc,%esp
80100a4a:	68 e0 d5 10 80       	push   $0x8010d5e0
80100a4f:	e8 e1 5f 00 00       	call   80106a35 <acquire>
80100a54:	83 c4 10             	add    $0x10,%esp
  while(n > 0){
80100a57:	e9 ac 00 00 00       	jmp    80100b08 <consoleread+0xdb>
    while(input.r == input.w){
      if(proc->killed){
80100a5c:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80100a62:	8b 40 2c             	mov    0x2c(%eax),%eax
80100a65:	85 c0                	test   %eax,%eax
80100a67:	74 28                	je     80100a91 <consoleread+0x64>
        release(&cons.lock);
80100a69:	83 ec 0c             	sub    $0xc,%esp
80100a6c:	68 e0 d5 10 80       	push   $0x8010d5e0
80100a71:	e8 26 60 00 00       	call   80106a9c <release>
80100a76:	83 c4 10             	add    $0x10,%esp
        ilock(ip);
80100a79:	83 ec 0c             	sub    $0xc,%esp
80100a7c:	ff 75 08             	pushl  0x8(%ebp)
80100a7f:	e8 84 0f 00 00       	call   80101a08 <ilock>
80100a84:	83 c4 10             	add    $0x10,%esp
        return -1;
80100a87:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80100a8c:	e9 ab 00 00 00       	jmp    80100b3c <consoleread+0x10f>
      }
      sleep(&input.r, &cons.lock);
80100a91:	83 ec 08             	sub    $0x8,%esp
80100a94:	68 e0 d5 10 80       	push   $0x8010d5e0
80100a99:	68 20 28 11 80       	push   $0x80112820
80100a9e:	e8 49 49 00 00       	call   801053ec <sleep>
80100aa3:	83 c4 10             	add    $0x10,%esp

  iunlock(ip);
  target = n;
  acquire(&cons.lock);
  while(n > 0){
    while(input.r == input.w){
80100aa6:	8b 15 20 28 11 80    	mov    0x80112820,%edx
80100aac:	a1 24 28 11 80       	mov    0x80112824,%eax
80100ab1:	39 c2                	cmp    %eax,%edx
80100ab3:	74 a7                	je     80100a5c <consoleread+0x2f>
        ilock(ip);
        return -1;
      }
      sleep(&input.r, &cons.lock);
    }
    c = input.buf[input.r++ % INPUT_BUF];
80100ab5:	a1 20 28 11 80       	mov    0x80112820,%eax
80100aba:	8d 50 01             	lea    0x1(%eax),%edx
80100abd:	89 15 20 28 11 80    	mov    %edx,0x80112820
80100ac3:	83 e0 7f             	and    $0x7f,%eax
80100ac6:	0f b6 80 a0 27 11 80 	movzbl -0x7feed860(%eax),%eax
80100acd:	0f be c0             	movsbl %al,%eax
80100ad0:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if(c == C('D')){  // EOF
80100ad3:	83 7d f0 04          	cmpl   $0x4,-0x10(%ebp)
80100ad7:	75 17                	jne    80100af0 <consoleread+0xc3>
      if(n < target){
80100ad9:	8b 45 10             	mov    0x10(%ebp),%eax
80100adc:	3b 45 f4             	cmp    -0xc(%ebp),%eax
80100adf:	73 2f                	jae    80100b10 <consoleread+0xe3>
        // Save ^D for next time, to make sure
        // caller gets a 0-byte result.
        input.r--;
80100ae1:	a1 20 28 11 80       	mov    0x80112820,%eax
80100ae6:	83 e8 01             	sub    $0x1,%eax
80100ae9:	a3 20 28 11 80       	mov    %eax,0x80112820
      }
      break;
80100aee:	eb 20                	jmp    80100b10 <consoleread+0xe3>
    }
    *dst++ = c;
80100af0:	8b 45 0c             	mov    0xc(%ebp),%eax
80100af3:	8d 50 01             	lea    0x1(%eax),%edx
80100af6:	89 55 0c             	mov    %edx,0xc(%ebp)
80100af9:	8b 55 f0             	mov    -0x10(%ebp),%edx
80100afc:	88 10                	mov    %dl,(%eax)
    --n;
80100afe:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
    if(c == '\n')
80100b02:	83 7d f0 0a          	cmpl   $0xa,-0x10(%ebp)
80100b06:	74 0b                	je     80100b13 <consoleread+0xe6>
  int c;

  iunlock(ip);
  target = n;
  acquire(&cons.lock);
  while(n > 0){
80100b08:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80100b0c:	7f 98                	jg     80100aa6 <consoleread+0x79>
80100b0e:	eb 04                	jmp    80100b14 <consoleread+0xe7>
      if(n < target){
        // Save ^D for next time, to make sure
        // caller gets a 0-byte result.
        input.r--;
      }
      break;
80100b10:	90                   	nop
80100b11:	eb 01                	jmp    80100b14 <consoleread+0xe7>
    }
    *dst++ = c;
    --n;
    if(c == '\n')
      break;
80100b13:	90                   	nop
  }
  release(&cons.lock);
80100b14:	83 ec 0c             	sub    $0xc,%esp
80100b17:	68 e0 d5 10 80       	push   $0x8010d5e0
80100b1c:	e8 7b 5f 00 00       	call   80106a9c <release>
80100b21:	83 c4 10             	add    $0x10,%esp
  ilock(ip);
80100b24:	83 ec 0c             	sub    $0xc,%esp
80100b27:	ff 75 08             	pushl  0x8(%ebp)
80100b2a:	e8 d9 0e 00 00       	call   80101a08 <ilock>
80100b2f:	83 c4 10             	add    $0x10,%esp

  return target - n;
80100b32:	8b 45 10             	mov    0x10(%ebp),%eax
80100b35:	8b 55 f4             	mov    -0xc(%ebp),%edx
80100b38:	29 c2                	sub    %eax,%edx
80100b3a:	89 d0                	mov    %edx,%eax
}
80100b3c:	c9                   	leave  
80100b3d:	c3                   	ret    

80100b3e <consolewrite>:

int
consolewrite(struct inode *ip, char *buf, int n)
{
80100b3e:	55                   	push   %ebp
80100b3f:	89 e5                	mov    %esp,%ebp
80100b41:	83 ec 18             	sub    $0x18,%esp
  int i;

  iunlock(ip);
80100b44:	83 ec 0c             	sub    $0xc,%esp
80100b47:	ff 75 08             	pushl  0x8(%ebp)
80100b4a:	e8 17 10 00 00       	call   80101b66 <iunlock>
80100b4f:	83 c4 10             	add    $0x10,%esp
  acquire(&cons.lock);
80100b52:	83 ec 0c             	sub    $0xc,%esp
80100b55:	68 e0 d5 10 80       	push   $0x8010d5e0
80100b5a:	e8 d6 5e 00 00       	call   80106a35 <acquire>
80100b5f:	83 c4 10             	add    $0x10,%esp
  for(i = 0; i < n; i++)
80100b62:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80100b69:	eb 21                	jmp    80100b8c <consolewrite+0x4e>
    consputc(buf[i] & 0xff);
80100b6b:	8b 55 f4             	mov    -0xc(%ebp),%edx
80100b6e:	8b 45 0c             	mov    0xc(%ebp),%eax
80100b71:	01 d0                	add    %edx,%eax
80100b73:	0f b6 00             	movzbl (%eax),%eax
80100b76:	0f be c0             	movsbl %al,%eax
80100b79:	0f b6 c0             	movzbl %al,%eax
80100b7c:	83 ec 0c             	sub    $0xc,%esp
80100b7f:	50                   	push   %eax
80100b80:	e8 0d fc ff ff       	call   80100792 <consputc>
80100b85:	83 c4 10             	add    $0x10,%esp
{
  int i;

  iunlock(ip);
  acquire(&cons.lock);
  for(i = 0; i < n; i++)
80100b88:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80100b8c:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100b8f:	3b 45 10             	cmp    0x10(%ebp),%eax
80100b92:	7c d7                	jl     80100b6b <consolewrite+0x2d>
    consputc(buf[i] & 0xff);
  release(&cons.lock);
80100b94:	83 ec 0c             	sub    $0xc,%esp
80100b97:	68 e0 d5 10 80       	push   $0x8010d5e0
80100b9c:	e8 fb 5e 00 00       	call   80106a9c <release>
80100ba1:	83 c4 10             	add    $0x10,%esp
  ilock(ip);
80100ba4:	83 ec 0c             	sub    $0xc,%esp
80100ba7:	ff 75 08             	pushl  0x8(%ebp)
80100baa:	e8 59 0e 00 00       	call   80101a08 <ilock>
80100baf:	83 c4 10             	add    $0x10,%esp

  return n;
80100bb2:	8b 45 10             	mov    0x10(%ebp),%eax
}
80100bb5:	c9                   	leave  
80100bb6:	c3                   	ret    

80100bb7 <consoleinit>:

void
consoleinit(void)
{
80100bb7:	55                   	push   %ebp
80100bb8:	89 e5                	mov    %esp,%ebp
80100bba:	83 ec 08             	sub    $0x8,%esp
  initlock(&cons.lock, "console");
80100bbd:	83 ec 08             	sub    $0x8,%esp
80100bc0:	68 4e a1 10 80       	push   $0x8010a14e
80100bc5:	68 e0 d5 10 80       	push   $0x8010d5e0
80100bca:	e8 44 5e 00 00       	call   80106a13 <initlock>
80100bcf:	83 c4 10             	add    $0x10,%esp

  devsw[CONSOLE].write = consolewrite;
80100bd2:	c7 05 ec 31 11 80 3e 	movl   $0x80100b3e,0x801131ec
80100bd9:	0b 10 80 
  devsw[CONSOLE].read = consoleread;
80100bdc:	c7 05 e8 31 11 80 2d 	movl   $0x80100a2d,0x801131e8
80100be3:	0a 10 80 
  cons.locking = 1;
80100be6:	c7 05 14 d6 10 80 01 	movl   $0x1,0x8010d614
80100bed:	00 00 00 

  picenable(IRQ_KBD);
80100bf0:	83 ec 0c             	sub    $0xc,%esp
80100bf3:	6a 01                	push   $0x1
80100bf5:	e8 cf 33 00 00       	call   80103fc9 <picenable>
80100bfa:	83 c4 10             	add    $0x10,%esp
  ioapicenable(IRQ_KBD, 0);
80100bfd:	83 ec 08             	sub    $0x8,%esp
80100c00:	6a 00                	push   $0x0
80100c02:	6a 01                	push   $0x1
80100c04:	e8 6f 1f 00 00       	call   80102b78 <ioapicenable>
80100c09:	83 c4 10             	add    $0x10,%esp
}
80100c0c:	90                   	nop
80100c0d:	c9                   	leave  
80100c0e:	c3                   	ret    

80100c0f <exec>:
#include "x86.h"
#include "elf.h"

int
exec(char *path, char **argv)
{
80100c0f:	55                   	push   %ebp
80100c10:	89 e5                	mov    %esp,%ebp
80100c12:	81 ec 18 01 00 00    	sub    $0x118,%esp
  struct elfhdr elf;
  struct inode *ip;
  struct proghdr ph;
  pde_t *pgdir, *oldpgdir;

  begin_op();
80100c18:	e8 ce 29 00 00       	call   801035eb <begin_op>
  if((ip = namei(path)) == 0){
80100c1d:	83 ec 0c             	sub    $0xc,%esp
80100c20:	ff 75 08             	pushl  0x8(%ebp)
80100c23:	e8 9e 19 00 00       	call   801025c6 <namei>
80100c28:	83 c4 10             	add    $0x10,%esp
80100c2b:	89 45 d8             	mov    %eax,-0x28(%ebp)
80100c2e:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
80100c32:	75 0f                	jne    80100c43 <exec+0x34>
    end_op();
80100c34:	e8 3e 2a 00 00       	call   80103677 <end_op>
    return -1;
80100c39:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80100c3e:	e9 ce 03 00 00       	jmp    80101011 <exec+0x402>
  }
  ilock(ip);
80100c43:	83 ec 0c             	sub    $0xc,%esp
80100c46:	ff 75 d8             	pushl  -0x28(%ebp)
80100c49:	e8 ba 0d 00 00       	call   80101a08 <ilock>
80100c4e:	83 c4 10             	add    $0x10,%esp
  pgdir = 0;
80100c51:	c7 45 d4 00 00 00 00 	movl   $0x0,-0x2c(%ebp)

  // Check ELF header
  if(readi(ip, (char*)&elf, 0, sizeof(elf)) < sizeof(elf))
80100c58:	6a 34                	push   $0x34
80100c5a:	6a 00                	push   $0x0
80100c5c:	8d 85 0c ff ff ff    	lea    -0xf4(%ebp),%eax
80100c62:	50                   	push   %eax
80100c63:	ff 75 d8             	pushl  -0x28(%ebp)
80100c66:	e8 0b 13 00 00       	call   80101f76 <readi>
80100c6b:	83 c4 10             	add    $0x10,%esp
80100c6e:	83 f8 33             	cmp    $0x33,%eax
80100c71:	0f 86 49 03 00 00    	jbe    80100fc0 <exec+0x3b1>
    goto bad;
  if(elf.magic != ELF_MAGIC)
80100c77:	8b 85 0c ff ff ff    	mov    -0xf4(%ebp),%eax
80100c7d:	3d 7f 45 4c 46       	cmp    $0x464c457f,%eax
80100c82:	0f 85 3b 03 00 00    	jne    80100fc3 <exec+0x3b4>
    goto bad;

  if((pgdir = setupkvm()) == 0)
80100c88:	e8 40 8c 00 00       	call   801098cd <setupkvm>
80100c8d:	89 45 d4             	mov    %eax,-0x2c(%ebp)
80100c90:	83 7d d4 00          	cmpl   $0x0,-0x2c(%ebp)
80100c94:	0f 84 2c 03 00 00    	je     80100fc6 <exec+0x3b7>
    goto bad;

  // Load program into memory.
  sz = 0;
80100c9a:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
80100ca1:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
80100ca8:	8b 85 28 ff ff ff    	mov    -0xd8(%ebp),%eax
80100cae:	89 45 e8             	mov    %eax,-0x18(%ebp)
80100cb1:	e9 ab 00 00 00       	jmp    80100d61 <exec+0x152>
    if(readi(ip, (char*)&ph, off, sizeof(ph)) != sizeof(ph))
80100cb6:	8b 45 e8             	mov    -0x18(%ebp),%eax
80100cb9:	6a 20                	push   $0x20
80100cbb:	50                   	push   %eax
80100cbc:	8d 85 ec fe ff ff    	lea    -0x114(%ebp),%eax
80100cc2:	50                   	push   %eax
80100cc3:	ff 75 d8             	pushl  -0x28(%ebp)
80100cc6:	e8 ab 12 00 00       	call   80101f76 <readi>
80100ccb:	83 c4 10             	add    $0x10,%esp
80100cce:	83 f8 20             	cmp    $0x20,%eax
80100cd1:	0f 85 f2 02 00 00    	jne    80100fc9 <exec+0x3ba>
      goto bad;
    if(ph.type != ELF_PROG_LOAD)
80100cd7:	8b 85 ec fe ff ff    	mov    -0x114(%ebp),%eax
80100cdd:	83 f8 01             	cmp    $0x1,%eax
80100ce0:	75 71                	jne    80100d53 <exec+0x144>
      continue;
    if(ph.memsz < ph.filesz)
80100ce2:	8b 95 00 ff ff ff    	mov    -0x100(%ebp),%edx
80100ce8:	8b 85 fc fe ff ff    	mov    -0x104(%ebp),%eax
80100cee:	39 c2                	cmp    %eax,%edx
80100cf0:	0f 82 d6 02 00 00    	jb     80100fcc <exec+0x3bd>
      goto bad;
    if((sz = allocuvm(pgdir, sz, ph.vaddr + ph.memsz)) == 0)
80100cf6:	8b 95 f4 fe ff ff    	mov    -0x10c(%ebp),%edx
80100cfc:	8b 85 00 ff ff ff    	mov    -0x100(%ebp),%eax
80100d02:	01 d0                	add    %edx,%eax
80100d04:	83 ec 04             	sub    $0x4,%esp
80100d07:	50                   	push   %eax
80100d08:	ff 75 e0             	pushl  -0x20(%ebp)
80100d0b:	ff 75 d4             	pushl  -0x2c(%ebp)
80100d0e:	e8 61 8f 00 00       	call   80109c74 <allocuvm>
80100d13:	83 c4 10             	add    $0x10,%esp
80100d16:	89 45 e0             	mov    %eax,-0x20(%ebp)
80100d19:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
80100d1d:	0f 84 ac 02 00 00    	je     80100fcf <exec+0x3c0>
      goto bad;
    if(loaduvm(pgdir, (char*)ph.vaddr, ip, ph.off, ph.filesz) < 0)
80100d23:	8b 95 fc fe ff ff    	mov    -0x104(%ebp),%edx
80100d29:	8b 85 f0 fe ff ff    	mov    -0x110(%ebp),%eax
80100d2f:	8b 8d f4 fe ff ff    	mov    -0x10c(%ebp),%ecx
80100d35:	83 ec 0c             	sub    $0xc,%esp
80100d38:	52                   	push   %edx
80100d39:	50                   	push   %eax
80100d3a:	ff 75 d8             	pushl  -0x28(%ebp)
80100d3d:	51                   	push   %ecx
80100d3e:	ff 75 d4             	pushl  -0x2c(%ebp)
80100d41:	e8 57 8e 00 00       	call   80109b9d <loaduvm>
80100d46:	83 c4 20             	add    $0x20,%esp
80100d49:	85 c0                	test   %eax,%eax
80100d4b:	0f 88 81 02 00 00    	js     80100fd2 <exec+0x3c3>
80100d51:	eb 01                	jmp    80100d54 <exec+0x145>
  sz = 0;
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
    if(readi(ip, (char*)&ph, off, sizeof(ph)) != sizeof(ph))
      goto bad;
    if(ph.type != ELF_PROG_LOAD)
      continue;
80100d53:	90                   	nop
  if((pgdir = setupkvm()) == 0)
    goto bad;

  // Load program into memory.
  sz = 0;
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
80100d54:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
80100d58:	8b 45 e8             	mov    -0x18(%ebp),%eax
80100d5b:	83 c0 20             	add    $0x20,%eax
80100d5e:	89 45 e8             	mov    %eax,-0x18(%ebp)
80100d61:	0f b7 85 38 ff ff ff 	movzwl -0xc8(%ebp),%eax
80100d68:	0f b7 c0             	movzwl %ax,%eax
80100d6b:	3b 45 ec             	cmp    -0x14(%ebp),%eax
80100d6e:	0f 8f 42 ff ff ff    	jg     80100cb6 <exec+0xa7>
    if((sz = allocuvm(pgdir, sz, ph.vaddr + ph.memsz)) == 0)
      goto bad;
    if(loaduvm(pgdir, (char*)ph.vaddr, ip, ph.off, ph.filesz) < 0)
      goto bad;
  }
  iunlockput(ip);
80100d74:	83 ec 0c             	sub    $0xc,%esp
80100d77:	ff 75 d8             	pushl  -0x28(%ebp)
80100d7a:	e8 49 0f 00 00       	call   80101cc8 <iunlockput>
80100d7f:	83 c4 10             	add    $0x10,%esp
  end_op();
80100d82:	e8 f0 28 00 00       	call   80103677 <end_op>
  ip = 0;
80100d87:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)

  // Allocate two pages at the next page boundary.
  // Make the first inaccessible.  Use the second as the user stack.
  sz = PGROUNDUP(sz);
80100d8e:	8b 45 e0             	mov    -0x20(%ebp),%eax
80100d91:	05 ff 0f 00 00       	add    $0xfff,%eax
80100d96:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80100d9b:	89 45 e0             	mov    %eax,-0x20(%ebp)
  if((sz = allocuvm(pgdir, sz, sz + 2*PGSIZE)) == 0)
80100d9e:	8b 45 e0             	mov    -0x20(%ebp),%eax
80100da1:	05 00 20 00 00       	add    $0x2000,%eax
80100da6:	83 ec 04             	sub    $0x4,%esp
80100da9:	50                   	push   %eax
80100daa:	ff 75 e0             	pushl  -0x20(%ebp)
80100dad:	ff 75 d4             	pushl  -0x2c(%ebp)
80100db0:	e8 bf 8e 00 00       	call   80109c74 <allocuvm>
80100db5:	83 c4 10             	add    $0x10,%esp
80100db8:	89 45 e0             	mov    %eax,-0x20(%ebp)
80100dbb:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
80100dbf:	0f 84 10 02 00 00    	je     80100fd5 <exec+0x3c6>
    goto bad;
  clearpteu(pgdir, (char*)(sz - 2*PGSIZE));
80100dc5:	8b 45 e0             	mov    -0x20(%ebp),%eax
80100dc8:	2d 00 20 00 00       	sub    $0x2000,%eax
80100dcd:	83 ec 08             	sub    $0x8,%esp
80100dd0:	50                   	push   %eax
80100dd1:	ff 75 d4             	pushl  -0x2c(%ebp)
80100dd4:	e8 c1 90 00 00       	call   80109e9a <clearpteu>
80100dd9:	83 c4 10             	add    $0x10,%esp
  sp = sz;
80100ddc:	8b 45 e0             	mov    -0x20(%ebp),%eax
80100ddf:	89 45 dc             	mov    %eax,-0x24(%ebp)

  // Push argument strings, prepare rest of stack in ustack.
  for(argc = 0; argv[argc]; argc++) {
80100de2:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
80100de9:	e9 96 00 00 00       	jmp    80100e84 <exec+0x275>
    if(argc >= MAXARG)
80100dee:	83 7d e4 1f          	cmpl   $0x1f,-0x1c(%ebp)
80100df2:	0f 87 e0 01 00 00    	ja     80100fd8 <exec+0x3c9>
      goto bad;
    sp = (sp - (strlen(argv[argc]) + 1)) & ~3;
80100df8:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100dfb:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
80100e02:	8b 45 0c             	mov    0xc(%ebp),%eax
80100e05:	01 d0                	add    %edx,%eax
80100e07:	8b 00                	mov    (%eax),%eax
80100e09:	83 ec 0c             	sub    $0xc,%esp
80100e0c:	50                   	push   %eax
80100e0d:	e8 d3 60 00 00       	call   80106ee5 <strlen>
80100e12:	83 c4 10             	add    $0x10,%esp
80100e15:	89 c2                	mov    %eax,%edx
80100e17:	8b 45 dc             	mov    -0x24(%ebp),%eax
80100e1a:	29 d0                	sub    %edx,%eax
80100e1c:	83 e8 01             	sub    $0x1,%eax
80100e1f:	83 e0 fc             	and    $0xfffffffc,%eax
80100e22:	89 45 dc             	mov    %eax,-0x24(%ebp)
    if(copyout(pgdir, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
80100e25:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100e28:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
80100e2f:	8b 45 0c             	mov    0xc(%ebp),%eax
80100e32:	01 d0                	add    %edx,%eax
80100e34:	8b 00                	mov    (%eax),%eax
80100e36:	83 ec 0c             	sub    $0xc,%esp
80100e39:	50                   	push   %eax
80100e3a:	e8 a6 60 00 00       	call   80106ee5 <strlen>
80100e3f:	83 c4 10             	add    $0x10,%esp
80100e42:	83 c0 01             	add    $0x1,%eax
80100e45:	89 c1                	mov    %eax,%ecx
80100e47:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100e4a:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
80100e51:	8b 45 0c             	mov    0xc(%ebp),%eax
80100e54:	01 d0                	add    %edx,%eax
80100e56:	8b 00                	mov    (%eax),%eax
80100e58:	51                   	push   %ecx
80100e59:	50                   	push   %eax
80100e5a:	ff 75 dc             	pushl  -0x24(%ebp)
80100e5d:	ff 75 d4             	pushl  -0x2c(%ebp)
80100e60:	e8 ec 91 00 00       	call   8010a051 <copyout>
80100e65:	83 c4 10             	add    $0x10,%esp
80100e68:	85 c0                	test   %eax,%eax
80100e6a:	0f 88 6b 01 00 00    	js     80100fdb <exec+0x3cc>
      goto bad;
    ustack[3+argc] = sp;
80100e70:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100e73:	8d 50 03             	lea    0x3(%eax),%edx
80100e76:	8b 45 dc             	mov    -0x24(%ebp),%eax
80100e79:	89 84 95 40 ff ff ff 	mov    %eax,-0xc0(%ebp,%edx,4)
    goto bad;
  clearpteu(pgdir, (char*)(sz - 2*PGSIZE));
  sp = sz;

  // Push argument strings, prepare rest of stack in ustack.
  for(argc = 0; argv[argc]; argc++) {
80100e80:	83 45 e4 01          	addl   $0x1,-0x1c(%ebp)
80100e84:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100e87:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
80100e8e:	8b 45 0c             	mov    0xc(%ebp),%eax
80100e91:	01 d0                	add    %edx,%eax
80100e93:	8b 00                	mov    (%eax),%eax
80100e95:	85 c0                	test   %eax,%eax
80100e97:	0f 85 51 ff ff ff    	jne    80100dee <exec+0x1df>
    sp = (sp - (strlen(argv[argc]) + 1)) & ~3;
    if(copyout(pgdir, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
      goto bad;
    ustack[3+argc] = sp;
  }
  ustack[3+argc] = 0;
80100e9d:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100ea0:	83 c0 03             	add    $0x3,%eax
80100ea3:	c7 84 85 40 ff ff ff 	movl   $0x0,-0xc0(%ebp,%eax,4)
80100eaa:	00 00 00 00 

  ustack[0] = 0xffffffff;  // fake return PC
80100eae:	c7 85 40 ff ff ff ff 	movl   $0xffffffff,-0xc0(%ebp)
80100eb5:	ff ff ff 
  ustack[1] = argc;
80100eb8:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100ebb:	89 85 44 ff ff ff    	mov    %eax,-0xbc(%ebp)
  ustack[2] = sp - (argc+1)*4;  // argv pointer
80100ec1:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100ec4:	83 c0 01             	add    $0x1,%eax
80100ec7:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
80100ece:	8b 45 dc             	mov    -0x24(%ebp),%eax
80100ed1:	29 d0                	sub    %edx,%eax
80100ed3:	89 85 48 ff ff ff    	mov    %eax,-0xb8(%ebp)

  sp -= (3+argc+1) * 4;
80100ed9:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100edc:	83 c0 04             	add    $0x4,%eax
80100edf:	c1 e0 02             	shl    $0x2,%eax
80100ee2:	29 45 dc             	sub    %eax,-0x24(%ebp)
  if(copyout(pgdir, sp, ustack, (3+argc+1)*4) < 0)
80100ee5:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100ee8:	83 c0 04             	add    $0x4,%eax
80100eeb:	c1 e0 02             	shl    $0x2,%eax
80100eee:	50                   	push   %eax
80100eef:	8d 85 40 ff ff ff    	lea    -0xc0(%ebp),%eax
80100ef5:	50                   	push   %eax
80100ef6:	ff 75 dc             	pushl  -0x24(%ebp)
80100ef9:	ff 75 d4             	pushl  -0x2c(%ebp)
80100efc:	e8 50 91 00 00       	call   8010a051 <copyout>
80100f01:	83 c4 10             	add    $0x10,%esp
80100f04:	85 c0                	test   %eax,%eax
80100f06:	0f 88 d2 00 00 00    	js     80100fde <exec+0x3cf>
    goto bad;

  // Save program name for debugging.
  for(last=s=path; *s; s++)
80100f0c:	8b 45 08             	mov    0x8(%ebp),%eax
80100f0f:	89 45 f4             	mov    %eax,-0xc(%ebp)
80100f12:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100f15:	89 45 f0             	mov    %eax,-0x10(%ebp)
80100f18:	eb 17                	jmp    80100f31 <exec+0x322>
    if(*s == '/')
80100f1a:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100f1d:	0f b6 00             	movzbl (%eax),%eax
80100f20:	3c 2f                	cmp    $0x2f,%al
80100f22:	75 09                	jne    80100f2d <exec+0x31e>
      last = s+1;
80100f24:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100f27:	83 c0 01             	add    $0x1,%eax
80100f2a:	89 45 f0             	mov    %eax,-0x10(%ebp)
  sp -= (3+argc+1) * 4;
  if(copyout(pgdir, sp, ustack, (3+argc+1)*4) < 0)
    goto bad;

  // Save program name for debugging.
  for(last=s=path; *s; s++)
80100f2d:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80100f31:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100f34:	0f b6 00             	movzbl (%eax),%eax
80100f37:	84 c0                	test   %al,%al
80100f39:	75 df                	jne    80100f1a <exec+0x30b>
    if(*s == '/')
      last = s+1;
  safestrcpy(proc->name, last, sizeof(proc->name));
80100f3b:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80100f41:	83 c0 74             	add    $0x74,%eax
80100f44:	83 ec 04             	sub    $0x4,%esp
80100f47:	6a 10                	push   $0x10
80100f49:	ff 75 f0             	pushl  -0x10(%ebp)
80100f4c:	50                   	push   %eax
80100f4d:	e8 49 5f 00 00       	call   80106e9b <safestrcpy>
80100f52:	83 c4 10             	add    $0x10,%esp

  // Commit to the user image.
  oldpgdir = proc->pgdir;
80100f55:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80100f5b:	8b 40 04             	mov    0x4(%eax),%eax
80100f5e:	89 45 d0             	mov    %eax,-0x30(%ebp)
  proc->pgdir = pgdir;
80100f61:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80100f67:	8b 55 d4             	mov    -0x2c(%ebp),%edx
80100f6a:	89 50 04             	mov    %edx,0x4(%eax)
  proc->sz = sz;
80100f6d:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80100f73:	8b 55 e0             	mov    -0x20(%ebp),%edx
80100f76:	89 10                	mov    %edx,(%eax)
  proc->tf->eip = elf.entry;  // main
80100f78:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80100f7e:	8b 40 20             	mov    0x20(%eax),%eax
80100f81:	8b 95 24 ff ff ff    	mov    -0xdc(%ebp),%edx
80100f87:	89 50 38             	mov    %edx,0x38(%eax)
  proc->tf->esp = sp;
80100f8a:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80100f90:	8b 40 20             	mov    0x20(%eax),%eax
80100f93:	8b 55 dc             	mov    -0x24(%ebp),%edx
80100f96:	89 50 44             	mov    %edx,0x44(%eax)
  switchuvm(proc);
80100f99:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80100f9f:	83 ec 0c             	sub    $0xc,%esp
80100fa2:	50                   	push   %eax
80100fa3:	e8 0c 8a 00 00       	call   801099b4 <switchuvm>
80100fa8:	83 c4 10             	add    $0x10,%esp
  freevm(oldpgdir);
80100fab:	83 ec 0c             	sub    $0xc,%esp
80100fae:	ff 75 d0             	pushl  -0x30(%ebp)
80100fb1:	e8 44 8e 00 00       	call   80109dfa <freevm>
80100fb6:	83 c4 10             	add    $0x10,%esp
  return 0;
80100fb9:	b8 00 00 00 00       	mov    $0x0,%eax
80100fbe:	eb 51                	jmp    80101011 <exec+0x402>
  ilock(ip);
  pgdir = 0;

  // Check ELF header
  if(readi(ip, (char*)&elf, 0, sizeof(elf)) < sizeof(elf))
    goto bad;
80100fc0:	90                   	nop
80100fc1:	eb 1c                	jmp    80100fdf <exec+0x3d0>
  if(elf.magic != ELF_MAGIC)
    goto bad;
80100fc3:	90                   	nop
80100fc4:	eb 19                	jmp    80100fdf <exec+0x3d0>

  if((pgdir = setupkvm()) == 0)
    goto bad;
80100fc6:	90                   	nop
80100fc7:	eb 16                	jmp    80100fdf <exec+0x3d0>

  // Load program into memory.
  sz = 0;
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
    if(readi(ip, (char*)&ph, off, sizeof(ph)) != sizeof(ph))
      goto bad;
80100fc9:	90                   	nop
80100fca:	eb 13                	jmp    80100fdf <exec+0x3d0>
    if(ph.type != ELF_PROG_LOAD)
      continue;
    if(ph.memsz < ph.filesz)
      goto bad;
80100fcc:	90                   	nop
80100fcd:	eb 10                	jmp    80100fdf <exec+0x3d0>
    if((sz = allocuvm(pgdir, sz, ph.vaddr + ph.memsz)) == 0)
      goto bad;
80100fcf:	90                   	nop
80100fd0:	eb 0d                	jmp    80100fdf <exec+0x3d0>
    if(loaduvm(pgdir, (char*)ph.vaddr, ip, ph.off, ph.filesz) < 0)
      goto bad;
80100fd2:	90                   	nop
80100fd3:	eb 0a                	jmp    80100fdf <exec+0x3d0>

  // Allocate two pages at the next page boundary.
  // Make the first inaccessible.  Use the second as the user stack.
  sz = PGROUNDUP(sz);
  if((sz = allocuvm(pgdir, sz, sz + 2*PGSIZE)) == 0)
    goto bad;
80100fd5:	90                   	nop
80100fd6:	eb 07                	jmp    80100fdf <exec+0x3d0>
  sp = sz;

  // Push argument strings, prepare rest of stack in ustack.
  for(argc = 0; argv[argc]; argc++) {
    if(argc >= MAXARG)
      goto bad;
80100fd8:	90                   	nop
80100fd9:	eb 04                	jmp    80100fdf <exec+0x3d0>
    sp = (sp - (strlen(argv[argc]) + 1)) & ~3;
    if(copyout(pgdir, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
      goto bad;
80100fdb:	90                   	nop
80100fdc:	eb 01                	jmp    80100fdf <exec+0x3d0>
  ustack[1] = argc;
  ustack[2] = sp - (argc+1)*4;  // argv pointer

  sp -= (3+argc+1) * 4;
  if(copyout(pgdir, sp, ustack, (3+argc+1)*4) < 0)
    goto bad;
80100fde:	90                   	nop
  switchuvm(proc);
  freevm(oldpgdir);
  return 0;

 bad:
  if(pgdir)
80100fdf:	83 7d d4 00          	cmpl   $0x0,-0x2c(%ebp)
80100fe3:	74 0e                	je     80100ff3 <exec+0x3e4>
    freevm(pgdir);
80100fe5:	83 ec 0c             	sub    $0xc,%esp
80100fe8:	ff 75 d4             	pushl  -0x2c(%ebp)
80100feb:	e8 0a 8e 00 00       	call   80109dfa <freevm>
80100ff0:	83 c4 10             	add    $0x10,%esp
  if(ip){
80100ff3:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
80100ff7:	74 13                	je     8010100c <exec+0x3fd>
    iunlockput(ip);
80100ff9:	83 ec 0c             	sub    $0xc,%esp
80100ffc:	ff 75 d8             	pushl  -0x28(%ebp)
80100fff:	e8 c4 0c 00 00       	call   80101cc8 <iunlockput>
80101004:	83 c4 10             	add    $0x10,%esp
    end_op();
80101007:	e8 6b 26 00 00       	call   80103677 <end_op>
  }
  return -1;
8010100c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80101011:	c9                   	leave  
80101012:	c3                   	ret    

80101013 <fileinit>:
  struct file file[NFILE];
} ftable;

void
fileinit(void)
{
80101013:	55                   	push   %ebp
80101014:	89 e5                	mov    %esp,%ebp
80101016:	83 ec 08             	sub    $0x8,%esp
  initlock(&ftable.lock, "ftable");
80101019:	83 ec 08             	sub    $0x8,%esp
8010101c:	68 56 a1 10 80       	push   $0x8010a156
80101021:	68 40 28 11 80       	push   $0x80112840
80101026:	e8 e8 59 00 00       	call   80106a13 <initlock>
8010102b:	83 c4 10             	add    $0x10,%esp
}
8010102e:	90                   	nop
8010102f:	c9                   	leave  
80101030:	c3                   	ret    

80101031 <filealloc>:

// Allocate a file structure.
struct file*
filealloc(void)
{
80101031:	55                   	push   %ebp
80101032:	89 e5                	mov    %esp,%ebp
80101034:	83 ec 18             	sub    $0x18,%esp
  struct file *f;

  acquire(&ftable.lock);
80101037:	83 ec 0c             	sub    $0xc,%esp
8010103a:	68 40 28 11 80       	push   $0x80112840
8010103f:	e8 f1 59 00 00       	call   80106a35 <acquire>
80101044:	83 c4 10             	add    $0x10,%esp
  for(f = ftable.file; f < ftable.file + NFILE; f++){
80101047:	c7 45 f4 74 28 11 80 	movl   $0x80112874,-0xc(%ebp)
8010104e:	eb 2d                	jmp    8010107d <filealloc+0x4c>
    if(f->ref == 0){
80101050:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101053:	8b 40 04             	mov    0x4(%eax),%eax
80101056:	85 c0                	test   %eax,%eax
80101058:	75 1f                	jne    80101079 <filealloc+0x48>
      f->ref = 1;
8010105a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010105d:	c7 40 04 01 00 00 00 	movl   $0x1,0x4(%eax)
      release(&ftable.lock);
80101064:	83 ec 0c             	sub    $0xc,%esp
80101067:	68 40 28 11 80       	push   $0x80112840
8010106c:	e8 2b 5a 00 00       	call   80106a9c <release>
80101071:	83 c4 10             	add    $0x10,%esp
      return f;
80101074:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101077:	eb 23                	jmp    8010109c <filealloc+0x6b>
filealloc(void)
{
  struct file *f;

  acquire(&ftable.lock);
  for(f = ftable.file; f < ftable.file + NFILE; f++){
80101079:	83 45 f4 18          	addl   $0x18,-0xc(%ebp)
8010107d:	b8 d4 31 11 80       	mov    $0x801131d4,%eax
80101082:	39 45 f4             	cmp    %eax,-0xc(%ebp)
80101085:	72 c9                	jb     80101050 <filealloc+0x1f>
      f->ref = 1;
      release(&ftable.lock);
      return f;
    }
  }
  release(&ftable.lock);
80101087:	83 ec 0c             	sub    $0xc,%esp
8010108a:	68 40 28 11 80       	push   $0x80112840
8010108f:	e8 08 5a 00 00       	call   80106a9c <release>
80101094:	83 c4 10             	add    $0x10,%esp
  return 0;
80101097:	b8 00 00 00 00       	mov    $0x0,%eax
}
8010109c:	c9                   	leave  
8010109d:	c3                   	ret    

8010109e <filedup>:

// Increment ref count for file f.
struct file*
filedup(struct file *f)
{
8010109e:	55                   	push   %ebp
8010109f:	89 e5                	mov    %esp,%ebp
801010a1:	83 ec 08             	sub    $0x8,%esp
  acquire(&ftable.lock);
801010a4:	83 ec 0c             	sub    $0xc,%esp
801010a7:	68 40 28 11 80       	push   $0x80112840
801010ac:	e8 84 59 00 00       	call   80106a35 <acquire>
801010b1:	83 c4 10             	add    $0x10,%esp
  if(f->ref < 1)
801010b4:	8b 45 08             	mov    0x8(%ebp),%eax
801010b7:	8b 40 04             	mov    0x4(%eax),%eax
801010ba:	85 c0                	test   %eax,%eax
801010bc:	7f 0d                	jg     801010cb <filedup+0x2d>
    panic("filedup");
801010be:	83 ec 0c             	sub    $0xc,%esp
801010c1:	68 5d a1 10 80       	push   $0x8010a15d
801010c6:	e8 9b f4 ff ff       	call   80100566 <panic>
  f->ref++;
801010cb:	8b 45 08             	mov    0x8(%ebp),%eax
801010ce:	8b 40 04             	mov    0x4(%eax),%eax
801010d1:	8d 50 01             	lea    0x1(%eax),%edx
801010d4:	8b 45 08             	mov    0x8(%ebp),%eax
801010d7:	89 50 04             	mov    %edx,0x4(%eax)
  release(&ftable.lock);
801010da:	83 ec 0c             	sub    $0xc,%esp
801010dd:	68 40 28 11 80       	push   $0x80112840
801010e2:	e8 b5 59 00 00       	call   80106a9c <release>
801010e7:	83 c4 10             	add    $0x10,%esp
  return f;
801010ea:	8b 45 08             	mov    0x8(%ebp),%eax
}
801010ed:	c9                   	leave  
801010ee:	c3                   	ret    

801010ef <fileclose>:

// Close file f.  (Decrement ref count, close when reaches 0.)
void
fileclose(struct file *f)
{
801010ef:	55                   	push   %ebp
801010f0:	89 e5                	mov    %esp,%ebp
801010f2:	83 ec 28             	sub    $0x28,%esp
  struct file ff;

  acquire(&ftable.lock);
801010f5:	83 ec 0c             	sub    $0xc,%esp
801010f8:	68 40 28 11 80       	push   $0x80112840
801010fd:	e8 33 59 00 00       	call   80106a35 <acquire>
80101102:	83 c4 10             	add    $0x10,%esp
  if(f->ref < 1)
80101105:	8b 45 08             	mov    0x8(%ebp),%eax
80101108:	8b 40 04             	mov    0x4(%eax),%eax
8010110b:	85 c0                	test   %eax,%eax
8010110d:	7f 0d                	jg     8010111c <fileclose+0x2d>
    panic("fileclose");
8010110f:	83 ec 0c             	sub    $0xc,%esp
80101112:	68 65 a1 10 80       	push   $0x8010a165
80101117:	e8 4a f4 ff ff       	call   80100566 <panic>
  if(--f->ref > 0){
8010111c:	8b 45 08             	mov    0x8(%ebp),%eax
8010111f:	8b 40 04             	mov    0x4(%eax),%eax
80101122:	8d 50 ff             	lea    -0x1(%eax),%edx
80101125:	8b 45 08             	mov    0x8(%ebp),%eax
80101128:	89 50 04             	mov    %edx,0x4(%eax)
8010112b:	8b 45 08             	mov    0x8(%ebp),%eax
8010112e:	8b 40 04             	mov    0x4(%eax),%eax
80101131:	85 c0                	test   %eax,%eax
80101133:	7e 15                	jle    8010114a <fileclose+0x5b>
    release(&ftable.lock);
80101135:	83 ec 0c             	sub    $0xc,%esp
80101138:	68 40 28 11 80       	push   $0x80112840
8010113d:	e8 5a 59 00 00       	call   80106a9c <release>
80101142:	83 c4 10             	add    $0x10,%esp
80101145:	e9 8b 00 00 00       	jmp    801011d5 <fileclose+0xe6>
    return;
  }
  ff = *f;
8010114a:	8b 45 08             	mov    0x8(%ebp),%eax
8010114d:	8b 10                	mov    (%eax),%edx
8010114f:	89 55 e0             	mov    %edx,-0x20(%ebp)
80101152:	8b 50 04             	mov    0x4(%eax),%edx
80101155:	89 55 e4             	mov    %edx,-0x1c(%ebp)
80101158:	8b 50 08             	mov    0x8(%eax),%edx
8010115b:	89 55 e8             	mov    %edx,-0x18(%ebp)
8010115e:	8b 50 0c             	mov    0xc(%eax),%edx
80101161:	89 55 ec             	mov    %edx,-0x14(%ebp)
80101164:	8b 50 10             	mov    0x10(%eax),%edx
80101167:	89 55 f0             	mov    %edx,-0x10(%ebp)
8010116a:	8b 40 14             	mov    0x14(%eax),%eax
8010116d:	89 45 f4             	mov    %eax,-0xc(%ebp)
  f->ref = 0;
80101170:	8b 45 08             	mov    0x8(%ebp),%eax
80101173:	c7 40 04 00 00 00 00 	movl   $0x0,0x4(%eax)
  f->type = FD_NONE;
8010117a:	8b 45 08             	mov    0x8(%ebp),%eax
8010117d:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
  release(&ftable.lock);
80101183:	83 ec 0c             	sub    $0xc,%esp
80101186:	68 40 28 11 80       	push   $0x80112840
8010118b:	e8 0c 59 00 00       	call   80106a9c <release>
80101190:	83 c4 10             	add    $0x10,%esp
  
  if(ff.type == FD_PIPE)
80101193:	8b 45 e0             	mov    -0x20(%ebp),%eax
80101196:	83 f8 01             	cmp    $0x1,%eax
80101199:	75 19                	jne    801011b4 <fileclose+0xc5>
    pipeclose(ff.pipe, ff.writable);
8010119b:	0f b6 45 e9          	movzbl -0x17(%ebp),%eax
8010119f:	0f be d0             	movsbl %al,%edx
801011a2:	8b 45 ec             	mov    -0x14(%ebp),%eax
801011a5:	83 ec 08             	sub    $0x8,%esp
801011a8:	52                   	push   %edx
801011a9:	50                   	push   %eax
801011aa:	e8 83 30 00 00       	call   80104232 <pipeclose>
801011af:	83 c4 10             	add    $0x10,%esp
801011b2:	eb 21                	jmp    801011d5 <fileclose+0xe6>
  else if(ff.type == FD_INODE){
801011b4:	8b 45 e0             	mov    -0x20(%ebp),%eax
801011b7:	83 f8 02             	cmp    $0x2,%eax
801011ba:	75 19                	jne    801011d5 <fileclose+0xe6>
    begin_op();
801011bc:	e8 2a 24 00 00       	call   801035eb <begin_op>
    iput(ff.ip);
801011c1:	8b 45 f0             	mov    -0x10(%ebp),%eax
801011c4:	83 ec 0c             	sub    $0xc,%esp
801011c7:	50                   	push   %eax
801011c8:	e8 0b 0a 00 00       	call   80101bd8 <iput>
801011cd:	83 c4 10             	add    $0x10,%esp
    end_op();
801011d0:	e8 a2 24 00 00       	call   80103677 <end_op>
  }
}
801011d5:	c9                   	leave  
801011d6:	c3                   	ret    

801011d7 <filestat>:

// Get metadata about file f.
int
filestat(struct file *f, struct stat *st)
{
801011d7:	55                   	push   %ebp
801011d8:	89 e5                	mov    %esp,%ebp
801011da:	83 ec 08             	sub    $0x8,%esp
  if(f->type == FD_INODE){
801011dd:	8b 45 08             	mov    0x8(%ebp),%eax
801011e0:	8b 00                	mov    (%eax),%eax
801011e2:	83 f8 02             	cmp    $0x2,%eax
801011e5:	75 40                	jne    80101227 <filestat+0x50>
    ilock(f->ip);
801011e7:	8b 45 08             	mov    0x8(%ebp),%eax
801011ea:	8b 40 10             	mov    0x10(%eax),%eax
801011ed:	83 ec 0c             	sub    $0xc,%esp
801011f0:	50                   	push   %eax
801011f1:	e8 12 08 00 00       	call   80101a08 <ilock>
801011f6:	83 c4 10             	add    $0x10,%esp
    stati(f->ip, st);
801011f9:	8b 45 08             	mov    0x8(%ebp),%eax
801011fc:	8b 40 10             	mov    0x10(%eax),%eax
801011ff:	83 ec 08             	sub    $0x8,%esp
80101202:	ff 75 0c             	pushl  0xc(%ebp)
80101205:	50                   	push   %eax
80101206:	e8 25 0d 00 00       	call   80101f30 <stati>
8010120b:	83 c4 10             	add    $0x10,%esp
    iunlock(f->ip);
8010120e:	8b 45 08             	mov    0x8(%ebp),%eax
80101211:	8b 40 10             	mov    0x10(%eax),%eax
80101214:	83 ec 0c             	sub    $0xc,%esp
80101217:	50                   	push   %eax
80101218:	e8 49 09 00 00       	call   80101b66 <iunlock>
8010121d:	83 c4 10             	add    $0x10,%esp
    return 0;
80101220:	b8 00 00 00 00       	mov    $0x0,%eax
80101225:	eb 05                	jmp    8010122c <filestat+0x55>
  }
  return -1;
80101227:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
8010122c:	c9                   	leave  
8010122d:	c3                   	ret    

8010122e <fileread>:

// Read from file f.
int
fileread(struct file *f, char *addr, int n)
{
8010122e:	55                   	push   %ebp
8010122f:	89 e5                	mov    %esp,%ebp
80101231:	83 ec 18             	sub    $0x18,%esp
  int r;

  if(f->readable == 0)
80101234:	8b 45 08             	mov    0x8(%ebp),%eax
80101237:	0f b6 40 08          	movzbl 0x8(%eax),%eax
8010123b:	84 c0                	test   %al,%al
8010123d:	75 0a                	jne    80101249 <fileread+0x1b>
    return -1;
8010123f:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80101244:	e9 9b 00 00 00       	jmp    801012e4 <fileread+0xb6>
  if(f->type == FD_PIPE)
80101249:	8b 45 08             	mov    0x8(%ebp),%eax
8010124c:	8b 00                	mov    (%eax),%eax
8010124e:	83 f8 01             	cmp    $0x1,%eax
80101251:	75 1a                	jne    8010126d <fileread+0x3f>
    return piperead(f->pipe, addr, n);
80101253:	8b 45 08             	mov    0x8(%ebp),%eax
80101256:	8b 40 0c             	mov    0xc(%eax),%eax
80101259:	83 ec 04             	sub    $0x4,%esp
8010125c:	ff 75 10             	pushl  0x10(%ebp)
8010125f:	ff 75 0c             	pushl  0xc(%ebp)
80101262:	50                   	push   %eax
80101263:	e8 72 31 00 00       	call   801043da <piperead>
80101268:	83 c4 10             	add    $0x10,%esp
8010126b:	eb 77                	jmp    801012e4 <fileread+0xb6>
  if(f->type == FD_INODE){
8010126d:	8b 45 08             	mov    0x8(%ebp),%eax
80101270:	8b 00                	mov    (%eax),%eax
80101272:	83 f8 02             	cmp    $0x2,%eax
80101275:	75 60                	jne    801012d7 <fileread+0xa9>
    ilock(f->ip);
80101277:	8b 45 08             	mov    0x8(%ebp),%eax
8010127a:	8b 40 10             	mov    0x10(%eax),%eax
8010127d:	83 ec 0c             	sub    $0xc,%esp
80101280:	50                   	push   %eax
80101281:	e8 82 07 00 00       	call   80101a08 <ilock>
80101286:	83 c4 10             	add    $0x10,%esp
    if((r = readi(f->ip, addr, f->off, n)) > 0)
80101289:	8b 4d 10             	mov    0x10(%ebp),%ecx
8010128c:	8b 45 08             	mov    0x8(%ebp),%eax
8010128f:	8b 50 14             	mov    0x14(%eax),%edx
80101292:	8b 45 08             	mov    0x8(%ebp),%eax
80101295:	8b 40 10             	mov    0x10(%eax),%eax
80101298:	51                   	push   %ecx
80101299:	52                   	push   %edx
8010129a:	ff 75 0c             	pushl  0xc(%ebp)
8010129d:	50                   	push   %eax
8010129e:	e8 d3 0c 00 00       	call   80101f76 <readi>
801012a3:	83 c4 10             	add    $0x10,%esp
801012a6:	89 45 f4             	mov    %eax,-0xc(%ebp)
801012a9:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801012ad:	7e 11                	jle    801012c0 <fileread+0x92>
      f->off += r;
801012af:	8b 45 08             	mov    0x8(%ebp),%eax
801012b2:	8b 50 14             	mov    0x14(%eax),%edx
801012b5:	8b 45 f4             	mov    -0xc(%ebp),%eax
801012b8:	01 c2                	add    %eax,%edx
801012ba:	8b 45 08             	mov    0x8(%ebp),%eax
801012bd:	89 50 14             	mov    %edx,0x14(%eax)
    iunlock(f->ip);
801012c0:	8b 45 08             	mov    0x8(%ebp),%eax
801012c3:	8b 40 10             	mov    0x10(%eax),%eax
801012c6:	83 ec 0c             	sub    $0xc,%esp
801012c9:	50                   	push   %eax
801012ca:	e8 97 08 00 00       	call   80101b66 <iunlock>
801012cf:	83 c4 10             	add    $0x10,%esp
    return r;
801012d2:	8b 45 f4             	mov    -0xc(%ebp),%eax
801012d5:	eb 0d                	jmp    801012e4 <fileread+0xb6>
  }
  panic("fileread");
801012d7:	83 ec 0c             	sub    $0xc,%esp
801012da:	68 6f a1 10 80       	push   $0x8010a16f
801012df:	e8 82 f2 ff ff       	call   80100566 <panic>
}
801012e4:	c9                   	leave  
801012e5:	c3                   	ret    

801012e6 <filewrite>:

// Write to file f.
int
filewrite(struct file *f, char *addr, int n)
{
801012e6:	55                   	push   %ebp
801012e7:	89 e5                	mov    %esp,%ebp
801012e9:	53                   	push   %ebx
801012ea:	83 ec 14             	sub    $0x14,%esp
  int r;

  if(f->writable == 0)
801012ed:	8b 45 08             	mov    0x8(%ebp),%eax
801012f0:	0f b6 40 09          	movzbl 0x9(%eax),%eax
801012f4:	84 c0                	test   %al,%al
801012f6:	75 0a                	jne    80101302 <filewrite+0x1c>
    return -1;
801012f8:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801012fd:	e9 1b 01 00 00       	jmp    8010141d <filewrite+0x137>
  if(f->type == FD_PIPE)
80101302:	8b 45 08             	mov    0x8(%ebp),%eax
80101305:	8b 00                	mov    (%eax),%eax
80101307:	83 f8 01             	cmp    $0x1,%eax
8010130a:	75 1d                	jne    80101329 <filewrite+0x43>
    return pipewrite(f->pipe, addr, n);
8010130c:	8b 45 08             	mov    0x8(%ebp),%eax
8010130f:	8b 40 0c             	mov    0xc(%eax),%eax
80101312:	83 ec 04             	sub    $0x4,%esp
80101315:	ff 75 10             	pushl  0x10(%ebp)
80101318:	ff 75 0c             	pushl  0xc(%ebp)
8010131b:	50                   	push   %eax
8010131c:	e8 bb 2f 00 00       	call   801042dc <pipewrite>
80101321:	83 c4 10             	add    $0x10,%esp
80101324:	e9 f4 00 00 00       	jmp    8010141d <filewrite+0x137>
  if(f->type == FD_INODE){
80101329:	8b 45 08             	mov    0x8(%ebp),%eax
8010132c:	8b 00                	mov    (%eax),%eax
8010132e:	83 f8 02             	cmp    $0x2,%eax
80101331:	0f 85 d9 00 00 00    	jne    80101410 <filewrite+0x12a>
    // the maximum log transaction size, including
    // i-node, indirect block, allocation blocks,
    // and 2 blocks of slop for non-aligned writes.
    // this really belongs lower down, since writei()
    // might be writing a device like the console.
    int max = ((LOGSIZE-1-1-2) / 2) * 512;
80101337:	c7 45 ec 00 1a 00 00 	movl   $0x1a00,-0x14(%ebp)
    int i = 0;
8010133e:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
    while(i < n){
80101345:	e9 a3 00 00 00       	jmp    801013ed <filewrite+0x107>
      int n1 = n - i;
8010134a:	8b 45 10             	mov    0x10(%ebp),%eax
8010134d:	2b 45 f4             	sub    -0xc(%ebp),%eax
80101350:	89 45 f0             	mov    %eax,-0x10(%ebp)
      if(n1 > max)
80101353:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101356:	3b 45 ec             	cmp    -0x14(%ebp),%eax
80101359:	7e 06                	jle    80101361 <filewrite+0x7b>
        n1 = max;
8010135b:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010135e:	89 45 f0             	mov    %eax,-0x10(%ebp)

      begin_op();
80101361:	e8 85 22 00 00       	call   801035eb <begin_op>
      ilock(f->ip);
80101366:	8b 45 08             	mov    0x8(%ebp),%eax
80101369:	8b 40 10             	mov    0x10(%eax),%eax
8010136c:	83 ec 0c             	sub    $0xc,%esp
8010136f:	50                   	push   %eax
80101370:	e8 93 06 00 00       	call   80101a08 <ilock>
80101375:	83 c4 10             	add    $0x10,%esp
      if ((r = writei(f->ip, addr + i, f->off, n1)) > 0)
80101378:	8b 4d f0             	mov    -0x10(%ebp),%ecx
8010137b:	8b 45 08             	mov    0x8(%ebp),%eax
8010137e:	8b 50 14             	mov    0x14(%eax),%edx
80101381:	8b 5d f4             	mov    -0xc(%ebp),%ebx
80101384:	8b 45 0c             	mov    0xc(%ebp),%eax
80101387:	01 c3                	add    %eax,%ebx
80101389:	8b 45 08             	mov    0x8(%ebp),%eax
8010138c:	8b 40 10             	mov    0x10(%eax),%eax
8010138f:	51                   	push   %ecx
80101390:	52                   	push   %edx
80101391:	53                   	push   %ebx
80101392:	50                   	push   %eax
80101393:	e8 35 0d 00 00       	call   801020cd <writei>
80101398:	83 c4 10             	add    $0x10,%esp
8010139b:	89 45 e8             	mov    %eax,-0x18(%ebp)
8010139e:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
801013a2:	7e 11                	jle    801013b5 <filewrite+0xcf>
        f->off += r;
801013a4:	8b 45 08             	mov    0x8(%ebp),%eax
801013a7:	8b 50 14             	mov    0x14(%eax),%edx
801013aa:	8b 45 e8             	mov    -0x18(%ebp),%eax
801013ad:	01 c2                	add    %eax,%edx
801013af:	8b 45 08             	mov    0x8(%ebp),%eax
801013b2:	89 50 14             	mov    %edx,0x14(%eax)
      iunlock(f->ip);
801013b5:	8b 45 08             	mov    0x8(%ebp),%eax
801013b8:	8b 40 10             	mov    0x10(%eax),%eax
801013bb:	83 ec 0c             	sub    $0xc,%esp
801013be:	50                   	push   %eax
801013bf:	e8 a2 07 00 00       	call   80101b66 <iunlock>
801013c4:	83 c4 10             	add    $0x10,%esp
      end_op();
801013c7:	e8 ab 22 00 00       	call   80103677 <end_op>

      if(r < 0)
801013cc:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
801013d0:	78 29                	js     801013fb <filewrite+0x115>
        break;
      if(r != n1)
801013d2:	8b 45 e8             	mov    -0x18(%ebp),%eax
801013d5:	3b 45 f0             	cmp    -0x10(%ebp),%eax
801013d8:	74 0d                	je     801013e7 <filewrite+0x101>
        panic("short filewrite");
801013da:	83 ec 0c             	sub    $0xc,%esp
801013dd:	68 78 a1 10 80       	push   $0x8010a178
801013e2:	e8 7f f1 ff ff       	call   80100566 <panic>
      i += r;
801013e7:	8b 45 e8             	mov    -0x18(%ebp),%eax
801013ea:	01 45 f4             	add    %eax,-0xc(%ebp)
    // and 2 blocks of slop for non-aligned writes.
    // this really belongs lower down, since writei()
    // might be writing a device like the console.
    int max = ((LOGSIZE-1-1-2) / 2) * 512;
    int i = 0;
    while(i < n){
801013ed:	8b 45 f4             	mov    -0xc(%ebp),%eax
801013f0:	3b 45 10             	cmp    0x10(%ebp),%eax
801013f3:	0f 8c 51 ff ff ff    	jl     8010134a <filewrite+0x64>
801013f9:	eb 01                	jmp    801013fc <filewrite+0x116>
        f->off += r;
      iunlock(f->ip);
      end_op();

      if(r < 0)
        break;
801013fb:	90                   	nop
      if(r != n1)
        panic("short filewrite");
      i += r;
    }
    return i == n ? n : -1;
801013fc:	8b 45 f4             	mov    -0xc(%ebp),%eax
801013ff:	3b 45 10             	cmp    0x10(%ebp),%eax
80101402:	75 05                	jne    80101409 <filewrite+0x123>
80101404:	8b 45 10             	mov    0x10(%ebp),%eax
80101407:	eb 14                	jmp    8010141d <filewrite+0x137>
80101409:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010140e:	eb 0d                	jmp    8010141d <filewrite+0x137>
  }
  panic("filewrite");
80101410:	83 ec 0c             	sub    $0xc,%esp
80101413:	68 88 a1 10 80       	push   $0x8010a188
80101418:	e8 49 f1 ff ff       	call   80100566 <panic>
}
8010141d:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80101420:	c9                   	leave  
80101421:	c3                   	ret    

80101422 <readsb>:
struct superblock sb;   // there should be one per dev, but we run with one dev

// Read the super block.
void
readsb(int dev, struct superblock *sb)
{
80101422:	55                   	push   %ebp
80101423:	89 e5                	mov    %esp,%ebp
80101425:	83 ec 18             	sub    $0x18,%esp
  struct buf *bp;
  
  bp = bread(dev, 1);
80101428:	8b 45 08             	mov    0x8(%ebp),%eax
8010142b:	83 ec 08             	sub    $0x8,%esp
8010142e:	6a 01                	push   $0x1
80101430:	50                   	push   %eax
80101431:	e8 80 ed ff ff       	call   801001b6 <bread>
80101436:	83 c4 10             	add    $0x10,%esp
80101439:	89 45 f4             	mov    %eax,-0xc(%ebp)
  memmove(sb, bp->data, sizeof(*sb));
8010143c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010143f:	83 c0 18             	add    $0x18,%eax
80101442:	83 ec 04             	sub    $0x4,%esp
80101445:	6a 1c                	push   $0x1c
80101447:	50                   	push   %eax
80101448:	ff 75 0c             	pushl  0xc(%ebp)
8010144b:	e8 07 59 00 00       	call   80106d57 <memmove>
80101450:	83 c4 10             	add    $0x10,%esp
  brelse(bp);
80101453:	83 ec 0c             	sub    $0xc,%esp
80101456:	ff 75 f4             	pushl  -0xc(%ebp)
80101459:	e8 d0 ed ff ff       	call   8010022e <brelse>
8010145e:	83 c4 10             	add    $0x10,%esp
}
80101461:	90                   	nop
80101462:	c9                   	leave  
80101463:	c3                   	ret    

80101464 <bzero>:

// Zero a block.
static void
bzero(int dev, int bno)
{
80101464:	55                   	push   %ebp
80101465:	89 e5                	mov    %esp,%ebp
80101467:	83 ec 18             	sub    $0x18,%esp
  struct buf *bp;
  
  bp = bread(dev, bno);
8010146a:	8b 55 0c             	mov    0xc(%ebp),%edx
8010146d:	8b 45 08             	mov    0x8(%ebp),%eax
80101470:	83 ec 08             	sub    $0x8,%esp
80101473:	52                   	push   %edx
80101474:	50                   	push   %eax
80101475:	e8 3c ed ff ff       	call   801001b6 <bread>
8010147a:	83 c4 10             	add    $0x10,%esp
8010147d:	89 45 f4             	mov    %eax,-0xc(%ebp)
  memset(bp->data, 0, BSIZE);
80101480:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101483:	83 c0 18             	add    $0x18,%eax
80101486:	83 ec 04             	sub    $0x4,%esp
80101489:	68 00 02 00 00       	push   $0x200
8010148e:	6a 00                	push   $0x0
80101490:	50                   	push   %eax
80101491:	e8 02 58 00 00       	call   80106c98 <memset>
80101496:	83 c4 10             	add    $0x10,%esp
  log_write(bp);
80101499:	83 ec 0c             	sub    $0xc,%esp
8010149c:	ff 75 f4             	pushl  -0xc(%ebp)
8010149f:	e8 7f 23 00 00       	call   80103823 <log_write>
801014a4:	83 c4 10             	add    $0x10,%esp
  brelse(bp);
801014a7:	83 ec 0c             	sub    $0xc,%esp
801014aa:	ff 75 f4             	pushl  -0xc(%ebp)
801014ad:	e8 7c ed ff ff       	call   8010022e <brelse>
801014b2:	83 c4 10             	add    $0x10,%esp
}
801014b5:	90                   	nop
801014b6:	c9                   	leave  
801014b7:	c3                   	ret    

801014b8 <balloc>:
// Blocks. 

// Allocate a zeroed disk block.
static uint
balloc(uint dev)
{
801014b8:	55                   	push   %ebp
801014b9:	89 e5                	mov    %esp,%ebp
801014bb:	83 ec 18             	sub    $0x18,%esp
  int b, bi, m;
  struct buf *bp;

  bp = 0;
801014be:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  for(b = 0; b < sb.size; b += BPB){
801014c5:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
801014cc:	e9 13 01 00 00       	jmp    801015e4 <balloc+0x12c>
    bp = bread(dev, BBLOCK(b, sb));
801014d1:	8b 45 f4             	mov    -0xc(%ebp),%eax
801014d4:	8d 90 ff 0f 00 00    	lea    0xfff(%eax),%edx
801014da:	85 c0                	test   %eax,%eax
801014dc:	0f 48 c2             	cmovs  %edx,%eax
801014df:	c1 f8 0c             	sar    $0xc,%eax
801014e2:	89 c2                	mov    %eax,%edx
801014e4:	a1 58 32 11 80       	mov    0x80113258,%eax
801014e9:	01 d0                	add    %edx,%eax
801014eb:	83 ec 08             	sub    $0x8,%esp
801014ee:	50                   	push   %eax
801014ef:	ff 75 08             	pushl  0x8(%ebp)
801014f2:	e8 bf ec ff ff       	call   801001b6 <bread>
801014f7:	83 c4 10             	add    $0x10,%esp
801014fa:	89 45 ec             	mov    %eax,-0x14(%ebp)
    for(bi = 0; bi < BPB && b + bi < sb.size; bi++){
801014fd:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
80101504:	e9 a6 00 00 00       	jmp    801015af <balloc+0xf7>
      m = 1 << (bi % 8);
80101509:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010150c:	99                   	cltd   
8010150d:	c1 ea 1d             	shr    $0x1d,%edx
80101510:	01 d0                	add    %edx,%eax
80101512:	83 e0 07             	and    $0x7,%eax
80101515:	29 d0                	sub    %edx,%eax
80101517:	ba 01 00 00 00       	mov    $0x1,%edx
8010151c:	89 c1                	mov    %eax,%ecx
8010151e:	d3 e2                	shl    %cl,%edx
80101520:	89 d0                	mov    %edx,%eax
80101522:	89 45 e8             	mov    %eax,-0x18(%ebp)
      if((bp->data[bi/8] & m) == 0){  // Is block free?
80101525:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101528:	8d 50 07             	lea    0x7(%eax),%edx
8010152b:	85 c0                	test   %eax,%eax
8010152d:	0f 48 c2             	cmovs  %edx,%eax
80101530:	c1 f8 03             	sar    $0x3,%eax
80101533:	89 c2                	mov    %eax,%edx
80101535:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101538:	0f b6 44 10 18       	movzbl 0x18(%eax,%edx,1),%eax
8010153d:	0f b6 c0             	movzbl %al,%eax
80101540:	23 45 e8             	and    -0x18(%ebp),%eax
80101543:	85 c0                	test   %eax,%eax
80101545:	75 64                	jne    801015ab <balloc+0xf3>
        bp->data[bi/8] |= m;  // Mark block in use.
80101547:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010154a:	8d 50 07             	lea    0x7(%eax),%edx
8010154d:	85 c0                	test   %eax,%eax
8010154f:	0f 48 c2             	cmovs  %edx,%eax
80101552:	c1 f8 03             	sar    $0x3,%eax
80101555:	8b 55 ec             	mov    -0x14(%ebp),%edx
80101558:	0f b6 54 02 18       	movzbl 0x18(%edx,%eax,1),%edx
8010155d:	89 d1                	mov    %edx,%ecx
8010155f:	8b 55 e8             	mov    -0x18(%ebp),%edx
80101562:	09 ca                	or     %ecx,%edx
80101564:	89 d1                	mov    %edx,%ecx
80101566:	8b 55 ec             	mov    -0x14(%ebp),%edx
80101569:	88 4c 02 18          	mov    %cl,0x18(%edx,%eax,1)
        log_write(bp);
8010156d:	83 ec 0c             	sub    $0xc,%esp
80101570:	ff 75 ec             	pushl  -0x14(%ebp)
80101573:	e8 ab 22 00 00       	call   80103823 <log_write>
80101578:	83 c4 10             	add    $0x10,%esp
        brelse(bp);
8010157b:	83 ec 0c             	sub    $0xc,%esp
8010157e:	ff 75 ec             	pushl  -0x14(%ebp)
80101581:	e8 a8 ec ff ff       	call   8010022e <brelse>
80101586:	83 c4 10             	add    $0x10,%esp
        bzero(dev, b + bi);
80101589:	8b 55 f4             	mov    -0xc(%ebp),%edx
8010158c:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010158f:	01 c2                	add    %eax,%edx
80101591:	8b 45 08             	mov    0x8(%ebp),%eax
80101594:	83 ec 08             	sub    $0x8,%esp
80101597:	52                   	push   %edx
80101598:	50                   	push   %eax
80101599:	e8 c6 fe ff ff       	call   80101464 <bzero>
8010159e:	83 c4 10             	add    $0x10,%esp
        return b + bi;
801015a1:	8b 55 f4             	mov    -0xc(%ebp),%edx
801015a4:	8b 45 f0             	mov    -0x10(%ebp),%eax
801015a7:	01 d0                	add    %edx,%eax
801015a9:	eb 57                	jmp    80101602 <balloc+0x14a>
  struct buf *bp;

  bp = 0;
  for(b = 0; b < sb.size; b += BPB){
    bp = bread(dev, BBLOCK(b, sb));
    for(bi = 0; bi < BPB && b + bi < sb.size; bi++){
801015ab:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
801015af:	81 7d f0 ff 0f 00 00 	cmpl   $0xfff,-0x10(%ebp)
801015b6:	7f 17                	jg     801015cf <balloc+0x117>
801015b8:	8b 55 f4             	mov    -0xc(%ebp),%edx
801015bb:	8b 45 f0             	mov    -0x10(%ebp),%eax
801015be:	01 d0                	add    %edx,%eax
801015c0:	89 c2                	mov    %eax,%edx
801015c2:	a1 40 32 11 80       	mov    0x80113240,%eax
801015c7:	39 c2                	cmp    %eax,%edx
801015c9:	0f 82 3a ff ff ff    	jb     80101509 <balloc+0x51>
        brelse(bp);
        bzero(dev, b + bi);
        return b + bi;
      }
    }
    brelse(bp);
801015cf:	83 ec 0c             	sub    $0xc,%esp
801015d2:	ff 75 ec             	pushl  -0x14(%ebp)
801015d5:	e8 54 ec ff ff       	call   8010022e <brelse>
801015da:	83 c4 10             	add    $0x10,%esp
{
  int b, bi, m;
  struct buf *bp;

  bp = 0;
  for(b = 0; b < sb.size; b += BPB){
801015dd:	81 45 f4 00 10 00 00 	addl   $0x1000,-0xc(%ebp)
801015e4:	8b 15 40 32 11 80    	mov    0x80113240,%edx
801015ea:	8b 45 f4             	mov    -0xc(%ebp),%eax
801015ed:	39 c2                	cmp    %eax,%edx
801015ef:	0f 87 dc fe ff ff    	ja     801014d1 <balloc+0x19>
        return b + bi;
      }
    }
    brelse(bp);
  }
  panic("balloc: out of blocks");
801015f5:	83 ec 0c             	sub    $0xc,%esp
801015f8:	68 94 a1 10 80       	push   $0x8010a194
801015fd:	e8 64 ef ff ff       	call   80100566 <panic>
}
80101602:	c9                   	leave  
80101603:	c3                   	ret    

80101604 <bfree>:

// Free a disk block.
static void
bfree(int dev, uint b)
{
80101604:	55                   	push   %ebp
80101605:	89 e5                	mov    %esp,%ebp
80101607:	83 ec 18             	sub    $0x18,%esp
  struct buf *bp;
  int bi, m;

  readsb(dev, &sb);
8010160a:	83 ec 08             	sub    $0x8,%esp
8010160d:	68 40 32 11 80       	push   $0x80113240
80101612:	ff 75 08             	pushl  0x8(%ebp)
80101615:	e8 08 fe ff ff       	call   80101422 <readsb>
8010161a:	83 c4 10             	add    $0x10,%esp
  bp = bread(dev, BBLOCK(b, sb));
8010161d:	8b 45 0c             	mov    0xc(%ebp),%eax
80101620:	c1 e8 0c             	shr    $0xc,%eax
80101623:	89 c2                	mov    %eax,%edx
80101625:	a1 58 32 11 80       	mov    0x80113258,%eax
8010162a:	01 c2                	add    %eax,%edx
8010162c:	8b 45 08             	mov    0x8(%ebp),%eax
8010162f:	83 ec 08             	sub    $0x8,%esp
80101632:	52                   	push   %edx
80101633:	50                   	push   %eax
80101634:	e8 7d eb ff ff       	call   801001b6 <bread>
80101639:	83 c4 10             	add    $0x10,%esp
8010163c:	89 45 f4             	mov    %eax,-0xc(%ebp)
  bi = b % BPB;
8010163f:	8b 45 0c             	mov    0xc(%ebp),%eax
80101642:	25 ff 0f 00 00       	and    $0xfff,%eax
80101647:	89 45 f0             	mov    %eax,-0x10(%ebp)
  m = 1 << (bi % 8);
8010164a:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010164d:	99                   	cltd   
8010164e:	c1 ea 1d             	shr    $0x1d,%edx
80101651:	01 d0                	add    %edx,%eax
80101653:	83 e0 07             	and    $0x7,%eax
80101656:	29 d0                	sub    %edx,%eax
80101658:	ba 01 00 00 00       	mov    $0x1,%edx
8010165d:	89 c1                	mov    %eax,%ecx
8010165f:	d3 e2                	shl    %cl,%edx
80101661:	89 d0                	mov    %edx,%eax
80101663:	89 45 ec             	mov    %eax,-0x14(%ebp)
  if((bp->data[bi/8] & m) == 0)
80101666:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101669:	8d 50 07             	lea    0x7(%eax),%edx
8010166c:	85 c0                	test   %eax,%eax
8010166e:	0f 48 c2             	cmovs  %edx,%eax
80101671:	c1 f8 03             	sar    $0x3,%eax
80101674:	89 c2                	mov    %eax,%edx
80101676:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101679:	0f b6 44 10 18       	movzbl 0x18(%eax,%edx,1),%eax
8010167e:	0f b6 c0             	movzbl %al,%eax
80101681:	23 45 ec             	and    -0x14(%ebp),%eax
80101684:	85 c0                	test   %eax,%eax
80101686:	75 0d                	jne    80101695 <bfree+0x91>
    panic("freeing free block");
80101688:	83 ec 0c             	sub    $0xc,%esp
8010168b:	68 aa a1 10 80       	push   $0x8010a1aa
80101690:	e8 d1 ee ff ff       	call   80100566 <panic>
  bp->data[bi/8] &= ~m;
80101695:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101698:	8d 50 07             	lea    0x7(%eax),%edx
8010169b:	85 c0                	test   %eax,%eax
8010169d:	0f 48 c2             	cmovs  %edx,%eax
801016a0:	c1 f8 03             	sar    $0x3,%eax
801016a3:	8b 55 f4             	mov    -0xc(%ebp),%edx
801016a6:	0f b6 54 02 18       	movzbl 0x18(%edx,%eax,1),%edx
801016ab:	89 d1                	mov    %edx,%ecx
801016ad:	8b 55 ec             	mov    -0x14(%ebp),%edx
801016b0:	f7 d2                	not    %edx
801016b2:	21 ca                	and    %ecx,%edx
801016b4:	89 d1                	mov    %edx,%ecx
801016b6:	8b 55 f4             	mov    -0xc(%ebp),%edx
801016b9:	88 4c 02 18          	mov    %cl,0x18(%edx,%eax,1)
  log_write(bp);
801016bd:	83 ec 0c             	sub    $0xc,%esp
801016c0:	ff 75 f4             	pushl  -0xc(%ebp)
801016c3:	e8 5b 21 00 00       	call   80103823 <log_write>
801016c8:	83 c4 10             	add    $0x10,%esp
  brelse(bp);
801016cb:	83 ec 0c             	sub    $0xc,%esp
801016ce:	ff 75 f4             	pushl  -0xc(%ebp)
801016d1:	e8 58 eb ff ff       	call   8010022e <brelse>
801016d6:	83 c4 10             	add    $0x10,%esp
}
801016d9:	90                   	nop
801016da:	c9                   	leave  
801016db:	c3                   	ret    

801016dc <iinit>:
  struct inode inode[NINODE];
} icache;

void
iinit(int dev)
{
801016dc:	55                   	push   %ebp
801016dd:	89 e5                	mov    %esp,%ebp
801016df:	57                   	push   %edi
801016e0:	56                   	push   %esi
801016e1:	53                   	push   %ebx
801016e2:	83 ec 1c             	sub    $0x1c,%esp
  initlock(&icache.lock, "icache");
801016e5:	83 ec 08             	sub    $0x8,%esp
801016e8:	68 bd a1 10 80       	push   $0x8010a1bd
801016ed:	68 60 32 11 80       	push   $0x80113260
801016f2:	e8 1c 53 00 00       	call   80106a13 <initlock>
801016f7:	83 c4 10             	add    $0x10,%esp
  readsb(dev, &sb);
801016fa:	83 ec 08             	sub    $0x8,%esp
801016fd:	68 40 32 11 80       	push   $0x80113240
80101702:	ff 75 08             	pushl  0x8(%ebp)
80101705:	e8 18 fd ff ff       	call   80101422 <readsb>
8010170a:	83 c4 10             	add    $0x10,%esp
  cprintf("sb: size %d nblocks %d ninodes %d nlog %d logstart %d inodestart %d bmap start %d\n", sb.size,
8010170d:	a1 58 32 11 80       	mov    0x80113258,%eax
80101712:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80101715:	8b 3d 54 32 11 80    	mov    0x80113254,%edi
8010171b:	8b 35 50 32 11 80    	mov    0x80113250,%esi
80101721:	8b 1d 4c 32 11 80    	mov    0x8011324c,%ebx
80101727:	8b 0d 48 32 11 80    	mov    0x80113248,%ecx
8010172d:	8b 15 44 32 11 80    	mov    0x80113244,%edx
80101733:	a1 40 32 11 80       	mov    0x80113240,%eax
80101738:	ff 75 e4             	pushl  -0x1c(%ebp)
8010173b:	57                   	push   %edi
8010173c:	56                   	push   %esi
8010173d:	53                   	push   %ebx
8010173e:	51                   	push   %ecx
8010173f:	52                   	push   %edx
80101740:	50                   	push   %eax
80101741:	68 c4 a1 10 80       	push   $0x8010a1c4
80101746:	e8 7b ec ff ff       	call   801003c6 <cprintf>
8010174b:	83 c4 20             	add    $0x20,%esp
          sb.nblocks, sb.ninodes, sb.nlog, sb.logstart, sb.inodestart, sb.bmapstart);
}
8010174e:	90                   	nop
8010174f:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101752:	5b                   	pop    %ebx
80101753:	5e                   	pop    %esi
80101754:	5f                   	pop    %edi
80101755:	5d                   	pop    %ebp
80101756:	c3                   	ret    

80101757 <ialloc>:

// Allocate a new inode with the given type on device dev.
// A free inode has a type of zero.
struct inode*
ialloc(uint dev, short type)
{
80101757:	55                   	push   %ebp
80101758:	89 e5                	mov    %esp,%ebp
8010175a:	83 ec 28             	sub    $0x28,%esp
8010175d:	8b 45 0c             	mov    0xc(%ebp),%eax
80101760:	66 89 45 e4          	mov    %ax,-0x1c(%ebp)
  int inum;
  struct buf *bp;
  struct dinode *dip;

  for(inum = 1; inum < sb.ninodes; inum++){
80101764:	c7 45 f4 01 00 00 00 	movl   $0x1,-0xc(%ebp)
8010176b:	e9 9e 00 00 00       	jmp    8010180e <ialloc+0xb7>
    bp = bread(dev, IBLOCK(inum, sb));
80101770:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101773:	c1 e8 03             	shr    $0x3,%eax
80101776:	89 c2                	mov    %eax,%edx
80101778:	a1 54 32 11 80       	mov    0x80113254,%eax
8010177d:	01 d0                	add    %edx,%eax
8010177f:	83 ec 08             	sub    $0x8,%esp
80101782:	50                   	push   %eax
80101783:	ff 75 08             	pushl  0x8(%ebp)
80101786:	e8 2b ea ff ff       	call   801001b6 <bread>
8010178b:	83 c4 10             	add    $0x10,%esp
8010178e:	89 45 f0             	mov    %eax,-0x10(%ebp)
    dip = (struct dinode*)bp->data + inum%IPB;
80101791:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101794:	8d 50 18             	lea    0x18(%eax),%edx
80101797:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010179a:	83 e0 07             	and    $0x7,%eax
8010179d:	c1 e0 06             	shl    $0x6,%eax
801017a0:	01 d0                	add    %edx,%eax
801017a2:	89 45 ec             	mov    %eax,-0x14(%ebp)
    if(dip->type == 0){  // a free inode
801017a5:	8b 45 ec             	mov    -0x14(%ebp),%eax
801017a8:	0f b7 00             	movzwl (%eax),%eax
801017ab:	66 85 c0             	test   %ax,%ax
801017ae:	75 4c                	jne    801017fc <ialloc+0xa5>
      memset(dip, 0, sizeof(*dip));
801017b0:	83 ec 04             	sub    $0x4,%esp
801017b3:	6a 40                	push   $0x40
801017b5:	6a 00                	push   $0x0
801017b7:	ff 75 ec             	pushl  -0x14(%ebp)
801017ba:	e8 d9 54 00 00       	call   80106c98 <memset>
801017bf:	83 c4 10             	add    $0x10,%esp
      dip->type = type;
801017c2:	8b 45 ec             	mov    -0x14(%ebp),%eax
801017c5:	0f b7 55 e4          	movzwl -0x1c(%ebp),%edx
801017c9:	66 89 10             	mov    %dx,(%eax)
      log_write(bp);   // mark it allocated on the disk
801017cc:	83 ec 0c             	sub    $0xc,%esp
801017cf:	ff 75 f0             	pushl  -0x10(%ebp)
801017d2:	e8 4c 20 00 00       	call   80103823 <log_write>
801017d7:	83 c4 10             	add    $0x10,%esp
      brelse(bp);
801017da:	83 ec 0c             	sub    $0xc,%esp
801017dd:	ff 75 f0             	pushl  -0x10(%ebp)
801017e0:	e8 49 ea ff ff       	call   8010022e <brelse>
801017e5:	83 c4 10             	add    $0x10,%esp
      return iget(dev, inum);
801017e8:	8b 45 f4             	mov    -0xc(%ebp),%eax
801017eb:	83 ec 08             	sub    $0x8,%esp
801017ee:	50                   	push   %eax
801017ef:	ff 75 08             	pushl  0x8(%ebp)
801017f2:	e8 f8 00 00 00       	call   801018ef <iget>
801017f7:	83 c4 10             	add    $0x10,%esp
801017fa:	eb 30                	jmp    8010182c <ialloc+0xd5>
    }
    brelse(bp);
801017fc:	83 ec 0c             	sub    $0xc,%esp
801017ff:	ff 75 f0             	pushl  -0x10(%ebp)
80101802:	e8 27 ea ff ff       	call   8010022e <brelse>
80101807:	83 c4 10             	add    $0x10,%esp
{
  int inum;
  struct buf *bp;
  struct dinode *dip;

  for(inum = 1; inum < sb.ninodes; inum++){
8010180a:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
8010180e:	8b 15 48 32 11 80    	mov    0x80113248,%edx
80101814:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101817:	39 c2                	cmp    %eax,%edx
80101819:	0f 87 51 ff ff ff    	ja     80101770 <ialloc+0x19>
      brelse(bp);
      return iget(dev, inum);
    }
    brelse(bp);
  }
  panic("ialloc: no inodes");
8010181f:	83 ec 0c             	sub    $0xc,%esp
80101822:	68 17 a2 10 80       	push   $0x8010a217
80101827:	e8 3a ed ff ff       	call   80100566 <panic>
}
8010182c:	c9                   	leave  
8010182d:	c3                   	ret    

8010182e <iupdate>:

// Copy a modified in-memory inode to disk.
void
iupdate(struct inode *ip)
{
8010182e:	55                   	push   %ebp
8010182f:	89 e5                	mov    %esp,%ebp
80101831:	83 ec 18             	sub    $0x18,%esp
  struct buf *bp;
  struct dinode *dip;

  bp = bread(ip->dev, IBLOCK(ip->inum, sb));
80101834:	8b 45 08             	mov    0x8(%ebp),%eax
80101837:	8b 40 04             	mov    0x4(%eax),%eax
8010183a:	c1 e8 03             	shr    $0x3,%eax
8010183d:	89 c2                	mov    %eax,%edx
8010183f:	a1 54 32 11 80       	mov    0x80113254,%eax
80101844:	01 c2                	add    %eax,%edx
80101846:	8b 45 08             	mov    0x8(%ebp),%eax
80101849:	8b 00                	mov    (%eax),%eax
8010184b:	83 ec 08             	sub    $0x8,%esp
8010184e:	52                   	push   %edx
8010184f:	50                   	push   %eax
80101850:	e8 61 e9 ff ff       	call   801001b6 <bread>
80101855:	83 c4 10             	add    $0x10,%esp
80101858:	89 45 f4             	mov    %eax,-0xc(%ebp)
  dip = (struct dinode*)bp->data + ip->inum%IPB;
8010185b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010185e:	8d 50 18             	lea    0x18(%eax),%edx
80101861:	8b 45 08             	mov    0x8(%ebp),%eax
80101864:	8b 40 04             	mov    0x4(%eax),%eax
80101867:	83 e0 07             	and    $0x7,%eax
8010186a:	c1 e0 06             	shl    $0x6,%eax
8010186d:	01 d0                	add    %edx,%eax
8010186f:	89 45 f0             	mov    %eax,-0x10(%ebp)
  dip->type = ip->type;
80101872:	8b 45 08             	mov    0x8(%ebp),%eax
80101875:	0f b7 50 10          	movzwl 0x10(%eax),%edx
80101879:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010187c:	66 89 10             	mov    %dx,(%eax)
  dip->major = ip->major;
8010187f:	8b 45 08             	mov    0x8(%ebp),%eax
80101882:	0f b7 50 12          	movzwl 0x12(%eax),%edx
80101886:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101889:	66 89 50 02          	mov    %dx,0x2(%eax)
  dip->minor = ip->minor;
8010188d:	8b 45 08             	mov    0x8(%ebp),%eax
80101890:	0f b7 50 14          	movzwl 0x14(%eax),%edx
80101894:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101897:	66 89 50 04          	mov    %dx,0x4(%eax)
  dip->nlink = ip->nlink;
8010189b:	8b 45 08             	mov    0x8(%ebp),%eax
8010189e:	0f b7 50 16          	movzwl 0x16(%eax),%edx
801018a2:	8b 45 f0             	mov    -0x10(%ebp),%eax
801018a5:	66 89 50 06          	mov    %dx,0x6(%eax)
  dip->size = ip->size;
801018a9:	8b 45 08             	mov    0x8(%ebp),%eax
801018ac:	8b 50 18             	mov    0x18(%eax),%edx
801018af:	8b 45 f0             	mov    -0x10(%ebp),%eax
801018b2:	89 50 08             	mov    %edx,0x8(%eax)
  memmove(dip->addrs, ip->addrs, sizeof(ip->addrs));
801018b5:	8b 45 08             	mov    0x8(%ebp),%eax
801018b8:	8d 50 1c             	lea    0x1c(%eax),%edx
801018bb:	8b 45 f0             	mov    -0x10(%ebp),%eax
801018be:	83 c0 0c             	add    $0xc,%eax
801018c1:	83 ec 04             	sub    $0x4,%esp
801018c4:	6a 34                	push   $0x34
801018c6:	52                   	push   %edx
801018c7:	50                   	push   %eax
801018c8:	e8 8a 54 00 00       	call   80106d57 <memmove>
801018cd:	83 c4 10             	add    $0x10,%esp
  log_write(bp);
801018d0:	83 ec 0c             	sub    $0xc,%esp
801018d3:	ff 75 f4             	pushl  -0xc(%ebp)
801018d6:	e8 48 1f 00 00       	call   80103823 <log_write>
801018db:	83 c4 10             	add    $0x10,%esp
  brelse(bp);
801018de:	83 ec 0c             	sub    $0xc,%esp
801018e1:	ff 75 f4             	pushl  -0xc(%ebp)
801018e4:	e8 45 e9 ff ff       	call   8010022e <brelse>
801018e9:	83 c4 10             	add    $0x10,%esp
}
801018ec:	90                   	nop
801018ed:	c9                   	leave  
801018ee:	c3                   	ret    

801018ef <iget>:
// Find the inode with number inum on device dev
// and return the in-memory copy. Does not lock
// the inode and does not read it from disk.
static struct inode*
iget(uint dev, uint inum)
{
801018ef:	55                   	push   %ebp
801018f0:	89 e5                	mov    %esp,%ebp
801018f2:	83 ec 18             	sub    $0x18,%esp
  struct inode *ip, *empty;

  acquire(&icache.lock);
801018f5:	83 ec 0c             	sub    $0xc,%esp
801018f8:	68 60 32 11 80       	push   $0x80113260
801018fd:	e8 33 51 00 00       	call   80106a35 <acquire>
80101902:	83 c4 10             	add    $0x10,%esp

  // Is the inode already cached?
  empty = 0;
80101905:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
8010190c:	c7 45 f4 94 32 11 80 	movl   $0x80113294,-0xc(%ebp)
80101913:	eb 5d                	jmp    80101972 <iget+0x83>
    if(ip->ref > 0 && ip->dev == dev && ip->inum == inum){
80101915:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101918:	8b 40 08             	mov    0x8(%eax),%eax
8010191b:	85 c0                	test   %eax,%eax
8010191d:	7e 39                	jle    80101958 <iget+0x69>
8010191f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101922:	8b 00                	mov    (%eax),%eax
80101924:	3b 45 08             	cmp    0x8(%ebp),%eax
80101927:	75 2f                	jne    80101958 <iget+0x69>
80101929:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010192c:	8b 40 04             	mov    0x4(%eax),%eax
8010192f:	3b 45 0c             	cmp    0xc(%ebp),%eax
80101932:	75 24                	jne    80101958 <iget+0x69>
      ip->ref++;
80101934:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101937:	8b 40 08             	mov    0x8(%eax),%eax
8010193a:	8d 50 01             	lea    0x1(%eax),%edx
8010193d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101940:	89 50 08             	mov    %edx,0x8(%eax)
      release(&icache.lock);
80101943:	83 ec 0c             	sub    $0xc,%esp
80101946:	68 60 32 11 80       	push   $0x80113260
8010194b:	e8 4c 51 00 00       	call   80106a9c <release>
80101950:	83 c4 10             	add    $0x10,%esp
      return ip;
80101953:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101956:	eb 74                	jmp    801019cc <iget+0xdd>
    }
    if(empty == 0 && ip->ref == 0)    // Remember empty slot.
80101958:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
8010195c:	75 10                	jne    8010196e <iget+0x7f>
8010195e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101961:	8b 40 08             	mov    0x8(%eax),%eax
80101964:	85 c0                	test   %eax,%eax
80101966:	75 06                	jne    8010196e <iget+0x7f>
      empty = ip;
80101968:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010196b:	89 45 f0             	mov    %eax,-0x10(%ebp)

  acquire(&icache.lock);

  // Is the inode already cached?
  empty = 0;
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
8010196e:	83 45 f4 50          	addl   $0x50,-0xc(%ebp)
80101972:	81 7d f4 34 42 11 80 	cmpl   $0x80114234,-0xc(%ebp)
80101979:	72 9a                	jb     80101915 <iget+0x26>
    if(empty == 0 && ip->ref == 0)    // Remember empty slot.
      empty = ip;
  }

  // Recycle an inode cache entry.
  if(empty == 0)
8010197b:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
8010197f:	75 0d                	jne    8010198e <iget+0x9f>
    panic("iget: no inodes");
80101981:	83 ec 0c             	sub    $0xc,%esp
80101984:	68 29 a2 10 80       	push   $0x8010a229
80101989:	e8 d8 eb ff ff       	call   80100566 <panic>

  ip = empty;
8010198e:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101991:	89 45 f4             	mov    %eax,-0xc(%ebp)
  ip->dev = dev;
80101994:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101997:	8b 55 08             	mov    0x8(%ebp),%edx
8010199a:	89 10                	mov    %edx,(%eax)
  ip->inum = inum;
8010199c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010199f:	8b 55 0c             	mov    0xc(%ebp),%edx
801019a2:	89 50 04             	mov    %edx,0x4(%eax)
  ip->ref = 1;
801019a5:	8b 45 f4             	mov    -0xc(%ebp),%eax
801019a8:	c7 40 08 01 00 00 00 	movl   $0x1,0x8(%eax)
  ip->flags = 0;
801019af:	8b 45 f4             	mov    -0xc(%ebp),%eax
801019b2:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
  release(&icache.lock);
801019b9:	83 ec 0c             	sub    $0xc,%esp
801019bc:	68 60 32 11 80       	push   $0x80113260
801019c1:	e8 d6 50 00 00       	call   80106a9c <release>
801019c6:	83 c4 10             	add    $0x10,%esp

  return ip;
801019c9:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
801019cc:	c9                   	leave  
801019cd:	c3                   	ret    

801019ce <idup>:

// Increment reference count for ip.
// Returns ip to enable ip = idup(ip1) idiom.
struct inode*
idup(struct inode *ip)
{
801019ce:	55                   	push   %ebp
801019cf:	89 e5                	mov    %esp,%ebp
801019d1:	83 ec 08             	sub    $0x8,%esp
  acquire(&icache.lock);
801019d4:	83 ec 0c             	sub    $0xc,%esp
801019d7:	68 60 32 11 80       	push   $0x80113260
801019dc:	e8 54 50 00 00       	call   80106a35 <acquire>
801019e1:	83 c4 10             	add    $0x10,%esp
  ip->ref++;
801019e4:	8b 45 08             	mov    0x8(%ebp),%eax
801019e7:	8b 40 08             	mov    0x8(%eax),%eax
801019ea:	8d 50 01             	lea    0x1(%eax),%edx
801019ed:	8b 45 08             	mov    0x8(%ebp),%eax
801019f0:	89 50 08             	mov    %edx,0x8(%eax)
  release(&icache.lock);
801019f3:	83 ec 0c             	sub    $0xc,%esp
801019f6:	68 60 32 11 80       	push   $0x80113260
801019fb:	e8 9c 50 00 00       	call   80106a9c <release>
80101a00:	83 c4 10             	add    $0x10,%esp
  return ip;
80101a03:	8b 45 08             	mov    0x8(%ebp),%eax
}
80101a06:	c9                   	leave  
80101a07:	c3                   	ret    

80101a08 <ilock>:

// Lock the given inode.
// Reads the inode from disk if necessary.
void
ilock(struct inode *ip)
{
80101a08:	55                   	push   %ebp
80101a09:	89 e5                	mov    %esp,%ebp
80101a0b:	83 ec 18             	sub    $0x18,%esp
  struct buf *bp;
  struct dinode *dip;

  if(ip == 0 || ip->ref < 1)
80101a0e:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80101a12:	74 0a                	je     80101a1e <ilock+0x16>
80101a14:	8b 45 08             	mov    0x8(%ebp),%eax
80101a17:	8b 40 08             	mov    0x8(%eax),%eax
80101a1a:	85 c0                	test   %eax,%eax
80101a1c:	7f 0d                	jg     80101a2b <ilock+0x23>
    panic("ilock");
80101a1e:	83 ec 0c             	sub    $0xc,%esp
80101a21:	68 39 a2 10 80       	push   $0x8010a239
80101a26:	e8 3b eb ff ff       	call   80100566 <panic>

  acquire(&icache.lock);
80101a2b:	83 ec 0c             	sub    $0xc,%esp
80101a2e:	68 60 32 11 80       	push   $0x80113260
80101a33:	e8 fd 4f 00 00       	call   80106a35 <acquire>
80101a38:	83 c4 10             	add    $0x10,%esp
  while(ip->flags & I_BUSY)
80101a3b:	eb 13                	jmp    80101a50 <ilock+0x48>
    sleep(ip, &icache.lock);
80101a3d:	83 ec 08             	sub    $0x8,%esp
80101a40:	68 60 32 11 80       	push   $0x80113260
80101a45:	ff 75 08             	pushl  0x8(%ebp)
80101a48:	e8 9f 39 00 00       	call   801053ec <sleep>
80101a4d:	83 c4 10             	add    $0x10,%esp

  if(ip == 0 || ip->ref < 1)
    panic("ilock");

  acquire(&icache.lock);
  while(ip->flags & I_BUSY)
80101a50:	8b 45 08             	mov    0x8(%ebp),%eax
80101a53:	8b 40 0c             	mov    0xc(%eax),%eax
80101a56:	83 e0 01             	and    $0x1,%eax
80101a59:	85 c0                	test   %eax,%eax
80101a5b:	75 e0                	jne    80101a3d <ilock+0x35>
    sleep(ip, &icache.lock);
  ip->flags |= I_BUSY;
80101a5d:	8b 45 08             	mov    0x8(%ebp),%eax
80101a60:	8b 40 0c             	mov    0xc(%eax),%eax
80101a63:	83 c8 01             	or     $0x1,%eax
80101a66:	89 c2                	mov    %eax,%edx
80101a68:	8b 45 08             	mov    0x8(%ebp),%eax
80101a6b:	89 50 0c             	mov    %edx,0xc(%eax)
  release(&icache.lock);
80101a6e:	83 ec 0c             	sub    $0xc,%esp
80101a71:	68 60 32 11 80       	push   $0x80113260
80101a76:	e8 21 50 00 00       	call   80106a9c <release>
80101a7b:	83 c4 10             	add    $0x10,%esp

  if(!(ip->flags & I_VALID)){
80101a7e:	8b 45 08             	mov    0x8(%ebp),%eax
80101a81:	8b 40 0c             	mov    0xc(%eax),%eax
80101a84:	83 e0 02             	and    $0x2,%eax
80101a87:	85 c0                	test   %eax,%eax
80101a89:	0f 85 d4 00 00 00    	jne    80101b63 <ilock+0x15b>
    bp = bread(ip->dev, IBLOCK(ip->inum, sb));
80101a8f:	8b 45 08             	mov    0x8(%ebp),%eax
80101a92:	8b 40 04             	mov    0x4(%eax),%eax
80101a95:	c1 e8 03             	shr    $0x3,%eax
80101a98:	89 c2                	mov    %eax,%edx
80101a9a:	a1 54 32 11 80       	mov    0x80113254,%eax
80101a9f:	01 c2                	add    %eax,%edx
80101aa1:	8b 45 08             	mov    0x8(%ebp),%eax
80101aa4:	8b 00                	mov    (%eax),%eax
80101aa6:	83 ec 08             	sub    $0x8,%esp
80101aa9:	52                   	push   %edx
80101aaa:	50                   	push   %eax
80101aab:	e8 06 e7 ff ff       	call   801001b6 <bread>
80101ab0:	83 c4 10             	add    $0x10,%esp
80101ab3:	89 45 f4             	mov    %eax,-0xc(%ebp)
    dip = (struct dinode*)bp->data + ip->inum%IPB;
80101ab6:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101ab9:	8d 50 18             	lea    0x18(%eax),%edx
80101abc:	8b 45 08             	mov    0x8(%ebp),%eax
80101abf:	8b 40 04             	mov    0x4(%eax),%eax
80101ac2:	83 e0 07             	and    $0x7,%eax
80101ac5:	c1 e0 06             	shl    $0x6,%eax
80101ac8:	01 d0                	add    %edx,%eax
80101aca:	89 45 f0             	mov    %eax,-0x10(%ebp)
    ip->type = dip->type;
80101acd:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101ad0:	0f b7 10             	movzwl (%eax),%edx
80101ad3:	8b 45 08             	mov    0x8(%ebp),%eax
80101ad6:	66 89 50 10          	mov    %dx,0x10(%eax)
    ip->major = dip->major;
80101ada:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101add:	0f b7 50 02          	movzwl 0x2(%eax),%edx
80101ae1:	8b 45 08             	mov    0x8(%ebp),%eax
80101ae4:	66 89 50 12          	mov    %dx,0x12(%eax)
    ip->minor = dip->minor;
80101ae8:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101aeb:	0f b7 50 04          	movzwl 0x4(%eax),%edx
80101aef:	8b 45 08             	mov    0x8(%ebp),%eax
80101af2:	66 89 50 14          	mov    %dx,0x14(%eax)
    ip->nlink = dip->nlink;
80101af6:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101af9:	0f b7 50 06          	movzwl 0x6(%eax),%edx
80101afd:	8b 45 08             	mov    0x8(%ebp),%eax
80101b00:	66 89 50 16          	mov    %dx,0x16(%eax)
    ip->size = dip->size;
80101b04:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101b07:	8b 50 08             	mov    0x8(%eax),%edx
80101b0a:	8b 45 08             	mov    0x8(%ebp),%eax
80101b0d:	89 50 18             	mov    %edx,0x18(%eax)
    memmove(ip->addrs, dip->addrs, sizeof(ip->addrs));
80101b10:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101b13:	8d 50 0c             	lea    0xc(%eax),%edx
80101b16:	8b 45 08             	mov    0x8(%ebp),%eax
80101b19:	83 c0 1c             	add    $0x1c,%eax
80101b1c:	83 ec 04             	sub    $0x4,%esp
80101b1f:	6a 34                	push   $0x34
80101b21:	52                   	push   %edx
80101b22:	50                   	push   %eax
80101b23:	e8 2f 52 00 00       	call   80106d57 <memmove>
80101b28:	83 c4 10             	add    $0x10,%esp
    brelse(bp);
80101b2b:	83 ec 0c             	sub    $0xc,%esp
80101b2e:	ff 75 f4             	pushl  -0xc(%ebp)
80101b31:	e8 f8 e6 ff ff       	call   8010022e <brelse>
80101b36:	83 c4 10             	add    $0x10,%esp
    ip->flags |= I_VALID;
80101b39:	8b 45 08             	mov    0x8(%ebp),%eax
80101b3c:	8b 40 0c             	mov    0xc(%eax),%eax
80101b3f:	83 c8 02             	or     $0x2,%eax
80101b42:	89 c2                	mov    %eax,%edx
80101b44:	8b 45 08             	mov    0x8(%ebp),%eax
80101b47:	89 50 0c             	mov    %edx,0xc(%eax)
    if(ip->type == 0)
80101b4a:	8b 45 08             	mov    0x8(%ebp),%eax
80101b4d:	0f b7 40 10          	movzwl 0x10(%eax),%eax
80101b51:	66 85 c0             	test   %ax,%ax
80101b54:	75 0d                	jne    80101b63 <ilock+0x15b>
      panic("ilock: no type");
80101b56:	83 ec 0c             	sub    $0xc,%esp
80101b59:	68 3f a2 10 80       	push   $0x8010a23f
80101b5e:	e8 03 ea ff ff       	call   80100566 <panic>
  }
}
80101b63:	90                   	nop
80101b64:	c9                   	leave  
80101b65:	c3                   	ret    

80101b66 <iunlock>:

// Unlock the given inode.
void
iunlock(struct inode *ip)
{
80101b66:	55                   	push   %ebp
80101b67:	89 e5                	mov    %esp,%ebp
80101b69:	83 ec 08             	sub    $0x8,%esp
  if(ip == 0 || !(ip->flags & I_BUSY) || ip->ref < 1)
80101b6c:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80101b70:	74 17                	je     80101b89 <iunlock+0x23>
80101b72:	8b 45 08             	mov    0x8(%ebp),%eax
80101b75:	8b 40 0c             	mov    0xc(%eax),%eax
80101b78:	83 e0 01             	and    $0x1,%eax
80101b7b:	85 c0                	test   %eax,%eax
80101b7d:	74 0a                	je     80101b89 <iunlock+0x23>
80101b7f:	8b 45 08             	mov    0x8(%ebp),%eax
80101b82:	8b 40 08             	mov    0x8(%eax),%eax
80101b85:	85 c0                	test   %eax,%eax
80101b87:	7f 0d                	jg     80101b96 <iunlock+0x30>
    panic("iunlock");
80101b89:	83 ec 0c             	sub    $0xc,%esp
80101b8c:	68 4e a2 10 80       	push   $0x8010a24e
80101b91:	e8 d0 e9 ff ff       	call   80100566 <panic>

  acquire(&icache.lock);
80101b96:	83 ec 0c             	sub    $0xc,%esp
80101b99:	68 60 32 11 80       	push   $0x80113260
80101b9e:	e8 92 4e 00 00       	call   80106a35 <acquire>
80101ba3:	83 c4 10             	add    $0x10,%esp
  ip->flags &= ~I_BUSY;
80101ba6:	8b 45 08             	mov    0x8(%ebp),%eax
80101ba9:	8b 40 0c             	mov    0xc(%eax),%eax
80101bac:	83 e0 fe             	and    $0xfffffffe,%eax
80101baf:	89 c2                	mov    %eax,%edx
80101bb1:	8b 45 08             	mov    0x8(%ebp),%eax
80101bb4:	89 50 0c             	mov    %edx,0xc(%eax)
  wakeup(ip);
80101bb7:	83 ec 0c             	sub    $0xc,%esp
80101bba:	ff 75 08             	pushl  0x8(%ebp)
80101bbd:	e8 d3 39 00 00       	call   80105595 <wakeup>
80101bc2:	83 c4 10             	add    $0x10,%esp
  release(&icache.lock);
80101bc5:	83 ec 0c             	sub    $0xc,%esp
80101bc8:	68 60 32 11 80       	push   $0x80113260
80101bcd:	e8 ca 4e 00 00       	call   80106a9c <release>
80101bd2:	83 c4 10             	add    $0x10,%esp
}
80101bd5:	90                   	nop
80101bd6:	c9                   	leave  
80101bd7:	c3                   	ret    

80101bd8 <iput>:
// to it, free the inode (and its content) on disk.
// All calls to iput() must be inside a transaction in
// case it has to free the inode.
void
iput(struct inode *ip)
{
80101bd8:	55                   	push   %ebp
80101bd9:	89 e5                	mov    %esp,%ebp
80101bdb:	83 ec 08             	sub    $0x8,%esp
  acquire(&icache.lock);
80101bde:	83 ec 0c             	sub    $0xc,%esp
80101be1:	68 60 32 11 80       	push   $0x80113260
80101be6:	e8 4a 4e 00 00       	call   80106a35 <acquire>
80101beb:	83 c4 10             	add    $0x10,%esp
  if(ip->ref == 1 && (ip->flags & I_VALID) && ip->nlink == 0){
80101bee:	8b 45 08             	mov    0x8(%ebp),%eax
80101bf1:	8b 40 08             	mov    0x8(%eax),%eax
80101bf4:	83 f8 01             	cmp    $0x1,%eax
80101bf7:	0f 85 a9 00 00 00    	jne    80101ca6 <iput+0xce>
80101bfd:	8b 45 08             	mov    0x8(%ebp),%eax
80101c00:	8b 40 0c             	mov    0xc(%eax),%eax
80101c03:	83 e0 02             	and    $0x2,%eax
80101c06:	85 c0                	test   %eax,%eax
80101c08:	0f 84 98 00 00 00    	je     80101ca6 <iput+0xce>
80101c0e:	8b 45 08             	mov    0x8(%ebp),%eax
80101c11:	0f b7 40 16          	movzwl 0x16(%eax),%eax
80101c15:	66 85 c0             	test   %ax,%ax
80101c18:	0f 85 88 00 00 00    	jne    80101ca6 <iput+0xce>
    // inode has no links and no other references: truncate and free.
    if(ip->flags & I_BUSY)
80101c1e:	8b 45 08             	mov    0x8(%ebp),%eax
80101c21:	8b 40 0c             	mov    0xc(%eax),%eax
80101c24:	83 e0 01             	and    $0x1,%eax
80101c27:	85 c0                	test   %eax,%eax
80101c29:	74 0d                	je     80101c38 <iput+0x60>
      panic("iput busy");
80101c2b:	83 ec 0c             	sub    $0xc,%esp
80101c2e:	68 56 a2 10 80       	push   $0x8010a256
80101c33:	e8 2e e9 ff ff       	call   80100566 <panic>
    ip->flags |= I_BUSY;
80101c38:	8b 45 08             	mov    0x8(%ebp),%eax
80101c3b:	8b 40 0c             	mov    0xc(%eax),%eax
80101c3e:	83 c8 01             	or     $0x1,%eax
80101c41:	89 c2                	mov    %eax,%edx
80101c43:	8b 45 08             	mov    0x8(%ebp),%eax
80101c46:	89 50 0c             	mov    %edx,0xc(%eax)
    release(&icache.lock);
80101c49:	83 ec 0c             	sub    $0xc,%esp
80101c4c:	68 60 32 11 80       	push   $0x80113260
80101c51:	e8 46 4e 00 00       	call   80106a9c <release>
80101c56:	83 c4 10             	add    $0x10,%esp
    itrunc(ip);
80101c59:	83 ec 0c             	sub    $0xc,%esp
80101c5c:	ff 75 08             	pushl  0x8(%ebp)
80101c5f:	e8 a8 01 00 00       	call   80101e0c <itrunc>
80101c64:	83 c4 10             	add    $0x10,%esp
    ip->type = 0;
80101c67:	8b 45 08             	mov    0x8(%ebp),%eax
80101c6a:	66 c7 40 10 00 00    	movw   $0x0,0x10(%eax)
    iupdate(ip);
80101c70:	83 ec 0c             	sub    $0xc,%esp
80101c73:	ff 75 08             	pushl  0x8(%ebp)
80101c76:	e8 b3 fb ff ff       	call   8010182e <iupdate>
80101c7b:	83 c4 10             	add    $0x10,%esp
    acquire(&icache.lock);
80101c7e:	83 ec 0c             	sub    $0xc,%esp
80101c81:	68 60 32 11 80       	push   $0x80113260
80101c86:	e8 aa 4d 00 00       	call   80106a35 <acquire>
80101c8b:	83 c4 10             	add    $0x10,%esp
    ip->flags = 0;
80101c8e:	8b 45 08             	mov    0x8(%ebp),%eax
80101c91:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
    wakeup(ip);
80101c98:	83 ec 0c             	sub    $0xc,%esp
80101c9b:	ff 75 08             	pushl  0x8(%ebp)
80101c9e:	e8 f2 38 00 00       	call   80105595 <wakeup>
80101ca3:	83 c4 10             	add    $0x10,%esp
  }
  ip->ref--;
80101ca6:	8b 45 08             	mov    0x8(%ebp),%eax
80101ca9:	8b 40 08             	mov    0x8(%eax),%eax
80101cac:	8d 50 ff             	lea    -0x1(%eax),%edx
80101caf:	8b 45 08             	mov    0x8(%ebp),%eax
80101cb2:	89 50 08             	mov    %edx,0x8(%eax)
  release(&icache.lock);
80101cb5:	83 ec 0c             	sub    $0xc,%esp
80101cb8:	68 60 32 11 80       	push   $0x80113260
80101cbd:	e8 da 4d 00 00       	call   80106a9c <release>
80101cc2:	83 c4 10             	add    $0x10,%esp
}
80101cc5:	90                   	nop
80101cc6:	c9                   	leave  
80101cc7:	c3                   	ret    

80101cc8 <iunlockput>:

// Common idiom: unlock, then put.
void
iunlockput(struct inode *ip)
{
80101cc8:	55                   	push   %ebp
80101cc9:	89 e5                	mov    %esp,%ebp
80101ccb:	83 ec 08             	sub    $0x8,%esp
  iunlock(ip);
80101cce:	83 ec 0c             	sub    $0xc,%esp
80101cd1:	ff 75 08             	pushl  0x8(%ebp)
80101cd4:	e8 8d fe ff ff       	call   80101b66 <iunlock>
80101cd9:	83 c4 10             	add    $0x10,%esp
  iput(ip);
80101cdc:	83 ec 0c             	sub    $0xc,%esp
80101cdf:	ff 75 08             	pushl  0x8(%ebp)
80101ce2:	e8 f1 fe ff ff       	call   80101bd8 <iput>
80101ce7:	83 c4 10             	add    $0x10,%esp
}
80101cea:	90                   	nop
80101ceb:	c9                   	leave  
80101cec:	c3                   	ret    

80101ced <bmap>:

// Return the disk block address of the nth block in inode ip.
// If there is no such block, bmap allocates one.
static uint
bmap(struct inode *ip, uint bn)
{
80101ced:	55                   	push   %ebp
80101cee:	89 e5                	mov    %esp,%ebp
80101cf0:	53                   	push   %ebx
80101cf1:	83 ec 14             	sub    $0x14,%esp
  uint addr, *a;
  struct buf *bp;

  if(bn < NDIRECT){
80101cf4:	83 7d 0c 0b          	cmpl   $0xb,0xc(%ebp)
80101cf8:	77 42                	ja     80101d3c <bmap+0x4f>
    if((addr = ip->addrs[bn]) == 0)
80101cfa:	8b 45 08             	mov    0x8(%ebp),%eax
80101cfd:	8b 55 0c             	mov    0xc(%ebp),%edx
80101d00:	83 c2 04             	add    $0x4,%edx
80101d03:	8b 44 90 0c          	mov    0xc(%eax,%edx,4),%eax
80101d07:	89 45 f4             	mov    %eax,-0xc(%ebp)
80101d0a:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80101d0e:	75 24                	jne    80101d34 <bmap+0x47>
      ip->addrs[bn] = addr = balloc(ip->dev);
80101d10:	8b 45 08             	mov    0x8(%ebp),%eax
80101d13:	8b 00                	mov    (%eax),%eax
80101d15:	83 ec 0c             	sub    $0xc,%esp
80101d18:	50                   	push   %eax
80101d19:	e8 9a f7 ff ff       	call   801014b8 <balloc>
80101d1e:	83 c4 10             	add    $0x10,%esp
80101d21:	89 45 f4             	mov    %eax,-0xc(%ebp)
80101d24:	8b 45 08             	mov    0x8(%ebp),%eax
80101d27:	8b 55 0c             	mov    0xc(%ebp),%edx
80101d2a:	8d 4a 04             	lea    0x4(%edx),%ecx
80101d2d:	8b 55 f4             	mov    -0xc(%ebp),%edx
80101d30:	89 54 88 0c          	mov    %edx,0xc(%eax,%ecx,4)
    return addr;
80101d34:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101d37:	e9 cb 00 00 00       	jmp    80101e07 <bmap+0x11a>
  }
  bn -= NDIRECT;
80101d3c:	83 6d 0c 0c          	subl   $0xc,0xc(%ebp)

  if(bn < NINDIRECT){
80101d40:	83 7d 0c 7f          	cmpl   $0x7f,0xc(%ebp)
80101d44:	0f 87 b0 00 00 00    	ja     80101dfa <bmap+0x10d>
    // Load indirect block, allocating if necessary.
    if((addr = ip->addrs[NDIRECT]) == 0)
80101d4a:	8b 45 08             	mov    0x8(%ebp),%eax
80101d4d:	8b 40 4c             	mov    0x4c(%eax),%eax
80101d50:	89 45 f4             	mov    %eax,-0xc(%ebp)
80101d53:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80101d57:	75 1d                	jne    80101d76 <bmap+0x89>
      ip->addrs[NDIRECT] = addr = balloc(ip->dev);
80101d59:	8b 45 08             	mov    0x8(%ebp),%eax
80101d5c:	8b 00                	mov    (%eax),%eax
80101d5e:	83 ec 0c             	sub    $0xc,%esp
80101d61:	50                   	push   %eax
80101d62:	e8 51 f7 ff ff       	call   801014b8 <balloc>
80101d67:	83 c4 10             	add    $0x10,%esp
80101d6a:	89 45 f4             	mov    %eax,-0xc(%ebp)
80101d6d:	8b 45 08             	mov    0x8(%ebp),%eax
80101d70:	8b 55 f4             	mov    -0xc(%ebp),%edx
80101d73:	89 50 4c             	mov    %edx,0x4c(%eax)
    bp = bread(ip->dev, addr);
80101d76:	8b 45 08             	mov    0x8(%ebp),%eax
80101d79:	8b 00                	mov    (%eax),%eax
80101d7b:	83 ec 08             	sub    $0x8,%esp
80101d7e:	ff 75 f4             	pushl  -0xc(%ebp)
80101d81:	50                   	push   %eax
80101d82:	e8 2f e4 ff ff       	call   801001b6 <bread>
80101d87:	83 c4 10             	add    $0x10,%esp
80101d8a:	89 45 f0             	mov    %eax,-0x10(%ebp)
    a = (uint*)bp->data;
80101d8d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101d90:	83 c0 18             	add    $0x18,%eax
80101d93:	89 45 ec             	mov    %eax,-0x14(%ebp)
    if((addr = a[bn]) == 0){
80101d96:	8b 45 0c             	mov    0xc(%ebp),%eax
80101d99:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
80101da0:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101da3:	01 d0                	add    %edx,%eax
80101da5:	8b 00                	mov    (%eax),%eax
80101da7:	89 45 f4             	mov    %eax,-0xc(%ebp)
80101daa:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80101dae:	75 37                	jne    80101de7 <bmap+0xfa>
      a[bn] = addr = balloc(ip->dev);
80101db0:	8b 45 0c             	mov    0xc(%ebp),%eax
80101db3:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
80101dba:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101dbd:	8d 1c 02             	lea    (%edx,%eax,1),%ebx
80101dc0:	8b 45 08             	mov    0x8(%ebp),%eax
80101dc3:	8b 00                	mov    (%eax),%eax
80101dc5:	83 ec 0c             	sub    $0xc,%esp
80101dc8:	50                   	push   %eax
80101dc9:	e8 ea f6 ff ff       	call   801014b8 <balloc>
80101dce:	83 c4 10             	add    $0x10,%esp
80101dd1:	89 45 f4             	mov    %eax,-0xc(%ebp)
80101dd4:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101dd7:	89 03                	mov    %eax,(%ebx)
      log_write(bp);
80101dd9:	83 ec 0c             	sub    $0xc,%esp
80101ddc:	ff 75 f0             	pushl  -0x10(%ebp)
80101ddf:	e8 3f 1a 00 00       	call   80103823 <log_write>
80101de4:	83 c4 10             	add    $0x10,%esp
    }
    brelse(bp);
80101de7:	83 ec 0c             	sub    $0xc,%esp
80101dea:	ff 75 f0             	pushl  -0x10(%ebp)
80101ded:	e8 3c e4 ff ff       	call   8010022e <brelse>
80101df2:	83 c4 10             	add    $0x10,%esp
    return addr;
80101df5:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101df8:	eb 0d                	jmp    80101e07 <bmap+0x11a>
  }

  panic("bmap: out of range");
80101dfa:	83 ec 0c             	sub    $0xc,%esp
80101dfd:	68 60 a2 10 80       	push   $0x8010a260
80101e02:	e8 5f e7 ff ff       	call   80100566 <panic>
}
80101e07:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80101e0a:	c9                   	leave  
80101e0b:	c3                   	ret    

80101e0c <itrunc>:
// to it (no directory entries referring to it)
// and has no in-memory reference to it (is
// not an open file or current directory).
static void
itrunc(struct inode *ip)
{
80101e0c:	55                   	push   %ebp
80101e0d:	89 e5                	mov    %esp,%ebp
80101e0f:	83 ec 18             	sub    $0x18,%esp
  int i, j;
  struct buf *bp;
  uint *a;

  for(i = 0; i < NDIRECT; i++){
80101e12:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80101e19:	eb 45                	jmp    80101e60 <itrunc+0x54>
    if(ip->addrs[i]){
80101e1b:	8b 45 08             	mov    0x8(%ebp),%eax
80101e1e:	8b 55 f4             	mov    -0xc(%ebp),%edx
80101e21:	83 c2 04             	add    $0x4,%edx
80101e24:	8b 44 90 0c          	mov    0xc(%eax,%edx,4),%eax
80101e28:	85 c0                	test   %eax,%eax
80101e2a:	74 30                	je     80101e5c <itrunc+0x50>
      bfree(ip->dev, ip->addrs[i]);
80101e2c:	8b 45 08             	mov    0x8(%ebp),%eax
80101e2f:	8b 55 f4             	mov    -0xc(%ebp),%edx
80101e32:	83 c2 04             	add    $0x4,%edx
80101e35:	8b 44 90 0c          	mov    0xc(%eax,%edx,4),%eax
80101e39:	8b 55 08             	mov    0x8(%ebp),%edx
80101e3c:	8b 12                	mov    (%edx),%edx
80101e3e:	83 ec 08             	sub    $0x8,%esp
80101e41:	50                   	push   %eax
80101e42:	52                   	push   %edx
80101e43:	e8 bc f7 ff ff       	call   80101604 <bfree>
80101e48:	83 c4 10             	add    $0x10,%esp
      ip->addrs[i] = 0;
80101e4b:	8b 45 08             	mov    0x8(%ebp),%eax
80101e4e:	8b 55 f4             	mov    -0xc(%ebp),%edx
80101e51:	83 c2 04             	add    $0x4,%edx
80101e54:	c7 44 90 0c 00 00 00 	movl   $0x0,0xc(%eax,%edx,4)
80101e5b:	00 
{
  int i, j;
  struct buf *bp;
  uint *a;

  for(i = 0; i < NDIRECT; i++){
80101e5c:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80101e60:	83 7d f4 0b          	cmpl   $0xb,-0xc(%ebp)
80101e64:	7e b5                	jle    80101e1b <itrunc+0xf>
      bfree(ip->dev, ip->addrs[i]);
      ip->addrs[i] = 0;
    }
  }
  
  if(ip->addrs[NDIRECT]){
80101e66:	8b 45 08             	mov    0x8(%ebp),%eax
80101e69:	8b 40 4c             	mov    0x4c(%eax),%eax
80101e6c:	85 c0                	test   %eax,%eax
80101e6e:	0f 84 a1 00 00 00    	je     80101f15 <itrunc+0x109>
    bp = bread(ip->dev, ip->addrs[NDIRECT]);
80101e74:	8b 45 08             	mov    0x8(%ebp),%eax
80101e77:	8b 50 4c             	mov    0x4c(%eax),%edx
80101e7a:	8b 45 08             	mov    0x8(%ebp),%eax
80101e7d:	8b 00                	mov    (%eax),%eax
80101e7f:	83 ec 08             	sub    $0x8,%esp
80101e82:	52                   	push   %edx
80101e83:	50                   	push   %eax
80101e84:	e8 2d e3 ff ff       	call   801001b6 <bread>
80101e89:	83 c4 10             	add    $0x10,%esp
80101e8c:	89 45 ec             	mov    %eax,-0x14(%ebp)
    a = (uint*)bp->data;
80101e8f:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101e92:	83 c0 18             	add    $0x18,%eax
80101e95:	89 45 e8             	mov    %eax,-0x18(%ebp)
    for(j = 0; j < NINDIRECT; j++){
80101e98:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
80101e9f:	eb 3c                	jmp    80101edd <itrunc+0xd1>
      if(a[j])
80101ea1:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101ea4:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
80101eab:	8b 45 e8             	mov    -0x18(%ebp),%eax
80101eae:	01 d0                	add    %edx,%eax
80101eb0:	8b 00                	mov    (%eax),%eax
80101eb2:	85 c0                	test   %eax,%eax
80101eb4:	74 23                	je     80101ed9 <itrunc+0xcd>
        bfree(ip->dev, a[j]);
80101eb6:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101eb9:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
80101ec0:	8b 45 e8             	mov    -0x18(%ebp),%eax
80101ec3:	01 d0                	add    %edx,%eax
80101ec5:	8b 00                	mov    (%eax),%eax
80101ec7:	8b 55 08             	mov    0x8(%ebp),%edx
80101eca:	8b 12                	mov    (%edx),%edx
80101ecc:	83 ec 08             	sub    $0x8,%esp
80101ecf:	50                   	push   %eax
80101ed0:	52                   	push   %edx
80101ed1:	e8 2e f7 ff ff       	call   80101604 <bfree>
80101ed6:	83 c4 10             	add    $0x10,%esp
  }
  
  if(ip->addrs[NDIRECT]){
    bp = bread(ip->dev, ip->addrs[NDIRECT]);
    a = (uint*)bp->data;
    for(j = 0; j < NINDIRECT; j++){
80101ed9:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
80101edd:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101ee0:	83 f8 7f             	cmp    $0x7f,%eax
80101ee3:	76 bc                	jbe    80101ea1 <itrunc+0x95>
      if(a[j])
        bfree(ip->dev, a[j]);
    }
    brelse(bp);
80101ee5:	83 ec 0c             	sub    $0xc,%esp
80101ee8:	ff 75 ec             	pushl  -0x14(%ebp)
80101eeb:	e8 3e e3 ff ff       	call   8010022e <brelse>
80101ef0:	83 c4 10             	add    $0x10,%esp
    bfree(ip->dev, ip->addrs[NDIRECT]);
80101ef3:	8b 45 08             	mov    0x8(%ebp),%eax
80101ef6:	8b 40 4c             	mov    0x4c(%eax),%eax
80101ef9:	8b 55 08             	mov    0x8(%ebp),%edx
80101efc:	8b 12                	mov    (%edx),%edx
80101efe:	83 ec 08             	sub    $0x8,%esp
80101f01:	50                   	push   %eax
80101f02:	52                   	push   %edx
80101f03:	e8 fc f6 ff ff       	call   80101604 <bfree>
80101f08:	83 c4 10             	add    $0x10,%esp
    ip->addrs[NDIRECT] = 0;
80101f0b:	8b 45 08             	mov    0x8(%ebp),%eax
80101f0e:	c7 40 4c 00 00 00 00 	movl   $0x0,0x4c(%eax)
  }

  ip->size = 0;
80101f15:	8b 45 08             	mov    0x8(%ebp),%eax
80101f18:	c7 40 18 00 00 00 00 	movl   $0x0,0x18(%eax)
  iupdate(ip);
80101f1f:	83 ec 0c             	sub    $0xc,%esp
80101f22:	ff 75 08             	pushl  0x8(%ebp)
80101f25:	e8 04 f9 ff ff       	call   8010182e <iupdate>
80101f2a:	83 c4 10             	add    $0x10,%esp
}
80101f2d:	90                   	nop
80101f2e:	c9                   	leave  
80101f2f:	c3                   	ret    

80101f30 <stati>:

// Copy stat information from inode.
void
stati(struct inode *ip, struct stat *st)
{
80101f30:	55                   	push   %ebp
80101f31:	89 e5                	mov    %esp,%ebp
  st->dev = ip->dev;
80101f33:	8b 45 08             	mov    0x8(%ebp),%eax
80101f36:	8b 00                	mov    (%eax),%eax
80101f38:	89 c2                	mov    %eax,%edx
80101f3a:	8b 45 0c             	mov    0xc(%ebp),%eax
80101f3d:	89 50 04             	mov    %edx,0x4(%eax)
  st->ino = ip->inum;
80101f40:	8b 45 08             	mov    0x8(%ebp),%eax
80101f43:	8b 50 04             	mov    0x4(%eax),%edx
80101f46:	8b 45 0c             	mov    0xc(%ebp),%eax
80101f49:	89 50 08             	mov    %edx,0x8(%eax)
  st->type = ip->type;
80101f4c:	8b 45 08             	mov    0x8(%ebp),%eax
80101f4f:	0f b7 50 10          	movzwl 0x10(%eax),%edx
80101f53:	8b 45 0c             	mov    0xc(%ebp),%eax
80101f56:	66 89 10             	mov    %dx,(%eax)
  st->nlink = ip->nlink;
80101f59:	8b 45 08             	mov    0x8(%ebp),%eax
80101f5c:	0f b7 50 16          	movzwl 0x16(%eax),%edx
80101f60:	8b 45 0c             	mov    0xc(%ebp),%eax
80101f63:	66 89 50 0c          	mov    %dx,0xc(%eax)
  st->size = ip->size;
80101f67:	8b 45 08             	mov    0x8(%ebp),%eax
80101f6a:	8b 50 18             	mov    0x18(%eax),%edx
80101f6d:	8b 45 0c             	mov    0xc(%ebp),%eax
80101f70:	89 50 10             	mov    %edx,0x10(%eax)
}
80101f73:	90                   	nop
80101f74:	5d                   	pop    %ebp
80101f75:	c3                   	ret    

80101f76 <readi>:

// Read data from inode.
int
readi(struct inode *ip, char *dst, uint off, uint n)
{
80101f76:	55                   	push   %ebp
80101f77:	89 e5                	mov    %esp,%ebp
80101f79:	83 ec 18             	sub    $0x18,%esp
  uint tot, m;
  struct buf *bp;

  if(ip->type == T_DEV){
80101f7c:	8b 45 08             	mov    0x8(%ebp),%eax
80101f7f:	0f b7 40 10          	movzwl 0x10(%eax),%eax
80101f83:	66 83 f8 03          	cmp    $0x3,%ax
80101f87:	75 5c                	jne    80101fe5 <readi+0x6f>
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].read)
80101f89:	8b 45 08             	mov    0x8(%ebp),%eax
80101f8c:	0f b7 40 12          	movzwl 0x12(%eax),%eax
80101f90:	66 85 c0             	test   %ax,%ax
80101f93:	78 20                	js     80101fb5 <readi+0x3f>
80101f95:	8b 45 08             	mov    0x8(%ebp),%eax
80101f98:	0f b7 40 12          	movzwl 0x12(%eax),%eax
80101f9c:	66 83 f8 09          	cmp    $0x9,%ax
80101fa0:	7f 13                	jg     80101fb5 <readi+0x3f>
80101fa2:	8b 45 08             	mov    0x8(%ebp),%eax
80101fa5:	0f b7 40 12          	movzwl 0x12(%eax),%eax
80101fa9:	98                   	cwtl   
80101faa:	8b 04 c5 e0 31 11 80 	mov    -0x7feece20(,%eax,8),%eax
80101fb1:	85 c0                	test   %eax,%eax
80101fb3:	75 0a                	jne    80101fbf <readi+0x49>
      return -1;
80101fb5:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80101fba:	e9 0c 01 00 00       	jmp    801020cb <readi+0x155>
    return devsw[ip->major].read(ip, dst, n);
80101fbf:	8b 45 08             	mov    0x8(%ebp),%eax
80101fc2:	0f b7 40 12          	movzwl 0x12(%eax),%eax
80101fc6:	98                   	cwtl   
80101fc7:	8b 04 c5 e0 31 11 80 	mov    -0x7feece20(,%eax,8),%eax
80101fce:	8b 55 14             	mov    0x14(%ebp),%edx
80101fd1:	83 ec 04             	sub    $0x4,%esp
80101fd4:	52                   	push   %edx
80101fd5:	ff 75 0c             	pushl  0xc(%ebp)
80101fd8:	ff 75 08             	pushl  0x8(%ebp)
80101fdb:	ff d0                	call   *%eax
80101fdd:	83 c4 10             	add    $0x10,%esp
80101fe0:	e9 e6 00 00 00       	jmp    801020cb <readi+0x155>
  }

  if(off > ip->size || off + n < off)
80101fe5:	8b 45 08             	mov    0x8(%ebp),%eax
80101fe8:	8b 40 18             	mov    0x18(%eax),%eax
80101feb:	3b 45 10             	cmp    0x10(%ebp),%eax
80101fee:	72 0d                	jb     80101ffd <readi+0x87>
80101ff0:	8b 55 10             	mov    0x10(%ebp),%edx
80101ff3:	8b 45 14             	mov    0x14(%ebp),%eax
80101ff6:	01 d0                	add    %edx,%eax
80101ff8:	3b 45 10             	cmp    0x10(%ebp),%eax
80101ffb:	73 0a                	jae    80102007 <readi+0x91>
    return -1;
80101ffd:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80102002:	e9 c4 00 00 00       	jmp    801020cb <readi+0x155>
  if(off + n > ip->size)
80102007:	8b 55 10             	mov    0x10(%ebp),%edx
8010200a:	8b 45 14             	mov    0x14(%ebp),%eax
8010200d:	01 c2                	add    %eax,%edx
8010200f:	8b 45 08             	mov    0x8(%ebp),%eax
80102012:	8b 40 18             	mov    0x18(%eax),%eax
80102015:	39 c2                	cmp    %eax,%edx
80102017:	76 0c                	jbe    80102025 <readi+0xaf>
    n = ip->size - off;
80102019:	8b 45 08             	mov    0x8(%ebp),%eax
8010201c:	8b 40 18             	mov    0x18(%eax),%eax
8010201f:	2b 45 10             	sub    0x10(%ebp),%eax
80102022:	89 45 14             	mov    %eax,0x14(%ebp)

  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
80102025:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
8010202c:	e9 8b 00 00 00       	jmp    801020bc <readi+0x146>
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
80102031:	8b 45 10             	mov    0x10(%ebp),%eax
80102034:	c1 e8 09             	shr    $0x9,%eax
80102037:	83 ec 08             	sub    $0x8,%esp
8010203a:	50                   	push   %eax
8010203b:	ff 75 08             	pushl  0x8(%ebp)
8010203e:	e8 aa fc ff ff       	call   80101ced <bmap>
80102043:	83 c4 10             	add    $0x10,%esp
80102046:	89 c2                	mov    %eax,%edx
80102048:	8b 45 08             	mov    0x8(%ebp),%eax
8010204b:	8b 00                	mov    (%eax),%eax
8010204d:	83 ec 08             	sub    $0x8,%esp
80102050:	52                   	push   %edx
80102051:	50                   	push   %eax
80102052:	e8 5f e1 ff ff       	call   801001b6 <bread>
80102057:	83 c4 10             	add    $0x10,%esp
8010205a:	89 45 f0             	mov    %eax,-0x10(%ebp)
    m = min(n - tot, BSIZE - off%BSIZE);
8010205d:	8b 45 10             	mov    0x10(%ebp),%eax
80102060:	25 ff 01 00 00       	and    $0x1ff,%eax
80102065:	ba 00 02 00 00       	mov    $0x200,%edx
8010206a:	29 c2                	sub    %eax,%edx
8010206c:	8b 45 14             	mov    0x14(%ebp),%eax
8010206f:	2b 45 f4             	sub    -0xc(%ebp),%eax
80102072:	39 c2                	cmp    %eax,%edx
80102074:	0f 46 c2             	cmovbe %edx,%eax
80102077:	89 45 ec             	mov    %eax,-0x14(%ebp)
    memmove(dst, bp->data + off%BSIZE, m);
8010207a:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010207d:	8d 50 18             	lea    0x18(%eax),%edx
80102080:	8b 45 10             	mov    0x10(%ebp),%eax
80102083:	25 ff 01 00 00       	and    $0x1ff,%eax
80102088:	01 d0                	add    %edx,%eax
8010208a:	83 ec 04             	sub    $0x4,%esp
8010208d:	ff 75 ec             	pushl  -0x14(%ebp)
80102090:	50                   	push   %eax
80102091:	ff 75 0c             	pushl  0xc(%ebp)
80102094:	e8 be 4c 00 00       	call   80106d57 <memmove>
80102099:	83 c4 10             	add    $0x10,%esp
    brelse(bp);
8010209c:	83 ec 0c             	sub    $0xc,%esp
8010209f:	ff 75 f0             	pushl  -0x10(%ebp)
801020a2:	e8 87 e1 ff ff       	call   8010022e <brelse>
801020a7:	83 c4 10             	add    $0x10,%esp
  if(off > ip->size || off + n < off)
    return -1;
  if(off + n > ip->size)
    n = ip->size - off;

  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
801020aa:	8b 45 ec             	mov    -0x14(%ebp),%eax
801020ad:	01 45 f4             	add    %eax,-0xc(%ebp)
801020b0:	8b 45 ec             	mov    -0x14(%ebp),%eax
801020b3:	01 45 10             	add    %eax,0x10(%ebp)
801020b6:	8b 45 ec             	mov    -0x14(%ebp),%eax
801020b9:	01 45 0c             	add    %eax,0xc(%ebp)
801020bc:	8b 45 f4             	mov    -0xc(%ebp),%eax
801020bf:	3b 45 14             	cmp    0x14(%ebp),%eax
801020c2:	0f 82 69 ff ff ff    	jb     80102031 <readi+0xbb>
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
    m = min(n - tot, BSIZE - off%BSIZE);
    memmove(dst, bp->data + off%BSIZE, m);
    brelse(bp);
  }
  return n;
801020c8:	8b 45 14             	mov    0x14(%ebp),%eax
}
801020cb:	c9                   	leave  
801020cc:	c3                   	ret    

801020cd <writei>:

// Write data to inode.
int
writei(struct inode *ip, char *src, uint off, uint n)
{
801020cd:	55                   	push   %ebp
801020ce:	89 e5                	mov    %esp,%ebp
801020d0:	83 ec 18             	sub    $0x18,%esp
  uint tot, m;
  struct buf *bp;

  if(ip->type == T_DEV){
801020d3:	8b 45 08             	mov    0x8(%ebp),%eax
801020d6:	0f b7 40 10          	movzwl 0x10(%eax),%eax
801020da:	66 83 f8 03          	cmp    $0x3,%ax
801020de:	75 5c                	jne    8010213c <writei+0x6f>
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].write)
801020e0:	8b 45 08             	mov    0x8(%ebp),%eax
801020e3:	0f b7 40 12          	movzwl 0x12(%eax),%eax
801020e7:	66 85 c0             	test   %ax,%ax
801020ea:	78 20                	js     8010210c <writei+0x3f>
801020ec:	8b 45 08             	mov    0x8(%ebp),%eax
801020ef:	0f b7 40 12          	movzwl 0x12(%eax),%eax
801020f3:	66 83 f8 09          	cmp    $0x9,%ax
801020f7:	7f 13                	jg     8010210c <writei+0x3f>
801020f9:	8b 45 08             	mov    0x8(%ebp),%eax
801020fc:	0f b7 40 12          	movzwl 0x12(%eax),%eax
80102100:	98                   	cwtl   
80102101:	8b 04 c5 e4 31 11 80 	mov    -0x7feece1c(,%eax,8),%eax
80102108:	85 c0                	test   %eax,%eax
8010210a:	75 0a                	jne    80102116 <writei+0x49>
      return -1;
8010210c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80102111:	e9 3d 01 00 00       	jmp    80102253 <writei+0x186>
    return devsw[ip->major].write(ip, src, n);
80102116:	8b 45 08             	mov    0x8(%ebp),%eax
80102119:	0f b7 40 12          	movzwl 0x12(%eax),%eax
8010211d:	98                   	cwtl   
8010211e:	8b 04 c5 e4 31 11 80 	mov    -0x7feece1c(,%eax,8),%eax
80102125:	8b 55 14             	mov    0x14(%ebp),%edx
80102128:	83 ec 04             	sub    $0x4,%esp
8010212b:	52                   	push   %edx
8010212c:	ff 75 0c             	pushl  0xc(%ebp)
8010212f:	ff 75 08             	pushl  0x8(%ebp)
80102132:	ff d0                	call   *%eax
80102134:	83 c4 10             	add    $0x10,%esp
80102137:	e9 17 01 00 00       	jmp    80102253 <writei+0x186>
  }

  if(off > ip->size || off + n < off)
8010213c:	8b 45 08             	mov    0x8(%ebp),%eax
8010213f:	8b 40 18             	mov    0x18(%eax),%eax
80102142:	3b 45 10             	cmp    0x10(%ebp),%eax
80102145:	72 0d                	jb     80102154 <writei+0x87>
80102147:	8b 55 10             	mov    0x10(%ebp),%edx
8010214a:	8b 45 14             	mov    0x14(%ebp),%eax
8010214d:	01 d0                	add    %edx,%eax
8010214f:	3b 45 10             	cmp    0x10(%ebp),%eax
80102152:	73 0a                	jae    8010215e <writei+0x91>
    return -1;
80102154:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80102159:	e9 f5 00 00 00       	jmp    80102253 <writei+0x186>
  if(off + n > MAXFILE*BSIZE)
8010215e:	8b 55 10             	mov    0x10(%ebp),%edx
80102161:	8b 45 14             	mov    0x14(%ebp),%eax
80102164:	01 d0                	add    %edx,%eax
80102166:	3d 00 18 01 00       	cmp    $0x11800,%eax
8010216b:	76 0a                	jbe    80102177 <writei+0xaa>
    return -1;
8010216d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80102172:	e9 dc 00 00 00       	jmp    80102253 <writei+0x186>

  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
80102177:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
8010217e:	e9 99 00 00 00       	jmp    8010221c <writei+0x14f>
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
80102183:	8b 45 10             	mov    0x10(%ebp),%eax
80102186:	c1 e8 09             	shr    $0x9,%eax
80102189:	83 ec 08             	sub    $0x8,%esp
8010218c:	50                   	push   %eax
8010218d:	ff 75 08             	pushl  0x8(%ebp)
80102190:	e8 58 fb ff ff       	call   80101ced <bmap>
80102195:	83 c4 10             	add    $0x10,%esp
80102198:	89 c2                	mov    %eax,%edx
8010219a:	8b 45 08             	mov    0x8(%ebp),%eax
8010219d:	8b 00                	mov    (%eax),%eax
8010219f:	83 ec 08             	sub    $0x8,%esp
801021a2:	52                   	push   %edx
801021a3:	50                   	push   %eax
801021a4:	e8 0d e0 ff ff       	call   801001b6 <bread>
801021a9:	83 c4 10             	add    $0x10,%esp
801021ac:	89 45 f0             	mov    %eax,-0x10(%ebp)
    m = min(n - tot, BSIZE - off%BSIZE);
801021af:	8b 45 10             	mov    0x10(%ebp),%eax
801021b2:	25 ff 01 00 00       	and    $0x1ff,%eax
801021b7:	ba 00 02 00 00       	mov    $0x200,%edx
801021bc:	29 c2                	sub    %eax,%edx
801021be:	8b 45 14             	mov    0x14(%ebp),%eax
801021c1:	2b 45 f4             	sub    -0xc(%ebp),%eax
801021c4:	39 c2                	cmp    %eax,%edx
801021c6:	0f 46 c2             	cmovbe %edx,%eax
801021c9:	89 45 ec             	mov    %eax,-0x14(%ebp)
    memmove(bp->data + off%BSIZE, src, m);
801021cc:	8b 45 f0             	mov    -0x10(%ebp),%eax
801021cf:	8d 50 18             	lea    0x18(%eax),%edx
801021d2:	8b 45 10             	mov    0x10(%ebp),%eax
801021d5:	25 ff 01 00 00       	and    $0x1ff,%eax
801021da:	01 d0                	add    %edx,%eax
801021dc:	83 ec 04             	sub    $0x4,%esp
801021df:	ff 75 ec             	pushl  -0x14(%ebp)
801021e2:	ff 75 0c             	pushl  0xc(%ebp)
801021e5:	50                   	push   %eax
801021e6:	e8 6c 4b 00 00       	call   80106d57 <memmove>
801021eb:	83 c4 10             	add    $0x10,%esp
    log_write(bp);
801021ee:	83 ec 0c             	sub    $0xc,%esp
801021f1:	ff 75 f0             	pushl  -0x10(%ebp)
801021f4:	e8 2a 16 00 00       	call   80103823 <log_write>
801021f9:	83 c4 10             	add    $0x10,%esp
    brelse(bp);
801021fc:	83 ec 0c             	sub    $0xc,%esp
801021ff:	ff 75 f0             	pushl  -0x10(%ebp)
80102202:	e8 27 e0 ff ff       	call   8010022e <brelse>
80102207:	83 c4 10             	add    $0x10,%esp
  if(off > ip->size || off + n < off)
    return -1;
  if(off + n > MAXFILE*BSIZE)
    return -1;

  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
8010220a:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010220d:	01 45 f4             	add    %eax,-0xc(%ebp)
80102210:	8b 45 ec             	mov    -0x14(%ebp),%eax
80102213:	01 45 10             	add    %eax,0x10(%ebp)
80102216:	8b 45 ec             	mov    -0x14(%ebp),%eax
80102219:	01 45 0c             	add    %eax,0xc(%ebp)
8010221c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010221f:	3b 45 14             	cmp    0x14(%ebp),%eax
80102222:	0f 82 5b ff ff ff    	jb     80102183 <writei+0xb6>
    memmove(bp->data + off%BSIZE, src, m);
    log_write(bp);
    brelse(bp);
  }

  if(n > 0 && off > ip->size){
80102228:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
8010222c:	74 22                	je     80102250 <writei+0x183>
8010222e:	8b 45 08             	mov    0x8(%ebp),%eax
80102231:	8b 40 18             	mov    0x18(%eax),%eax
80102234:	3b 45 10             	cmp    0x10(%ebp),%eax
80102237:	73 17                	jae    80102250 <writei+0x183>
    ip->size = off;
80102239:	8b 45 08             	mov    0x8(%ebp),%eax
8010223c:	8b 55 10             	mov    0x10(%ebp),%edx
8010223f:	89 50 18             	mov    %edx,0x18(%eax)
    iupdate(ip);
80102242:	83 ec 0c             	sub    $0xc,%esp
80102245:	ff 75 08             	pushl  0x8(%ebp)
80102248:	e8 e1 f5 ff ff       	call   8010182e <iupdate>
8010224d:	83 c4 10             	add    $0x10,%esp
  }
  return n;
80102250:	8b 45 14             	mov    0x14(%ebp),%eax
}
80102253:	c9                   	leave  
80102254:	c3                   	ret    

80102255 <namecmp>:

// Directories

int
namecmp(const char *s, const char *t)
{
80102255:	55                   	push   %ebp
80102256:	89 e5                	mov    %esp,%ebp
80102258:	83 ec 08             	sub    $0x8,%esp
  return strncmp(s, t, DIRSIZ);
8010225b:	83 ec 04             	sub    $0x4,%esp
8010225e:	6a 0e                	push   $0xe
80102260:	ff 75 0c             	pushl  0xc(%ebp)
80102263:	ff 75 08             	pushl  0x8(%ebp)
80102266:	e8 82 4b 00 00       	call   80106ded <strncmp>
8010226b:	83 c4 10             	add    $0x10,%esp
}
8010226e:	c9                   	leave  
8010226f:	c3                   	ret    

80102270 <dirlookup>:

// Look for a directory entry in a directory.
// If found, set *poff to byte offset of entry.
struct inode*
dirlookup(struct inode *dp, char *name, uint *poff)
{
80102270:	55                   	push   %ebp
80102271:	89 e5                	mov    %esp,%ebp
80102273:	83 ec 28             	sub    $0x28,%esp
  uint off, inum;
  struct dirent de;

  if(dp->type != T_DIR)
80102276:	8b 45 08             	mov    0x8(%ebp),%eax
80102279:	0f b7 40 10          	movzwl 0x10(%eax),%eax
8010227d:	66 83 f8 01          	cmp    $0x1,%ax
80102281:	74 0d                	je     80102290 <dirlookup+0x20>
    panic("dirlookup not DIR");
80102283:	83 ec 0c             	sub    $0xc,%esp
80102286:	68 73 a2 10 80       	push   $0x8010a273
8010228b:	e8 d6 e2 ff ff       	call   80100566 <panic>

  for(off = 0; off < dp->size; off += sizeof(de)){
80102290:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80102297:	eb 7b                	jmp    80102314 <dirlookup+0xa4>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80102299:	6a 10                	push   $0x10
8010229b:	ff 75 f4             	pushl  -0xc(%ebp)
8010229e:	8d 45 e0             	lea    -0x20(%ebp),%eax
801022a1:	50                   	push   %eax
801022a2:	ff 75 08             	pushl  0x8(%ebp)
801022a5:	e8 cc fc ff ff       	call   80101f76 <readi>
801022aa:	83 c4 10             	add    $0x10,%esp
801022ad:	83 f8 10             	cmp    $0x10,%eax
801022b0:	74 0d                	je     801022bf <dirlookup+0x4f>
      panic("dirlink read");
801022b2:	83 ec 0c             	sub    $0xc,%esp
801022b5:	68 85 a2 10 80       	push   $0x8010a285
801022ba:	e8 a7 e2 ff ff       	call   80100566 <panic>
    if(de.inum == 0)
801022bf:	0f b7 45 e0          	movzwl -0x20(%ebp),%eax
801022c3:	66 85 c0             	test   %ax,%ax
801022c6:	74 47                	je     8010230f <dirlookup+0x9f>
      continue;
    if(namecmp(name, de.name) == 0){
801022c8:	83 ec 08             	sub    $0x8,%esp
801022cb:	8d 45 e0             	lea    -0x20(%ebp),%eax
801022ce:	83 c0 02             	add    $0x2,%eax
801022d1:	50                   	push   %eax
801022d2:	ff 75 0c             	pushl  0xc(%ebp)
801022d5:	e8 7b ff ff ff       	call   80102255 <namecmp>
801022da:	83 c4 10             	add    $0x10,%esp
801022dd:	85 c0                	test   %eax,%eax
801022df:	75 2f                	jne    80102310 <dirlookup+0xa0>
      // entry matches path element
      if(poff)
801022e1:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
801022e5:	74 08                	je     801022ef <dirlookup+0x7f>
        *poff = off;
801022e7:	8b 45 10             	mov    0x10(%ebp),%eax
801022ea:	8b 55 f4             	mov    -0xc(%ebp),%edx
801022ed:	89 10                	mov    %edx,(%eax)
      inum = de.inum;
801022ef:	0f b7 45 e0          	movzwl -0x20(%ebp),%eax
801022f3:	0f b7 c0             	movzwl %ax,%eax
801022f6:	89 45 f0             	mov    %eax,-0x10(%ebp)
      return iget(dp->dev, inum);
801022f9:	8b 45 08             	mov    0x8(%ebp),%eax
801022fc:	8b 00                	mov    (%eax),%eax
801022fe:	83 ec 08             	sub    $0x8,%esp
80102301:	ff 75 f0             	pushl  -0x10(%ebp)
80102304:	50                   	push   %eax
80102305:	e8 e5 f5 ff ff       	call   801018ef <iget>
8010230a:	83 c4 10             	add    $0x10,%esp
8010230d:	eb 19                	jmp    80102328 <dirlookup+0xb8>

  for(off = 0; off < dp->size; off += sizeof(de)){
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
      panic("dirlink read");
    if(de.inum == 0)
      continue;
8010230f:	90                   	nop
  struct dirent de;

  if(dp->type != T_DIR)
    panic("dirlookup not DIR");

  for(off = 0; off < dp->size; off += sizeof(de)){
80102310:	83 45 f4 10          	addl   $0x10,-0xc(%ebp)
80102314:	8b 45 08             	mov    0x8(%ebp),%eax
80102317:	8b 40 18             	mov    0x18(%eax),%eax
8010231a:	3b 45 f4             	cmp    -0xc(%ebp),%eax
8010231d:	0f 87 76 ff ff ff    	ja     80102299 <dirlookup+0x29>
      inum = de.inum;
      return iget(dp->dev, inum);
    }
  }

  return 0;
80102323:	b8 00 00 00 00       	mov    $0x0,%eax
}
80102328:	c9                   	leave  
80102329:	c3                   	ret    

8010232a <dirlink>:

// Write a new directory entry (name, inum) into the directory dp.
int
dirlink(struct inode *dp, char *name, uint inum)
{
8010232a:	55                   	push   %ebp
8010232b:	89 e5                	mov    %esp,%ebp
8010232d:	83 ec 28             	sub    $0x28,%esp
  int off;
  struct dirent de;
  struct inode *ip;

  // Check that name is not present.
  if((ip = dirlookup(dp, name, 0)) != 0){
80102330:	83 ec 04             	sub    $0x4,%esp
80102333:	6a 00                	push   $0x0
80102335:	ff 75 0c             	pushl  0xc(%ebp)
80102338:	ff 75 08             	pushl  0x8(%ebp)
8010233b:	e8 30 ff ff ff       	call   80102270 <dirlookup>
80102340:	83 c4 10             	add    $0x10,%esp
80102343:	89 45 f0             	mov    %eax,-0x10(%ebp)
80102346:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
8010234a:	74 18                	je     80102364 <dirlink+0x3a>
    iput(ip);
8010234c:	83 ec 0c             	sub    $0xc,%esp
8010234f:	ff 75 f0             	pushl  -0x10(%ebp)
80102352:	e8 81 f8 ff ff       	call   80101bd8 <iput>
80102357:	83 c4 10             	add    $0x10,%esp
    return -1;
8010235a:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010235f:	e9 9c 00 00 00       	jmp    80102400 <dirlink+0xd6>
  }

  // Look for an empty dirent.
  for(off = 0; off < dp->size; off += sizeof(de)){
80102364:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
8010236b:	eb 39                	jmp    801023a6 <dirlink+0x7c>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
8010236d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102370:	6a 10                	push   $0x10
80102372:	50                   	push   %eax
80102373:	8d 45 e0             	lea    -0x20(%ebp),%eax
80102376:	50                   	push   %eax
80102377:	ff 75 08             	pushl  0x8(%ebp)
8010237a:	e8 f7 fb ff ff       	call   80101f76 <readi>
8010237f:	83 c4 10             	add    $0x10,%esp
80102382:	83 f8 10             	cmp    $0x10,%eax
80102385:	74 0d                	je     80102394 <dirlink+0x6a>
      panic("dirlink read");
80102387:	83 ec 0c             	sub    $0xc,%esp
8010238a:	68 85 a2 10 80       	push   $0x8010a285
8010238f:	e8 d2 e1 ff ff       	call   80100566 <panic>
    if(de.inum == 0)
80102394:	0f b7 45 e0          	movzwl -0x20(%ebp),%eax
80102398:	66 85 c0             	test   %ax,%ax
8010239b:	74 18                	je     801023b5 <dirlink+0x8b>
    iput(ip);
    return -1;
  }

  // Look for an empty dirent.
  for(off = 0; off < dp->size; off += sizeof(de)){
8010239d:	8b 45 f4             	mov    -0xc(%ebp),%eax
801023a0:	83 c0 10             	add    $0x10,%eax
801023a3:	89 45 f4             	mov    %eax,-0xc(%ebp)
801023a6:	8b 45 08             	mov    0x8(%ebp),%eax
801023a9:	8b 50 18             	mov    0x18(%eax),%edx
801023ac:	8b 45 f4             	mov    -0xc(%ebp),%eax
801023af:	39 c2                	cmp    %eax,%edx
801023b1:	77 ba                	ja     8010236d <dirlink+0x43>
801023b3:	eb 01                	jmp    801023b6 <dirlink+0x8c>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
      panic("dirlink read");
    if(de.inum == 0)
      break;
801023b5:	90                   	nop
  }

  strncpy(de.name, name, DIRSIZ);
801023b6:	83 ec 04             	sub    $0x4,%esp
801023b9:	6a 0e                	push   $0xe
801023bb:	ff 75 0c             	pushl  0xc(%ebp)
801023be:	8d 45 e0             	lea    -0x20(%ebp),%eax
801023c1:	83 c0 02             	add    $0x2,%eax
801023c4:	50                   	push   %eax
801023c5:	e8 79 4a 00 00       	call   80106e43 <strncpy>
801023ca:	83 c4 10             	add    $0x10,%esp
  de.inum = inum;
801023cd:	8b 45 10             	mov    0x10(%ebp),%eax
801023d0:	66 89 45 e0          	mov    %ax,-0x20(%ebp)
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
801023d4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801023d7:	6a 10                	push   $0x10
801023d9:	50                   	push   %eax
801023da:	8d 45 e0             	lea    -0x20(%ebp),%eax
801023dd:	50                   	push   %eax
801023de:	ff 75 08             	pushl  0x8(%ebp)
801023e1:	e8 e7 fc ff ff       	call   801020cd <writei>
801023e6:	83 c4 10             	add    $0x10,%esp
801023e9:	83 f8 10             	cmp    $0x10,%eax
801023ec:	74 0d                	je     801023fb <dirlink+0xd1>
    panic("dirlink");
801023ee:	83 ec 0c             	sub    $0xc,%esp
801023f1:	68 92 a2 10 80       	push   $0x8010a292
801023f6:	e8 6b e1 ff ff       	call   80100566 <panic>
  
  return 0;
801023fb:	b8 00 00 00 00       	mov    $0x0,%eax
}
80102400:	c9                   	leave  
80102401:	c3                   	ret    

80102402 <skipelem>:
//   skipelem("a", name) = "", setting name = "a"
//   skipelem("", name) = skipelem("////", name) = 0
//
static char*
skipelem(char *path, char *name)
{
80102402:	55                   	push   %ebp
80102403:	89 e5                	mov    %esp,%ebp
80102405:	83 ec 18             	sub    $0x18,%esp
  char *s;
  int len;

  while(*path == '/')
80102408:	eb 04                	jmp    8010240e <skipelem+0xc>
    path++;
8010240a:	83 45 08 01          	addl   $0x1,0x8(%ebp)
skipelem(char *path, char *name)
{
  char *s;
  int len;

  while(*path == '/')
8010240e:	8b 45 08             	mov    0x8(%ebp),%eax
80102411:	0f b6 00             	movzbl (%eax),%eax
80102414:	3c 2f                	cmp    $0x2f,%al
80102416:	74 f2                	je     8010240a <skipelem+0x8>
    path++;
  if(*path == 0)
80102418:	8b 45 08             	mov    0x8(%ebp),%eax
8010241b:	0f b6 00             	movzbl (%eax),%eax
8010241e:	84 c0                	test   %al,%al
80102420:	75 07                	jne    80102429 <skipelem+0x27>
    return 0;
80102422:	b8 00 00 00 00       	mov    $0x0,%eax
80102427:	eb 7b                	jmp    801024a4 <skipelem+0xa2>
  s = path;
80102429:	8b 45 08             	mov    0x8(%ebp),%eax
8010242c:	89 45 f4             	mov    %eax,-0xc(%ebp)
  while(*path != '/' && *path != 0)
8010242f:	eb 04                	jmp    80102435 <skipelem+0x33>
    path++;
80102431:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  while(*path == '/')
    path++;
  if(*path == 0)
    return 0;
  s = path;
  while(*path != '/' && *path != 0)
80102435:	8b 45 08             	mov    0x8(%ebp),%eax
80102438:	0f b6 00             	movzbl (%eax),%eax
8010243b:	3c 2f                	cmp    $0x2f,%al
8010243d:	74 0a                	je     80102449 <skipelem+0x47>
8010243f:	8b 45 08             	mov    0x8(%ebp),%eax
80102442:	0f b6 00             	movzbl (%eax),%eax
80102445:	84 c0                	test   %al,%al
80102447:	75 e8                	jne    80102431 <skipelem+0x2f>
    path++;
  len = path - s;
80102449:	8b 55 08             	mov    0x8(%ebp),%edx
8010244c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010244f:	29 c2                	sub    %eax,%edx
80102451:	89 d0                	mov    %edx,%eax
80102453:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(len >= DIRSIZ)
80102456:	83 7d f0 0d          	cmpl   $0xd,-0x10(%ebp)
8010245a:	7e 15                	jle    80102471 <skipelem+0x6f>
    memmove(name, s, DIRSIZ);
8010245c:	83 ec 04             	sub    $0x4,%esp
8010245f:	6a 0e                	push   $0xe
80102461:	ff 75 f4             	pushl  -0xc(%ebp)
80102464:	ff 75 0c             	pushl  0xc(%ebp)
80102467:	e8 eb 48 00 00       	call   80106d57 <memmove>
8010246c:	83 c4 10             	add    $0x10,%esp
8010246f:	eb 26                	jmp    80102497 <skipelem+0x95>
  else {
    memmove(name, s, len);
80102471:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102474:	83 ec 04             	sub    $0x4,%esp
80102477:	50                   	push   %eax
80102478:	ff 75 f4             	pushl  -0xc(%ebp)
8010247b:	ff 75 0c             	pushl  0xc(%ebp)
8010247e:	e8 d4 48 00 00       	call   80106d57 <memmove>
80102483:	83 c4 10             	add    $0x10,%esp
    name[len] = 0;
80102486:	8b 55 f0             	mov    -0x10(%ebp),%edx
80102489:	8b 45 0c             	mov    0xc(%ebp),%eax
8010248c:	01 d0                	add    %edx,%eax
8010248e:	c6 00 00             	movb   $0x0,(%eax)
  }
  while(*path == '/')
80102491:	eb 04                	jmp    80102497 <skipelem+0x95>
    path++;
80102493:	83 45 08 01          	addl   $0x1,0x8(%ebp)
    memmove(name, s, DIRSIZ);
  else {
    memmove(name, s, len);
    name[len] = 0;
  }
  while(*path == '/')
80102497:	8b 45 08             	mov    0x8(%ebp),%eax
8010249a:	0f b6 00             	movzbl (%eax),%eax
8010249d:	3c 2f                	cmp    $0x2f,%al
8010249f:	74 f2                	je     80102493 <skipelem+0x91>
    path++;
  return path;
801024a1:	8b 45 08             	mov    0x8(%ebp),%eax
}
801024a4:	c9                   	leave  
801024a5:	c3                   	ret    

801024a6 <namex>:
// If parent != 0, return the inode for the parent and copy the final
// path element into name, which must have room for DIRSIZ bytes.
// Must be called inside a transaction since it calls iput().
static struct inode*
namex(char *path, int nameiparent, char *name)
{
801024a6:	55                   	push   %ebp
801024a7:	89 e5                	mov    %esp,%ebp
801024a9:	83 ec 18             	sub    $0x18,%esp
  struct inode *ip, *next;

  if(*path == '/')
801024ac:	8b 45 08             	mov    0x8(%ebp),%eax
801024af:	0f b6 00             	movzbl (%eax),%eax
801024b2:	3c 2f                	cmp    $0x2f,%al
801024b4:	75 17                	jne    801024cd <namex+0x27>
    ip = iget(ROOTDEV, ROOTINO);
801024b6:	83 ec 08             	sub    $0x8,%esp
801024b9:	6a 01                	push   $0x1
801024bb:	6a 01                	push   $0x1
801024bd:	e8 2d f4 ff ff       	call   801018ef <iget>
801024c2:	83 c4 10             	add    $0x10,%esp
801024c5:	89 45 f4             	mov    %eax,-0xc(%ebp)
801024c8:	e9 bb 00 00 00       	jmp    80102588 <namex+0xe2>
  else
    ip = idup(proc->cwd);
801024cd:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801024d3:	8b 40 70             	mov    0x70(%eax),%eax
801024d6:	83 ec 0c             	sub    $0xc,%esp
801024d9:	50                   	push   %eax
801024da:	e8 ef f4 ff ff       	call   801019ce <idup>
801024df:	83 c4 10             	add    $0x10,%esp
801024e2:	89 45 f4             	mov    %eax,-0xc(%ebp)

  while((path = skipelem(path, name)) != 0){
801024e5:	e9 9e 00 00 00       	jmp    80102588 <namex+0xe2>
    ilock(ip);
801024ea:	83 ec 0c             	sub    $0xc,%esp
801024ed:	ff 75 f4             	pushl  -0xc(%ebp)
801024f0:	e8 13 f5 ff ff       	call   80101a08 <ilock>
801024f5:	83 c4 10             	add    $0x10,%esp
    if(ip->type != T_DIR){
801024f8:	8b 45 f4             	mov    -0xc(%ebp),%eax
801024fb:	0f b7 40 10          	movzwl 0x10(%eax),%eax
801024ff:	66 83 f8 01          	cmp    $0x1,%ax
80102503:	74 18                	je     8010251d <namex+0x77>
      iunlockput(ip);
80102505:	83 ec 0c             	sub    $0xc,%esp
80102508:	ff 75 f4             	pushl  -0xc(%ebp)
8010250b:	e8 b8 f7 ff ff       	call   80101cc8 <iunlockput>
80102510:	83 c4 10             	add    $0x10,%esp
      return 0;
80102513:	b8 00 00 00 00       	mov    $0x0,%eax
80102518:	e9 a7 00 00 00       	jmp    801025c4 <namex+0x11e>
    }
    if(nameiparent && *path == '\0'){
8010251d:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
80102521:	74 20                	je     80102543 <namex+0x9d>
80102523:	8b 45 08             	mov    0x8(%ebp),%eax
80102526:	0f b6 00             	movzbl (%eax),%eax
80102529:	84 c0                	test   %al,%al
8010252b:	75 16                	jne    80102543 <namex+0x9d>
      // Stop one level early.
      iunlock(ip);
8010252d:	83 ec 0c             	sub    $0xc,%esp
80102530:	ff 75 f4             	pushl  -0xc(%ebp)
80102533:	e8 2e f6 ff ff       	call   80101b66 <iunlock>
80102538:	83 c4 10             	add    $0x10,%esp
      return ip;
8010253b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010253e:	e9 81 00 00 00       	jmp    801025c4 <namex+0x11e>
    }
    if((next = dirlookup(ip, name, 0)) == 0){
80102543:	83 ec 04             	sub    $0x4,%esp
80102546:	6a 00                	push   $0x0
80102548:	ff 75 10             	pushl  0x10(%ebp)
8010254b:	ff 75 f4             	pushl  -0xc(%ebp)
8010254e:	e8 1d fd ff ff       	call   80102270 <dirlookup>
80102553:	83 c4 10             	add    $0x10,%esp
80102556:	89 45 f0             	mov    %eax,-0x10(%ebp)
80102559:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
8010255d:	75 15                	jne    80102574 <namex+0xce>
      iunlockput(ip);
8010255f:	83 ec 0c             	sub    $0xc,%esp
80102562:	ff 75 f4             	pushl  -0xc(%ebp)
80102565:	e8 5e f7 ff ff       	call   80101cc8 <iunlockput>
8010256a:	83 c4 10             	add    $0x10,%esp
      return 0;
8010256d:	b8 00 00 00 00       	mov    $0x0,%eax
80102572:	eb 50                	jmp    801025c4 <namex+0x11e>
    }
    iunlockput(ip);
80102574:	83 ec 0c             	sub    $0xc,%esp
80102577:	ff 75 f4             	pushl  -0xc(%ebp)
8010257a:	e8 49 f7 ff ff       	call   80101cc8 <iunlockput>
8010257f:	83 c4 10             	add    $0x10,%esp
    ip = next;
80102582:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102585:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(*path == '/')
    ip = iget(ROOTDEV, ROOTINO);
  else
    ip = idup(proc->cwd);

  while((path = skipelem(path, name)) != 0){
80102588:	83 ec 08             	sub    $0x8,%esp
8010258b:	ff 75 10             	pushl  0x10(%ebp)
8010258e:	ff 75 08             	pushl  0x8(%ebp)
80102591:	e8 6c fe ff ff       	call   80102402 <skipelem>
80102596:	83 c4 10             	add    $0x10,%esp
80102599:	89 45 08             	mov    %eax,0x8(%ebp)
8010259c:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
801025a0:	0f 85 44 ff ff ff    	jne    801024ea <namex+0x44>
      return 0;
    }
    iunlockput(ip);
    ip = next;
  }
  if(nameiparent){
801025a6:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
801025aa:	74 15                	je     801025c1 <namex+0x11b>
    iput(ip);
801025ac:	83 ec 0c             	sub    $0xc,%esp
801025af:	ff 75 f4             	pushl  -0xc(%ebp)
801025b2:	e8 21 f6 ff ff       	call   80101bd8 <iput>
801025b7:	83 c4 10             	add    $0x10,%esp
    return 0;
801025ba:	b8 00 00 00 00       	mov    $0x0,%eax
801025bf:	eb 03                	jmp    801025c4 <namex+0x11e>
  }
  return ip;
801025c1:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
801025c4:	c9                   	leave  
801025c5:	c3                   	ret    

801025c6 <namei>:

struct inode*
namei(char *path)
{
801025c6:	55                   	push   %ebp
801025c7:	89 e5                	mov    %esp,%ebp
801025c9:	83 ec 18             	sub    $0x18,%esp
  char name[DIRSIZ];
  return namex(path, 0, name);
801025cc:	83 ec 04             	sub    $0x4,%esp
801025cf:	8d 45 ea             	lea    -0x16(%ebp),%eax
801025d2:	50                   	push   %eax
801025d3:	6a 00                	push   $0x0
801025d5:	ff 75 08             	pushl  0x8(%ebp)
801025d8:	e8 c9 fe ff ff       	call   801024a6 <namex>
801025dd:	83 c4 10             	add    $0x10,%esp
}
801025e0:	c9                   	leave  
801025e1:	c3                   	ret    

801025e2 <nameiparent>:

struct inode*
nameiparent(char *path, char *name)
{
801025e2:	55                   	push   %ebp
801025e3:	89 e5                	mov    %esp,%ebp
801025e5:	83 ec 08             	sub    $0x8,%esp
  return namex(path, 1, name);
801025e8:	83 ec 04             	sub    $0x4,%esp
801025eb:	ff 75 0c             	pushl  0xc(%ebp)
801025ee:	6a 01                	push   $0x1
801025f0:	ff 75 08             	pushl  0x8(%ebp)
801025f3:	e8 ae fe ff ff       	call   801024a6 <namex>
801025f8:	83 c4 10             	add    $0x10,%esp
}
801025fb:	c9                   	leave  
801025fc:	c3                   	ret    

801025fd <inb>:

// end of CS333 added routines

static inline uchar
inb(ushort port)
{
801025fd:	55                   	push   %ebp
801025fe:	89 e5                	mov    %esp,%ebp
80102600:	83 ec 14             	sub    $0x14,%esp
80102603:	8b 45 08             	mov    0x8(%ebp),%eax
80102606:	66 89 45 ec          	mov    %ax,-0x14(%ebp)
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
8010260a:	0f b7 45 ec          	movzwl -0x14(%ebp),%eax
8010260e:	89 c2                	mov    %eax,%edx
80102610:	ec                   	in     (%dx),%al
80102611:	88 45 ff             	mov    %al,-0x1(%ebp)
  return data;
80102614:	0f b6 45 ff          	movzbl -0x1(%ebp),%eax
}
80102618:	c9                   	leave  
80102619:	c3                   	ret    

8010261a <insl>:

static inline void
insl(int port, void *addr, int cnt)
{
8010261a:	55                   	push   %ebp
8010261b:	89 e5                	mov    %esp,%ebp
8010261d:	57                   	push   %edi
8010261e:	53                   	push   %ebx
  asm volatile("cld; rep insl" :
8010261f:	8b 55 08             	mov    0x8(%ebp),%edx
80102622:	8b 4d 0c             	mov    0xc(%ebp),%ecx
80102625:	8b 45 10             	mov    0x10(%ebp),%eax
80102628:	89 cb                	mov    %ecx,%ebx
8010262a:	89 df                	mov    %ebx,%edi
8010262c:	89 c1                	mov    %eax,%ecx
8010262e:	fc                   	cld    
8010262f:	f3 6d                	rep insl (%dx),%es:(%edi)
80102631:	89 c8                	mov    %ecx,%eax
80102633:	89 fb                	mov    %edi,%ebx
80102635:	89 5d 0c             	mov    %ebx,0xc(%ebp)
80102638:	89 45 10             	mov    %eax,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "d" (port), "0" (addr), "1" (cnt) :
               "memory", "cc");
}
8010263b:	90                   	nop
8010263c:	5b                   	pop    %ebx
8010263d:	5f                   	pop    %edi
8010263e:	5d                   	pop    %ebp
8010263f:	c3                   	ret    

80102640 <outb>:

static inline void
outb(ushort port, uchar data)
{
80102640:	55                   	push   %ebp
80102641:	89 e5                	mov    %esp,%ebp
80102643:	83 ec 08             	sub    $0x8,%esp
80102646:	8b 55 08             	mov    0x8(%ebp),%edx
80102649:	8b 45 0c             	mov    0xc(%ebp),%eax
8010264c:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
80102650:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102653:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
80102657:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
8010265b:	ee                   	out    %al,(%dx)
}
8010265c:	90                   	nop
8010265d:	c9                   	leave  
8010265e:	c3                   	ret    

8010265f <outsl>:
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
}

static inline void
outsl(int port, const void *addr, int cnt)
{
8010265f:	55                   	push   %ebp
80102660:	89 e5                	mov    %esp,%ebp
80102662:	56                   	push   %esi
80102663:	53                   	push   %ebx
  asm volatile("cld; rep outsl" :
80102664:	8b 55 08             	mov    0x8(%ebp),%edx
80102667:	8b 4d 0c             	mov    0xc(%ebp),%ecx
8010266a:	8b 45 10             	mov    0x10(%ebp),%eax
8010266d:	89 cb                	mov    %ecx,%ebx
8010266f:	89 de                	mov    %ebx,%esi
80102671:	89 c1                	mov    %eax,%ecx
80102673:	fc                   	cld    
80102674:	f3 6f                	rep outsl %ds:(%esi),(%dx)
80102676:	89 c8                	mov    %ecx,%eax
80102678:	89 f3                	mov    %esi,%ebx
8010267a:	89 5d 0c             	mov    %ebx,0xc(%ebp)
8010267d:	89 45 10             	mov    %eax,0x10(%ebp)
               "=S" (addr), "=c" (cnt) :
               "d" (port), "0" (addr), "1" (cnt) :
               "cc");
}
80102680:	90                   	nop
80102681:	5b                   	pop    %ebx
80102682:	5e                   	pop    %esi
80102683:	5d                   	pop    %ebp
80102684:	c3                   	ret    

80102685 <idewait>:
static void idestart(struct buf*);

// Wait for IDE disk to become ready.
static int
idewait(int checkerr)
{
80102685:	55                   	push   %ebp
80102686:	89 e5                	mov    %esp,%ebp
80102688:	83 ec 10             	sub    $0x10,%esp
  int r;

  while(((r = inb(0x1f7)) & (IDE_BSY|IDE_DRDY)) != IDE_DRDY) 
8010268b:	90                   	nop
8010268c:	68 f7 01 00 00       	push   $0x1f7
80102691:	e8 67 ff ff ff       	call   801025fd <inb>
80102696:	83 c4 04             	add    $0x4,%esp
80102699:	0f b6 c0             	movzbl %al,%eax
8010269c:	89 45 fc             	mov    %eax,-0x4(%ebp)
8010269f:	8b 45 fc             	mov    -0x4(%ebp),%eax
801026a2:	25 c0 00 00 00       	and    $0xc0,%eax
801026a7:	83 f8 40             	cmp    $0x40,%eax
801026aa:	75 e0                	jne    8010268c <idewait+0x7>
    ;
  if(checkerr && (r & (IDE_DF|IDE_ERR)) != 0)
801026ac:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
801026b0:	74 11                	je     801026c3 <idewait+0x3e>
801026b2:	8b 45 fc             	mov    -0x4(%ebp),%eax
801026b5:	83 e0 21             	and    $0x21,%eax
801026b8:	85 c0                	test   %eax,%eax
801026ba:	74 07                	je     801026c3 <idewait+0x3e>
    return -1;
801026bc:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801026c1:	eb 05                	jmp    801026c8 <idewait+0x43>
  return 0;
801026c3:	b8 00 00 00 00       	mov    $0x0,%eax
}
801026c8:	c9                   	leave  
801026c9:	c3                   	ret    

801026ca <ideinit>:

void
ideinit(void)
{
801026ca:	55                   	push   %ebp
801026cb:	89 e5                	mov    %esp,%ebp
801026cd:	83 ec 18             	sub    $0x18,%esp
  int i;
  
  initlock(&idelock, "ide");
801026d0:	83 ec 08             	sub    $0x8,%esp
801026d3:	68 9a a2 10 80       	push   $0x8010a29a
801026d8:	68 20 d6 10 80       	push   $0x8010d620
801026dd:	e8 31 43 00 00       	call   80106a13 <initlock>
801026e2:	83 c4 10             	add    $0x10,%esp
  picenable(IRQ_IDE);
801026e5:	83 ec 0c             	sub    $0xc,%esp
801026e8:	6a 0e                	push   $0xe
801026ea:	e8 da 18 00 00       	call   80103fc9 <picenable>
801026ef:	83 c4 10             	add    $0x10,%esp
  ioapicenable(IRQ_IDE, ncpu - 1);
801026f2:	a1 60 49 11 80       	mov    0x80114960,%eax
801026f7:	83 e8 01             	sub    $0x1,%eax
801026fa:	83 ec 08             	sub    $0x8,%esp
801026fd:	50                   	push   %eax
801026fe:	6a 0e                	push   $0xe
80102700:	e8 73 04 00 00       	call   80102b78 <ioapicenable>
80102705:	83 c4 10             	add    $0x10,%esp
  idewait(0);
80102708:	83 ec 0c             	sub    $0xc,%esp
8010270b:	6a 00                	push   $0x0
8010270d:	e8 73 ff ff ff       	call   80102685 <idewait>
80102712:	83 c4 10             	add    $0x10,%esp
  
  // Check if disk 1 is present
  outb(0x1f6, 0xe0 | (1<<4));
80102715:	83 ec 08             	sub    $0x8,%esp
80102718:	68 f0 00 00 00       	push   $0xf0
8010271d:	68 f6 01 00 00       	push   $0x1f6
80102722:	e8 19 ff ff ff       	call   80102640 <outb>
80102727:	83 c4 10             	add    $0x10,%esp
  for(i=0; i<1000; i++){
8010272a:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80102731:	eb 24                	jmp    80102757 <ideinit+0x8d>
    if(inb(0x1f7) != 0){
80102733:	83 ec 0c             	sub    $0xc,%esp
80102736:	68 f7 01 00 00       	push   $0x1f7
8010273b:	e8 bd fe ff ff       	call   801025fd <inb>
80102740:	83 c4 10             	add    $0x10,%esp
80102743:	84 c0                	test   %al,%al
80102745:	74 0c                	je     80102753 <ideinit+0x89>
      havedisk1 = 1;
80102747:	c7 05 58 d6 10 80 01 	movl   $0x1,0x8010d658
8010274e:	00 00 00 
      break;
80102751:	eb 0d                	jmp    80102760 <ideinit+0x96>
  ioapicenable(IRQ_IDE, ncpu - 1);
  idewait(0);
  
  // Check if disk 1 is present
  outb(0x1f6, 0xe0 | (1<<4));
  for(i=0; i<1000; i++){
80102753:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80102757:	81 7d f4 e7 03 00 00 	cmpl   $0x3e7,-0xc(%ebp)
8010275e:	7e d3                	jle    80102733 <ideinit+0x69>
      break;
    }
  }
  
  // Switch back to disk 0.
  outb(0x1f6, 0xe0 | (0<<4));
80102760:	83 ec 08             	sub    $0x8,%esp
80102763:	68 e0 00 00 00       	push   $0xe0
80102768:	68 f6 01 00 00       	push   $0x1f6
8010276d:	e8 ce fe ff ff       	call   80102640 <outb>
80102772:	83 c4 10             	add    $0x10,%esp
}
80102775:	90                   	nop
80102776:	c9                   	leave  
80102777:	c3                   	ret    

80102778 <idestart>:

// Start the request for b.  Caller must hold idelock.
static void
idestart(struct buf *b)
{
80102778:	55                   	push   %ebp
80102779:	89 e5                	mov    %esp,%ebp
8010277b:	83 ec 18             	sub    $0x18,%esp
  if(b == 0)
8010277e:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80102782:	75 0d                	jne    80102791 <idestart+0x19>
    panic("idestart");
80102784:	83 ec 0c             	sub    $0xc,%esp
80102787:	68 9e a2 10 80       	push   $0x8010a29e
8010278c:	e8 d5 dd ff ff       	call   80100566 <panic>
  if(b->blockno >= FSSIZE)
80102791:	8b 45 08             	mov    0x8(%ebp),%eax
80102794:	8b 40 08             	mov    0x8(%eax),%eax
80102797:	3d cf 07 00 00       	cmp    $0x7cf,%eax
8010279c:	76 0d                	jbe    801027ab <idestart+0x33>
    panic("incorrect blockno");
8010279e:	83 ec 0c             	sub    $0xc,%esp
801027a1:	68 a7 a2 10 80       	push   $0x8010a2a7
801027a6:	e8 bb dd ff ff       	call   80100566 <panic>
  int sector_per_block =  BSIZE/SECTOR_SIZE;
801027ab:	c7 45 f4 01 00 00 00 	movl   $0x1,-0xc(%ebp)
  int sector = b->blockno * sector_per_block;
801027b2:	8b 45 08             	mov    0x8(%ebp),%eax
801027b5:	8b 50 08             	mov    0x8(%eax),%edx
801027b8:	8b 45 f4             	mov    -0xc(%ebp),%eax
801027bb:	0f af c2             	imul   %edx,%eax
801027be:	89 45 f0             	mov    %eax,-0x10(%ebp)

  if (sector_per_block > 7) panic("idestart");
801027c1:	83 7d f4 07          	cmpl   $0x7,-0xc(%ebp)
801027c5:	7e 0d                	jle    801027d4 <idestart+0x5c>
801027c7:	83 ec 0c             	sub    $0xc,%esp
801027ca:	68 9e a2 10 80       	push   $0x8010a29e
801027cf:	e8 92 dd ff ff       	call   80100566 <panic>
  
  idewait(0);
801027d4:	83 ec 0c             	sub    $0xc,%esp
801027d7:	6a 00                	push   $0x0
801027d9:	e8 a7 fe ff ff       	call   80102685 <idewait>
801027de:	83 c4 10             	add    $0x10,%esp
  outb(0x3f6, 0);  // generate interrupt
801027e1:	83 ec 08             	sub    $0x8,%esp
801027e4:	6a 00                	push   $0x0
801027e6:	68 f6 03 00 00       	push   $0x3f6
801027eb:	e8 50 fe ff ff       	call   80102640 <outb>
801027f0:	83 c4 10             	add    $0x10,%esp
  outb(0x1f2, sector_per_block);  // number of sectors
801027f3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801027f6:	0f b6 c0             	movzbl %al,%eax
801027f9:	83 ec 08             	sub    $0x8,%esp
801027fc:	50                   	push   %eax
801027fd:	68 f2 01 00 00       	push   $0x1f2
80102802:	e8 39 fe ff ff       	call   80102640 <outb>
80102807:	83 c4 10             	add    $0x10,%esp
  outb(0x1f3, sector & 0xff);
8010280a:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010280d:	0f b6 c0             	movzbl %al,%eax
80102810:	83 ec 08             	sub    $0x8,%esp
80102813:	50                   	push   %eax
80102814:	68 f3 01 00 00       	push   $0x1f3
80102819:	e8 22 fe ff ff       	call   80102640 <outb>
8010281e:	83 c4 10             	add    $0x10,%esp
  outb(0x1f4, (sector >> 8) & 0xff);
80102821:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102824:	c1 f8 08             	sar    $0x8,%eax
80102827:	0f b6 c0             	movzbl %al,%eax
8010282a:	83 ec 08             	sub    $0x8,%esp
8010282d:	50                   	push   %eax
8010282e:	68 f4 01 00 00       	push   $0x1f4
80102833:	e8 08 fe ff ff       	call   80102640 <outb>
80102838:	83 c4 10             	add    $0x10,%esp
  outb(0x1f5, (sector >> 16) & 0xff);
8010283b:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010283e:	c1 f8 10             	sar    $0x10,%eax
80102841:	0f b6 c0             	movzbl %al,%eax
80102844:	83 ec 08             	sub    $0x8,%esp
80102847:	50                   	push   %eax
80102848:	68 f5 01 00 00       	push   $0x1f5
8010284d:	e8 ee fd ff ff       	call   80102640 <outb>
80102852:	83 c4 10             	add    $0x10,%esp
  outb(0x1f6, 0xe0 | ((b->dev&1)<<4) | ((sector>>24)&0x0f));
80102855:	8b 45 08             	mov    0x8(%ebp),%eax
80102858:	8b 40 04             	mov    0x4(%eax),%eax
8010285b:	83 e0 01             	and    $0x1,%eax
8010285e:	c1 e0 04             	shl    $0x4,%eax
80102861:	89 c2                	mov    %eax,%edx
80102863:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102866:	c1 f8 18             	sar    $0x18,%eax
80102869:	83 e0 0f             	and    $0xf,%eax
8010286c:	09 d0                	or     %edx,%eax
8010286e:	83 c8 e0             	or     $0xffffffe0,%eax
80102871:	0f b6 c0             	movzbl %al,%eax
80102874:	83 ec 08             	sub    $0x8,%esp
80102877:	50                   	push   %eax
80102878:	68 f6 01 00 00       	push   $0x1f6
8010287d:	e8 be fd ff ff       	call   80102640 <outb>
80102882:	83 c4 10             	add    $0x10,%esp
  if(b->flags & B_DIRTY){
80102885:	8b 45 08             	mov    0x8(%ebp),%eax
80102888:	8b 00                	mov    (%eax),%eax
8010288a:	83 e0 04             	and    $0x4,%eax
8010288d:	85 c0                	test   %eax,%eax
8010288f:	74 30                	je     801028c1 <idestart+0x149>
    outb(0x1f7, IDE_CMD_WRITE);
80102891:	83 ec 08             	sub    $0x8,%esp
80102894:	6a 30                	push   $0x30
80102896:	68 f7 01 00 00       	push   $0x1f7
8010289b:	e8 a0 fd ff ff       	call   80102640 <outb>
801028a0:	83 c4 10             	add    $0x10,%esp
    outsl(0x1f0, b->data, BSIZE/4);
801028a3:	8b 45 08             	mov    0x8(%ebp),%eax
801028a6:	83 c0 18             	add    $0x18,%eax
801028a9:	83 ec 04             	sub    $0x4,%esp
801028ac:	68 80 00 00 00       	push   $0x80
801028b1:	50                   	push   %eax
801028b2:	68 f0 01 00 00       	push   $0x1f0
801028b7:	e8 a3 fd ff ff       	call   8010265f <outsl>
801028bc:	83 c4 10             	add    $0x10,%esp
  } else {
    outb(0x1f7, IDE_CMD_READ);
  }
}
801028bf:	eb 12                	jmp    801028d3 <idestart+0x15b>
  outb(0x1f6, 0xe0 | ((b->dev&1)<<4) | ((sector>>24)&0x0f));
  if(b->flags & B_DIRTY){
    outb(0x1f7, IDE_CMD_WRITE);
    outsl(0x1f0, b->data, BSIZE/4);
  } else {
    outb(0x1f7, IDE_CMD_READ);
801028c1:	83 ec 08             	sub    $0x8,%esp
801028c4:	6a 20                	push   $0x20
801028c6:	68 f7 01 00 00       	push   $0x1f7
801028cb:	e8 70 fd ff ff       	call   80102640 <outb>
801028d0:	83 c4 10             	add    $0x10,%esp
  }
}
801028d3:	90                   	nop
801028d4:	c9                   	leave  
801028d5:	c3                   	ret    

801028d6 <ideintr>:

// Interrupt handler.
void
ideintr(void)
{
801028d6:	55                   	push   %ebp
801028d7:	89 e5                	mov    %esp,%ebp
801028d9:	83 ec 18             	sub    $0x18,%esp
  struct buf *b;

  // First queued buffer is the active request.
  acquire(&idelock);
801028dc:	83 ec 0c             	sub    $0xc,%esp
801028df:	68 20 d6 10 80       	push   $0x8010d620
801028e4:	e8 4c 41 00 00       	call   80106a35 <acquire>
801028e9:	83 c4 10             	add    $0x10,%esp
  if((b = idequeue) == 0){
801028ec:	a1 54 d6 10 80       	mov    0x8010d654,%eax
801028f1:	89 45 f4             	mov    %eax,-0xc(%ebp)
801028f4:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801028f8:	75 15                	jne    8010290f <ideintr+0x39>
    release(&idelock);
801028fa:	83 ec 0c             	sub    $0xc,%esp
801028fd:	68 20 d6 10 80       	push   $0x8010d620
80102902:	e8 95 41 00 00       	call   80106a9c <release>
80102907:	83 c4 10             	add    $0x10,%esp
    // cprintf("spurious IDE interrupt\n");
    return;
8010290a:	e9 9a 00 00 00       	jmp    801029a9 <ideintr+0xd3>
  }
  idequeue = b->qnext;
8010290f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102912:	8b 40 14             	mov    0x14(%eax),%eax
80102915:	a3 54 d6 10 80       	mov    %eax,0x8010d654

  // Read data if needed.
  if(!(b->flags & B_DIRTY) && idewait(1) >= 0)
8010291a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010291d:	8b 00                	mov    (%eax),%eax
8010291f:	83 e0 04             	and    $0x4,%eax
80102922:	85 c0                	test   %eax,%eax
80102924:	75 2d                	jne    80102953 <ideintr+0x7d>
80102926:	83 ec 0c             	sub    $0xc,%esp
80102929:	6a 01                	push   $0x1
8010292b:	e8 55 fd ff ff       	call   80102685 <idewait>
80102930:	83 c4 10             	add    $0x10,%esp
80102933:	85 c0                	test   %eax,%eax
80102935:	78 1c                	js     80102953 <ideintr+0x7d>
    insl(0x1f0, b->data, BSIZE/4);
80102937:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010293a:	83 c0 18             	add    $0x18,%eax
8010293d:	83 ec 04             	sub    $0x4,%esp
80102940:	68 80 00 00 00       	push   $0x80
80102945:	50                   	push   %eax
80102946:	68 f0 01 00 00       	push   $0x1f0
8010294b:	e8 ca fc ff ff       	call   8010261a <insl>
80102950:	83 c4 10             	add    $0x10,%esp
  
  // Wake process waiting for this buf.
  b->flags |= B_VALID;
80102953:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102956:	8b 00                	mov    (%eax),%eax
80102958:	83 c8 02             	or     $0x2,%eax
8010295b:	89 c2                	mov    %eax,%edx
8010295d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102960:	89 10                	mov    %edx,(%eax)
  b->flags &= ~B_DIRTY;
80102962:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102965:	8b 00                	mov    (%eax),%eax
80102967:	83 e0 fb             	and    $0xfffffffb,%eax
8010296a:	89 c2                	mov    %eax,%edx
8010296c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010296f:	89 10                	mov    %edx,(%eax)
  wakeup(b);
80102971:	83 ec 0c             	sub    $0xc,%esp
80102974:	ff 75 f4             	pushl  -0xc(%ebp)
80102977:	e8 19 2c 00 00       	call   80105595 <wakeup>
8010297c:	83 c4 10             	add    $0x10,%esp
  
  // Start disk on next buf in queue.
  if(idequeue != 0)
8010297f:	a1 54 d6 10 80       	mov    0x8010d654,%eax
80102984:	85 c0                	test   %eax,%eax
80102986:	74 11                	je     80102999 <ideintr+0xc3>
    idestart(idequeue);
80102988:	a1 54 d6 10 80       	mov    0x8010d654,%eax
8010298d:	83 ec 0c             	sub    $0xc,%esp
80102990:	50                   	push   %eax
80102991:	e8 e2 fd ff ff       	call   80102778 <idestart>
80102996:	83 c4 10             	add    $0x10,%esp

  release(&idelock);
80102999:	83 ec 0c             	sub    $0xc,%esp
8010299c:	68 20 d6 10 80       	push   $0x8010d620
801029a1:	e8 f6 40 00 00       	call   80106a9c <release>
801029a6:	83 c4 10             	add    $0x10,%esp
}
801029a9:	c9                   	leave  
801029aa:	c3                   	ret    

801029ab <iderw>:
// Sync buf with disk. 
// If B_DIRTY is set, write buf to disk, clear B_DIRTY, set B_VALID.
// Else if B_VALID is not set, read buf from disk, set B_VALID.
void
iderw(struct buf *b)
{
801029ab:	55                   	push   %ebp
801029ac:	89 e5                	mov    %esp,%ebp
801029ae:	83 ec 18             	sub    $0x18,%esp
  struct buf **pp;

  if(!(b->flags & B_BUSY))
801029b1:	8b 45 08             	mov    0x8(%ebp),%eax
801029b4:	8b 00                	mov    (%eax),%eax
801029b6:	83 e0 01             	and    $0x1,%eax
801029b9:	85 c0                	test   %eax,%eax
801029bb:	75 0d                	jne    801029ca <iderw+0x1f>
    panic("iderw: buf not busy");
801029bd:	83 ec 0c             	sub    $0xc,%esp
801029c0:	68 b9 a2 10 80       	push   $0x8010a2b9
801029c5:	e8 9c db ff ff       	call   80100566 <panic>
  if((b->flags & (B_VALID|B_DIRTY)) == B_VALID)
801029ca:	8b 45 08             	mov    0x8(%ebp),%eax
801029cd:	8b 00                	mov    (%eax),%eax
801029cf:	83 e0 06             	and    $0x6,%eax
801029d2:	83 f8 02             	cmp    $0x2,%eax
801029d5:	75 0d                	jne    801029e4 <iderw+0x39>
    panic("iderw: nothing to do");
801029d7:	83 ec 0c             	sub    $0xc,%esp
801029da:	68 cd a2 10 80       	push   $0x8010a2cd
801029df:	e8 82 db ff ff       	call   80100566 <panic>
  if(b->dev != 0 && !havedisk1)
801029e4:	8b 45 08             	mov    0x8(%ebp),%eax
801029e7:	8b 40 04             	mov    0x4(%eax),%eax
801029ea:	85 c0                	test   %eax,%eax
801029ec:	74 16                	je     80102a04 <iderw+0x59>
801029ee:	a1 58 d6 10 80       	mov    0x8010d658,%eax
801029f3:	85 c0                	test   %eax,%eax
801029f5:	75 0d                	jne    80102a04 <iderw+0x59>
    panic("iderw: ide disk 1 not present");
801029f7:	83 ec 0c             	sub    $0xc,%esp
801029fa:	68 e2 a2 10 80       	push   $0x8010a2e2
801029ff:	e8 62 db ff ff       	call   80100566 <panic>

  acquire(&idelock);  //DOC:acquire-lock
80102a04:	83 ec 0c             	sub    $0xc,%esp
80102a07:	68 20 d6 10 80       	push   $0x8010d620
80102a0c:	e8 24 40 00 00       	call   80106a35 <acquire>
80102a11:	83 c4 10             	add    $0x10,%esp

  // Append b to idequeue.
  b->qnext = 0;
80102a14:	8b 45 08             	mov    0x8(%ebp),%eax
80102a17:	c7 40 14 00 00 00 00 	movl   $0x0,0x14(%eax)
  for(pp=&idequeue; *pp; pp=&(*pp)->qnext)  //DOC:insert-queue
80102a1e:	c7 45 f4 54 d6 10 80 	movl   $0x8010d654,-0xc(%ebp)
80102a25:	eb 0b                	jmp    80102a32 <iderw+0x87>
80102a27:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102a2a:	8b 00                	mov    (%eax),%eax
80102a2c:	83 c0 14             	add    $0x14,%eax
80102a2f:	89 45 f4             	mov    %eax,-0xc(%ebp)
80102a32:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102a35:	8b 00                	mov    (%eax),%eax
80102a37:	85 c0                	test   %eax,%eax
80102a39:	75 ec                	jne    80102a27 <iderw+0x7c>
    ;
  *pp = b;
80102a3b:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102a3e:	8b 55 08             	mov    0x8(%ebp),%edx
80102a41:	89 10                	mov    %edx,(%eax)
  
  // Start disk if necessary.
  if(idequeue == b)
80102a43:	a1 54 d6 10 80       	mov    0x8010d654,%eax
80102a48:	3b 45 08             	cmp    0x8(%ebp),%eax
80102a4b:	75 23                	jne    80102a70 <iderw+0xc5>
    idestart(b);
80102a4d:	83 ec 0c             	sub    $0xc,%esp
80102a50:	ff 75 08             	pushl  0x8(%ebp)
80102a53:	e8 20 fd ff ff       	call   80102778 <idestart>
80102a58:	83 c4 10             	add    $0x10,%esp
  
  // Wait for request to finish.
  while((b->flags & (B_VALID|B_DIRTY)) != B_VALID){
80102a5b:	eb 13                	jmp    80102a70 <iderw+0xc5>
    sleep(b, &idelock);
80102a5d:	83 ec 08             	sub    $0x8,%esp
80102a60:	68 20 d6 10 80       	push   $0x8010d620
80102a65:	ff 75 08             	pushl  0x8(%ebp)
80102a68:	e8 7f 29 00 00       	call   801053ec <sleep>
80102a6d:	83 c4 10             	add    $0x10,%esp
  // Start disk if necessary.
  if(idequeue == b)
    idestart(b);
  
  // Wait for request to finish.
  while((b->flags & (B_VALID|B_DIRTY)) != B_VALID){
80102a70:	8b 45 08             	mov    0x8(%ebp),%eax
80102a73:	8b 00                	mov    (%eax),%eax
80102a75:	83 e0 06             	and    $0x6,%eax
80102a78:	83 f8 02             	cmp    $0x2,%eax
80102a7b:	75 e0                	jne    80102a5d <iderw+0xb2>
    sleep(b, &idelock);
  }

  release(&idelock);
80102a7d:	83 ec 0c             	sub    $0xc,%esp
80102a80:	68 20 d6 10 80       	push   $0x8010d620
80102a85:	e8 12 40 00 00       	call   80106a9c <release>
80102a8a:	83 c4 10             	add    $0x10,%esp
}
80102a8d:	90                   	nop
80102a8e:	c9                   	leave  
80102a8f:	c3                   	ret    

80102a90 <ioapicread>:
  uint data;
};

static uint
ioapicread(int reg)
{
80102a90:	55                   	push   %ebp
80102a91:	89 e5                	mov    %esp,%ebp
  ioapic->reg = reg;
80102a93:	a1 34 42 11 80       	mov    0x80114234,%eax
80102a98:	8b 55 08             	mov    0x8(%ebp),%edx
80102a9b:	89 10                	mov    %edx,(%eax)
  return ioapic->data;
80102a9d:	a1 34 42 11 80       	mov    0x80114234,%eax
80102aa2:	8b 40 10             	mov    0x10(%eax),%eax
}
80102aa5:	5d                   	pop    %ebp
80102aa6:	c3                   	ret    

80102aa7 <ioapicwrite>:

static void
ioapicwrite(int reg, uint data)
{
80102aa7:	55                   	push   %ebp
80102aa8:	89 e5                	mov    %esp,%ebp
  ioapic->reg = reg;
80102aaa:	a1 34 42 11 80       	mov    0x80114234,%eax
80102aaf:	8b 55 08             	mov    0x8(%ebp),%edx
80102ab2:	89 10                	mov    %edx,(%eax)
  ioapic->data = data;
80102ab4:	a1 34 42 11 80       	mov    0x80114234,%eax
80102ab9:	8b 55 0c             	mov    0xc(%ebp),%edx
80102abc:	89 50 10             	mov    %edx,0x10(%eax)
}
80102abf:	90                   	nop
80102ac0:	5d                   	pop    %ebp
80102ac1:	c3                   	ret    

80102ac2 <ioapicinit>:

void
ioapicinit(void)
{
80102ac2:	55                   	push   %ebp
80102ac3:	89 e5                	mov    %esp,%ebp
80102ac5:	83 ec 18             	sub    $0x18,%esp
  int i, id, maxintr;

  if(!ismp)
80102ac8:	a1 64 43 11 80       	mov    0x80114364,%eax
80102acd:	85 c0                	test   %eax,%eax
80102acf:	0f 84 a0 00 00 00    	je     80102b75 <ioapicinit+0xb3>
    return;

  ioapic = (volatile struct ioapic*)IOAPIC;
80102ad5:	c7 05 34 42 11 80 00 	movl   $0xfec00000,0x80114234
80102adc:	00 c0 fe 
  maxintr = (ioapicread(REG_VER) >> 16) & 0xFF;
80102adf:	6a 01                	push   $0x1
80102ae1:	e8 aa ff ff ff       	call   80102a90 <ioapicread>
80102ae6:	83 c4 04             	add    $0x4,%esp
80102ae9:	c1 e8 10             	shr    $0x10,%eax
80102aec:	25 ff 00 00 00       	and    $0xff,%eax
80102af1:	89 45 f0             	mov    %eax,-0x10(%ebp)
  id = ioapicread(REG_ID) >> 24;
80102af4:	6a 00                	push   $0x0
80102af6:	e8 95 ff ff ff       	call   80102a90 <ioapicread>
80102afb:	83 c4 04             	add    $0x4,%esp
80102afe:	c1 e8 18             	shr    $0x18,%eax
80102b01:	89 45 ec             	mov    %eax,-0x14(%ebp)
  if(id != ioapicid)
80102b04:	0f b6 05 60 43 11 80 	movzbl 0x80114360,%eax
80102b0b:	0f b6 c0             	movzbl %al,%eax
80102b0e:	3b 45 ec             	cmp    -0x14(%ebp),%eax
80102b11:	74 10                	je     80102b23 <ioapicinit+0x61>
    cprintf("ioapicinit: id isn't equal to ioapicid; not a MP\n");
80102b13:	83 ec 0c             	sub    $0xc,%esp
80102b16:	68 00 a3 10 80       	push   $0x8010a300
80102b1b:	e8 a6 d8 ff ff       	call   801003c6 <cprintf>
80102b20:	83 c4 10             	add    $0x10,%esp

  // Mark all interrupts edge-triggered, active high, disabled,
  // and not routed to any CPUs.
  for(i = 0; i <= maxintr; i++){
80102b23:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80102b2a:	eb 3f                	jmp    80102b6b <ioapicinit+0xa9>
    ioapicwrite(REG_TABLE+2*i, INT_DISABLED | (T_IRQ0 + i));
80102b2c:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102b2f:	83 c0 20             	add    $0x20,%eax
80102b32:	0d 00 00 01 00       	or     $0x10000,%eax
80102b37:	89 c2                	mov    %eax,%edx
80102b39:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102b3c:	83 c0 08             	add    $0x8,%eax
80102b3f:	01 c0                	add    %eax,%eax
80102b41:	83 ec 08             	sub    $0x8,%esp
80102b44:	52                   	push   %edx
80102b45:	50                   	push   %eax
80102b46:	e8 5c ff ff ff       	call   80102aa7 <ioapicwrite>
80102b4b:	83 c4 10             	add    $0x10,%esp
    ioapicwrite(REG_TABLE+2*i+1, 0);
80102b4e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102b51:	83 c0 08             	add    $0x8,%eax
80102b54:	01 c0                	add    %eax,%eax
80102b56:	83 c0 01             	add    $0x1,%eax
80102b59:	83 ec 08             	sub    $0x8,%esp
80102b5c:	6a 00                	push   $0x0
80102b5e:	50                   	push   %eax
80102b5f:	e8 43 ff ff ff       	call   80102aa7 <ioapicwrite>
80102b64:	83 c4 10             	add    $0x10,%esp
  if(id != ioapicid)
    cprintf("ioapicinit: id isn't equal to ioapicid; not a MP\n");

  // Mark all interrupts edge-triggered, active high, disabled,
  // and not routed to any CPUs.
  for(i = 0; i <= maxintr; i++){
80102b67:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80102b6b:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102b6e:	3b 45 f0             	cmp    -0x10(%ebp),%eax
80102b71:	7e b9                	jle    80102b2c <ioapicinit+0x6a>
80102b73:	eb 01                	jmp    80102b76 <ioapicinit+0xb4>
ioapicinit(void)
{
  int i, id, maxintr;

  if(!ismp)
    return;
80102b75:	90                   	nop
  // and not routed to any CPUs.
  for(i = 0; i <= maxintr; i++){
    ioapicwrite(REG_TABLE+2*i, INT_DISABLED | (T_IRQ0 + i));
    ioapicwrite(REG_TABLE+2*i+1, 0);
  }
}
80102b76:	c9                   	leave  
80102b77:	c3                   	ret    

80102b78 <ioapicenable>:

void
ioapicenable(int irq, int cpunum)
{
80102b78:	55                   	push   %ebp
80102b79:	89 e5                	mov    %esp,%ebp
  if(!ismp)
80102b7b:	a1 64 43 11 80       	mov    0x80114364,%eax
80102b80:	85 c0                	test   %eax,%eax
80102b82:	74 39                	je     80102bbd <ioapicenable+0x45>
    return;

  // Mark interrupt edge-triggered, active high,
  // enabled, and routed to the given cpunum,
  // which happens to be that cpu's APIC ID.
  ioapicwrite(REG_TABLE+2*irq, T_IRQ0 + irq);
80102b84:	8b 45 08             	mov    0x8(%ebp),%eax
80102b87:	83 c0 20             	add    $0x20,%eax
80102b8a:	89 c2                	mov    %eax,%edx
80102b8c:	8b 45 08             	mov    0x8(%ebp),%eax
80102b8f:	83 c0 08             	add    $0x8,%eax
80102b92:	01 c0                	add    %eax,%eax
80102b94:	52                   	push   %edx
80102b95:	50                   	push   %eax
80102b96:	e8 0c ff ff ff       	call   80102aa7 <ioapicwrite>
80102b9b:	83 c4 08             	add    $0x8,%esp
  ioapicwrite(REG_TABLE+2*irq+1, cpunum << 24);
80102b9e:	8b 45 0c             	mov    0xc(%ebp),%eax
80102ba1:	c1 e0 18             	shl    $0x18,%eax
80102ba4:	89 c2                	mov    %eax,%edx
80102ba6:	8b 45 08             	mov    0x8(%ebp),%eax
80102ba9:	83 c0 08             	add    $0x8,%eax
80102bac:	01 c0                	add    %eax,%eax
80102bae:	83 c0 01             	add    $0x1,%eax
80102bb1:	52                   	push   %edx
80102bb2:	50                   	push   %eax
80102bb3:	e8 ef fe ff ff       	call   80102aa7 <ioapicwrite>
80102bb8:	83 c4 08             	add    $0x8,%esp
80102bbb:	eb 01                	jmp    80102bbe <ioapicenable+0x46>

void
ioapicenable(int irq, int cpunum)
{
  if(!ismp)
    return;
80102bbd:	90                   	nop
  // Mark interrupt edge-triggered, active high,
  // enabled, and routed to the given cpunum,
  // which happens to be that cpu's APIC ID.
  ioapicwrite(REG_TABLE+2*irq, T_IRQ0 + irq);
  ioapicwrite(REG_TABLE+2*irq+1, cpunum << 24);
}
80102bbe:	c9                   	leave  
80102bbf:	c3                   	ret    

80102bc0 <v2p>:
#define KERNBASE 0x80000000         // First kernel virtual address
#define KERNLINK (KERNBASE+EXTMEM)  // Address where kernel is linked

#ifndef __ASSEMBLER__

static inline uint v2p(void *a) { return ((uint) (a))  - KERNBASE; }
80102bc0:	55                   	push   %ebp
80102bc1:	89 e5                	mov    %esp,%ebp
80102bc3:	8b 45 08             	mov    0x8(%ebp),%eax
80102bc6:	05 00 00 00 80       	add    $0x80000000,%eax
80102bcb:	5d                   	pop    %ebp
80102bcc:	c3                   	ret    

80102bcd <kinit1>:
// the pages mapped by entrypgdir on free list.
// 2. main() calls kinit2() with the rest of the physical pages
// after installing a full page table that maps them on all cores.
void
kinit1(void *vstart, void *vend)
{
80102bcd:	55                   	push   %ebp
80102bce:	89 e5                	mov    %esp,%ebp
80102bd0:	83 ec 08             	sub    $0x8,%esp
  initlock(&kmem.lock, "kmem");
80102bd3:	83 ec 08             	sub    $0x8,%esp
80102bd6:	68 32 a3 10 80       	push   $0x8010a332
80102bdb:	68 40 42 11 80       	push   $0x80114240
80102be0:	e8 2e 3e 00 00       	call   80106a13 <initlock>
80102be5:	83 c4 10             	add    $0x10,%esp
  kmem.use_lock = 0;
80102be8:	c7 05 74 42 11 80 00 	movl   $0x0,0x80114274
80102bef:	00 00 00 
  freerange(vstart, vend);
80102bf2:	83 ec 08             	sub    $0x8,%esp
80102bf5:	ff 75 0c             	pushl  0xc(%ebp)
80102bf8:	ff 75 08             	pushl  0x8(%ebp)
80102bfb:	e8 2a 00 00 00       	call   80102c2a <freerange>
80102c00:	83 c4 10             	add    $0x10,%esp
}
80102c03:	90                   	nop
80102c04:	c9                   	leave  
80102c05:	c3                   	ret    

80102c06 <kinit2>:

void
kinit2(void *vstart, void *vend)
{
80102c06:	55                   	push   %ebp
80102c07:	89 e5                	mov    %esp,%ebp
80102c09:	83 ec 08             	sub    $0x8,%esp
  freerange(vstart, vend);
80102c0c:	83 ec 08             	sub    $0x8,%esp
80102c0f:	ff 75 0c             	pushl  0xc(%ebp)
80102c12:	ff 75 08             	pushl  0x8(%ebp)
80102c15:	e8 10 00 00 00       	call   80102c2a <freerange>
80102c1a:	83 c4 10             	add    $0x10,%esp
  kmem.use_lock = 1;
80102c1d:	c7 05 74 42 11 80 01 	movl   $0x1,0x80114274
80102c24:	00 00 00 
}
80102c27:	90                   	nop
80102c28:	c9                   	leave  
80102c29:	c3                   	ret    

80102c2a <freerange>:

void
freerange(void *vstart, void *vend)
{
80102c2a:	55                   	push   %ebp
80102c2b:	89 e5                	mov    %esp,%ebp
80102c2d:	83 ec 18             	sub    $0x18,%esp
  char *p;
  p = (char*)PGROUNDUP((uint)vstart);
80102c30:	8b 45 08             	mov    0x8(%ebp),%eax
80102c33:	05 ff 0f 00 00       	add    $0xfff,%eax
80102c38:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80102c3d:	89 45 f4             	mov    %eax,-0xc(%ebp)
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
80102c40:	eb 15                	jmp    80102c57 <freerange+0x2d>
    kfree(p);
80102c42:	83 ec 0c             	sub    $0xc,%esp
80102c45:	ff 75 f4             	pushl  -0xc(%ebp)
80102c48:	e8 1a 00 00 00       	call   80102c67 <kfree>
80102c4d:	83 c4 10             	add    $0x10,%esp
void
freerange(void *vstart, void *vend)
{
  char *p;
  p = (char*)PGROUNDUP((uint)vstart);
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
80102c50:	81 45 f4 00 10 00 00 	addl   $0x1000,-0xc(%ebp)
80102c57:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102c5a:	05 00 10 00 00       	add    $0x1000,%eax
80102c5f:	3b 45 0c             	cmp    0xc(%ebp),%eax
80102c62:	76 de                	jbe    80102c42 <freerange+0x18>
    kfree(p);
}
80102c64:	90                   	nop
80102c65:	c9                   	leave  
80102c66:	c3                   	ret    

80102c67 <kfree>:
// which normally should have been returned by a
// call to kalloc().  (The exception is when
// initializing the allocator; see kinit above.)
void
kfree(char *v)
{
80102c67:	55                   	push   %ebp
80102c68:	89 e5                	mov    %esp,%ebp
80102c6a:	83 ec 18             	sub    $0x18,%esp
  struct run *r;

  if((uint)v % PGSIZE || v < end || v2p(v) >= PHYSTOP)
80102c6d:	8b 45 08             	mov    0x8(%ebp),%eax
80102c70:	25 ff 0f 00 00       	and    $0xfff,%eax
80102c75:	85 c0                	test   %eax,%eax
80102c77:	75 1b                	jne    80102c94 <kfree+0x2d>
80102c79:	81 7d 08 5c 79 11 80 	cmpl   $0x8011795c,0x8(%ebp)
80102c80:	72 12                	jb     80102c94 <kfree+0x2d>
80102c82:	ff 75 08             	pushl  0x8(%ebp)
80102c85:	e8 36 ff ff ff       	call   80102bc0 <v2p>
80102c8a:	83 c4 04             	add    $0x4,%esp
80102c8d:	3d ff ff ff 0d       	cmp    $0xdffffff,%eax
80102c92:	76 0d                	jbe    80102ca1 <kfree+0x3a>
    panic("kfree");
80102c94:	83 ec 0c             	sub    $0xc,%esp
80102c97:	68 37 a3 10 80       	push   $0x8010a337
80102c9c:	e8 c5 d8 ff ff       	call   80100566 <panic>

  // Fill with junk to catch dangling refs.
  memset(v, 1, PGSIZE);
80102ca1:	83 ec 04             	sub    $0x4,%esp
80102ca4:	68 00 10 00 00       	push   $0x1000
80102ca9:	6a 01                	push   $0x1
80102cab:	ff 75 08             	pushl  0x8(%ebp)
80102cae:	e8 e5 3f 00 00       	call   80106c98 <memset>
80102cb3:	83 c4 10             	add    $0x10,%esp

  if(kmem.use_lock)
80102cb6:	a1 74 42 11 80       	mov    0x80114274,%eax
80102cbb:	85 c0                	test   %eax,%eax
80102cbd:	74 10                	je     80102ccf <kfree+0x68>
    acquire(&kmem.lock);
80102cbf:	83 ec 0c             	sub    $0xc,%esp
80102cc2:	68 40 42 11 80       	push   $0x80114240
80102cc7:	e8 69 3d 00 00       	call   80106a35 <acquire>
80102ccc:	83 c4 10             	add    $0x10,%esp
  r = (struct run*)v;
80102ccf:	8b 45 08             	mov    0x8(%ebp),%eax
80102cd2:	89 45 f4             	mov    %eax,-0xc(%ebp)
  r->next = kmem.freelist;
80102cd5:	8b 15 78 42 11 80    	mov    0x80114278,%edx
80102cdb:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102cde:	89 10                	mov    %edx,(%eax)
  kmem.freelist = r;
80102ce0:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102ce3:	a3 78 42 11 80       	mov    %eax,0x80114278
  if(kmem.use_lock)
80102ce8:	a1 74 42 11 80       	mov    0x80114274,%eax
80102ced:	85 c0                	test   %eax,%eax
80102cef:	74 10                	je     80102d01 <kfree+0x9a>
    release(&kmem.lock);
80102cf1:	83 ec 0c             	sub    $0xc,%esp
80102cf4:	68 40 42 11 80       	push   $0x80114240
80102cf9:	e8 9e 3d 00 00       	call   80106a9c <release>
80102cfe:	83 c4 10             	add    $0x10,%esp
}
80102d01:	90                   	nop
80102d02:	c9                   	leave  
80102d03:	c3                   	ret    

80102d04 <kalloc>:
// Allocate one 4096-byte page of physical memory.
// Returns a pointer that the kernel can use.
// Returns 0 if the memory cannot be allocated.
char*
kalloc(void)
{
80102d04:	55                   	push   %ebp
80102d05:	89 e5                	mov    %esp,%ebp
80102d07:	83 ec 18             	sub    $0x18,%esp
  struct run *r;

  if(kmem.use_lock)
80102d0a:	a1 74 42 11 80       	mov    0x80114274,%eax
80102d0f:	85 c0                	test   %eax,%eax
80102d11:	74 10                	je     80102d23 <kalloc+0x1f>
    acquire(&kmem.lock);
80102d13:	83 ec 0c             	sub    $0xc,%esp
80102d16:	68 40 42 11 80       	push   $0x80114240
80102d1b:	e8 15 3d 00 00       	call   80106a35 <acquire>
80102d20:	83 c4 10             	add    $0x10,%esp
  r = kmem.freelist;
80102d23:	a1 78 42 11 80       	mov    0x80114278,%eax
80102d28:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(r)
80102d2b:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80102d2f:	74 0a                	je     80102d3b <kalloc+0x37>
    kmem.freelist = r->next;
80102d31:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102d34:	8b 00                	mov    (%eax),%eax
80102d36:	a3 78 42 11 80       	mov    %eax,0x80114278
  if(kmem.use_lock)
80102d3b:	a1 74 42 11 80       	mov    0x80114274,%eax
80102d40:	85 c0                	test   %eax,%eax
80102d42:	74 10                	je     80102d54 <kalloc+0x50>
    release(&kmem.lock);
80102d44:	83 ec 0c             	sub    $0xc,%esp
80102d47:	68 40 42 11 80       	push   $0x80114240
80102d4c:	e8 4b 3d 00 00       	call   80106a9c <release>
80102d51:	83 c4 10             	add    $0x10,%esp
  return (char*)r;
80102d54:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
80102d57:	c9                   	leave  
80102d58:	c3                   	ret    

80102d59 <inb>:

// end of CS333 added routines

static inline uchar
inb(ushort port)
{
80102d59:	55                   	push   %ebp
80102d5a:	89 e5                	mov    %esp,%ebp
80102d5c:	83 ec 14             	sub    $0x14,%esp
80102d5f:	8b 45 08             	mov    0x8(%ebp),%eax
80102d62:	66 89 45 ec          	mov    %ax,-0x14(%ebp)
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102d66:	0f b7 45 ec          	movzwl -0x14(%ebp),%eax
80102d6a:	89 c2                	mov    %eax,%edx
80102d6c:	ec                   	in     (%dx),%al
80102d6d:	88 45 ff             	mov    %al,-0x1(%ebp)
  return data;
80102d70:	0f b6 45 ff          	movzbl -0x1(%ebp),%eax
}
80102d74:	c9                   	leave  
80102d75:	c3                   	ret    

80102d76 <kbdgetc>:
#include "defs.h"
#include "kbd.h"

int
kbdgetc(void)
{
80102d76:	55                   	push   %ebp
80102d77:	89 e5                	mov    %esp,%ebp
80102d79:	83 ec 10             	sub    $0x10,%esp
  static uchar *charcode[4] = {
    normalmap, shiftmap, ctlmap, ctlmap
  };
  uint st, data, c;

  st = inb(KBSTATP);
80102d7c:	6a 64                	push   $0x64
80102d7e:	e8 d6 ff ff ff       	call   80102d59 <inb>
80102d83:	83 c4 04             	add    $0x4,%esp
80102d86:	0f b6 c0             	movzbl %al,%eax
80102d89:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if((st & KBS_DIB) == 0)
80102d8c:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102d8f:	83 e0 01             	and    $0x1,%eax
80102d92:	85 c0                	test   %eax,%eax
80102d94:	75 0a                	jne    80102da0 <kbdgetc+0x2a>
    return -1;
80102d96:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80102d9b:	e9 23 01 00 00       	jmp    80102ec3 <kbdgetc+0x14d>
  data = inb(KBDATAP);
80102da0:	6a 60                	push   $0x60
80102da2:	e8 b2 ff ff ff       	call   80102d59 <inb>
80102da7:	83 c4 04             	add    $0x4,%esp
80102daa:	0f b6 c0             	movzbl %al,%eax
80102dad:	89 45 fc             	mov    %eax,-0x4(%ebp)

  if(data == 0xE0){
80102db0:	81 7d fc e0 00 00 00 	cmpl   $0xe0,-0x4(%ebp)
80102db7:	75 17                	jne    80102dd0 <kbdgetc+0x5a>
    shift |= E0ESC;
80102db9:	a1 5c d6 10 80       	mov    0x8010d65c,%eax
80102dbe:	83 c8 40             	or     $0x40,%eax
80102dc1:	a3 5c d6 10 80       	mov    %eax,0x8010d65c
    return 0;
80102dc6:	b8 00 00 00 00       	mov    $0x0,%eax
80102dcb:	e9 f3 00 00 00       	jmp    80102ec3 <kbdgetc+0x14d>
  } else if(data & 0x80){
80102dd0:	8b 45 fc             	mov    -0x4(%ebp),%eax
80102dd3:	25 80 00 00 00       	and    $0x80,%eax
80102dd8:	85 c0                	test   %eax,%eax
80102dda:	74 45                	je     80102e21 <kbdgetc+0xab>
    // Key released
    data = (shift & E0ESC ? data : data & 0x7F);
80102ddc:	a1 5c d6 10 80       	mov    0x8010d65c,%eax
80102de1:	83 e0 40             	and    $0x40,%eax
80102de4:	85 c0                	test   %eax,%eax
80102de6:	75 08                	jne    80102df0 <kbdgetc+0x7a>
80102de8:	8b 45 fc             	mov    -0x4(%ebp),%eax
80102deb:	83 e0 7f             	and    $0x7f,%eax
80102dee:	eb 03                	jmp    80102df3 <kbdgetc+0x7d>
80102df0:	8b 45 fc             	mov    -0x4(%ebp),%eax
80102df3:	89 45 fc             	mov    %eax,-0x4(%ebp)
    shift &= ~(shiftcode[data] | E0ESC);
80102df6:	8b 45 fc             	mov    -0x4(%ebp),%eax
80102df9:	05 20 b0 10 80       	add    $0x8010b020,%eax
80102dfe:	0f b6 00             	movzbl (%eax),%eax
80102e01:	83 c8 40             	or     $0x40,%eax
80102e04:	0f b6 c0             	movzbl %al,%eax
80102e07:	f7 d0                	not    %eax
80102e09:	89 c2                	mov    %eax,%edx
80102e0b:	a1 5c d6 10 80       	mov    0x8010d65c,%eax
80102e10:	21 d0                	and    %edx,%eax
80102e12:	a3 5c d6 10 80       	mov    %eax,0x8010d65c
    return 0;
80102e17:	b8 00 00 00 00       	mov    $0x0,%eax
80102e1c:	e9 a2 00 00 00       	jmp    80102ec3 <kbdgetc+0x14d>
  } else if(shift & E0ESC){
80102e21:	a1 5c d6 10 80       	mov    0x8010d65c,%eax
80102e26:	83 e0 40             	and    $0x40,%eax
80102e29:	85 c0                	test   %eax,%eax
80102e2b:	74 14                	je     80102e41 <kbdgetc+0xcb>
    // Last character was an E0 escape; or with 0x80
    data |= 0x80;
80102e2d:	81 4d fc 80 00 00 00 	orl    $0x80,-0x4(%ebp)
    shift &= ~E0ESC;
80102e34:	a1 5c d6 10 80       	mov    0x8010d65c,%eax
80102e39:	83 e0 bf             	and    $0xffffffbf,%eax
80102e3c:	a3 5c d6 10 80       	mov    %eax,0x8010d65c
  }

  shift |= shiftcode[data];
80102e41:	8b 45 fc             	mov    -0x4(%ebp),%eax
80102e44:	05 20 b0 10 80       	add    $0x8010b020,%eax
80102e49:	0f b6 00             	movzbl (%eax),%eax
80102e4c:	0f b6 d0             	movzbl %al,%edx
80102e4f:	a1 5c d6 10 80       	mov    0x8010d65c,%eax
80102e54:	09 d0                	or     %edx,%eax
80102e56:	a3 5c d6 10 80       	mov    %eax,0x8010d65c
  shift ^= togglecode[data];
80102e5b:	8b 45 fc             	mov    -0x4(%ebp),%eax
80102e5e:	05 20 b1 10 80       	add    $0x8010b120,%eax
80102e63:	0f b6 00             	movzbl (%eax),%eax
80102e66:	0f b6 d0             	movzbl %al,%edx
80102e69:	a1 5c d6 10 80       	mov    0x8010d65c,%eax
80102e6e:	31 d0                	xor    %edx,%eax
80102e70:	a3 5c d6 10 80       	mov    %eax,0x8010d65c
  c = charcode[shift & (CTL | SHIFT)][data];
80102e75:	a1 5c d6 10 80       	mov    0x8010d65c,%eax
80102e7a:	83 e0 03             	and    $0x3,%eax
80102e7d:	8b 14 85 20 b5 10 80 	mov    -0x7fef4ae0(,%eax,4),%edx
80102e84:	8b 45 fc             	mov    -0x4(%ebp),%eax
80102e87:	01 d0                	add    %edx,%eax
80102e89:	0f b6 00             	movzbl (%eax),%eax
80102e8c:	0f b6 c0             	movzbl %al,%eax
80102e8f:	89 45 f8             	mov    %eax,-0x8(%ebp)
  if(shift & CAPSLOCK){
80102e92:	a1 5c d6 10 80       	mov    0x8010d65c,%eax
80102e97:	83 e0 08             	and    $0x8,%eax
80102e9a:	85 c0                	test   %eax,%eax
80102e9c:	74 22                	je     80102ec0 <kbdgetc+0x14a>
    if('a' <= c && c <= 'z')
80102e9e:	83 7d f8 60          	cmpl   $0x60,-0x8(%ebp)
80102ea2:	76 0c                	jbe    80102eb0 <kbdgetc+0x13a>
80102ea4:	83 7d f8 7a          	cmpl   $0x7a,-0x8(%ebp)
80102ea8:	77 06                	ja     80102eb0 <kbdgetc+0x13a>
      c += 'A' - 'a';
80102eaa:	83 6d f8 20          	subl   $0x20,-0x8(%ebp)
80102eae:	eb 10                	jmp    80102ec0 <kbdgetc+0x14a>
    else if('A' <= c && c <= 'Z')
80102eb0:	83 7d f8 40          	cmpl   $0x40,-0x8(%ebp)
80102eb4:	76 0a                	jbe    80102ec0 <kbdgetc+0x14a>
80102eb6:	83 7d f8 5a          	cmpl   $0x5a,-0x8(%ebp)
80102eba:	77 04                	ja     80102ec0 <kbdgetc+0x14a>
      c += 'a' - 'A';
80102ebc:	83 45 f8 20          	addl   $0x20,-0x8(%ebp)
  }
  return c;
80102ec0:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
80102ec3:	c9                   	leave  
80102ec4:	c3                   	ret    

80102ec5 <kbdintr>:

void
kbdintr(void)
{
80102ec5:	55                   	push   %ebp
80102ec6:	89 e5                	mov    %esp,%ebp
80102ec8:	83 ec 08             	sub    $0x8,%esp
  consoleintr(kbdgetc);
80102ecb:	83 ec 0c             	sub    $0xc,%esp
80102ece:	68 76 2d 10 80       	push   $0x80102d76
80102ed3:	e8 21 d9 ff ff       	call   801007f9 <consoleintr>
80102ed8:	83 c4 10             	add    $0x10,%esp
}
80102edb:	90                   	nop
80102edc:	c9                   	leave  
80102edd:	c3                   	ret    

80102ede <inb>:

// end of CS333 added routines

static inline uchar
inb(ushort port)
{
80102ede:	55                   	push   %ebp
80102edf:	89 e5                	mov    %esp,%ebp
80102ee1:	83 ec 14             	sub    $0x14,%esp
80102ee4:	8b 45 08             	mov    0x8(%ebp),%eax
80102ee7:	66 89 45 ec          	mov    %ax,-0x14(%ebp)
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102eeb:	0f b7 45 ec          	movzwl -0x14(%ebp),%eax
80102eef:	89 c2                	mov    %eax,%edx
80102ef1:	ec                   	in     (%dx),%al
80102ef2:	88 45 ff             	mov    %al,-0x1(%ebp)
  return data;
80102ef5:	0f b6 45 ff          	movzbl -0x1(%ebp),%eax
}
80102ef9:	c9                   	leave  
80102efa:	c3                   	ret    

80102efb <outb>:
               "memory", "cc");
}

static inline void
outb(ushort port, uchar data)
{
80102efb:	55                   	push   %ebp
80102efc:	89 e5                	mov    %esp,%ebp
80102efe:	83 ec 08             	sub    $0x8,%esp
80102f01:	8b 55 08             	mov    0x8(%ebp),%edx
80102f04:	8b 45 0c             	mov    0xc(%ebp),%eax
80102f07:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
80102f0b:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102f0e:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
80102f12:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
80102f16:	ee                   	out    %al,(%dx)
}
80102f17:	90                   	nop
80102f18:	c9                   	leave  
80102f19:	c3                   	ret    

80102f1a <readeflags>:
  asm volatile("ltr %0" : : "r" (sel));
}

static inline uint
readeflags(void)
{
80102f1a:	55                   	push   %ebp
80102f1b:	89 e5                	mov    %esp,%ebp
80102f1d:	83 ec 10             	sub    $0x10,%esp
  uint eflags;
  asm volatile("pushfl; popl %0" : "=r" (eflags));
80102f20:	9c                   	pushf  
80102f21:	58                   	pop    %eax
80102f22:	89 45 fc             	mov    %eax,-0x4(%ebp)
  return eflags;
80102f25:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80102f28:	c9                   	leave  
80102f29:	c3                   	ret    

80102f2a <lapicw>:

volatile uint *lapic;  // Initialized in mp.c

static void
lapicw(int index, int value)
{
80102f2a:	55                   	push   %ebp
80102f2b:	89 e5                	mov    %esp,%ebp
  lapic[index] = value;
80102f2d:	a1 7c 42 11 80       	mov    0x8011427c,%eax
80102f32:	8b 55 08             	mov    0x8(%ebp),%edx
80102f35:	c1 e2 02             	shl    $0x2,%edx
80102f38:	01 c2                	add    %eax,%edx
80102f3a:	8b 45 0c             	mov    0xc(%ebp),%eax
80102f3d:	89 02                	mov    %eax,(%edx)
  lapic[ID];  // wait for write to finish, by reading
80102f3f:	a1 7c 42 11 80       	mov    0x8011427c,%eax
80102f44:	83 c0 20             	add    $0x20,%eax
80102f47:	8b 00                	mov    (%eax),%eax
}
80102f49:	90                   	nop
80102f4a:	5d                   	pop    %ebp
80102f4b:	c3                   	ret    

80102f4c <lapicinit>:

void
lapicinit(void)
{
80102f4c:	55                   	push   %ebp
80102f4d:	89 e5                	mov    %esp,%ebp
  if(!lapic) 
80102f4f:	a1 7c 42 11 80       	mov    0x8011427c,%eax
80102f54:	85 c0                	test   %eax,%eax
80102f56:	0f 84 0b 01 00 00    	je     80103067 <lapicinit+0x11b>
    return;

  // Enable local APIC; set spurious interrupt vector.
  lapicw(SVR, ENABLE | (T_IRQ0 + IRQ_SPURIOUS));
80102f5c:	68 3f 01 00 00       	push   $0x13f
80102f61:	6a 3c                	push   $0x3c
80102f63:	e8 c2 ff ff ff       	call   80102f2a <lapicw>
80102f68:	83 c4 08             	add    $0x8,%esp

  // The timer repeatedly counts down at bus frequency
  // from lapic[TICR] and then issues an interrupt.  
  // If xv6 cared more about precise timekeeping,
  // TICR would be calibrated using an external time source.
  lapicw(TDCR, X1);
80102f6b:	6a 0b                	push   $0xb
80102f6d:	68 f8 00 00 00       	push   $0xf8
80102f72:	e8 b3 ff ff ff       	call   80102f2a <lapicw>
80102f77:	83 c4 08             	add    $0x8,%esp
  lapicw(TIMER, PERIODIC | (T_IRQ0 + IRQ_TIMER));
80102f7a:	68 20 00 02 00       	push   $0x20020
80102f7f:	68 c8 00 00 00       	push   $0xc8
80102f84:	e8 a1 ff ff ff       	call   80102f2a <lapicw>
80102f89:	83 c4 08             	add    $0x8,%esp
  lapicw(TICR, 10000000); 
80102f8c:	68 80 96 98 00       	push   $0x989680
80102f91:	68 e0 00 00 00       	push   $0xe0
80102f96:	e8 8f ff ff ff       	call   80102f2a <lapicw>
80102f9b:	83 c4 08             	add    $0x8,%esp

  // Disable logical interrupt lines.
  lapicw(LINT0, MASKED);
80102f9e:	68 00 00 01 00       	push   $0x10000
80102fa3:	68 d4 00 00 00       	push   $0xd4
80102fa8:	e8 7d ff ff ff       	call   80102f2a <lapicw>
80102fad:	83 c4 08             	add    $0x8,%esp
  lapicw(LINT1, MASKED);
80102fb0:	68 00 00 01 00       	push   $0x10000
80102fb5:	68 d8 00 00 00       	push   $0xd8
80102fba:	e8 6b ff ff ff       	call   80102f2a <lapicw>
80102fbf:	83 c4 08             	add    $0x8,%esp

  // Disable performance counter overflow interrupts
  // on machines that provide that interrupt entry.
  if(((lapic[VER]>>16) & 0xFF) >= 4)
80102fc2:	a1 7c 42 11 80       	mov    0x8011427c,%eax
80102fc7:	83 c0 30             	add    $0x30,%eax
80102fca:	8b 00                	mov    (%eax),%eax
80102fcc:	c1 e8 10             	shr    $0x10,%eax
80102fcf:	0f b6 c0             	movzbl %al,%eax
80102fd2:	83 f8 03             	cmp    $0x3,%eax
80102fd5:	76 12                	jbe    80102fe9 <lapicinit+0x9d>
    lapicw(PCINT, MASKED);
80102fd7:	68 00 00 01 00       	push   $0x10000
80102fdc:	68 d0 00 00 00       	push   $0xd0
80102fe1:	e8 44 ff ff ff       	call   80102f2a <lapicw>
80102fe6:	83 c4 08             	add    $0x8,%esp

  // Map error interrupt to IRQ_ERROR.
  lapicw(ERROR, T_IRQ0 + IRQ_ERROR);
80102fe9:	6a 33                	push   $0x33
80102feb:	68 dc 00 00 00       	push   $0xdc
80102ff0:	e8 35 ff ff ff       	call   80102f2a <lapicw>
80102ff5:	83 c4 08             	add    $0x8,%esp

  // Clear error status register (requires back-to-back writes).
  lapicw(ESR, 0);
80102ff8:	6a 00                	push   $0x0
80102ffa:	68 a0 00 00 00       	push   $0xa0
80102fff:	e8 26 ff ff ff       	call   80102f2a <lapicw>
80103004:	83 c4 08             	add    $0x8,%esp
  lapicw(ESR, 0);
80103007:	6a 00                	push   $0x0
80103009:	68 a0 00 00 00       	push   $0xa0
8010300e:	e8 17 ff ff ff       	call   80102f2a <lapicw>
80103013:	83 c4 08             	add    $0x8,%esp

  // Ack any outstanding interrupts.
  lapicw(EOI, 0);
80103016:	6a 00                	push   $0x0
80103018:	6a 2c                	push   $0x2c
8010301a:	e8 0b ff ff ff       	call   80102f2a <lapicw>
8010301f:	83 c4 08             	add    $0x8,%esp

  // Send an Init Level De-Assert to synchronise arbitration ID's.
  lapicw(ICRHI, 0);
80103022:	6a 00                	push   $0x0
80103024:	68 c4 00 00 00       	push   $0xc4
80103029:	e8 fc fe ff ff       	call   80102f2a <lapicw>
8010302e:	83 c4 08             	add    $0x8,%esp
  lapicw(ICRLO, BCAST | INIT | LEVEL);
80103031:	68 00 85 08 00       	push   $0x88500
80103036:	68 c0 00 00 00       	push   $0xc0
8010303b:	e8 ea fe ff ff       	call   80102f2a <lapicw>
80103040:	83 c4 08             	add    $0x8,%esp
  while(lapic[ICRLO] & DELIVS)
80103043:	90                   	nop
80103044:	a1 7c 42 11 80       	mov    0x8011427c,%eax
80103049:	05 00 03 00 00       	add    $0x300,%eax
8010304e:	8b 00                	mov    (%eax),%eax
80103050:	25 00 10 00 00       	and    $0x1000,%eax
80103055:	85 c0                	test   %eax,%eax
80103057:	75 eb                	jne    80103044 <lapicinit+0xf8>
    ;

  // Enable interrupts on the APIC (but not on the processor).
  lapicw(TPR, 0);
80103059:	6a 00                	push   $0x0
8010305b:	6a 20                	push   $0x20
8010305d:	e8 c8 fe ff ff       	call   80102f2a <lapicw>
80103062:	83 c4 08             	add    $0x8,%esp
80103065:	eb 01                	jmp    80103068 <lapicinit+0x11c>

void
lapicinit(void)
{
  if(!lapic) 
    return;
80103067:	90                   	nop
  while(lapic[ICRLO] & DELIVS)
    ;

  // Enable interrupts on the APIC (but not on the processor).
  lapicw(TPR, 0);
}
80103068:	c9                   	leave  
80103069:	c3                   	ret    

8010306a <cpunum>:

int
cpunum(void)
{
8010306a:	55                   	push   %ebp
8010306b:	89 e5                	mov    %esp,%ebp
8010306d:	83 ec 08             	sub    $0x8,%esp
  // Cannot call cpu when interrupts are enabled:
  // result not guaranteed to last long enough to be used!
  // Would prefer to panic but even printing is chancy here:
  // almost everything, including cprintf and panic, calls cpu,
  // often indirectly through acquire and release.
  if(readeflags()&FL_IF){
80103070:	e8 a5 fe ff ff       	call   80102f1a <readeflags>
80103075:	25 00 02 00 00       	and    $0x200,%eax
8010307a:	85 c0                	test   %eax,%eax
8010307c:	74 26                	je     801030a4 <cpunum+0x3a>
    static int n;
    if(n++ == 0)
8010307e:	a1 60 d6 10 80       	mov    0x8010d660,%eax
80103083:	8d 50 01             	lea    0x1(%eax),%edx
80103086:	89 15 60 d6 10 80    	mov    %edx,0x8010d660
8010308c:	85 c0                	test   %eax,%eax
8010308e:	75 14                	jne    801030a4 <cpunum+0x3a>
      cprintf("cpu called from %x with interrupts enabled\n",
80103090:	8b 45 04             	mov    0x4(%ebp),%eax
80103093:	83 ec 08             	sub    $0x8,%esp
80103096:	50                   	push   %eax
80103097:	68 40 a3 10 80       	push   $0x8010a340
8010309c:	e8 25 d3 ff ff       	call   801003c6 <cprintf>
801030a1:	83 c4 10             	add    $0x10,%esp
        __builtin_return_address(0));
  }

  if(lapic)
801030a4:	a1 7c 42 11 80       	mov    0x8011427c,%eax
801030a9:	85 c0                	test   %eax,%eax
801030ab:	74 0f                	je     801030bc <cpunum+0x52>
    return lapic[ID]>>24;
801030ad:	a1 7c 42 11 80       	mov    0x8011427c,%eax
801030b2:	83 c0 20             	add    $0x20,%eax
801030b5:	8b 00                	mov    (%eax),%eax
801030b7:	c1 e8 18             	shr    $0x18,%eax
801030ba:	eb 05                	jmp    801030c1 <cpunum+0x57>
  return 0;
801030bc:	b8 00 00 00 00       	mov    $0x0,%eax
}
801030c1:	c9                   	leave  
801030c2:	c3                   	ret    

801030c3 <lapiceoi>:

// Acknowledge interrupt.
void
lapiceoi(void)
{
801030c3:	55                   	push   %ebp
801030c4:	89 e5                	mov    %esp,%ebp
  if(lapic)
801030c6:	a1 7c 42 11 80       	mov    0x8011427c,%eax
801030cb:	85 c0                	test   %eax,%eax
801030cd:	74 0c                	je     801030db <lapiceoi+0x18>
    lapicw(EOI, 0);
801030cf:	6a 00                	push   $0x0
801030d1:	6a 2c                	push   $0x2c
801030d3:	e8 52 fe ff ff       	call   80102f2a <lapicw>
801030d8:	83 c4 08             	add    $0x8,%esp
}
801030db:	90                   	nop
801030dc:	c9                   	leave  
801030dd:	c3                   	ret    

801030de <microdelay>:

// Spin for a given number of microseconds.
// On real hardware would want to tune this dynamically.
void
microdelay(int us)
{
801030de:	55                   	push   %ebp
801030df:	89 e5                	mov    %esp,%ebp
}
801030e1:	90                   	nop
801030e2:	5d                   	pop    %ebp
801030e3:	c3                   	ret    

801030e4 <lapicstartap>:

// Start additional processor running entry code at addr.
// See Appendix B of MultiProcessor Specification.
void
lapicstartap(uchar apicid, uint addr)
{
801030e4:	55                   	push   %ebp
801030e5:	89 e5                	mov    %esp,%ebp
801030e7:	83 ec 14             	sub    $0x14,%esp
801030ea:	8b 45 08             	mov    0x8(%ebp),%eax
801030ed:	88 45 ec             	mov    %al,-0x14(%ebp)
  ushort *wrv;
  
  // "The BSP must initialize CMOS shutdown code to 0AH
  // and the warm reset vector (DWORD based at 40:67) to point at
  // the AP startup code prior to the [universal startup algorithm]."
  outb(CMOS_PORT, 0xF);  // offset 0xF is shutdown code
801030f0:	6a 0f                	push   $0xf
801030f2:	6a 70                	push   $0x70
801030f4:	e8 02 fe ff ff       	call   80102efb <outb>
801030f9:	83 c4 08             	add    $0x8,%esp
  outb(CMOS_PORT+1, 0x0A);
801030fc:	6a 0a                	push   $0xa
801030fe:	6a 71                	push   $0x71
80103100:	e8 f6 fd ff ff       	call   80102efb <outb>
80103105:	83 c4 08             	add    $0x8,%esp
  wrv = (ushort*)P2V((0x40<<4 | 0x67));  // Warm reset vector
80103108:	c7 45 f8 67 04 00 80 	movl   $0x80000467,-0x8(%ebp)
  wrv[0] = 0;
8010310f:	8b 45 f8             	mov    -0x8(%ebp),%eax
80103112:	66 c7 00 00 00       	movw   $0x0,(%eax)
  wrv[1] = addr >> 4;
80103117:	8b 45 f8             	mov    -0x8(%ebp),%eax
8010311a:	83 c0 02             	add    $0x2,%eax
8010311d:	8b 55 0c             	mov    0xc(%ebp),%edx
80103120:	c1 ea 04             	shr    $0x4,%edx
80103123:	66 89 10             	mov    %dx,(%eax)

  // "Universal startup algorithm."
  // Send INIT (level-triggered) interrupt to reset other CPU.
  lapicw(ICRHI, apicid<<24);
80103126:	0f b6 45 ec          	movzbl -0x14(%ebp),%eax
8010312a:	c1 e0 18             	shl    $0x18,%eax
8010312d:	50                   	push   %eax
8010312e:	68 c4 00 00 00       	push   $0xc4
80103133:	e8 f2 fd ff ff       	call   80102f2a <lapicw>
80103138:	83 c4 08             	add    $0x8,%esp
  lapicw(ICRLO, INIT | LEVEL | ASSERT);
8010313b:	68 00 c5 00 00       	push   $0xc500
80103140:	68 c0 00 00 00       	push   $0xc0
80103145:	e8 e0 fd ff ff       	call   80102f2a <lapicw>
8010314a:	83 c4 08             	add    $0x8,%esp
  microdelay(200);
8010314d:	68 c8 00 00 00       	push   $0xc8
80103152:	e8 87 ff ff ff       	call   801030de <microdelay>
80103157:	83 c4 04             	add    $0x4,%esp
  lapicw(ICRLO, INIT | LEVEL);
8010315a:	68 00 85 00 00       	push   $0x8500
8010315f:	68 c0 00 00 00       	push   $0xc0
80103164:	e8 c1 fd ff ff       	call   80102f2a <lapicw>
80103169:	83 c4 08             	add    $0x8,%esp
  microdelay(100);    // should be 10ms, but too slow in Bochs!
8010316c:	6a 64                	push   $0x64
8010316e:	e8 6b ff ff ff       	call   801030de <microdelay>
80103173:	83 c4 04             	add    $0x4,%esp
  // Send startup IPI (twice!) to enter code.
  // Regular hardware is supposed to only accept a STARTUP
  // when it is in the halted state due to an INIT.  So the second
  // should be ignored, but it is part of the official Intel algorithm.
  // Bochs complains about the second one.  Too bad for Bochs.
  for(i = 0; i < 2; i++){
80103176:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
8010317d:	eb 3d                	jmp    801031bc <lapicstartap+0xd8>
    lapicw(ICRHI, apicid<<24);
8010317f:	0f b6 45 ec          	movzbl -0x14(%ebp),%eax
80103183:	c1 e0 18             	shl    $0x18,%eax
80103186:	50                   	push   %eax
80103187:	68 c4 00 00 00       	push   $0xc4
8010318c:	e8 99 fd ff ff       	call   80102f2a <lapicw>
80103191:	83 c4 08             	add    $0x8,%esp
    lapicw(ICRLO, STARTUP | (addr>>12));
80103194:	8b 45 0c             	mov    0xc(%ebp),%eax
80103197:	c1 e8 0c             	shr    $0xc,%eax
8010319a:	80 cc 06             	or     $0x6,%ah
8010319d:	50                   	push   %eax
8010319e:	68 c0 00 00 00       	push   $0xc0
801031a3:	e8 82 fd ff ff       	call   80102f2a <lapicw>
801031a8:	83 c4 08             	add    $0x8,%esp
    microdelay(200);
801031ab:	68 c8 00 00 00       	push   $0xc8
801031b0:	e8 29 ff ff ff       	call   801030de <microdelay>
801031b5:	83 c4 04             	add    $0x4,%esp
  // Send startup IPI (twice!) to enter code.
  // Regular hardware is supposed to only accept a STARTUP
  // when it is in the halted state due to an INIT.  So the second
  // should be ignored, but it is part of the official Intel algorithm.
  // Bochs complains about the second one.  Too bad for Bochs.
  for(i = 0; i < 2; i++){
801031b8:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
801031bc:	83 7d fc 01          	cmpl   $0x1,-0x4(%ebp)
801031c0:	7e bd                	jle    8010317f <lapicstartap+0x9b>
    lapicw(ICRHI, apicid<<24);
    lapicw(ICRLO, STARTUP | (addr>>12));
    microdelay(200);
  }
}
801031c2:	90                   	nop
801031c3:	c9                   	leave  
801031c4:	c3                   	ret    

801031c5 <cmos_read>:
#define DAY     0x07
#define MONTH   0x08
#define YEAR    0x09

static uint cmos_read(uint reg)
{
801031c5:	55                   	push   %ebp
801031c6:	89 e5                	mov    %esp,%ebp
  outb(CMOS_PORT,  reg);
801031c8:	8b 45 08             	mov    0x8(%ebp),%eax
801031cb:	0f b6 c0             	movzbl %al,%eax
801031ce:	50                   	push   %eax
801031cf:	6a 70                	push   $0x70
801031d1:	e8 25 fd ff ff       	call   80102efb <outb>
801031d6:	83 c4 08             	add    $0x8,%esp
  microdelay(200);
801031d9:	68 c8 00 00 00       	push   $0xc8
801031de:	e8 fb fe ff ff       	call   801030de <microdelay>
801031e3:	83 c4 04             	add    $0x4,%esp

  return inb(CMOS_RETURN);
801031e6:	6a 71                	push   $0x71
801031e8:	e8 f1 fc ff ff       	call   80102ede <inb>
801031ed:	83 c4 04             	add    $0x4,%esp
801031f0:	0f b6 c0             	movzbl %al,%eax
}
801031f3:	c9                   	leave  
801031f4:	c3                   	ret    

801031f5 <fill_rtcdate>:

static void fill_rtcdate(struct rtcdate *r)
{
801031f5:	55                   	push   %ebp
801031f6:	89 e5                	mov    %esp,%ebp
  r->second = cmos_read(SECS);
801031f8:	6a 00                	push   $0x0
801031fa:	e8 c6 ff ff ff       	call   801031c5 <cmos_read>
801031ff:	83 c4 04             	add    $0x4,%esp
80103202:	89 c2                	mov    %eax,%edx
80103204:	8b 45 08             	mov    0x8(%ebp),%eax
80103207:	89 10                	mov    %edx,(%eax)
  r->minute = cmos_read(MINS);
80103209:	6a 02                	push   $0x2
8010320b:	e8 b5 ff ff ff       	call   801031c5 <cmos_read>
80103210:	83 c4 04             	add    $0x4,%esp
80103213:	89 c2                	mov    %eax,%edx
80103215:	8b 45 08             	mov    0x8(%ebp),%eax
80103218:	89 50 04             	mov    %edx,0x4(%eax)
  r->hour   = cmos_read(HOURS);
8010321b:	6a 04                	push   $0x4
8010321d:	e8 a3 ff ff ff       	call   801031c5 <cmos_read>
80103222:	83 c4 04             	add    $0x4,%esp
80103225:	89 c2                	mov    %eax,%edx
80103227:	8b 45 08             	mov    0x8(%ebp),%eax
8010322a:	89 50 08             	mov    %edx,0x8(%eax)
  r->day    = cmos_read(DAY);
8010322d:	6a 07                	push   $0x7
8010322f:	e8 91 ff ff ff       	call   801031c5 <cmos_read>
80103234:	83 c4 04             	add    $0x4,%esp
80103237:	89 c2                	mov    %eax,%edx
80103239:	8b 45 08             	mov    0x8(%ebp),%eax
8010323c:	89 50 0c             	mov    %edx,0xc(%eax)
  r->month  = cmos_read(MONTH);
8010323f:	6a 08                	push   $0x8
80103241:	e8 7f ff ff ff       	call   801031c5 <cmos_read>
80103246:	83 c4 04             	add    $0x4,%esp
80103249:	89 c2                	mov    %eax,%edx
8010324b:	8b 45 08             	mov    0x8(%ebp),%eax
8010324e:	89 50 10             	mov    %edx,0x10(%eax)
  r->year   = cmos_read(YEAR);
80103251:	6a 09                	push   $0x9
80103253:	e8 6d ff ff ff       	call   801031c5 <cmos_read>
80103258:	83 c4 04             	add    $0x4,%esp
8010325b:	89 c2                	mov    %eax,%edx
8010325d:	8b 45 08             	mov    0x8(%ebp),%eax
80103260:	89 50 14             	mov    %edx,0x14(%eax)
}
80103263:	90                   	nop
80103264:	c9                   	leave  
80103265:	c3                   	ret    

80103266 <cmostime>:

// qemu seems to use 24-hour GWT and the values are BCD encoded
void cmostime(struct rtcdate *r)
{
80103266:	55                   	push   %ebp
80103267:	89 e5                	mov    %esp,%ebp
80103269:	83 ec 48             	sub    $0x48,%esp
  struct rtcdate t1, t2;
  int sb, bcd;

  sb = cmos_read(CMOS_STATB);
8010326c:	6a 0b                	push   $0xb
8010326e:	e8 52 ff ff ff       	call   801031c5 <cmos_read>
80103273:	83 c4 04             	add    $0x4,%esp
80103276:	89 45 f4             	mov    %eax,-0xc(%ebp)

  bcd = (sb & (1 << 2)) == 0;
80103279:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010327c:	83 e0 04             	and    $0x4,%eax
8010327f:	85 c0                	test   %eax,%eax
80103281:	0f 94 c0             	sete   %al
80103284:	0f b6 c0             	movzbl %al,%eax
80103287:	89 45 f0             	mov    %eax,-0x10(%ebp)

  // make sure CMOS doesn't modify time while we read it
  for (;;) {
    fill_rtcdate(&t1);
8010328a:	8d 45 d8             	lea    -0x28(%ebp),%eax
8010328d:	50                   	push   %eax
8010328e:	e8 62 ff ff ff       	call   801031f5 <fill_rtcdate>
80103293:	83 c4 04             	add    $0x4,%esp
    if (cmos_read(CMOS_STATA) & CMOS_UIP)
80103296:	6a 0a                	push   $0xa
80103298:	e8 28 ff ff ff       	call   801031c5 <cmos_read>
8010329d:	83 c4 04             	add    $0x4,%esp
801032a0:	25 80 00 00 00       	and    $0x80,%eax
801032a5:	85 c0                	test   %eax,%eax
801032a7:	75 27                	jne    801032d0 <cmostime+0x6a>
        continue;
    fill_rtcdate(&t2);
801032a9:	8d 45 c0             	lea    -0x40(%ebp),%eax
801032ac:	50                   	push   %eax
801032ad:	e8 43 ff ff ff       	call   801031f5 <fill_rtcdate>
801032b2:	83 c4 04             	add    $0x4,%esp
    if (memcmp(&t1, &t2, sizeof(t1)) == 0)
801032b5:	83 ec 04             	sub    $0x4,%esp
801032b8:	6a 18                	push   $0x18
801032ba:	8d 45 c0             	lea    -0x40(%ebp),%eax
801032bd:	50                   	push   %eax
801032be:	8d 45 d8             	lea    -0x28(%ebp),%eax
801032c1:	50                   	push   %eax
801032c2:	e8 38 3a 00 00       	call   80106cff <memcmp>
801032c7:	83 c4 10             	add    $0x10,%esp
801032ca:	85 c0                	test   %eax,%eax
801032cc:	74 05                	je     801032d3 <cmostime+0x6d>
801032ce:	eb ba                	jmp    8010328a <cmostime+0x24>

  // make sure CMOS doesn't modify time while we read it
  for (;;) {
    fill_rtcdate(&t1);
    if (cmos_read(CMOS_STATA) & CMOS_UIP)
        continue;
801032d0:	90                   	nop
    fill_rtcdate(&t2);
    if (memcmp(&t1, &t2, sizeof(t1)) == 0)
      break;
  }
801032d1:	eb b7                	jmp    8010328a <cmostime+0x24>
    fill_rtcdate(&t1);
    if (cmos_read(CMOS_STATA) & CMOS_UIP)
        continue;
    fill_rtcdate(&t2);
    if (memcmp(&t1, &t2, sizeof(t1)) == 0)
      break;
801032d3:	90                   	nop
  }

  // convert
  if (bcd) {
801032d4:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
801032d8:	0f 84 b4 00 00 00    	je     80103392 <cmostime+0x12c>
#define    CONV(x)     (t1.x = ((t1.x >> 4) * 10) + (t1.x & 0xf))
    CONV(second);
801032de:	8b 45 d8             	mov    -0x28(%ebp),%eax
801032e1:	c1 e8 04             	shr    $0x4,%eax
801032e4:	89 c2                	mov    %eax,%edx
801032e6:	89 d0                	mov    %edx,%eax
801032e8:	c1 e0 02             	shl    $0x2,%eax
801032eb:	01 d0                	add    %edx,%eax
801032ed:	01 c0                	add    %eax,%eax
801032ef:	89 c2                	mov    %eax,%edx
801032f1:	8b 45 d8             	mov    -0x28(%ebp),%eax
801032f4:	83 e0 0f             	and    $0xf,%eax
801032f7:	01 d0                	add    %edx,%eax
801032f9:	89 45 d8             	mov    %eax,-0x28(%ebp)
    CONV(minute);
801032fc:	8b 45 dc             	mov    -0x24(%ebp),%eax
801032ff:	c1 e8 04             	shr    $0x4,%eax
80103302:	89 c2                	mov    %eax,%edx
80103304:	89 d0                	mov    %edx,%eax
80103306:	c1 e0 02             	shl    $0x2,%eax
80103309:	01 d0                	add    %edx,%eax
8010330b:	01 c0                	add    %eax,%eax
8010330d:	89 c2                	mov    %eax,%edx
8010330f:	8b 45 dc             	mov    -0x24(%ebp),%eax
80103312:	83 e0 0f             	and    $0xf,%eax
80103315:	01 d0                	add    %edx,%eax
80103317:	89 45 dc             	mov    %eax,-0x24(%ebp)
    CONV(hour  );
8010331a:	8b 45 e0             	mov    -0x20(%ebp),%eax
8010331d:	c1 e8 04             	shr    $0x4,%eax
80103320:	89 c2                	mov    %eax,%edx
80103322:	89 d0                	mov    %edx,%eax
80103324:	c1 e0 02             	shl    $0x2,%eax
80103327:	01 d0                	add    %edx,%eax
80103329:	01 c0                	add    %eax,%eax
8010332b:	89 c2                	mov    %eax,%edx
8010332d:	8b 45 e0             	mov    -0x20(%ebp),%eax
80103330:	83 e0 0f             	and    $0xf,%eax
80103333:	01 d0                	add    %edx,%eax
80103335:	89 45 e0             	mov    %eax,-0x20(%ebp)
    CONV(day   );
80103338:	8b 45 e4             	mov    -0x1c(%ebp),%eax
8010333b:	c1 e8 04             	shr    $0x4,%eax
8010333e:	89 c2                	mov    %eax,%edx
80103340:	89 d0                	mov    %edx,%eax
80103342:	c1 e0 02             	shl    $0x2,%eax
80103345:	01 d0                	add    %edx,%eax
80103347:	01 c0                	add    %eax,%eax
80103349:	89 c2                	mov    %eax,%edx
8010334b:	8b 45 e4             	mov    -0x1c(%ebp),%eax
8010334e:	83 e0 0f             	and    $0xf,%eax
80103351:	01 d0                	add    %edx,%eax
80103353:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    CONV(month );
80103356:	8b 45 e8             	mov    -0x18(%ebp),%eax
80103359:	c1 e8 04             	shr    $0x4,%eax
8010335c:	89 c2                	mov    %eax,%edx
8010335e:	89 d0                	mov    %edx,%eax
80103360:	c1 e0 02             	shl    $0x2,%eax
80103363:	01 d0                	add    %edx,%eax
80103365:	01 c0                	add    %eax,%eax
80103367:	89 c2                	mov    %eax,%edx
80103369:	8b 45 e8             	mov    -0x18(%ebp),%eax
8010336c:	83 e0 0f             	and    $0xf,%eax
8010336f:	01 d0                	add    %edx,%eax
80103371:	89 45 e8             	mov    %eax,-0x18(%ebp)
    CONV(year  );
80103374:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103377:	c1 e8 04             	shr    $0x4,%eax
8010337a:	89 c2                	mov    %eax,%edx
8010337c:	89 d0                	mov    %edx,%eax
8010337e:	c1 e0 02             	shl    $0x2,%eax
80103381:	01 d0                	add    %edx,%eax
80103383:	01 c0                	add    %eax,%eax
80103385:	89 c2                	mov    %eax,%edx
80103387:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010338a:	83 e0 0f             	and    $0xf,%eax
8010338d:	01 d0                	add    %edx,%eax
8010338f:	89 45 ec             	mov    %eax,-0x14(%ebp)
#undef     CONV
  }

  *r = t1;
80103392:	8b 45 08             	mov    0x8(%ebp),%eax
80103395:	8b 55 d8             	mov    -0x28(%ebp),%edx
80103398:	89 10                	mov    %edx,(%eax)
8010339a:	8b 55 dc             	mov    -0x24(%ebp),%edx
8010339d:	89 50 04             	mov    %edx,0x4(%eax)
801033a0:	8b 55 e0             	mov    -0x20(%ebp),%edx
801033a3:	89 50 08             	mov    %edx,0x8(%eax)
801033a6:	8b 55 e4             	mov    -0x1c(%ebp),%edx
801033a9:	89 50 0c             	mov    %edx,0xc(%eax)
801033ac:	8b 55 e8             	mov    -0x18(%ebp),%edx
801033af:	89 50 10             	mov    %edx,0x10(%eax)
801033b2:	8b 55 ec             	mov    -0x14(%ebp),%edx
801033b5:	89 50 14             	mov    %edx,0x14(%eax)
  r->year += 2000;
801033b8:	8b 45 08             	mov    0x8(%ebp),%eax
801033bb:	8b 40 14             	mov    0x14(%eax),%eax
801033be:	8d 90 d0 07 00 00    	lea    0x7d0(%eax),%edx
801033c4:	8b 45 08             	mov    0x8(%ebp),%eax
801033c7:	89 50 14             	mov    %edx,0x14(%eax)
}
801033ca:	90                   	nop
801033cb:	c9                   	leave  
801033cc:	c3                   	ret    

801033cd <initlog>:
static void recover_from_log(void);
static void commit();

void
initlog(int dev)
{
801033cd:	55                   	push   %ebp
801033ce:	89 e5                	mov    %esp,%ebp
801033d0:	83 ec 28             	sub    $0x28,%esp
  if (sizeof(struct logheader) >= BSIZE)
    panic("initlog: too big logheader");

  struct superblock sb;
  initlock(&log.lock, "log");
801033d3:	83 ec 08             	sub    $0x8,%esp
801033d6:	68 6c a3 10 80       	push   $0x8010a36c
801033db:	68 80 42 11 80       	push   $0x80114280
801033e0:	e8 2e 36 00 00       	call   80106a13 <initlock>
801033e5:	83 c4 10             	add    $0x10,%esp
  readsb(dev, &sb);
801033e8:	83 ec 08             	sub    $0x8,%esp
801033eb:	8d 45 dc             	lea    -0x24(%ebp),%eax
801033ee:	50                   	push   %eax
801033ef:	ff 75 08             	pushl  0x8(%ebp)
801033f2:	e8 2b e0 ff ff       	call   80101422 <readsb>
801033f7:	83 c4 10             	add    $0x10,%esp
  log.start = sb.logstart;
801033fa:	8b 45 ec             	mov    -0x14(%ebp),%eax
801033fd:	a3 b4 42 11 80       	mov    %eax,0x801142b4
  log.size = sb.nlog;
80103402:	8b 45 e8             	mov    -0x18(%ebp),%eax
80103405:	a3 b8 42 11 80       	mov    %eax,0x801142b8
  log.dev = dev;
8010340a:	8b 45 08             	mov    0x8(%ebp),%eax
8010340d:	a3 c4 42 11 80       	mov    %eax,0x801142c4
  recover_from_log();
80103412:	e8 b2 01 00 00       	call   801035c9 <recover_from_log>
}
80103417:	90                   	nop
80103418:	c9                   	leave  
80103419:	c3                   	ret    

8010341a <install_trans>:

// Copy committed blocks from log to their home location
static void 
install_trans(void)
{
8010341a:	55                   	push   %ebp
8010341b:	89 e5                	mov    %esp,%ebp
8010341d:	83 ec 18             	sub    $0x18,%esp
  int tail;

  for (tail = 0; tail < log.lh.n; tail++) {
80103420:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80103427:	e9 95 00 00 00       	jmp    801034c1 <install_trans+0xa7>
    struct buf *lbuf = bread(log.dev, log.start+tail+1); // read log block
8010342c:	8b 15 b4 42 11 80    	mov    0x801142b4,%edx
80103432:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103435:	01 d0                	add    %edx,%eax
80103437:	83 c0 01             	add    $0x1,%eax
8010343a:	89 c2                	mov    %eax,%edx
8010343c:	a1 c4 42 11 80       	mov    0x801142c4,%eax
80103441:	83 ec 08             	sub    $0x8,%esp
80103444:	52                   	push   %edx
80103445:	50                   	push   %eax
80103446:	e8 6b cd ff ff       	call   801001b6 <bread>
8010344b:	83 c4 10             	add    $0x10,%esp
8010344e:	89 45 f0             	mov    %eax,-0x10(%ebp)
    struct buf *dbuf = bread(log.dev, log.lh.block[tail]); // read dst
80103451:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103454:	83 c0 10             	add    $0x10,%eax
80103457:	8b 04 85 8c 42 11 80 	mov    -0x7feebd74(,%eax,4),%eax
8010345e:	89 c2                	mov    %eax,%edx
80103460:	a1 c4 42 11 80       	mov    0x801142c4,%eax
80103465:	83 ec 08             	sub    $0x8,%esp
80103468:	52                   	push   %edx
80103469:	50                   	push   %eax
8010346a:	e8 47 cd ff ff       	call   801001b6 <bread>
8010346f:	83 c4 10             	add    $0x10,%esp
80103472:	89 45 ec             	mov    %eax,-0x14(%ebp)
    memmove(dbuf->data, lbuf->data, BSIZE);  // copy block to dst
80103475:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103478:	8d 50 18             	lea    0x18(%eax),%edx
8010347b:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010347e:	83 c0 18             	add    $0x18,%eax
80103481:	83 ec 04             	sub    $0x4,%esp
80103484:	68 00 02 00 00       	push   $0x200
80103489:	52                   	push   %edx
8010348a:	50                   	push   %eax
8010348b:	e8 c7 38 00 00       	call   80106d57 <memmove>
80103490:	83 c4 10             	add    $0x10,%esp
    bwrite(dbuf);  // write dst to disk
80103493:	83 ec 0c             	sub    $0xc,%esp
80103496:	ff 75 ec             	pushl  -0x14(%ebp)
80103499:	e8 51 cd ff ff       	call   801001ef <bwrite>
8010349e:	83 c4 10             	add    $0x10,%esp
    brelse(lbuf); 
801034a1:	83 ec 0c             	sub    $0xc,%esp
801034a4:	ff 75 f0             	pushl  -0x10(%ebp)
801034a7:	e8 82 cd ff ff       	call   8010022e <brelse>
801034ac:	83 c4 10             	add    $0x10,%esp
    brelse(dbuf);
801034af:	83 ec 0c             	sub    $0xc,%esp
801034b2:	ff 75 ec             	pushl  -0x14(%ebp)
801034b5:	e8 74 cd ff ff       	call   8010022e <brelse>
801034ba:	83 c4 10             	add    $0x10,%esp
static void 
install_trans(void)
{
  int tail;

  for (tail = 0; tail < log.lh.n; tail++) {
801034bd:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
801034c1:	a1 c8 42 11 80       	mov    0x801142c8,%eax
801034c6:	3b 45 f4             	cmp    -0xc(%ebp),%eax
801034c9:	0f 8f 5d ff ff ff    	jg     8010342c <install_trans+0x12>
    memmove(dbuf->data, lbuf->data, BSIZE);  // copy block to dst
    bwrite(dbuf);  // write dst to disk
    brelse(lbuf); 
    brelse(dbuf);
  }
}
801034cf:	90                   	nop
801034d0:	c9                   	leave  
801034d1:	c3                   	ret    

801034d2 <read_head>:

// Read the log header from disk into the in-memory log header
static void
read_head(void)
{
801034d2:	55                   	push   %ebp
801034d3:	89 e5                	mov    %esp,%ebp
801034d5:	83 ec 18             	sub    $0x18,%esp
  struct buf *buf = bread(log.dev, log.start);
801034d8:	a1 b4 42 11 80       	mov    0x801142b4,%eax
801034dd:	89 c2                	mov    %eax,%edx
801034df:	a1 c4 42 11 80       	mov    0x801142c4,%eax
801034e4:	83 ec 08             	sub    $0x8,%esp
801034e7:	52                   	push   %edx
801034e8:	50                   	push   %eax
801034e9:	e8 c8 cc ff ff       	call   801001b6 <bread>
801034ee:	83 c4 10             	add    $0x10,%esp
801034f1:	89 45 f0             	mov    %eax,-0x10(%ebp)
  struct logheader *lh = (struct logheader *) (buf->data);
801034f4:	8b 45 f0             	mov    -0x10(%ebp),%eax
801034f7:	83 c0 18             	add    $0x18,%eax
801034fa:	89 45 ec             	mov    %eax,-0x14(%ebp)
  int i;
  log.lh.n = lh->n;
801034fd:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103500:	8b 00                	mov    (%eax),%eax
80103502:	a3 c8 42 11 80       	mov    %eax,0x801142c8
  for (i = 0; i < log.lh.n; i++) {
80103507:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
8010350e:	eb 1b                	jmp    8010352b <read_head+0x59>
    log.lh.block[i] = lh->block[i];
80103510:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103513:	8b 55 f4             	mov    -0xc(%ebp),%edx
80103516:	8b 44 90 04          	mov    0x4(%eax,%edx,4),%eax
8010351a:	8b 55 f4             	mov    -0xc(%ebp),%edx
8010351d:	83 c2 10             	add    $0x10,%edx
80103520:	89 04 95 8c 42 11 80 	mov    %eax,-0x7feebd74(,%edx,4)
{
  struct buf *buf = bread(log.dev, log.start);
  struct logheader *lh = (struct logheader *) (buf->data);
  int i;
  log.lh.n = lh->n;
  for (i = 0; i < log.lh.n; i++) {
80103527:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
8010352b:	a1 c8 42 11 80       	mov    0x801142c8,%eax
80103530:	3b 45 f4             	cmp    -0xc(%ebp),%eax
80103533:	7f db                	jg     80103510 <read_head+0x3e>
    log.lh.block[i] = lh->block[i];
  }
  brelse(buf);
80103535:	83 ec 0c             	sub    $0xc,%esp
80103538:	ff 75 f0             	pushl  -0x10(%ebp)
8010353b:	e8 ee cc ff ff       	call   8010022e <brelse>
80103540:	83 c4 10             	add    $0x10,%esp
}
80103543:	90                   	nop
80103544:	c9                   	leave  
80103545:	c3                   	ret    

80103546 <write_head>:
// Write in-memory log header to disk.
// This is the true point at which the
// current transaction commits.
static void
write_head(void)
{
80103546:	55                   	push   %ebp
80103547:	89 e5                	mov    %esp,%ebp
80103549:	83 ec 18             	sub    $0x18,%esp
  struct buf *buf = bread(log.dev, log.start);
8010354c:	a1 b4 42 11 80       	mov    0x801142b4,%eax
80103551:	89 c2                	mov    %eax,%edx
80103553:	a1 c4 42 11 80       	mov    0x801142c4,%eax
80103558:	83 ec 08             	sub    $0x8,%esp
8010355b:	52                   	push   %edx
8010355c:	50                   	push   %eax
8010355d:	e8 54 cc ff ff       	call   801001b6 <bread>
80103562:	83 c4 10             	add    $0x10,%esp
80103565:	89 45 f0             	mov    %eax,-0x10(%ebp)
  struct logheader *hb = (struct logheader *) (buf->data);
80103568:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010356b:	83 c0 18             	add    $0x18,%eax
8010356e:	89 45 ec             	mov    %eax,-0x14(%ebp)
  int i;
  hb->n = log.lh.n;
80103571:	8b 15 c8 42 11 80    	mov    0x801142c8,%edx
80103577:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010357a:	89 10                	mov    %edx,(%eax)
  for (i = 0; i < log.lh.n; i++) {
8010357c:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80103583:	eb 1b                	jmp    801035a0 <write_head+0x5a>
    hb->block[i] = log.lh.block[i];
80103585:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103588:	83 c0 10             	add    $0x10,%eax
8010358b:	8b 0c 85 8c 42 11 80 	mov    -0x7feebd74(,%eax,4),%ecx
80103592:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103595:	8b 55 f4             	mov    -0xc(%ebp),%edx
80103598:	89 4c 90 04          	mov    %ecx,0x4(%eax,%edx,4)
{
  struct buf *buf = bread(log.dev, log.start);
  struct logheader *hb = (struct logheader *) (buf->data);
  int i;
  hb->n = log.lh.n;
  for (i = 0; i < log.lh.n; i++) {
8010359c:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
801035a0:	a1 c8 42 11 80       	mov    0x801142c8,%eax
801035a5:	3b 45 f4             	cmp    -0xc(%ebp),%eax
801035a8:	7f db                	jg     80103585 <write_head+0x3f>
    hb->block[i] = log.lh.block[i];
  }
  bwrite(buf);
801035aa:	83 ec 0c             	sub    $0xc,%esp
801035ad:	ff 75 f0             	pushl  -0x10(%ebp)
801035b0:	e8 3a cc ff ff       	call   801001ef <bwrite>
801035b5:	83 c4 10             	add    $0x10,%esp
  brelse(buf);
801035b8:	83 ec 0c             	sub    $0xc,%esp
801035bb:	ff 75 f0             	pushl  -0x10(%ebp)
801035be:	e8 6b cc ff ff       	call   8010022e <brelse>
801035c3:	83 c4 10             	add    $0x10,%esp
}
801035c6:	90                   	nop
801035c7:	c9                   	leave  
801035c8:	c3                   	ret    

801035c9 <recover_from_log>:

static void
recover_from_log(void)
{
801035c9:	55                   	push   %ebp
801035ca:	89 e5                	mov    %esp,%ebp
801035cc:	83 ec 08             	sub    $0x8,%esp
  read_head();      
801035cf:	e8 fe fe ff ff       	call   801034d2 <read_head>
  install_trans(); // if committed, copy from log to disk
801035d4:	e8 41 fe ff ff       	call   8010341a <install_trans>
  log.lh.n = 0;
801035d9:	c7 05 c8 42 11 80 00 	movl   $0x0,0x801142c8
801035e0:	00 00 00 
  write_head(); // clear the log
801035e3:	e8 5e ff ff ff       	call   80103546 <write_head>
}
801035e8:	90                   	nop
801035e9:	c9                   	leave  
801035ea:	c3                   	ret    

801035eb <begin_op>:

// called at the start of each FS system call.
void
begin_op(void)
{
801035eb:	55                   	push   %ebp
801035ec:	89 e5                	mov    %esp,%ebp
801035ee:	83 ec 08             	sub    $0x8,%esp
  acquire(&log.lock);
801035f1:	83 ec 0c             	sub    $0xc,%esp
801035f4:	68 80 42 11 80       	push   $0x80114280
801035f9:	e8 37 34 00 00       	call   80106a35 <acquire>
801035fe:	83 c4 10             	add    $0x10,%esp
  while(1){
    if(log.committing){
80103601:	a1 c0 42 11 80       	mov    0x801142c0,%eax
80103606:	85 c0                	test   %eax,%eax
80103608:	74 17                	je     80103621 <begin_op+0x36>
      sleep(&log, &log.lock);
8010360a:	83 ec 08             	sub    $0x8,%esp
8010360d:	68 80 42 11 80       	push   $0x80114280
80103612:	68 80 42 11 80       	push   $0x80114280
80103617:	e8 d0 1d 00 00       	call   801053ec <sleep>
8010361c:	83 c4 10             	add    $0x10,%esp
8010361f:	eb e0                	jmp    80103601 <begin_op+0x16>
    } else if(log.lh.n + (log.outstanding+1)*MAXOPBLOCKS > LOGSIZE){
80103621:	8b 0d c8 42 11 80    	mov    0x801142c8,%ecx
80103627:	a1 bc 42 11 80       	mov    0x801142bc,%eax
8010362c:	8d 50 01             	lea    0x1(%eax),%edx
8010362f:	89 d0                	mov    %edx,%eax
80103631:	c1 e0 02             	shl    $0x2,%eax
80103634:	01 d0                	add    %edx,%eax
80103636:	01 c0                	add    %eax,%eax
80103638:	01 c8                	add    %ecx,%eax
8010363a:	83 f8 1e             	cmp    $0x1e,%eax
8010363d:	7e 17                	jle    80103656 <begin_op+0x6b>
      // this op might exhaust log space; wait for commit.
      sleep(&log, &log.lock);
8010363f:	83 ec 08             	sub    $0x8,%esp
80103642:	68 80 42 11 80       	push   $0x80114280
80103647:	68 80 42 11 80       	push   $0x80114280
8010364c:	e8 9b 1d 00 00       	call   801053ec <sleep>
80103651:	83 c4 10             	add    $0x10,%esp
80103654:	eb ab                	jmp    80103601 <begin_op+0x16>
    } else {
      log.outstanding += 1;
80103656:	a1 bc 42 11 80       	mov    0x801142bc,%eax
8010365b:	83 c0 01             	add    $0x1,%eax
8010365e:	a3 bc 42 11 80       	mov    %eax,0x801142bc
      release(&log.lock);
80103663:	83 ec 0c             	sub    $0xc,%esp
80103666:	68 80 42 11 80       	push   $0x80114280
8010366b:	e8 2c 34 00 00       	call   80106a9c <release>
80103670:	83 c4 10             	add    $0x10,%esp
      break;
80103673:	90                   	nop
    }
  }
}
80103674:	90                   	nop
80103675:	c9                   	leave  
80103676:	c3                   	ret    

80103677 <end_op>:

// called at the end of each FS system call.
// commits if this was the last outstanding operation.
void
end_op(void)
{
80103677:	55                   	push   %ebp
80103678:	89 e5                	mov    %esp,%ebp
8010367a:	83 ec 18             	sub    $0x18,%esp
  int do_commit = 0;
8010367d:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

  acquire(&log.lock);
80103684:	83 ec 0c             	sub    $0xc,%esp
80103687:	68 80 42 11 80       	push   $0x80114280
8010368c:	e8 a4 33 00 00       	call   80106a35 <acquire>
80103691:	83 c4 10             	add    $0x10,%esp
  log.outstanding -= 1;
80103694:	a1 bc 42 11 80       	mov    0x801142bc,%eax
80103699:	83 e8 01             	sub    $0x1,%eax
8010369c:	a3 bc 42 11 80       	mov    %eax,0x801142bc
  if(log.committing)
801036a1:	a1 c0 42 11 80       	mov    0x801142c0,%eax
801036a6:	85 c0                	test   %eax,%eax
801036a8:	74 0d                	je     801036b7 <end_op+0x40>
    panic("log.committing");
801036aa:	83 ec 0c             	sub    $0xc,%esp
801036ad:	68 70 a3 10 80       	push   $0x8010a370
801036b2:	e8 af ce ff ff       	call   80100566 <panic>
  if(log.outstanding == 0){
801036b7:	a1 bc 42 11 80       	mov    0x801142bc,%eax
801036bc:	85 c0                	test   %eax,%eax
801036be:	75 13                	jne    801036d3 <end_op+0x5c>
    do_commit = 1;
801036c0:	c7 45 f4 01 00 00 00 	movl   $0x1,-0xc(%ebp)
    log.committing = 1;
801036c7:	c7 05 c0 42 11 80 01 	movl   $0x1,0x801142c0
801036ce:	00 00 00 
801036d1:	eb 10                	jmp    801036e3 <end_op+0x6c>
  } else {
    // begin_op() may be waiting for log space.
    wakeup(&log);
801036d3:	83 ec 0c             	sub    $0xc,%esp
801036d6:	68 80 42 11 80       	push   $0x80114280
801036db:	e8 b5 1e 00 00       	call   80105595 <wakeup>
801036e0:	83 c4 10             	add    $0x10,%esp
  }
  release(&log.lock);
801036e3:	83 ec 0c             	sub    $0xc,%esp
801036e6:	68 80 42 11 80       	push   $0x80114280
801036eb:	e8 ac 33 00 00       	call   80106a9c <release>
801036f0:	83 c4 10             	add    $0x10,%esp

  if(do_commit){
801036f3:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801036f7:	74 3f                	je     80103738 <end_op+0xc1>
    // call commit w/o holding locks, since not allowed
    // to sleep with locks.
    commit();
801036f9:	e8 f5 00 00 00       	call   801037f3 <commit>
    acquire(&log.lock);
801036fe:	83 ec 0c             	sub    $0xc,%esp
80103701:	68 80 42 11 80       	push   $0x80114280
80103706:	e8 2a 33 00 00       	call   80106a35 <acquire>
8010370b:	83 c4 10             	add    $0x10,%esp
    log.committing = 0;
8010370e:	c7 05 c0 42 11 80 00 	movl   $0x0,0x801142c0
80103715:	00 00 00 
    wakeup(&log);
80103718:	83 ec 0c             	sub    $0xc,%esp
8010371b:	68 80 42 11 80       	push   $0x80114280
80103720:	e8 70 1e 00 00       	call   80105595 <wakeup>
80103725:	83 c4 10             	add    $0x10,%esp
    release(&log.lock);
80103728:	83 ec 0c             	sub    $0xc,%esp
8010372b:	68 80 42 11 80       	push   $0x80114280
80103730:	e8 67 33 00 00       	call   80106a9c <release>
80103735:	83 c4 10             	add    $0x10,%esp
  }
}
80103738:	90                   	nop
80103739:	c9                   	leave  
8010373a:	c3                   	ret    

8010373b <write_log>:

// Copy modified blocks from cache to log.
static void 
write_log(void)
{
8010373b:	55                   	push   %ebp
8010373c:	89 e5                	mov    %esp,%ebp
8010373e:	83 ec 18             	sub    $0x18,%esp
  int tail;

  for (tail = 0; tail < log.lh.n; tail++) {
80103741:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80103748:	e9 95 00 00 00       	jmp    801037e2 <write_log+0xa7>
    struct buf *to = bread(log.dev, log.start+tail+1); // log block
8010374d:	8b 15 b4 42 11 80    	mov    0x801142b4,%edx
80103753:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103756:	01 d0                	add    %edx,%eax
80103758:	83 c0 01             	add    $0x1,%eax
8010375b:	89 c2                	mov    %eax,%edx
8010375d:	a1 c4 42 11 80       	mov    0x801142c4,%eax
80103762:	83 ec 08             	sub    $0x8,%esp
80103765:	52                   	push   %edx
80103766:	50                   	push   %eax
80103767:	e8 4a ca ff ff       	call   801001b6 <bread>
8010376c:	83 c4 10             	add    $0x10,%esp
8010376f:	89 45 f0             	mov    %eax,-0x10(%ebp)
    struct buf *from = bread(log.dev, log.lh.block[tail]); // cache block
80103772:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103775:	83 c0 10             	add    $0x10,%eax
80103778:	8b 04 85 8c 42 11 80 	mov    -0x7feebd74(,%eax,4),%eax
8010377f:	89 c2                	mov    %eax,%edx
80103781:	a1 c4 42 11 80       	mov    0x801142c4,%eax
80103786:	83 ec 08             	sub    $0x8,%esp
80103789:	52                   	push   %edx
8010378a:	50                   	push   %eax
8010378b:	e8 26 ca ff ff       	call   801001b6 <bread>
80103790:	83 c4 10             	add    $0x10,%esp
80103793:	89 45 ec             	mov    %eax,-0x14(%ebp)
    memmove(to->data, from->data, BSIZE);
80103796:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103799:	8d 50 18             	lea    0x18(%eax),%edx
8010379c:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010379f:	83 c0 18             	add    $0x18,%eax
801037a2:	83 ec 04             	sub    $0x4,%esp
801037a5:	68 00 02 00 00       	push   $0x200
801037aa:	52                   	push   %edx
801037ab:	50                   	push   %eax
801037ac:	e8 a6 35 00 00       	call   80106d57 <memmove>
801037b1:	83 c4 10             	add    $0x10,%esp
    bwrite(to);  // write the log
801037b4:	83 ec 0c             	sub    $0xc,%esp
801037b7:	ff 75 f0             	pushl  -0x10(%ebp)
801037ba:	e8 30 ca ff ff       	call   801001ef <bwrite>
801037bf:	83 c4 10             	add    $0x10,%esp
    brelse(from); 
801037c2:	83 ec 0c             	sub    $0xc,%esp
801037c5:	ff 75 ec             	pushl  -0x14(%ebp)
801037c8:	e8 61 ca ff ff       	call   8010022e <brelse>
801037cd:	83 c4 10             	add    $0x10,%esp
    brelse(to);
801037d0:	83 ec 0c             	sub    $0xc,%esp
801037d3:	ff 75 f0             	pushl  -0x10(%ebp)
801037d6:	e8 53 ca ff ff       	call   8010022e <brelse>
801037db:	83 c4 10             	add    $0x10,%esp
static void 
write_log(void)
{
  int tail;

  for (tail = 0; tail < log.lh.n; tail++) {
801037de:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
801037e2:	a1 c8 42 11 80       	mov    0x801142c8,%eax
801037e7:	3b 45 f4             	cmp    -0xc(%ebp),%eax
801037ea:	0f 8f 5d ff ff ff    	jg     8010374d <write_log+0x12>
    memmove(to->data, from->data, BSIZE);
    bwrite(to);  // write the log
    brelse(from); 
    brelse(to);
  }
}
801037f0:	90                   	nop
801037f1:	c9                   	leave  
801037f2:	c3                   	ret    

801037f3 <commit>:

static void
commit()
{
801037f3:	55                   	push   %ebp
801037f4:	89 e5                	mov    %esp,%ebp
801037f6:	83 ec 08             	sub    $0x8,%esp
  if (log.lh.n > 0) {
801037f9:	a1 c8 42 11 80       	mov    0x801142c8,%eax
801037fe:	85 c0                	test   %eax,%eax
80103800:	7e 1e                	jle    80103820 <commit+0x2d>
    write_log();     // Write modified blocks from cache to log
80103802:	e8 34 ff ff ff       	call   8010373b <write_log>
    write_head();    // Write header to disk -- the real commit
80103807:	e8 3a fd ff ff       	call   80103546 <write_head>
    install_trans(); // Now install writes to home locations
8010380c:	e8 09 fc ff ff       	call   8010341a <install_trans>
    log.lh.n = 0; 
80103811:	c7 05 c8 42 11 80 00 	movl   $0x0,0x801142c8
80103818:	00 00 00 
    write_head();    // Erase the transaction from the log
8010381b:	e8 26 fd ff ff       	call   80103546 <write_head>
  }
}
80103820:	90                   	nop
80103821:	c9                   	leave  
80103822:	c3                   	ret    

80103823 <log_write>:
//   modify bp->data[]
//   log_write(bp)
//   brelse(bp)
void
log_write(struct buf *b)
{
80103823:	55                   	push   %ebp
80103824:	89 e5                	mov    %esp,%ebp
80103826:	83 ec 18             	sub    $0x18,%esp
  int i;

  if (log.lh.n >= LOGSIZE || log.lh.n >= log.size - 1)
80103829:	a1 c8 42 11 80       	mov    0x801142c8,%eax
8010382e:	83 f8 1d             	cmp    $0x1d,%eax
80103831:	7f 12                	jg     80103845 <log_write+0x22>
80103833:	a1 c8 42 11 80       	mov    0x801142c8,%eax
80103838:	8b 15 b8 42 11 80    	mov    0x801142b8,%edx
8010383e:	83 ea 01             	sub    $0x1,%edx
80103841:	39 d0                	cmp    %edx,%eax
80103843:	7c 0d                	jl     80103852 <log_write+0x2f>
    panic("too big a transaction");
80103845:	83 ec 0c             	sub    $0xc,%esp
80103848:	68 7f a3 10 80       	push   $0x8010a37f
8010384d:	e8 14 cd ff ff       	call   80100566 <panic>
  if (log.outstanding < 1)
80103852:	a1 bc 42 11 80       	mov    0x801142bc,%eax
80103857:	85 c0                	test   %eax,%eax
80103859:	7f 0d                	jg     80103868 <log_write+0x45>
    panic("log_write outside of trans");
8010385b:	83 ec 0c             	sub    $0xc,%esp
8010385e:	68 95 a3 10 80       	push   $0x8010a395
80103863:	e8 fe cc ff ff       	call   80100566 <panic>

  acquire(&log.lock);
80103868:	83 ec 0c             	sub    $0xc,%esp
8010386b:	68 80 42 11 80       	push   $0x80114280
80103870:	e8 c0 31 00 00       	call   80106a35 <acquire>
80103875:	83 c4 10             	add    $0x10,%esp
  for (i = 0; i < log.lh.n; i++) {
80103878:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
8010387f:	eb 1d                	jmp    8010389e <log_write+0x7b>
    if (log.lh.block[i] == b->blockno)   // log absorbtion
80103881:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103884:	83 c0 10             	add    $0x10,%eax
80103887:	8b 04 85 8c 42 11 80 	mov    -0x7feebd74(,%eax,4),%eax
8010388e:	89 c2                	mov    %eax,%edx
80103890:	8b 45 08             	mov    0x8(%ebp),%eax
80103893:	8b 40 08             	mov    0x8(%eax),%eax
80103896:	39 c2                	cmp    %eax,%edx
80103898:	74 10                	je     801038aa <log_write+0x87>
    panic("too big a transaction");
  if (log.outstanding < 1)
    panic("log_write outside of trans");

  acquire(&log.lock);
  for (i = 0; i < log.lh.n; i++) {
8010389a:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
8010389e:	a1 c8 42 11 80       	mov    0x801142c8,%eax
801038a3:	3b 45 f4             	cmp    -0xc(%ebp),%eax
801038a6:	7f d9                	jg     80103881 <log_write+0x5e>
801038a8:	eb 01                	jmp    801038ab <log_write+0x88>
    if (log.lh.block[i] == b->blockno)   // log absorbtion
      break;
801038aa:	90                   	nop
  }
  log.lh.block[i] = b->blockno;
801038ab:	8b 45 08             	mov    0x8(%ebp),%eax
801038ae:	8b 40 08             	mov    0x8(%eax),%eax
801038b1:	89 c2                	mov    %eax,%edx
801038b3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801038b6:	83 c0 10             	add    $0x10,%eax
801038b9:	89 14 85 8c 42 11 80 	mov    %edx,-0x7feebd74(,%eax,4)
  if (i == log.lh.n)
801038c0:	a1 c8 42 11 80       	mov    0x801142c8,%eax
801038c5:	3b 45 f4             	cmp    -0xc(%ebp),%eax
801038c8:	75 0d                	jne    801038d7 <log_write+0xb4>
    log.lh.n++;
801038ca:	a1 c8 42 11 80       	mov    0x801142c8,%eax
801038cf:	83 c0 01             	add    $0x1,%eax
801038d2:	a3 c8 42 11 80       	mov    %eax,0x801142c8
  b->flags |= B_DIRTY; // prevent eviction
801038d7:	8b 45 08             	mov    0x8(%ebp),%eax
801038da:	8b 00                	mov    (%eax),%eax
801038dc:	83 c8 04             	or     $0x4,%eax
801038df:	89 c2                	mov    %eax,%edx
801038e1:	8b 45 08             	mov    0x8(%ebp),%eax
801038e4:	89 10                	mov    %edx,(%eax)
  release(&log.lock);
801038e6:	83 ec 0c             	sub    $0xc,%esp
801038e9:	68 80 42 11 80       	push   $0x80114280
801038ee:	e8 a9 31 00 00       	call   80106a9c <release>
801038f3:	83 c4 10             	add    $0x10,%esp
}
801038f6:	90                   	nop
801038f7:	c9                   	leave  
801038f8:	c3                   	ret    

801038f9 <v2p>:
801038f9:	55                   	push   %ebp
801038fa:	89 e5                	mov    %esp,%ebp
801038fc:	8b 45 08             	mov    0x8(%ebp),%eax
801038ff:	05 00 00 00 80       	add    $0x80000000,%eax
80103904:	5d                   	pop    %ebp
80103905:	c3                   	ret    

80103906 <p2v>:
static inline void *p2v(uint a) { return (void *) ((a) + KERNBASE); }
80103906:	55                   	push   %ebp
80103907:	89 e5                	mov    %esp,%ebp
80103909:	8b 45 08             	mov    0x8(%ebp),%eax
8010390c:	05 00 00 00 80       	add    $0x80000000,%eax
80103911:	5d                   	pop    %ebp
80103912:	c3                   	ret    

80103913 <xchg>:
  asm volatile("sti");
}

static inline uint
xchg(volatile uint *addr, uint newval)
{
80103913:	55                   	push   %ebp
80103914:	89 e5                	mov    %esp,%ebp
80103916:	83 ec 10             	sub    $0x10,%esp
  uint result;
  
  // The + in "+m" denotes a read-modify-write operand.
  asm volatile("lock; xchgl %0, %1" :
80103919:	8b 55 08             	mov    0x8(%ebp),%edx
8010391c:	8b 45 0c             	mov    0xc(%ebp),%eax
8010391f:	8b 4d 08             	mov    0x8(%ebp),%ecx
80103922:	f0 87 02             	lock xchg %eax,(%edx)
80103925:	89 45 fc             	mov    %eax,-0x4(%ebp)
               "+m" (*addr), "=a" (result) :
               "1" (newval) :
               "cc");
  return result;
80103928:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
8010392b:	c9                   	leave  
8010392c:	c3                   	ret    

8010392d <main>:
// Bootstrap processor starts running C code here.
// Allocate a real stack and switch to it, first
// doing some setup required for memory allocator to work.
int
main(void)
{
8010392d:	8d 4c 24 04          	lea    0x4(%esp),%ecx
80103931:	83 e4 f0             	and    $0xfffffff0,%esp
80103934:	ff 71 fc             	pushl  -0x4(%ecx)
80103937:	55                   	push   %ebp
80103938:	89 e5                	mov    %esp,%ebp
8010393a:	51                   	push   %ecx
8010393b:	83 ec 04             	sub    $0x4,%esp
  kinit1(end, P2V(4*1024*1024)); // phys page allocator
8010393e:	83 ec 08             	sub    $0x8,%esp
80103941:	68 00 00 40 80       	push   $0x80400000
80103946:	68 5c 79 11 80       	push   $0x8011795c
8010394b:	e8 7d f2 ff ff       	call   80102bcd <kinit1>
80103950:	83 c4 10             	add    $0x10,%esp
  kvmalloc();      // kernel page table
80103953:	e8 27 60 00 00       	call   8010997f <kvmalloc>
  mpinit();        // collect info about this machine
80103958:	e8 43 04 00 00       	call   80103da0 <mpinit>
  lapicinit();
8010395d:	e8 ea f5 ff ff       	call   80102f4c <lapicinit>
  seginit();       // set up segments
80103962:	e8 c1 59 00 00       	call   80109328 <seginit>
  cprintf("\ncpu%d: starting xv6\n\n", cpu->id);
80103967:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
8010396d:	0f b6 00             	movzbl (%eax),%eax
80103970:	0f b6 c0             	movzbl %al,%eax
80103973:	83 ec 08             	sub    $0x8,%esp
80103976:	50                   	push   %eax
80103977:	68 b0 a3 10 80       	push   $0x8010a3b0
8010397c:	e8 45 ca ff ff       	call   801003c6 <cprintf>
80103981:	83 c4 10             	add    $0x10,%esp
  picinit();       // interrupt controller
80103984:	e8 6d 06 00 00       	call   80103ff6 <picinit>
  ioapicinit();    // another interrupt controller
80103989:	e8 34 f1 ff ff       	call   80102ac2 <ioapicinit>
  consoleinit();   // I/O devices & their interrupts
8010398e:	e8 24 d2 ff ff       	call   80100bb7 <consoleinit>
  uartinit();      // serial port
80103993:	e8 ec 4c 00 00       	call   80108684 <uartinit>
  pinit();         // process table
80103998:	e8 5d 0b 00 00       	call   801044fa <pinit>
  tvinit();        // trap vectors
8010399d:	e8 de 48 00 00       	call   80108280 <tvinit>
  binit();         // buffer cache
801039a2:	e8 8d c6 ff ff       	call   80100034 <binit>
  fileinit();      // file table
801039a7:	e8 67 d6 ff ff       	call   80101013 <fileinit>
  ideinit();       // disk
801039ac:	e8 19 ed ff ff       	call   801026ca <ideinit>
  if(!ismp)
801039b1:	a1 64 43 11 80       	mov    0x80114364,%eax
801039b6:	85 c0                	test   %eax,%eax
801039b8:	75 05                	jne    801039bf <main+0x92>
    timerinit();   // uniprocessor timer
801039ba:	e8 12 48 00 00       	call   801081d1 <timerinit>
  startothers();   // start other processors
801039bf:	e8 7f 00 00 00       	call   80103a43 <startothers>
  kinit2(P2V(4*1024*1024), P2V(PHYSTOP)); // must come after startothers()
801039c4:	83 ec 08             	sub    $0x8,%esp
801039c7:	68 00 00 00 8e       	push   $0x8e000000
801039cc:	68 00 00 40 80       	push   $0x80400000
801039d1:	e8 30 f2 ff ff       	call   80102c06 <kinit2>
801039d6:	83 c4 10             	add    $0x10,%esp
  userinit();      // first user process
801039d9:	e8 08 0d 00 00       	call   801046e6 <userinit>
  // Finish setting up this processor in mpmain.
  mpmain();
801039de:	e8 1a 00 00 00       	call   801039fd <mpmain>

801039e3 <mpenter>:
}

// Other CPUs jump here from entryother.S.
static void
mpenter(void)
{
801039e3:	55                   	push   %ebp
801039e4:	89 e5                	mov    %esp,%ebp
801039e6:	83 ec 08             	sub    $0x8,%esp
  switchkvm(); 
801039e9:	e8 a9 5f 00 00       	call   80109997 <switchkvm>
  seginit();
801039ee:	e8 35 59 00 00       	call   80109328 <seginit>
  lapicinit();
801039f3:	e8 54 f5 ff ff       	call   80102f4c <lapicinit>
  mpmain();
801039f8:	e8 00 00 00 00       	call   801039fd <mpmain>

801039fd <mpmain>:
}

// Common CPU setup code.
static void
mpmain(void)
{
801039fd:	55                   	push   %ebp
801039fe:	89 e5                	mov    %esp,%ebp
80103a00:	83 ec 08             	sub    $0x8,%esp
  cprintf("cpu%d: starting\n", cpu->id);
80103a03:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80103a09:	0f b6 00             	movzbl (%eax),%eax
80103a0c:	0f b6 c0             	movzbl %al,%eax
80103a0f:	83 ec 08             	sub    $0x8,%esp
80103a12:	50                   	push   %eax
80103a13:	68 c7 a3 10 80       	push   $0x8010a3c7
80103a18:	e8 a9 c9 ff ff       	call   801003c6 <cprintf>
80103a1d:	83 c4 10             	add    $0x10,%esp
  idtinit();       // load idt register
80103a20:	e8 bc 49 00 00       	call   801083e1 <idtinit>
  xchg(&cpu->started, 1); // tell startothers() we're up
80103a25:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80103a2b:	05 a8 00 00 00       	add    $0xa8,%eax
80103a30:	83 ec 08             	sub    $0x8,%esp
80103a33:	6a 01                	push   $0x1
80103a35:	50                   	push   %eax
80103a36:	e8 d8 fe ff ff       	call   80103913 <xchg>
80103a3b:	83 c4 10             	add    $0x10,%esp
  scheduler();     // start running processes
80103a3e:	e8 3f 16 00 00       	call   80105082 <scheduler>

80103a43 <startothers>:
pde_t entrypgdir[];  // For entry.S

// Start the non-boot (AP) processors.
static void
startothers(void)
{
80103a43:	55                   	push   %ebp
80103a44:	89 e5                	mov    %esp,%ebp
80103a46:	53                   	push   %ebx
80103a47:	83 ec 14             	sub    $0x14,%esp
  char *stack;

  // Write entry code to unused memory at 0x7000.
  // The linker has placed the image of entryother.S in
  // _binary_entryother_start.
  code = p2v(0x7000);
80103a4a:	68 00 70 00 00       	push   $0x7000
80103a4f:	e8 b2 fe ff ff       	call   80103906 <p2v>
80103a54:	83 c4 04             	add    $0x4,%esp
80103a57:	89 45 f0             	mov    %eax,-0x10(%ebp)
  memmove(code, _binary_entryother_start, (uint)_binary_entryother_size);
80103a5a:	b8 8a 00 00 00       	mov    $0x8a,%eax
80103a5f:	83 ec 04             	sub    $0x4,%esp
80103a62:	50                   	push   %eax
80103a63:	68 2c d5 10 80       	push   $0x8010d52c
80103a68:	ff 75 f0             	pushl  -0x10(%ebp)
80103a6b:	e8 e7 32 00 00       	call   80106d57 <memmove>
80103a70:	83 c4 10             	add    $0x10,%esp

  for(c = cpus; c < cpus+ncpu; c++){
80103a73:	c7 45 f4 80 43 11 80 	movl   $0x80114380,-0xc(%ebp)
80103a7a:	e9 90 00 00 00       	jmp    80103b0f <startothers+0xcc>
    if(c == cpus+cpunum())  // We've started already.
80103a7f:	e8 e6 f5 ff ff       	call   8010306a <cpunum>
80103a84:	69 c0 bc 00 00 00    	imul   $0xbc,%eax,%eax
80103a8a:	05 80 43 11 80       	add    $0x80114380,%eax
80103a8f:	3b 45 f4             	cmp    -0xc(%ebp),%eax
80103a92:	74 73                	je     80103b07 <startothers+0xc4>
      continue;

    // Tell entryother.S what stack to use, where to enter, and what 
    // pgdir to use. We cannot use kpgdir yet, because the AP processor
    // is running in low  memory, so we use entrypgdir for the APs too.
    stack = kalloc();
80103a94:	e8 6b f2 ff ff       	call   80102d04 <kalloc>
80103a99:	89 45 ec             	mov    %eax,-0x14(%ebp)
    *(void**)(code-4) = stack + KSTACKSIZE;
80103a9c:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103a9f:	83 e8 04             	sub    $0x4,%eax
80103aa2:	8b 55 ec             	mov    -0x14(%ebp),%edx
80103aa5:	81 c2 00 10 00 00    	add    $0x1000,%edx
80103aab:	89 10                	mov    %edx,(%eax)
    *(void**)(code-8) = mpenter;
80103aad:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103ab0:	83 e8 08             	sub    $0x8,%eax
80103ab3:	c7 00 e3 39 10 80    	movl   $0x801039e3,(%eax)
    *(int**)(code-12) = (void *) v2p(entrypgdir);
80103ab9:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103abc:	8d 58 f4             	lea    -0xc(%eax),%ebx
80103abf:	83 ec 0c             	sub    $0xc,%esp
80103ac2:	68 00 c0 10 80       	push   $0x8010c000
80103ac7:	e8 2d fe ff ff       	call   801038f9 <v2p>
80103acc:	83 c4 10             	add    $0x10,%esp
80103acf:	89 03                	mov    %eax,(%ebx)

    lapicstartap(c->id, v2p(code));
80103ad1:	83 ec 0c             	sub    $0xc,%esp
80103ad4:	ff 75 f0             	pushl  -0x10(%ebp)
80103ad7:	e8 1d fe ff ff       	call   801038f9 <v2p>
80103adc:	83 c4 10             	add    $0x10,%esp
80103adf:	89 c2                	mov    %eax,%edx
80103ae1:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103ae4:	0f b6 00             	movzbl (%eax),%eax
80103ae7:	0f b6 c0             	movzbl %al,%eax
80103aea:	83 ec 08             	sub    $0x8,%esp
80103aed:	52                   	push   %edx
80103aee:	50                   	push   %eax
80103aef:	e8 f0 f5 ff ff       	call   801030e4 <lapicstartap>
80103af4:	83 c4 10             	add    $0x10,%esp

    // wait for cpu to finish mpmain()
    while(c->started == 0)
80103af7:	90                   	nop
80103af8:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103afb:	8b 80 a8 00 00 00    	mov    0xa8(%eax),%eax
80103b01:	85 c0                	test   %eax,%eax
80103b03:	74 f3                	je     80103af8 <startothers+0xb5>
80103b05:	eb 01                	jmp    80103b08 <startothers+0xc5>
  code = p2v(0x7000);
  memmove(code, _binary_entryother_start, (uint)_binary_entryother_size);

  for(c = cpus; c < cpus+ncpu; c++){
    if(c == cpus+cpunum())  // We've started already.
      continue;
80103b07:	90                   	nop
  // The linker has placed the image of entryother.S in
  // _binary_entryother_start.
  code = p2v(0x7000);
  memmove(code, _binary_entryother_start, (uint)_binary_entryother_size);

  for(c = cpus; c < cpus+ncpu; c++){
80103b08:	81 45 f4 bc 00 00 00 	addl   $0xbc,-0xc(%ebp)
80103b0f:	a1 60 49 11 80       	mov    0x80114960,%eax
80103b14:	69 c0 bc 00 00 00    	imul   $0xbc,%eax,%eax
80103b1a:	05 80 43 11 80       	add    $0x80114380,%eax
80103b1f:	3b 45 f4             	cmp    -0xc(%ebp),%eax
80103b22:	0f 87 57 ff ff ff    	ja     80103a7f <startothers+0x3c>

    // wait for cpu to finish mpmain()
    while(c->started == 0)
      ;
  }
}
80103b28:	90                   	nop
80103b29:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80103b2c:	c9                   	leave  
80103b2d:	c3                   	ret    

80103b2e <p2v>:
80103b2e:	55                   	push   %ebp
80103b2f:	89 e5                	mov    %esp,%ebp
80103b31:	8b 45 08             	mov    0x8(%ebp),%eax
80103b34:	05 00 00 00 80       	add    $0x80000000,%eax
80103b39:	5d                   	pop    %ebp
80103b3a:	c3                   	ret    

80103b3b <inb>:

// end of CS333 added routines

static inline uchar
inb(ushort port)
{
80103b3b:	55                   	push   %ebp
80103b3c:	89 e5                	mov    %esp,%ebp
80103b3e:	83 ec 14             	sub    $0x14,%esp
80103b41:	8b 45 08             	mov    0x8(%ebp),%eax
80103b44:	66 89 45 ec          	mov    %ax,-0x14(%ebp)
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80103b48:	0f b7 45 ec          	movzwl -0x14(%ebp),%eax
80103b4c:	89 c2                	mov    %eax,%edx
80103b4e:	ec                   	in     (%dx),%al
80103b4f:	88 45 ff             	mov    %al,-0x1(%ebp)
  return data;
80103b52:	0f b6 45 ff          	movzbl -0x1(%ebp),%eax
}
80103b56:	c9                   	leave  
80103b57:	c3                   	ret    

80103b58 <outb>:
               "memory", "cc");
}

static inline void
outb(ushort port, uchar data)
{
80103b58:	55                   	push   %ebp
80103b59:	89 e5                	mov    %esp,%ebp
80103b5b:	83 ec 08             	sub    $0x8,%esp
80103b5e:	8b 55 08             	mov    0x8(%ebp),%edx
80103b61:	8b 45 0c             	mov    0xc(%ebp),%eax
80103b64:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
80103b68:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80103b6b:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
80103b6f:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
80103b73:	ee                   	out    %al,(%dx)
}
80103b74:	90                   	nop
80103b75:	c9                   	leave  
80103b76:	c3                   	ret    

80103b77 <mpbcpu>:
int ncpu;
uchar ioapicid;

int
mpbcpu(void)
{
80103b77:	55                   	push   %ebp
80103b78:	89 e5                	mov    %esp,%ebp
  return bcpu-cpus;
80103b7a:	a1 64 d6 10 80       	mov    0x8010d664,%eax
80103b7f:	89 c2                	mov    %eax,%edx
80103b81:	b8 80 43 11 80       	mov    $0x80114380,%eax
80103b86:	29 c2                	sub    %eax,%edx
80103b88:	89 d0                	mov    %edx,%eax
80103b8a:	c1 f8 02             	sar    $0x2,%eax
80103b8d:	69 c0 cf 46 7d 67    	imul   $0x677d46cf,%eax,%eax
}
80103b93:	5d                   	pop    %ebp
80103b94:	c3                   	ret    

80103b95 <sum>:

static uchar
sum(uchar *addr, int len)
{
80103b95:	55                   	push   %ebp
80103b96:	89 e5                	mov    %esp,%ebp
80103b98:	83 ec 10             	sub    $0x10,%esp
  int i, sum;
  
  sum = 0;
80103b9b:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%ebp)
  for(i=0; i<len; i++)
80103ba2:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
80103ba9:	eb 15                	jmp    80103bc0 <sum+0x2b>
    sum += addr[i];
80103bab:	8b 55 fc             	mov    -0x4(%ebp),%edx
80103bae:	8b 45 08             	mov    0x8(%ebp),%eax
80103bb1:	01 d0                	add    %edx,%eax
80103bb3:	0f b6 00             	movzbl (%eax),%eax
80103bb6:	0f b6 c0             	movzbl %al,%eax
80103bb9:	01 45 f8             	add    %eax,-0x8(%ebp)
sum(uchar *addr, int len)
{
  int i, sum;
  
  sum = 0;
  for(i=0; i<len; i++)
80103bbc:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
80103bc0:	8b 45 fc             	mov    -0x4(%ebp),%eax
80103bc3:	3b 45 0c             	cmp    0xc(%ebp),%eax
80103bc6:	7c e3                	jl     80103bab <sum+0x16>
    sum += addr[i];
  return sum;
80103bc8:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
80103bcb:	c9                   	leave  
80103bcc:	c3                   	ret    

80103bcd <mpsearch1>:

// Look for an MP structure in the len bytes at addr.
static struct mp*
mpsearch1(uint a, int len)
{
80103bcd:	55                   	push   %ebp
80103bce:	89 e5                	mov    %esp,%ebp
80103bd0:	83 ec 18             	sub    $0x18,%esp
  uchar *e, *p, *addr;

  addr = p2v(a);
80103bd3:	ff 75 08             	pushl  0x8(%ebp)
80103bd6:	e8 53 ff ff ff       	call   80103b2e <p2v>
80103bdb:	83 c4 04             	add    $0x4,%esp
80103bde:	89 45 f0             	mov    %eax,-0x10(%ebp)
  e = addr+len;
80103be1:	8b 55 0c             	mov    0xc(%ebp),%edx
80103be4:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103be7:	01 d0                	add    %edx,%eax
80103be9:	89 45 ec             	mov    %eax,-0x14(%ebp)
  for(p = addr; p < e; p += sizeof(struct mp))
80103bec:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103bef:	89 45 f4             	mov    %eax,-0xc(%ebp)
80103bf2:	eb 36                	jmp    80103c2a <mpsearch1+0x5d>
    if(memcmp(p, "_MP_", 4) == 0 && sum(p, sizeof(struct mp)) == 0)
80103bf4:	83 ec 04             	sub    $0x4,%esp
80103bf7:	6a 04                	push   $0x4
80103bf9:	68 d8 a3 10 80       	push   $0x8010a3d8
80103bfe:	ff 75 f4             	pushl  -0xc(%ebp)
80103c01:	e8 f9 30 00 00       	call   80106cff <memcmp>
80103c06:	83 c4 10             	add    $0x10,%esp
80103c09:	85 c0                	test   %eax,%eax
80103c0b:	75 19                	jne    80103c26 <mpsearch1+0x59>
80103c0d:	83 ec 08             	sub    $0x8,%esp
80103c10:	6a 10                	push   $0x10
80103c12:	ff 75 f4             	pushl  -0xc(%ebp)
80103c15:	e8 7b ff ff ff       	call   80103b95 <sum>
80103c1a:	83 c4 10             	add    $0x10,%esp
80103c1d:	84 c0                	test   %al,%al
80103c1f:	75 05                	jne    80103c26 <mpsearch1+0x59>
      return (struct mp*)p;
80103c21:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103c24:	eb 11                	jmp    80103c37 <mpsearch1+0x6a>
{
  uchar *e, *p, *addr;

  addr = p2v(a);
  e = addr+len;
  for(p = addr; p < e; p += sizeof(struct mp))
80103c26:	83 45 f4 10          	addl   $0x10,-0xc(%ebp)
80103c2a:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103c2d:	3b 45 ec             	cmp    -0x14(%ebp),%eax
80103c30:	72 c2                	jb     80103bf4 <mpsearch1+0x27>
    if(memcmp(p, "_MP_", 4) == 0 && sum(p, sizeof(struct mp)) == 0)
      return (struct mp*)p;
  return 0;
80103c32:	b8 00 00 00 00       	mov    $0x0,%eax
}
80103c37:	c9                   	leave  
80103c38:	c3                   	ret    

80103c39 <mpsearch>:
// 1) in the first KB of the EBDA;
// 2) in the last KB of system base memory;
// 3) in the BIOS ROM between 0xE0000 and 0xFFFFF.
static struct mp*
mpsearch(void)
{
80103c39:	55                   	push   %ebp
80103c3a:	89 e5                	mov    %esp,%ebp
80103c3c:	83 ec 18             	sub    $0x18,%esp
  uchar *bda;
  uint p;
  struct mp *mp;

  bda = (uchar *) P2V(0x400);
80103c3f:	c7 45 f4 00 04 00 80 	movl   $0x80000400,-0xc(%ebp)
  if((p = ((bda[0x0F]<<8)| bda[0x0E]) << 4)){
80103c46:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103c49:	83 c0 0f             	add    $0xf,%eax
80103c4c:	0f b6 00             	movzbl (%eax),%eax
80103c4f:	0f b6 c0             	movzbl %al,%eax
80103c52:	c1 e0 08             	shl    $0x8,%eax
80103c55:	89 c2                	mov    %eax,%edx
80103c57:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103c5a:	83 c0 0e             	add    $0xe,%eax
80103c5d:	0f b6 00             	movzbl (%eax),%eax
80103c60:	0f b6 c0             	movzbl %al,%eax
80103c63:	09 d0                	or     %edx,%eax
80103c65:	c1 e0 04             	shl    $0x4,%eax
80103c68:	89 45 f0             	mov    %eax,-0x10(%ebp)
80103c6b:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80103c6f:	74 21                	je     80103c92 <mpsearch+0x59>
    if((mp = mpsearch1(p, 1024)))
80103c71:	83 ec 08             	sub    $0x8,%esp
80103c74:	68 00 04 00 00       	push   $0x400
80103c79:	ff 75 f0             	pushl  -0x10(%ebp)
80103c7c:	e8 4c ff ff ff       	call   80103bcd <mpsearch1>
80103c81:	83 c4 10             	add    $0x10,%esp
80103c84:	89 45 ec             	mov    %eax,-0x14(%ebp)
80103c87:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
80103c8b:	74 51                	je     80103cde <mpsearch+0xa5>
      return mp;
80103c8d:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103c90:	eb 61                	jmp    80103cf3 <mpsearch+0xba>
  } else {
    p = ((bda[0x14]<<8)|bda[0x13])*1024;
80103c92:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103c95:	83 c0 14             	add    $0x14,%eax
80103c98:	0f b6 00             	movzbl (%eax),%eax
80103c9b:	0f b6 c0             	movzbl %al,%eax
80103c9e:	c1 e0 08             	shl    $0x8,%eax
80103ca1:	89 c2                	mov    %eax,%edx
80103ca3:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103ca6:	83 c0 13             	add    $0x13,%eax
80103ca9:	0f b6 00             	movzbl (%eax),%eax
80103cac:	0f b6 c0             	movzbl %al,%eax
80103caf:	09 d0                	or     %edx,%eax
80103cb1:	c1 e0 0a             	shl    $0xa,%eax
80103cb4:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if((mp = mpsearch1(p-1024, 1024)))
80103cb7:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103cba:	2d 00 04 00 00       	sub    $0x400,%eax
80103cbf:	83 ec 08             	sub    $0x8,%esp
80103cc2:	68 00 04 00 00       	push   $0x400
80103cc7:	50                   	push   %eax
80103cc8:	e8 00 ff ff ff       	call   80103bcd <mpsearch1>
80103ccd:	83 c4 10             	add    $0x10,%esp
80103cd0:	89 45 ec             	mov    %eax,-0x14(%ebp)
80103cd3:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
80103cd7:	74 05                	je     80103cde <mpsearch+0xa5>
      return mp;
80103cd9:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103cdc:	eb 15                	jmp    80103cf3 <mpsearch+0xba>
  }
  return mpsearch1(0xF0000, 0x10000);
80103cde:	83 ec 08             	sub    $0x8,%esp
80103ce1:	68 00 00 01 00       	push   $0x10000
80103ce6:	68 00 00 0f 00       	push   $0xf0000
80103ceb:	e8 dd fe ff ff       	call   80103bcd <mpsearch1>
80103cf0:	83 c4 10             	add    $0x10,%esp
}
80103cf3:	c9                   	leave  
80103cf4:	c3                   	ret    

80103cf5 <mpconfig>:
// Check for correct signature, calculate the checksum and,
// if correct, check the version.
// To do: check extended table checksum.
static struct mpconf*
mpconfig(struct mp **pmp)
{
80103cf5:	55                   	push   %ebp
80103cf6:	89 e5                	mov    %esp,%ebp
80103cf8:	83 ec 18             	sub    $0x18,%esp
  struct mpconf *conf;
  struct mp *mp;

  if((mp = mpsearch()) == 0 || mp->physaddr == 0)
80103cfb:	e8 39 ff ff ff       	call   80103c39 <mpsearch>
80103d00:	89 45 f4             	mov    %eax,-0xc(%ebp)
80103d03:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80103d07:	74 0a                	je     80103d13 <mpconfig+0x1e>
80103d09:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103d0c:	8b 40 04             	mov    0x4(%eax),%eax
80103d0f:	85 c0                	test   %eax,%eax
80103d11:	75 0a                	jne    80103d1d <mpconfig+0x28>
    return 0;
80103d13:	b8 00 00 00 00       	mov    $0x0,%eax
80103d18:	e9 81 00 00 00       	jmp    80103d9e <mpconfig+0xa9>
  conf = (struct mpconf*) p2v((uint) mp->physaddr);
80103d1d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103d20:	8b 40 04             	mov    0x4(%eax),%eax
80103d23:	83 ec 0c             	sub    $0xc,%esp
80103d26:	50                   	push   %eax
80103d27:	e8 02 fe ff ff       	call   80103b2e <p2v>
80103d2c:	83 c4 10             	add    $0x10,%esp
80103d2f:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(memcmp(conf, "PCMP", 4) != 0)
80103d32:	83 ec 04             	sub    $0x4,%esp
80103d35:	6a 04                	push   $0x4
80103d37:	68 dd a3 10 80       	push   $0x8010a3dd
80103d3c:	ff 75 f0             	pushl  -0x10(%ebp)
80103d3f:	e8 bb 2f 00 00       	call   80106cff <memcmp>
80103d44:	83 c4 10             	add    $0x10,%esp
80103d47:	85 c0                	test   %eax,%eax
80103d49:	74 07                	je     80103d52 <mpconfig+0x5d>
    return 0;
80103d4b:	b8 00 00 00 00       	mov    $0x0,%eax
80103d50:	eb 4c                	jmp    80103d9e <mpconfig+0xa9>
  if(conf->version != 1 && conf->version != 4)
80103d52:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103d55:	0f b6 40 06          	movzbl 0x6(%eax),%eax
80103d59:	3c 01                	cmp    $0x1,%al
80103d5b:	74 12                	je     80103d6f <mpconfig+0x7a>
80103d5d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103d60:	0f b6 40 06          	movzbl 0x6(%eax),%eax
80103d64:	3c 04                	cmp    $0x4,%al
80103d66:	74 07                	je     80103d6f <mpconfig+0x7a>
    return 0;
80103d68:	b8 00 00 00 00       	mov    $0x0,%eax
80103d6d:	eb 2f                	jmp    80103d9e <mpconfig+0xa9>
  if(sum((uchar*)conf, conf->length) != 0)
80103d6f:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103d72:	0f b7 40 04          	movzwl 0x4(%eax),%eax
80103d76:	0f b7 c0             	movzwl %ax,%eax
80103d79:	83 ec 08             	sub    $0x8,%esp
80103d7c:	50                   	push   %eax
80103d7d:	ff 75 f0             	pushl  -0x10(%ebp)
80103d80:	e8 10 fe ff ff       	call   80103b95 <sum>
80103d85:	83 c4 10             	add    $0x10,%esp
80103d88:	84 c0                	test   %al,%al
80103d8a:	74 07                	je     80103d93 <mpconfig+0x9e>
    return 0;
80103d8c:	b8 00 00 00 00       	mov    $0x0,%eax
80103d91:	eb 0b                	jmp    80103d9e <mpconfig+0xa9>
  *pmp = mp;
80103d93:	8b 45 08             	mov    0x8(%ebp),%eax
80103d96:	8b 55 f4             	mov    -0xc(%ebp),%edx
80103d99:	89 10                	mov    %edx,(%eax)
  return conf;
80103d9b:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
80103d9e:	c9                   	leave  
80103d9f:	c3                   	ret    

80103da0 <mpinit>:

void
mpinit(void)
{
80103da0:	55                   	push   %ebp
80103da1:	89 e5                	mov    %esp,%ebp
80103da3:	83 ec 28             	sub    $0x28,%esp
  struct mp *mp;
  struct mpconf *conf;
  struct mpproc *proc;
  struct mpioapic *ioapic;

  bcpu = &cpus[0];
80103da6:	c7 05 64 d6 10 80 80 	movl   $0x80114380,0x8010d664
80103dad:	43 11 80 
  if((conf = mpconfig(&mp)) == 0)
80103db0:	83 ec 0c             	sub    $0xc,%esp
80103db3:	8d 45 e0             	lea    -0x20(%ebp),%eax
80103db6:	50                   	push   %eax
80103db7:	e8 39 ff ff ff       	call   80103cf5 <mpconfig>
80103dbc:	83 c4 10             	add    $0x10,%esp
80103dbf:	89 45 f0             	mov    %eax,-0x10(%ebp)
80103dc2:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80103dc6:	0f 84 96 01 00 00    	je     80103f62 <mpinit+0x1c2>
    return;
  ismp = 1;
80103dcc:	c7 05 64 43 11 80 01 	movl   $0x1,0x80114364
80103dd3:	00 00 00 
  lapic = (uint*)conf->lapicaddr;
80103dd6:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103dd9:	8b 40 24             	mov    0x24(%eax),%eax
80103ddc:	a3 7c 42 11 80       	mov    %eax,0x8011427c
  for(p=(uchar*)(conf+1), e=(uchar*)conf+conf->length; p<e; ){
80103de1:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103de4:	83 c0 2c             	add    $0x2c,%eax
80103de7:	89 45 f4             	mov    %eax,-0xc(%ebp)
80103dea:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103ded:	0f b7 40 04          	movzwl 0x4(%eax),%eax
80103df1:	0f b7 d0             	movzwl %ax,%edx
80103df4:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103df7:	01 d0                	add    %edx,%eax
80103df9:	89 45 ec             	mov    %eax,-0x14(%ebp)
80103dfc:	e9 f2 00 00 00       	jmp    80103ef3 <mpinit+0x153>
    switch(*p){
80103e01:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103e04:	0f b6 00             	movzbl (%eax),%eax
80103e07:	0f b6 c0             	movzbl %al,%eax
80103e0a:	83 f8 04             	cmp    $0x4,%eax
80103e0d:	0f 87 bc 00 00 00    	ja     80103ecf <mpinit+0x12f>
80103e13:	8b 04 85 20 a4 10 80 	mov    -0x7fef5be0(,%eax,4),%eax
80103e1a:	ff e0                	jmp    *%eax
    case MPPROC:
      proc = (struct mpproc*)p;
80103e1c:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103e1f:	89 45 e8             	mov    %eax,-0x18(%ebp)
      if(ncpu != proc->apicid){
80103e22:	8b 45 e8             	mov    -0x18(%ebp),%eax
80103e25:	0f b6 40 01          	movzbl 0x1(%eax),%eax
80103e29:	0f b6 d0             	movzbl %al,%edx
80103e2c:	a1 60 49 11 80       	mov    0x80114960,%eax
80103e31:	39 c2                	cmp    %eax,%edx
80103e33:	74 2b                	je     80103e60 <mpinit+0xc0>
        cprintf("mpinit: ncpu=%d apicid=%d\n", ncpu, proc->apicid);
80103e35:	8b 45 e8             	mov    -0x18(%ebp),%eax
80103e38:	0f b6 40 01          	movzbl 0x1(%eax),%eax
80103e3c:	0f b6 d0             	movzbl %al,%edx
80103e3f:	a1 60 49 11 80       	mov    0x80114960,%eax
80103e44:	83 ec 04             	sub    $0x4,%esp
80103e47:	52                   	push   %edx
80103e48:	50                   	push   %eax
80103e49:	68 e2 a3 10 80       	push   $0x8010a3e2
80103e4e:	e8 73 c5 ff ff       	call   801003c6 <cprintf>
80103e53:	83 c4 10             	add    $0x10,%esp
        ismp = 0;
80103e56:	c7 05 64 43 11 80 00 	movl   $0x0,0x80114364
80103e5d:	00 00 00 
      }
      if(proc->flags & MPBOOT)
80103e60:	8b 45 e8             	mov    -0x18(%ebp),%eax
80103e63:	0f b6 40 03          	movzbl 0x3(%eax),%eax
80103e67:	0f b6 c0             	movzbl %al,%eax
80103e6a:	83 e0 02             	and    $0x2,%eax
80103e6d:	85 c0                	test   %eax,%eax
80103e6f:	74 15                	je     80103e86 <mpinit+0xe6>
        bcpu = &cpus[ncpu];
80103e71:	a1 60 49 11 80       	mov    0x80114960,%eax
80103e76:	69 c0 bc 00 00 00    	imul   $0xbc,%eax,%eax
80103e7c:	05 80 43 11 80       	add    $0x80114380,%eax
80103e81:	a3 64 d6 10 80       	mov    %eax,0x8010d664
      cpus[ncpu].id = ncpu;
80103e86:	a1 60 49 11 80       	mov    0x80114960,%eax
80103e8b:	8b 15 60 49 11 80    	mov    0x80114960,%edx
80103e91:	69 c0 bc 00 00 00    	imul   $0xbc,%eax,%eax
80103e97:	05 80 43 11 80       	add    $0x80114380,%eax
80103e9c:	88 10                	mov    %dl,(%eax)
      ncpu++;
80103e9e:	a1 60 49 11 80       	mov    0x80114960,%eax
80103ea3:	83 c0 01             	add    $0x1,%eax
80103ea6:	a3 60 49 11 80       	mov    %eax,0x80114960
      p += sizeof(struct mpproc);
80103eab:	83 45 f4 14          	addl   $0x14,-0xc(%ebp)
      continue;
80103eaf:	eb 42                	jmp    80103ef3 <mpinit+0x153>
    case MPIOAPIC:
      ioapic = (struct mpioapic*)p;
80103eb1:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103eb4:	89 45 e4             	mov    %eax,-0x1c(%ebp)
      ioapicid = ioapic->apicno;
80103eb7:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80103eba:	0f b6 40 01          	movzbl 0x1(%eax),%eax
80103ebe:	a2 60 43 11 80       	mov    %al,0x80114360
      p += sizeof(struct mpioapic);
80103ec3:	83 45 f4 08          	addl   $0x8,-0xc(%ebp)
      continue;
80103ec7:	eb 2a                	jmp    80103ef3 <mpinit+0x153>
    case MPBUS:
    case MPIOINTR:
    case MPLINTR:
      p += 8;
80103ec9:	83 45 f4 08          	addl   $0x8,-0xc(%ebp)
      continue;
80103ecd:	eb 24                	jmp    80103ef3 <mpinit+0x153>
    default:
      cprintf("mpinit: unknown config type %x\n", *p);
80103ecf:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103ed2:	0f b6 00             	movzbl (%eax),%eax
80103ed5:	0f b6 c0             	movzbl %al,%eax
80103ed8:	83 ec 08             	sub    $0x8,%esp
80103edb:	50                   	push   %eax
80103edc:	68 00 a4 10 80       	push   $0x8010a400
80103ee1:	e8 e0 c4 ff ff       	call   801003c6 <cprintf>
80103ee6:	83 c4 10             	add    $0x10,%esp
      ismp = 0;
80103ee9:	c7 05 64 43 11 80 00 	movl   $0x0,0x80114364
80103ef0:	00 00 00 
  bcpu = &cpus[0];
  if((conf = mpconfig(&mp)) == 0)
    return;
  ismp = 1;
  lapic = (uint*)conf->lapicaddr;
  for(p=(uchar*)(conf+1), e=(uchar*)conf+conf->length; p<e; ){
80103ef3:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103ef6:	3b 45 ec             	cmp    -0x14(%ebp),%eax
80103ef9:	0f 82 02 ff ff ff    	jb     80103e01 <mpinit+0x61>
    default:
      cprintf("mpinit: unknown config type %x\n", *p);
      ismp = 0;
    }
  }
  if(!ismp){
80103eff:	a1 64 43 11 80       	mov    0x80114364,%eax
80103f04:	85 c0                	test   %eax,%eax
80103f06:	75 1d                	jne    80103f25 <mpinit+0x185>
    // Didn't like what we found; fall back to no MP.
    ncpu = 1;
80103f08:	c7 05 60 49 11 80 01 	movl   $0x1,0x80114960
80103f0f:	00 00 00 
    lapic = 0;
80103f12:	c7 05 7c 42 11 80 00 	movl   $0x0,0x8011427c
80103f19:	00 00 00 
    ioapicid = 0;
80103f1c:	c6 05 60 43 11 80 00 	movb   $0x0,0x80114360
    return;
80103f23:	eb 3e                	jmp    80103f63 <mpinit+0x1c3>
  }

  if(mp->imcrp){
80103f25:	8b 45 e0             	mov    -0x20(%ebp),%eax
80103f28:	0f b6 40 0c          	movzbl 0xc(%eax),%eax
80103f2c:	84 c0                	test   %al,%al
80103f2e:	74 33                	je     80103f63 <mpinit+0x1c3>
    // Bochs doesn't support IMCR, so this doesn't run on Bochs.
    // But it would on real hardware.
    outb(0x22, 0x70);   // Select IMCR
80103f30:	83 ec 08             	sub    $0x8,%esp
80103f33:	6a 70                	push   $0x70
80103f35:	6a 22                	push   $0x22
80103f37:	e8 1c fc ff ff       	call   80103b58 <outb>
80103f3c:	83 c4 10             	add    $0x10,%esp
    outb(0x23, inb(0x23) | 1);  // Mask external interrupts.
80103f3f:	83 ec 0c             	sub    $0xc,%esp
80103f42:	6a 23                	push   $0x23
80103f44:	e8 f2 fb ff ff       	call   80103b3b <inb>
80103f49:	83 c4 10             	add    $0x10,%esp
80103f4c:	83 c8 01             	or     $0x1,%eax
80103f4f:	0f b6 c0             	movzbl %al,%eax
80103f52:	83 ec 08             	sub    $0x8,%esp
80103f55:	50                   	push   %eax
80103f56:	6a 23                	push   $0x23
80103f58:	e8 fb fb ff ff       	call   80103b58 <outb>
80103f5d:	83 c4 10             	add    $0x10,%esp
80103f60:	eb 01                	jmp    80103f63 <mpinit+0x1c3>
  struct mpproc *proc;
  struct mpioapic *ioapic;

  bcpu = &cpus[0];
  if((conf = mpconfig(&mp)) == 0)
    return;
80103f62:	90                   	nop
    // Bochs doesn't support IMCR, so this doesn't run on Bochs.
    // But it would on real hardware.
    outb(0x22, 0x70);   // Select IMCR
    outb(0x23, inb(0x23) | 1);  // Mask external interrupts.
  }
}
80103f63:	c9                   	leave  
80103f64:	c3                   	ret    

80103f65 <outb>:
               "memory", "cc");
}

static inline void
outb(ushort port, uchar data)
{
80103f65:	55                   	push   %ebp
80103f66:	89 e5                	mov    %esp,%ebp
80103f68:	83 ec 08             	sub    $0x8,%esp
80103f6b:	8b 55 08             	mov    0x8(%ebp),%edx
80103f6e:	8b 45 0c             	mov    0xc(%ebp),%eax
80103f71:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
80103f75:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80103f78:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
80103f7c:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
80103f80:	ee                   	out    %al,(%dx)
}
80103f81:	90                   	nop
80103f82:	c9                   	leave  
80103f83:	c3                   	ret    

80103f84 <picsetmask>:
// Initial IRQ mask has interrupt 2 enabled (for slave 8259A).
static ushort irqmask = 0xFFFF & ~(1<<IRQ_SLAVE);

static void
picsetmask(ushort mask)
{
80103f84:	55                   	push   %ebp
80103f85:	89 e5                	mov    %esp,%ebp
80103f87:	83 ec 04             	sub    $0x4,%esp
80103f8a:	8b 45 08             	mov    0x8(%ebp),%eax
80103f8d:	66 89 45 fc          	mov    %ax,-0x4(%ebp)
  irqmask = mask;
80103f91:	0f b7 45 fc          	movzwl -0x4(%ebp),%eax
80103f95:	66 a3 00 d0 10 80    	mov    %ax,0x8010d000
  outb(IO_PIC1+1, mask);
80103f9b:	0f b7 45 fc          	movzwl -0x4(%ebp),%eax
80103f9f:	0f b6 c0             	movzbl %al,%eax
80103fa2:	50                   	push   %eax
80103fa3:	6a 21                	push   $0x21
80103fa5:	e8 bb ff ff ff       	call   80103f65 <outb>
80103faa:	83 c4 08             	add    $0x8,%esp
  outb(IO_PIC2+1, mask >> 8);
80103fad:	0f b7 45 fc          	movzwl -0x4(%ebp),%eax
80103fb1:	66 c1 e8 08          	shr    $0x8,%ax
80103fb5:	0f b6 c0             	movzbl %al,%eax
80103fb8:	50                   	push   %eax
80103fb9:	68 a1 00 00 00       	push   $0xa1
80103fbe:	e8 a2 ff ff ff       	call   80103f65 <outb>
80103fc3:	83 c4 08             	add    $0x8,%esp
}
80103fc6:	90                   	nop
80103fc7:	c9                   	leave  
80103fc8:	c3                   	ret    

80103fc9 <picenable>:

void
picenable(int irq)
{
80103fc9:	55                   	push   %ebp
80103fca:	89 e5                	mov    %esp,%ebp
  picsetmask(irqmask & ~(1<<irq));
80103fcc:	8b 45 08             	mov    0x8(%ebp),%eax
80103fcf:	ba 01 00 00 00       	mov    $0x1,%edx
80103fd4:	89 c1                	mov    %eax,%ecx
80103fd6:	d3 e2                	shl    %cl,%edx
80103fd8:	89 d0                	mov    %edx,%eax
80103fda:	f7 d0                	not    %eax
80103fdc:	89 c2                	mov    %eax,%edx
80103fde:	0f b7 05 00 d0 10 80 	movzwl 0x8010d000,%eax
80103fe5:	21 d0                	and    %edx,%eax
80103fe7:	0f b7 c0             	movzwl %ax,%eax
80103fea:	50                   	push   %eax
80103feb:	e8 94 ff ff ff       	call   80103f84 <picsetmask>
80103ff0:	83 c4 04             	add    $0x4,%esp
}
80103ff3:	90                   	nop
80103ff4:	c9                   	leave  
80103ff5:	c3                   	ret    

80103ff6 <picinit>:

// Initialize the 8259A interrupt controllers.
void
picinit(void)
{
80103ff6:	55                   	push   %ebp
80103ff7:	89 e5                	mov    %esp,%ebp
  // mask all interrupts
  outb(IO_PIC1+1, 0xFF);
80103ff9:	68 ff 00 00 00       	push   $0xff
80103ffe:	6a 21                	push   $0x21
80104000:	e8 60 ff ff ff       	call   80103f65 <outb>
80104005:	83 c4 08             	add    $0x8,%esp
  outb(IO_PIC2+1, 0xFF);
80104008:	68 ff 00 00 00       	push   $0xff
8010400d:	68 a1 00 00 00       	push   $0xa1
80104012:	e8 4e ff ff ff       	call   80103f65 <outb>
80104017:	83 c4 08             	add    $0x8,%esp

  // ICW1:  0001g0hi
  //    g:  0 = edge triggering, 1 = level triggering
  //    h:  0 = cascaded PICs, 1 = master only
  //    i:  0 = no ICW4, 1 = ICW4 required
  outb(IO_PIC1, 0x11);
8010401a:	6a 11                	push   $0x11
8010401c:	6a 20                	push   $0x20
8010401e:	e8 42 ff ff ff       	call   80103f65 <outb>
80104023:	83 c4 08             	add    $0x8,%esp

  // ICW2:  Vector offset
  outb(IO_PIC1+1, T_IRQ0);
80104026:	6a 20                	push   $0x20
80104028:	6a 21                	push   $0x21
8010402a:	e8 36 ff ff ff       	call   80103f65 <outb>
8010402f:	83 c4 08             	add    $0x8,%esp

  // ICW3:  (master PIC) bit mask of IR lines connected to slaves
  //        (slave PIC) 3-bit # of slave's connection to master
  outb(IO_PIC1+1, 1<<IRQ_SLAVE);
80104032:	6a 04                	push   $0x4
80104034:	6a 21                	push   $0x21
80104036:	e8 2a ff ff ff       	call   80103f65 <outb>
8010403b:	83 c4 08             	add    $0x8,%esp
  //    m:  0 = slave PIC, 1 = master PIC
  //      (ignored when b is 0, as the master/slave role
  //      can be hardwired).
  //    a:  1 = Automatic EOI mode
  //    p:  0 = MCS-80/85 mode, 1 = intel x86 mode
  outb(IO_PIC1+1, 0x3);
8010403e:	6a 03                	push   $0x3
80104040:	6a 21                	push   $0x21
80104042:	e8 1e ff ff ff       	call   80103f65 <outb>
80104047:	83 c4 08             	add    $0x8,%esp

  // Set up slave (8259A-2)
  outb(IO_PIC2, 0x11);                  // ICW1
8010404a:	6a 11                	push   $0x11
8010404c:	68 a0 00 00 00       	push   $0xa0
80104051:	e8 0f ff ff ff       	call   80103f65 <outb>
80104056:	83 c4 08             	add    $0x8,%esp
  outb(IO_PIC2+1, T_IRQ0 + 8);      // ICW2
80104059:	6a 28                	push   $0x28
8010405b:	68 a1 00 00 00       	push   $0xa1
80104060:	e8 00 ff ff ff       	call   80103f65 <outb>
80104065:	83 c4 08             	add    $0x8,%esp
  outb(IO_PIC2+1, IRQ_SLAVE);           // ICW3
80104068:	6a 02                	push   $0x2
8010406a:	68 a1 00 00 00       	push   $0xa1
8010406f:	e8 f1 fe ff ff       	call   80103f65 <outb>
80104074:	83 c4 08             	add    $0x8,%esp
  // NB Automatic EOI mode doesn't tend to work on the slave.
  // Linux source code says it's "to be investigated".
  outb(IO_PIC2+1, 0x3);                 // ICW4
80104077:	6a 03                	push   $0x3
80104079:	68 a1 00 00 00       	push   $0xa1
8010407e:	e8 e2 fe ff ff       	call   80103f65 <outb>
80104083:	83 c4 08             	add    $0x8,%esp

  // OCW3:  0ef01prs
  //   ef:  0x = NOP, 10 = clear specific mask, 11 = set specific mask
  //    p:  0 = no polling, 1 = polling mode
  //   rs:  0x = NOP, 10 = read IRR, 11 = read ISR
  outb(IO_PIC1, 0x68);             // clear specific mask
80104086:	6a 68                	push   $0x68
80104088:	6a 20                	push   $0x20
8010408a:	e8 d6 fe ff ff       	call   80103f65 <outb>
8010408f:	83 c4 08             	add    $0x8,%esp
  outb(IO_PIC1, 0x0a);             // read IRR by default
80104092:	6a 0a                	push   $0xa
80104094:	6a 20                	push   $0x20
80104096:	e8 ca fe ff ff       	call   80103f65 <outb>
8010409b:	83 c4 08             	add    $0x8,%esp

  outb(IO_PIC2, 0x68);             // OCW3
8010409e:	6a 68                	push   $0x68
801040a0:	68 a0 00 00 00       	push   $0xa0
801040a5:	e8 bb fe ff ff       	call   80103f65 <outb>
801040aa:	83 c4 08             	add    $0x8,%esp
  outb(IO_PIC2, 0x0a);             // OCW3
801040ad:	6a 0a                	push   $0xa
801040af:	68 a0 00 00 00       	push   $0xa0
801040b4:	e8 ac fe ff ff       	call   80103f65 <outb>
801040b9:	83 c4 08             	add    $0x8,%esp

  if(irqmask != 0xFFFF)
801040bc:	0f b7 05 00 d0 10 80 	movzwl 0x8010d000,%eax
801040c3:	66 83 f8 ff          	cmp    $0xffff,%ax
801040c7:	74 13                	je     801040dc <picinit+0xe6>
    picsetmask(irqmask);
801040c9:	0f b7 05 00 d0 10 80 	movzwl 0x8010d000,%eax
801040d0:	0f b7 c0             	movzwl %ax,%eax
801040d3:	50                   	push   %eax
801040d4:	e8 ab fe ff ff       	call   80103f84 <picsetmask>
801040d9:	83 c4 04             	add    $0x4,%esp
}
801040dc:	90                   	nop
801040dd:	c9                   	leave  
801040de:	c3                   	ret    

801040df <pipealloc>:
  int writeopen;  // write fd is still open
};

int
pipealloc(struct file **f0, struct file **f1)
{
801040df:	55                   	push   %ebp
801040e0:	89 e5                	mov    %esp,%ebp
801040e2:	83 ec 18             	sub    $0x18,%esp
  struct pipe *p;

  p = 0;
801040e5:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  *f0 = *f1 = 0;
801040ec:	8b 45 0c             	mov    0xc(%ebp),%eax
801040ef:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
801040f5:	8b 45 0c             	mov    0xc(%ebp),%eax
801040f8:	8b 10                	mov    (%eax),%edx
801040fa:	8b 45 08             	mov    0x8(%ebp),%eax
801040fd:	89 10                	mov    %edx,(%eax)
  if((*f0 = filealloc()) == 0 || (*f1 = filealloc()) == 0)
801040ff:	e8 2d cf ff ff       	call   80101031 <filealloc>
80104104:	89 c2                	mov    %eax,%edx
80104106:	8b 45 08             	mov    0x8(%ebp),%eax
80104109:	89 10                	mov    %edx,(%eax)
8010410b:	8b 45 08             	mov    0x8(%ebp),%eax
8010410e:	8b 00                	mov    (%eax),%eax
80104110:	85 c0                	test   %eax,%eax
80104112:	0f 84 cb 00 00 00    	je     801041e3 <pipealloc+0x104>
80104118:	e8 14 cf ff ff       	call   80101031 <filealloc>
8010411d:	89 c2                	mov    %eax,%edx
8010411f:	8b 45 0c             	mov    0xc(%ebp),%eax
80104122:	89 10                	mov    %edx,(%eax)
80104124:	8b 45 0c             	mov    0xc(%ebp),%eax
80104127:	8b 00                	mov    (%eax),%eax
80104129:	85 c0                	test   %eax,%eax
8010412b:	0f 84 b2 00 00 00    	je     801041e3 <pipealloc+0x104>
    goto bad;
  if((p = (struct pipe*)kalloc()) == 0)
80104131:	e8 ce eb ff ff       	call   80102d04 <kalloc>
80104136:	89 45 f4             	mov    %eax,-0xc(%ebp)
80104139:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
8010413d:	0f 84 9f 00 00 00    	je     801041e2 <pipealloc+0x103>
    goto bad;
  p->readopen = 1;
80104143:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104146:	c7 80 3c 02 00 00 01 	movl   $0x1,0x23c(%eax)
8010414d:	00 00 00 
  p->writeopen = 1;
80104150:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104153:	c7 80 40 02 00 00 01 	movl   $0x1,0x240(%eax)
8010415a:	00 00 00 
  p->nwrite = 0;
8010415d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104160:	c7 80 38 02 00 00 00 	movl   $0x0,0x238(%eax)
80104167:	00 00 00 
  p->nread = 0;
8010416a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010416d:	c7 80 34 02 00 00 00 	movl   $0x0,0x234(%eax)
80104174:	00 00 00 
  initlock(&p->lock, "pipe");
80104177:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010417a:	83 ec 08             	sub    $0x8,%esp
8010417d:	68 34 a4 10 80       	push   $0x8010a434
80104182:	50                   	push   %eax
80104183:	e8 8b 28 00 00       	call   80106a13 <initlock>
80104188:	83 c4 10             	add    $0x10,%esp
  (*f0)->type = FD_PIPE;
8010418b:	8b 45 08             	mov    0x8(%ebp),%eax
8010418e:	8b 00                	mov    (%eax),%eax
80104190:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
  (*f0)->readable = 1;
80104196:	8b 45 08             	mov    0x8(%ebp),%eax
80104199:	8b 00                	mov    (%eax),%eax
8010419b:	c6 40 08 01          	movb   $0x1,0x8(%eax)
  (*f0)->writable = 0;
8010419f:	8b 45 08             	mov    0x8(%ebp),%eax
801041a2:	8b 00                	mov    (%eax),%eax
801041a4:	c6 40 09 00          	movb   $0x0,0x9(%eax)
  (*f0)->pipe = p;
801041a8:	8b 45 08             	mov    0x8(%ebp),%eax
801041ab:	8b 00                	mov    (%eax),%eax
801041ad:	8b 55 f4             	mov    -0xc(%ebp),%edx
801041b0:	89 50 0c             	mov    %edx,0xc(%eax)
  (*f1)->type = FD_PIPE;
801041b3:	8b 45 0c             	mov    0xc(%ebp),%eax
801041b6:	8b 00                	mov    (%eax),%eax
801041b8:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
  (*f1)->readable = 0;
801041be:	8b 45 0c             	mov    0xc(%ebp),%eax
801041c1:	8b 00                	mov    (%eax),%eax
801041c3:	c6 40 08 00          	movb   $0x0,0x8(%eax)
  (*f1)->writable = 1;
801041c7:	8b 45 0c             	mov    0xc(%ebp),%eax
801041ca:	8b 00                	mov    (%eax),%eax
801041cc:	c6 40 09 01          	movb   $0x1,0x9(%eax)
  (*f1)->pipe = p;
801041d0:	8b 45 0c             	mov    0xc(%ebp),%eax
801041d3:	8b 00                	mov    (%eax),%eax
801041d5:	8b 55 f4             	mov    -0xc(%ebp),%edx
801041d8:	89 50 0c             	mov    %edx,0xc(%eax)
  return 0;
801041db:	b8 00 00 00 00       	mov    $0x0,%eax
801041e0:	eb 4e                	jmp    80104230 <pipealloc+0x151>
  p = 0;
  *f0 = *f1 = 0;
  if((*f0 = filealloc()) == 0 || (*f1 = filealloc()) == 0)
    goto bad;
  if((p = (struct pipe*)kalloc()) == 0)
    goto bad;
801041e2:	90                   	nop
  (*f1)->writable = 1;
  (*f1)->pipe = p;
  return 0;

 bad:
  if(p)
801041e3:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801041e7:	74 0e                	je     801041f7 <pipealloc+0x118>
    kfree((char*)p);
801041e9:	83 ec 0c             	sub    $0xc,%esp
801041ec:	ff 75 f4             	pushl  -0xc(%ebp)
801041ef:	e8 73 ea ff ff       	call   80102c67 <kfree>
801041f4:	83 c4 10             	add    $0x10,%esp
  if(*f0)
801041f7:	8b 45 08             	mov    0x8(%ebp),%eax
801041fa:	8b 00                	mov    (%eax),%eax
801041fc:	85 c0                	test   %eax,%eax
801041fe:	74 11                	je     80104211 <pipealloc+0x132>
    fileclose(*f0);
80104200:	8b 45 08             	mov    0x8(%ebp),%eax
80104203:	8b 00                	mov    (%eax),%eax
80104205:	83 ec 0c             	sub    $0xc,%esp
80104208:	50                   	push   %eax
80104209:	e8 e1 ce ff ff       	call   801010ef <fileclose>
8010420e:	83 c4 10             	add    $0x10,%esp
  if(*f1)
80104211:	8b 45 0c             	mov    0xc(%ebp),%eax
80104214:	8b 00                	mov    (%eax),%eax
80104216:	85 c0                	test   %eax,%eax
80104218:	74 11                	je     8010422b <pipealloc+0x14c>
    fileclose(*f1);
8010421a:	8b 45 0c             	mov    0xc(%ebp),%eax
8010421d:	8b 00                	mov    (%eax),%eax
8010421f:	83 ec 0c             	sub    $0xc,%esp
80104222:	50                   	push   %eax
80104223:	e8 c7 ce ff ff       	call   801010ef <fileclose>
80104228:	83 c4 10             	add    $0x10,%esp
  return -1;
8010422b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80104230:	c9                   	leave  
80104231:	c3                   	ret    

80104232 <pipeclose>:

void
pipeclose(struct pipe *p, int writable)
{
80104232:	55                   	push   %ebp
80104233:	89 e5                	mov    %esp,%ebp
80104235:	83 ec 08             	sub    $0x8,%esp
  acquire(&p->lock);
80104238:	8b 45 08             	mov    0x8(%ebp),%eax
8010423b:	83 ec 0c             	sub    $0xc,%esp
8010423e:	50                   	push   %eax
8010423f:	e8 f1 27 00 00       	call   80106a35 <acquire>
80104244:	83 c4 10             	add    $0x10,%esp
  if(writable){
80104247:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
8010424b:	74 23                	je     80104270 <pipeclose+0x3e>
    p->writeopen = 0;
8010424d:	8b 45 08             	mov    0x8(%ebp),%eax
80104250:	c7 80 40 02 00 00 00 	movl   $0x0,0x240(%eax)
80104257:	00 00 00 
    wakeup(&p->nread);
8010425a:	8b 45 08             	mov    0x8(%ebp),%eax
8010425d:	05 34 02 00 00       	add    $0x234,%eax
80104262:	83 ec 0c             	sub    $0xc,%esp
80104265:	50                   	push   %eax
80104266:	e8 2a 13 00 00       	call   80105595 <wakeup>
8010426b:	83 c4 10             	add    $0x10,%esp
8010426e:	eb 21                	jmp    80104291 <pipeclose+0x5f>
  } else {
    p->readopen = 0;
80104270:	8b 45 08             	mov    0x8(%ebp),%eax
80104273:	c7 80 3c 02 00 00 00 	movl   $0x0,0x23c(%eax)
8010427a:	00 00 00 
    wakeup(&p->nwrite);
8010427d:	8b 45 08             	mov    0x8(%ebp),%eax
80104280:	05 38 02 00 00       	add    $0x238,%eax
80104285:	83 ec 0c             	sub    $0xc,%esp
80104288:	50                   	push   %eax
80104289:	e8 07 13 00 00       	call   80105595 <wakeup>
8010428e:	83 c4 10             	add    $0x10,%esp
  }
  if(p->readopen == 0 && p->writeopen == 0){
80104291:	8b 45 08             	mov    0x8(%ebp),%eax
80104294:	8b 80 3c 02 00 00    	mov    0x23c(%eax),%eax
8010429a:	85 c0                	test   %eax,%eax
8010429c:	75 2c                	jne    801042ca <pipeclose+0x98>
8010429e:	8b 45 08             	mov    0x8(%ebp),%eax
801042a1:	8b 80 40 02 00 00    	mov    0x240(%eax),%eax
801042a7:	85 c0                	test   %eax,%eax
801042a9:	75 1f                	jne    801042ca <pipeclose+0x98>
    release(&p->lock);
801042ab:	8b 45 08             	mov    0x8(%ebp),%eax
801042ae:	83 ec 0c             	sub    $0xc,%esp
801042b1:	50                   	push   %eax
801042b2:	e8 e5 27 00 00       	call   80106a9c <release>
801042b7:	83 c4 10             	add    $0x10,%esp
    kfree((char*)p);
801042ba:	83 ec 0c             	sub    $0xc,%esp
801042bd:	ff 75 08             	pushl  0x8(%ebp)
801042c0:	e8 a2 e9 ff ff       	call   80102c67 <kfree>
801042c5:	83 c4 10             	add    $0x10,%esp
801042c8:	eb 0f                	jmp    801042d9 <pipeclose+0xa7>
  } else
    release(&p->lock);
801042ca:	8b 45 08             	mov    0x8(%ebp),%eax
801042cd:	83 ec 0c             	sub    $0xc,%esp
801042d0:	50                   	push   %eax
801042d1:	e8 c6 27 00 00       	call   80106a9c <release>
801042d6:	83 c4 10             	add    $0x10,%esp
}
801042d9:	90                   	nop
801042da:	c9                   	leave  
801042db:	c3                   	ret    

801042dc <pipewrite>:

int
pipewrite(struct pipe *p, char *addr, int n)
{
801042dc:	55                   	push   %ebp
801042dd:	89 e5                	mov    %esp,%ebp
801042df:	83 ec 18             	sub    $0x18,%esp
  int i;

  acquire(&p->lock);
801042e2:	8b 45 08             	mov    0x8(%ebp),%eax
801042e5:	83 ec 0c             	sub    $0xc,%esp
801042e8:	50                   	push   %eax
801042e9:	e8 47 27 00 00       	call   80106a35 <acquire>
801042ee:	83 c4 10             	add    $0x10,%esp
  for(i = 0; i < n; i++){
801042f1:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
801042f8:	e9 ad 00 00 00       	jmp    801043aa <pipewrite+0xce>
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
      if(p->readopen == 0 || proc->killed){
801042fd:	8b 45 08             	mov    0x8(%ebp),%eax
80104300:	8b 80 3c 02 00 00    	mov    0x23c(%eax),%eax
80104306:	85 c0                	test   %eax,%eax
80104308:	74 0d                	je     80104317 <pipewrite+0x3b>
8010430a:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104310:	8b 40 2c             	mov    0x2c(%eax),%eax
80104313:	85 c0                	test   %eax,%eax
80104315:	74 19                	je     80104330 <pipewrite+0x54>
        release(&p->lock);
80104317:	8b 45 08             	mov    0x8(%ebp),%eax
8010431a:	83 ec 0c             	sub    $0xc,%esp
8010431d:	50                   	push   %eax
8010431e:	e8 79 27 00 00       	call   80106a9c <release>
80104323:	83 c4 10             	add    $0x10,%esp
        return -1;
80104326:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010432b:	e9 a8 00 00 00       	jmp    801043d8 <pipewrite+0xfc>
      }
      wakeup(&p->nread);
80104330:	8b 45 08             	mov    0x8(%ebp),%eax
80104333:	05 34 02 00 00       	add    $0x234,%eax
80104338:	83 ec 0c             	sub    $0xc,%esp
8010433b:	50                   	push   %eax
8010433c:	e8 54 12 00 00       	call   80105595 <wakeup>
80104341:	83 c4 10             	add    $0x10,%esp
      sleep(&p->nwrite, &p->lock);  //DOC: pipewrite-sleep
80104344:	8b 45 08             	mov    0x8(%ebp),%eax
80104347:	8b 55 08             	mov    0x8(%ebp),%edx
8010434a:	81 c2 38 02 00 00    	add    $0x238,%edx
80104350:	83 ec 08             	sub    $0x8,%esp
80104353:	50                   	push   %eax
80104354:	52                   	push   %edx
80104355:	e8 92 10 00 00       	call   801053ec <sleep>
8010435a:	83 c4 10             	add    $0x10,%esp
{
  int i;

  acquire(&p->lock);
  for(i = 0; i < n; i++){
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
8010435d:	8b 45 08             	mov    0x8(%ebp),%eax
80104360:	8b 90 38 02 00 00    	mov    0x238(%eax),%edx
80104366:	8b 45 08             	mov    0x8(%ebp),%eax
80104369:	8b 80 34 02 00 00    	mov    0x234(%eax),%eax
8010436f:	05 00 02 00 00       	add    $0x200,%eax
80104374:	39 c2                	cmp    %eax,%edx
80104376:	74 85                	je     801042fd <pipewrite+0x21>
        return -1;
      }
      wakeup(&p->nread);
      sleep(&p->nwrite, &p->lock);  //DOC: pipewrite-sleep
    }
    p->data[p->nwrite++ % PIPESIZE] = addr[i];
80104378:	8b 45 08             	mov    0x8(%ebp),%eax
8010437b:	8b 80 38 02 00 00    	mov    0x238(%eax),%eax
80104381:	8d 48 01             	lea    0x1(%eax),%ecx
80104384:	8b 55 08             	mov    0x8(%ebp),%edx
80104387:	89 8a 38 02 00 00    	mov    %ecx,0x238(%edx)
8010438d:	25 ff 01 00 00       	and    $0x1ff,%eax
80104392:	89 c1                	mov    %eax,%ecx
80104394:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104397:	8b 45 0c             	mov    0xc(%ebp),%eax
8010439a:	01 d0                	add    %edx,%eax
8010439c:	0f b6 10             	movzbl (%eax),%edx
8010439f:	8b 45 08             	mov    0x8(%ebp),%eax
801043a2:	88 54 08 34          	mov    %dl,0x34(%eax,%ecx,1)
pipewrite(struct pipe *p, char *addr, int n)
{
  int i;

  acquire(&p->lock);
  for(i = 0; i < n; i++){
801043a6:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
801043aa:	8b 45 f4             	mov    -0xc(%ebp),%eax
801043ad:	3b 45 10             	cmp    0x10(%ebp),%eax
801043b0:	7c ab                	jl     8010435d <pipewrite+0x81>
      wakeup(&p->nread);
      sleep(&p->nwrite, &p->lock);  //DOC: pipewrite-sleep
    }
    p->data[p->nwrite++ % PIPESIZE] = addr[i];
  }
  wakeup(&p->nread);  //DOC: pipewrite-wakeup1
801043b2:	8b 45 08             	mov    0x8(%ebp),%eax
801043b5:	05 34 02 00 00       	add    $0x234,%eax
801043ba:	83 ec 0c             	sub    $0xc,%esp
801043bd:	50                   	push   %eax
801043be:	e8 d2 11 00 00       	call   80105595 <wakeup>
801043c3:	83 c4 10             	add    $0x10,%esp
  release(&p->lock);
801043c6:	8b 45 08             	mov    0x8(%ebp),%eax
801043c9:	83 ec 0c             	sub    $0xc,%esp
801043cc:	50                   	push   %eax
801043cd:	e8 ca 26 00 00       	call   80106a9c <release>
801043d2:	83 c4 10             	add    $0x10,%esp
  return n;
801043d5:	8b 45 10             	mov    0x10(%ebp),%eax
}
801043d8:	c9                   	leave  
801043d9:	c3                   	ret    

801043da <piperead>:

int
piperead(struct pipe *p, char *addr, int n)
{
801043da:	55                   	push   %ebp
801043db:	89 e5                	mov    %esp,%ebp
801043dd:	53                   	push   %ebx
801043de:	83 ec 14             	sub    $0x14,%esp
  int i;

  acquire(&p->lock);
801043e1:	8b 45 08             	mov    0x8(%ebp),%eax
801043e4:	83 ec 0c             	sub    $0xc,%esp
801043e7:	50                   	push   %eax
801043e8:	e8 48 26 00 00       	call   80106a35 <acquire>
801043ed:	83 c4 10             	add    $0x10,%esp
  while(p->nread == p->nwrite && p->writeopen){  //DOC: pipe-empty
801043f0:	eb 3f                	jmp    80104431 <piperead+0x57>
    if(proc->killed){
801043f2:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801043f8:	8b 40 2c             	mov    0x2c(%eax),%eax
801043fb:	85 c0                	test   %eax,%eax
801043fd:	74 19                	je     80104418 <piperead+0x3e>
      release(&p->lock);
801043ff:	8b 45 08             	mov    0x8(%ebp),%eax
80104402:	83 ec 0c             	sub    $0xc,%esp
80104405:	50                   	push   %eax
80104406:	e8 91 26 00 00       	call   80106a9c <release>
8010440b:	83 c4 10             	add    $0x10,%esp
      return -1;
8010440e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104413:	e9 bf 00 00 00       	jmp    801044d7 <piperead+0xfd>
    }
    sleep(&p->nread, &p->lock); //DOC: piperead-sleep
80104418:	8b 45 08             	mov    0x8(%ebp),%eax
8010441b:	8b 55 08             	mov    0x8(%ebp),%edx
8010441e:	81 c2 34 02 00 00    	add    $0x234,%edx
80104424:	83 ec 08             	sub    $0x8,%esp
80104427:	50                   	push   %eax
80104428:	52                   	push   %edx
80104429:	e8 be 0f 00 00       	call   801053ec <sleep>
8010442e:	83 c4 10             	add    $0x10,%esp
piperead(struct pipe *p, char *addr, int n)
{
  int i;

  acquire(&p->lock);
  while(p->nread == p->nwrite && p->writeopen){  //DOC: pipe-empty
80104431:	8b 45 08             	mov    0x8(%ebp),%eax
80104434:	8b 90 34 02 00 00    	mov    0x234(%eax),%edx
8010443a:	8b 45 08             	mov    0x8(%ebp),%eax
8010443d:	8b 80 38 02 00 00    	mov    0x238(%eax),%eax
80104443:	39 c2                	cmp    %eax,%edx
80104445:	75 0d                	jne    80104454 <piperead+0x7a>
80104447:	8b 45 08             	mov    0x8(%ebp),%eax
8010444a:	8b 80 40 02 00 00    	mov    0x240(%eax),%eax
80104450:	85 c0                	test   %eax,%eax
80104452:	75 9e                	jne    801043f2 <piperead+0x18>
      release(&p->lock);
      return -1;
    }
    sleep(&p->nread, &p->lock); //DOC: piperead-sleep
  }
  for(i = 0; i < n; i++){  //DOC: piperead-copy
80104454:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
8010445b:	eb 49                	jmp    801044a6 <piperead+0xcc>
    if(p->nread == p->nwrite)
8010445d:	8b 45 08             	mov    0x8(%ebp),%eax
80104460:	8b 90 34 02 00 00    	mov    0x234(%eax),%edx
80104466:	8b 45 08             	mov    0x8(%ebp),%eax
80104469:	8b 80 38 02 00 00    	mov    0x238(%eax),%eax
8010446f:	39 c2                	cmp    %eax,%edx
80104471:	74 3d                	je     801044b0 <piperead+0xd6>
      break;
    addr[i] = p->data[p->nread++ % PIPESIZE];
80104473:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104476:	8b 45 0c             	mov    0xc(%ebp),%eax
80104479:	8d 1c 02             	lea    (%edx,%eax,1),%ebx
8010447c:	8b 45 08             	mov    0x8(%ebp),%eax
8010447f:	8b 80 34 02 00 00    	mov    0x234(%eax),%eax
80104485:	8d 48 01             	lea    0x1(%eax),%ecx
80104488:	8b 55 08             	mov    0x8(%ebp),%edx
8010448b:	89 8a 34 02 00 00    	mov    %ecx,0x234(%edx)
80104491:	25 ff 01 00 00       	and    $0x1ff,%eax
80104496:	89 c2                	mov    %eax,%edx
80104498:	8b 45 08             	mov    0x8(%ebp),%eax
8010449b:	0f b6 44 10 34       	movzbl 0x34(%eax,%edx,1),%eax
801044a0:	88 03                	mov    %al,(%ebx)
      release(&p->lock);
      return -1;
    }
    sleep(&p->nread, &p->lock); //DOC: piperead-sleep
  }
  for(i = 0; i < n; i++){  //DOC: piperead-copy
801044a2:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
801044a6:	8b 45 f4             	mov    -0xc(%ebp),%eax
801044a9:	3b 45 10             	cmp    0x10(%ebp),%eax
801044ac:	7c af                	jl     8010445d <piperead+0x83>
801044ae:	eb 01                	jmp    801044b1 <piperead+0xd7>
    if(p->nread == p->nwrite)
      break;
801044b0:	90                   	nop
    addr[i] = p->data[p->nread++ % PIPESIZE];
  }
  wakeup(&p->nwrite);  //DOC: piperead-wakeup
801044b1:	8b 45 08             	mov    0x8(%ebp),%eax
801044b4:	05 38 02 00 00       	add    $0x238,%eax
801044b9:	83 ec 0c             	sub    $0xc,%esp
801044bc:	50                   	push   %eax
801044bd:	e8 d3 10 00 00       	call   80105595 <wakeup>
801044c2:	83 c4 10             	add    $0x10,%esp
  release(&p->lock);
801044c5:	8b 45 08             	mov    0x8(%ebp),%eax
801044c8:	83 ec 0c             	sub    $0xc,%esp
801044cb:	50                   	push   %eax
801044cc:	e8 cb 25 00 00       	call   80106a9c <release>
801044d1:	83 c4 10             	add    $0x10,%esp
  return i;
801044d4:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
801044d7:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801044da:	c9                   	leave  
801044db:	c3                   	ret    

801044dc <hlt>:
}

// hlt() added by Noah Zentzis, Fall 2016.
static inline void
hlt()
{
801044dc:	55                   	push   %ebp
801044dd:	89 e5                	mov    %esp,%ebp
  asm volatile("hlt");
801044df:	f4                   	hlt    
}
801044e0:	90                   	nop
801044e1:	5d                   	pop    %ebp
801044e2:	c3                   	ret    

801044e3 <readeflags>:
  asm volatile("ltr %0" : : "r" (sel));
}

static inline uint
readeflags(void)
{
801044e3:	55                   	push   %ebp
801044e4:	89 e5                	mov    %esp,%ebp
801044e6:	83 ec 10             	sub    $0x10,%esp
  uint eflags;
  asm volatile("pushfl; popl %0" : "=r" (eflags));
801044e9:	9c                   	pushf  
801044ea:	58                   	pop    %eax
801044eb:	89 45 fc             	mov    %eax,-0x4(%ebp)
  return eflags;
801044ee:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
801044f1:	c9                   	leave  
801044f2:	c3                   	ret    

801044f3 <sti>:
  asm volatile("cli");
}

static inline void
sti(void)
{
801044f3:	55                   	push   %ebp
801044f4:	89 e5                	mov    %esp,%ebp
  asm volatile("sti");
801044f6:	fb                   	sti    
}
801044f7:	90                   	nop
801044f8:	5d                   	pop    %ebp
801044f9:	c3                   	ret    

801044fa <pinit>:
static void demote(struct proc *proc);
#endif

void
pinit(void)
{
801044fa:	55                   	push   %ebp
801044fb:	89 e5                	mov    %esp,%ebp
801044fd:	83 ec 08             	sub    $0x8,%esp
  initlock(&ptable.lock, "ptable");
80104500:	83 ec 08             	sub    $0x8,%esp
80104503:	68 3c a4 10 80       	push   $0x8010a43c
80104508:	68 80 49 11 80       	push   $0x80114980
8010450d:	e8 01 25 00 00       	call   80106a13 <initlock>
80104512:	83 c4 10             	add    $0x10,%esp
}
80104515:	90                   	nop
80104516:	c9                   	leave  
80104517:	c3                   	ret    

80104518 <allocproc>:
// If found, change state to EMBRYO and initialize
// state required to run in the kernel.
// Otherwise return 0.
static struct proc*															//*********** ALLOCPROC *************
allocproc(void)
{
80104518:	55                   	push   %ebp
80104519:	89 e5                	mov    %esp,%ebp
8010451b:	83 ec 18             	sub    $0x18,%esp
  struct proc *p;
  char *sp;

  acquire(&ptable.lock);
8010451e:	83 ec 0c             	sub    $0xc,%esp
80104521:	68 80 49 11 80       	push   $0x80114980
80104526:	e8 0a 25 00 00       	call   80106a35 <acquire>
8010452b:	83 c4 10             	add    $0x10,%esp
  #ifdef CS333_P3P4
  //removeFromFreeList
  p = ptable.pLists.free;
8010452e:	a1 d4 70 11 80       	mov    0x801170d4,%eax
80104533:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if (p)
80104536:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
8010453a:	0f 84 91 00 00 00    	je     801045d1 <allocproc+0xb9>
  {
    ptable.pLists.free = p->next;
80104540:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104543:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
80104549:	a3 d4 70 11 80       	mov    %eax,0x801170d4
    p->next = 0;
8010454e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104551:	c7 80 90 00 00 00 00 	movl   $0x0,0x90(%eax)
80104558:	00 00 00 
    goto found;
8010455b:	90                   	nop
  #endif
  release(&ptable.lock);
  return 0;

found:
  p->state = EMBRYO;
8010455c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010455f:	c7 40 0c 01 00 00 00 	movl   $0x1,0xc(%eax)
  #ifdef CS333_P3P4
  p->priority = 0;
80104566:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104569:	c7 80 94 00 00 00 00 	movl   $0x0,0x94(%eax)
80104570:	00 00 00 
  p->budget = BUDGET;
80104573:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104576:	c7 80 98 00 00 00 14 	movl   $0x14,0x98(%eax)
8010457d:	00 00 00 
  addToEmbryoList(p);
80104580:	83 ec 0c             	sub    $0xc,%esp
80104583:	ff 75 f4             	pushl  -0xc(%ebp)
80104586:	e8 21 20 00 00       	call   801065ac <addToEmbryoList>
8010458b:	83 c4 10             	add    $0x10,%esp
  #endif
  p->pid = nextpid++;
8010458e:	a1 04 d0 10 80       	mov    0x8010d004,%eax
80104593:	8d 50 01             	lea    0x1(%eax),%edx
80104596:	89 15 04 d0 10 80    	mov    %edx,0x8010d004
8010459c:	89 c2                	mov    %eax,%edx
8010459e:	8b 45 f4             	mov    -0xc(%ebp),%eax
801045a1:	89 50 10             	mov    %edx,0x10(%eax)
  release(&ptable.lock);
801045a4:	83 ec 0c             	sub    $0xc,%esp
801045a7:	68 80 49 11 80       	push   $0x80114980
801045ac:	e8 eb 24 00 00       	call   80106a9c <release>
801045b1:	83 c4 10             	add    $0x10,%esp

  // Allocate kernel stack.
  if((p->kstack = kalloc()) == 0){
801045b4:	e8 4b e7 ff ff       	call   80102d04 <kalloc>
801045b9:	89 c2                	mov    %eax,%edx
801045bb:	8b 45 f4             	mov    -0xc(%ebp),%eax
801045be:	89 50 08             	mov    %edx,0x8(%eax)
801045c1:	8b 45 f4             	mov    -0xc(%ebp),%eax
801045c4:	8b 40 08             	mov    0x8(%eax),%eax
801045c7:	85 c0                	test   %eax,%eax
801045c9:	0f 85 8f 00 00 00    	jne    8010465e <allocproc+0x146>
801045cf:	eb 1a                	jmp    801045eb <allocproc+0xd3>
  #else
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
    if(p->state == UNUSED)
      goto found;
  #endif
  release(&ptable.lock);
801045d1:	83 ec 0c             	sub    $0xc,%esp
801045d4:	68 80 49 11 80       	push   $0x80114980
801045d9:	e8 be 24 00 00       	call   80106a9c <release>
801045de:	83 c4 10             	add    $0x10,%esp
  return 0;
801045e1:	b8 00 00 00 00       	mov    $0x0,%eax
801045e6:	e9 f9 00 00 00       	jmp    801046e4 <allocproc+0x1cc>
  release(&ptable.lock);

  // Allocate kernel stack.
  if((p->kstack = kalloc()) == 0){
    #ifdef CS333_P3P4
    acquire(&ptable.lock);
801045eb:	83 ec 0c             	sub    $0xc,%esp
801045ee:	68 80 49 11 80       	push   $0x80114980
801045f3:	e8 3d 24 00 00       	call   80106a35 <acquire>
801045f8:	83 c4 10             	add    $0x10,%esp
    int rc = removeFromEmbryoList(p);
801045fb:	83 ec 0c             	sub    $0xc,%esp
801045fe:	ff 75 f4             	pushl  -0xc(%ebp)
80104601:	e8 10 20 00 00       	call   80106616 <removeFromEmbryoList>
80104606:	83 c4 10             	add    $0x10,%esp
80104609:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if (rc == -1) panic("removeFromEmbryoList failed in allocproc!!!\n");
8010460c:	83 7d f0 ff          	cmpl   $0xffffffff,-0x10(%ebp)
80104610:	75 0d                	jne    8010461f <allocproc+0x107>
80104612:	83 ec 0c             	sub    $0xc,%esp
80104615:	68 44 a4 10 80       	push   $0x8010a444
8010461a:	e8 47 bf ff ff       	call   80100566 <panic>
    p->state = UNUSED;
8010461f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104622:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
    p->budget = 0;
80104629:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010462c:	c7 80 98 00 00 00 00 	movl   $0x0,0x98(%eax)
80104633:	00 00 00 
    addToFreeList(p);
80104636:	83 ec 0c             	sub    $0xc,%esp
80104639:	ff 75 f4             	pushl  -0xc(%ebp)
8010463c:	e8 f8 17 00 00       	call   80105e39 <addToFreeList>
80104641:	83 c4 10             	add    $0x10,%esp
    release(&ptable.lock);
80104644:	83 ec 0c             	sub    $0xc,%esp
80104647:	68 80 49 11 80       	push   $0x80114980
8010464c:	e8 4b 24 00 00       	call   80106a9c <release>
80104651:	83 c4 10             	add    $0x10,%esp
    #endif
    return 0;
80104654:	b8 00 00 00 00       	mov    $0x0,%eax
80104659:	e9 86 00 00 00       	jmp    801046e4 <allocproc+0x1cc>
  }
  sp = p->kstack + KSTACKSIZE;
8010465e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104661:	8b 40 08             	mov    0x8(%eax),%eax
80104664:	05 00 10 00 00       	add    $0x1000,%eax
80104669:	89 45 ec             	mov    %eax,-0x14(%ebp)
  
  // Leave room for trap frame.
  sp -= sizeof *p->tf;
8010466c:	83 6d ec 4c          	subl   $0x4c,-0x14(%ebp)
  p->tf = (struct trapframe*)sp;
80104670:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104673:	8b 55 ec             	mov    -0x14(%ebp),%edx
80104676:	89 50 20             	mov    %edx,0x20(%eax)
  
  // Set up new context to start executing at forkret,
  // which returns to trapret.
  sp -= 4;
80104679:	83 6d ec 04          	subl   $0x4,-0x14(%ebp)
  *(uint*)sp = (uint)trapret;
8010467d:	ba 2e 82 10 80       	mov    $0x8010822e,%edx
80104682:	8b 45 ec             	mov    -0x14(%ebp),%eax
80104685:	89 10                	mov    %edx,(%eax)

  sp -= sizeof *p->context;
80104687:	83 6d ec 14          	subl   $0x14,-0x14(%ebp)
  p->context = (struct context*)sp;
8010468b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010468e:	8b 55 ec             	mov    -0x14(%ebp),%edx
80104691:	89 50 24             	mov    %edx,0x24(%eax)
  memset(p->context, 0, sizeof *p->context);
80104694:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104697:	8b 40 24             	mov    0x24(%eax),%eax
8010469a:	83 ec 04             	sub    $0x4,%esp
8010469d:	6a 14                	push   $0x14
8010469f:	6a 00                	push   $0x0
801046a1:	50                   	push   %eax
801046a2:	e8 f1 25 00 00       	call   80106c98 <memset>
801046a7:	83 c4 10             	add    $0x10,%esp
  p->context->eip = (uint)forkret;
801046aa:	8b 45 f4             	mov    -0xc(%ebp),%eax
801046ad:	8b 40 24             	mov    0x24(%eax),%eax
801046b0:	ba a6 53 10 80       	mov    $0x801053a6,%edx
801046b5:	89 50 10             	mov    %edx,0x10(%eax)

  p->start_ticks = ticks;
801046b8:	8b 15 00 79 11 80    	mov    0x80117900,%edx
801046be:	8b 45 f4             	mov    -0xc(%ebp),%eax
801046c1:	89 90 84 00 00 00    	mov    %edx,0x84(%eax)
  p->cpu_ticks_in = 0;
801046c7:	8b 45 f4             	mov    -0xc(%ebp),%eax
801046ca:	c7 80 8c 00 00 00 00 	movl   $0x0,0x8c(%eax)
801046d1:	00 00 00 
  p->cpu_ticks_total = 0;
801046d4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801046d7:	c7 80 88 00 00 00 00 	movl   $0x0,0x88(%eax)
801046de:	00 00 00 

  return p;
801046e1:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
801046e4:	c9                   	leave  
801046e5:	c3                   	ret    

801046e6 <userinit>:

// Set up first user process.
void																	//********** USERINIT *************
userinit(void)
{
801046e6:	55                   	push   %ebp
801046e7:	89 e5                	mov    %esp,%ebp
801046e9:	83 ec 18             	sub    $0x18,%esp
  struct proc *p;
  extern char _binary_initcode_start[], _binary_initcode_size[];
  
  #ifdef CS333_P3P4
  // init other lists
  acquire(&ptable.lock);
801046ec:	83 ec 0c             	sub    $0xc,%esp
801046ef:	68 80 49 11 80       	push   $0x80114980
801046f4:	e8 3c 23 00 00       	call   80106a35 <acquire>
801046f9:	83 c4 10             	add    $0x10,%esp
  ptable.pLists.sleep = 0;
801046fc:	c7 05 d8 70 11 80 00 	movl   $0x0,0x801170d8
80104703:	00 00 00 
  ptable.pLists.zombie = 0;
80104706:	c7 05 dc 70 11 80 00 	movl   $0x0,0x801170dc
8010470d:	00 00 00 
  ptable.pLists.running = 0;
80104710:	c7 05 e0 70 11 80 00 	movl   $0x0,0x801170e0
80104717:	00 00 00 
  ptable.pLists.embryo = 0;
8010471a:	c7 05 e4 70 11 80 00 	movl   $0x0,0x801170e4
80104721:	00 00 00 
  initFreeList();
80104724:	e8 71 17 00 00       	call   80105e9a <initFreeList>
  for(int i = 0; i < MAX; ++i){
80104729:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80104730:	eb 17                	jmp    80104749 <userinit+0x63>
    ptable.pLists.ready[i] = 0;
80104732:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104735:	05 cc 09 00 00       	add    $0x9cc,%eax
8010473a:	c7 04 85 84 49 11 80 	movl   $0x0,-0x7feeb67c(,%eax,4)
80104741:	00 00 00 00 
  ptable.pLists.sleep = 0;
  ptable.pLists.zombie = 0;
  ptable.pLists.running = 0;
  ptable.pLists.embryo = 0;
  initFreeList();
  for(int i = 0; i < MAX; ++i){
80104745:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80104749:	83 7d f4 06          	cmpl   $0x6,-0xc(%ebp)
8010474d:	7e e3                	jle    80104732 <userinit+0x4c>
    ptable.pLists.ready[i] = 0;
  }
  ptable.PromoteAtTime = ticks + TICKS_TO_PROMOTE;	// Set PromoteAtTime
8010474f:	a1 00 79 11 80       	mov    0x80117900,%eax
80104754:	05 20 03 00 00       	add    $0x320,%eax
80104759:	a3 e8 70 11 80       	mov    %eax,0x801170e8
  release(&ptable.lock);
8010475e:	83 ec 0c             	sub    $0xc,%esp
80104761:	68 80 49 11 80       	push   $0x80114980
80104766:	e8 31 23 00 00       	call   80106a9c <release>
8010476b:	83 c4 10             	add    $0x10,%esp
  #endif
  p = allocproc();
8010476e:	e8 a5 fd ff ff       	call   80104518 <allocproc>
80104773:	89 45 f0             	mov    %eax,-0x10(%ebp)
  initproc = p;
80104776:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104779:	a3 68 d6 10 80       	mov    %eax,0x8010d668
  if((p->pgdir = setupkvm()) == 0)
8010477e:	e8 4a 51 00 00       	call   801098cd <setupkvm>
80104783:	89 c2                	mov    %eax,%edx
80104785:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104788:	89 50 04             	mov    %edx,0x4(%eax)
8010478b:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010478e:	8b 40 04             	mov    0x4(%eax),%eax
80104791:	85 c0                	test   %eax,%eax
80104793:	75 0d                	jne    801047a2 <userinit+0xbc>
    panic("userinit: out of memory?");
80104795:	83 ec 0c             	sub    $0xc,%esp
80104798:	68 71 a4 10 80       	push   $0x8010a471
8010479d:	e8 c4 bd ff ff       	call   80100566 <panic>
  inituvm(p->pgdir, _binary_initcode_start, (int)_binary_initcode_size);
801047a2:	ba 2c 00 00 00       	mov    $0x2c,%edx
801047a7:	8b 45 f0             	mov    -0x10(%ebp),%eax
801047aa:	8b 40 04             	mov    0x4(%eax),%eax
801047ad:	83 ec 04             	sub    $0x4,%esp
801047b0:	52                   	push   %edx
801047b1:	68 00 d5 10 80       	push   $0x8010d500
801047b6:	50                   	push   %eax
801047b7:	e8 6b 53 00 00       	call   80109b27 <inituvm>
801047bc:	83 c4 10             	add    $0x10,%esp
  p->sz = PGSIZE;
801047bf:	8b 45 f0             	mov    -0x10(%ebp),%eax
801047c2:	c7 00 00 10 00 00    	movl   $0x1000,(%eax)
  memset(p->tf, 0, sizeof(*p->tf));
801047c8:	8b 45 f0             	mov    -0x10(%ebp),%eax
801047cb:	8b 40 20             	mov    0x20(%eax),%eax
801047ce:	83 ec 04             	sub    $0x4,%esp
801047d1:	6a 4c                	push   $0x4c
801047d3:	6a 00                	push   $0x0
801047d5:	50                   	push   %eax
801047d6:	e8 bd 24 00 00       	call   80106c98 <memset>
801047db:	83 c4 10             	add    $0x10,%esp
  p->tf->cs = (SEG_UCODE << 3) | DPL_USER;
801047de:	8b 45 f0             	mov    -0x10(%ebp),%eax
801047e1:	8b 40 20             	mov    0x20(%eax),%eax
801047e4:	66 c7 40 3c 23 00    	movw   $0x23,0x3c(%eax)
  p->tf->ds = (SEG_UDATA << 3) | DPL_USER;
801047ea:	8b 45 f0             	mov    -0x10(%ebp),%eax
801047ed:	8b 40 20             	mov    0x20(%eax),%eax
801047f0:	66 c7 40 2c 2b 00    	movw   $0x2b,0x2c(%eax)
  p->tf->es = p->tf->ds;
801047f6:	8b 45 f0             	mov    -0x10(%ebp),%eax
801047f9:	8b 40 20             	mov    0x20(%eax),%eax
801047fc:	8b 55 f0             	mov    -0x10(%ebp),%edx
801047ff:	8b 52 20             	mov    0x20(%edx),%edx
80104802:	0f b7 52 2c          	movzwl 0x2c(%edx),%edx
80104806:	66 89 50 28          	mov    %dx,0x28(%eax)
  p->tf->ss = p->tf->ds;
8010480a:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010480d:	8b 40 20             	mov    0x20(%eax),%eax
80104810:	8b 55 f0             	mov    -0x10(%ebp),%edx
80104813:	8b 52 20             	mov    0x20(%edx),%edx
80104816:	0f b7 52 2c          	movzwl 0x2c(%edx),%edx
8010481a:	66 89 50 48          	mov    %dx,0x48(%eax)
  p->tf->eflags = FL_IF;
8010481e:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104821:	8b 40 20             	mov    0x20(%eax),%eax
80104824:	c7 40 40 00 02 00 00 	movl   $0x200,0x40(%eax)
  p->tf->esp = PGSIZE;
8010482b:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010482e:	8b 40 20             	mov    0x20(%eax),%eax
80104831:	c7 40 44 00 10 00 00 	movl   $0x1000,0x44(%eax)
  p->tf->eip = 0;  // beginning of initcode.S
80104838:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010483b:	8b 40 20             	mov    0x20(%eax),%eax
8010483e:	c7 40 38 00 00 00 00 	movl   $0x0,0x38(%eax)

  safestrcpy(p->name, "initcode", sizeof(p->name));
80104845:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104848:	83 c0 74             	add    $0x74,%eax
8010484b:	83 ec 04             	sub    $0x4,%esp
8010484e:	6a 10                	push   $0x10
80104850:	68 8a a4 10 80       	push   $0x8010a48a
80104855:	50                   	push   %eax
80104856:	e8 40 26 00 00       	call   80106e9b <safestrcpy>
8010485b:	83 c4 10             	add    $0x10,%esp
  p->cwd = namei("/");
8010485e:	83 ec 0c             	sub    $0xc,%esp
80104861:	68 93 a4 10 80       	push   $0x8010a493
80104866:	e8 5b dd ff ff       	call   801025c6 <namei>
8010486b:	83 c4 10             	add    $0x10,%esp
8010486e:	89 c2                	mov    %eax,%edx
80104870:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104873:	89 50 70             	mov    %edx,0x70(%eax)

//========== Project 2 System Calls Start ====================
  p->uid = UID_DEFAULT; //set initial uid
80104876:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104879:	c7 40 14 00 00 00 00 	movl   $0x0,0x14(%eax)
  p->gid = GID_DEFAULT; //and gids
80104880:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104883:	c7 40 18 00 00 00 00 	movl   $0x0,0x18(%eax)
//========== Project 2 System Calls End ====================

  #ifdef CS333_P3P4
  // init ready list
  acquire(&ptable.lock);
8010488a:	83 ec 0c             	sub    $0xc,%esp
8010488d:	68 80 49 11 80       	push   $0x80114980
80104892:	e8 9e 21 00 00       	call   80106a35 <acquire>
80104897:	83 c4 10             	add    $0x10,%esp
  int rc = removeFromEmbryoList(p);
8010489a:	83 ec 0c             	sub    $0xc,%esp
8010489d:	ff 75 f0             	pushl  -0x10(%ebp)
801048a0:	e8 71 1d 00 00       	call   80106616 <removeFromEmbryoList>
801048a5:	83 c4 10             	add    $0x10,%esp
801048a8:	89 45 ec             	mov    %eax,-0x14(%ebp)
  if (rc == -1) panic("removeFromEmbryoList failed in userinit!!!\n");
801048ab:	83 7d ec ff          	cmpl   $0xffffffff,-0x14(%ebp)
801048af:	75 0d                	jne    801048be <userinit+0x1d8>
801048b1:	83 ec 0c             	sub    $0xc,%esp
801048b4:	68 98 a4 10 80       	push   $0x8010a498
801048b9:	e8 a8 bc ff ff       	call   80100566 <panic>
  p->state = RUNNABLE;
801048be:	8b 45 f0             	mov    -0x10(%ebp),%eax
801048c1:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
  p->priority = 0;
801048c8:	8b 45 f0             	mov    -0x10(%ebp),%eax
801048cb:	c7 80 94 00 00 00 00 	movl   $0x0,0x94(%eax)
801048d2:	00 00 00 
  addToReadyList(p, p->priority);
801048d5:	8b 45 f0             	mov    -0x10(%ebp),%eax
801048d8:	8b 80 94 00 00 00    	mov    0x94(%eax),%eax
801048de:	83 ec 08             	sub    $0x8,%esp
801048e1:	50                   	push   %eax
801048e2:	ff 75 f0             	pushl  -0x10(%ebp)
801048e5:	e8 d8 16 00 00       	call   80105fc2 <addToReadyList>
801048ea:	83 c4 10             	add    $0x10,%esp
  release(&ptable.lock);
801048ed:	83 ec 0c             	sub    $0xc,%esp
801048f0:	68 80 49 11 80       	push   $0x80114980
801048f5:	e8 a2 21 00 00       	call   80106a9c <release>
801048fa:	83 c4 10             	add    $0x10,%esp
  #endif
}
801048fd:	90                   	nop
801048fe:	c9                   	leave  
801048ff:	c3                   	ret    

80104900 <growproc>:

// Grow current process's memory by n bytes.
// Return 0 on success, -1 on failure.
int
growproc(int n)
{
80104900:	55                   	push   %ebp
80104901:	89 e5                	mov    %esp,%ebp
80104903:	83 ec 18             	sub    $0x18,%esp
  uint sz;
  
  sz = proc->sz;
80104906:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010490c:	8b 00                	mov    (%eax),%eax
8010490e:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(n > 0){
80104911:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80104915:	7e 31                	jle    80104948 <growproc+0x48>
    if((sz = allocuvm(proc->pgdir, sz, sz + n)) == 0)
80104917:	8b 55 08             	mov    0x8(%ebp),%edx
8010491a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010491d:	01 c2                	add    %eax,%edx
8010491f:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104925:	8b 40 04             	mov    0x4(%eax),%eax
80104928:	83 ec 04             	sub    $0x4,%esp
8010492b:	52                   	push   %edx
8010492c:	ff 75 f4             	pushl  -0xc(%ebp)
8010492f:	50                   	push   %eax
80104930:	e8 3f 53 00 00       	call   80109c74 <allocuvm>
80104935:	83 c4 10             	add    $0x10,%esp
80104938:	89 45 f4             	mov    %eax,-0xc(%ebp)
8010493b:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
8010493f:	75 3e                	jne    8010497f <growproc+0x7f>
      return -1;
80104941:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104946:	eb 59                	jmp    801049a1 <growproc+0xa1>
  } else if(n < 0){
80104948:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
8010494c:	79 31                	jns    8010497f <growproc+0x7f>
    if((sz = deallocuvm(proc->pgdir, sz, sz + n)) == 0)
8010494e:	8b 55 08             	mov    0x8(%ebp),%edx
80104951:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104954:	01 c2                	add    %eax,%edx
80104956:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010495c:	8b 40 04             	mov    0x4(%eax),%eax
8010495f:	83 ec 04             	sub    $0x4,%esp
80104962:	52                   	push   %edx
80104963:	ff 75 f4             	pushl  -0xc(%ebp)
80104966:	50                   	push   %eax
80104967:	e8 d1 53 00 00       	call   80109d3d <deallocuvm>
8010496c:	83 c4 10             	add    $0x10,%esp
8010496f:	89 45 f4             	mov    %eax,-0xc(%ebp)
80104972:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80104976:	75 07                	jne    8010497f <growproc+0x7f>
      return -1;
80104978:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010497d:	eb 22                	jmp    801049a1 <growproc+0xa1>
  }
  proc->sz = sz;
8010497f:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104985:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104988:	89 10                	mov    %edx,(%eax)
  switchuvm(proc);
8010498a:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104990:	83 ec 0c             	sub    $0xc,%esp
80104993:	50                   	push   %eax
80104994:	e8 1b 50 00 00       	call   801099b4 <switchuvm>
80104999:	83 c4 10             	add    $0x10,%esp
  return 0;
8010499c:	b8 00 00 00 00       	mov    $0x0,%eax
}
801049a1:	c9                   	leave  
801049a2:	c3                   	ret    

801049a3 <fork>:
// Create a new process copying p as the parent.
// Sets up stack to return as if from system call.
// Caller must set state of returned proc to RUNNABLE.
int																	//************* FORK *************
fork(void)
{
801049a3:	55                   	push   %ebp
801049a4:	89 e5                	mov    %esp,%ebp
801049a6:	57                   	push   %edi
801049a7:	56                   	push   %esi
801049a8:	53                   	push   %ebx
801049a9:	83 ec 2c             	sub    $0x2c,%esp
  int i, pid;
  struct proc *np;

  // Allocate process.
  if((np = allocproc()) == 0)
801049ac:	e8 67 fb ff ff       	call   80104518 <allocproc>
801049b1:	89 45 e0             	mov    %eax,-0x20(%ebp)
801049b4:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
801049b8:	75 0a                	jne    801049c4 <fork+0x21>
    return -1;
801049ba:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801049bf:	e9 4b 02 00 00       	jmp    80104c0f <fork+0x26c>

  // Copy process state from p.
  if((np->pgdir = copyuvm(proc->pgdir, proc->sz)) == 0){
801049c4:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801049ca:	8b 10                	mov    (%eax),%edx
801049cc:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801049d2:	8b 40 04             	mov    0x4(%eax),%eax
801049d5:	83 ec 08             	sub    $0x8,%esp
801049d8:	52                   	push   %edx
801049d9:	50                   	push   %eax
801049da:	e8 fc 54 00 00       	call   80109edb <copyuvm>
801049df:	83 c4 10             	add    $0x10,%esp
801049e2:	89 c2                	mov    %eax,%edx
801049e4:	8b 45 e0             	mov    -0x20(%ebp),%eax
801049e7:	89 50 04             	mov    %edx,0x4(%eax)
801049ea:	8b 45 e0             	mov    -0x20(%ebp),%eax
801049ed:	8b 40 04             	mov    0x4(%eax),%eax
801049f0:	85 c0                	test   %eax,%eax
801049f2:	0f 85 82 00 00 00    	jne    80104a7a <fork+0xd7>
    kfree(np->kstack);
801049f8:	8b 45 e0             	mov    -0x20(%ebp),%eax
801049fb:	8b 40 08             	mov    0x8(%eax),%eax
801049fe:	83 ec 0c             	sub    $0xc,%esp
80104a01:	50                   	push   %eax
80104a02:	e8 60 e2 ff ff       	call   80102c67 <kfree>
80104a07:	83 c4 10             	add    $0x10,%esp
    np->kstack = 0;
80104a0a:	8b 45 e0             	mov    -0x20(%ebp),%eax
80104a0d:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
    #ifdef CS333_P3P4
    acquire(&ptable.lock);
80104a14:	83 ec 0c             	sub    $0xc,%esp
80104a17:	68 80 49 11 80       	push   $0x80114980
80104a1c:	e8 14 20 00 00       	call   80106a35 <acquire>
80104a21:	83 c4 10             	add    $0x10,%esp
    int rc = removeFromEmbryoList(np);
80104a24:	83 ec 0c             	sub    $0xc,%esp
80104a27:	ff 75 e0             	pushl  -0x20(%ebp)
80104a2a:	e8 e7 1b 00 00       	call   80106616 <removeFromEmbryoList>
80104a2f:	83 c4 10             	add    $0x10,%esp
80104a32:	89 45 dc             	mov    %eax,-0x24(%ebp)
    if (rc == -1) panic("removeFromEmbryoList failed in fork 1!!!");
80104a35:	83 7d dc ff          	cmpl   $0xffffffff,-0x24(%ebp)
80104a39:	75 0d                	jne    80104a48 <fork+0xa5>
80104a3b:	83 ec 0c             	sub    $0xc,%esp
80104a3e:	68 c4 a4 10 80       	push   $0x8010a4c4
80104a43:	e8 1e bb ff ff       	call   80100566 <panic>
    np->state = UNUSED;
80104a48:	8b 45 e0             	mov    -0x20(%ebp),%eax
80104a4b:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
    addToFreeList(np);
80104a52:	83 ec 0c             	sub    $0xc,%esp
80104a55:	ff 75 e0             	pushl  -0x20(%ebp)
80104a58:	e8 dc 13 00 00       	call   80105e39 <addToFreeList>
80104a5d:	83 c4 10             	add    $0x10,%esp
    release(&ptable.lock);
80104a60:	83 ec 0c             	sub    $0xc,%esp
80104a63:	68 80 49 11 80       	push   $0x80114980
80104a68:	e8 2f 20 00 00       	call   80106a9c <release>
80104a6d:	83 c4 10             	add    $0x10,%esp
    #endif
    return -1;
80104a70:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104a75:	e9 95 01 00 00       	jmp    80104c0f <fork+0x26c>
  }
  np->sz = proc->sz;
80104a7a:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104a80:	8b 10                	mov    (%eax),%edx
80104a82:	8b 45 e0             	mov    -0x20(%ebp),%eax
80104a85:	89 10                	mov    %edx,(%eax)
  np->parent = proc;
80104a87:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
80104a8e:	8b 45 e0             	mov    -0x20(%ebp),%eax
80104a91:	89 50 1c             	mov    %edx,0x1c(%eax)
  *np->tf = *proc->tf;
80104a94:	8b 45 e0             	mov    -0x20(%ebp),%eax
80104a97:	8b 50 20             	mov    0x20(%eax),%edx
80104a9a:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104aa0:	8b 40 20             	mov    0x20(%eax),%eax
80104aa3:	89 c3                	mov    %eax,%ebx
80104aa5:	b8 13 00 00 00       	mov    $0x13,%eax
80104aaa:	89 d7                	mov    %edx,%edi
80104aac:	89 de                	mov    %ebx,%esi
80104aae:	89 c1                	mov    %eax,%ecx
80104ab0:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)

  // Clear %eax so that fork returns 0 in the child.
  np->tf->eax = 0;
80104ab2:	8b 45 e0             	mov    -0x20(%ebp),%eax
80104ab5:	8b 40 20             	mov    0x20(%eax),%eax
80104ab8:	c7 40 1c 00 00 00 00 	movl   $0x0,0x1c(%eax)

  for(i = 0; i < NOFILE; i++)
80104abf:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
80104ac6:	eb 40                	jmp    80104b08 <fork+0x165>
    if(proc->ofile[i])
80104ac8:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104ace:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80104ad1:	83 c2 0c             	add    $0xc,%edx
80104ad4:	8b 04 90             	mov    (%eax,%edx,4),%eax
80104ad7:	85 c0                	test   %eax,%eax
80104ad9:	74 29                	je     80104b04 <fork+0x161>
      np->ofile[i] = filedup(proc->ofile[i]);
80104adb:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104ae1:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80104ae4:	83 c2 0c             	add    $0xc,%edx
80104ae7:	8b 04 90             	mov    (%eax,%edx,4),%eax
80104aea:	83 ec 0c             	sub    $0xc,%esp
80104aed:	50                   	push   %eax
80104aee:	e8 ab c5 ff ff       	call   8010109e <filedup>
80104af3:	83 c4 10             	add    $0x10,%esp
80104af6:	89 c1                	mov    %eax,%ecx
80104af8:	8b 45 e0             	mov    -0x20(%ebp),%eax
80104afb:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80104afe:	83 c2 0c             	add    $0xc,%edx
80104b01:	89 0c 90             	mov    %ecx,(%eax,%edx,4)
  *np->tf = *proc->tf;

  // Clear %eax so that fork returns 0 in the child.
  np->tf->eax = 0;

  for(i = 0; i < NOFILE; i++)
80104b04:	83 45 e4 01          	addl   $0x1,-0x1c(%ebp)
80104b08:	83 7d e4 0f          	cmpl   $0xf,-0x1c(%ebp)
80104b0c:	7e ba                	jle    80104ac8 <fork+0x125>
    if(proc->ofile[i])
      np->ofile[i] = filedup(proc->ofile[i]);
  np->cwd = idup(proc->cwd);
80104b0e:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104b14:	8b 40 70             	mov    0x70(%eax),%eax
80104b17:	83 ec 0c             	sub    $0xc,%esp
80104b1a:	50                   	push   %eax
80104b1b:	e8 ae ce ff ff       	call   801019ce <idup>
80104b20:	83 c4 10             	add    $0x10,%esp
80104b23:	89 c2                	mov    %eax,%edx
80104b25:	8b 45 e0             	mov    -0x20(%ebp),%eax
80104b28:	89 50 70             	mov    %edx,0x70(%eax)

  safestrcpy(np->name, proc->name, sizeof(proc->name));
80104b2b:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104b31:	8d 50 74             	lea    0x74(%eax),%edx
80104b34:	8b 45 e0             	mov    -0x20(%ebp),%eax
80104b37:	83 c0 74             	add    $0x74,%eax
80104b3a:	83 ec 04             	sub    $0x4,%esp
80104b3d:	6a 10                	push   $0x10
80104b3f:	52                   	push   %edx
80104b40:	50                   	push   %eax
80104b41:	e8 55 23 00 00       	call   80106e9b <safestrcpy>
80104b46:	83 c4 10             	add    $0x10,%esp
 
  pid = np->pid;
80104b49:	8b 45 e0             	mov    -0x20(%ebp),%eax
80104b4c:	8b 40 10             	mov    0x10(%eax),%eax
80104b4f:	89 45 d8             	mov    %eax,-0x28(%ebp)
//========== Project 2 System Calls Start ====================
  np->uid = proc->uid; //passing the uid and gid
80104b52:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104b58:	8b 50 14             	mov    0x14(%eax),%edx
80104b5b:	8b 45 e0             	mov    -0x20(%ebp),%eax
80104b5e:	89 50 14             	mov    %edx,0x14(%eax)
  np->gid = proc->gid; //on to the next process
80104b61:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104b67:	8b 50 18             	mov    0x18(%eax),%edx
80104b6a:	8b 45 e0             	mov    -0x20(%ebp),%eax
80104b6d:	89 50 18             	mov    %edx,0x18(%eax)
  np->cpu_ticks_total = 0;
80104b70:	8b 45 e0             	mov    -0x20(%ebp),%eax
80104b73:	c7 80 88 00 00 00 00 	movl   $0x0,0x88(%eax)
80104b7a:	00 00 00 
  np->start_ticks = ticks;
80104b7d:	8b 15 00 79 11 80    	mov    0x80117900,%edx
80104b83:	8b 45 e0             	mov    -0x20(%ebp),%eax
80104b86:	89 90 84 00 00 00    	mov    %edx,0x84(%eax)
//========== Project 2 System Calls End ====================


  // lock to force the compiler to emit the np->state write last.
  acquire(&ptable.lock);
80104b8c:	83 ec 0c             	sub    $0xc,%esp
80104b8f:	68 80 49 11 80       	push   $0x80114980
80104b94:	e8 9c 1e 00 00       	call   80106a35 <acquire>
80104b99:	83 c4 10             	add    $0x10,%esp
  #ifdef CS333_P3P4
  int rc = removeFromEmbryoList(np);
80104b9c:	83 ec 0c             	sub    $0xc,%esp
80104b9f:	ff 75 e0             	pushl  -0x20(%ebp)
80104ba2:	e8 6f 1a 00 00       	call   80106616 <removeFromEmbryoList>
80104ba7:	83 c4 10             	add    $0x10,%esp
80104baa:	89 45 d4             	mov    %eax,-0x2c(%ebp)
  if (rc == -1) panic("removeFromEmbryoList failed in fork 2!!!\n");
80104bad:	83 7d d4 ff          	cmpl   $0xffffffff,-0x2c(%ebp)
80104bb1:	75 0d                	jne    80104bc0 <fork+0x21d>
80104bb3:	83 ec 0c             	sub    $0xc,%esp
80104bb6:	68 f0 a4 10 80       	push   $0x8010a4f0
80104bbb:	e8 a6 b9 ff ff       	call   80100566 <panic>
  np->state = RUNNABLE;
80104bc0:	8b 45 e0             	mov    -0x20(%ebp),%eax
80104bc3:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
  np->priority = 0;
80104bca:	8b 45 e0             	mov    -0x20(%ebp),%eax
80104bcd:	c7 80 94 00 00 00 00 	movl   $0x0,0x94(%eax)
80104bd4:	00 00 00 
  np->budget = BUDGET;
80104bd7:	8b 45 e0             	mov    -0x20(%ebp),%eax
80104bda:	c7 80 98 00 00 00 14 	movl   $0x14,0x98(%eax)
80104be1:	00 00 00 
  addToReadyList(np, np->priority);
80104be4:	8b 45 e0             	mov    -0x20(%ebp),%eax
80104be7:	8b 80 94 00 00 00    	mov    0x94(%eax),%eax
80104bed:	83 ec 08             	sub    $0x8,%esp
80104bf0:	50                   	push   %eax
80104bf1:	ff 75 e0             	pushl  -0x20(%ebp)
80104bf4:	e8 c9 13 00 00       	call   80105fc2 <addToReadyList>
80104bf9:	83 c4 10             	add    $0x10,%esp
  #endif
  release(&ptable.lock);
80104bfc:	83 ec 0c             	sub    $0xc,%esp
80104bff:	68 80 49 11 80       	push   $0x80114980
80104c04:	e8 93 1e 00 00       	call   80106a9c <release>
80104c09:	83 c4 10             	add    $0x10,%esp
  
  return pid;
80104c0c:	8b 45 d8             	mov    -0x28(%ebp),%eax
}
80104c0f:	8d 65 f4             	lea    -0xc(%ebp),%esp
80104c12:	5b                   	pop    %ebx
80104c13:	5e                   	pop    %esi
80104c14:	5f                   	pop    %edi
80104c15:	5d                   	pop    %ebp
80104c16:	c3                   	ret    

80104c17 <exit>:
  panic("zombie exit");
}
#else
void																	//************ EXIT *************
exit(void)
{
80104c17:	55                   	push   %ebp
80104c18:	89 e5                	mov    %esp,%ebp
80104c1a:	83 ec 18             	sub    $0x18,%esp
  struct proc *p;
  int fd;

  if(proc == initproc)
80104c1d:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
80104c24:	a1 68 d6 10 80       	mov    0x8010d668,%eax
80104c29:	39 c2                	cmp    %eax,%edx
80104c2b:	75 0d                	jne    80104c3a <exit+0x23>
    panic("init exiting");
80104c2d:	83 ec 0c             	sub    $0xc,%esp
80104c30:	68 1a a5 10 80       	push   $0x8010a51a
80104c35:	e8 2c b9 ff ff       	call   80100566 <panic>

  // Close all open files.
  for(fd = 0; fd < NOFILE; fd++){
80104c3a:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
80104c41:	eb 45                	jmp    80104c88 <exit+0x71>
    if(proc->ofile[fd]){
80104c43:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104c49:	8b 55 f0             	mov    -0x10(%ebp),%edx
80104c4c:	83 c2 0c             	add    $0xc,%edx
80104c4f:	8b 04 90             	mov    (%eax,%edx,4),%eax
80104c52:	85 c0                	test   %eax,%eax
80104c54:	74 2e                	je     80104c84 <exit+0x6d>
      fileclose(proc->ofile[fd]);
80104c56:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104c5c:	8b 55 f0             	mov    -0x10(%ebp),%edx
80104c5f:	83 c2 0c             	add    $0xc,%edx
80104c62:	8b 04 90             	mov    (%eax,%edx,4),%eax
80104c65:	83 ec 0c             	sub    $0xc,%esp
80104c68:	50                   	push   %eax
80104c69:	e8 81 c4 ff ff       	call   801010ef <fileclose>
80104c6e:	83 c4 10             	add    $0x10,%esp
      proc->ofile[fd] = 0;
80104c71:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104c77:	8b 55 f0             	mov    -0x10(%ebp),%edx
80104c7a:	83 c2 0c             	add    $0xc,%edx
80104c7d:	c7 04 90 00 00 00 00 	movl   $0x0,(%eax,%edx,4)

  if(proc == initproc)
    panic("init exiting");

  // Close all open files.
  for(fd = 0; fd < NOFILE; fd++){
80104c84:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
80104c88:	83 7d f0 0f          	cmpl   $0xf,-0x10(%ebp)
80104c8c:	7e b5                	jle    80104c43 <exit+0x2c>
      fileclose(proc->ofile[fd]);
      proc->ofile[fd] = 0;
    }
  }

  begin_op();
80104c8e:	e8 58 e9 ff ff       	call   801035eb <begin_op>
  iput(proc->cwd);
80104c93:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104c99:	8b 40 70             	mov    0x70(%eax),%eax
80104c9c:	83 ec 0c             	sub    $0xc,%esp
80104c9f:	50                   	push   %eax
80104ca0:	e8 33 cf ff ff       	call   80101bd8 <iput>
80104ca5:	83 c4 10             	add    $0x10,%esp
  end_op();
80104ca8:	e8 ca e9 ff ff       	call   80103677 <end_op>
  proc->cwd = 0;
80104cad:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104cb3:	c7 40 70 00 00 00 00 	movl   $0x0,0x70(%eax)

  acquire(&ptable.lock);
80104cba:	83 ec 0c             	sub    $0xc,%esp
80104cbd:	68 80 49 11 80       	push   $0x80114980
80104cc2:	e8 6e 1d 00 00       	call   80106a35 <acquire>
80104cc7:	83 c4 10             	add    $0x10,%esp

  // Parent might be sleeping in wait().
  wakeup1(proc->parent);
80104cca:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104cd0:	8b 40 1c             	mov    0x1c(%eax),%eax
80104cd3:	83 ec 0c             	sub    $0xc,%esp
80104cd6:	50                   	push   %eax
80104cd7:	e8 17 08 00 00       	call   801054f3 <wakeup1>
80104cdc:	83 c4 10             	add    $0x10,%esp

  // Pass abandoned children to init.
  for(int i = 0; i < MAX; ++i){
80104cdf:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
80104ce6:	eb 46                	jmp    80104d2e <exit+0x117>
    p = ptable.pLists.ready[i];
80104ce8:	8b 45 ec             	mov    -0x14(%ebp),%eax
80104ceb:	05 cc 09 00 00       	add    $0x9cc,%eax
80104cf0:	8b 04 85 84 49 11 80 	mov    -0x7feeb67c(,%eax,4),%eax
80104cf7:	89 45 f4             	mov    %eax,-0xc(%ebp)
    while (p) {
80104cfa:	eb 28                	jmp    80104d24 <exit+0x10d>
      if(p->parent == proc) {
80104cfc:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104cff:	8b 50 1c             	mov    0x1c(%eax),%edx
80104d02:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104d08:	39 c2                	cmp    %eax,%edx
80104d0a:	75 0c                	jne    80104d18 <exit+0x101>
        p->parent = initproc;
80104d0c:	8b 15 68 d6 10 80    	mov    0x8010d668,%edx
80104d12:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104d15:	89 50 1c             	mov    %edx,0x1c(%eax)
      }
      p = p->next;
80104d18:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104d1b:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
80104d21:	89 45 f4             	mov    %eax,-0xc(%ebp)
  wakeup1(proc->parent);

  // Pass abandoned children to init.
  for(int i = 0; i < MAX; ++i){
    p = ptable.pLists.ready[i];
    while (p) {
80104d24:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80104d28:	75 d2                	jne    80104cfc <exit+0xe5>

  // Parent might be sleeping in wait().
  wakeup1(proc->parent);

  // Pass abandoned children to init.
  for(int i = 0; i < MAX; ++i){
80104d2a:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
80104d2e:	83 7d ec 06          	cmpl   $0x6,-0x14(%ebp)
80104d32:	7e b4                	jle    80104ce8 <exit+0xd1>
        p->parent = initproc;
      }
      p = p->next;
    }
  }
  p = ptable.pLists.sleep;
80104d34:	a1 d8 70 11 80       	mov    0x801170d8,%eax
80104d39:	89 45 f4             	mov    %eax,-0xc(%ebp)
  while (p) {
80104d3c:	eb 28                	jmp    80104d66 <exit+0x14f>
    if(p->parent == proc) {
80104d3e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104d41:	8b 50 1c             	mov    0x1c(%eax),%edx
80104d44:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104d4a:	39 c2                	cmp    %eax,%edx
80104d4c:	75 0c                	jne    80104d5a <exit+0x143>
      p->parent = initproc;
80104d4e:	8b 15 68 d6 10 80    	mov    0x8010d668,%edx
80104d54:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104d57:	89 50 1c             	mov    %edx,0x1c(%eax)
    }
    p = p->next;
80104d5a:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104d5d:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
80104d63:	89 45 f4             	mov    %eax,-0xc(%ebp)
      }
      p = p->next;
    }
  }
  p = ptable.pLists.sleep;
  while (p) {
80104d66:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80104d6a:	75 d2                	jne    80104d3e <exit+0x127>
    if(p->parent == proc) {
      p->parent = initproc;
    }
    p = p->next;
  }
  p = ptable.pLists.running;
80104d6c:	a1 e0 70 11 80       	mov    0x801170e0,%eax
80104d71:	89 45 f4             	mov    %eax,-0xc(%ebp)
  while (p) {
80104d74:	eb 28                	jmp    80104d9e <exit+0x187>
    if(p->parent == proc) {
80104d76:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104d79:	8b 50 1c             	mov    0x1c(%eax),%edx
80104d7c:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104d82:	39 c2                	cmp    %eax,%edx
80104d84:	75 0c                	jne    80104d92 <exit+0x17b>
      p->parent = initproc;
80104d86:	8b 15 68 d6 10 80    	mov    0x8010d668,%edx
80104d8c:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104d8f:	89 50 1c             	mov    %edx,0x1c(%eax)
    }
    p = p->next;
80104d92:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104d95:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
80104d9b:	89 45 f4             	mov    %eax,-0xc(%ebp)
      p->parent = initproc;
    }
    p = p->next;
  }
  p = ptable.pLists.running;
  while (p) {
80104d9e:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80104da2:	75 d2                	jne    80104d76 <exit+0x15f>
    if(p->parent == proc) {
      p->parent = initproc;
    }
    p = p->next;
  }
  p = ptable.pLists.zombie;
80104da4:	a1 dc 70 11 80       	mov    0x801170dc,%eax
80104da9:	89 45 f4             	mov    %eax,-0xc(%ebp)
  while (p) {
80104dac:	eb 39                	jmp    80104de7 <exit+0x1d0>
    if(p->parent == proc) {
80104dae:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104db1:	8b 50 1c             	mov    0x1c(%eax),%edx
80104db4:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104dba:	39 c2                	cmp    %eax,%edx
80104dbc:	75 1d                	jne    80104ddb <exit+0x1c4>
      p->parent = initproc;
80104dbe:	8b 15 68 d6 10 80    	mov    0x8010d668,%edx
80104dc4:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104dc7:	89 50 1c             	mov    %edx,0x1c(%eax)
      wakeup1(initproc);
80104dca:	a1 68 d6 10 80       	mov    0x8010d668,%eax
80104dcf:	83 ec 0c             	sub    $0xc,%esp
80104dd2:	50                   	push   %eax
80104dd3:	e8 1b 07 00 00       	call   801054f3 <wakeup1>
80104dd8:	83 c4 10             	add    $0x10,%esp
    }
    p = p->next;
80104ddb:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104dde:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
80104de4:	89 45 f4             	mov    %eax,-0xc(%ebp)
      p->parent = initproc;
    }
    p = p->next;
  }
  p = ptable.pLists.zombie;
  while (p) {
80104de7:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80104deb:	75 c1                	jne    80104dae <exit+0x197>
    }
    p = p->next;
  }

  // Jump into the scheduler, never to return.
  int rc = removeFromRunningList(proc);
80104ded:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104df3:	83 ec 0c             	sub    $0xc,%esp
80104df6:	50                   	push   %eax
80104df7:	e8 4e 19 00 00       	call   8010674a <removeFromRunningList>
80104dfc:	83 c4 10             	add    $0x10,%esp
80104dff:	89 45 e8             	mov    %eax,-0x18(%ebp)
  if (rc == -1) panic("removeFromRunningList failed in exit!!!\n");
80104e02:	83 7d e8 ff          	cmpl   $0xffffffff,-0x18(%ebp)
80104e06:	75 0d                	jne    80104e15 <exit+0x1fe>
80104e08:	83 ec 0c             	sub    $0xc,%esp
80104e0b:	68 28 a5 10 80       	push   $0x8010a528
80104e10:	e8 51 b7 ff ff       	call   80100566 <panic>
  proc->state = ZOMBIE;
80104e15:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104e1b:	c7 40 0c 05 00 00 00 	movl   $0x5,0xc(%eax)
  //
  if(!holding(&ptable.lock)) panic("I need the lock to add to the zombie list\n");
80104e22:	83 ec 0c             	sub    $0xc,%esp
80104e25:	68 80 49 11 80       	push   $0x80114980
80104e2a:	e8 39 1d 00 00       	call   80106b68 <holding>
80104e2f:	83 c4 10             	add    $0x10,%esp
80104e32:	85 c0                	test   %eax,%eax
80104e34:	75 0d                	jne    80104e43 <exit+0x22c>
80104e36:	83 ec 0c             	sub    $0xc,%esp
80104e39:	68 54 a5 10 80       	push   $0x8010a554
80104e3e:	e8 23 b7 ff ff       	call   80100566 <panic>
  addToZombieList(proc);
80104e43:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104e49:	83 ec 0c             	sub    $0xc,%esp
80104e4c:	50                   	push   %eax
80104e4d:	e8 24 15 00 00       	call   80106376 <addToZombieList>
80104e52:	83 c4 10             	add    $0x10,%esp
  //
  sched();
80104e55:	e8 b2 03 00 00       	call   8010520c <sched>
  panic("zombie exit");
80104e5a:	83 ec 0c             	sub    $0xc,%esp
80104e5d:	68 7f a5 10 80       	push   $0x8010a57f
80104e62:	e8 ff b6 ff ff       	call   80100566 <panic>

80104e67 <wait>:
  }
}
#else
int																	//************* WAIT **************
wait(void)
{
80104e67:	55                   	push   %ebp
80104e68:	89 e5                	mov    %esp,%ebp
80104e6a:	83 ec 28             	sub    $0x28,%esp
  struct proc *p;
  int havekids, pid;

  acquire(&ptable.lock);
80104e6d:	83 ec 0c             	sub    $0xc,%esp
80104e70:	68 80 49 11 80       	push   $0x80114980
80104e75:	e8 bb 1b 00 00       	call   80106a35 <acquire>
80104e7a:	83 c4 10             	add    $0x10,%esp
  for(;;){
    // Scan through table looking for zombie children.
    havekids = 0;
80104e7d:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
    p = ptable.pLists.zombie;
80104e84:	a1 dc 70 11 80       	mov    0x801170dc,%eax
80104e89:	89 45 f4             	mov    %eax,-0xc(%ebp)
    while (p) {
80104e8c:	e9 d7 00 00 00       	jmp    80104f68 <wait+0x101>
      if(p->parent == proc) {
80104e91:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104e94:	8b 50 1c             	mov    0x1c(%eax),%edx
80104e97:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104e9d:	39 c2                	cmp    %eax,%edx
80104e9f:	0f 85 b7 00 00 00    	jne    80104f5c <wait+0xf5>
        havekids = 1;
80104ea5:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
        int rc = removeFromZombieList(p);
80104eac:	83 ec 0c             	sub    $0xc,%esp
80104eaf:	ff 75 f4             	pushl  -0xc(%ebp)
80104eb2:	e8 15 16 00 00       	call   801064cc <removeFromZombieList>
80104eb7:	83 c4 10             	add    $0x10,%esp
80104eba:	89 45 e8             	mov    %eax,-0x18(%ebp)
        if (rc == -1) panic("removeFromZombieList failed in wait!!!\n");
80104ebd:	83 7d e8 ff          	cmpl   $0xffffffff,-0x18(%ebp)
80104ec1:	75 0d                	jne    80104ed0 <wait+0x69>
80104ec3:	83 ec 0c             	sub    $0xc,%esp
80104ec6:	68 8c a5 10 80       	push   $0x8010a58c
80104ecb:	e8 96 b6 ff ff       	call   80100566 <panic>
        pid = p->pid;
80104ed0:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104ed3:	8b 40 10             	mov    0x10(%eax),%eax
80104ed6:	89 45 e4             	mov    %eax,-0x1c(%ebp)
        kfree(p->kstack);
80104ed9:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104edc:	8b 40 08             	mov    0x8(%eax),%eax
80104edf:	83 ec 0c             	sub    $0xc,%esp
80104ee2:	50                   	push   %eax
80104ee3:	e8 7f dd ff ff       	call   80102c67 <kfree>
80104ee8:	83 c4 10             	add    $0x10,%esp
        p->kstack = 0;
80104eeb:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104eee:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
        freevm(p->pgdir);
80104ef5:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104ef8:	8b 40 04             	mov    0x4(%eax),%eax
80104efb:	83 ec 0c             	sub    $0xc,%esp
80104efe:	50                   	push   %eax
80104eff:	e8 f6 4e 00 00       	call   80109dfa <freevm>
80104f04:	83 c4 10             	add    $0x10,%esp
        p->state = UNUSED;
80104f07:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104f0a:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
        addToFreeList(p);
80104f11:	83 ec 0c             	sub    $0xc,%esp
80104f14:	ff 75 f4             	pushl  -0xc(%ebp)
80104f17:	e8 1d 0f 00 00       	call   80105e39 <addToFreeList>
80104f1c:	83 c4 10             	add    $0x10,%esp
        p->pid = 0;
80104f1f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104f22:	c7 40 10 00 00 00 00 	movl   $0x0,0x10(%eax)
        p->parent = 0;
80104f29:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104f2c:	c7 40 1c 00 00 00 00 	movl   $0x0,0x1c(%eax)
        p->name[0] = 0;
80104f33:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104f36:	c6 40 74 00          	movb   $0x0,0x74(%eax)
        p->killed = 0;
80104f3a:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104f3d:	c7 40 2c 00 00 00 00 	movl   $0x0,0x2c(%eax)
        release(&ptable.lock);
80104f44:	83 ec 0c             	sub    $0xc,%esp
80104f47:	68 80 49 11 80       	push   $0x80114980
80104f4c:	e8 4b 1b 00 00       	call   80106a9c <release>
80104f51:	83 c4 10             	add    $0x10,%esp
        return pid;
80104f54:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104f57:	e9 24 01 00 00       	jmp    80105080 <wait+0x219>
      }
      p = p->next;
80104f5c:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104f5f:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
80104f65:	89 45 f4             	mov    %eax,-0xc(%ebp)
  acquire(&ptable.lock);
  for(;;){
    // Scan through table looking for zombie children.
    havekids = 0;
    p = ptable.pLists.zombie;
    while (p) {
80104f68:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80104f6c:	0f 85 1f ff ff ff    	jne    80104e91 <wait+0x2a>
        release(&ptable.lock);
        return pid;
      }
      p = p->next;
    }
    if (!havekids) {
80104f72:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80104f76:	75 50                	jne    80104fc8 <wait+0x161>
      for(int i = 0; i < MAX; ++i){
80104f78:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
80104f7f:	eb 41                	jmp    80104fc2 <wait+0x15b>
        p = ptable.pLists.ready[i];
80104f81:	8b 45 ec             	mov    -0x14(%ebp),%eax
80104f84:	05 cc 09 00 00       	add    $0x9cc,%eax
80104f89:	8b 04 85 84 49 11 80 	mov    -0x7feeb67c(,%eax,4),%eax
80104f90:	89 45 f4             	mov    %eax,-0xc(%ebp)
        while (p) {
80104f93:	eb 23                	jmp    80104fb8 <wait+0x151>
          if(p->parent == proc) {
80104f95:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104f98:	8b 50 1c             	mov    0x1c(%eax),%edx
80104f9b:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104fa1:	39 c2                	cmp    %eax,%edx
80104fa3:	75 07                	jne    80104fac <wait+0x145>
            havekids = 1;
80104fa5:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
          }
          p = p->next;
80104fac:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104faf:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
80104fb5:	89 45 f4             	mov    %eax,-0xc(%ebp)
      p = p->next;
    }
    if (!havekids) {
      for(int i = 0; i < MAX; ++i){
        p = ptable.pLists.ready[i];
        while (p) {
80104fb8:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80104fbc:	75 d7                	jne    80104f95 <wait+0x12e>
        return pid;
      }
      p = p->next;
    }
    if (!havekids) {
      for(int i = 0; i < MAX; ++i){
80104fbe:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
80104fc2:	83 7d ec 06          	cmpl   $0x6,-0x14(%ebp)
80104fc6:	7e b9                	jle    80104f81 <wait+0x11a>
          }
          p = p->next;
        }
      }
    }
    if (!havekids) {
80104fc8:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80104fcc:	75 33                	jne    80105001 <wait+0x19a>
      p = ptable.pLists.sleep;
80104fce:	a1 d8 70 11 80       	mov    0x801170d8,%eax
80104fd3:	89 45 f4             	mov    %eax,-0xc(%ebp)
      while (p) {
80104fd6:	eb 23                	jmp    80104ffb <wait+0x194>
        if(p->parent == proc) {
80104fd8:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104fdb:	8b 50 1c             	mov    0x1c(%eax),%edx
80104fde:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104fe4:	39 c2                	cmp    %eax,%edx
80104fe6:	75 07                	jne    80104fef <wait+0x188>
          havekids = 1;
80104fe8:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
        }
        p = p->next;
80104fef:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104ff2:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
80104ff8:	89 45 f4             	mov    %eax,-0xc(%ebp)
        }
      }
    }
    if (!havekids) {
      p = ptable.pLists.sleep;
      while (p) {
80104ffb:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80104fff:	75 d7                	jne    80104fd8 <wait+0x171>
          havekids = 1;
        }
        p = p->next;
      }
    }
    if (!havekids) {
80105001:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80105005:	75 33                	jne    8010503a <wait+0x1d3>
      p = ptable.pLists.running;
80105007:	a1 e0 70 11 80       	mov    0x801170e0,%eax
8010500c:	89 45 f4             	mov    %eax,-0xc(%ebp)
      while (p) {
8010500f:	eb 23                	jmp    80105034 <wait+0x1cd>
        if(p->parent == proc) {
80105011:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105014:	8b 50 1c             	mov    0x1c(%eax),%edx
80105017:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010501d:	39 c2                	cmp    %eax,%edx
8010501f:	75 07                	jne    80105028 <wait+0x1c1>
          havekids = 1;
80105021:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
        }
        p = p->next;
80105028:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010502b:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
80105031:	89 45 f4             	mov    %eax,-0xc(%ebp)
        p = p->next;
      }
    }
    if (!havekids) {
      p = ptable.pLists.running;
      while (p) {
80105034:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80105038:	75 d7                	jne    80105011 <wait+0x1aa>
        }
        p = p->next;
      }
    }
    // No point waiting if we don't have any children.
    if(!havekids || proc->killed){
8010503a:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
8010503e:	74 0d                	je     8010504d <wait+0x1e6>
80105040:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105046:	8b 40 2c             	mov    0x2c(%eax),%eax
80105049:	85 c0                	test   %eax,%eax
8010504b:	74 17                	je     80105064 <wait+0x1fd>
      release(&ptable.lock);
8010504d:	83 ec 0c             	sub    $0xc,%esp
80105050:	68 80 49 11 80       	push   $0x80114980
80105055:	e8 42 1a 00 00       	call   80106a9c <release>
8010505a:	83 c4 10             	add    $0x10,%esp
      return -1;
8010505d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105062:	eb 1c                	jmp    80105080 <wait+0x219>
    }
    // Wait for children to exit.  (See wakeup1 call in proc_exit.)
    sleep(proc, &ptable.lock);  //DOC: wait-sleep
80105064:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010506a:	83 ec 08             	sub    $0x8,%esp
8010506d:	68 80 49 11 80       	push   $0x80114980
80105072:	50                   	push   %eax
80105073:	e8 74 03 00 00       	call   801053ec <sleep>
80105078:	83 c4 10             	add    $0x10,%esp
  }
8010507b:	e9 fd fd ff ff       	jmp    80104e7d <wait+0x16>
}
80105080:	c9                   	leave  
80105081:	c3                   	ret    

80105082 <scheduler>:

#else
// CS333_P3 MLFQ scheduler implementation goes here
void																//************ SCHEDULER **************
scheduler(void)
{
80105082:	55                   	push   %ebp
80105083:	89 e5                	mov    %esp,%ebp
80105085:	83 ec 18             	sub    $0x18,%esp
  struct proc *p;
  int idle;  // for checking if processor is idle

  for(;;){
    acquire(&ptable.lock);
80105088:	83 ec 0c             	sub    $0xc,%esp
8010508b:	68 80 49 11 80       	push   $0x80114980
80105090:	e8 a0 19 00 00       	call   80106a35 <acquire>
80105095:	83 c4 10             	add    $0x10,%esp
    if(ticks >= ptable.PromoteAtTime){
80105098:	8b 15 e8 70 11 80    	mov    0x801170e8,%edx
8010509e:	a1 00 79 11 80       	mov    0x80117900,%eax
801050a3:	39 c2                	cmp    %eax,%edx
801050a5:	77 34                	ja     801050db <scheduler+0x59>
      promoteReady();				//promote ready list
801050a7:	e8 7b 17 00 00       	call   80106827 <promoteReady>
      promoteList(&ptable.pLists.running);	//promote sleep and running
801050ac:	83 ec 0c             	sub    $0xc,%esp
801050af:	68 e0 70 11 80       	push   $0x801170e0
801050b4:	e8 6a 18 00 00       	call   80106923 <promoteList>
801050b9:	83 c4 10             	add    $0x10,%esp
      promoteList(&ptable.pLists.sleep);
801050bc:	83 ec 0c             	sub    $0xc,%esp
801050bf:	68 d8 70 11 80       	push   $0x801170d8
801050c4:	e8 5a 18 00 00       	call   80106923 <promoteList>
801050c9:	83 c4 10             	add    $0x10,%esp
      ptable.PromoteAtTime = ticks + TICKS_TO_PROMOTE;
801050cc:	a1 00 79 11 80       	mov    0x80117900,%eax
801050d1:	05 20 03 00 00       	add    $0x320,%eax
801050d6:	a3 e8 70 11 80       	mov    %eax,0x801170e8
    }    
    release(&ptable.lock);
801050db:	83 ec 0c             	sub    $0xc,%esp
801050de:	68 80 49 11 80       	push   $0x80114980
801050e3:	e8 b4 19 00 00       	call   80106a9c <release>
801050e8:	83 c4 10             	add    $0x10,%esp
    // Enable interrupts on this processor.
    sti();
801050eb:	e8 03 f4 ff ff       	call   801044f3 <sti>

    idle = 1;  // assume idle unless we schedule a process
801050f0:	c7 45 f4 01 00 00 00 	movl   $0x1,-0xc(%ebp)

    acquire(&ptable.lock);
801050f7:	83 ec 0c             	sub    $0xc,%esp
801050fa:	68 80 49 11 80       	push   $0x80114980
801050ff:	e8 31 19 00 00       	call   80106a35 <acquire>
80105104:	83 c4 10             	add    $0x10,%esp
    for(int i = 0; i < MAX; ++i){
80105107:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
8010510e:	e9 c6 00 00 00       	jmp    801051d9 <scheduler+0x157>
      p = ptable.pLists.ready[i];
80105113:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105116:	05 cc 09 00 00       	add    $0x9cc,%eax
8010511b:	8b 04 85 84 49 11 80 	mov    -0x7feeb67c(,%eax,4),%eax
80105122:	89 45 ec             	mov    %eax,-0x14(%ebp)
      if (p) 
80105125:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
80105129:	0f 84 a6 00 00 00    	je     801051d5 <scheduler+0x153>
      {
        ptable.pLists.ready[i] = p->next;
8010512f:	8b 45 ec             	mov    -0x14(%ebp),%eax
80105132:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
80105138:	8b 55 f0             	mov    -0x10(%ebp),%edx
8010513b:	81 c2 cc 09 00 00    	add    $0x9cc,%edx
80105141:	89 04 95 84 49 11 80 	mov    %eax,-0x7feeb67c(,%edx,4)
        if(p->state != RUNNABLE) 
80105148:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010514b:	8b 40 0c             	mov    0xc(%eax),%eax
8010514e:	83 f8 03             	cmp    $0x3,%eax
80105151:	74 0d                	je     80105160 <scheduler+0xde>
        {
          panic("OH NO!!! There is a not RUNNABLE job on the ready list!!!\n");
80105153:	83 ec 0c             	sub    $0xc,%esp
80105156:	68 b4 a5 10 80       	push   $0x8010a5b4
8010515b:	e8 06 b4 ff ff       	call   80100566 <panic>
        }

        // switch to chosen process.  it is the process's job
        // to release ptable.lock and then reacquire it
        // before jumping back to us.
        idle = 0;  // not idle this timeslice
80105160:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
        proc = p;
80105167:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010516a:	65 a3 04 00 00 00    	mov    %eax,%gs:0x4
        switchuvm(p);
80105170:	83 ec 0c             	sub    $0xc,%esp
80105173:	ff 75 ec             	pushl  -0x14(%ebp)
80105176:	e8 39 48 00 00       	call   801099b4 <switchuvm>
8010517b:	83 c4 10             	add    $0x10,%esp
        // set cpu ticks in when a new process is scheduled
        p->cpu_ticks_in = ticks;
8010517e:	8b 15 00 79 11 80    	mov    0x80117900,%edx
80105184:	8b 45 ec             	mov    -0x14(%ebp),%eax
80105187:	89 90 8c 00 00 00    	mov    %edx,0x8c(%eax)
        // anywhere after a process is chosen
        p->state = RUNNING;
8010518d:	8b 45 ec             	mov    -0x14(%ebp),%eax
80105190:	c7 40 0c 04 00 00 00 	movl   $0x4,0xc(%eax)
        addToRunningList(p);
80105197:	83 ec 0c             	sub    $0xc,%esp
8010519a:	ff 75 ec             	pushl  -0x14(%ebp)
8010519d:	e8 3e 15 00 00       	call   801066e0 <addToRunningList>
801051a2:	83 c4 10             	add    $0x10,%esp
        swtch(&cpu->scheduler, proc->context);
801051a5:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801051ab:	8b 40 24             	mov    0x24(%eax),%eax
801051ae:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
801051b5:	83 c2 04             	add    $0x4,%edx
801051b8:	83 ec 08             	sub    $0x8,%esp
801051bb:	50                   	push   %eax
801051bc:	52                   	push   %edx
801051bd:	e8 4a 1d 00 00       	call   80106f0c <swtch>
801051c2:	83 c4 10             	add    $0x10,%esp
        switchkvm();
801051c5:	e8 cd 47 00 00       	call   80109997 <switchkvm>

        // process is done running for now.
        // it should have changed its p->state before coming back.
        proc = 0;
801051ca:	65 c7 05 04 00 00 00 	movl   $0x0,%gs:0x4
801051d1:	00 00 00 00 
    sti();

    idle = 1;  // assume idle unless we schedule a process

    acquire(&ptable.lock);
    for(int i = 0; i < MAX; ++i){
801051d5:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
801051d9:	83 7d f0 06          	cmpl   $0x6,-0x10(%ebp)
801051dd:	0f 8e 30 ff ff ff    	jle    80105113 <scheduler+0x91>
        // it should have changed its p->state before coming back.
        proc = 0;
      }
    }
//    ptable.PromoteAtTime = ticks + TICKS_TO_PROMOTE;
    release(&ptable.lock);
801051e3:	83 ec 0c             	sub    $0xc,%esp
801051e6:	68 80 49 11 80       	push   $0x80114980
801051eb:	e8 ac 18 00 00       	call   80106a9c <release>
801051f0:	83 c4 10             	add    $0x10,%esp
    // if idle, wait for next interrupt
    if (idle) {
801051f3:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801051f7:	0f 84 8b fe ff ff    	je     80105088 <scheduler+0x6>
      sti();
801051fd:	e8 f1 f2 ff ff       	call   801044f3 <sti>
      hlt();
80105202:	e8 d5 f2 ff ff       	call   801044dc <hlt>
    }
  }
80105207:	e9 7c fe ff ff       	jmp    80105088 <scheduler+0x6>

8010520c <sched>:
}

#else
void																	//*********** SCHED *****************
sched(void)
{
8010520c:	55                   	push   %ebp
8010520d:	89 e5                	mov    %esp,%ebp
8010520f:	53                   	push   %ebx
80105210:	83 ec 14             	sub    $0x14,%esp
  int intena;

  if(!holding(&ptable.lock))
80105213:	83 ec 0c             	sub    $0xc,%esp
80105216:	68 80 49 11 80       	push   $0x80114980
8010521b:	e8 48 19 00 00       	call   80106b68 <holding>
80105220:	83 c4 10             	add    $0x10,%esp
80105223:	85 c0                	test   %eax,%eax
80105225:	75 0d                	jne    80105234 <sched+0x28>
    panic("sched ptable.lock");
80105227:	83 ec 0c             	sub    $0xc,%esp
8010522a:	68 ef a5 10 80       	push   $0x8010a5ef
8010522f:	e8 32 b3 ff ff       	call   80100566 <panic>
  if(cpu->ncli != 1)
80105234:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
8010523a:	8b 80 ac 00 00 00    	mov    0xac(%eax),%eax
80105240:	83 f8 01             	cmp    $0x1,%eax
80105243:	74 0d                	je     80105252 <sched+0x46>
    panic("sched locks");
80105245:	83 ec 0c             	sub    $0xc,%esp
80105248:	68 01 a6 10 80       	push   $0x8010a601
8010524d:	e8 14 b3 ff ff       	call   80100566 <panic>
  if(proc->state == RUNNING)
80105252:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105258:	8b 40 0c             	mov    0xc(%eax),%eax
8010525b:	83 f8 04             	cmp    $0x4,%eax
8010525e:	75 0d                	jne    8010526d <sched+0x61>
    panic("sched running");
80105260:	83 ec 0c             	sub    $0xc,%esp
80105263:	68 0d a6 10 80       	push   $0x8010a60d
80105268:	e8 f9 b2 ff ff       	call   80100566 <panic>
  if(readeflags()&FL_IF)
8010526d:	e8 71 f2 ff ff       	call   801044e3 <readeflags>
80105272:	25 00 02 00 00       	and    $0x200,%eax
80105277:	85 c0                	test   %eax,%eax
80105279:	74 0d                	je     80105288 <sched+0x7c>
    panic("sched interruptible");
8010527b:	83 ec 0c             	sub    $0xc,%esp
8010527e:	68 1b a6 10 80       	push   $0x8010a61b
80105283:	e8 de b2 ff ff       	call   80100566 <panic>
  intena = cpu->intena;
80105288:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
8010528e:	8b 80 b0 00 00 00    	mov    0xb0(%eax),%eax
80105294:	89 45 f4             	mov    %eax,-0xc(%ebp)
  
  // update the total time a process is in a cpu
  proc->cpu_ticks_total += (ticks - proc->cpu_ticks_in);
80105297:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010529d:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
801052a4:	8b 8a 88 00 00 00    	mov    0x88(%edx),%ecx
801052aa:	8b 1d 00 79 11 80    	mov    0x80117900,%ebx
801052b0:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
801052b7:	8b 92 8c 00 00 00    	mov    0x8c(%edx),%edx
801052bd:	29 d3                	sub    %edx,%ebx
801052bf:	89 da                	mov    %ebx,%edx
801052c1:	01 ca                	add    %ecx,%edx
801052c3:	89 90 88 00 00 00    	mov    %edx,0x88(%eax)
  //removeFromRunningList(proc);
  //if (rc == -1) panic("removeFromRunningList failed in sched!!!\n");
  // everytime the proc leaves
  demote(proc);
801052c9:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801052cf:	83 ec 0c             	sub    $0xc,%esp
801052d2:	50                   	push   %eax
801052d3:	e8 96 16 00 00       	call   8010696e <demote>
801052d8:	83 c4 10             	add    $0x10,%esp
  swtch(&proc->context, cpu->scheduler);
801052db:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
801052e1:	8b 40 04             	mov    0x4(%eax),%eax
801052e4:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
801052eb:	83 c2 24             	add    $0x24,%edx
801052ee:	83 ec 08             	sub    $0x8,%esp
801052f1:	50                   	push   %eax
801052f2:	52                   	push   %edx
801052f3:	e8 14 1c 00 00       	call   80106f0c <swtch>
801052f8:	83 c4 10             	add    $0x10,%esp
  cpu->intena = intena;
801052fb:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80105301:	8b 55 f4             	mov    -0xc(%ebp),%edx
80105304:	89 90 b0 00 00 00    	mov    %edx,0xb0(%eax)
  //proc->budget = (proc->budget) - (proc->cpu_ticks_total);
}
8010530a:	90                   	nop
8010530b:	8b 5d fc             	mov    -0x4(%ebp),%ebx
8010530e:	c9                   	leave  
8010530f:	c3                   	ret    

80105310 <yield>:
#endif

// Give up the CPU for one scheduling round.
void																	//************* YIELD *************
yield(void)
{
80105310:	55                   	push   %ebp
80105311:	89 e5                	mov    %esp,%ebp
80105313:	83 ec 18             	sub    $0x18,%esp
  acquire(&ptable.lock);  //DOC: yieldlock
80105316:	83 ec 0c             	sub    $0xc,%esp
80105319:	68 80 49 11 80       	push   $0x80114980
8010531e:	e8 12 17 00 00       	call   80106a35 <acquire>
80105323:	83 c4 10             	add    $0x10,%esp
  #ifdef CS333_P3P4
  int rc = removeFromRunningList(proc);
80105326:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010532c:	83 ec 0c             	sub    $0xc,%esp
8010532f:	50                   	push   %eax
80105330:	e8 15 14 00 00       	call   8010674a <removeFromRunningList>
80105335:	83 c4 10             	add    $0x10,%esp
80105338:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if (rc == -1) panic("removeFromRunningList failed in yield!\n");
8010533b:	83 7d f4 ff          	cmpl   $0xffffffff,-0xc(%ebp)
8010533f:	75 0d                	jne    8010534e <yield+0x3e>
80105341:	83 ec 0c             	sub    $0xc,%esp
80105344:	68 30 a6 10 80       	push   $0x8010a630
80105349:	e8 18 b2 ff ff       	call   80100566 <panic>
  proc->state = RUNNABLE;
8010534e:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105354:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
  demote(proc);
8010535b:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105361:	83 ec 0c             	sub    $0xc,%esp
80105364:	50                   	push   %eax
80105365:	e8 04 16 00 00       	call   8010696e <demote>
8010536a:	83 c4 10             	add    $0x10,%esp
  addToReadyList(proc, proc->priority);
8010536d:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105373:	8b 80 94 00 00 00    	mov    0x94(%eax),%eax
80105379:	89 c2                	mov    %eax,%edx
8010537b:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105381:	83 ec 08             	sub    $0x8,%esp
80105384:	52                   	push   %edx
80105385:	50                   	push   %eax
80105386:	e8 37 0c 00 00       	call   80105fc2 <addToReadyList>
8010538b:	83 c4 10             	add    $0x10,%esp
  #endif
  sched();
8010538e:	e8 79 fe ff ff       	call   8010520c <sched>
  release(&ptable.lock);
80105393:	83 ec 0c             	sub    $0xc,%esp
80105396:	68 80 49 11 80       	push   $0x80114980
8010539b:	e8 fc 16 00 00       	call   80106a9c <release>
801053a0:	83 c4 10             	add    $0x10,%esp
}
801053a3:	90                   	nop
801053a4:	c9                   	leave  
801053a5:	c3                   	ret    

801053a6 <forkret>:

// A fork child's very first scheduling by scheduler()
// will swtch here.  "Return" to user space.
void
forkret(void)
{
801053a6:	55                   	push   %ebp
801053a7:	89 e5                	mov    %esp,%ebp
801053a9:	83 ec 08             	sub    $0x8,%esp
  static int first = 1;
  // Still holding ptable.lock from scheduler.
  release(&ptable.lock);
801053ac:	83 ec 0c             	sub    $0xc,%esp
801053af:	68 80 49 11 80       	push   $0x80114980
801053b4:	e8 e3 16 00 00       	call   80106a9c <release>
801053b9:	83 c4 10             	add    $0x10,%esp

  if (first) {
801053bc:	a1 20 d0 10 80       	mov    0x8010d020,%eax
801053c1:	85 c0                	test   %eax,%eax
801053c3:	74 24                	je     801053e9 <forkret+0x43>
    // Some initialization functions must be run in the context
    // of a regular process (e.g., they call sleep), and thus cannot 
    // be run from main().
    first = 0;
801053c5:	c7 05 20 d0 10 80 00 	movl   $0x0,0x8010d020
801053cc:	00 00 00 
    iinit(ROOTDEV);
801053cf:	83 ec 0c             	sub    $0xc,%esp
801053d2:	6a 01                	push   $0x1
801053d4:	e8 03 c3 ff ff       	call   801016dc <iinit>
801053d9:	83 c4 10             	add    $0x10,%esp
    initlog(ROOTDEV);
801053dc:	83 ec 0c             	sub    $0xc,%esp
801053df:	6a 01                	push   $0x1
801053e1:	e8 e7 df ff ff       	call   801033cd <initlog>
801053e6:	83 c4 10             	add    $0x10,%esp
  }
  
  // Return to "caller", actually trapret (see allocproc).
}
801053e9:	90                   	nop
801053ea:	c9                   	leave  
801053eb:	c3                   	ret    

801053ec <sleep>:
// Reacquires lock when awakened.
// 2016/12/28: ticklock removed from xv6. sleep() changed to
// accept a NULL lock to accommodate.
void																	//************* SLEEP ***************
sleep(void *chan, struct spinlock *lk)
{
801053ec:	55                   	push   %ebp
801053ed:	89 e5                	mov    %esp,%ebp
801053ef:	83 ec 18             	sub    $0x18,%esp
  if(proc == 0)
801053f2:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801053f8:	85 c0                	test   %eax,%eax
801053fa:	75 0d                	jne    80105409 <sleep+0x1d>
    panic("sleep");
801053fc:	83 ec 0c             	sub    $0xc,%esp
801053ff:	68 58 a6 10 80       	push   $0x8010a658
80105404:	e8 5d b1 ff ff       	call   80100566 <panic>
  // change p->state and then call sched.
  // Once we hold ptable.lock, we can be
  // guaranteed that we won't miss any wakeup
  // (wakeup runs with ptable.lock locked),
  // so it's okay to release lk.
  if(lk != &ptable.lock){  //DOC: sleeplock0
80105409:	81 7d 0c 80 49 11 80 	cmpl   $0x80114980,0xc(%ebp)
80105410:	74 24                	je     80105436 <sleep+0x4a>
    acquire(&ptable.lock);  //DOC: sleeplock1
80105412:	83 ec 0c             	sub    $0xc,%esp
80105415:	68 80 49 11 80       	push   $0x80114980
8010541a:	e8 16 16 00 00       	call   80106a35 <acquire>
8010541f:	83 c4 10             	add    $0x10,%esp
#ifdef NOTINUSE
    release(lk);
#else
    if (lk) release(lk);
80105422:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
80105426:	74 0e                	je     80105436 <sleep+0x4a>
80105428:	83 ec 0c             	sub    $0xc,%esp
8010542b:	ff 75 0c             	pushl  0xc(%ebp)
8010542e:	e8 69 16 00 00       	call   80106a9c <release>
80105433:	83 c4 10             	add    $0x10,%esp
#endif
  }

  // Go to sleep.
  proc->chan = chan;
80105436:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010543c:	8b 55 08             	mov    0x8(%ebp),%edx
8010543f:	89 50 28             	mov    %edx,0x28(%eax)
  #ifdef CS333_P3P4
  int rc = removeFromRunningList(proc);
80105442:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105448:	83 ec 0c             	sub    $0xc,%esp
8010544b:	50                   	push   %eax
8010544c:	e8 f9 12 00 00       	call   8010674a <removeFromRunningList>
80105451:	83 c4 10             	add    $0x10,%esp
80105454:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if (rc == -1) panic("removeFromRunningList failed in yield!\n");
80105457:	83 7d f4 ff          	cmpl   $0xffffffff,-0xc(%ebp)
8010545b:	75 0d                	jne    8010546a <sleep+0x7e>
8010545d:	83 ec 0c             	sub    $0xc,%esp
80105460:	68 30 a6 10 80       	push   $0x8010a630
80105465:	e8 fc b0 ff ff       	call   80100566 <panic>
  demote(proc);
8010546a:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105470:	83 ec 0c             	sub    $0xc,%esp
80105473:	50                   	push   %eax
80105474:	e8 f5 14 00 00       	call   8010696e <demote>
80105479:	83 c4 10             	add    $0x10,%esp
  proc->state = SLEEPING;
8010547c:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105482:	c7 40 0c 02 00 00 00 	movl   $0x2,0xc(%eax)
  rc = addToSleepList(proc);
80105489:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010548f:	83 ec 0c             	sub    $0xc,%esp
80105492:	50                   	push   %eax
80105493:	e8 9a 0d 00 00       	call   80106232 <addToSleepList>
80105498:	83 c4 10             	add    $0x10,%esp
8010549b:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if (rc == -1) panic("Add to sleep list failed in sleep()\n");
8010549e:	83 7d f4 ff          	cmpl   $0xffffffff,-0xc(%ebp)
801054a2:	75 0d                	jne    801054b1 <sleep+0xc5>
801054a4:	83 ec 0c             	sub    $0xc,%esp
801054a7:	68 60 a6 10 80       	push   $0x8010a660
801054ac:	e8 b5 b0 ff ff       	call   80100566 <panic>
  #endif
  sched();
801054b1:	e8 56 fd ff ff       	call   8010520c <sched>

  // Tidy up.
  proc->chan = 0;
801054b6:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801054bc:	c7 40 28 00 00 00 00 	movl   $0x0,0x28(%eax)

  // Reacquire original lock.
  if(lk != &ptable.lock){  //DOC: sleeplock2
801054c3:	81 7d 0c 80 49 11 80 	cmpl   $0x80114980,0xc(%ebp)
801054ca:	74 24                	je     801054f0 <sleep+0x104>
    release(&ptable.lock);
801054cc:	83 ec 0c             	sub    $0xc,%esp
801054cf:	68 80 49 11 80       	push   $0x80114980
801054d4:	e8 c3 15 00 00       	call   80106a9c <release>
801054d9:	83 c4 10             	add    $0x10,%esp
#ifdef NOTINUSE
    acquire(lk);
#else
    if (lk) acquire(lk);
801054dc:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
801054e0:	74 0e                	je     801054f0 <sleep+0x104>
801054e2:	83 ec 0c             	sub    $0xc,%esp
801054e5:	ff 75 0c             	pushl  0xc(%ebp)
801054e8:	e8 48 15 00 00       	call   80106a35 <acquire>
801054ed:	83 c4 10             	add    $0x10,%esp
#endif
  }
}
801054f0:	90                   	nop
801054f1:	c9                   	leave  
801054f2:	c3                   	ret    

801054f3 <wakeup1>:
  }
}
#else
static void																//*********** WAKEUP1 ****************
wakeup1(void *chan)
{
801054f3:	55                   	push   %ebp
801054f4:	89 e5                	mov    %esp,%ebp
801054f6:	83 ec 18             	sub    $0x18,%esp
  if(!holding(&ptable.lock)) panic("Not holding lock in wakeup1!!!!\n");
801054f9:	83 ec 0c             	sub    $0xc,%esp
801054fc:	68 80 49 11 80       	push   $0x80114980
80105501:	e8 62 16 00 00       	call   80106b68 <holding>
80105506:	83 c4 10             	add    $0x10,%esp
80105509:	85 c0                	test   %eax,%eax
8010550b:	75 0d                	jne    8010551a <wakeup1+0x27>
8010550d:	83 ec 0c             	sub    $0xc,%esp
80105510:	68 88 a6 10 80       	push   $0x8010a688
80105515:	e8 4c b0 ff ff       	call   80100566 <panic>
  if(!ptable.pLists.sleep) 
8010551a:	a1 d8 70 11 80       	mov    0x801170d8,%eax
8010551f:	85 c0                	test   %eax,%eax
80105521:	74 6f                	je     80105592 <wakeup1+0x9f>
  {
    return;
  }

  struct proc *p = ptable.pLists.sleep;
80105523:	a1 d8 70 11 80       	mov    0x801170d8,%eax
80105528:	89 45 f4             	mov    %eax,-0xc(%ebp)
  while (p)
8010552b:	eb 5d                	jmp    8010558a <wakeup1+0x97>
  {
    if(p->chan == chan)
8010552d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105530:	8b 40 28             	mov    0x28(%eax),%eax
80105533:	3b 45 08             	cmp    0x8(%ebp),%eax
80105536:	75 46                	jne    8010557e <wakeup1+0x8b>
    {
      int rc = removeFromSleepList(p);
80105538:	83 ec 0c             	sub    $0xc,%esp
8010553b:	ff 75 f4             	pushl  -0xc(%ebp)
8010553e:	e8 69 0d 00 00       	call   801062ac <removeFromSleepList>
80105543:	83 c4 10             	add    $0x10,%esp
80105546:	89 45 f0             	mov    %eax,-0x10(%ebp)
      if (rc == -1) panic("removeFromSleepList failed in wakeup1!\n");
80105549:	83 7d f0 ff          	cmpl   $0xffffffff,-0x10(%ebp)
8010554d:	75 0d                	jne    8010555c <wakeup1+0x69>
8010554f:	83 ec 0c             	sub    $0xc,%esp
80105552:	68 ac a6 10 80       	push   $0x8010a6ac
80105557:	e8 0a b0 ff ff       	call   80100566 <panic>
      p->state = RUNNABLE;
8010555c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010555f:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
      addToReadyList(p, p->priority);
80105566:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105569:	8b 80 94 00 00 00    	mov    0x94(%eax),%eax
8010556f:	83 ec 08             	sub    $0x8,%esp
80105572:	50                   	push   %eax
80105573:	ff 75 f4             	pushl  -0xc(%ebp)
80105576:	e8 47 0a 00 00       	call   80105fc2 <addToReadyList>
8010557b:	83 c4 10             	add    $0x10,%esp
    }
    p = p->next;
8010557e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105581:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
80105587:	89 45 f4             	mov    %eax,-0xc(%ebp)
  {
    return;
  }

  struct proc *p = ptable.pLists.sleep;
  while (p)
8010558a:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
8010558e:	75 9d                	jne    8010552d <wakeup1+0x3a>
80105590:	eb 01                	jmp    80105593 <wakeup1+0xa0>
wakeup1(void *chan)
{
  if(!holding(&ptable.lock)) panic("Not holding lock in wakeup1!!!!\n");
  if(!ptable.pLists.sleep) 
  {
    return;
80105592:	90                   	nop
      p->state = RUNNABLE;
      addToReadyList(p, p->priority);
    }
    p = p->next;
  }
}
80105593:	c9                   	leave  
80105594:	c3                   	ret    

80105595 <wakeup>:


// Wake up all processes sleeping on chan.
void
wakeup(void *chan)
{
80105595:	55                   	push   %ebp
80105596:	89 e5                	mov    %esp,%ebp
80105598:	83 ec 08             	sub    $0x8,%esp
  acquire(&ptable.lock);
8010559b:	83 ec 0c             	sub    $0xc,%esp
8010559e:	68 80 49 11 80       	push   $0x80114980
801055a3:	e8 8d 14 00 00       	call   80106a35 <acquire>
801055a8:	83 c4 10             	add    $0x10,%esp
  wakeup1(chan);
801055ab:	83 ec 0c             	sub    $0xc,%esp
801055ae:	ff 75 08             	pushl  0x8(%ebp)
801055b1:	e8 3d ff ff ff       	call   801054f3 <wakeup1>
801055b6:	83 c4 10             	add    $0x10,%esp
  release(&ptable.lock);
801055b9:	83 ec 0c             	sub    $0xc,%esp
801055bc:	68 80 49 11 80       	push   $0x80114980
801055c1:	e8 d6 14 00 00       	call   80106a9c <release>
801055c6:	83 c4 10             	add    $0x10,%esp
}
801055c9:	90                   	nop
801055ca:	c9                   	leave  
801055cb:	c3                   	ret    

801055cc <kill>:
  return -1;
}
#else
int																//************** KILL ******************
kill(int pid)
{
801055cc:	55                   	push   %ebp
801055cd:	89 e5                	mov    %esp,%ebp
801055cf:	83 ec 18             	sub    $0x18,%esp
  struct proc *p;

  acquire(&ptable.lock);
801055d2:	83 ec 0c             	sub    $0xc,%esp
801055d5:	68 80 49 11 80       	push   $0x80114980
801055da:	e8 56 14 00 00       	call   80106a35 <acquire>
801055df:	83 c4 10             	add    $0x10,%esp
  p = ptable.pLists.sleep;
801055e2:	a1 d8 70 11 80       	mov    0x801170d8,%eax
801055e7:	89 45 f4             	mov    %eax,-0xc(%ebp)
  while (p) { 
801055ea:	e9 90 00 00 00       	jmp    8010567f <kill+0xb3>
    if (p->pid == pid){
801055ef:	8b 45 f4             	mov    -0xc(%ebp),%eax
801055f2:	8b 50 10             	mov    0x10(%eax),%edx
801055f5:	8b 45 08             	mov    0x8(%ebp),%eax
801055f8:	39 c2                	cmp    %eax,%edx
801055fa:	75 77                	jne    80105673 <kill+0xa7>
      p->killed = 1;
801055fc:	8b 45 f4             	mov    -0xc(%ebp),%eax
801055ff:	c7 40 2c 01 00 00 00 	movl   $0x1,0x2c(%eax)
      int rc = removeFromSleepList(p);
80105606:	83 ec 0c             	sub    $0xc,%esp
80105609:	ff 75 f4             	pushl  -0xc(%ebp)
8010560c:	e8 9b 0c 00 00       	call   801062ac <removeFromSleepList>
80105611:	83 c4 10             	add    $0x10,%esp
80105614:	89 45 ec             	mov    %eax,-0x14(%ebp)
      if (rc == -1) panic("removeFromSleepList failed in kill!!!\n");
80105617:	83 7d ec ff          	cmpl   $0xffffffff,-0x14(%ebp)
8010561b:	75 0d                	jne    8010562a <kill+0x5e>
8010561d:	83 ec 0c             	sub    $0xc,%esp
80105620:	68 d4 a6 10 80       	push   $0x8010a6d4
80105625:	e8 3c af ff ff       	call   80100566 <panic>
      p->state = RUNNABLE;
8010562a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010562d:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
      p->priority = 0;
80105634:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105637:	c7 80 94 00 00 00 00 	movl   $0x0,0x94(%eax)
8010563e:	00 00 00 
      addToReadyList(p, p->priority);
80105641:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105644:	8b 80 94 00 00 00    	mov    0x94(%eax),%eax
8010564a:	83 ec 08             	sub    $0x8,%esp
8010564d:	50                   	push   %eax
8010564e:	ff 75 f4             	pushl  -0xc(%ebp)
80105651:	e8 6c 09 00 00       	call   80105fc2 <addToReadyList>
80105656:	83 c4 10             	add    $0x10,%esp
      release(&ptable.lock);
80105659:	83 ec 0c             	sub    $0xc,%esp
8010565c:	68 80 49 11 80       	push   $0x80114980
80105661:	e8 36 14 00 00       	call   80106a9c <release>
80105666:	83 c4 10             	add    $0x10,%esp
      return 0;
80105669:	b8 00 00 00 00       	mov    $0x0,%eax
8010566e:	e9 29 01 00 00       	jmp    8010579c <kill+0x1d0>
    }
    p = p->next;
80105673:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105676:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
8010567c:	89 45 f4             	mov    %eax,-0xc(%ebp)
{
  struct proc *p;

  acquire(&ptable.lock);
  p = ptable.pLists.sleep;
  while (p) { 
8010567f:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80105683:	0f 85 66 ff ff ff    	jne    801055ef <kill+0x23>
      release(&ptable.lock);
      return 0;
    }
    p = p->next;
  }
  p = ptable.pLists.zombie;
80105689:	a1 dc 70 11 80       	mov    0x801170dc,%eax
8010568e:	89 45 f4             	mov    %eax,-0xc(%ebp)
  while (p) { 
80105691:	eb 3d                	jmp    801056d0 <kill+0x104>
    if (p->pid == pid){
80105693:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105696:	8b 50 10             	mov    0x10(%eax),%edx
80105699:	8b 45 08             	mov    0x8(%ebp),%eax
8010569c:	39 c2                	cmp    %eax,%edx
8010569e:	75 24                	jne    801056c4 <kill+0xf8>
      p->killed = 1;
801056a0:	8b 45 f4             	mov    -0xc(%ebp),%eax
801056a3:	c7 40 2c 01 00 00 00 	movl   $0x1,0x2c(%eax)
      release(&ptable.lock);
801056aa:	83 ec 0c             	sub    $0xc,%esp
801056ad:	68 80 49 11 80       	push   $0x80114980
801056b2:	e8 e5 13 00 00       	call   80106a9c <release>
801056b7:	83 c4 10             	add    $0x10,%esp
      return 0;
801056ba:	b8 00 00 00 00       	mov    $0x0,%eax
801056bf:	e9 d8 00 00 00       	jmp    8010579c <kill+0x1d0>
    }
    p = p->next;
801056c4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801056c7:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
801056cd:	89 45 f4             	mov    %eax,-0xc(%ebp)
      return 0;
    }
    p = p->next;
  }
  p = ptable.pLists.zombie;
  while (p) { 
801056d0:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801056d4:	75 bd                	jne    80105693 <kill+0xc7>
      release(&ptable.lock);
      return 0;
    }
    p = p->next;
  }
  for(int i = 0; i < MAX; ++i){
801056d6:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
801056dd:	eb 58                	jmp    80105737 <kill+0x16b>
    p = ptable.pLists.ready[i];
801056df:	8b 45 f0             	mov    -0x10(%ebp),%eax
801056e2:	05 cc 09 00 00       	add    $0x9cc,%eax
801056e7:	8b 04 85 84 49 11 80 	mov    -0x7feeb67c(,%eax,4),%eax
801056ee:	89 45 f4             	mov    %eax,-0xc(%ebp)
    while (p) { 
801056f1:	eb 3a                	jmp    8010572d <kill+0x161>
      if (p->pid == pid){
801056f3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801056f6:	8b 50 10             	mov    0x10(%eax),%edx
801056f9:	8b 45 08             	mov    0x8(%ebp),%eax
801056fc:	39 c2                	cmp    %eax,%edx
801056fe:	75 21                	jne    80105721 <kill+0x155>
        p->killed = 1;
80105700:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105703:	c7 40 2c 01 00 00 00 	movl   $0x1,0x2c(%eax)
        release(&ptable.lock);
8010570a:	83 ec 0c             	sub    $0xc,%esp
8010570d:	68 80 49 11 80       	push   $0x80114980
80105712:	e8 85 13 00 00       	call   80106a9c <release>
80105717:	83 c4 10             	add    $0x10,%esp
        return 0;
8010571a:	b8 00 00 00 00       	mov    $0x0,%eax
8010571f:	eb 7b                	jmp    8010579c <kill+0x1d0>
      }
      p = p->next;
80105721:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105724:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
8010572a:	89 45 f4             	mov    %eax,-0xc(%ebp)
    }
    p = p->next;
  }
  for(int i = 0; i < MAX; ++i){
    p = ptable.pLists.ready[i];
    while (p) { 
8010572d:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80105731:	75 c0                	jne    801056f3 <kill+0x127>
      release(&ptable.lock);
      return 0;
    }
    p = p->next;
  }
  for(int i = 0; i < MAX; ++i){
80105733:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
80105737:	83 7d f0 06          	cmpl   $0x6,-0x10(%ebp)
8010573b:	7e a2                	jle    801056df <kill+0x113>
        return 0;
      }
      p = p->next;
    }
  }
  p = ptable.pLists.running;
8010573d:	a1 e0 70 11 80       	mov    0x801170e0,%eax
80105742:	89 45 f4             	mov    %eax,-0xc(%ebp)
  while (p) { 
80105745:	eb 3a                	jmp    80105781 <kill+0x1b5>
    if (p->pid == pid){
80105747:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010574a:	8b 50 10             	mov    0x10(%eax),%edx
8010574d:	8b 45 08             	mov    0x8(%ebp),%eax
80105750:	39 c2                	cmp    %eax,%edx
80105752:	75 21                	jne    80105775 <kill+0x1a9>
      p->killed = 1;
80105754:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105757:	c7 40 2c 01 00 00 00 	movl   $0x1,0x2c(%eax)
      release(&ptable.lock);
8010575e:	83 ec 0c             	sub    $0xc,%esp
80105761:	68 80 49 11 80       	push   $0x80114980
80105766:	e8 31 13 00 00       	call   80106a9c <release>
8010576b:	83 c4 10             	add    $0x10,%esp
      return 0;
8010576e:	b8 00 00 00 00       	mov    $0x0,%eax
80105773:	eb 27                	jmp    8010579c <kill+0x1d0>
    }
    p = p->next;
80105775:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105778:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
8010577e:	89 45 f4             	mov    %eax,-0xc(%ebp)
      }
      p = p->next;
    }
  }
  p = ptable.pLists.running;
  while (p) { 
80105781:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80105785:	75 c0                	jne    80105747 <kill+0x17b>
      release(&ptable.lock);
      return 0;
    }
    p = p->next;
  }
  release(&ptable.lock);
80105787:	83 ec 0c             	sub    $0xc,%esp
8010578a:	68 80 49 11 80       	push   $0x80114980
8010578f:	e8 08 13 00 00       	call   80106a9c <release>
80105794:	83 c4 10             	add    $0x10,%esp
  return -1;
80105797:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
8010579c:	c9                   	leave  
8010579d:	c3                   	ret    

8010579e <procdump>:
// Print a process listing to console.  For debugging.
// Runs when user types ^P on console.
// No lock to avoid wedging a stuck machine further.
void																	//************ PROCDUMP ************
procdump(void)
{
8010579e:	55                   	push   %ebp
8010579f:	89 e5                	mov    %esp,%ebp
801057a1:	57                   	push   %edi
801057a2:	56                   	push   %esi
801057a3:	53                   	push   %ebx
801057a4:	83 ec 6c             	sub    $0x6c,%esp
  int i, ppid;
  struct proc *p;
  char *state;
  uint pc[10], ticks0;

  ticks0 = ticks;   // close enough
801057a7:	a1 00 79 11 80       	mov    0x80117900,%eax
801057ac:	89 45 d4             	mov    %eax,-0x2c(%ebp)

  cprintf("\nPID\tName        UID        GID     PPID\tPrio\tElapsed\tCPU\tState\tSize\tPCs\n");
801057af:	83 ec 0c             	sub    $0xc,%esp
801057b2:	68 28 a7 10 80       	push   $0x8010a728
801057b7:	e8 0a ac ff ff       	call   801003c6 <cprintf>
801057bc:	83 c4 10             	add    $0x10,%esp

  
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
801057bf:	c7 45 dc b4 49 11 80 	movl   $0x801149b4,-0x24(%ebp)
801057c6:	e9 29 02 00 00       	jmp    801059f4 <procdump+0x256>

    if(p->state == UNUSED)
801057cb:	8b 45 dc             	mov    -0x24(%ebp),%eax
801057ce:	8b 40 0c             	mov    0xc(%eax),%eax
801057d1:	85 c0                	test   %eax,%eax
801057d3:	0f 84 13 02 00 00    	je     801059ec <procdump+0x24e>
      continue;
    if(p->state >= 0 && p->state < NELEM(states) && states[p->state])
801057d9:	8b 45 dc             	mov    -0x24(%ebp),%eax
801057dc:	8b 40 0c             	mov    0xc(%eax),%eax
801057df:	83 f8 05             	cmp    $0x5,%eax
801057e2:	77 23                	ja     80105807 <procdump+0x69>
801057e4:	8b 45 dc             	mov    -0x24(%ebp),%eax
801057e7:	8b 40 0c             	mov    0xc(%eax),%eax
801057ea:	8b 04 85 08 d0 10 80 	mov    -0x7fef2ff8(,%eax,4),%eax
801057f1:	85 c0                	test   %eax,%eax
801057f3:	74 12                	je     80105807 <procdump+0x69>
      state = states[p->state];
801057f5:	8b 45 dc             	mov    -0x24(%ebp),%eax
801057f8:	8b 40 0c             	mov    0xc(%eax),%eax
801057fb:	8b 04 85 08 d0 10 80 	mov    -0x7fef2ff8(,%eax,4),%eax
80105802:	89 45 d8             	mov    %eax,-0x28(%ebp)
80105805:	eb 07                	jmp    8010580e <procdump+0x70>
    else
      state = "???";
80105807:	c7 45 d8 72 a7 10 80 	movl   $0x8010a772,-0x28(%ebp)

    if (p->parent) ppid = p->parent->pid;
8010580e:	8b 45 dc             	mov    -0x24(%ebp),%eax
80105811:	8b 40 1c             	mov    0x1c(%eax),%eax
80105814:	85 c0                	test   %eax,%eax
80105816:	74 0e                	je     80105826 <procdump+0x88>
80105818:	8b 45 dc             	mov    -0x24(%ebp),%eax
8010581b:	8b 40 1c             	mov    0x1c(%eax),%eax
8010581e:	8b 40 10             	mov    0x10(%eax),%eax
80105821:	89 45 e0             	mov    %eax,-0x20(%ebp)
80105824:	eb 09                	jmp    8010582f <procdump+0x91>
    else ppid = p->pid;
80105826:	8b 45 dc             	mov    -0x24(%ebp),%eax
80105829:	8b 40 10             	mov    0x10(%eax),%eax
8010582c:	89 45 e0             	mov    %eax,-0x20(%ebp)
    int len = strlen(p->name);
8010582f:	8b 45 dc             	mov    -0x24(%ebp),%eax
80105832:	83 c0 74             	add    $0x74,%eax
80105835:	83 ec 0c             	sub    $0xc,%esp
80105838:	50                   	push   %eax
80105839:	e8 a7 16 00 00       	call   80106ee5 <strlen>
8010583e:	83 c4 10             	add    $0x10,%esp
80105841:	89 45 d0             	mov    %eax,-0x30(%ebp)
    cprintf("%d\t%s", p->pid, p->name);
80105844:	8b 45 dc             	mov    -0x24(%ebp),%eax
80105847:	8d 50 74             	lea    0x74(%eax),%edx
8010584a:	8b 45 dc             	mov    -0x24(%ebp),%eax
8010584d:	8b 40 10             	mov    0x10(%eax),%eax
80105850:	83 ec 04             	sub    $0x4,%esp
80105853:	52                   	push   %edx
80105854:	50                   	push   %eax
80105855:	68 76 a7 10 80       	push   $0x8010a776
8010585a:	e8 67 ab ff ff       	call   801003c6 <cprintf>
8010585f:	83 c4 10             	add    $0x10,%esp
    int MAXNAME = 12;
80105862:	c7 45 cc 0c 00 00 00 	movl   $0xc,-0x34(%ebp)
    for (i=len; i<=MAXNAME; i++) cprintf(" ");
80105869:	8b 45 d0             	mov    -0x30(%ebp),%eax
8010586c:	89 45 e4             	mov    %eax,-0x1c(%ebp)
8010586f:	eb 14                	jmp    80105885 <procdump+0xe7>
80105871:	83 ec 0c             	sub    $0xc,%esp
80105874:	68 7c a7 10 80       	push   $0x8010a77c
80105879:	e8 48 ab ff ff       	call   801003c6 <cprintf>
8010587e:	83 c4 10             	add    $0x10,%esp
80105881:	83 45 e4 01          	addl   $0x1,-0x1c(%ebp)
80105885:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80105888:	3b 45 cc             	cmp    -0x34(%ebp),%eax
8010588b:	7e e4                	jle    80105871 <procdump+0xd3>
    cprintf("%d", p->uid);
8010588d:	8b 45 dc             	mov    -0x24(%ebp),%eax
80105890:	8b 40 14             	mov    0x14(%eax),%eax
80105893:	83 ec 08             	sub    $0x8,%esp
80105896:	50                   	push   %eax
80105897:	68 7e a7 10 80       	push   $0x8010a77e
8010589c:	e8 25 ab ff ff       	call   801003c6 <cprintf>
801058a1:	83 c4 10             	add    $0x10,%esp
    if (p->uid <100) cprintf("\t\t");
801058a4:	8b 45 dc             	mov    -0x24(%ebp),%eax
801058a7:	8b 40 14             	mov    0x14(%eax),%eax
801058aa:	83 f8 63             	cmp    $0x63,%eax
801058ad:	77 12                	ja     801058c1 <procdump+0x123>
801058af:	83 ec 0c             	sub    $0xc,%esp
801058b2:	68 81 a7 10 80       	push   $0x8010a781
801058b7:	e8 0a ab ff ff       	call   801003c6 <cprintf>
801058bc:	83 c4 10             	add    $0x10,%esp
801058bf:	eb 10                	jmp    801058d1 <procdump+0x133>
    else cprintf("\t");
801058c1:	83 ec 0c             	sub    $0xc,%esp
801058c4:	68 84 a7 10 80       	push   $0x8010a784
801058c9:	e8 f8 aa ff ff       	call   801003c6 <cprintf>
801058ce:	83 c4 10             	add    $0x10,%esp

    cprintf("%d\t%d\t%d\t%d.%d\t%d.%d\t%s\t%d\t",  p->gid, ppid, p->priority,
801058d1:	8b 45 dc             	mov    -0x24(%ebp),%eax
801058d4:	8b 38                	mov    (%eax),%edi
		    (ticks0 - p->start_ticks)/100, (ticks0 - p->start_ticks) % 100,
		    p->cpu_ticks_total/100, p->cpu_ticks_total%100, state, p->sz);
801058d6:	8b 45 dc             	mov    -0x24(%ebp),%eax
801058d9:	8b 88 88 00 00 00    	mov    0x88(%eax),%ecx
    for (i=len; i<=MAXNAME; i++) cprintf(" ");
    cprintf("%d", p->uid);
    if (p->uid <100) cprintf("\t\t");
    else cprintf("\t");

    cprintf("%d\t%d\t%d\t%d.%d\t%d.%d\t%s\t%d\t",  p->gid, ppid, p->priority,
801058df:	ba 1f 85 eb 51       	mov    $0x51eb851f,%edx
801058e4:	89 c8                	mov    %ecx,%eax
801058e6:	f7 e2                	mul    %edx
801058e8:	89 d6                	mov    %edx,%esi
801058ea:	c1 ee 05             	shr    $0x5,%esi
801058ed:	6b c6 64             	imul   $0x64,%esi,%eax
801058f0:	89 ce                	mov    %ecx,%esi
801058f2:	29 c6                	sub    %eax,%esi
		    (ticks0 - p->start_ticks)/100, (ticks0 - p->start_ticks) % 100,
		    p->cpu_ticks_total/100, p->cpu_ticks_total%100, state, p->sz);
801058f4:	8b 45 dc             	mov    -0x24(%ebp),%eax
801058f7:	8b 80 88 00 00 00    	mov    0x88(%eax),%eax
    for (i=len; i<=MAXNAME; i++) cprintf(" ");
    cprintf("%d", p->uid);
    if (p->uid <100) cprintf("\t\t");
    else cprintf("\t");

    cprintf("%d\t%d\t%d\t%d.%d\t%d.%d\t%s\t%d\t",  p->gid, ppid, p->priority,
801058fd:	ba 1f 85 eb 51       	mov    $0x51eb851f,%edx
80105902:	f7 e2                	mul    %edx
80105904:	c1 ea 05             	shr    $0x5,%edx
80105907:	89 55 94             	mov    %edx,-0x6c(%ebp)
		    (ticks0 - p->start_ticks)/100, (ticks0 - p->start_ticks) % 100,
8010590a:	8b 45 dc             	mov    -0x24(%ebp),%eax
8010590d:	8b 80 84 00 00 00    	mov    0x84(%eax),%eax
80105913:	8b 55 d4             	mov    -0x2c(%ebp),%edx
80105916:	89 d1                	mov    %edx,%ecx
80105918:	29 c1                	sub    %eax,%ecx
    for (i=len; i<=MAXNAME; i++) cprintf(" ");
    cprintf("%d", p->uid);
    if (p->uid <100) cprintf("\t\t");
    else cprintf("\t");

    cprintf("%d\t%d\t%d\t%d.%d\t%d.%d\t%s\t%d\t",  p->gid, ppid, p->priority,
8010591a:	ba 1f 85 eb 51       	mov    $0x51eb851f,%edx
8010591f:	89 c8                	mov    %ecx,%eax
80105921:	f7 e2                	mul    %edx
80105923:	89 d3                	mov    %edx,%ebx
80105925:	c1 eb 05             	shr    $0x5,%ebx
80105928:	6b c3 64             	imul   $0x64,%ebx,%eax
8010592b:	89 cb                	mov    %ecx,%ebx
8010592d:	29 c3                	sub    %eax,%ebx
		    (ticks0 - p->start_ticks)/100, (ticks0 - p->start_ticks) % 100,
8010592f:	8b 45 dc             	mov    -0x24(%ebp),%eax
80105932:	8b 80 84 00 00 00    	mov    0x84(%eax),%eax
80105938:	8b 55 d4             	mov    -0x2c(%ebp),%edx
8010593b:	89 d1                	mov    %edx,%ecx
8010593d:	29 c1                	sub    %eax,%ecx
8010593f:	89 c8                	mov    %ecx,%eax
    for (i=len; i<=MAXNAME; i++) cprintf(" ");
    cprintf("%d", p->uid);
    if (p->uid <100) cprintf("\t\t");
    else cprintf("\t");

    cprintf("%d\t%d\t%d\t%d.%d\t%d.%d\t%s\t%d\t",  p->gid, ppid, p->priority,
80105941:	ba 1f 85 eb 51       	mov    $0x51eb851f,%edx
80105946:	f7 e2                	mul    %edx
80105948:	89 d1                	mov    %edx,%ecx
8010594a:	c1 e9 05             	shr    $0x5,%ecx
8010594d:	8b 45 dc             	mov    -0x24(%ebp),%eax
80105950:	8b 90 94 00 00 00    	mov    0x94(%eax),%edx
80105956:	8b 45 dc             	mov    -0x24(%ebp),%eax
80105959:	8b 40 18             	mov    0x18(%eax),%eax
8010595c:	83 ec 08             	sub    $0x8,%esp
8010595f:	57                   	push   %edi
80105960:	ff 75 d8             	pushl  -0x28(%ebp)
80105963:	56                   	push   %esi
80105964:	ff 75 94             	pushl  -0x6c(%ebp)
80105967:	53                   	push   %ebx
80105968:	51                   	push   %ecx
80105969:	52                   	push   %edx
8010596a:	ff 75 e0             	pushl  -0x20(%ebp)
8010596d:	50                   	push   %eax
8010596e:	68 86 a7 10 80       	push   $0x8010a786
80105973:	e8 4e aa ff ff       	call   801003c6 <cprintf>
80105978:	83 c4 30             	add    $0x30,%esp
		    (ticks0 - p->start_ticks)/100, (ticks0 - p->start_ticks) % 100,
		    p->cpu_ticks_total/100, p->cpu_ticks_total%100, state, p->sz);

    if(p->state == SLEEPING){
8010597b:	8b 45 dc             	mov    -0x24(%ebp),%eax
8010597e:	8b 40 0c             	mov    0xc(%eax),%eax
80105981:	83 f8 02             	cmp    $0x2,%eax
80105984:	75 54                	jne    801059da <procdump+0x23c>
      getcallerpcs((uint*)p->context->ebp+2, pc);
80105986:	8b 45 dc             	mov    -0x24(%ebp),%eax
80105989:	8b 40 24             	mov    0x24(%eax),%eax
8010598c:	8b 40 0c             	mov    0xc(%eax),%eax
8010598f:	83 c0 08             	add    $0x8,%eax
80105992:	89 c2                	mov    %eax,%edx
80105994:	83 ec 08             	sub    $0x8,%esp
80105997:	8d 45 a4             	lea    -0x5c(%ebp),%eax
8010599a:	50                   	push   %eax
8010599b:	52                   	push   %edx
8010599c:	e8 4d 11 00 00       	call   80106aee <getcallerpcs>
801059a1:	83 c4 10             	add    $0x10,%esp
      for(i=0; i<10 && pc[i] != 0; i++)
801059a4:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
801059ab:	eb 1c                	jmp    801059c9 <procdump+0x22b>
        cprintf(" %p", pc[i]);
801059ad:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801059b0:	8b 44 85 a4          	mov    -0x5c(%ebp,%eax,4),%eax
801059b4:	83 ec 08             	sub    $0x8,%esp
801059b7:	50                   	push   %eax
801059b8:	68 a2 a7 10 80       	push   $0x8010a7a2
801059bd:	e8 04 aa ff ff       	call   801003c6 <cprintf>
801059c2:	83 c4 10             	add    $0x10,%esp
		    (ticks0 - p->start_ticks)/100, (ticks0 - p->start_ticks) % 100,
		    p->cpu_ticks_total/100, p->cpu_ticks_total%100, state, p->sz);

    if(p->state == SLEEPING){
      getcallerpcs((uint*)p->context->ebp+2, pc);
      for(i=0; i<10 && pc[i] != 0; i++)
801059c5:	83 45 e4 01          	addl   $0x1,-0x1c(%ebp)
801059c9:	83 7d e4 09          	cmpl   $0x9,-0x1c(%ebp)
801059cd:	7f 0b                	jg     801059da <procdump+0x23c>
801059cf:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801059d2:	8b 44 85 a4          	mov    -0x5c(%ebp,%eax,4),%eax
801059d6:	85 c0                	test   %eax,%eax
801059d8:	75 d3                	jne    801059ad <procdump+0x20f>
        cprintf(" %p", pc[i]);
    }
    cprintf("\n");
801059da:	83 ec 0c             	sub    $0xc,%esp
801059dd:	68 a6 a7 10 80       	push   $0x8010a7a6
801059e2:	e8 df a9 ff ff       	call   801003c6 <cprintf>
801059e7:	83 c4 10             	add    $0x10,%esp
801059ea:	eb 01                	jmp    801059ed <procdump+0x24f>

  
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){

    if(p->state == UNUSED)
      continue;
801059ec:	90                   	nop
  ticks0 = ticks;   // close enough

  cprintf("\nPID\tName        UID        GID     PPID\tPrio\tElapsed\tCPU\tState\tSize\tPCs\n");

  
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
801059ed:	81 45 dc 9c 00 00 00 	addl   $0x9c,-0x24(%ebp)
801059f4:	81 7d dc b4 70 11 80 	cmpl   $0x801170b4,-0x24(%ebp)
801059fb:	0f 82 ca fd ff ff    	jb     801057cb <procdump+0x2d>
      for(i=0; i<10 && pc[i] != 0; i++)
        cprintf(" %p", pc[i]);
    }
    cprintf("\n");
  }
}
80105a01:	90                   	nop
80105a02:	8d 65 f4             	lea    -0xc(%ebp),%esp
80105a05:	5b                   	pop    %ebx
80105a06:	5e                   	pop    %esi
80105a07:	5f                   	pop    %edi
80105a08:	5d                   	pop    %ebp
80105a09:	c3                   	ret    

80105a0a <getProcInfo>:


int																	//******** getProcInfo *************
getProcInfo(int count, struct uproc* table)
{
80105a0a:	55                   	push   %ebp
80105a0b:	89 e5                	mov    %esp,%ebp
80105a0d:	83 ec 18             	sub    $0x18,%esp
    uint num = 0, ticks0;
80105a10:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
    struct proc *p;

    ticks0 = ticks;   
80105a17:	a1 00 79 11 80       	mov    0x80117900,%eax
80105a1c:	89 45 ec             	mov    %eax,-0x14(%ebp)

    acquire(&ptable.lock); //be sure to lock ptable to avoid bad data
80105a1f:	83 ec 0c             	sub    $0xc,%esp
80105a22:	68 80 49 11 80       	push   $0x80114980
80105a27:	e8 09 10 00 00       	call   80106a35 <acquire>
80105a2c:	83 c4 10             	add    $0x10,%esp
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80105a2f:	c7 45 f0 b4 49 11 80 	movl   $0x801149b4,-0x10(%ebp)
80105a36:	e9 a8 01 00 00       	jmp    80105be3 <getProcInfo+0x1d9>
      if(p->state == UNUSED) //if the proc is unused skip it
80105a3b:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105a3e:	8b 40 0c             	mov    0xc(%eax),%eax
80105a41:	85 c0                	test   %eax,%eax
80105a43:	0f 84 92 01 00 00    	je     80105bdb <getProcInfo+0x1d1>
        continue;
      if (num == count || num == NPROC) //reach the max of the table
80105a49:	8b 45 08             	mov    0x8(%ebp),%eax
80105a4c:	39 45 f4             	cmp    %eax,-0xc(%ebp)
80105a4f:	0f 84 9b 01 00 00    	je     80105bf0 <getProcInfo+0x1e6>
80105a55:	83 7d f4 40          	cmpl   $0x40,-0xc(%ebp)
80105a59:	0f 84 91 01 00 00    	je     80105bf0 <getProcInfo+0x1e6>
      break;
      //fill the table with the proc info
      table[num].pid = p->pid;
80105a5f:	8b 55 f4             	mov    -0xc(%ebp),%edx
80105a62:	89 d0                	mov    %edx,%eax
80105a64:	01 c0                	add    %eax,%eax
80105a66:	01 d0                	add    %edx,%eax
80105a68:	c1 e0 05             	shl    $0x5,%eax
80105a6b:	89 c2                	mov    %eax,%edx
80105a6d:	8b 45 0c             	mov    0xc(%ebp),%eax
80105a70:	01 c2                	add    %eax,%edx
80105a72:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105a75:	8b 40 10             	mov    0x10(%eax),%eax
80105a78:	89 02                	mov    %eax,(%edx)
      table[num].uid = p->uid;
80105a7a:	8b 55 f4             	mov    -0xc(%ebp),%edx
80105a7d:	89 d0                	mov    %edx,%eax
80105a7f:	01 c0                	add    %eax,%eax
80105a81:	01 d0                	add    %edx,%eax
80105a83:	c1 e0 05             	shl    $0x5,%eax
80105a86:	89 c2                	mov    %eax,%edx
80105a88:	8b 45 0c             	mov    0xc(%ebp),%eax
80105a8b:	01 c2                	add    %eax,%edx
80105a8d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105a90:	8b 40 14             	mov    0x14(%eax),%eax
80105a93:	89 42 04             	mov    %eax,0x4(%edx)
      table[num].gid = p->gid;
80105a96:	8b 55 f4             	mov    -0xc(%ebp),%edx
80105a99:	89 d0                	mov    %edx,%eax
80105a9b:	01 c0                	add    %eax,%eax
80105a9d:	01 d0                	add    %edx,%eax
80105a9f:	c1 e0 05             	shl    $0x5,%eax
80105aa2:	89 c2                	mov    %eax,%edx
80105aa4:	8b 45 0c             	mov    0xc(%ebp),%eax
80105aa7:	01 c2                	add    %eax,%edx
80105aa9:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105aac:	8b 40 18             	mov    0x18(%eax),%eax
80105aaf:	89 42 08             	mov    %eax,0x8(%edx)
      table[num].priority = p->priority;
80105ab2:	8b 55 f4             	mov    -0xc(%ebp),%edx
80105ab5:	89 d0                	mov    %edx,%eax
80105ab7:	01 c0                	add    %eax,%eax
80105ab9:	01 d0                	add    %edx,%eax
80105abb:	c1 e0 05             	shl    $0x5,%eax
80105abe:	89 c2                	mov    %eax,%edx
80105ac0:	8b 45 0c             	mov    0xc(%ebp),%eax
80105ac3:	01 c2                	add    %eax,%edx
80105ac5:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105ac8:	8b 80 94 00 00 00    	mov    0x94(%eax),%eax
80105ace:	89 42 18             	mov    %eax,0x18(%edx)
      table[num].CPU_total_ticks = p->cpu_ticks_total;
80105ad1:	8b 55 f4             	mov    -0xc(%ebp),%edx
80105ad4:	89 d0                	mov    %edx,%eax
80105ad6:	01 c0                	add    %eax,%eax
80105ad8:	01 d0                	add    %edx,%eax
80105ada:	c1 e0 05             	shl    $0x5,%eax
80105add:	89 c2                	mov    %eax,%edx
80105adf:	8b 45 0c             	mov    0xc(%ebp),%eax
80105ae2:	01 c2                	add    %eax,%edx
80105ae4:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105ae7:	8b 80 88 00 00 00    	mov    0x88(%eax),%eax
80105aed:	89 42 14             	mov    %eax,0x14(%edx)
      table[num].elapsed_ticks = ticks0 - p->start_ticks;
80105af0:	8b 55 f4             	mov    -0xc(%ebp),%edx
80105af3:	89 d0                	mov    %edx,%eax
80105af5:	01 c0                	add    %eax,%eax
80105af7:	01 d0                	add    %edx,%eax
80105af9:	c1 e0 05             	shl    $0x5,%eax
80105afc:	89 c2                	mov    %eax,%edx
80105afe:	8b 45 0c             	mov    0xc(%ebp),%eax
80105b01:	01 c2                	add    %eax,%edx
80105b03:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105b06:	8b 80 84 00 00 00    	mov    0x84(%eax),%eax
80105b0c:	8b 4d ec             	mov    -0x14(%ebp),%ecx
80105b0f:	29 c1                	sub    %eax,%ecx
80105b11:	89 c8                	mov    %ecx,%eax
80105b13:	89 42 10             	mov    %eax,0x10(%edx)
      if (p->parent)
80105b16:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105b19:	8b 40 1c             	mov    0x1c(%eax),%eax
80105b1c:	85 c0                	test   %eax,%eax
80105b1e:	74 21                	je     80105b41 <getProcInfo+0x137>
        table[num].ppid = p->parent->pid;
80105b20:	8b 55 f4             	mov    -0xc(%ebp),%edx
80105b23:	89 d0                	mov    %edx,%eax
80105b25:	01 c0                	add    %eax,%eax
80105b27:	01 d0                	add    %edx,%eax
80105b29:	c1 e0 05             	shl    $0x5,%eax
80105b2c:	89 c2                	mov    %eax,%edx
80105b2e:	8b 45 0c             	mov    0xc(%ebp),%eax
80105b31:	01 c2                	add    %eax,%edx
80105b33:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105b36:	8b 40 1c             	mov    0x1c(%eax),%eax
80105b39:	8b 40 10             	mov    0x10(%eax),%eax
80105b3c:	89 42 0c             	mov    %eax,0xc(%edx)
80105b3f:	eb 1c                	jmp    80105b5d <getProcInfo+0x153>
      else
        table[num].ppid = p->pid;  
80105b41:	8b 55 f4             	mov    -0xc(%ebp),%edx
80105b44:	89 d0                	mov    %edx,%eax
80105b46:	01 c0                	add    %eax,%eax
80105b48:	01 d0                	add    %edx,%eax
80105b4a:	c1 e0 05             	shl    $0x5,%eax
80105b4d:	89 c2                	mov    %eax,%edx
80105b4f:	8b 45 0c             	mov    0xc(%ebp),%eax
80105b52:	01 c2                	add    %eax,%edx
80105b54:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105b57:	8b 40 10             	mov    0x10(%eax),%eax
80105b5a:	89 42 0c             	mov    %eax,0xc(%edx)
      safestrcpy(table[num].state, states[p->state], sizeof(table[num].state));
80105b5d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105b60:	8b 40 0c             	mov    0xc(%eax),%eax
80105b63:	8b 0c 85 08 d0 10 80 	mov    -0x7fef2ff8(,%eax,4),%ecx
80105b6a:	8b 55 f4             	mov    -0xc(%ebp),%edx
80105b6d:	89 d0                	mov    %edx,%eax
80105b6f:	01 c0                	add    %eax,%eax
80105b71:	01 d0                	add    %edx,%eax
80105b73:	c1 e0 05             	shl    $0x5,%eax
80105b76:	89 c2                	mov    %eax,%edx
80105b78:	8b 45 0c             	mov    0xc(%ebp),%eax
80105b7b:	01 d0                	add    %edx,%eax
80105b7d:	83 c0 1c             	add    $0x1c,%eax
80105b80:	83 ec 04             	sub    $0x4,%esp
80105b83:	6a 20                	push   $0x20
80105b85:	51                   	push   %ecx
80105b86:	50                   	push   %eax
80105b87:	e8 0f 13 00 00       	call   80106e9b <safestrcpy>
80105b8c:	83 c4 10             	add    $0x10,%esp
      table[num].size = p->sz;
80105b8f:	8b 55 f4             	mov    -0xc(%ebp),%edx
80105b92:	89 d0                	mov    %edx,%eax
80105b94:	01 c0                	add    %eax,%eax
80105b96:	01 d0                	add    %edx,%eax
80105b98:	c1 e0 05             	shl    $0x5,%eax
80105b9b:	89 c2                	mov    %eax,%edx
80105b9d:	8b 45 0c             	mov    0xc(%ebp),%eax
80105ba0:	01 c2                	add    %eax,%edx
80105ba2:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105ba5:	8b 00                	mov    (%eax),%eax
80105ba7:	89 42 3c             	mov    %eax,0x3c(%edx)
      safestrcpy(table[num].name, p->name, sizeof(table[num].name));
80105baa:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105bad:	8d 48 74             	lea    0x74(%eax),%ecx
80105bb0:	8b 55 f4             	mov    -0xc(%ebp),%edx
80105bb3:	89 d0                	mov    %edx,%eax
80105bb5:	01 c0                	add    %eax,%eax
80105bb7:	01 d0                	add    %edx,%eax
80105bb9:	c1 e0 05             	shl    $0x5,%eax
80105bbc:	89 c2                	mov    %eax,%edx
80105bbe:	8b 45 0c             	mov    0xc(%ebp),%eax
80105bc1:	01 d0                	add    %edx,%eax
80105bc3:	83 c0 40             	add    $0x40,%eax
80105bc6:	83 ec 04             	sub    $0x4,%esp
80105bc9:	6a 20                	push   $0x20
80105bcb:	51                   	push   %ecx
80105bcc:	50                   	push   %eax
80105bcd:	e8 c9 12 00 00       	call   80106e9b <safestrcpy>
80105bd2:	83 c4 10             	add    $0x10,%esp
      num++; //next proc
80105bd5:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80105bd9:	eb 01                	jmp    80105bdc <getProcInfo+0x1d2>
    ticks0 = ticks;   

    acquire(&ptable.lock); //be sure to lock ptable to avoid bad data
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
      if(p->state == UNUSED) //if the proc is unused skip it
        continue;
80105bdb:	90                   	nop
    struct proc *p;

    ticks0 = ticks;   

    acquire(&ptable.lock); //be sure to lock ptable to avoid bad data
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80105bdc:	81 45 f0 9c 00 00 00 	addl   $0x9c,-0x10(%ebp)
80105be3:	81 7d f0 b4 70 11 80 	cmpl   $0x801170b4,-0x10(%ebp)
80105bea:	0f 82 4b fe ff ff    	jb     80105a3b <getProcInfo+0x31>
      safestrcpy(table[num].state, states[p->state], sizeof(table[num].state));
      table[num].size = p->sz;
      safestrcpy(table[num].name, p->name, sizeof(table[num].name));
      num++; //next proc
    }
  release(&ptable.lock); //be sure to share the lock. Sharing is caring
80105bf0:	83 ec 0c             	sub    $0xc,%esp
80105bf3:	68 80 49 11 80       	push   $0x80114980
80105bf8:	e8 9f 0e 00 00       	call   80106a9c <release>
80105bfd:	83 c4 10             	add    $0x10,%esp
  return num; //return the number of procs
80105c00:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
80105c03:	c9                   	leave  
80105c04:	c3                   	ret    

80105c05 <setpriority>:

int																//************** PRIORITY ***************
setpriority(int pid, int priority)
{
80105c05:	55                   	push   %ebp
80105c06:	89 e5                	mov    %esp,%ebp
80105c08:	83 ec 18             	sub    $0x18,%esp
  struct proc * p;

  if(priority > MAX  || priority < 0){
80105c0b:	83 7d 0c 07          	cmpl   $0x7,0xc(%ebp)
80105c0f:	7f 06                	jg     80105c17 <setpriority+0x12>
80105c11:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
80105c15:	79 1a                	jns    80105c31 <setpriority+0x2c>
    cprintf("The new priority is out of bounds!!\n");
80105c17:	83 ec 0c             	sub    $0xc,%esp
80105c1a:	68 a8 a7 10 80       	push   $0x8010a7a8
80105c1f:	e8 a2 a7 ff ff       	call   801003c6 <cprintf>
80105c24:	83 c4 10             	add    $0x10,%esp
    return -1;
80105c27:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105c2c:	e9 06 02 00 00       	jmp    80105e37 <setpriority+0x232>
  }
  if(pid <= 0){
80105c31:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80105c35:	7f 1d                	jg     80105c54 <setpriority+0x4f>
    cprintf("\nThe PID: %d is invalid!! PASS!\n", pid);
80105c37:	83 ec 08             	sub    $0x8,%esp
80105c3a:	ff 75 08             	pushl  0x8(%ebp)
80105c3d:	68 d0 a7 10 80       	push   $0x8010a7d0
80105c42:	e8 7f a7 ff ff       	call   801003c6 <cprintf>
80105c47:	83 c4 10             	add    $0x10,%esp
    return 2;
80105c4a:	b8 02 00 00 00       	mov    $0x2,%eax
80105c4f:	e9 e3 01 00 00       	jmp    80105e37 <setpriority+0x232>
  }

  acquire(&ptable.lock);
80105c54:	83 ec 0c             	sub    $0xc,%esp
80105c57:	68 80 49 11 80       	push   $0x80114980
80105c5c:	e8 d4 0d 00 00       	call   80106a35 <acquire>
80105c61:	83 c4 10             	add    $0x10,%esp
  for(int i = 0; i < MAX; ++i){
80105c64:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
80105c6b:	eb 77                	jmp    80105ce4 <setpriority+0xdf>
    p = ptable.pLists.ready[i];				// Check the ready list
80105c6d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105c70:	05 cc 09 00 00       	add    $0x9cc,%eax
80105c75:	8b 04 85 84 49 11 80 	mov    -0x7feeb67c(,%eax,4),%eax
80105c7c:	89 45 f4             	mov    %eax,-0xc(%ebp)
    while (p) { 
80105c7f:	eb 59                	jmp    80105cda <setpriority+0xd5>
      if((p->pid == pid) /*|| (p->parent->pid == pid)*/){
80105c81:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105c84:	8b 50 10             	mov    0x10(%eax),%edx
80105c87:	8b 45 08             	mov    0x8(%ebp),%eax
80105c8a:	39 c2                	cmp    %eax,%edx
80105c8c:	75 40                	jne    80105cce <setpriority+0xc9>
        p->priority = priority;
80105c8e:	8b 55 0c             	mov    0xc(%ebp),%edx
80105c91:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105c94:	89 90 94 00 00 00    	mov    %edx,0x94(%eax)
        release(&ptable.lock);
80105c9a:	83 ec 0c             	sub    $0xc,%esp
80105c9d:	68 80 49 11 80       	push   $0x80114980
80105ca2:	e8 f5 0d 00 00       	call   80106a9c <release>
80105ca7:	83 c4 10             	add    $0x10,%esp
        cprintf("The new priority is: %d\n", p->priority);
80105caa:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105cad:	8b 80 94 00 00 00    	mov    0x94(%eax),%eax
80105cb3:	83 ec 08             	sub    $0x8,%esp
80105cb6:	50                   	push   %eax
80105cb7:	68 f1 a7 10 80       	push   $0x8010a7f1
80105cbc:	e8 05 a7 ff ff       	call   801003c6 <cprintf>
80105cc1:	83 c4 10             	add    $0x10,%esp
        return 0;
80105cc4:	b8 00 00 00 00       	mov    $0x0,%eax
80105cc9:	e9 69 01 00 00       	jmp    80105e37 <setpriority+0x232>
      }
      p = p->next;
80105cce:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105cd1:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
80105cd7:	89 45 f4             	mov    %eax,-0xc(%ebp)
  }

  acquire(&ptable.lock);
  for(int i = 0; i < MAX; ++i){
    p = ptable.pLists.ready[i];				// Check the ready list
    while (p) { 
80105cda:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80105cde:	75 a1                	jne    80105c81 <setpriority+0x7c>
    cprintf("\nThe PID: %d is invalid!! PASS!\n", pid);
    return 2;
  }

  acquire(&ptable.lock);
  for(int i = 0; i < MAX; ++i){
80105ce0:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
80105ce4:	83 7d f0 06          	cmpl   $0x6,-0x10(%ebp)
80105ce8:	7e 83                	jle    80105c6d <setpriority+0x68>
        return 0;
      }
      p = p->next;
    }
  }
  p = ptable.pLists.sleep;				// Check the sleeping list
80105cea:	a1 d8 70 11 80       	mov    0x801170d8,%eax
80105cef:	89 45 f4             	mov    %eax,-0xc(%ebp)
  while (p) { 
80105cf2:	eb 59                	jmp    80105d4d <setpriority+0x148>
    if((p->pid == pid) /*|| (p->parent->pid == pid)*/){
80105cf4:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105cf7:	8b 50 10             	mov    0x10(%eax),%edx
80105cfa:	8b 45 08             	mov    0x8(%ebp),%eax
80105cfd:	39 c2                	cmp    %eax,%edx
80105cff:	75 40                	jne    80105d41 <setpriority+0x13c>
      p->priority = priority;
80105d01:	8b 55 0c             	mov    0xc(%ebp),%edx
80105d04:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105d07:	89 90 94 00 00 00    	mov    %edx,0x94(%eax)
      release(&ptable.lock);
80105d0d:	83 ec 0c             	sub    $0xc,%esp
80105d10:	68 80 49 11 80       	push   $0x80114980
80105d15:	e8 82 0d 00 00       	call   80106a9c <release>
80105d1a:	83 c4 10             	add    $0x10,%esp
      cprintf("The new priority is: %d\n", p->priority);
80105d1d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105d20:	8b 80 94 00 00 00    	mov    0x94(%eax),%eax
80105d26:	83 ec 08             	sub    $0x8,%esp
80105d29:	50                   	push   %eax
80105d2a:	68 f1 a7 10 80       	push   $0x8010a7f1
80105d2f:	e8 92 a6 ff ff       	call   801003c6 <cprintf>
80105d34:	83 c4 10             	add    $0x10,%esp
      return 0;
80105d37:	b8 00 00 00 00       	mov    $0x0,%eax
80105d3c:	e9 f6 00 00 00       	jmp    80105e37 <setpriority+0x232>
    }
    p = p->next;
80105d41:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105d44:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
80105d4a:	89 45 f4             	mov    %eax,-0xc(%ebp)
      }
      p = p->next;
    }
  }
  p = ptable.pLists.sleep;				// Check the sleeping list
  while (p) { 
80105d4d:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80105d51:	75 a1                	jne    80105cf4 <setpriority+0xef>
      cprintf("The new priority is: %d\n", p->priority);
      return 0;
    }
    p = p->next;
  }
  p = ptable.pLists.running;				// Check the running list
80105d53:	a1 e0 70 11 80       	mov    0x801170e0,%eax
80105d58:	89 45 f4             	mov    %eax,-0xc(%ebp)
  while (p) { 
80105d5b:	eb 59                	jmp    80105db6 <setpriority+0x1b1>
    if((p->pid == pid) /*|| (p->parent->pid == pid)*/){
80105d5d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105d60:	8b 50 10             	mov    0x10(%eax),%edx
80105d63:	8b 45 08             	mov    0x8(%ebp),%eax
80105d66:	39 c2                	cmp    %eax,%edx
80105d68:	75 40                	jne    80105daa <setpriority+0x1a5>
      p->priority = priority;
80105d6a:	8b 55 0c             	mov    0xc(%ebp),%edx
80105d6d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105d70:	89 90 94 00 00 00    	mov    %edx,0x94(%eax)
      release(&ptable.lock);
80105d76:	83 ec 0c             	sub    $0xc,%esp
80105d79:	68 80 49 11 80       	push   $0x80114980
80105d7e:	e8 19 0d 00 00       	call   80106a9c <release>
80105d83:	83 c4 10             	add    $0x10,%esp
      cprintf("The new priority is: %d\n", p->priority);
80105d86:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105d89:	8b 80 94 00 00 00    	mov    0x94(%eax),%eax
80105d8f:	83 ec 08             	sub    $0x8,%esp
80105d92:	50                   	push   %eax
80105d93:	68 f1 a7 10 80       	push   $0x8010a7f1
80105d98:	e8 29 a6 ff ff       	call   801003c6 <cprintf>
80105d9d:	83 c4 10             	add    $0x10,%esp
      return 0;
80105da0:	b8 00 00 00 00       	mov    $0x0,%eax
80105da5:	e9 8d 00 00 00       	jmp    80105e37 <setpriority+0x232>
    }
    p = p->next;
80105daa:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105dad:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
80105db3:	89 45 f4             	mov    %eax,-0xc(%ebp)
      return 0;
    }
    p = p->next;
  }
  p = ptable.pLists.running;				// Check the running list
  while (p) { 
80105db6:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80105dba:	75 a1                	jne    80105d5d <setpriority+0x158>
      cprintf("The new priority is: %d\n", p->priority);
      return 0;
    }
    p = p->next;
  }
  p = ptable.pLists.zombie;				// Check the zombie list
80105dbc:	a1 dc 70 11 80       	mov    0x801170dc,%eax
80105dc1:	89 45 f4             	mov    %eax,-0xc(%ebp)
  while (p) { 
80105dc4:	eb 56                	jmp    80105e1c <setpriority+0x217>
    if((p->pid == pid) /*|| (p->parent->pid == pid)*/){
80105dc6:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105dc9:	8b 50 10             	mov    0x10(%eax),%edx
80105dcc:	8b 45 08             	mov    0x8(%ebp),%eax
80105dcf:	39 c2                	cmp    %eax,%edx
80105dd1:	75 3d                	jne    80105e10 <setpriority+0x20b>
      p->priority = priority;
80105dd3:	8b 55 0c             	mov    0xc(%ebp),%edx
80105dd6:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105dd9:	89 90 94 00 00 00    	mov    %edx,0x94(%eax)
      release(&ptable.lock);
80105ddf:	83 ec 0c             	sub    $0xc,%esp
80105de2:	68 80 49 11 80       	push   $0x80114980
80105de7:	e8 b0 0c 00 00       	call   80106a9c <release>
80105dec:	83 c4 10             	add    $0x10,%esp
      cprintf("The new priority is: %d\n", p->priority);
80105def:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105df2:	8b 80 94 00 00 00    	mov    0x94(%eax),%eax
80105df8:	83 ec 08             	sub    $0x8,%esp
80105dfb:	50                   	push   %eax
80105dfc:	68 f1 a7 10 80       	push   $0x8010a7f1
80105e01:	e8 c0 a5 ff ff       	call   801003c6 <cprintf>
80105e06:	83 c4 10             	add    $0x10,%esp
      return 0;
80105e09:	b8 00 00 00 00       	mov    $0x0,%eax
80105e0e:	eb 27                	jmp    80105e37 <setpriority+0x232>
    }
    p = p->next;
80105e10:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105e13:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
80105e19:	89 45 f4             	mov    %eax,-0xc(%ebp)
      return 0;
    }
    p = p->next;
  }
  p = ptable.pLists.zombie;				// Check the zombie list
  while (p) { 
80105e1c:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80105e20:	75 a4                	jne    80105dc6 <setpriority+0x1c1>
      cprintf("The new priority is: %d\n", p->priority);
      return 0;
    }
    p = p->next;
  }
  release(&ptable.lock);
80105e22:	83 ec 0c             	sub    $0xc,%esp
80105e25:	68 80 49 11 80       	push   $0x80114980
80105e2a:	e8 6d 0c 00 00       	call   80106a9c <release>
80105e2f:	83 c4 10             	add    $0x10,%esp
  return -1;
80105e32:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80105e37:	c9                   	leave  
80105e38:	c3                   	ret    

80105e39 <addToFreeList>:

#ifdef CS333_P3P4
// ptable lock is required to call this function!!!
static void 
addToFreeList(struct proc *p)
{
80105e39:	55                   	push   %ebp
80105e3a:	89 e5                	mov    %esp,%ebp
80105e3c:	83 ec 08             	sub    $0x8,%esp
  if (!holding(&ptable.lock)) panic("Where is my lock?! Lock needed in addToFreeList\n");
80105e3f:	83 ec 0c             	sub    $0xc,%esp
80105e42:	68 80 49 11 80       	push   $0x80114980
80105e47:	e8 1c 0d 00 00       	call   80106b68 <holding>
80105e4c:	83 c4 10             	add    $0x10,%esp
80105e4f:	85 c0                	test   %eax,%eax
80105e51:	75 0d                	jne    80105e60 <addToFreeList+0x27>
80105e53:	83 ec 0c             	sub    $0xc,%esp
80105e56:	68 0c a8 10 80       	push   $0x8010a80c
80105e5b:	e8 06 a7 ff ff       	call   80100566 <panic>
  if (ptable.pLists.free != 0)
80105e60:	a1 d4 70 11 80       	mov    0x801170d4,%eax
80105e65:	85 c0                	test   %eax,%eax
80105e67:	74 19                	je     80105e82 <addToFreeList+0x49>
  {
    p->next = ptable.pLists.free;
80105e69:	8b 15 d4 70 11 80    	mov    0x801170d4,%edx
80105e6f:	8b 45 08             	mov    0x8(%ebp),%eax
80105e72:	89 90 90 00 00 00    	mov    %edx,0x90(%eax)
    ptable.pLists.free = p;
80105e78:	8b 45 08             	mov    0x8(%ebp),%eax
80105e7b:	a3 d4 70 11 80       	mov    %eax,0x801170d4
  else
  {
    ptable.pLists.free = p;
    p->next = 0;
  }
  return;
80105e80:	eb 16                	jmp    80105e98 <addToFreeList+0x5f>
    p->next = ptable.pLists.free;
    ptable.pLists.free = p;
  }
  else
  {
    ptable.pLists.free = p;
80105e82:	8b 45 08             	mov    0x8(%ebp),%eax
80105e85:	a3 d4 70 11 80       	mov    %eax,0x801170d4
    p->next = 0;
80105e8a:	8b 45 08             	mov    0x8(%ebp),%eax
80105e8d:	c7 80 90 00 00 00 00 	movl   $0x0,0x90(%eax)
80105e94:	00 00 00 
  }
  return;
80105e97:	90                   	nop
}
80105e98:	c9                   	leave  
80105e99:	c3                   	ret    

80105e9a <initFreeList>:

// don't call initFreeList without the ptable lock!!
static void 
initFreeList(void)
{
80105e9a:	55                   	push   %ebp
80105e9b:	89 e5                	mov    %esp,%ebp
80105e9d:	83 ec 18             	sub    $0x18,%esp
  int i;
  if(!holding(&ptable.lock)) panic("Holding ptable.lock in initFreeList where I shouldn't be\n");
80105ea0:	83 ec 0c             	sub    $0xc,%esp
80105ea3:	68 80 49 11 80       	push   $0x80114980
80105ea8:	e8 bb 0c 00 00       	call   80106b68 <holding>
80105ead:	83 c4 10             	add    $0x10,%esp
80105eb0:	85 c0                	test   %eax,%eax
80105eb2:	75 0d                	jne    80105ec1 <initFreeList+0x27>
80105eb4:	83 ec 0c             	sub    $0xc,%esp
80105eb7:	68 40 a8 10 80       	push   $0x8010a840
80105ebc:	e8 a5 a6 ff ff       	call   80100566 <panic>
  for (i = NPROC-1; i>=0; --i)
80105ec1:	c7 45 f4 3f 00 00 00 	movl   $0x3f,-0xc(%ebp)
80105ec8:	eb 24                	jmp    80105eee <initFreeList+0x54>
    addToFreeList(&ptable.proc[i]);
80105eca:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105ecd:	69 c0 9c 00 00 00    	imul   $0x9c,%eax,%eax
80105ed3:	83 c0 30             	add    $0x30,%eax
80105ed6:	05 80 49 11 80       	add    $0x80114980,%eax
80105edb:	83 c0 04             	add    $0x4,%eax
80105ede:	83 ec 0c             	sub    $0xc,%esp
80105ee1:	50                   	push   %eax
80105ee2:	e8 52 ff ff ff       	call   80105e39 <addToFreeList>
80105ee7:	83 c4 10             	add    $0x10,%esp
static void 
initFreeList(void)
{
  int i;
  if(!holding(&ptable.lock)) panic("Holding ptable.lock in initFreeList where I shouldn't be\n");
  for (i = NPROC-1; i>=0; --i)
80105eea:	83 6d f4 01          	subl   $0x1,-0xc(%ebp)
80105eee:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80105ef2:	79 d6                	jns    80105eca <initFreeList+0x30>
    addToFreeList(&ptable.proc[i]);
  return;
80105ef4:	90                   	nop
}
80105ef5:	c9                   	leave  
80105ef6:	c3                   	ret    

80105ef7 <recursiveCountFreeList>:

int 
recursiveCountFreeList(struct proc *p)
{
80105ef7:	55                   	push   %ebp
80105ef8:	89 e5                	mov    %esp,%ebp
80105efa:	83 ec 08             	sub    $0x8,%esp
  if (!holding(&ptable.lock)) panic("Where is my lock?! Lock needed in recursiveCountFreeList\n");
80105efd:	83 ec 0c             	sub    $0xc,%esp
80105f00:	68 80 49 11 80       	push   $0x80114980
80105f05:	e8 5e 0c 00 00       	call   80106b68 <holding>
80105f0a:	83 c4 10             	add    $0x10,%esp
80105f0d:	85 c0                	test   %eax,%eax
80105f0f:	75 0d                	jne    80105f1e <recursiveCountFreeList+0x27>
80105f11:	83 ec 0c             	sub    $0xc,%esp
80105f14:	68 7c a8 10 80       	push   $0x8010a87c
80105f19:	e8 48 a6 ff ff       	call   80100566 <panic>
  if (p == 0) return 0;
80105f1e:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80105f22:	75 07                	jne    80105f2b <recursiveCountFreeList+0x34>
80105f24:	b8 00 00 00 00       	mov    $0x0,%eax
80105f29:	eb 18                	jmp    80105f43 <recursiveCountFreeList+0x4c>
  return recursiveCountFreeList(p-> next) + 1;
80105f2b:	8b 45 08             	mov    0x8(%ebp),%eax
80105f2e:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
80105f34:	83 ec 0c             	sub    $0xc,%esp
80105f37:	50                   	push   %eax
80105f38:	e8 ba ff ff ff       	call   80105ef7 <recursiveCountFreeList>
80105f3d:	83 c4 10             	add    $0x10,%esp
80105f40:	83 c0 01             	add    $0x1,%eax
}
80105f43:	c9                   	leave  
80105f44:	c3                   	ret    

80105f45 <countFreeList>:

void 
countFreeList(void)
{
80105f45:	55                   	push   %ebp
80105f46:	89 e5                	mov    %esp,%ebp
80105f48:	83 ec 18             	sub    $0x18,%esp
  if(holding(&ptable.lock)) panic("Holding ptable.lock in countFreeList where I shouldn't be\n");
80105f4b:	83 ec 0c             	sub    $0xc,%esp
80105f4e:	68 80 49 11 80       	push   $0x80114980
80105f53:	e8 10 0c 00 00       	call   80106b68 <holding>
80105f58:	83 c4 10             	add    $0x10,%esp
80105f5b:	85 c0                	test   %eax,%eax
80105f5d:	74 0d                	je     80105f6c <countFreeList+0x27>
80105f5f:	83 ec 0c             	sub    $0xc,%esp
80105f62:	68 b8 a8 10 80       	push   $0x8010a8b8
80105f67:	e8 fa a5 ff ff       	call   80100566 <panic>
  int size = 0;
80105f6c:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  acquire(&ptable.lock);
80105f73:	83 ec 0c             	sub    $0xc,%esp
80105f76:	68 80 49 11 80       	push   $0x80114980
80105f7b:	e8 b5 0a 00 00       	call   80106a35 <acquire>
80105f80:	83 c4 10             	add    $0x10,%esp
  struct proc *p = ptable.pLists.free;
80105f83:	a1 d4 70 11 80       	mov    0x801170d4,%eax
80105f88:	89 45 f0             	mov    %eax,-0x10(%ebp)
  size = recursiveCountFreeList(p);
80105f8b:	83 ec 0c             	sub    $0xc,%esp
80105f8e:	ff 75 f0             	pushl  -0x10(%ebp)
80105f91:	e8 61 ff ff ff       	call   80105ef7 <recursiveCountFreeList>
80105f96:	83 c4 10             	add    $0x10,%esp
80105f99:	89 45 f4             	mov    %eax,-0xc(%ebp)
  release(&ptable.lock);
80105f9c:	83 ec 0c             	sub    $0xc,%esp
80105f9f:	68 80 49 11 80       	push   $0x80114980
80105fa4:	e8 f3 0a 00 00       	call   80106a9c <release>
80105fa9:	83 c4 10             	add    $0x10,%esp
  cprintf("Free List Size: %d processes\n", size);
80105fac:	83 ec 08             	sub    $0x8,%esp
80105faf:	ff 75 f4             	pushl  -0xc(%ebp)
80105fb2:	68 f3 a8 10 80       	push   $0x8010a8f3
80105fb7:	e8 0a a4 ff ff       	call   801003c6 <cprintf>
80105fbc:	83 c4 10             	add    $0x10,%esp
  return;
80105fbf:	90                   	nop
}
80105fc0:	c9                   	leave  
80105fc1:	c3                   	ret    

80105fc2 <addToReadyList>:

// ptable lock is required to call this function!!!
static void 
addToReadyList(struct proc *p, int pty)
{
80105fc2:	55                   	push   %ebp
80105fc3:	89 e5                	mov    %esp,%ebp
80105fc5:	83 ec 18             	sub    $0x18,%esp
  struct proc* temp;
  if (!holding(&ptable.lock)) panic("Where is my lock?! lock is needed in addToReadyList\n");
80105fc8:	83 ec 0c             	sub    $0xc,%esp
80105fcb:	68 80 49 11 80       	push   $0x80114980
80105fd0:	e8 93 0b 00 00       	call   80106b68 <holding>
80105fd5:	83 c4 10             	add    $0x10,%esp
80105fd8:	85 c0                	test   %eax,%eax
80105fda:	75 0d                	jne    80105fe9 <addToReadyList+0x27>
80105fdc:	83 ec 0c             	sub    $0xc,%esp
80105fdf:	68 14 a9 10 80       	push   $0x8010a914
80105fe4:	e8 7d a5 ff ff       	call   80100566 <panic>
  
  temp = ptable.pLists.ready[pty];
80105fe9:	8b 45 0c             	mov    0xc(%ebp),%eax
80105fec:	05 cc 09 00 00       	add    $0x9cc,%eax
80105ff1:	8b 04 85 84 49 11 80 	mov    -0x7feeb67c(,%eax,4),%eax
80105ff8:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if (temp == 0) ptable.pLists.ready[pty] = p;
80105ffb:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80105fff:	75 21                	jne    80106022 <addToReadyList+0x60>
80106001:	8b 45 0c             	mov    0xc(%ebp),%eax
80106004:	8d 90 cc 09 00 00    	lea    0x9cc(%eax),%edx
8010600a:	8b 45 08             	mov    0x8(%ebp),%eax
8010600d:	89 04 95 84 49 11 80 	mov    %eax,-0x7feeb67c(,%edx,4)
80106014:	eb 25                	jmp    8010603b <addToReadyList+0x79>
  else 
  {
    while (temp->next != 0) temp = temp->next;
80106016:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106019:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
8010601f:	89 45 f4             	mov    %eax,-0xc(%ebp)
80106022:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106025:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
8010602b:	85 c0                	test   %eax,%eax
8010602d:	75 e7                	jne    80106016 <addToReadyList+0x54>
    temp->next = p;
8010602f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106032:	8b 55 08             	mov    0x8(%ebp),%edx
80106035:	89 90 90 00 00 00    	mov    %edx,0x90(%eax)
  }
  p->next = 0;
8010603b:	8b 45 08             	mov    0x8(%ebp),%eax
8010603e:	c7 80 90 00 00 00 00 	movl   $0x0,0x90(%eax)
80106045:	00 00 00 
  return;
80106048:	90                   	nop
}
80106049:	c9                   	leave  
8010604a:	c3                   	ret    

8010604b <countReadyList>:

void 
countReadyList(void)
{
8010604b:	55                   	push   %ebp
8010604c:	89 e5                	mov    %esp,%ebp
8010604e:	83 ec 18             	sub    $0x18,%esp
  if(holding(&ptable.lock)) panic("Holding ptable.lock in countReadyList where I shouldn't be\n");
80106051:	83 ec 0c             	sub    $0xc,%esp
80106054:	68 80 49 11 80       	push   $0x80114980
80106059:	e8 0a 0b 00 00       	call   80106b68 <holding>
8010605e:	83 c4 10             	add    $0x10,%esp
80106061:	85 c0                	test   %eax,%eax
80106063:	74 0d                	je     80106072 <countReadyList+0x27>
80106065:	83 ec 0c             	sub    $0xc,%esp
80106068:	68 4c a9 10 80       	push   $0x8010a94c
8010606d:	e8 f4 a4 ff ff       	call   80100566 <panic>
  acquire(&ptable.lock);
80106072:	83 ec 0c             	sub    $0xc,%esp
80106075:	68 80 49 11 80       	push   $0x80114980
8010607a:	e8 b6 09 00 00       	call   80106a35 <acquire>
8010607f:	83 c4 10             	add    $0x10,%esp
  for(int i = 0; i < MAX; ++i){
80106082:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80106089:	e9 b1 00 00 00       	jmp    8010613f <countReadyList+0xf4>
    struct proc *p = ptable.pLists.ready[i];
8010608e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106091:	05 cc 09 00 00       	add    $0x9cc,%eax
80106096:	8b 04 85 84 49 11 80 	mov    -0x7feeb67c(,%eax,4),%eax
8010609d:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if (p == 0) {
801060a0:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
801060a4:	75 25                	jne    801060cb <countReadyList+0x80>
      cprintf("The Ready List is empty\n");
801060a6:	83 ec 0c             	sub    $0xc,%esp
801060a9:	68 88 a9 10 80       	push   $0x8010a988
801060ae:	e8 13 a3 ff ff       	call   801003c6 <cprintf>
801060b3:	83 c4 10             	add    $0x10,%esp
      release(&ptable.lock);
801060b6:	83 ec 0c             	sub    $0xc,%esp
801060b9:	68 80 49 11 80       	push   $0x80114980
801060be:	e8 d9 09 00 00       	call   80106a9c <release>
801060c3:	83 c4 10             	add    $0x10,%esp
      return;
801060c6:	e9 8f 00 00 00       	jmp    8010615a <countReadyList+0x10f>
    }
    cprintf("Ready List Processes:\n%d: ", i);
801060cb:	83 ec 08             	sub    $0x8,%esp
801060ce:	ff 75 f4             	pushl  -0xc(%ebp)
801060d1:	68 a1 a9 10 80       	push   $0x8010a9a1
801060d6:	e8 eb a2 ff ff       	call   801003c6 <cprintf>
801060db:	83 c4 10             	add    $0x10,%esp
    while (p->next) 
801060de:	eb 2d                	jmp    8010610d <countReadyList+0xc2>
    {
      cprintf("(%d, %d) -> ", p->pid, p->budget);
801060e0:	8b 45 f0             	mov    -0x10(%ebp),%eax
801060e3:	8b 90 98 00 00 00    	mov    0x98(%eax),%edx
801060e9:	8b 45 f0             	mov    -0x10(%ebp),%eax
801060ec:	8b 40 10             	mov    0x10(%eax),%eax
801060ef:	83 ec 04             	sub    $0x4,%esp
801060f2:	52                   	push   %edx
801060f3:	50                   	push   %eax
801060f4:	68 bc a9 10 80       	push   $0x8010a9bc
801060f9:	e8 c8 a2 ff ff       	call   801003c6 <cprintf>
801060fe:	83 c4 10             	add    $0x10,%esp
      p = p->next;
80106101:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106104:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
8010610a:	89 45 f0             	mov    %eax,-0x10(%ebp)
      cprintf("The Ready List is empty\n");
      release(&ptable.lock);
      return;
    }
    cprintf("Ready List Processes:\n%d: ", i);
    while (p->next) 
8010610d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106110:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
80106116:	85 c0                	test   %eax,%eax
80106118:	75 c6                	jne    801060e0 <countReadyList+0x95>
    {
      cprintf("(%d, %d) -> ", p->pid, p->budget);
      p = p->next;
    }
    cprintf("(%d, %d)\n", p->pid, p->budget);
8010611a:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010611d:	8b 90 98 00 00 00    	mov    0x98(%eax),%edx
80106123:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106126:	8b 40 10             	mov    0x10(%eax),%eax
80106129:	83 ec 04             	sub    $0x4,%esp
8010612c:	52                   	push   %edx
8010612d:	50                   	push   %eax
8010612e:	68 c9 a9 10 80       	push   $0x8010a9c9
80106133:	e8 8e a2 ff ff       	call   801003c6 <cprintf>
80106138:	83 c4 10             	add    $0x10,%esp
void 
countReadyList(void)
{
  if(holding(&ptable.lock)) panic("Holding ptable.lock in countReadyList where I shouldn't be\n");
  acquire(&ptable.lock);
  for(int i = 0; i < MAX; ++i){
8010613b:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
8010613f:	83 7d f4 06          	cmpl   $0x6,-0xc(%ebp)
80106143:	0f 8e 45 ff ff ff    	jle    8010608e <countReadyList+0x43>
      cprintf("(%d, %d) -> ", p->pid, p->budget);
      p = p->next;
    }
    cprintf("(%d, %d)\n", p->pid, p->budget);
  }
  release(&ptable.lock);
80106149:	83 ec 0c             	sub    $0xc,%esp
8010614c:	68 80 49 11 80       	push   $0x80114980
80106151:	e8 46 09 00 00       	call   80106a9c <release>
80106156:	83 c4 10             	add    $0x10,%esp
  return;
80106159:	90                   	nop
}
8010615a:	c9                   	leave  
8010615b:	c3                   	ret    

8010615c <countSleepList>:

void 
countSleepList(void)
{
8010615c:	55                   	push   %ebp
8010615d:	89 e5                	mov    %esp,%ebp
8010615f:	83 ec 18             	sub    $0x18,%esp
  if(holding(&ptable.lock)) panic("Holding ptable.lock in countSleepList where I shouldn't be\n");
80106162:	83 ec 0c             	sub    $0xc,%esp
80106165:	68 80 49 11 80       	push   $0x80114980
8010616a:	e8 f9 09 00 00       	call   80106b68 <holding>
8010616f:	83 c4 10             	add    $0x10,%esp
80106172:	85 c0                	test   %eax,%eax
80106174:	74 0d                	je     80106183 <countSleepList+0x27>
80106176:	83 ec 0c             	sub    $0xc,%esp
80106179:	68 d4 a9 10 80       	push   $0x8010a9d4
8010617e:	e8 e3 a3 ff ff       	call   80100566 <panic>
  acquire(&ptable.lock);
80106183:	83 ec 0c             	sub    $0xc,%esp
80106186:	68 80 49 11 80       	push   $0x80114980
8010618b:	e8 a5 08 00 00       	call   80106a35 <acquire>
80106190:	83 c4 10             	add    $0x10,%esp
  if (ptable.pLists.sleep == 0) {
80106193:	a1 d8 70 11 80       	mov    0x801170d8,%eax
80106198:	85 c0                	test   %eax,%eax
8010619a:	75 22                	jne    801061be <countSleepList+0x62>
    cprintf("The Sleep List is empty\n");
8010619c:	83 ec 0c             	sub    $0xc,%esp
8010619f:	68 10 aa 10 80       	push   $0x8010aa10
801061a4:	e8 1d a2 ff ff       	call   801003c6 <cprintf>
801061a9:	83 c4 10             	add    $0x10,%esp
    release(&ptable.lock);
801061ac:	83 ec 0c             	sub    $0xc,%esp
801061af:	68 80 49 11 80       	push   $0x80114980
801061b4:	e8 e3 08 00 00       	call   80106a9c <release>
801061b9:	83 c4 10             	add    $0x10,%esp
    return;
801061bc:	eb 72                	jmp    80106230 <countSleepList+0xd4>
  }
  struct proc *p = ptable.pLists.sleep;
801061be:	a1 d8 70 11 80       	mov    0x801170d8,%eax
801061c3:	89 45 f4             	mov    %eax,-0xc(%ebp)
  cprintf("Sleep List Processes: \n");
801061c6:	83 ec 0c             	sub    $0xc,%esp
801061c9:	68 29 aa 10 80       	push   $0x8010aa29
801061ce:	e8 f3 a1 ff ff       	call   801003c6 <cprintf>
801061d3:	83 c4 10             	add    $0x10,%esp
  while (p->next) 
801061d6:	eb 23                	jmp    801061fb <countSleepList+0x9f>
  {
    cprintf("%d -> ", p->pid);
801061d8:	8b 45 f4             	mov    -0xc(%ebp),%eax
801061db:	8b 40 10             	mov    0x10(%eax),%eax
801061de:	83 ec 08             	sub    $0x8,%esp
801061e1:	50                   	push   %eax
801061e2:	68 41 aa 10 80       	push   $0x8010aa41
801061e7:	e8 da a1 ff ff       	call   801003c6 <cprintf>
801061ec:	83 c4 10             	add    $0x10,%esp
    p = p->next;
801061ef:	8b 45 f4             	mov    -0xc(%ebp),%eax
801061f2:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
801061f8:	89 45 f4             	mov    %eax,-0xc(%ebp)
    release(&ptable.lock);
    return;
  }
  struct proc *p = ptable.pLists.sleep;
  cprintf("Sleep List Processes: \n");
  while (p->next) 
801061fb:	8b 45 f4             	mov    -0xc(%ebp),%eax
801061fe:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
80106204:	85 c0                	test   %eax,%eax
80106206:	75 d0                	jne    801061d8 <countSleepList+0x7c>
  {
    cprintf("%d -> ", p->pid);
    p = p->next;
  }
  cprintf("%d\n", p->pid);
80106208:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010620b:	8b 40 10             	mov    0x10(%eax),%eax
8010620e:	83 ec 08             	sub    $0x8,%esp
80106211:	50                   	push   %eax
80106212:	68 48 aa 10 80       	push   $0x8010aa48
80106217:	e8 aa a1 ff ff       	call   801003c6 <cprintf>
8010621c:	83 c4 10             	add    $0x10,%esp
  release(&ptable.lock);
8010621f:	83 ec 0c             	sub    $0xc,%esp
80106222:	68 80 49 11 80       	push   $0x80114980
80106227:	e8 70 08 00 00       	call   80106a9c <release>
8010622c:	83 c4 10             	add    $0x10,%esp
  return;
8010622f:	90                   	nop
}
80106230:	c9                   	leave  
80106231:	c3                   	ret    

80106232 <addToSleepList>:

static int 
addToSleepList(struct proc *p)
{
80106232:	55                   	push   %ebp
80106233:	89 e5                	mov    %esp,%ebp
80106235:	83 ec 08             	sub    $0x8,%esp
  if (!holding(&ptable.lock)) panic("Where is my lock?! lock is needed in addToSleepList\n");
80106238:	83 ec 0c             	sub    $0xc,%esp
8010623b:	68 80 49 11 80       	push   $0x80114980
80106240:	e8 23 09 00 00       	call   80106b68 <holding>
80106245:	83 c4 10             	add    $0x10,%esp
80106248:	85 c0                	test   %eax,%eax
8010624a:	75 0d                	jne    80106259 <addToSleepList+0x27>
8010624c:	83 ec 0c             	sub    $0xc,%esp
8010624f:	68 4c aa 10 80       	push   $0x8010aa4c
80106254:	e8 0d a3 ff ff       	call   80100566 <panic>
  
  if (ptable.pLists.sleep == 0)
80106259:	a1 d8 70 11 80       	mov    0x801170d8,%eax
8010625e:	85 c0                	test   %eax,%eax
80106260:	75 1c                	jne    8010627e <addToSleepList+0x4c>
  {
    ptable.pLists.sleep = p;
80106262:	8b 45 08             	mov    0x8(%ebp),%eax
80106265:	a3 d8 70 11 80       	mov    %eax,0x801170d8
    p->next = 0;
8010626a:	8b 45 08             	mov    0x8(%ebp),%eax
8010626d:	c7 80 90 00 00 00 00 	movl   $0x0,0x90(%eax)
80106274:	00 00 00 
    return 0;
80106277:	b8 00 00 00 00       	mov    $0x0,%eax
8010627c:	eb 2c                	jmp    801062aa <addToSleepList+0x78>
  }
  if (ptable.pLists.sleep != 0)
8010627e:	a1 d8 70 11 80       	mov    0x801170d8,%eax
80106283:	85 c0                	test   %eax,%eax
80106285:	74 1e                	je     801062a5 <addToSleepList+0x73>
  {
    p->next = ptable.pLists.sleep;
80106287:	8b 15 d8 70 11 80    	mov    0x801170d8,%edx
8010628d:	8b 45 08             	mov    0x8(%ebp),%eax
80106290:	89 90 90 00 00 00    	mov    %edx,0x90(%eax)
    ptable.pLists.sleep = p;
80106296:	8b 45 08             	mov    0x8(%ebp),%eax
80106299:	a3 d8 70 11 80       	mov    %eax,0x801170d8
    return 0;
8010629e:	b8 00 00 00 00       	mov    $0x0,%eax
801062a3:	eb 05                	jmp    801062aa <addToSleepList+0x78>
  }
  return -1;
801062a5:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
801062aa:	c9                   	leave  
801062ab:	c3                   	ret    

801062ac <removeFromSleepList>:

static int 
removeFromSleepList(struct proc *p)
{
801062ac:	55                   	push   %ebp
801062ad:	89 e5                	mov    %esp,%ebp
801062af:	83 ec 18             	sub    $0x18,%esp
  if (!holding(&ptable.lock)) panic("Where is my lock?! lock is needed in removeFromSleepList\n");
801062b2:	83 ec 0c             	sub    $0xc,%esp
801062b5:	68 80 49 11 80       	push   $0x80114980
801062ba:	e8 a9 08 00 00       	call   80106b68 <holding>
801062bf:	83 c4 10             	add    $0x10,%esp
801062c2:	85 c0                	test   %eax,%eax
801062c4:	75 0d                	jne    801062d3 <removeFromSleepList+0x27>
801062c6:	83 ec 0c             	sub    $0xc,%esp
801062c9:	68 84 aa 10 80       	push   $0x8010aa84
801062ce:	e8 93 a2 ff ff       	call   80100566 <panic>
  if (ptable.pLists.sleep->pid == p->pid)
801062d3:	a1 d8 70 11 80       	mov    0x801170d8,%eax
801062d8:	8b 50 10             	mov    0x10(%eax),%edx
801062db:	8b 45 08             	mov    0x8(%ebp),%eax
801062de:	8b 40 10             	mov    0x10(%eax),%eax
801062e1:	39 c2                	cmp    %eax,%edx
801062e3:	75 24                	jne    80106309 <removeFromSleepList+0x5d>
  {
    ptable.pLists.sleep = ptable.pLists.sleep->next;
801062e5:	a1 d8 70 11 80       	mov    0x801170d8,%eax
801062ea:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
801062f0:	a3 d8 70 11 80       	mov    %eax,0x801170d8
    p->next = 0;
801062f5:	8b 45 08             	mov    0x8(%ebp),%eax
801062f8:	c7 80 90 00 00 00 00 	movl   $0x0,0x90(%eax)
801062ff:	00 00 00 
    return 0;
80106302:	b8 00 00 00 00       	mov    $0x0,%eax
80106307:	eb 6b                	jmp    80106374 <removeFromSleepList+0xc8>
  }
  struct proc *current = ptable.pLists.sleep->next;
80106309:	a1 d8 70 11 80       	mov    0x801170d8,%eax
8010630e:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
80106314:	89 45 f4             	mov    %eax,-0xc(%ebp)
  struct proc *prev = ptable.pLists.sleep;
80106317:	a1 d8 70 11 80       	mov    0x801170d8,%eax
8010631c:	89 45 f0             	mov    %eax,-0x10(%ebp)
  while (current)
8010631f:	eb 48                	jmp    80106369 <removeFromSleepList+0xbd>
  {
    if(current->pid == p->pid)
80106321:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106324:	8b 50 10             	mov    0x10(%eax),%edx
80106327:	8b 45 08             	mov    0x8(%ebp),%eax
8010632a:	8b 40 10             	mov    0x10(%eax),%eax
8010632d:	39 c2                	cmp    %eax,%edx
8010632f:	75 26                	jne    80106357 <removeFromSleepList+0xab>
    {
      prev->next = current->next;
80106331:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106334:	8b 90 90 00 00 00    	mov    0x90(%eax),%edx
8010633a:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010633d:	89 90 90 00 00 00    	mov    %edx,0x90(%eax)
      p->next = 0;
80106343:	8b 45 08             	mov    0x8(%ebp),%eax
80106346:	c7 80 90 00 00 00 00 	movl   $0x0,0x90(%eax)
8010634d:	00 00 00 
      return 0;
80106350:	b8 00 00 00 00       	mov    $0x0,%eax
80106355:	eb 1d                	jmp    80106374 <removeFromSleepList+0xc8>
    }
    prev = current;
80106357:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010635a:	89 45 f0             	mov    %eax,-0x10(%ebp)
    current = current->next;
8010635d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106360:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
80106366:	89 45 f4             	mov    %eax,-0xc(%ebp)
    p->next = 0;
    return 0;
  }
  struct proc *current = ptable.pLists.sleep->next;
  struct proc *prev = ptable.pLists.sleep;
  while (current)
80106369:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
8010636d:	75 b2                	jne    80106321 <removeFromSleepList+0x75>
      return 0;
    }
    prev = current;
    current = current->next;
  }
  return -1;
8010636f:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80106374:	c9                   	leave  
80106375:	c3                   	ret    

80106376 <addToZombieList>:

static void 
addToZombieList(struct proc *p)
{
80106376:	55                   	push   %ebp
80106377:	89 e5                	mov    %esp,%ebp
80106379:	83 ec 08             	sub    $0x8,%esp
  if (!holding(&ptable.lock)) panic("Where is my lock?! lock is needed in addToZombieList\n");
8010637c:	83 ec 0c             	sub    $0xc,%esp
8010637f:	68 80 49 11 80       	push   $0x80114980
80106384:	e8 df 07 00 00       	call   80106b68 <holding>
80106389:	83 c4 10             	add    $0x10,%esp
8010638c:	85 c0                	test   %eax,%eax
8010638e:	75 0d                	jne    8010639d <addToZombieList+0x27>
80106390:	83 ec 0c             	sub    $0xc,%esp
80106393:	68 c0 aa 10 80       	push   $0x8010aac0
80106398:	e8 c9 a1 ff ff       	call   80100566 <panic>

  if (ptable.pLists.zombie != 0)
8010639d:	a1 dc 70 11 80       	mov    0x801170dc,%eax
801063a2:	85 c0                	test   %eax,%eax
801063a4:	74 19                	je     801063bf <addToZombieList+0x49>
  {  
    p->next = ptable.pLists.zombie;
801063a6:	8b 15 dc 70 11 80    	mov    0x801170dc,%edx
801063ac:	8b 45 08             	mov    0x8(%ebp),%eax
801063af:	89 90 90 00 00 00    	mov    %edx,0x90(%eax)
    ptable.pLists.zombie = p;
801063b5:	8b 45 08             	mov    0x8(%ebp),%eax
801063b8:	a3 dc 70 11 80       	mov    %eax,0x801170dc
    return;
801063bd:	eb 21                	jmp    801063e0 <addToZombieList+0x6a>
  }
  if (ptable.pLists.zombie == 0)
801063bf:	a1 dc 70 11 80       	mov    0x801170dc,%eax
801063c4:	85 c0                	test   %eax,%eax
801063c6:	75 17                	jne    801063df <addToZombieList+0x69>
  {
    ptable.pLists.zombie = p;
801063c8:	8b 45 08             	mov    0x8(%ebp),%eax
801063cb:	a3 dc 70 11 80       	mov    %eax,0x801170dc
    p->next = 0;
801063d0:	8b 45 08             	mov    0x8(%ebp),%eax
801063d3:	c7 80 90 00 00 00 00 	movl   $0x0,0x90(%eax)
801063da:	00 00 00 
    return;
801063dd:	eb 01                	jmp    801063e0 <addToZombieList+0x6a>
  } 
  return;
801063df:	90                   	nop
}
801063e0:	c9                   	leave  
801063e1:	c3                   	ret    

801063e2 <countZombieList>:

void 
countZombieList(void)
{
801063e2:	55                   	push   %ebp
801063e3:	89 e5                	mov    %esp,%ebp
801063e5:	83 ec 18             	sub    $0x18,%esp
  acquire(&ptable.lock);
801063e8:	83 ec 0c             	sub    $0xc,%esp
801063eb:	68 80 49 11 80       	push   $0x80114980
801063f0:	e8 40 06 00 00       	call   80106a35 <acquire>
801063f5:	83 c4 10             	add    $0x10,%esp
  struct proc *temp = ptable.pLists.zombie;
801063f8:	a1 dc 70 11 80       	mov    0x801170dc,%eax
801063fd:	89 45 f4             	mov    %eax,-0xc(%ebp)
  int ppid;
  if (temp != 0)
80106400:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80106404:	0f 84 9f 00 00 00    	je     801064a9 <countZombieList+0xc7>
  {
    cprintf("Zombie List Processes:\n");
8010640a:	83 ec 0c             	sub    $0xc,%esp
8010640d:	68 f6 aa 10 80       	push   $0x8010aaf6
80106412:	e8 af 9f ff ff       	call   801003c6 <cprintf>
80106417:	83 c4 10             	add    $0x10,%esp
    while (temp->next) 
8010641a:	eb 45                	jmp    80106461 <countZombieList+0x7f>
    {
      ppid = temp->pid;
8010641c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010641f:	8b 40 10             	mov    0x10(%eax),%eax
80106422:	89 45 f0             	mov    %eax,-0x10(%ebp)
      if (temp->parent) ppid = temp->parent->pid;
80106425:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106428:	8b 40 1c             	mov    0x1c(%eax),%eax
8010642b:	85 c0                	test   %eax,%eax
8010642d:	74 0c                	je     8010643b <countZombieList+0x59>
8010642f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106432:	8b 40 1c             	mov    0x1c(%eax),%eax
80106435:	8b 40 10             	mov    0x10(%eax),%eax
80106438:	89 45 f0             	mov    %eax,-0x10(%ebp)
      cprintf("(%d, PPID%d) -> ", temp->pid, ppid);
8010643b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010643e:	8b 40 10             	mov    0x10(%eax),%eax
80106441:	83 ec 04             	sub    $0x4,%esp
80106444:	ff 75 f0             	pushl  -0x10(%ebp)
80106447:	50                   	push   %eax
80106448:	68 0e ab 10 80       	push   $0x8010ab0e
8010644d:	e8 74 9f ff ff       	call   801003c6 <cprintf>
80106452:	83 c4 10             	add    $0x10,%esp
      temp = temp->next;
80106455:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106458:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
8010645e:	89 45 f4             	mov    %eax,-0xc(%ebp)
  struct proc *temp = ptable.pLists.zombie;
  int ppid;
  if (temp != 0)
  {
    cprintf("Zombie List Processes:\n");
    while (temp->next) 
80106461:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106464:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
8010646a:	85 c0                	test   %eax,%eax
8010646c:	75 ae                	jne    8010641c <countZombieList+0x3a>
      ppid = temp->pid;
      if (temp->parent) ppid = temp->parent->pid;
      cprintf("(%d, PPID%d) -> ", temp->pid, ppid);
      temp = temp->next;
    }
    ppid = temp->pid;
8010646e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106471:	8b 40 10             	mov    0x10(%eax),%eax
80106474:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if (temp->parent) ppid = temp->parent->pid;
80106477:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010647a:	8b 40 1c             	mov    0x1c(%eax),%eax
8010647d:	85 c0                	test   %eax,%eax
8010647f:	74 0c                	je     8010648d <countZombieList+0xab>
80106481:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106484:	8b 40 1c             	mov    0x1c(%eax),%eax
80106487:	8b 40 10             	mov    0x10(%eax),%eax
8010648a:	89 45 f0             	mov    %eax,-0x10(%ebp)
    cprintf("(%d, PPID%d)\n", temp->pid, ppid);
8010648d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106490:	8b 40 10             	mov    0x10(%eax),%eax
80106493:	83 ec 04             	sub    $0x4,%esp
80106496:	ff 75 f0             	pushl  -0x10(%ebp)
80106499:	50                   	push   %eax
8010649a:	68 1f ab 10 80       	push   $0x8010ab1f
8010649f:	e8 22 9f ff ff       	call   801003c6 <cprintf>
801064a4:	83 c4 10             	add    $0x10,%esp
801064a7:	eb 10                	jmp    801064b9 <countZombieList+0xd7>
  }
  else cprintf("The Zombie List is empty\n");
801064a9:	83 ec 0c             	sub    $0xc,%esp
801064ac:	68 2d ab 10 80       	push   $0x8010ab2d
801064b1:	e8 10 9f ff ff       	call   801003c6 <cprintf>
801064b6:	83 c4 10             	add    $0x10,%esp
  release(&ptable.lock);
801064b9:	83 ec 0c             	sub    $0xc,%esp
801064bc:	68 80 49 11 80       	push   $0x80114980
801064c1:	e8 d6 05 00 00       	call   80106a9c <release>
801064c6:	83 c4 10             	add    $0x10,%esp
  return;
801064c9:	90                   	nop
}
801064ca:	c9                   	leave  
801064cb:	c3                   	ret    

801064cc <removeFromZombieList>:

static int
removeFromZombieList(struct proc *p)
{
801064cc:	55                   	push   %ebp
801064cd:	89 e5                	mov    %esp,%ebp
801064cf:	83 ec 18             	sub    $0x18,%esp
  if (!holding(&ptable.lock)) panic("Where is my lock?! lock is needed in removeFrompZombieList\n");
801064d2:	83 ec 0c             	sub    $0xc,%esp
801064d5:	68 80 49 11 80       	push   $0x80114980
801064da:	e8 89 06 00 00       	call   80106b68 <holding>
801064df:	83 c4 10             	add    $0x10,%esp
801064e2:	85 c0                	test   %eax,%eax
801064e4:	75 0d                	jne    801064f3 <removeFromZombieList+0x27>
801064e6:	83 ec 0c             	sub    $0xc,%esp
801064e9:	68 48 ab 10 80       	push   $0x8010ab48
801064ee:	e8 73 a0 ff ff       	call   80100566 <panic>
  if (ptable.pLists.zombie == 0) panic("Zombie List is empty!\n");
801064f3:	a1 dc 70 11 80       	mov    0x801170dc,%eax
801064f8:	85 c0                	test   %eax,%eax
801064fa:	75 0d                	jne    80106509 <removeFromZombieList+0x3d>
801064fc:	83 ec 0c             	sub    $0xc,%esp
801064ff:	68 84 ab 10 80       	push   $0x8010ab84
80106504:	e8 5d a0 ff ff       	call   80100566 <panic>
  if (ptable.pLists.zombie->pid == p->pid)
80106509:	a1 dc 70 11 80       	mov    0x801170dc,%eax
8010650e:	8b 50 10             	mov    0x10(%eax),%edx
80106511:	8b 45 08             	mov    0x8(%ebp),%eax
80106514:	8b 40 10             	mov    0x10(%eax),%eax
80106517:	39 c2                	cmp    %eax,%edx
80106519:	75 24                	jne    8010653f <removeFromZombieList+0x73>
  {
    ptable.pLists.zombie = ptable.pLists.zombie->next;
8010651b:	a1 dc 70 11 80       	mov    0x801170dc,%eax
80106520:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
80106526:	a3 dc 70 11 80       	mov    %eax,0x801170dc
    p->next = 0;
8010652b:	8b 45 08             	mov    0x8(%ebp),%eax
8010652e:	c7 80 90 00 00 00 00 	movl   $0x0,0x90(%eax)
80106535:	00 00 00 
    return 0;
80106538:	b8 00 00 00 00       	mov    $0x0,%eax
8010653d:	eb 6b                	jmp    801065aa <removeFromZombieList+0xde>
  }
  struct proc *current = ptable.pLists.zombie->next;
8010653f:	a1 dc 70 11 80       	mov    0x801170dc,%eax
80106544:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
8010654a:	89 45 f4             	mov    %eax,-0xc(%ebp)
  struct proc *prev = ptable.pLists.zombie;
8010654d:	a1 dc 70 11 80       	mov    0x801170dc,%eax
80106552:	89 45 f0             	mov    %eax,-0x10(%ebp)
  while(current)
80106555:	eb 48                	jmp    8010659f <removeFromZombieList+0xd3>
  {
    if (current->pid == p->pid)
80106557:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010655a:	8b 50 10             	mov    0x10(%eax),%edx
8010655d:	8b 45 08             	mov    0x8(%ebp),%eax
80106560:	8b 40 10             	mov    0x10(%eax),%eax
80106563:	39 c2                	cmp    %eax,%edx
80106565:	75 26                	jne    8010658d <removeFromZombieList+0xc1>
    {
      prev->next = current->next;
80106567:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010656a:	8b 90 90 00 00 00    	mov    0x90(%eax),%edx
80106570:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106573:	89 90 90 00 00 00    	mov    %edx,0x90(%eax)
      current -> next = 0;
80106579:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010657c:	c7 80 90 00 00 00 00 	movl   $0x0,0x90(%eax)
80106583:	00 00 00 
      return 0;
80106586:	b8 00 00 00 00       	mov    $0x0,%eax
8010658b:	eb 1d                	jmp    801065aa <removeFromZombieList+0xde>
    }
    prev = current;
8010658d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106590:	89 45 f0             	mov    %eax,-0x10(%ebp)
    current = current -> next;
80106593:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106596:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
8010659c:	89 45 f4             	mov    %eax,-0xc(%ebp)
    p->next = 0;
    return 0;
  }
  struct proc *current = ptable.pLists.zombie->next;
  struct proc *prev = ptable.pLists.zombie;
  while(current)
8010659f:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801065a3:	75 b2                	jne    80106557 <removeFromZombieList+0x8b>
      return 0;
    }
    prev = current;
    current = current -> next;
  }
  return -1;
801065a5:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
801065aa:	c9                   	leave  
801065ab:	c3                   	ret    

801065ac <addToEmbryoList>:

static void 
addToEmbryoList(struct proc *p)
{
801065ac:	55                   	push   %ebp
801065ad:	89 e5                	mov    %esp,%ebp
801065af:	83 ec 08             	sub    $0x8,%esp
  if (!holding(&ptable.lock)) panic("Where is my lock?! lock is needed in assToEmbryoList\n");
801065b2:	83 ec 0c             	sub    $0xc,%esp
801065b5:	68 80 49 11 80       	push   $0x80114980
801065ba:	e8 a9 05 00 00       	call   80106b68 <holding>
801065bf:	83 c4 10             	add    $0x10,%esp
801065c2:	85 c0                	test   %eax,%eax
801065c4:	75 0d                	jne    801065d3 <addToEmbryoList+0x27>
801065c6:	83 ec 0c             	sub    $0xc,%esp
801065c9:	68 9c ab 10 80       	push   $0x8010ab9c
801065ce:	e8 93 9f ff ff       	call   80100566 <panic>

  if (ptable.pLists.embryo != 0)
801065d3:	a1 e4 70 11 80       	mov    0x801170e4,%eax
801065d8:	85 c0                	test   %eax,%eax
801065da:	74 19                	je     801065f5 <addToEmbryoList+0x49>
  {  
    p->next = ptable.pLists.embryo;
801065dc:	8b 15 e4 70 11 80    	mov    0x801170e4,%edx
801065e2:	8b 45 08             	mov    0x8(%ebp),%eax
801065e5:	89 90 90 00 00 00    	mov    %edx,0x90(%eax)
    ptable.pLists.embryo = p;
801065eb:	8b 45 08             	mov    0x8(%ebp),%eax
801065ee:	a3 e4 70 11 80       	mov    %eax,0x801170e4
    return;
801065f3:	eb 1f                	jmp    80106614 <addToEmbryoList+0x68>
  }
  if (ptable.pLists.embryo == 0)
801065f5:	a1 e4 70 11 80       	mov    0x801170e4,%eax
801065fa:	85 c0                	test   %eax,%eax
801065fc:	75 16                	jne    80106614 <addToEmbryoList+0x68>
  {
    ptable.pLists.embryo = p;
801065fe:	8b 45 08             	mov    0x8(%ebp),%eax
80106601:	a3 e4 70 11 80       	mov    %eax,0x801170e4
    p->next = 0;
80106606:	8b 45 08             	mov    0x8(%ebp),%eax
80106609:	c7 80 90 00 00 00 00 	movl   $0x0,0x90(%eax)
80106610:	00 00 00 
    return;
80106613:	90                   	nop
  } 
}
80106614:	c9                   	leave  
80106615:	c3                   	ret    

80106616 <removeFromEmbryoList>:

static int 
removeFromEmbryoList(struct proc *p)
{
80106616:	55                   	push   %ebp
80106617:	89 e5                	mov    %esp,%ebp
80106619:	83 ec 18             	sub    $0x18,%esp
  if (!holding(&ptable.lock)) panic("Where is my lock?! lock is needed in removeFrompEmbryoList\n");
8010661c:	83 ec 0c             	sub    $0xc,%esp
8010661f:	68 80 49 11 80       	push   $0x80114980
80106624:	e8 3f 05 00 00       	call   80106b68 <holding>
80106629:	83 c4 10             	add    $0x10,%esp
8010662c:	85 c0                	test   %eax,%eax
8010662e:	75 0d                	jne    8010663d <removeFromEmbryoList+0x27>
80106630:	83 ec 0c             	sub    $0xc,%esp
80106633:	68 d4 ab 10 80       	push   $0x8010abd4
80106638:	e8 29 9f ff ff       	call   80100566 <panic>
  if (ptable.pLists.embryo->pid == p->pid)
8010663d:	a1 e4 70 11 80       	mov    0x801170e4,%eax
80106642:	8b 50 10             	mov    0x10(%eax),%edx
80106645:	8b 45 08             	mov    0x8(%ebp),%eax
80106648:	8b 40 10             	mov    0x10(%eax),%eax
8010664b:	39 c2                	cmp    %eax,%edx
8010664d:	75 24                	jne    80106673 <removeFromEmbryoList+0x5d>
  {
    ptable.pLists.embryo = ptable.pLists.embryo->next;
8010664f:	a1 e4 70 11 80       	mov    0x801170e4,%eax
80106654:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
8010665a:	a3 e4 70 11 80       	mov    %eax,0x801170e4
    p->next = 0;
8010665f:	8b 45 08             	mov    0x8(%ebp),%eax
80106662:	c7 80 90 00 00 00 00 	movl   $0x0,0x90(%eax)
80106669:	00 00 00 
    return 0;
8010666c:	b8 00 00 00 00       	mov    $0x0,%eax
80106671:	eb 6b                	jmp    801066de <removeFromEmbryoList+0xc8>
  }
  struct proc *current = ptable.pLists.embryo->next;
80106673:	a1 e4 70 11 80       	mov    0x801170e4,%eax
80106678:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
8010667e:	89 45 f4             	mov    %eax,-0xc(%ebp)
  struct proc *prev = ptable.pLists.embryo;
80106681:	a1 e4 70 11 80       	mov    0x801170e4,%eax
80106686:	89 45 f0             	mov    %eax,-0x10(%ebp)
  while(current)
80106689:	eb 48                	jmp    801066d3 <removeFromEmbryoList+0xbd>
  {
    if (current->pid == p->pid)
8010668b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010668e:	8b 50 10             	mov    0x10(%eax),%edx
80106691:	8b 45 08             	mov    0x8(%ebp),%eax
80106694:	8b 40 10             	mov    0x10(%eax),%eax
80106697:	39 c2                	cmp    %eax,%edx
80106699:	75 26                	jne    801066c1 <removeFromEmbryoList+0xab>
    {
      prev->next = current->next;
8010669b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010669e:	8b 90 90 00 00 00    	mov    0x90(%eax),%edx
801066a4:	8b 45 f0             	mov    -0x10(%ebp),%eax
801066a7:	89 90 90 00 00 00    	mov    %edx,0x90(%eax)
      current -> next = 0;
801066ad:	8b 45 f4             	mov    -0xc(%ebp),%eax
801066b0:	c7 80 90 00 00 00 00 	movl   $0x0,0x90(%eax)
801066b7:	00 00 00 
      return 0;
801066ba:	b8 00 00 00 00       	mov    $0x0,%eax
801066bf:	eb 1d                	jmp    801066de <removeFromEmbryoList+0xc8>
    }
    prev = current;
801066c1:	8b 45 f4             	mov    -0xc(%ebp),%eax
801066c4:	89 45 f0             	mov    %eax,-0x10(%ebp)
    current = current -> next;
801066c7:	8b 45 f4             	mov    -0xc(%ebp),%eax
801066ca:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
801066d0:	89 45 f4             	mov    %eax,-0xc(%ebp)
    p->next = 0;
    return 0;
  }
  struct proc *current = ptable.pLists.embryo->next;
  struct proc *prev = ptable.pLists.embryo;
  while(current)
801066d3:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801066d7:	75 b2                	jne    8010668b <removeFromEmbryoList+0x75>
      return 0;
    }
    prev = current;
    current = current -> next;
  }
  return -1;
801066d9:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
801066de:	c9                   	leave  
801066df:	c3                   	ret    

801066e0 <addToRunningList>:

static void 
addToRunningList(struct proc *p)
{
801066e0:	55                   	push   %ebp
801066e1:	89 e5                	mov    %esp,%ebp
801066e3:	83 ec 08             	sub    $0x8,%esp
  if (!holding(&ptable.lock)) panic("Where is my lock?! lock is needed in addToRunningList\n");
801066e6:	83 ec 0c             	sub    $0xc,%esp
801066e9:	68 80 49 11 80       	push   $0x80114980
801066ee:	e8 75 04 00 00       	call   80106b68 <holding>
801066f3:	83 c4 10             	add    $0x10,%esp
801066f6:	85 c0                	test   %eax,%eax
801066f8:	75 0d                	jne    80106707 <addToRunningList+0x27>
801066fa:	83 ec 0c             	sub    $0xc,%esp
801066fd:	68 10 ac 10 80       	push   $0x8010ac10
80106702:	e8 5f 9e ff ff       	call   80100566 <panic>

  if (ptable.pLists.running != 0)
80106707:	a1 e0 70 11 80       	mov    0x801170e0,%eax
8010670c:	85 c0                	test   %eax,%eax
8010670e:	74 19                	je     80106729 <addToRunningList+0x49>
  {  
    p->next = ptable.pLists.running;
80106710:	8b 15 e0 70 11 80    	mov    0x801170e0,%edx
80106716:	8b 45 08             	mov    0x8(%ebp),%eax
80106719:	89 90 90 00 00 00    	mov    %edx,0x90(%eax)
    ptable.pLists.running = p;
8010671f:	8b 45 08             	mov    0x8(%ebp),%eax
80106722:	a3 e0 70 11 80       	mov    %eax,0x801170e0
    return;
80106727:	eb 1f                	jmp    80106748 <addToRunningList+0x68>
  }
  if (ptable.pLists.running == 0)
80106729:	a1 e0 70 11 80       	mov    0x801170e0,%eax
8010672e:	85 c0                	test   %eax,%eax
80106730:	75 16                	jne    80106748 <addToRunningList+0x68>
  {
    ptable.pLists.running = p;
80106732:	8b 45 08             	mov    0x8(%ebp),%eax
80106735:	a3 e0 70 11 80       	mov    %eax,0x801170e0
    p->next = 0;
8010673a:	8b 45 08             	mov    0x8(%ebp),%eax
8010673d:	c7 80 90 00 00 00 00 	movl   $0x0,0x90(%eax)
80106744:	00 00 00 
    return;
80106747:	90                   	nop
  } 
}
80106748:	c9                   	leave  
80106749:	c3                   	ret    

8010674a <removeFromRunningList>:

static int 
removeFromRunningList(struct proc *p)
{
8010674a:	55                   	push   %ebp
8010674b:	89 e5                	mov    %esp,%ebp
8010674d:	83 ec 18             	sub    $0x18,%esp
  if (ptable.pLists.running == 0) return 0;
80106750:	a1 e0 70 11 80       	mov    0x801170e0,%eax
80106755:	85 c0                	test   %eax,%eax
80106757:	75 0a                	jne    80106763 <removeFromRunningList+0x19>
80106759:	b8 00 00 00 00       	mov    $0x0,%eax
8010675e:	e9 c2 00 00 00       	jmp    80106825 <removeFromRunningList+0xdb>
  if (!holding(&ptable.lock)) panic("Where is my lock?! lock is needed in removeFrompRunningList\n");
80106763:	83 ec 0c             	sub    $0xc,%esp
80106766:	68 80 49 11 80       	push   $0x80114980
8010676b:	e8 f8 03 00 00       	call   80106b68 <holding>
80106770:	83 c4 10             	add    $0x10,%esp
80106773:	85 c0                	test   %eax,%eax
80106775:	75 0d                	jne    80106784 <removeFromRunningList+0x3a>
80106777:	83 ec 0c             	sub    $0xc,%esp
8010677a:	68 48 ac 10 80       	push   $0x8010ac48
8010677f:	e8 e2 9d ff ff       	call   80100566 <panic>
  if (ptable.pLists.running->pid == p->pid)
80106784:	a1 e0 70 11 80       	mov    0x801170e0,%eax
80106789:	8b 50 10             	mov    0x10(%eax),%edx
8010678c:	8b 45 08             	mov    0x8(%ebp),%eax
8010678f:	8b 40 10             	mov    0x10(%eax),%eax
80106792:	39 c2                	cmp    %eax,%edx
80106794:	75 24                	jne    801067ba <removeFromRunningList+0x70>
  {
    ptable.pLists.running = ptable.pLists.running->next;
80106796:	a1 e0 70 11 80       	mov    0x801170e0,%eax
8010679b:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
801067a1:	a3 e0 70 11 80       	mov    %eax,0x801170e0
    p->next = 0;
801067a6:	8b 45 08             	mov    0x8(%ebp),%eax
801067a9:	c7 80 90 00 00 00 00 	movl   $0x0,0x90(%eax)
801067b0:	00 00 00 
    return 0;
801067b3:	b8 00 00 00 00       	mov    $0x0,%eax
801067b8:	eb 6b                	jmp    80106825 <removeFromRunningList+0xdb>
  }
  struct proc *current = ptable.pLists.running->next;
801067ba:	a1 e0 70 11 80       	mov    0x801170e0,%eax
801067bf:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
801067c5:	89 45 f4             	mov    %eax,-0xc(%ebp)
  struct proc *prev = ptable.pLists.running;
801067c8:	a1 e0 70 11 80       	mov    0x801170e0,%eax
801067cd:	89 45 f0             	mov    %eax,-0x10(%ebp)
  while(current)
801067d0:	eb 48                	jmp    8010681a <removeFromRunningList+0xd0>
  {
    if (current->pid == p->pid)
801067d2:	8b 45 f4             	mov    -0xc(%ebp),%eax
801067d5:	8b 50 10             	mov    0x10(%eax),%edx
801067d8:	8b 45 08             	mov    0x8(%ebp),%eax
801067db:	8b 40 10             	mov    0x10(%eax),%eax
801067de:	39 c2                	cmp    %eax,%edx
801067e0:	75 26                	jne    80106808 <removeFromRunningList+0xbe>
    {
      prev->next = current->next;
801067e2:	8b 45 f4             	mov    -0xc(%ebp),%eax
801067e5:	8b 90 90 00 00 00    	mov    0x90(%eax),%edx
801067eb:	8b 45 f0             	mov    -0x10(%ebp),%eax
801067ee:	89 90 90 00 00 00    	mov    %edx,0x90(%eax)
      current->next = 0;
801067f4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801067f7:	c7 80 90 00 00 00 00 	movl   $0x0,0x90(%eax)
801067fe:	00 00 00 
      return 0;
80106801:	b8 00 00 00 00       	mov    $0x0,%eax
80106806:	eb 1d                	jmp    80106825 <removeFromRunningList+0xdb>
    }
    prev = current;
80106808:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010680b:	89 45 f0             	mov    %eax,-0x10(%ebp)
    current = current -> next;
8010680e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106811:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
80106817:	89 45 f4             	mov    %eax,-0xc(%ebp)
    p->next = 0;
    return 0;
  }
  struct proc *current = ptable.pLists.running->next;
  struct proc *prev = ptable.pLists.running;
  while(current)
8010681a:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
8010681e:	75 b2                	jne    801067d2 <removeFromRunningList+0x88>
      return 0;
    }
    prev = current;
    current = current -> next;
  }
  return -1;
80106820:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80106825:	c9                   	leave  
80106826:	c3                   	ret    

80106827 <promoteReady>:
static int 
promoteReady()
{
80106827:	55                   	push   %ebp
80106828:	89 e5                	mov    %esp,%ebp
8010682a:	83 ec 18             	sub    $0x18,%esp
  struct proc *p;
  
  for(int i = 1; i < MAX; ++i){
8010682d:	c7 45 f4 01 00 00 00 	movl   $0x1,-0xc(%ebp)
80106834:	e9 d9 00 00 00       	jmp    80106912 <promoteReady+0xeb>
    p = ptable.pLists.ready[i];
80106839:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010683c:	05 cc 09 00 00       	add    $0x9cc,%eax
80106841:	8b 04 85 84 49 11 80 	mov    -0x7feeb67c(,%eax,4),%eax
80106848:	89 45 e8             	mov    %eax,-0x18(%ebp)
    if(p){
8010684b:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
8010684f:	0f 84 b2 00 00 00    	je     80106907 <promoteReady+0xe0>
      struct proc *temp, *current;
      if (!holding(&ptable.lock)) panic("Where is my lock?! lock is needed in promoteReady()\n");
80106855:	83 ec 0c             	sub    $0xc,%esp
80106858:	68 80 49 11 80       	push   $0x80114980
8010685d:	e8 06 03 00 00       	call   80106b68 <holding>
80106862:	83 c4 10             	add    $0x10,%esp
80106865:	85 c0                	test   %eax,%eax
80106867:	75 0d                	jne    80106876 <promoteReady+0x4f>
80106869:	83 ec 0c             	sub    $0xc,%esp
8010686c:	68 88 ac 10 80       	push   $0x8010ac88
80106871:	e8 f0 9c ff ff       	call   80100566 <panic>

      current = p;
80106876:	8b 45 e8             	mov    -0x18(%ebp),%eax
80106879:	89 45 ec             	mov    %eax,-0x14(%ebp)
      while(current) 
8010687c:	eb 2b                	jmp    801068a9 <promoteReady+0x82>
      { 
        current->priority = current->priority - 1;
8010687e:	8b 45 ec             	mov    -0x14(%ebp),%eax
80106881:	8b 80 94 00 00 00    	mov    0x94(%eax),%eax
80106887:	8d 50 ff             	lea    -0x1(%eax),%edx
8010688a:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010688d:	89 90 94 00 00 00    	mov    %edx,0x94(%eax)
        current->state = RUNNABLE;
80106893:	8b 45 ec             	mov    -0x14(%ebp),%eax
80106896:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
        //current->budget = BUDGET;
        current = current->next;
8010689d:	8b 45 ec             	mov    -0x14(%ebp),%eax
801068a0:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
801068a6:	89 45 ec             	mov    %eax,-0x14(%ebp)
    if(p){
      struct proc *temp, *current;
      if (!holding(&ptable.lock)) panic("Where is my lock?! lock is needed in promoteReady()\n");

      current = p;
      while(current) 
801068a9:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
801068ad:	75 cf                	jne    8010687e <promoteReady+0x57>
        current->priority = current->priority - 1;
        current->state = RUNNABLE;
        //current->budget = BUDGET;
        current = current->next;
      }
      temp = ptable.pLists.ready[i-1];
801068af:	8b 45 f4             	mov    -0xc(%ebp),%eax
801068b2:	83 e8 01             	sub    $0x1,%eax
801068b5:	05 cc 09 00 00       	add    $0x9cc,%eax
801068ba:	8b 04 85 84 49 11 80 	mov    -0x7feeb67c(,%eax,4),%eax
801068c1:	89 45 f0             	mov    %eax,-0x10(%ebp)
      if (temp == 0) ptable.pLists.ready[i-1] = p;
801068c4:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
801068c8:	75 24                	jne    801068ee <promoteReady+0xc7>
801068ca:	8b 45 f4             	mov    -0xc(%ebp),%eax
801068cd:	83 e8 01             	sub    $0x1,%eax
801068d0:	8d 90 cc 09 00 00    	lea    0x9cc(%eax),%edx
801068d6:	8b 45 e8             	mov    -0x18(%ebp),%eax
801068d9:	89 04 95 84 49 11 80 	mov    %eax,-0x7feeb67c(,%edx,4)
801068e0:	eb 25                	jmp    80106907 <promoteReady+0xe0>
      else {
        while (temp->next != 0) temp = temp->next;
801068e2:	8b 45 f0             	mov    -0x10(%ebp),%eax
801068e5:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
801068eb:	89 45 f0             	mov    %eax,-0x10(%ebp)
801068ee:	8b 45 f0             	mov    -0x10(%ebp),%eax
801068f1:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
801068f7:	85 c0                	test   %eax,%eax
801068f9:	75 e7                	jne    801068e2 <promoteReady+0xbb>
          temp->next = p;
801068fb:	8b 45 f0             	mov    -0x10(%ebp),%eax
801068fe:	8b 55 e8             	mov    -0x18(%ebp),%edx
80106901:	89 90 90 00 00 00    	mov    %edx,0x90(%eax)
      }
    }
    p = 0;
80106907:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)
static int 
promoteReady()
{
  struct proc *p;
  
  for(int i = 1; i < MAX; ++i){
8010690e:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80106912:	83 7d f4 06          	cmpl   $0x6,-0xc(%ebp)
80106916:	0f 8e 1d ff ff ff    	jle    80106839 <promoteReady+0x12>
          temp->next = p;
      }
    }
    p = 0;
  }
  return 0;
8010691c:	b8 00 00 00 00       	mov    $0x0,%eax
}
80106921:	c9                   	leave  
80106922:	c3                   	ret    

80106923 <promoteList>:
static int 
promoteList(struct proc ** head)
{
80106923:	55                   	push   %ebp
80106924:	89 e5                	mov    %esp,%ebp
80106926:	83 ec 10             	sub    $0x10,%esp
  struct proc * current;

  current = *head;
80106929:	8b 45 08             	mov    0x8(%ebp),%eax
8010692c:	8b 00                	mov    (%eax),%eax
8010692e:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while(current) 
80106931:	eb 2e                	jmp    80106961 <promoteList+0x3e>
  { 
    if(current->priority > 0)
80106933:	8b 45 fc             	mov    -0x4(%ebp),%eax
80106936:	8b 80 94 00 00 00    	mov    0x94(%eax),%eax
8010693c:	85 c0                	test   %eax,%eax
8010693e:	74 15                	je     80106955 <promoteList+0x32>
      current->priority = current->priority - 1;
80106940:	8b 45 fc             	mov    -0x4(%ebp),%eax
80106943:	8b 80 94 00 00 00    	mov    0x94(%eax),%eax
80106949:	8d 50 ff             	lea    -0x1(%eax),%edx
8010694c:	8b 45 fc             	mov    -0x4(%ebp),%eax
8010694f:	89 90 94 00 00 00    	mov    %edx,0x94(%eax)
    //current->budget = BUDGET;
    current = current->next;
80106955:	8b 45 fc             	mov    -0x4(%ebp),%eax
80106958:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
8010695e:	89 45 fc             	mov    %eax,-0x4(%ebp)
promoteList(struct proc ** head)
{
  struct proc * current;

  current = *head;
  while(current) 
80106961:	83 7d fc 00          	cmpl   $0x0,-0x4(%ebp)
80106965:	75 cc                	jne    80106933 <promoteList+0x10>
    if(current->priority > 0)
      current->priority = current->priority - 1;
    //current->budget = BUDGET;
    current = current->next;
  }
  return 0;
80106967:	b8 00 00 00 00       	mov    $0x0,%eax
}
8010696c:	c9                   	leave  
8010696d:	c3                   	ret    

8010696e <demote>:
static void 
demote(struct proc *proc)
{
8010696e:	55                   	push   %ebp
8010696f:	89 e5                	mov    %esp,%ebp
  proc->budget = proc->budget - (ticks - proc->cpu_ticks_in);
80106971:	8b 45 08             	mov    0x8(%ebp),%eax
80106974:	8b 80 98 00 00 00    	mov    0x98(%eax),%eax
8010697a:	89 c1                	mov    %eax,%ecx
8010697c:	8b 45 08             	mov    0x8(%ebp),%eax
8010697f:	8b 90 8c 00 00 00    	mov    0x8c(%eax),%edx
80106985:	a1 00 79 11 80       	mov    0x80117900,%eax
8010698a:	29 c2                	sub    %eax,%edx
8010698c:	89 d0                	mov    %edx,%eax
8010698e:	01 c8                	add    %ecx,%eax
80106990:	89 c2                	mov    %eax,%edx
80106992:	8b 45 08             	mov    0x8(%ebp),%eax
80106995:	89 90 98 00 00 00    	mov    %edx,0x98(%eax)
  if(proc->budget <= 0)
8010699b:	8b 45 08             	mov    0x8(%ebp),%eax
8010699e:	8b 80 98 00 00 00    	mov    0x98(%eax),%eax
801069a4:	85 c0                	test   %eax,%eax
801069a6:	7f 30                	jg     801069d8 <demote+0x6a>
  {
    if(proc->priority < MAX){
801069a8:	8b 45 08             	mov    0x8(%ebp),%eax
801069ab:	8b 80 94 00 00 00    	mov    0x94(%eax),%eax
801069b1:	83 f8 06             	cmp    $0x6,%eax
801069b4:	77 15                	ja     801069cb <demote+0x5d>
      proc->priority += 1;
801069b6:	8b 45 08             	mov    0x8(%ebp),%eax
801069b9:	8b 80 94 00 00 00    	mov    0x94(%eax),%eax
801069bf:	8d 50 01             	lea    0x1(%eax),%edx
801069c2:	8b 45 08             	mov    0x8(%ebp),%eax
801069c5:	89 90 94 00 00 00    	mov    %edx,0x94(%eax)
    }
    proc->budget = BUDGET;
801069cb:	8b 45 08             	mov    0x8(%ebp),%eax
801069ce:	c7 80 98 00 00 00 14 	movl   $0x14,0x98(%eax)
801069d5:	00 00 00 
  }
}
801069d8:	90                   	nop
801069d9:	5d                   	pop    %ebp
801069da:	c3                   	ret    

801069db <readeflags>:
  asm volatile("ltr %0" : : "r" (sel));
}

static inline uint
readeflags(void)
{
801069db:	55                   	push   %ebp
801069dc:	89 e5                	mov    %esp,%ebp
801069de:	83 ec 10             	sub    $0x10,%esp
  uint eflags;
  asm volatile("pushfl; popl %0" : "=r" (eflags));
801069e1:	9c                   	pushf  
801069e2:	58                   	pop    %eax
801069e3:	89 45 fc             	mov    %eax,-0x4(%ebp)
  return eflags;
801069e6:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
801069e9:	c9                   	leave  
801069ea:	c3                   	ret    

801069eb <cli>:
  asm volatile("movw %0, %%gs" : : "r" (v));
}

static inline void
cli(void)
{
801069eb:	55                   	push   %ebp
801069ec:	89 e5                	mov    %esp,%ebp
  asm volatile("cli");
801069ee:	fa                   	cli    
}
801069ef:	90                   	nop
801069f0:	5d                   	pop    %ebp
801069f1:	c3                   	ret    

801069f2 <sti>:

static inline void
sti(void)
{
801069f2:	55                   	push   %ebp
801069f3:	89 e5                	mov    %esp,%ebp
  asm volatile("sti");
801069f5:	fb                   	sti    
}
801069f6:	90                   	nop
801069f7:	5d                   	pop    %ebp
801069f8:	c3                   	ret    

801069f9 <xchg>:

static inline uint
xchg(volatile uint *addr, uint newval)
{
801069f9:	55                   	push   %ebp
801069fa:	89 e5                	mov    %esp,%ebp
801069fc:	83 ec 10             	sub    $0x10,%esp
  uint result;
  
  // The + in "+m" denotes a read-modify-write operand.
  asm volatile("lock; xchgl %0, %1" :
801069ff:	8b 55 08             	mov    0x8(%ebp),%edx
80106a02:	8b 45 0c             	mov    0xc(%ebp),%eax
80106a05:	8b 4d 08             	mov    0x8(%ebp),%ecx
80106a08:	f0 87 02             	lock xchg %eax,(%edx)
80106a0b:	89 45 fc             	mov    %eax,-0x4(%ebp)
               "+m" (*addr), "=a" (result) :
               "1" (newval) :
               "cc");
  return result;
80106a0e:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80106a11:	c9                   	leave  
80106a12:	c3                   	ret    

80106a13 <initlock>:
#include "proc.h"
#include "spinlock.h"

void
initlock(struct spinlock *lk, char *name)
{
80106a13:	55                   	push   %ebp
80106a14:	89 e5                	mov    %esp,%ebp
  lk->name = name;
80106a16:	8b 45 08             	mov    0x8(%ebp),%eax
80106a19:	8b 55 0c             	mov    0xc(%ebp),%edx
80106a1c:	89 50 04             	mov    %edx,0x4(%eax)
  lk->locked = 0;
80106a1f:	8b 45 08             	mov    0x8(%ebp),%eax
80106a22:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
  lk->cpu = 0;
80106a28:	8b 45 08             	mov    0x8(%ebp),%eax
80106a2b:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
}
80106a32:	90                   	nop
80106a33:	5d                   	pop    %ebp
80106a34:	c3                   	ret    

80106a35 <acquire>:
// Loops (spins) until the lock is acquired.
// Holding a lock for a long time may cause
// other CPUs to waste time spinning to acquire it.
void
acquire(struct spinlock *lk)
{
80106a35:	55                   	push   %ebp
80106a36:	89 e5                	mov    %esp,%ebp
80106a38:	83 ec 08             	sub    $0x8,%esp
  pushcli(); // disable interrupts to avoid deadlock.
80106a3b:	e8 52 01 00 00       	call   80106b92 <pushcli>
  if(holding(lk))
80106a40:	8b 45 08             	mov    0x8(%ebp),%eax
80106a43:	83 ec 0c             	sub    $0xc,%esp
80106a46:	50                   	push   %eax
80106a47:	e8 1c 01 00 00       	call   80106b68 <holding>
80106a4c:	83 c4 10             	add    $0x10,%esp
80106a4f:	85 c0                	test   %eax,%eax
80106a51:	74 0d                	je     80106a60 <acquire+0x2b>
    panic("acquire");
80106a53:	83 ec 0c             	sub    $0xc,%esp
80106a56:	68 bd ac 10 80       	push   $0x8010acbd
80106a5b:	e8 06 9b ff ff       	call   80100566 <panic>

  // The xchg is atomic.
  // It also serializes, so that reads after acquire are not
  // reordered before it. 
  while(xchg(&lk->locked, 1) != 0)
80106a60:	90                   	nop
80106a61:	8b 45 08             	mov    0x8(%ebp),%eax
80106a64:	83 ec 08             	sub    $0x8,%esp
80106a67:	6a 01                	push   $0x1
80106a69:	50                   	push   %eax
80106a6a:	e8 8a ff ff ff       	call   801069f9 <xchg>
80106a6f:	83 c4 10             	add    $0x10,%esp
80106a72:	85 c0                	test   %eax,%eax
80106a74:	75 eb                	jne    80106a61 <acquire+0x2c>
    ;

  // Record info about lock acquisition for debugging.
  lk->cpu = cpu;
80106a76:	8b 45 08             	mov    0x8(%ebp),%eax
80106a79:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
80106a80:	89 50 08             	mov    %edx,0x8(%eax)
  getcallerpcs(&lk, lk->pcs);
80106a83:	8b 45 08             	mov    0x8(%ebp),%eax
80106a86:	83 c0 0c             	add    $0xc,%eax
80106a89:	83 ec 08             	sub    $0x8,%esp
80106a8c:	50                   	push   %eax
80106a8d:	8d 45 08             	lea    0x8(%ebp),%eax
80106a90:	50                   	push   %eax
80106a91:	e8 58 00 00 00       	call   80106aee <getcallerpcs>
80106a96:	83 c4 10             	add    $0x10,%esp
}
80106a99:	90                   	nop
80106a9a:	c9                   	leave  
80106a9b:	c3                   	ret    

80106a9c <release>:

// Release the lock.
void
release(struct spinlock *lk)
{
80106a9c:	55                   	push   %ebp
80106a9d:	89 e5                	mov    %esp,%ebp
80106a9f:	83 ec 08             	sub    $0x8,%esp
  if(!holding(lk))
80106aa2:	83 ec 0c             	sub    $0xc,%esp
80106aa5:	ff 75 08             	pushl  0x8(%ebp)
80106aa8:	e8 bb 00 00 00       	call   80106b68 <holding>
80106aad:	83 c4 10             	add    $0x10,%esp
80106ab0:	85 c0                	test   %eax,%eax
80106ab2:	75 0d                	jne    80106ac1 <release+0x25>
    panic("release");
80106ab4:	83 ec 0c             	sub    $0xc,%esp
80106ab7:	68 c5 ac 10 80       	push   $0x8010acc5
80106abc:	e8 a5 9a ff ff       	call   80100566 <panic>

  lk->pcs[0] = 0;
80106ac1:	8b 45 08             	mov    0x8(%ebp),%eax
80106ac4:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
  lk->cpu = 0;
80106acb:	8b 45 08             	mov    0x8(%ebp),%eax
80106ace:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
  // But the 2007 Intel 64 Architecture Memory Ordering White
  // Paper says that Intel 64 and IA-32 will not move a load
  // after a store. So lock->locked = 0 would work here.
  // The xchg being asm volatile ensures gcc emits it after
  // the above assignments (and after the critical section).
  xchg(&lk->locked, 0);
80106ad5:	8b 45 08             	mov    0x8(%ebp),%eax
80106ad8:	83 ec 08             	sub    $0x8,%esp
80106adb:	6a 00                	push   $0x0
80106add:	50                   	push   %eax
80106ade:	e8 16 ff ff ff       	call   801069f9 <xchg>
80106ae3:	83 c4 10             	add    $0x10,%esp

  popcli();
80106ae6:	e8 ec 00 00 00       	call   80106bd7 <popcli>
}
80106aeb:	90                   	nop
80106aec:	c9                   	leave  
80106aed:	c3                   	ret    

80106aee <getcallerpcs>:

// Record the current call stack in pcs[] by following the %ebp chain.
void
getcallerpcs(void *v, uint pcs[])
{
80106aee:	55                   	push   %ebp
80106aef:	89 e5                	mov    %esp,%ebp
80106af1:	83 ec 10             	sub    $0x10,%esp
  uint *ebp;
  int i;
  
  ebp = (uint*)v - 2;
80106af4:	8b 45 08             	mov    0x8(%ebp),%eax
80106af7:	83 e8 08             	sub    $0x8,%eax
80106afa:	89 45 fc             	mov    %eax,-0x4(%ebp)
  for(i = 0; i < 10; i++){
80106afd:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%ebp)
80106b04:	eb 38                	jmp    80106b3e <getcallerpcs+0x50>
    if(ebp == 0 || ebp < (uint*)KERNBASE || ebp == (uint*)0xffffffff)
80106b06:	83 7d fc 00          	cmpl   $0x0,-0x4(%ebp)
80106b0a:	74 53                	je     80106b5f <getcallerpcs+0x71>
80106b0c:	81 7d fc ff ff ff 7f 	cmpl   $0x7fffffff,-0x4(%ebp)
80106b13:	76 4a                	jbe    80106b5f <getcallerpcs+0x71>
80106b15:	83 7d fc ff          	cmpl   $0xffffffff,-0x4(%ebp)
80106b19:	74 44                	je     80106b5f <getcallerpcs+0x71>
      break;
    pcs[i] = ebp[1];     // saved %eip
80106b1b:	8b 45 f8             	mov    -0x8(%ebp),%eax
80106b1e:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
80106b25:	8b 45 0c             	mov    0xc(%ebp),%eax
80106b28:	01 c2                	add    %eax,%edx
80106b2a:	8b 45 fc             	mov    -0x4(%ebp),%eax
80106b2d:	8b 40 04             	mov    0x4(%eax),%eax
80106b30:	89 02                	mov    %eax,(%edx)
    ebp = (uint*)ebp[0]; // saved %ebp
80106b32:	8b 45 fc             	mov    -0x4(%ebp),%eax
80106b35:	8b 00                	mov    (%eax),%eax
80106b37:	89 45 fc             	mov    %eax,-0x4(%ebp)
{
  uint *ebp;
  int i;
  
  ebp = (uint*)v - 2;
  for(i = 0; i < 10; i++){
80106b3a:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
80106b3e:	83 7d f8 09          	cmpl   $0x9,-0x8(%ebp)
80106b42:	7e c2                	jle    80106b06 <getcallerpcs+0x18>
    if(ebp == 0 || ebp < (uint*)KERNBASE || ebp == (uint*)0xffffffff)
      break;
    pcs[i] = ebp[1];     // saved %eip
    ebp = (uint*)ebp[0]; // saved %ebp
  }
  for(; i < 10; i++)
80106b44:	eb 19                	jmp    80106b5f <getcallerpcs+0x71>
    pcs[i] = 0;
80106b46:	8b 45 f8             	mov    -0x8(%ebp),%eax
80106b49:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
80106b50:	8b 45 0c             	mov    0xc(%ebp),%eax
80106b53:	01 d0                	add    %edx,%eax
80106b55:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
    if(ebp == 0 || ebp < (uint*)KERNBASE || ebp == (uint*)0xffffffff)
      break;
    pcs[i] = ebp[1];     // saved %eip
    ebp = (uint*)ebp[0]; // saved %ebp
  }
  for(; i < 10; i++)
80106b5b:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
80106b5f:	83 7d f8 09          	cmpl   $0x9,-0x8(%ebp)
80106b63:	7e e1                	jle    80106b46 <getcallerpcs+0x58>
    pcs[i] = 0;
}
80106b65:	90                   	nop
80106b66:	c9                   	leave  
80106b67:	c3                   	ret    

80106b68 <holding>:

// Check whether this cpu is holding the lock.
int
holding(struct spinlock *lock)
{
80106b68:	55                   	push   %ebp
80106b69:	89 e5                	mov    %esp,%ebp
  return lock->locked && lock->cpu == cpu;
80106b6b:	8b 45 08             	mov    0x8(%ebp),%eax
80106b6e:	8b 00                	mov    (%eax),%eax
80106b70:	85 c0                	test   %eax,%eax
80106b72:	74 17                	je     80106b8b <holding+0x23>
80106b74:	8b 45 08             	mov    0x8(%ebp),%eax
80106b77:	8b 50 08             	mov    0x8(%eax),%edx
80106b7a:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80106b80:	39 c2                	cmp    %eax,%edx
80106b82:	75 07                	jne    80106b8b <holding+0x23>
80106b84:	b8 01 00 00 00       	mov    $0x1,%eax
80106b89:	eb 05                	jmp    80106b90 <holding+0x28>
80106b8b:	b8 00 00 00 00       	mov    $0x0,%eax
}
80106b90:	5d                   	pop    %ebp
80106b91:	c3                   	ret    

80106b92 <pushcli>:
// it takes two popcli to undo two pushcli.  Also, if interrupts
// are off, then pushcli, popcli leaves them off.

void
pushcli(void)
{
80106b92:	55                   	push   %ebp
80106b93:	89 e5                	mov    %esp,%ebp
80106b95:	83 ec 10             	sub    $0x10,%esp
  int eflags;
  
  eflags = readeflags();
80106b98:	e8 3e fe ff ff       	call   801069db <readeflags>
80106b9d:	89 45 fc             	mov    %eax,-0x4(%ebp)
  cli();
80106ba0:	e8 46 fe ff ff       	call   801069eb <cli>
  if(cpu->ncli++ == 0)
80106ba5:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
80106bac:	8b 82 ac 00 00 00    	mov    0xac(%edx),%eax
80106bb2:	8d 48 01             	lea    0x1(%eax),%ecx
80106bb5:	89 8a ac 00 00 00    	mov    %ecx,0xac(%edx)
80106bbb:	85 c0                	test   %eax,%eax
80106bbd:	75 15                	jne    80106bd4 <pushcli+0x42>
    cpu->intena = eflags & FL_IF;
80106bbf:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80106bc5:	8b 55 fc             	mov    -0x4(%ebp),%edx
80106bc8:	81 e2 00 02 00 00    	and    $0x200,%edx
80106bce:	89 90 b0 00 00 00    	mov    %edx,0xb0(%eax)
}
80106bd4:	90                   	nop
80106bd5:	c9                   	leave  
80106bd6:	c3                   	ret    

80106bd7 <popcli>:

void
popcli(void)
{
80106bd7:	55                   	push   %ebp
80106bd8:	89 e5                	mov    %esp,%ebp
80106bda:	83 ec 08             	sub    $0x8,%esp
  if(readeflags()&FL_IF)
80106bdd:	e8 f9 fd ff ff       	call   801069db <readeflags>
80106be2:	25 00 02 00 00       	and    $0x200,%eax
80106be7:	85 c0                	test   %eax,%eax
80106be9:	74 0d                	je     80106bf8 <popcli+0x21>
    panic("popcli - interruptible");
80106beb:	83 ec 0c             	sub    $0xc,%esp
80106bee:	68 cd ac 10 80       	push   $0x8010accd
80106bf3:	e8 6e 99 ff ff       	call   80100566 <panic>
  if(--cpu->ncli < 0)
80106bf8:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80106bfe:	8b 90 ac 00 00 00    	mov    0xac(%eax),%edx
80106c04:	83 ea 01             	sub    $0x1,%edx
80106c07:	89 90 ac 00 00 00    	mov    %edx,0xac(%eax)
80106c0d:	8b 80 ac 00 00 00    	mov    0xac(%eax),%eax
80106c13:	85 c0                	test   %eax,%eax
80106c15:	79 0d                	jns    80106c24 <popcli+0x4d>
    panic("popcli");
80106c17:	83 ec 0c             	sub    $0xc,%esp
80106c1a:	68 e4 ac 10 80       	push   $0x8010ace4
80106c1f:	e8 42 99 ff ff       	call   80100566 <panic>
  if(cpu->ncli == 0 && cpu->intena)
80106c24:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80106c2a:	8b 80 ac 00 00 00    	mov    0xac(%eax),%eax
80106c30:	85 c0                	test   %eax,%eax
80106c32:	75 15                	jne    80106c49 <popcli+0x72>
80106c34:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80106c3a:	8b 80 b0 00 00 00    	mov    0xb0(%eax),%eax
80106c40:	85 c0                	test   %eax,%eax
80106c42:	74 05                	je     80106c49 <popcli+0x72>
    sti();
80106c44:	e8 a9 fd ff ff       	call   801069f2 <sti>
}
80106c49:	90                   	nop
80106c4a:	c9                   	leave  
80106c4b:	c3                   	ret    

80106c4c <stosb>:
               "cc");
}

static inline void
stosb(void *addr, int data, int cnt)
{
80106c4c:	55                   	push   %ebp
80106c4d:	89 e5                	mov    %esp,%ebp
80106c4f:	57                   	push   %edi
80106c50:	53                   	push   %ebx
  asm volatile("cld; rep stosb" :
80106c51:	8b 4d 08             	mov    0x8(%ebp),%ecx
80106c54:	8b 55 10             	mov    0x10(%ebp),%edx
80106c57:	8b 45 0c             	mov    0xc(%ebp),%eax
80106c5a:	89 cb                	mov    %ecx,%ebx
80106c5c:	89 df                	mov    %ebx,%edi
80106c5e:	89 d1                	mov    %edx,%ecx
80106c60:	fc                   	cld    
80106c61:	f3 aa                	rep stos %al,%es:(%edi)
80106c63:	89 ca                	mov    %ecx,%edx
80106c65:	89 fb                	mov    %edi,%ebx
80106c67:	89 5d 08             	mov    %ebx,0x8(%ebp)
80106c6a:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
80106c6d:	90                   	nop
80106c6e:	5b                   	pop    %ebx
80106c6f:	5f                   	pop    %edi
80106c70:	5d                   	pop    %ebp
80106c71:	c3                   	ret    

80106c72 <stosl>:

static inline void
stosl(void *addr, int data, int cnt)
{
80106c72:	55                   	push   %ebp
80106c73:	89 e5                	mov    %esp,%ebp
80106c75:	57                   	push   %edi
80106c76:	53                   	push   %ebx
  asm volatile("cld; rep stosl" :
80106c77:	8b 4d 08             	mov    0x8(%ebp),%ecx
80106c7a:	8b 55 10             	mov    0x10(%ebp),%edx
80106c7d:	8b 45 0c             	mov    0xc(%ebp),%eax
80106c80:	89 cb                	mov    %ecx,%ebx
80106c82:	89 df                	mov    %ebx,%edi
80106c84:	89 d1                	mov    %edx,%ecx
80106c86:	fc                   	cld    
80106c87:	f3 ab                	rep stos %eax,%es:(%edi)
80106c89:	89 ca                	mov    %ecx,%edx
80106c8b:	89 fb                	mov    %edi,%ebx
80106c8d:	89 5d 08             	mov    %ebx,0x8(%ebp)
80106c90:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
80106c93:	90                   	nop
80106c94:	5b                   	pop    %ebx
80106c95:	5f                   	pop    %edi
80106c96:	5d                   	pop    %ebp
80106c97:	c3                   	ret    

80106c98 <memset>:
#include "types.h"
#include "x86.h"

void*
memset(void *dst, int c, uint n)
{
80106c98:	55                   	push   %ebp
80106c99:	89 e5                	mov    %esp,%ebp
  if ((int)dst%4 == 0 && n%4 == 0){
80106c9b:	8b 45 08             	mov    0x8(%ebp),%eax
80106c9e:	83 e0 03             	and    $0x3,%eax
80106ca1:	85 c0                	test   %eax,%eax
80106ca3:	75 43                	jne    80106ce8 <memset+0x50>
80106ca5:	8b 45 10             	mov    0x10(%ebp),%eax
80106ca8:	83 e0 03             	and    $0x3,%eax
80106cab:	85 c0                	test   %eax,%eax
80106cad:	75 39                	jne    80106ce8 <memset+0x50>
    c &= 0xFF;
80106caf:	81 65 0c ff 00 00 00 	andl   $0xff,0xc(%ebp)
    stosl(dst, (c<<24)|(c<<16)|(c<<8)|c, n/4);
80106cb6:	8b 45 10             	mov    0x10(%ebp),%eax
80106cb9:	c1 e8 02             	shr    $0x2,%eax
80106cbc:	89 c1                	mov    %eax,%ecx
80106cbe:	8b 45 0c             	mov    0xc(%ebp),%eax
80106cc1:	c1 e0 18             	shl    $0x18,%eax
80106cc4:	89 c2                	mov    %eax,%edx
80106cc6:	8b 45 0c             	mov    0xc(%ebp),%eax
80106cc9:	c1 e0 10             	shl    $0x10,%eax
80106ccc:	09 c2                	or     %eax,%edx
80106cce:	8b 45 0c             	mov    0xc(%ebp),%eax
80106cd1:	c1 e0 08             	shl    $0x8,%eax
80106cd4:	09 d0                	or     %edx,%eax
80106cd6:	0b 45 0c             	or     0xc(%ebp),%eax
80106cd9:	51                   	push   %ecx
80106cda:	50                   	push   %eax
80106cdb:	ff 75 08             	pushl  0x8(%ebp)
80106cde:	e8 8f ff ff ff       	call   80106c72 <stosl>
80106ce3:	83 c4 0c             	add    $0xc,%esp
80106ce6:	eb 12                	jmp    80106cfa <memset+0x62>
  } else
    stosb(dst, c, n);
80106ce8:	8b 45 10             	mov    0x10(%ebp),%eax
80106ceb:	50                   	push   %eax
80106cec:	ff 75 0c             	pushl  0xc(%ebp)
80106cef:	ff 75 08             	pushl  0x8(%ebp)
80106cf2:	e8 55 ff ff ff       	call   80106c4c <stosb>
80106cf7:	83 c4 0c             	add    $0xc,%esp
  return dst;
80106cfa:	8b 45 08             	mov    0x8(%ebp),%eax
}
80106cfd:	c9                   	leave  
80106cfe:	c3                   	ret    

80106cff <memcmp>:

int
memcmp(const void *v1, const void *v2, uint n)
{
80106cff:	55                   	push   %ebp
80106d00:	89 e5                	mov    %esp,%ebp
80106d02:	83 ec 10             	sub    $0x10,%esp
  const uchar *s1, *s2;
  
  s1 = v1;
80106d05:	8b 45 08             	mov    0x8(%ebp),%eax
80106d08:	89 45 fc             	mov    %eax,-0x4(%ebp)
  s2 = v2;
80106d0b:	8b 45 0c             	mov    0xc(%ebp),%eax
80106d0e:	89 45 f8             	mov    %eax,-0x8(%ebp)
  while(n-- > 0){
80106d11:	eb 30                	jmp    80106d43 <memcmp+0x44>
    if(*s1 != *s2)
80106d13:	8b 45 fc             	mov    -0x4(%ebp),%eax
80106d16:	0f b6 10             	movzbl (%eax),%edx
80106d19:	8b 45 f8             	mov    -0x8(%ebp),%eax
80106d1c:	0f b6 00             	movzbl (%eax),%eax
80106d1f:	38 c2                	cmp    %al,%dl
80106d21:	74 18                	je     80106d3b <memcmp+0x3c>
      return *s1 - *s2;
80106d23:	8b 45 fc             	mov    -0x4(%ebp),%eax
80106d26:	0f b6 00             	movzbl (%eax),%eax
80106d29:	0f b6 d0             	movzbl %al,%edx
80106d2c:	8b 45 f8             	mov    -0x8(%ebp),%eax
80106d2f:	0f b6 00             	movzbl (%eax),%eax
80106d32:	0f b6 c0             	movzbl %al,%eax
80106d35:	29 c2                	sub    %eax,%edx
80106d37:	89 d0                	mov    %edx,%eax
80106d39:	eb 1a                	jmp    80106d55 <memcmp+0x56>
    s1++, s2++;
80106d3b:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
80106d3f:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
{
  const uchar *s1, *s2;
  
  s1 = v1;
  s2 = v2;
  while(n-- > 0){
80106d43:	8b 45 10             	mov    0x10(%ebp),%eax
80106d46:	8d 50 ff             	lea    -0x1(%eax),%edx
80106d49:	89 55 10             	mov    %edx,0x10(%ebp)
80106d4c:	85 c0                	test   %eax,%eax
80106d4e:	75 c3                	jne    80106d13 <memcmp+0x14>
    if(*s1 != *s2)
      return *s1 - *s2;
    s1++, s2++;
  }

  return 0;
80106d50:	b8 00 00 00 00       	mov    $0x0,%eax
}
80106d55:	c9                   	leave  
80106d56:	c3                   	ret    

80106d57 <memmove>:

void*
memmove(void *dst, const void *src, uint n)
{
80106d57:	55                   	push   %ebp
80106d58:	89 e5                	mov    %esp,%ebp
80106d5a:	83 ec 10             	sub    $0x10,%esp
  const char *s;
  char *d;

  s = src;
80106d5d:	8b 45 0c             	mov    0xc(%ebp),%eax
80106d60:	89 45 fc             	mov    %eax,-0x4(%ebp)
  d = dst;
80106d63:	8b 45 08             	mov    0x8(%ebp),%eax
80106d66:	89 45 f8             	mov    %eax,-0x8(%ebp)
  if(s < d && s + n > d){
80106d69:	8b 45 fc             	mov    -0x4(%ebp),%eax
80106d6c:	3b 45 f8             	cmp    -0x8(%ebp),%eax
80106d6f:	73 54                	jae    80106dc5 <memmove+0x6e>
80106d71:	8b 55 fc             	mov    -0x4(%ebp),%edx
80106d74:	8b 45 10             	mov    0x10(%ebp),%eax
80106d77:	01 d0                	add    %edx,%eax
80106d79:	3b 45 f8             	cmp    -0x8(%ebp),%eax
80106d7c:	76 47                	jbe    80106dc5 <memmove+0x6e>
    s += n;
80106d7e:	8b 45 10             	mov    0x10(%ebp),%eax
80106d81:	01 45 fc             	add    %eax,-0x4(%ebp)
    d += n;
80106d84:	8b 45 10             	mov    0x10(%ebp),%eax
80106d87:	01 45 f8             	add    %eax,-0x8(%ebp)
    while(n-- > 0)
80106d8a:	eb 13                	jmp    80106d9f <memmove+0x48>
      *--d = *--s;
80106d8c:	83 6d f8 01          	subl   $0x1,-0x8(%ebp)
80106d90:	83 6d fc 01          	subl   $0x1,-0x4(%ebp)
80106d94:	8b 45 fc             	mov    -0x4(%ebp),%eax
80106d97:	0f b6 10             	movzbl (%eax),%edx
80106d9a:	8b 45 f8             	mov    -0x8(%ebp),%eax
80106d9d:	88 10                	mov    %dl,(%eax)
  s = src;
  d = dst;
  if(s < d && s + n > d){
    s += n;
    d += n;
    while(n-- > 0)
80106d9f:	8b 45 10             	mov    0x10(%ebp),%eax
80106da2:	8d 50 ff             	lea    -0x1(%eax),%edx
80106da5:	89 55 10             	mov    %edx,0x10(%ebp)
80106da8:	85 c0                	test   %eax,%eax
80106daa:	75 e0                	jne    80106d8c <memmove+0x35>
  const char *s;
  char *d;

  s = src;
  d = dst;
  if(s < d && s + n > d){
80106dac:	eb 24                	jmp    80106dd2 <memmove+0x7b>
    d += n;
    while(n-- > 0)
      *--d = *--s;
  } else
    while(n-- > 0)
      *d++ = *s++;
80106dae:	8b 45 f8             	mov    -0x8(%ebp),%eax
80106db1:	8d 50 01             	lea    0x1(%eax),%edx
80106db4:	89 55 f8             	mov    %edx,-0x8(%ebp)
80106db7:	8b 55 fc             	mov    -0x4(%ebp),%edx
80106dba:	8d 4a 01             	lea    0x1(%edx),%ecx
80106dbd:	89 4d fc             	mov    %ecx,-0x4(%ebp)
80106dc0:	0f b6 12             	movzbl (%edx),%edx
80106dc3:	88 10                	mov    %dl,(%eax)
    s += n;
    d += n;
    while(n-- > 0)
      *--d = *--s;
  } else
    while(n-- > 0)
80106dc5:	8b 45 10             	mov    0x10(%ebp),%eax
80106dc8:	8d 50 ff             	lea    -0x1(%eax),%edx
80106dcb:	89 55 10             	mov    %edx,0x10(%ebp)
80106dce:	85 c0                	test   %eax,%eax
80106dd0:	75 dc                	jne    80106dae <memmove+0x57>
      *d++ = *s++;

  return dst;
80106dd2:	8b 45 08             	mov    0x8(%ebp),%eax
}
80106dd5:	c9                   	leave  
80106dd6:	c3                   	ret    

80106dd7 <memcpy>:

// memcpy exists to placate GCC.  Use memmove.
void*
memcpy(void *dst, const void *src, uint n)
{
80106dd7:	55                   	push   %ebp
80106dd8:	89 e5                	mov    %esp,%ebp
  return memmove(dst, src, n);
80106dda:	ff 75 10             	pushl  0x10(%ebp)
80106ddd:	ff 75 0c             	pushl  0xc(%ebp)
80106de0:	ff 75 08             	pushl  0x8(%ebp)
80106de3:	e8 6f ff ff ff       	call   80106d57 <memmove>
80106de8:	83 c4 0c             	add    $0xc,%esp
}
80106deb:	c9                   	leave  
80106dec:	c3                   	ret    

80106ded <strncmp>:

int
strncmp(const char *p, const char *q, uint n)
{
80106ded:	55                   	push   %ebp
80106dee:	89 e5                	mov    %esp,%ebp
  while(n > 0 && *p && *p == *q)
80106df0:	eb 0c                	jmp    80106dfe <strncmp+0x11>
    n--, p++, q++;
80106df2:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
80106df6:	83 45 08 01          	addl   $0x1,0x8(%ebp)
80106dfa:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strncmp(const char *p, const char *q, uint n)
{
  while(n > 0 && *p && *p == *q)
80106dfe:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80106e02:	74 1a                	je     80106e1e <strncmp+0x31>
80106e04:	8b 45 08             	mov    0x8(%ebp),%eax
80106e07:	0f b6 00             	movzbl (%eax),%eax
80106e0a:	84 c0                	test   %al,%al
80106e0c:	74 10                	je     80106e1e <strncmp+0x31>
80106e0e:	8b 45 08             	mov    0x8(%ebp),%eax
80106e11:	0f b6 10             	movzbl (%eax),%edx
80106e14:	8b 45 0c             	mov    0xc(%ebp),%eax
80106e17:	0f b6 00             	movzbl (%eax),%eax
80106e1a:	38 c2                	cmp    %al,%dl
80106e1c:	74 d4                	je     80106df2 <strncmp+0x5>
    n--, p++, q++;
  if(n == 0)
80106e1e:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80106e22:	75 07                	jne    80106e2b <strncmp+0x3e>
    return 0;
80106e24:	b8 00 00 00 00       	mov    $0x0,%eax
80106e29:	eb 16                	jmp    80106e41 <strncmp+0x54>
  return (uchar)*p - (uchar)*q;
80106e2b:	8b 45 08             	mov    0x8(%ebp),%eax
80106e2e:	0f b6 00             	movzbl (%eax),%eax
80106e31:	0f b6 d0             	movzbl %al,%edx
80106e34:	8b 45 0c             	mov    0xc(%ebp),%eax
80106e37:	0f b6 00             	movzbl (%eax),%eax
80106e3a:	0f b6 c0             	movzbl %al,%eax
80106e3d:	29 c2                	sub    %eax,%edx
80106e3f:	89 d0                	mov    %edx,%eax
}
80106e41:	5d                   	pop    %ebp
80106e42:	c3                   	ret    

80106e43 <strncpy>:

char*
strncpy(char *s, const char *t, int n)
{
80106e43:	55                   	push   %ebp
80106e44:	89 e5                	mov    %esp,%ebp
80106e46:	83 ec 10             	sub    $0x10,%esp
  char *os;
  
  os = s;
80106e49:	8b 45 08             	mov    0x8(%ebp),%eax
80106e4c:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while(n-- > 0 && (*s++ = *t++) != 0)
80106e4f:	90                   	nop
80106e50:	8b 45 10             	mov    0x10(%ebp),%eax
80106e53:	8d 50 ff             	lea    -0x1(%eax),%edx
80106e56:	89 55 10             	mov    %edx,0x10(%ebp)
80106e59:	85 c0                	test   %eax,%eax
80106e5b:	7e 2c                	jle    80106e89 <strncpy+0x46>
80106e5d:	8b 45 08             	mov    0x8(%ebp),%eax
80106e60:	8d 50 01             	lea    0x1(%eax),%edx
80106e63:	89 55 08             	mov    %edx,0x8(%ebp)
80106e66:	8b 55 0c             	mov    0xc(%ebp),%edx
80106e69:	8d 4a 01             	lea    0x1(%edx),%ecx
80106e6c:	89 4d 0c             	mov    %ecx,0xc(%ebp)
80106e6f:	0f b6 12             	movzbl (%edx),%edx
80106e72:	88 10                	mov    %dl,(%eax)
80106e74:	0f b6 00             	movzbl (%eax),%eax
80106e77:	84 c0                	test   %al,%al
80106e79:	75 d5                	jne    80106e50 <strncpy+0xd>
    ;
  while(n-- > 0)
80106e7b:	eb 0c                	jmp    80106e89 <strncpy+0x46>
    *s++ = 0;
80106e7d:	8b 45 08             	mov    0x8(%ebp),%eax
80106e80:	8d 50 01             	lea    0x1(%eax),%edx
80106e83:	89 55 08             	mov    %edx,0x8(%ebp)
80106e86:	c6 00 00             	movb   $0x0,(%eax)
  char *os;
  
  os = s;
  while(n-- > 0 && (*s++ = *t++) != 0)
    ;
  while(n-- > 0)
80106e89:	8b 45 10             	mov    0x10(%ebp),%eax
80106e8c:	8d 50 ff             	lea    -0x1(%eax),%edx
80106e8f:	89 55 10             	mov    %edx,0x10(%ebp)
80106e92:	85 c0                	test   %eax,%eax
80106e94:	7f e7                	jg     80106e7d <strncpy+0x3a>
    *s++ = 0;
  return os;
80106e96:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80106e99:	c9                   	leave  
80106e9a:	c3                   	ret    

80106e9b <safestrcpy>:

// Like strncpy but guaranteed to NUL-terminate.
char*
safestrcpy(char *s, const char *t, int n)
{
80106e9b:	55                   	push   %ebp
80106e9c:	89 e5                	mov    %esp,%ebp
80106e9e:	83 ec 10             	sub    $0x10,%esp
  char *os;
  
  os = s;
80106ea1:	8b 45 08             	mov    0x8(%ebp),%eax
80106ea4:	89 45 fc             	mov    %eax,-0x4(%ebp)
  if(n <= 0)
80106ea7:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80106eab:	7f 05                	jg     80106eb2 <safestrcpy+0x17>
    return os;
80106ead:	8b 45 fc             	mov    -0x4(%ebp),%eax
80106eb0:	eb 31                	jmp    80106ee3 <safestrcpy+0x48>
  while(--n > 0 && (*s++ = *t++) != 0)
80106eb2:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
80106eb6:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80106eba:	7e 1e                	jle    80106eda <safestrcpy+0x3f>
80106ebc:	8b 45 08             	mov    0x8(%ebp),%eax
80106ebf:	8d 50 01             	lea    0x1(%eax),%edx
80106ec2:	89 55 08             	mov    %edx,0x8(%ebp)
80106ec5:	8b 55 0c             	mov    0xc(%ebp),%edx
80106ec8:	8d 4a 01             	lea    0x1(%edx),%ecx
80106ecb:	89 4d 0c             	mov    %ecx,0xc(%ebp)
80106ece:	0f b6 12             	movzbl (%edx),%edx
80106ed1:	88 10                	mov    %dl,(%eax)
80106ed3:	0f b6 00             	movzbl (%eax),%eax
80106ed6:	84 c0                	test   %al,%al
80106ed8:	75 d8                	jne    80106eb2 <safestrcpy+0x17>
    ;
  *s = 0;
80106eda:	8b 45 08             	mov    0x8(%ebp),%eax
80106edd:	c6 00 00             	movb   $0x0,(%eax)
  return os;
80106ee0:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80106ee3:	c9                   	leave  
80106ee4:	c3                   	ret    

80106ee5 <strlen>:

int
strlen(const char *s)
{
80106ee5:	55                   	push   %ebp
80106ee6:	89 e5                	mov    %esp,%ebp
80106ee8:	83 ec 10             	sub    $0x10,%esp
  int n;

  for(n = 0; s[n]; n++)
80106eeb:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
80106ef2:	eb 04                	jmp    80106ef8 <strlen+0x13>
80106ef4:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
80106ef8:	8b 55 fc             	mov    -0x4(%ebp),%edx
80106efb:	8b 45 08             	mov    0x8(%ebp),%eax
80106efe:	01 d0                	add    %edx,%eax
80106f00:	0f b6 00             	movzbl (%eax),%eax
80106f03:	84 c0                	test   %al,%al
80106f05:	75 ed                	jne    80106ef4 <strlen+0xf>
    ;
  return n;
80106f07:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80106f0a:	c9                   	leave  
80106f0b:	c3                   	ret    

80106f0c <swtch>:
# Save current register context in old
# and then load register context from new.

.globl swtch
swtch:
  movl 4(%esp), %eax
80106f0c:	8b 44 24 04          	mov    0x4(%esp),%eax
  movl 8(%esp), %edx
80106f10:	8b 54 24 08          	mov    0x8(%esp),%edx

  # Save old callee-save registers
  pushl %ebp
80106f14:	55                   	push   %ebp
  pushl %ebx
80106f15:	53                   	push   %ebx
  pushl %esi
80106f16:	56                   	push   %esi
  pushl %edi
80106f17:	57                   	push   %edi

  # Switch stacks
  movl %esp, (%eax)
80106f18:	89 20                	mov    %esp,(%eax)
  movl %edx, %esp
80106f1a:	89 d4                	mov    %edx,%esp

  # Load new callee-save registers
  popl %edi
80106f1c:	5f                   	pop    %edi
  popl %esi
80106f1d:	5e                   	pop    %esi
  popl %ebx
80106f1e:	5b                   	pop    %ebx
  popl %ebp
80106f1f:	5d                   	pop    %ebp
  ret
80106f20:	c3                   	ret    

80106f21 <fetchint>:
// to a saved program counter, and then the first argument.

// Fetch the int at addr from the current process.
int
fetchint(uint addr, int *ip)
{
80106f21:	55                   	push   %ebp
80106f22:	89 e5                	mov    %esp,%ebp
  if(addr >= proc->sz || addr+4 > proc->sz)
80106f24:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106f2a:	8b 00                	mov    (%eax),%eax
80106f2c:	3b 45 08             	cmp    0x8(%ebp),%eax
80106f2f:	76 12                	jbe    80106f43 <fetchint+0x22>
80106f31:	8b 45 08             	mov    0x8(%ebp),%eax
80106f34:	8d 50 04             	lea    0x4(%eax),%edx
80106f37:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106f3d:	8b 00                	mov    (%eax),%eax
80106f3f:	39 c2                	cmp    %eax,%edx
80106f41:	76 07                	jbe    80106f4a <fetchint+0x29>
    return -1;
80106f43:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106f48:	eb 0f                	jmp    80106f59 <fetchint+0x38>
  *ip = *(int*)(addr);
80106f4a:	8b 45 08             	mov    0x8(%ebp),%eax
80106f4d:	8b 10                	mov    (%eax),%edx
80106f4f:	8b 45 0c             	mov    0xc(%ebp),%eax
80106f52:	89 10                	mov    %edx,(%eax)
  return 0;
80106f54:	b8 00 00 00 00       	mov    $0x0,%eax
}
80106f59:	5d                   	pop    %ebp
80106f5a:	c3                   	ret    

80106f5b <fetchstr>:
// Fetch the nul-terminated string at addr from the current process.
// Doesn't actually copy the string - just sets *pp to point at it.
// Returns length of string, not including nul.
int
fetchstr(uint addr, char **pp)
{
80106f5b:	55                   	push   %ebp
80106f5c:	89 e5                	mov    %esp,%ebp
80106f5e:	83 ec 10             	sub    $0x10,%esp
  char *s, *ep;

  if(addr >= proc->sz)
80106f61:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106f67:	8b 00                	mov    (%eax),%eax
80106f69:	3b 45 08             	cmp    0x8(%ebp),%eax
80106f6c:	77 07                	ja     80106f75 <fetchstr+0x1a>
    return -1;
80106f6e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106f73:	eb 46                	jmp    80106fbb <fetchstr+0x60>
  *pp = (char*)addr;
80106f75:	8b 55 08             	mov    0x8(%ebp),%edx
80106f78:	8b 45 0c             	mov    0xc(%ebp),%eax
80106f7b:	89 10                	mov    %edx,(%eax)
  ep = (char*)proc->sz;
80106f7d:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106f83:	8b 00                	mov    (%eax),%eax
80106f85:	89 45 f8             	mov    %eax,-0x8(%ebp)
  for(s = *pp; s < ep; s++)
80106f88:	8b 45 0c             	mov    0xc(%ebp),%eax
80106f8b:	8b 00                	mov    (%eax),%eax
80106f8d:	89 45 fc             	mov    %eax,-0x4(%ebp)
80106f90:	eb 1c                	jmp    80106fae <fetchstr+0x53>
    if(*s == 0)
80106f92:	8b 45 fc             	mov    -0x4(%ebp),%eax
80106f95:	0f b6 00             	movzbl (%eax),%eax
80106f98:	84 c0                	test   %al,%al
80106f9a:	75 0e                	jne    80106faa <fetchstr+0x4f>
      return s - *pp;
80106f9c:	8b 55 fc             	mov    -0x4(%ebp),%edx
80106f9f:	8b 45 0c             	mov    0xc(%ebp),%eax
80106fa2:	8b 00                	mov    (%eax),%eax
80106fa4:	29 c2                	sub    %eax,%edx
80106fa6:	89 d0                	mov    %edx,%eax
80106fa8:	eb 11                	jmp    80106fbb <fetchstr+0x60>

  if(addr >= proc->sz)
    return -1;
  *pp = (char*)addr;
  ep = (char*)proc->sz;
  for(s = *pp; s < ep; s++)
80106faa:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
80106fae:	8b 45 fc             	mov    -0x4(%ebp),%eax
80106fb1:	3b 45 f8             	cmp    -0x8(%ebp),%eax
80106fb4:	72 dc                	jb     80106f92 <fetchstr+0x37>
    if(*s == 0)
      return s - *pp;
  return -1;
80106fb6:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80106fbb:	c9                   	leave  
80106fbc:	c3                   	ret    

80106fbd <argint>:

// Fetch the nth 32-bit system call argument.
int
argint(int n, int *ip)
{
80106fbd:	55                   	push   %ebp
80106fbe:	89 e5                	mov    %esp,%ebp
  return fetchint(proc->tf->esp + 4 + 4*n, ip);
80106fc0:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106fc6:	8b 40 20             	mov    0x20(%eax),%eax
80106fc9:	8b 40 44             	mov    0x44(%eax),%eax
80106fcc:	8b 55 08             	mov    0x8(%ebp),%edx
80106fcf:	c1 e2 02             	shl    $0x2,%edx
80106fd2:	01 d0                	add    %edx,%eax
80106fd4:	83 c0 04             	add    $0x4,%eax
80106fd7:	ff 75 0c             	pushl  0xc(%ebp)
80106fda:	50                   	push   %eax
80106fdb:	e8 41 ff ff ff       	call   80106f21 <fetchint>
80106fe0:	83 c4 08             	add    $0x8,%esp
}
80106fe3:	c9                   	leave  
80106fe4:	c3                   	ret    

80106fe5 <argptr>:
// Fetch the nth word-sized system call argument as a pointer
// to a block of memory of size n bytes.  Check that the pointer
// lies within the process address space.
int
argptr(int n, char **pp, int size)
{
80106fe5:	55                   	push   %ebp
80106fe6:	89 e5                	mov    %esp,%ebp
80106fe8:	83 ec 10             	sub    $0x10,%esp
  int i;
  
  if(argint(n, &i) < 0)
80106feb:	8d 45 fc             	lea    -0x4(%ebp),%eax
80106fee:	50                   	push   %eax
80106fef:	ff 75 08             	pushl  0x8(%ebp)
80106ff2:	e8 c6 ff ff ff       	call   80106fbd <argint>
80106ff7:	83 c4 08             	add    $0x8,%esp
80106ffa:	85 c0                	test   %eax,%eax
80106ffc:	79 07                	jns    80107005 <argptr+0x20>
    return -1;
80106ffe:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80107003:	eb 3b                	jmp    80107040 <argptr+0x5b>
  if((uint)i >= proc->sz || (uint)i+size > proc->sz)
80107005:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010700b:	8b 00                	mov    (%eax),%eax
8010700d:	8b 55 fc             	mov    -0x4(%ebp),%edx
80107010:	39 d0                	cmp    %edx,%eax
80107012:	76 16                	jbe    8010702a <argptr+0x45>
80107014:	8b 45 fc             	mov    -0x4(%ebp),%eax
80107017:	89 c2                	mov    %eax,%edx
80107019:	8b 45 10             	mov    0x10(%ebp),%eax
8010701c:	01 c2                	add    %eax,%edx
8010701e:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80107024:	8b 00                	mov    (%eax),%eax
80107026:	39 c2                	cmp    %eax,%edx
80107028:	76 07                	jbe    80107031 <argptr+0x4c>
    return -1;
8010702a:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010702f:	eb 0f                	jmp    80107040 <argptr+0x5b>
  *pp = (char*)i;
80107031:	8b 45 fc             	mov    -0x4(%ebp),%eax
80107034:	89 c2                	mov    %eax,%edx
80107036:	8b 45 0c             	mov    0xc(%ebp),%eax
80107039:	89 10                	mov    %edx,(%eax)
  return 0;
8010703b:	b8 00 00 00 00       	mov    $0x0,%eax
}
80107040:	c9                   	leave  
80107041:	c3                   	ret    

80107042 <argstr>:
// Check that the pointer is valid and the string is nul-terminated.
// (There is no shared writable memory, so the string can't change
// between this check and being used by the kernel.)
int
argstr(int n, char **pp)
{
80107042:	55                   	push   %ebp
80107043:	89 e5                	mov    %esp,%ebp
80107045:	83 ec 10             	sub    $0x10,%esp
  int addr;
  if(argint(n, &addr) < 0)
80107048:	8d 45 fc             	lea    -0x4(%ebp),%eax
8010704b:	50                   	push   %eax
8010704c:	ff 75 08             	pushl  0x8(%ebp)
8010704f:	e8 69 ff ff ff       	call   80106fbd <argint>
80107054:	83 c4 08             	add    $0x8,%esp
80107057:	85 c0                	test   %eax,%eax
80107059:	79 07                	jns    80107062 <argstr+0x20>
    return -1;
8010705b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80107060:	eb 0f                	jmp    80107071 <argstr+0x2f>
  return fetchstr(addr, pp);
80107062:	8b 45 fc             	mov    -0x4(%ebp),%eax
80107065:	ff 75 0c             	pushl  0xc(%ebp)
80107068:	50                   	push   %eax
80107069:	e8 ed fe ff ff       	call   80106f5b <fetchstr>
8010706e:	83 c4 08             	add    $0x8,%esp
}
80107071:	c9                   	leave  
80107072:	c3                   	ret    

80107073 <syscall>:
};
#endif

void
syscall(void)
{
80107073:	55                   	push   %ebp
80107074:	89 e5                	mov    %esp,%ebp
80107076:	53                   	push   %ebx
80107077:	83 ec 14             	sub    $0x14,%esp
  int num;

  num = proc->tf->eax;
8010707a:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80107080:	8b 40 20             	mov    0x20(%eax),%eax
80107083:	8b 40 1c             	mov    0x1c(%eax),%eax
80107086:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(num > 0 && num < NELEM(syscalls) && syscalls[num]) {
80107089:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
8010708d:	7e 30                	jle    801070bf <syscall+0x4c>
8010708f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107092:	83 f8 1e             	cmp    $0x1e,%eax
80107095:	77 28                	ja     801070bf <syscall+0x4c>
80107097:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010709a:	8b 04 85 40 d0 10 80 	mov    -0x7fef2fc0(,%eax,4),%eax
801070a1:	85 c0                	test   %eax,%eax
801070a3:	74 1a                	je     801070bf <syscall+0x4c>
    proc->tf->eax = syscalls[num]();
801070a5:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801070ab:	8b 58 20             	mov    0x20(%eax),%ebx
801070ae:	8b 45 f4             	mov    -0xc(%ebp),%eax
801070b1:	8b 04 85 40 d0 10 80 	mov    -0x7fef2fc0(,%eax,4),%eax
801070b8:	ff d0                	call   *%eax
801070ba:	89 43 1c             	mov    %eax,0x1c(%ebx)
801070bd:	eb 34                	jmp    801070f3 <syscall+0x80>
  // some P1 code goes here
  cprintf("%s->%d\n", syscallzz[num], proc->tf->eax);
  #endif
  } else {
    cprintf("%d %s: unknown sys call %d\n",
            proc->pid, proc->name, num);
801070bf:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801070c5:	8d 50 74             	lea    0x74(%eax),%edx
801070c8:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  #ifdef PRINT_SYSCALLS
  // some P1 code goes here
  cprintf("%s->%d\n", syscallzz[num], proc->tf->eax);
  #endif
  } else {
    cprintf("%d %s: unknown sys call %d\n",
801070ce:	8b 40 10             	mov    0x10(%eax),%eax
801070d1:	ff 75 f4             	pushl  -0xc(%ebp)
801070d4:	52                   	push   %edx
801070d5:	50                   	push   %eax
801070d6:	68 eb ac 10 80       	push   $0x8010aceb
801070db:	e8 e6 92 ff ff       	call   801003c6 <cprintf>
801070e0:	83 c4 10             	add    $0x10,%esp
            proc->pid, proc->name, num);
    proc->tf->eax = -1;
801070e3:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801070e9:	8b 40 20             	mov    0x20(%eax),%eax
801070ec:	c7 40 1c ff ff ff ff 	movl   $0xffffffff,0x1c(%eax)
  }
}
801070f3:	90                   	nop
801070f4:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801070f7:	c9                   	leave  
801070f8:	c3                   	ret    

801070f9 <argfd>:

// Fetch the nth word-sized system call argument as a file descriptor
// and return both the descriptor and the corresponding struct file.
static int
argfd(int n, int *pfd, struct file **pf)
{
801070f9:	55                   	push   %ebp
801070fa:	89 e5                	mov    %esp,%ebp
801070fc:	83 ec 18             	sub    $0x18,%esp
  int fd;
  struct file *f;

  if(argint(n, &fd) < 0)
801070ff:	83 ec 08             	sub    $0x8,%esp
80107102:	8d 45 f0             	lea    -0x10(%ebp),%eax
80107105:	50                   	push   %eax
80107106:	ff 75 08             	pushl  0x8(%ebp)
80107109:	e8 af fe ff ff       	call   80106fbd <argint>
8010710e:	83 c4 10             	add    $0x10,%esp
80107111:	85 c0                	test   %eax,%eax
80107113:	79 07                	jns    8010711c <argfd+0x23>
    return -1;
80107115:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010711a:	eb 4f                	jmp    8010716b <argfd+0x72>
  if(fd < 0 || fd >= NOFILE || (f=proc->ofile[fd]) == 0)
8010711c:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010711f:	85 c0                	test   %eax,%eax
80107121:	78 20                	js     80107143 <argfd+0x4a>
80107123:	8b 45 f0             	mov    -0x10(%ebp),%eax
80107126:	83 f8 0f             	cmp    $0xf,%eax
80107129:	7f 18                	jg     80107143 <argfd+0x4a>
8010712b:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80107131:	8b 55 f0             	mov    -0x10(%ebp),%edx
80107134:	83 c2 0c             	add    $0xc,%edx
80107137:	8b 04 90             	mov    (%eax,%edx,4),%eax
8010713a:	89 45 f4             	mov    %eax,-0xc(%ebp)
8010713d:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80107141:	75 07                	jne    8010714a <argfd+0x51>
    return -1;
80107143:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80107148:	eb 21                	jmp    8010716b <argfd+0x72>
  if(pfd)
8010714a:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
8010714e:	74 08                	je     80107158 <argfd+0x5f>
    *pfd = fd;
80107150:	8b 55 f0             	mov    -0x10(%ebp),%edx
80107153:	8b 45 0c             	mov    0xc(%ebp),%eax
80107156:	89 10                	mov    %edx,(%eax)
  if(pf)
80107158:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
8010715c:	74 08                	je     80107166 <argfd+0x6d>
    *pf = f;
8010715e:	8b 45 10             	mov    0x10(%ebp),%eax
80107161:	8b 55 f4             	mov    -0xc(%ebp),%edx
80107164:	89 10                	mov    %edx,(%eax)
  return 0;
80107166:	b8 00 00 00 00       	mov    $0x0,%eax
}
8010716b:	c9                   	leave  
8010716c:	c3                   	ret    

8010716d <fdalloc>:

// Allocate a file descriptor for the given file.
// Takes over file reference from caller on success.
static int
fdalloc(struct file *f)
{
8010716d:	55                   	push   %ebp
8010716e:	89 e5                	mov    %esp,%ebp
80107170:	83 ec 10             	sub    $0x10,%esp
  int fd;

  for(fd = 0; fd < NOFILE; fd++){
80107173:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
8010717a:	eb 2e                	jmp    801071aa <fdalloc+0x3d>
    if(proc->ofile[fd] == 0){
8010717c:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80107182:	8b 55 fc             	mov    -0x4(%ebp),%edx
80107185:	83 c2 0c             	add    $0xc,%edx
80107188:	8b 04 90             	mov    (%eax,%edx,4),%eax
8010718b:	85 c0                	test   %eax,%eax
8010718d:	75 17                	jne    801071a6 <fdalloc+0x39>
      proc->ofile[fd] = f;
8010718f:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80107195:	8b 55 fc             	mov    -0x4(%ebp),%edx
80107198:	8d 4a 0c             	lea    0xc(%edx),%ecx
8010719b:	8b 55 08             	mov    0x8(%ebp),%edx
8010719e:	89 14 88             	mov    %edx,(%eax,%ecx,4)
      return fd;
801071a1:	8b 45 fc             	mov    -0x4(%ebp),%eax
801071a4:	eb 0f                	jmp    801071b5 <fdalloc+0x48>
static int
fdalloc(struct file *f)
{
  int fd;

  for(fd = 0; fd < NOFILE; fd++){
801071a6:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
801071aa:	83 7d fc 0f          	cmpl   $0xf,-0x4(%ebp)
801071ae:	7e cc                	jle    8010717c <fdalloc+0xf>
    if(proc->ofile[fd] == 0){
      proc->ofile[fd] = f;
      return fd;
    }
  }
  return -1;
801071b0:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
801071b5:	c9                   	leave  
801071b6:	c3                   	ret    

801071b7 <sys_dup>:

int
sys_dup(void)
{
801071b7:	55                   	push   %ebp
801071b8:	89 e5                	mov    %esp,%ebp
801071ba:	83 ec 18             	sub    $0x18,%esp
  struct file *f;
  int fd;
  
  if(argfd(0, 0, &f) < 0)
801071bd:	83 ec 04             	sub    $0x4,%esp
801071c0:	8d 45 f0             	lea    -0x10(%ebp),%eax
801071c3:	50                   	push   %eax
801071c4:	6a 00                	push   $0x0
801071c6:	6a 00                	push   $0x0
801071c8:	e8 2c ff ff ff       	call   801070f9 <argfd>
801071cd:	83 c4 10             	add    $0x10,%esp
801071d0:	85 c0                	test   %eax,%eax
801071d2:	79 07                	jns    801071db <sys_dup+0x24>
    return -1;
801071d4:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801071d9:	eb 31                	jmp    8010720c <sys_dup+0x55>
  if((fd=fdalloc(f)) < 0)
801071db:	8b 45 f0             	mov    -0x10(%ebp),%eax
801071de:	83 ec 0c             	sub    $0xc,%esp
801071e1:	50                   	push   %eax
801071e2:	e8 86 ff ff ff       	call   8010716d <fdalloc>
801071e7:	83 c4 10             	add    $0x10,%esp
801071ea:	89 45 f4             	mov    %eax,-0xc(%ebp)
801071ed:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801071f1:	79 07                	jns    801071fa <sys_dup+0x43>
    return -1;
801071f3:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801071f8:	eb 12                	jmp    8010720c <sys_dup+0x55>
  filedup(f);
801071fa:	8b 45 f0             	mov    -0x10(%ebp),%eax
801071fd:	83 ec 0c             	sub    $0xc,%esp
80107200:	50                   	push   %eax
80107201:	e8 98 9e ff ff       	call   8010109e <filedup>
80107206:	83 c4 10             	add    $0x10,%esp
  return fd;
80107209:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
8010720c:	c9                   	leave  
8010720d:	c3                   	ret    

8010720e <sys_read>:

int
sys_read(void)
{
8010720e:	55                   	push   %ebp
8010720f:	89 e5                	mov    %esp,%ebp
80107211:	83 ec 18             	sub    $0x18,%esp
  struct file *f;
  int n;
  char *p;

  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
80107214:	83 ec 04             	sub    $0x4,%esp
80107217:	8d 45 f4             	lea    -0xc(%ebp),%eax
8010721a:	50                   	push   %eax
8010721b:	6a 00                	push   $0x0
8010721d:	6a 00                	push   $0x0
8010721f:	e8 d5 fe ff ff       	call   801070f9 <argfd>
80107224:	83 c4 10             	add    $0x10,%esp
80107227:	85 c0                	test   %eax,%eax
80107229:	78 2e                	js     80107259 <sys_read+0x4b>
8010722b:	83 ec 08             	sub    $0x8,%esp
8010722e:	8d 45 f0             	lea    -0x10(%ebp),%eax
80107231:	50                   	push   %eax
80107232:	6a 02                	push   $0x2
80107234:	e8 84 fd ff ff       	call   80106fbd <argint>
80107239:	83 c4 10             	add    $0x10,%esp
8010723c:	85 c0                	test   %eax,%eax
8010723e:	78 19                	js     80107259 <sys_read+0x4b>
80107240:	8b 45 f0             	mov    -0x10(%ebp),%eax
80107243:	83 ec 04             	sub    $0x4,%esp
80107246:	50                   	push   %eax
80107247:	8d 45 ec             	lea    -0x14(%ebp),%eax
8010724a:	50                   	push   %eax
8010724b:	6a 01                	push   $0x1
8010724d:	e8 93 fd ff ff       	call   80106fe5 <argptr>
80107252:	83 c4 10             	add    $0x10,%esp
80107255:	85 c0                	test   %eax,%eax
80107257:	79 07                	jns    80107260 <sys_read+0x52>
    return -1;
80107259:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010725e:	eb 17                	jmp    80107277 <sys_read+0x69>
  return fileread(f, p, n);
80107260:	8b 4d f0             	mov    -0x10(%ebp),%ecx
80107263:	8b 55 ec             	mov    -0x14(%ebp),%edx
80107266:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107269:	83 ec 04             	sub    $0x4,%esp
8010726c:	51                   	push   %ecx
8010726d:	52                   	push   %edx
8010726e:	50                   	push   %eax
8010726f:	e8 ba 9f ff ff       	call   8010122e <fileread>
80107274:	83 c4 10             	add    $0x10,%esp
}
80107277:	c9                   	leave  
80107278:	c3                   	ret    

80107279 <sys_write>:

int
sys_write(void)
{
80107279:	55                   	push   %ebp
8010727a:	89 e5                	mov    %esp,%ebp
8010727c:	83 ec 18             	sub    $0x18,%esp
  struct file *f;
  int n;
  char *p;

  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
8010727f:	83 ec 04             	sub    $0x4,%esp
80107282:	8d 45 f4             	lea    -0xc(%ebp),%eax
80107285:	50                   	push   %eax
80107286:	6a 00                	push   $0x0
80107288:	6a 00                	push   $0x0
8010728a:	e8 6a fe ff ff       	call   801070f9 <argfd>
8010728f:	83 c4 10             	add    $0x10,%esp
80107292:	85 c0                	test   %eax,%eax
80107294:	78 2e                	js     801072c4 <sys_write+0x4b>
80107296:	83 ec 08             	sub    $0x8,%esp
80107299:	8d 45 f0             	lea    -0x10(%ebp),%eax
8010729c:	50                   	push   %eax
8010729d:	6a 02                	push   $0x2
8010729f:	e8 19 fd ff ff       	call   80106fbd <argint>
801072a4:	83 c4 10             	add    $0x10,%esp
801072a7:	85 c0                	test   %eax,%eax
801072a9:	78 19                	js     801072c4 <sys_write+0x4b>
801072ab:	8b 45 f0             	mov    -0x10(%ebp),%eax
801072ae:	83 ec 04             	sub    $0x4,%esp
801072b1:	50                   	push   %eax
801072b2:	8d 45 ec             	lea    -0x14(%ebp),%eax
801072b5:	50                   	push   %eax
801072b6:	6a 01                	push   $0x1
801072b8:	e8 28 fd ff ff       	call   80106fe5 <argptr>
801072bd:	83 c4 10             	add    $0x10,%esp
801072c0:	85 c0                	test   %eax,%eax
801072c2:	79 07                	jns    801072cb <sys_write+0x52>
    return -1;
801072c4:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801072c9:	eb 17                	jmp    801072e2 <sys_write+0x69>
  return filewrite(f, p, n);
801072cb:	8b 4d f0             	mov    -0x10(%ebp),%ecx
801072ce:	8b 55 ec             	mov    -0x14(%ebp),%edx
801072d1:	8b 45 f4             	mov    -0xc(%ebp),%eax
801072d4:	83 ec 04             	sub    $0x4,%esp
801072d7:	51                   	push   %ecx
801072d8:	52                   	push   %edx
801072d9:	50                   	push   %eax
801072da:	e8 07 a0 ff ff       	call   801012e6 <filewrite>
801072df:	83 c4 10             	add    $0x10,%esp
}
801072e2:	c9                   	leave  
801072e3:	c3                   	ret    

801072e4 <sys_close>:

int
sys_close(void)
{
801072e4:	55                   	push   %ebp
801072e5:	89 e5                	mov    %esp,%ebp
801072e7:	83 ec 18             	sub    $0x18,%esp
  int fd;
  struct file *f;
  
  if(argfd(0, &fd, &f) < 0)
801072ea:	83 ec 04             	sub    $0x4,%esp
801072ed:	8d 45 f0             	lea    -0x10(%ebp),%eax
801072f0:	50                   	push   %eax
801072f1:	8d 45 f4             	lea    -0xc(%ebp),%eax
801072f4:	50                   	push   %eax
801072f5:	6a 00                	push   $0x0
801072f7:	e8 fd fd ff ff       	call   801070f9 <argfd>
801072fc:	83 c4 10             	add    $0x10,%esp
801072ff:	85 c0                	test   %eax,%eax
80107301:	79 07                	jns    8010730a <sys_close+0x26>
    return -1;
80107303:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80107308:	eb 27                	jmp    80107331 <sys_close+0x4d>
  proc->ofile[fd] = 0;
8010730a:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80107310:	8b 55 f4             	mov    -0xc(%ebp),%edx
80107313:	83 c2 0c             	add    $0xc,%edx
80107316:	c7 04 90 00 00 00 00 	movl   $0x0,(%eax,%edx,4)
  fileclose(f);
8010731d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80107320:	83 ec 0c             	sub    $0xc,%esp
80107323:	50                   	push   %eax
80107324:	e8 c6 9d ff ff       	call   801010ef <fileclose>
80107329:	83 c4 10             	add    $0x10,%esp
  return 0;
8010732c:	b8 00 00 00 00       	mov    $0x0,%eax
}
80107331:	c9                   	leave  
80107332:	c3                   	ret    

80107333 <sys_fstat>:

int
sys_fstat(void)
{
80107333:	55                   	push   %ebp
80107334:	89 e5                	mov    %esp,%ebp
80107336:	83 ec 18             	sub    $0x18,%esp
  struct file *f;
  struct stat *st;
  
  if(argfd(0, 0, &f) < 0 || argptr(1, (void*)&st, sizeof(*st)) < 0)
80107339:	83 ec 04             	sub    $0x4,%esp
8010733c:	8d 45 f4             	lea    -0xc(%ebp),%eax
8010733f:	50                   	push   %eax
80107340:	6a 00                	push   $0x0
80107342:	6a 00                	push   $0x0
80107344:	e8 b0 fd ff ff       	call   801070f9 <argfd>
80107349:	83 c4 10             	add    $0x10,%esp
8010734c:	85 c0                	test   %eax,%eax
8010734e:	78 17                	js     80107367 <sys_fstat+0x34>
80107350:	83 ec 04             	sub    $0x4,%esp
80107353:	6a 14                	push   $0x14
80107355:	8d 45 f0             	lea    -0x10(%ebp),%eax
80107358:	50                   	push   %eax
80107359:	6a 01                	push   $0x1
8010735b:	e8 85 fc ff ff       	call   80106fe5 <argptr>
80107360:	83 c4 10             	add    $0x10,%esp
80107363:	85 c0                	test   %eax,%eax
80107365:	79 07                	jns    8010736e <sys_fstat+0x3b>
    return -1;
80107367:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010736c:	eb 13                	jmp    80107381 <sys_fstat+0x4e>
  return filestat(f, st);
8010736e:	8b 55 f0             	mov    -0x10(%ebp),%edx
80107371:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107374:	83 ec 08             	sub    $0x8,%esp
80107377:	52                   	push   %edx
80107378:	50                   	push   %eax
80107379:	e8 59 9e ff ff       	call   801011d7 <filestat>
8010737e:	83 c4 10             	add    $0x10,%esp
}
80107381:	c9                   	leave  
80107382:	c3                   	ret    

80107383 <sys_link>:

// Create the path new as a link to the same inode as old.
int
sys_link(void)
{
80107383:	55                   	push   %ebp
80107384:	89 e5                	mov    %esp,%ebp
80107386:	83 ec 28             	sub    $0x28,%esp
  char name[DIRSIZ], *new, *old;
  struct inode *dp, *ip;

  if(argstr(0, &old) < 0 || argstr(1, &new) < 0)
80107389:	83 ec 08             	sub    $0x8,%esp
8010738c:	8d 45 d8             	lea    -0x28(%ebp),%eax
8010738f:	50                   	push   %eax
80107390:	6a 00                	push   $0x0
80107392:	e8 ab fc ff ff       	call   80107042 <argstr>
80107397:	83 c4 10             	add    $0x10,%esp
8010739a:	85 c0                	test   %eax,%eax
8010739c:	78 15                	js     801073b3 <sys_link+0x30>
8010739e:	83 ec 08             	sub    $0x8,%esp
801073a1:	8d 45 dc             	lea    -0x24(%ebp),%eax
801073a4:	50                   	push   %eax
801073a5:	6a 01                	push   $0x1
801073a7:	e8 96 fc ff ff       	call   80107042 <argstr>
801073ac:	83 c4 10             	add    $0x10,%esp
801073af:	85 c0                	test   %eax,%eax
801073b1:	79 0a                	jns    801073bd <sys_link+0x3a>
    return -1;
801073b3:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801073b8:	e9 68 01 00 00       	jmp    80107525 <sys_link+0x1a2>

  begin_op();
801073bd:	e8 29 c2 ff ff       	call   801035eb <begin_op>
  if((ip = namei(old)) == 0){
801073c2:	8b 45 d8             	mov    -0x28(%ebp),%eax
801073c5:	83 ec 0c             	sub    $0xc,%esp
801073c8:	50                   	push   %eax
801073c9:	e8 f8 b1 ff ff       	call   801025c6 <namei>
801073ce:	83 c4 10             	add    $0x10,%esp
801073d1:	89 45 f4             	mov    %eax,-0xc(%ebp)
801073d4:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801073d8:	75 0f                	jne    801073e9 <sys_link+0x66>
    end_op();
801073da:	e8 98 c2 ff ff       	call   80103677 <end_op>
    return -1;
801073df:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801073e4:	e9 3c 01 00 00       	jmp    80107525 <sys_link+0x1a2>
  }

  ilock(ip);
801073e9:	83 ec 0c             	sub    $0xc,%esp
801073ec:	ff 75 f4             	pushl  -0xc(%ebp)
801073ef:	e8 14 a6 ff ff       	call   80101a08 <ilock>
801073f4:	83 c4 10             	add    $0x10,%esp
  if(ip->type == T_DIR){
801073f7:	8b 45 f4             	mov    -0xc(%ebp),%eax
801073fa:	0f b7 40 10          	movzwl 0x10(%eax),%eax
801073fe:	66 83 f8 01          	cmp    $0x1,%ax
80107402:	75 1d                	jne    80107421 <sys_link+0x9e>
    iunlockput(ip);
80107404:	83 ec 0c             	sub    $0xc,%esp
80107407:	ff 75 f4             	pushl  -0xc(%ebp)
8010740a:	e8 b9 a8 ff ff       	call   80101cc8 <iunlockput>
8010740f:	83 c4 10             	add    $0x10,%esp
    end_op();
80107412:	e8 60 c2 ff ff       	call   80103677 <end_op>
    return -1;
80107417:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010741c:	e9 04 01 00 00       	jmp    80107525 <sys_link+0x1a2>
  }

  ip->nlink++;
80107421:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107424:	0f b7 40 16          	movzwl 0x16(%eax),%eax
80107428:	83 c0 01             	add    $0x1,%eax
8010742b:	89 c2                	mov    %eax,%edx
8010742d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107430:	66 89 50 16          	mov    %dx,0x16(%eax)
  iupdate(ip);
80107434:	83 ec 0c             	sub    $0xc,%esp
80107437:	ff 75 f4             	pushl  -0xc(%ebp)
8010743a:	e8 ef a3 ff ff       	call   8010182e <iupdate>
8010743f:	83 c4 10             	add    $0x10,%esp
  iunlock(ip);
80107442:	83 ec 0c             	sub    $0xc,%esp
80107445:	ff 75 f4             	pushl  -0xc(%ebp)
80107448:	e8 19 a7 ff ff       	call   80101b66 <iunlock>
8010744d:	83 c4 10             	add    $0x10,%esp

  if((dp = nameiparent(new, name)) == 0)
80107450:	8b 45 dc             	mov    -0x24(%ebp),%eax
80107453:	83 ec 08             	sub    $0x8,%esp
80107456:	8d 55 e2             	lea    -0x1e(%ebp),%edx
80107459:	52                   	push   %edx
8010745a:	50                   	push   %eax
8010745b:	e8 82 b1 ff ff       	call   801025e2 <nameiparent>
80107460:	83 c4 10             	add    $0x10,%esp
80107463:	89 45 f0             	mov    %eax,-0x10(%ebp)
80107466:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
8010746a:	74 71                	je     801074dd <sys_link+0x15a>
    goto bad;
  ilock(dp);
8010746c:	83 ec 0c             	sub    $0xc,%esp
8010746f:	ff 75 f0             	pushl  -0x10(%ebp)
80107472:	e8 91 a5 ff ff       	call   80101a08 <ilock>
80107477:	83 c4 10             	add    $0x10,%esp
  if(dp->dev != ip->dev || dirlink(dp, name, ip->inum) < 0){
8010747a:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010747d:	8b 10                	mov    (%eax),%edx
8010747f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107482:	8b 00                	mov    (%eax),%eax
80107484:	39 c2                	cmp    %eax,%edx
80107486:	75 1d                	jne    801074a5 <sys_link+0x122>
80107488:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010748b:	8b 40 04             	mov    0x4(%eax),%eax
8010748e:	83 ec 04             	sub    $0x4,%esp
80107491:	50                   	push   %eax
80107492:	8d 45 e2             	lea    -0x1e(%ebp),%eax
80107495:	50                   	push   %eax
80107496:	ff 75 f0             	pushl  -0x10(%ebp)
80107499:	e8 8c ae ff ff       	call   8010232a <dirlink>
8010749e:	83 c4 10             	add    $0x10,%esp
801074a1:	85 c0                	test   %eax,%eax
801074a3:	79 10                	jns    801074b5 <sys_link+0x132>
    iunlockput(dp);
801074a5:	83 ec 0c             	sub    $0xc,%esp
801074a8:	ff 75 f0             	pushl  -0x10(%ebp)
801074ab:	e8 18 a8 ff ff       	call   80101cc8 <iunlockput>
801074b0:	83 c4 10             	add    $0x10,%esp
    goto bad;
801074b3:	eb 29                	jmp    801074de <sys_link+0x15b>
  }
  iunlockput(dp);
801074b5:	83 ec 0c             	sub    $0xc,%esp
801074b8:	ff 75 f0             	pushl  -0x10(%ebp)
801074bb:	e8 08 a8 ff ff       	call   80101cc8 <iunlockput>
801074c0:	83 c4 10             	add    $0x10,%esp
  iput(ip);
801074c3:	83 ec 0c             	sub    $0xc,%esp
801074c6:	ff 75 f4             	pushl  -0xc(%ebp)
801074c9:	e8 0a a7 ff ff       	call   80101bd8 <iput>
801074ce:	83 c4 10             	add    $0x10,%esp

  end_op();
801074d1:	e8 a1 c1 ff ff       	call   80103677 <end_op>

  return 0;
801074d6:	b8 00 00 00 00       	mov    $0x0,%eax
801074db:	eb 48                	jmp    80107525 <sys_link+0x1a2>
  ip->nlink++;
  iupdate(ip);
  iunlock(ip);

  if((dp = nameiparent(new, name)) == 0)
    goto bad;
801074dd:	90                   	nop
  end_op();

  return 0;

bad:
  ilock(ip);
801074de:	83 ec 0c             	sub    $0xc,%esp
801074e1:	ff 75 f4             	pushl  -0xc(%ebp)
801074e4:	e8 1f a5 ff ff       	call   80101a08 <ilock>
801074e9:	83 c4 10             	add    $0x10,%esp
  ip->nlink--;
801074ec:	8b 45 f4             	mov    -0xc(%ebp),%eax
801074ef:	0f b7 40 16          	movzwl 0x16(%eax),%eax
801074f3:	83 e8 01             	sub    $0x1,%eax
801074f6:	89 c2                	mov    %eax,%edx
801074f8:	8b 45 f4             	mov    -0xc(%ebp),%eax
801074fb:	66 89 50 16          	mov    %dx,0x16(%eax)
  iupdate(ip);
801074ff:	83 ec 0c             	sub    $0xc,%esp
80107502:	ff 75 f4             	pushl  -0xc(%ebp)
80107505:	e8 24 a3 ff ff       	call   8010182e <iupdate>
8010750a:	83 c4 10             	add    $0x10,%esp
  iunlockput(ip);
8010750d:	83 ec 0c             	sub    $0xc,%esp
80107510:	ff 75 f4             	pushl  -0xc(%ebp)
80107513:	e8 b0 a7 ff ff       	call   80101cc8 <iunlockput>
80107518:	83 c4 10             	add    $0x10,%esp
  end_op();
8010751b:	e8 57 c1 ff ff       	call   80103677 <end_op>
  return -1;
80107520:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80107525:	c9                   	leave  
80107526:	c3                   	ret    

80107527 <isdirempty>:

// Is the directory dp empty except for "." and ".." ?
static int
isdirempty(struct inode *dp)
{
80107527:	55                   	push   %ebp
80107528:	89 e5                	mov    %esp,%ebp
8010752a:	83 ec 28             	sub    $0x28,%esp
  int off;
  struct dirent de;

  for(off=2*sizeof(de); off<dp->size; off+=sizeof(de)){
8010752d:	c7 45 f4 20 00 00 00 	movl   $0x20,-0xc(%ebp)
80107534:	eb 40                	jmp    80107576 <isdirempty+0x4f>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80107536:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107539:	6a 10                	push   $0x10
8010753b:	50                   	push   %eax
8010753c:	8d 45 e4             	lea    -0x1c(%ebp),%eax
8010753f:	50                   	push   %eax
80107540:	ff 75 08             	pushl  0x8(%ebp)
80107543:	e8 2e aa ff ff       	call   80101f76 <readi>
80107548:	83 c4 10             	add    $0x10,%esp
8010754b:	83 f8 10             	cmp    $0x10,%eax
8010754e:	74 0d                	je     8010755d <isdirempty+0x36>
      panic("isdirempty: readi");
80107550:	83 ec 0c             	sub    $0xc,%esp
80107553:	68 07 ad 10 80       	push   $0x8010ad07
80107558:	e8 09 90 ff ff       	call   80100566 <panic>
    if(de.inum != 0)
8010755d:	0f b7 45 e4          	movzwl -0x1c(%ebp),%eax
80107561:	66 85 c0             	test   %ax,%ax
80107564:	74 07                	je     8010756d <isdirempty+0x46>
      return 0;
80107566:	b8 00 00 00 00       	mov    $0x0,%eax
8010756b:	eb 1b                	jmp    80107588 <isdirempty+0x61>
isdirempty(struct inode *dp)
{
  int off;
  struct dirent de;

  for(off=2*sizeof(de); off<dp->size; off+=sizeof(de)){
8010756d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107570:	83 c0 10             	add    $0x10,%eax
80107573:	89 45 f4             	mov    %eax,-0xc(%ebp)
80107576:	8b 45 08             	mov    0x8(%ebp),%eax
80107579:	8b 50 18             	mov    0x18(%eax),%edx
8010757c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010757f:	39 c2                	cmp    %eax,%edx
80107581:	77 b3                	ja     80107536 <isdirempty+0xf>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
      panic("isdirempty: readi");
    if(de.inum != 0)
      return 0;
  }
  return 1;
80107583:	b8 01 00 00 00       	mov    $0x1,%eax
}
80107588:	c9                   	leave  
80107589:	c3                   	ret    

8010758a <sys_unlink>:

int
sys_unlink(void)
{
8010758a:	55                   	push   %ebp
8010758b:	89 e5                	mov    %esp,%ebp
8010758d:	83 ec 38             	sub    $0x38,%esp
  struct inode *ip, *dp;
  struct dirent de;
  char name[DIRSIZ], *path;
  uint off;

  if(argstr(0, &path) < 0)
80107590:	83 ec 08             	sub    $0x8,%esp
80107593:	8d 45 cc             	lea    -0x34(%ebp),%eax
80107596:	50                   	push   %eax
80107597:	6a 00                	push   $0x0
80107599:	e8 a4 fa ff ff       	call   80107042 <argstr>
8010759e:	83 c4 10             	add    $0x10,%esp
801075a1:	85 c0                	test   %eax,%eax
801075a3:	79 0a                	jns    801075af <sys_unlink+0x25>
    return -1;
801075a5:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801075aa:	e9 bc 01 00 00       	jmp    8010776b <sys_unlink+0x1e1>

  begin_op();
801075af:	e8 37 c0 ff ff       	call   801035eb <begin_op>
  if((dp = nameiparent(path, name)) == 0){
801075b4:	8b 45 cc             	mov    -0x34(%ebp),%eax
801075b7:	83 ec 08             	sub    $0x8,%esp
801075ba:	8d 55 d2             	lea    -0x2e(%ebp),%edx
801075bd:	52                   	push   %edx
801075be:	50                   	push   %eax
801075bf:	e8 1e b0 ff ff       	call   801025e2 <nameiparent>
801075c4:	83 c4 10             	add    $0x10,%esp
801075c7:	89 45 f4             	mov    %eax,-0xc(%ebp)
801075ca:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801075ce:	75 0f                	jne    801075df <sys_unlink+0x55>
    end_op();
801075d0:	e8 a2 c0 ff ff       	call   80103677 <end_op>
    return -1;
801075d5:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801075da:	e9 8c 01 00 00       	jmp    8010776b <sys_unlink+0x1e1>
  }

  ilock(dp);
801075df:	83 ec 0c             	sub    $0xc,%esp
801075e2:	ff 75 f4             	pushl  -0xc(%ebp)
801075e5:	e8 1e a4 ff ff       	call   80101a08 <ilock>
801075ea:	83 c4 10             	add    $0x10,%esp

  // Cannot unlink "." or "..".
  if(namecmp(name, ".") == 0 || namecmp(name, "..") == 0)
801075ed:	83 ec 08             	sub    $0x8,%esp
801075f0:	68 19 ad 10 80       	push   $0x8010ad19
801075f5:	8d 45 d2             	lea    -0x2e(%ebp),%eax
801075f8:	50                   	push   %eax
801075f9:	e8 57 ac ff ff       	call   80102255 <namecmp>
801075fe:	83 c4 10             	add    $0x10,%esp
80107601:	85 c0                	test   %eax,%eax
80107603:	0f 84 4a 01 00 00    	je     80107753 <sys_unlink+0x1c9>
80107609:	83 ec 08             	sub    $0x8,%esp
8010760c:	68 1b ad 10 80       	push   $0x8010ad1b
80107611:	8d 45 d2             	lea    -0x2e(%ebp),%eax
80107614:	50                   	push   %eax
80107615:	e8 3b ac ff ff       	call   80102255 <namecmp>
8010761a:	83 c4 10             	add    $0x10,%esp
8010761d:	85 c0                	test   %eax,%eax
8010761f:	0f 84 2e 01 00 00    	je     80107753 <sys_unlink+0x1c9>
    goto bad;

  if((ip = dirlookup(dp, name, &off)) == 0)
80107625:	83 ec 04             	sub    $0x4,%esp
80107628:	8d 45 c8             	lea    -0x38(%ebp),%eax
8010762b:	50                   	push   %eax
8010762c:	8d 45 d2             	lea    -0x2e(%ebp),%eax
8010762f:	50                   	push   %eax
80107630:	ff 75 f4             	pushl  -0xc(%ebp)
80107633:	e8 38 ac ff ff       	call   80102270 <dirlookup>
80107638:	83 c4 10             	add    $0x10,%esp
8010763b:	89 45 f0             	mov    %eax,-0x10(%ebp)
8010763e:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80107642:	0f 84 0a 01 00 00    	je     80107752 <sys_unlink+0x1c8>
    goto bad;
  ilock(ip);
80107648:	83 ec 0c             	sub    $0xc,%esp
8010764b:	ff 75 f0             	pushl  -0x10(%ebp)
8010764e:	e8 b5 a3 ff ff       	call   80101a08 <ilock>
80107653:	83 c4 10             	add    $0x10,%esp

  if(ip->nlink < 1)
80107656:	8b 45 f0             	mov    -0x10(%ebp),%eax
80107659:	0f b7 40 16          	movzwl 0x16(%eax),%eax
8010765d:	66 85 c0             	test   %ax,%ax
80107660:	7f 0d                	jg     8010766f <sys_unlink+0xe5>
    panic("unlink: nlink < 1");
80107662:	83 ec 0c             	sub    $0xc,%esp
80107665:	68 1e ad 10 80       	push   $0x8010ad1e
8010766a:	e8 f7 8e ff ff       	call   80100566 <panic>
  if(ip->type == T_DIR && !isdirempty(ip)){
8010766f:	8b 45 f0             	mov    -0x10(%ebp),%eax
80107672:	0f b7 40 10          	movzwl 0x10(%eax),%eax
80107676:	66 83 f8 01          	cmp    $0x1,%ax
8010767a:	75 25                	jne    801076a1 <sys_unlink+0x117>
8010767c:	83 ec 0c             	sub    $0xc,%esp
8010767f:	ff 75 f0             	pushl  -0x10(%ebp)
80107682:	e8 a0 fe ff ff       	call   80107527 <isdirempty>
80107687:	83 c4 10             	add    $0x10,%esp
8010768a:	85 c0                	test   %eax,%eax
8010768c:	75 13                	jne    801076a1 <sys_unlink+0x117>
    iunlockput(ip);
8010768e:	83 ec 0c             	sub    $0xc,%esp
80107691:	ff 75 f0             	pushl  -0x10(%ebp)
80107694:	e8 2f a6 ff ff       	call   80101cc8 <iunlockput>
80107699:	83 c4 10             	add    $0x10,%esp
    goto bad;
8010769c:	e9 b2 00 00 00       	jmp    80107753 <sys_unlink+0x1c9>
  }

  memset(&de, 0, sizeof(de));
801076a1:	83 ec 04             	sub    $0x4,%esp
801076a4:	6a 10                	push   $0x10
801076a6:	6a 00                	push   $0x0
801076a8:	8d 45 e0             	lea    -0x20(%ebp),%eax
801076ab:	50                   	push   %eax
801076ac:	e8 e7 f5 ff ff       	call   80106c98 <memset>
801076b1:	83 c4 10             	add    $0x10,%esp
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
801076b4:	8b 45 c8             	mov    -0x38(%ebp),%eax
801076b7:	6a 10                	push   $0x10
801076b9:	50                   	push   %eax
801076ba:	8d 45 e0             	lea    -0x20(%ebp),%eax
801076bd:	50                   	push   %eax
801076be:	ff 75 f4             	pushl  -0xc(%ebp)
801076c1:	e8 07 aa ff ff       	call   801020cd <writei>
801076c6:	83 c4 10             	add    $0x10,%esp
801076c9:	83 f8 10             	cmp    $0x10,%eax
801076cc:	74 0d                	je     801076db <sys_unlink+0x151>
    panic("unlink: writei");
801076ce:	83 ec 0c             	sub    $0xc,%esp
801076d1:	68 30 ad 10 80       	push   $0x8010ad30
801076d6:	e8 8b 8e ff ff       	call   80100566 <panic>
  if(ip->type == T_DIR){
801076db:	8b 45 f0             	mov    -0x10(%ebp),%eax
801076de:	0f b7 40 10          	movzwl 0x10(%eax),%eax
801076e2:	66 83 f8 01          	cmp    $0x1,%ax
801076e6:	75 21                	jne    80107709 <sys_unlink+0x17f>
    dp->nlink--;
801076e8:	8b 45 f4             	mov    -0xc(%ebp),%eax
801076eb:	0f b7 40 16          	movzwl 0x16(%eax),%eax
801076ef:	83 e8 01             	sub    $0x1,%eax
801076f2:	89 c2                	mov    %eax,%edx
801076f4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801076f7:	66 89 50 16          	mov    %dx,0x16(%eax)
    iupdate(dp);
801076fb:	83 ec 0c             	sub    $0xc,%esp
801076fe:	ff 75 f4             	pushl  -0xc(%ebp)
80107701:	e8 28 a1 ff ff       	call   8010182e <iupdate>
80107706:	83 c4 10             	add    $0x10,%esp
  }
  iunlockput(dp);
80107709:	83 ec 0c             	sub    $0xc,%esp
8010770c:	ff 75 f4             	pushl  -0xc(%ebp)
8010770f:	e8 b4 a5 ff ff       	call   80101cc8 <iunlockput>
80107714:	83 c4 10             	add    $0x10,%esp

  ip->nlink--;
80107717:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010771a:	0f b7 40 16          	movzwl 0x16(%eax),%eax
8010771e:	83 e8 01             	sub    $0x1,%eax
80107721:	89 c2                	mov    %eax,%edx
80107723:	8b 45 f0             	mov    -0x10(%ebp),%eax
80107726:	66 89 50 16          	mov    %dx,0x16(%eax)
  iupdate(ip);
8010772a:	83 ec 0c             	sub    $0xc,%esp
8010772d:	ff 75 f0             	pushl  -0x10(%ebp)
80107730:	e8 f9 a0 ff ff       	call   8010182e <iupdate>
80107735:	83 c4 10             	add    $0x10,%esp
  iunlockput(ip);
80107738:	83 ec 0c             	sub    $0xc,%esp
8010773b:	ff 75 f0             	pushl  -0x10(%ebp)
8010773e:	e8 85 a5 ff ff       	call   80101cc8 <iunlockput>
80107743:	83 c4 10             	add    $0x10,%esp

  end_op();
80107746:	e8 2c bf ff ff       	call   80103677 <end_op>

  return 0;
8010774b:	b8 00 00 00 00       	mov    $0x0,%eax
80107750:	eb 19                	jmp    8010776b <sys_unlink+0x1e1>
  // Cannot unlink "." or "..".
  if(namecmp(name, ".") == 0 || namecmp(name, "..") == 0)
    goto bad;

  if((ip = dirlookup(dp, name, &off)) == 0)
    goto bad;
80107752:	90                   	nop
  end_op();

  return 0;

bad:
  iunlockput(dp);
80107753:	83 ec 0c             	sub    $0xc,%esp
80107756:	ff 75 f4             	pushl  -0xc(%ebp)
80107759:	e8 6a a5 ff ff       	call   80101cc8 <iunlockput>
8010775e:	83 c4 10             	add    $0x10,%esp
  end_op();
80107761:	e8 11 bf ff ff       	call   80103677 <end_op>
  return -1;
80107766:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
8010776b:	c9                   	leave  
8010776c:	c3                   	ret    

8010776d <create>:

static struct inode*
create(char *path, short type, short major, short minor)
{
8010776d:	55                   	push   %ebp
8010776e:	89 e5                	mov    %esp,%ebp
80107770:	83 ec 38             	sub    $0x38,%esp
80107773:	8b 4d 0c             	mov    0xc(%ebp),%ecx
80107776:	8b 55 10             	mov    0x10(%ebp),%edx
80107779:	8b 45 14             	mov    0x14(%ebp),%eax
8010777c:	66 89 4d d4          	mov    %cx,-0x2c(%ebp)
80107780:	66 89 55 d0          	mov    %dx,-0x30(%ebp)
80107784:	66 89 45 cc          	mov    %ax,-0x34(%ebp)
  uint off;
  struct inode *ip, *dp;
  char name[DIRSIZ];

  if((dp = nameiparent(path, name)) == 0)
80107788:	83 ec 08             	sub    $0x8,%esp
8010778b:	8d 45 de             	lea    -0x22(%ebp),%eax
8010778e:	50                   	push   %eax
8010778f:	ff 75 08             	pushl  0x8(%ebp)
80107792:	e8 4b ae ff ff       	call   801025e2 <nameiparent>
80107797:	83 c4 10             	add    $0x10,%esp
8010779a:	89 45 f4             	mov    %eax,-0xc(%ebp)
8010779d:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801077a1:	75 0a                	jne    801077ad <create+0x40>
    return 0;
801077a3:	b8 00 00 00 00       	mov    $0x0,%eax
801077a8:	e9 90 01 00 00       	jmp    8010793d <create+0x1d0>
  ilock(dp);
801077ad:	83 ec 0c             	sub    $0xc,%esp
801077b0:	ff 75 f4             	pushl  -0xc(%ebp)
801077b3:	e8 50 a2 ff ff       	call   80101a08 <ilock>
801077b8:	83 c4 10             	add    $0x10,%esp

  if((ip = dirlookup(dp, name, &off)) != 0){
801077bb:	83 ec 04             	sub    $0x4,%esp
801077be:	8d 45 ec             	lea    -0x14(%ebp),%eax
801077c1:	50                   	push   %eax
801077c2:	8d 45 de             	lea    -0x22(%ebp),%eax
801077c5:	50                   	push   %eax
801077c6:	ff 75 f4             	pushl  -0xc(%ebp)
801077c9:	e8 a2 aa ff ff       	call   80102270 <dirlookup>
801077ce:	83 c4 10             	add    $0x10,%esp
801077d1:	89 45 f0             	mov    %eax,-0x10(%ebp)
801077d4:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
801077d8:	74 50                	je     8010782a <create+0xbd>
    iunlockput(dp);
801077da:	83 ec 0c             	sub    $0xc,%esp
801077dd:	ff 75 f4             	pushl  -0xc(%ebp)
801077e0:	e8 e3 a4 ff ff       	call   80101cc8 <iunlockput>
801077e5:	83 c4 10             	add    $0x10,%esp
    ilock(ip);
801077e8:	83 ec 0c             	sub    $0xc,%esp
801077eb:	ff 75 f0             	pushl  -0x10(%ebp)
801077ee:	e8 15 a2 ff ff       	call   80101a08 <ilock>
801077f3:	83 c4 10             	add    $0x10,%esp
    if(type == T_FILE && ip->type == T_FILE)
801077f6:	66 83 7d d4 02       	cmpw   $0x2,-0x2c(%ebp)
801077fb:	75 15                	jne    80107812 <create+0xa5>
801077fd:	8b 45 f0             	mov    -0x10(%ebp),%eax
80107800:	0f b7 40 10          	movzwl 0x10(%eax),%eax
80107804:	66 83 f8 02          	cmp    $0x2,%ax
80107808:	75 08                	jne    80107812 <create+0xa5>
      return ip;
8010780a:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010780d:	e9 2b 01 00 00       	jmp    8010793d <create+0x1d0>
    iunlockput(ip);
80107812:	83 ec 0c             	sub    $0xc,%esp
80107815:	ff 75 f0             	pushl  -0x10(%ebp)
80107818:	e8 ab a4 ff ff       	call   80101cc8 <iunlockput>
8010781d:	83 c4 10             	add    $0x10,%esp
    return 0;
80107820:	b8 00 00 00 00       	mov    $0x0,%eax
80107825:	e9 13 01 00 00       	jmp    8010793d <create+0x1d0>
  }

  if((ip = ialloc(dp->dev, type)) == 0)
8010782a:	0f bf 55 d4          	movswl -0x2c(%ebp),%edx
8010782e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107831:	8b 00                	mov    (%eax),%eax
80107833:	83 ec 08             	sub    $0x8,%esp
80107836:	52                   	push   %edx
80107837:	50                   	push   %eax
80107838:	e8 1a 9f ff ff       	call   80101757 <ialloc>
8010783d:	83 c4 10             	add    $0x10,%esp
80107840:	89 45 f0             	mov    %eax,-0x10(%ebp)
80107843:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80107847:	75 0d                	jne    80107856 <create+0xe9>
    panic("create: ialloc");
80107849:	83 ec 0c             	sub    $0xc,%esp
8010784c:	68 3f ad 10 80       	push   $0x8010ad3f
80107851:	e8 10 8d ff ff       	call   80100566 <panic>

  ilock(ip);
80107856:	83 ec 0c             	sub    $0xc,%esp
80107859:	ff 75 f0             	pushl  -0x10(%ebp)
8010785c:	e8 a7 a1 ff ff       	call   80101a08 <ilock>
80107861:	83 c4 10             	add    $0x10,%esp
  ip->major = major;
80107864:	8b 45 f0             	mov    -0x10(%ebp),%eax
80107867:	0f b7 55 d0          	movzwl -0x30(%ebp),%edx
8010786b:	66 89 50 12          	mov    %dx,0x12(%eax)
  ip->minor = minor;
8010786f:	8b 45 f0             	mov    -0x10(%ebp),%eax
80107872:	0f b7 55 cc          	movzwl -0x34(%ebp),%edx
80107876:	66 89 50 14          	mov    %dx,0x14(%eax)
  ip->nlink = 1;
8010787a:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010787d:	66 c7 40 16 01 00    	movw   $0x1,0x16(%eax)
  iupdate(ip);
80107883:	83 ec 0c             	sub    $0xc,%esp
80107886:	ff 75 f0             	pushl  -0x10(%ebp)
80107889:	e8 a0 9f ff ff       	call   8010182e <iupdate>
8010788e:	83 c4 10             	add    $0x10,%esp

  if(type == T_DIR){  // Create . and .. entries.
80107891:	66 83 7d d4 01       	cmpw   $0x1,-0x2c(%ebp)
80107896:	75 6a                	jne    80107902 <create+0x195>
    dp->nlink++;  // for ".."
80107898:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010789b:	0f b7 40 16          	movzwl 0x16(%eax),%eax
8010789f:	83 c0 01             	add    $0x1,%eax
801078a2:	89 c2                	mov    %eax,%edx
801078a4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801078a7:	66 89 50 16          	mov    %dx,0x16(%eax)
    iupdate(dp);
801078ab:	83 ec 0c             	sub    $0xc,%esp
801078ae:	ff 75 f4             	pushl  -0xc(%ebp)
801078b1:	e8 78 9f ff ff       	call   8010182e <iupdate>
801078b6:	83 c4 10             	add    $0x10,%esp
    // No ip->nlink++ for ".": avoid cyclic ref count.
    if(dirlink(ip, ".", ip->inum) < 0 || dirlink(ip, "..", dp->inum) < 0)
801078b9:	8b 45 f0             	mov    -0x10(%ebp),%eax
801078bc:	8b 40 04             	mov    0x4(%eax),%eax
801078bf:	83 ec 04             	sub    $0x4,%esp
801078c2:	50                   	push   %eax
801078c3:	68 19 ad 10 80       	push   $0x8010ad19
801078c8:	ff 75 f0             	pushl  -0x10(%ebp)
801078cb:	e8 5a aa ff ff       	call   8010232a <dirlink>
801078d0:	83 c4 10             	add    $0x10,%esp
801078d3:	85 c0                	test   %eax,%eax
801078d5:	78 1e                	js     801078f5 <create+0x188>
801078d7:	8b 45 f4             	mov    -0xc(%ebp),%eax
801078da:	8b 40 04             	mov    0x4(%eax),%eax
801078dd:	83 ec 04             	sub    $0x4,%esp
801078e0:	50                   	push   %eax
801078e1:	68 1b ad 10 80       	push   $0x8010ad1b
801078e6:	ff 75 f0             	pushl  -0x10(%ebp)
801078e9:	e8 3c aa ff ff       	call   8010232a <dirlink>
801078ee:	83 c4 10             	add    $0x10,%esp
801078f1:	85 c0                	test   %eax,%eax
801078f3:	79 0d                	jns    80107902 <create+0x195>
      panic("create dots");
801078f5:	83 ec 0c             	sub    $0xc,%esp
801078f8:	68 4e ad 10 80       	push   $0x8010ad4e
801078fd:	e8 64 8c ff ff       	call   80100566 <panic>
  }

  if(dirlink(dp, name, ip->inum) < 0)
80107902:	8b 45 f0             	mov    -0x10(%ebp),%eax
80107905:	8b 40 04             	mov    0x4(%eax),%eax
80107908:	83 ec 04             	sub    $0x4,%esp
8010790b:	50                   	push   %eax
8010790c:	8d 45 de             	lea    -0x22(%ebp),%eax
8010790f:	50                   	push   %eax
80107910:	ff 75 f4             	pushl  -0xc(%ebp)
80107913:	e8 12 aa ff ff       	call   8010232a <dirlink>
80107918:	83 c4 10             	add    $0x10,%esp
8010791b:	85 c0                	test   %eax,%eax
8010791d:	79 0d                	jns    8010792c <create+0x1bf>
    panic("create: dirlink");
8010791f:	83 ec 0c             	sub    $0xc,%esp
80107922:	68 5a ad 10 80       	push   $0x8010ad5a
80107927:	e8 3a 8c ff ff       	call   80100566 <panic>

  iunlockput(dp);
8010792c:	83 ec 0c             	sub    $0xc,%esp
8010792f:	ff 75 f4             	pushl  -0xc(%ebp)
80107932:	e8 91 a3 ff ff       	call   80101cc8 <iunlockput>
80107937:	83 c4 10             	add    $0x10,%esp

  return ip;
8010793a:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
8010793d:	c9                   	leave  
8010793e:	c3                   	ret    

8010793f <sys_open>:

int
sys_open(void)
{
8010793f:	55                   	push   %ebp
80107940:	89 e5                	mov    %esp,%ebp
80107942:	83 ec 28             	sub    $0x28,%esp
  char *path;
  int fd, omode;
  struct file *f;
  struct inode *ip;

  if(argstr(0, &path) < 0 || argint(1, &omode) < 0)
80107945:	83 ec 08             	sub    $0x8,%esp
80107948:	8d 45 e8             	lea    -0x18(%ebp),%eax
8010794b:	50                   	push   %eax
8010794c:	6a 00                	push   $0x0
8010794e:	e8 ef f6 ff ff       	call   80107042 <argstr>
80107953:	83 c4 10             	add    $0x10,%esp
80107956:	85 c0                	test   %eax,%eax
80107958:	78 15                	js     8010796f <sys_open+0x30>
8010795a:	83 ec 08             	sub    $0x8,%esp
8010795d:	8d 45 e4             	lea    -0x1c(%ebp),%eax
80107960:	50                   	push   %eax
80107961:	6a 01                	push   $0x1
80107963:	e8 55 f6 ff ff       	call   80106fbd <argint>
80107968:	83 c4 10             	add    $0x10,%esp
8010796b:	85 c0                	test   %eax,%eax
8010796d:	79 0a                	jns    80107979 <sys_open+0x3a>
    return -1;
8010796f:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80107974:	e9 61 01 00 00       	jmp    80107ada <sys_open+0x19b>

  begin_op();
80107979:	e8 6d bc ff ff       	call   801035eb <begin_op>

  if(omode & O_CREATE){
8010797e:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80107981:	25 00 02 00 00       	and    $0x200,%eax
80107986:	85 c0                	test   %eax,%eax
80107988:	74 2a                	je     801079b4 <sys_open+0x75>
    ip = create(path, T_FILE, 0, 0);
8010798a:	8b 45 e8             	mov    -0x18(%ebp),%eax
8010798d:	6a 00                	push   $0x0
8010798f:	6a 00                	push   $0x0
80107991:	6a 02                	push   $0x2
80107993:	50                   	push   %eax
80107994:	e8 d4 fd ff ff       	call   8010776d <create>
80107999:	83 c4 10             	add    $0x10,%esp
8010799c:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if(ip == 0){
8010799f:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801079a3:	75 75                	jne    80107a1a <sys_open+0xdb>
      end_op();
801079a5:	e8 cd bc ff ff       	call   80103677 <end_op>
      return -1;
801079aa:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801079af:	e9 26 01 00 00       	jmp    80107ada <sys_open+0x19b>
    }
  } else {
    if((ip = namei(path)) == 0){
801079b4:	8b 45 e8             	mov    -0x18(%ebp),%eax
801079b7:	83 ec 0c             	sub    $0xc,%esp
801079ba:	50                   	push   %eax
801079bb:	e8 06 ac ff ff       	call   801025c6 <namei>
801079c0:	83 c4 10             	add    $0x10,%esp
801079c3:	89 45 f4             	mov    %eax,-0xc(%ebp)
801079c6:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801079ca:	75 0f                	jne    801079db <sys_open+0x9c>
      end_op();
801079cc:	e8 a6 bc ff ff       	call   80103677 <end_op>
      return -1;
801079d1:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801079d6:	e9 ff 00 00 00       	jmp    80107ada <sys_open+0x19b>
    }
    ilock(ip);
801079db:	83 ec 0c             	sub    $0xc,%esp
801079de:	ff 75 f4             	pushl  -0xc(%ebp)
801079e1:	e8 22 a0 ff ff       	call   80101a08 <ilock>
801079e6:	83 c4 10             	add    $0x10,%esp
    if(ip->type == T_DIR && omode != O_RDONLY){
801079e9:	8b 45 f4             	mov    -0xc(%ebp),%eax
801079ec:	0f b7 40 10          	movzwl 0x10(%eax),%eax
801079f0:	66 83 f8 01          	cmp    $0x1,%ax
801079f4:	75 24                	jne    80107a1a <sys_open+0xdb>
801079f6:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801079f9:	85 c0                	test   %eax,%eax
801079fb:	74 1d                	je     80107a1a <sys_open+0xdb>
      iunlockput(ip);
801079fd:	83 ec 0c             	sub    $0xc,%esp
80107a00:	ff 75 f4             	pushl  -0xc(%ebp)
80107a03:	e8 c0 a2 ff ff       	call   80101cc8 <iunlockput>
80107a08:	83 c4 10             	add    $0x10,%esp
      end_op();
80107a0b:	e8 67 bc ff ff       	call   80103677 <end_op>
      return -1;
80107a10:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80107a15:	e9 c0 00 00 00       	jmp    80107ada <sys_open+0x19b>
    }
  }

  if((f = filealloc()) == 0 || (fd = fdalloc(f)) < 0){
80107a1a:	e8 12 96 ff ff       	call   80101031 <filealloc>
80107a1f:	89 45 f0             	mov    %eax,-0x10(%ebp)
80107a22:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80107a26:	74 17                	je     80107a3f <sys_open+0x100>
80107a28:	83 ec 0c             	sub    $0xc,%esp
80107a2b:	ff 75 f0             	pushl  -0x10(%ebp)
80107a2e:	e8 3a f7 ff ff       	call   8010716d <fdalloc>
80107a33:	83 c4 10             	add    $0x10,%esp
80107a36:	89 45 ec             	mov    %eax,-0x14(%ebp)
80107a39:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
80107a3d:	79 2e                	jns    80107a6d <sys_open+0x12e>
    if(f)
80107a3f:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80107a43:	74 0e                	je     80107a53 <sys_open+0x114>
      fileclose(f);
80107a45:	83 ec 0c             	sub    $0xc,%esp
80107a48:	ff 75 f0             	pushl  -0x10(%ebp)
80107a4b:	e8 9f 96 ff ff       	call   801010ef <fileclose>
80107a50:	83 c4 10             	add    $0x10,%esp
    iunlockput(ip);
80107a53:	83 ec 0c             	sub    $0xc,%esp
80107a56:	ff 75 f4             	pushl  -0xc(%ebp)
80107a59:	e8 6a a2 ff ff       	call   80101cc8 <iunlockput>
80107a5e:	83 c4 10             	add    $0x10,%esp
    end_op();
80107a61:	e8 11 bc ff ff       	call   80103677 <end_op>
    return -1;
80107a66:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80107a6b:	eb 6d                	jmp    80107ada <sys_open+0x19b>
  }
  iunlock(ip);
80107a6d:	83 ec 0c             	sub    $0xc,%esp
80107a70:	ff 75 f4             	pushl  -0xc(%ebp)
80107a73:	e8 ee a0 ff ff       	call   80101b66 <iunlock>
80107a78:	83 c4 10             	add    $0x10,%esp
  end_op();
80107a7b:	e8 f7 bb ff ff       	call   80103677 <end_op>

  f->type = FD_INODE;
80107a80:	8b 45 f0             	mov    -0x10(%ebp),%eax
80107a83:	c7 00 02 00 00 00    	movl   $0x2,(%eax)
  f->ip = ip;
80107a89:	8b 45 f0             	mov    -0x10(%ebp),%eax
80107a8c:	8b 55 f4             	mov    -0xc(%ebp),%edx
80107a8f:	89 50 10             	mov    %edx,0x10(%eax)
  f->off = 0;
80107a92:	8b 45 f0             	mov    -0x10(%ebp),%eax
80107a95:	c7 40 14 00 00 00 00 	movl   $0x0,0x14(%eax)
  f->readable = !(omode & O_WRONLY);
80107a9c:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80107a9f:	83 e0 01             	and    $0x1,%eax
80107aa2:	85 c0                	test   %eax,%eax
80107aa4:	0f 94 c0             	sete   %al
80107aa7:	89 c2                	mov    %eax,%edx
80107aa9:	8b 45 f0             	mov    -0x10(%ebp),%eax
80107aac:	88 50 08             	mov    %dl,0x8(%eax)
  f->writable = (omode & O_WRONLY) || (omode & O_RDWR);
80107aaf:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80107ab2:	83 e0 01             	and    $0x1,%eax
80107ab5:	85 c0                	test   %eax,%eax
80107ab7:	75 0a                	jne    80107ac3 <sys_open+0x184>
80107ab9:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80107abc:	83 e0 02             	and    $0x2,%eax
80107abf:	85 c0                	test   %eax,%eax
80107ac1:	74 07                	je     80107aca <sys_open+0x18b>
80107ac3:	b8 01 00 00 00       	mov    $0x1,%eax
80107ac8:	eb 05                	jmp    80107acf <sys_open+0x190>
80107aca:	b8 00 00 00 00       	mov    $0x0,%eax
80107acf:	89 c2                	mov    %eax,%edx
80107ad1:	8b 45 f0             	mov    -0x10(%ebp),%eax
80107ad4:	88 50 09             	mov    %dl,0x9(%eax)
  return fd;
80107ad7:	8b 45 ec             	mov    -0x14(%ebp),%eax
}
80107ada:	c9                   	leave  
80107adb:	c3                   	ret    

80107adc <sys_mkdir>:

int
sys_mkdir(void)
{
80107adc:	55                   	push   %ebp
80107add:	89 e5                	mov    %esp,%ebp
80107adf:	83 ec 18             	sub    $0x18,%esp
  char *path;
  struct inode *ip;

  begin_op();
80107ae2:	e8 04 bb ff ff       	call   801035eb <begin_op>
  if(argstr(0, &path) < 0 || (ip = create(path, T_DIR, 0, 0)) == 0){
80107ae7:	83 ec 08             	sub    $0x8,%esp
80107aea:	8d 45 f0             	lea    -0x10(%ebp),%eax
80107aed:	50                   	push   %eax
80107aee:	6a 00                	push   $0x0
80107af0:	e8 4d f5 ff ff       	call   80107042 <argstr>
80107af5:	83 c4 10             	add    $0x10,%esp
80107af8:	85 c0                	test   %eax,%eax
80107afa:	78 1b                	js     80107b17 <sys_mkdir+0x3b>
80107afc:	8b 45 f0             	mov    -0x10(%ebp),%eax
80107aff:	6a 00                	push   $0x0
80107b01:	6a 00                	push   $0x0
80107b03:	6a 01                	push   $0x1
80107b05:	50                   	push   %eax
80107b06:	e8 62 fc ff ff       	call   8010776d <create>
80107b0b:	83 c4 10             	add    $0x10,%esp
80107b0e:	89 45 f4             	mov    %eax,-0xc(%ebp)
80107b11:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80107b15:	75 0c                	jne    80107b23 <sys_mkdir+0x47>
    end_op();
80107b17:	e8 5b bb ff ff       	call   80103677 <end_op>
    return -1;
80107b1c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80107b21:	eb 18                	jmp    80107b3b <sys_mkdir+0x5f>
  }
  iunlockput(ip);
80107b23:	83 ec 0c             	sub    $0xc,%esp
80107b26:	ff 75 f4             	pushl  -0xc(%ebp)
80107b29:	e8 9a a1 ff ff       	call   80101cc8 <iunlockput>
80107b2e:	83 c4 10             	add    $0x10,%esp
  end_op();
80107b31:	e8 41 bb ff ff       	call   80103677 <end_op>
  return 0;
80107b36:	b8 00 00 00 00       	mov    $0x0,%eax
}
80107b3b:	c9                   	leave  
80107b3c:	c3                   	ret    

80107b3d <sys_mknod>:

int
sys_mknod(void)
{
80107b3d:	55                   	push   %ebp
80107b3e:	89 e5                	mov    %esp,%ebp
80107b40:	83 ec 28             	sub    $0x28,%esp
  struct inode *ip;
  char *path;
  int len;
  int major, minor;
  
  begin_op();
80107b43:	e8 a3 ba ff ff       	call   801035eb <begin_op>
  if((len=argstr(0, &path)) < 0 ||
80107b48:	83 ec 08             	sub    $0x8,%esp
80107b4b:	8d 45 ec             	lea    -0x14(%ebp),%eax
80107b4e:	50                   	push   %eax
80107b4f:	6a 00                	push   $0x0
80107b51:	e8 ec f4 ff ff       	call   80107042 <argstr>
80107b56:	83 c4 10             	add    $0x10,%esp
80107b59:	89 45 f4             	mov    %eax,-0xc(%ebp)
80107b5c:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80107b60:	78 4f                	js     80107bb1 <sys_mknod+0x74>
     argint(1, &major) < 0 ||
80107b62:	83 ec 08             	sub    $0x8,%esp
80107b65:	8d 45 e8             	lea    -0x18(%ebp),%eax
80107b68:	50                   	push   %eax
80107b69:	6a 01                	push   $0x1
80107b6b:	e8 4d f4 ff ff       	call   80106fbd <argint>
80107b70:	83 c4 10             	add    $0x10,%esp
  char *path;
  int len;
  int major, minor;
  
  begin_op();
  if((len=argstr(0, &path)) < 0 ||
80107b73:	85 c0                	test   %eax,%eax
80107b75:	78 3a                	js     80107bb1 <sys_mknod+0x74>
     argint(1, &major) < 0 ||
     argint(2, &minor) < 0 ||
80107b77:	83 ec 08             	sub    $0x8,%esp
80107b7a:	8d 45 e4             	lea    -0x1c(%ebp),%eax
80107b7d:	50                   	push   %eax
80107b7e:	6a 02                	push   $0x2
80107b80:	e8 38 f4 ff ff       	call   80106fbd <argint>
80107b85:	83 c4 10             	add    $0x10,%esp
  int len;
  int major, minor;
  
  begin_op();
  if((len=argstr(0, &path)) < 0 ||
     argint(1, &major) < 0 ||
80107b88:	85 c0                	test   %eax,%eax
80107b8a:	78 25                	js     80107bb1 <sys_mknod+0x74>
     argint(2, &minor) < 0 ||
     (ip = create(path, T_DEV, major, minor)) == 0){
80107b8c:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80107b8f:	0f bf c8             	movswl %ax,%ecx
80107b92:	8b 45 e8             	mov    -0x18(%ebp),%eax
80107b95:	0f bf d0             	movswl %ax,%edx
80107b98:	8b 45 ec             	mov    -0x14(%ebp),%eax
  int major, minor;
  
  begin_op();
  if((len=argstr(0, &path)) < 0 ||
     argint(1, &major) < 0 ||
     argint(2, &minor) < 0 ||
80107b9b:	51                   	push   %ecx
80107b9c:	52                   	push   %edx
80107b9d:	6a 03                	push   $0x3
80107b9f:	50                   	push   %eax
80107ba0:	e8 c8 fb ff ff       	call   8010776d <create>
80107ba5:	83 c4 10             	add    $0x10,%esp
80107ba8:	89 45 f0             	mov    %eax,-0x10(%ebp)
80107bab:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80107baf:	75 0c                	jne    80107bbd <sys_mknod+0x80>
     (ip = create(path, T_DEV, major, minor)) == 0){
    end_op();
80107bb1:	e8 c1 ba ff ff       	call   80103677 <end_op>
    return -1;
80107bb6:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80107bbb:	eb 18                	jmp    80107bd5 <sys_mknod+0x98>
  }
  iunlockput(ip);
80107bbd:	83 ec 0c             	sub    $0xc,%esp
80107bc0:	ff 75 f0             	pushl  -0x10(%ebp)
80107bc3:	e8 00 a1 ff ff       	call   80101cc8 <iunlockput>
80107bc8:	83 c4 10             	add    $0x10,%esp
  end_op();
80107bcb:	e8 a7 ba ff ff       	call   80103677 <end_op>
  return 0;
80107bd0:	b8 00 00 00 00       	mov    $0x0,%eax
}
80107bd5:	c9                   	leave  
80107bd6:	c3                   	ret    

80107bd7 <sys_chdir>:

int
sys_chdir(void)
{
80107bd7:	55                   	push   %ebp
80107bd8:	89 e5                	mov    %esp,%ebp
80107bda:	83 ec 18             	sub    $0x18,%esp
  char *path;
  struct inode *ip;

  begin_op();
80107bdd:	e8 09 ba ff ff       	call   801035eb <begin_op>
  if(argstr(0, &path) < 0 || (ip = namei(path)) == 0){
80107be2:	83 ec 08             	sub    $0x8,%esp
80107be5:	8d 45 f0             	lea    -0x10(%ebp),%eax
80107be8:	50                   	push   %eax
80107be9:	6a 00                	push   $0x0
80107beb:	e8 52 f4 ff ff       	call   80107042 <argstr>
80107bf0:	83 c4 10             	add    $0x10,%esp
80107bf3:	85 c0                	test   %eax,%eax
80107bf5:	78 18                	js     80107c0f <sys_chdir+0x38>
80107bf7:	8b 45 f0             	mov    -0x10(%ebp),%eax
80107bfa:	83 ec 0c             	sub    $0xc,%esp
80107bfd:	50                   	push   %eax
80107bfe:	e8 c3 a9 ff ff       	call   801025c6 <namei>
80107c03:	83 c4 10             	add    $0x10,%esp
80107c06:	89 45 f4             	mov    %eax,-0xc(%ebp)
80107c09:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80107c0d:	75 0c                	jne    80107c1b <sys_chdir+0x44>
    end_op();
80107c0f:	e8 63 ba ff ff       	call   80103677 <end_op>
    return -1;
80107c14:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80107c19:	eb 6e                	jmp    80107c89 <sys_chdir+0xb2>
  }
  ilock(ip);
80107c1b:	83 ec 0c             	sub    $0xc,%esp
80107c1e:	ff 75 f4             	pushl  -0xc(%ebp)
80107c21:	e8 e2 9d ff ff       	call   80101a08 <ilock>
80107c26:	83 c4 10             	add    $0x10,%esp
  if(ip->type != T_DIR){
80107c29:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107c2c:	0f b7 40 10          	movzwl 0x10(%eax),%eax
80107c30:	66 83 f8 01          	cmp    $0x1,%ax
80107c34:	74 1a                	je     80107c50 <sys_chdir+0x79>
    iunlockput(ip);
80107c36:	83 ec 0c             	sub    $0xc,%esp
80107c39:	ff 75 f4             	pushl  -0xc(%ebp)
80107c3c:	e8 87 a0 ff ff       	call   80101cc8 <iunlockput>
80107c41:	83 c4 10             	add    $0x10,%esp
    end_op();
80107c44:	e8 2e ba ff ff       	call   80103677 <end_op>
    return -1;
80107c49:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80107c4e:	eb 39                	jmp    80107c89 <sys_chdir+0xb2>
  }
  iunlock(ip);
80107c50:	83 ec 0c             	sub    $0xc,%esp
80107c53:	ff 75 f4             	pushl  -0xc(%ebp)
80107c56:	e8 0b 9f ff ff       	call   80101b66 <iunlock>
80107c5b:	83 c4 10             	add    $0x10,%esp
  iput(proc->cwd);
80107c5e:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80107c64:	8b 40 70             	mov    0x70(%eax),%eax
80107c67:	83 ec 0c             	sub    $0xc,%esp
80107c6a:	50                   	push   %eax
80107c6b:	e8 68 9f ff ff       	call   80101bd8 <iput>
80107c70:	83 c4 10             	add    $0x10,%esp
  end_op();
80107c73:	e8 ff b9 ff ff       	call   80103677 <end_op>
  proc->cwd = ip;
80107c78:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80107c7e:	8b 55 f4             	mov    -0xc(%ebp),%edx
80107c81:	89 50 70             	mov    %edx,0x70(%eax)
  return 0;
80107c84:	b8 00 00 00 00       	mov    $0x0,%eax
}
80107c89:	c9                   	leave  
80107c8a:	c3                   	ret    

80107c8b <sys_exec>:

int
sys_exec(void)
{
80107c8b:	55                   	push   %ebp
80107c8c:	89 e5                	mov    %esp,%ebp
80107c8e:	81 ec 98 00 00 00    	sub    $0x98,%esp
  char *path, *argv[MAXARG];
  int i;
  uint uargv, uarg;

  if(argstr(0, &path) < 0 || argint(1, (int*)&uargv) < 0){
80107c94:	83 ec 08             	sub    $0x8,%esp
80107c97:	8d 45 f0             	lea    -0x10(%ebp),%eax
80107c9a:	50                   	push   %eax
80107c9b:	6a 00                	push   $0x0
80107c9d:	e8 a0 f3 ff ff       	call   80107042 <argstr>
80107ca2:	83 c4 10             	add    $0x10,%esp
80107ca5:	85 c0                	test   %eax,%eax
80107ca7:	78 18                	js     80107cc1 <sys_exec+0x36>
80107ca9:	83 ec 08             	sub    $0x8,%esp
80107cac:	8d 85 6c ff ff ff    	lea    -0x94(%ebp),%eax
80107cb2:	50                   	push   %eax
80107cb3:	6a 01                	push   $0x1
80107cb5:	e8 03 f3 ff ff       	call   80106fbd <argint>
80107cba:	83 c4 10             	add    $0x10,%esp
80107cbd:	85 c0                	test   %eax,%eax
80107cbf:	79 0a                	jns    80107ccb <sys_exec+0x40>
    return -1;
80107cc1:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80107cc6:	e9 c6 00 00 00       	jmp    80107d91 <sys_exec+0x106>
  }
  memset(argv, 0, sizeof(argv));
80107ccb:	83 ec 04             	sub    $0x4,%esp
80107cce:	68 80 00 00 00       	push   $0x80
80107cd3:	6a 00                	push   $0x0
80107cd5:	8d 85 70 ff ff ff    	lea    -0x90(%ebp),%eax
80107cdb:	50                   	push   %eax
80107cdc:	e8 b7 ef ff ff       	call   80106c98 <memset>
80107ce1:	83 c4 10             	add    $0x10,%esp
  for(i=0;; i++){
80107ce4:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
    if(i >= NELEM(argv))
80107ceb:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107cee:	83 f8 1f             	cmp    $0x1f,%eax
80107cf1:	76 0a                	jbe    80107cfd <sys_exec+0x72>
      return -1;
80107cf3:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80107cf8:	e9 94 00 00 00       	jmp    80107d91 <sys_exec+0x106>
    if(fetchint(uargv+4*i, (int*)&uarg) < 0)
80107cfd:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107d00:	c1 e0 02             	shl    $0x2,%eax
80107d03:	89 c2                	mov    %eax,%edx
80107d05:	8b 85 6c ff ff ff    	mov    -0x94(%ebp),%eax
80107d0b:	01 c2                	add    %eax,%edx
80107d0d:	83 ec 08             	sub    $0x8,%esp
80107d10:	8d 85 68 ff ff ff    	lea    -0x98(%ebp),%eax
80107d16:	50                   	push   %eax
80107d17:	52                   	push   %edx
80107d18:	e8 04 f2 ff ff       	call   80106f21 <fetchint>
80107d1d:	83 c4 10             	add    $0x10,%esp
80107d20:	85 c0                	test   %eax,%eax
80107d22:	79 07                	jns    80107d2b <sys_exec+0xa0>
      return -1;
80107d24:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80107d29:	eb 66                	jmp    80107d91 <sys_exec+0x106>
    if(uarg == 0){
80107d2b:	8b 85 68 ff ff ff    	mov    -0x98(%ebp),%eax
80107d31:	85 c0                	test   %eax,%eax
80107d33:	75 27                	jne    80107d5c <sys_exec+0xd1>
      argv[i] = 0;
80107d35:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107d38:	c7 84 85 70 ff ff ff 	movl   $0x0,-0x90(%ebp,%eax,4)
80107d3f:	00 00 00 00 
      break;
80107d43:	90                   	nop
    }
    if(fetchstr(uarg, &argv[i]) < 0)
      return -1;
  }
  return exec(path, argv);
80107d44:	8b 45 f0             	mov    -0x10(%ebp),%eax
80107d47:	83 ec 08             	sub    $0x8,%esp
80107d4a:	8d 95 70 ff ff ff    	lea    -0x90(%ebp),%edx
80107d50:	52                   	push   %edx
80107d51:	50                   	push   %eax
80107d52:	e8 b8 8e ff ff       	call   80100c0f <exec>
80107d57:	83 c4 10             	add    $0x10,%esp
80107d5a:	eb 35                	jmp    80107d91 <sys_exec+0x106>
      return -1;
    if(uarg == 0){
      argv[i] = 0;
      break;
    }
    if(fetchstr(uarg, &argv[i]) < 0)
80107d5c:	8d 85 70 ff ff ff    	lea    -0x90(%ebp),%eax
80107d62:	8b 55 f4             	mov    -0xc(%ebp),%edx
80107d65:	c1 e2 02             	shl    $0x2,%edx
80107d68:	01 c2                	add    %eax,%edx
80107d6a:	8b 85 68 ff ff ff    	mov    -0x98(%ebp),%eax
80107d70:	83 ec 08             	sub    $0x8,%esp
80107d73:	52                   	push   %edx
80107d74:	50                   	push   %eax
80107d75:	e8 e1 f1 ff ff       	call   80106f5b <fetchstr>
80107d7a:	83 c4 10             	add    $0x10,%esp
80107d7d:	85 c0                	test   %eax,%eax
80107d7f:	79 07                	jns    80107d88 <sys_exec+0xfd>
      return -1;
80107d81:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80107d86:	eb 09                	jmp    80107d91 <sys_exec+0x106>

  if(argstr(0, &path) < 0 || argint(1, (int*)&uargv) < 0){
    return -1;
  }
  memset(argv, 0, sizeof(argv));
  for(i=0;; i++){
80107d88:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
      argv[i] = 0;
      break;
    }
    if(fetchstr(uarg, &argv[i]) < 0)
      return -1;
  }
80107d8c:	e9 5a ff ff ff       	jmp    80107ceb <sys_exec+0x60>
  return exec(path, argv);
}
80107d91:	c9                   	leave  
80107d92:	c3                   	ret    

80107d93 <sys_pipe>:

int
sys_pipe(void)
{
80107d93:	55                   	push   %ebp
80107d94:	89 e5                	mov    %esp,%ebp
80107d96:	83 ec 28             	sub    $0x28,%esp
  int *fd;
  struct file *rf, *wf;
  int fd0, fd1;

  if(argptr(0, (void*)&fd, 2*sizeof(fd[0])) < 0)
80107d99:	83 ec 04             	sub    $0x4,%esp
80107d9c:	6a 08                	push   $0x8
80107d9e:	8d 45 ec             	lea    -0x14(%ebp),%eax
80107da1:	50                   	push   %eax
80107da2:	6a 00                	push   $0x0
80107da4:	e8 3c f2 ff ff       	call   80106fe5 <argptr>
80107da9:	83 c4 10             	add    $0x10,%esp
80107dac:	85 c0                	test   %eax,%eax
80107dae:	79 0a                	jns    80107dba <sys_pipe+0x27>
    return -1;
80107db0:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80107db5:	e9 ae 00 00 00       	jmp    80107e68 <sys_pipe+0xd5>
  if(pipealloc(&rf, &wf) < 0)
80107dba:	83 ec 08             	sub    $0x8,%esp
80107dbd:	8d 45 e4             	lea    -0x1c(%ebp),%eax
80107dc0:	50                   	push   %eax
80107dc1:	8d 45 e8             	lea    -0x18(%ebp),%eax
80107dc4:	50                   	push   %eax
80107dc5:	e8 15 c3 ff ff       	call   801040df <pipealloc>
80107dca:	83 c4 10             	add    $0x10,%esp
80107dcd:	85 c0                	test   %eax,%eax
80107dcf:	79 0a                	jns    80107ddb <sys_pipe+0x48>
    return -1;
80107dd1:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80107dd6:	e9 8d 00 00 00       	jmp    80107e68 <sys_pipe+0xd5>
  fd0 = -1;
80107ddb:	c7 45 f4 ff ff ff ff 	movl   $0xffffffff,-0xc(%ebp)
  if((fd0 = fdalloc(rf)) < 0 || (fd1 = fdalloc(wf)) < 0){
80107de2:	8b 45 e8             	mov    -0x18(%ebp),%eax
80107de5:	83 ec 0c             	sub    $0xc,%esp
80107de8:	50                   	push   %eax
80107de9:	e8 7f f3 ff ff       	call   8010716d <fdalloc>
80107dee:	83 c4 10             	add    $0x10,%esp
80107df1:	89 45 f4             	mov    %eax,-0xc(%ebp)
80107df4:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80107df8:	78 18                	js     80107e12 <sys_pipe+0x7f>
80107dfa:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80107dfd:	83 ec 0c             	sub    $0xc,%esp
80107e00:	50                   	push   %eax
80107e01:	e8 67 f3 ff ff       	call   8010716d <fdalloc>
80107e06:	83 c4 10             	add    $0x10,%esp
80107e09:	89 45 f0             	mov    %eax,-0x10(%ebp)
80107e0c:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80107e10:	79 3e                	jns    80107e50 <sys_pipe+0xbd>
    if(fd0 >= 0)
80107e12:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80107e16:	78 13                	js     80107e2b <sys_pipe+0x98>
      proc->ofile[fd0] = 0;
80107e18:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80107e1e:	8b 55 f4             	mov    -0xc(%ebp),%edx
80107e21:	83 c2 0c             	add    $0xc,%edx
80107e24:	c7 04 90 00 00 00 00 	movl   $0x0,(%eax,%edx,4)
    fileclose(rf);
80107e2b:	8b 45 e8             	mov    -0x18(%ebp),%eax
80107e2e:	83 ec 0c             	sub    $0xc,%esp
80107e31:	50                   	push   %eax
80107e32:	e8 b8 92 ff ff       	call   801010ef <fileclose>
80107e37:	83 c4 10             	add    $0x10,%esp
    fileclose(wf);
80107e3a:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80107e3d:	83 ec 0c             	sub    $0xc,%esp
80107e40:	50                   	push   %eax
80107e41:	e8 a9 92 ff ff       	call   801010ef <fileclose>
80107e46:	83 c4 10             	add    $0x10,%esp
    return -1;
80107e49:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80107e4e:	eb 18                	jmp    80107e68 <sys_pipe+0xd5>
  }
  fd[0] = fd0;
80107e50:	8b 45 ec             	mov    -0x14(%ebp),%eax
80107e53:	8b 55 f4             	mov    -0xc(%ebp),%edx
80107e56:	89 10                	mov    %edx,(%eax)
  fd[1] = fd1;
80107e58:	8b 45 ec             	mov    -0x14(%ebp),%eax
80107e5b:	8d 50 04             	lea    0x4(%eax),%edx
80107e5e:	8b 45 f0             	mov    -0x10(%ebp),%eax
80107e61:	89 02                	mov    %eax,(%edx)
  return 0;
80107e63:	b8 00 00 00 00       	mov    $0x0,%eax
}
80107e68:	c9                   	leave  
80107e69:	c3                   	ret    

80107e6a <outw>:
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
}

static inline void
outw(ushort port, ushort data)
{
80107e6a:	55                   	push   %ebp
80107e6b:	89 e5                	mov    %esp,%ebp
80107e6d:	83 ec 08             	sub    $0x8,%esp
80107e70:	8b 55 08             	mov    0x8(%ebp),%edx
80107e73:	8b 45 0c             	mov    0xc(%ebp),%eax
80107e76:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
80107e7a:	66 89 45 f8          	mov    %ax,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80107e7e:	0f b7 45 f8          	movzwl -0x8(%ebp),%eax
80107e82:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
80107e86:	66 ef                	out    %ax,(%dx)
}
80107e88:	90                   	nop
80107e89:	c9                   	leave  
80107e8a:	c3                   	ret    

80107e8b <sys_fork>:
#include "proc.h"
#include "uproc.h"

int
sys_fork(void)
{
80107e8b:	55                   	push   %ebp
80107e8c:	89 e5                	mov    %esp,%ebp
80107e8e:	83 ec 08             	sub    $0x8,%esp
  return fork();
80107e91:	e8 0d cb ff ff       	call   801049a3 <fork>
}
80107e96:	c9                   	leave  
80107e97:	c3                   	ret    

80107e98 <sys_exit>:

int
sys_exit(void)
{
80107e98:	55                   	push   %ebp
80107e99:	89 e5                	mov    %esp,%ebp
80107e9b:	83 ec 08             	sub    $0x8,%esp
  exit();
80107e9e:	e8 74 cd ff ff       	call   80104c17 <exit>
  return 0;  // not reached
80107ea3:	b8 00 00 00 00       	mov    $0x0,%eax
}
80107ea8:	c9                   	leave  
80107ea9:	c3                   	ret    

80107eaa <sys_wait>:

int
sys_wait(void)
{
80107eaa:	55                   	push   %ebp
80107eab:	89 e5                	mov    %esp,%ebp
80107ead:	83 ec 08             	sub    $0x8,%esp
  return wait();
80107eb0:	e8 b2 cf ff ff       	call   80104e67 <wait>
}
80107eb5:	c9                   	leave  
80107eb6:	c3                   	ret    

80107eb7 <sys_kill>:

int
sys_kill(void)
{
80107eb7:	55                   	push   %ebp
80107eb8:	89 e5                	mov    %esp,%ebp
80107eba:	83 ec 18             	sub    $0x18,%esp
  int pid;

  if(argint(0, &pid) < 0)
80107ebd:	83 ec 08             	sub    $0x8,%esp
80107ec0:	8d 45 f4             	lea    -0xc(%ebp),%eax
80107ec3:	50                   	push   %eax
80107ec4:	6a 00                	push   $0x0
80107ec6:	e8 f2 f0 ff ff       	call   80106fbd <argint>
80107ecb:	83 c4 10             	add    $0x10,%esp
80107ece:	85 c0                	test   %eax,%eax
80107ed0:	79 07                	jns    80107ed9 <sys_kill+0x22>
    return -1;
80107ed2:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80107ed7:	eb 0f                	jmp    80107ee8 <sys_kill+0x31>
  return kill(pid);
80107ed9:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107edc:	83 ec 0c             	sub    $0xc,%esp
80107edf:	50                   	push   %eax
80107ee0:	e8 e7 d6 ff ff       	call   801055cc <kill>
80107ee5:	83 c4 10             	add    $0x10,%esp
}
80107ee8:	c9                   	leave  
80107ee9:	c3                   	ret    

80107eea <sys_getpid>:

int
sys_getpid(void)
{
80107eea:	55                   	push   %ebp
80107eeb:	89 e5                	mov    %esp,%ebp
  return proc->pid;
80107eed:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80107ef3:	8b 40 10             	mov    0x10(%eax),%eax
}
80107ef6:	5d                   	pop    %ebp
80107ef7:	c3                   	ret    

80107ef8 <sys_sbrk>:

int
sys_sbrk(void)
{
80107ef8:	55                   	push   %ebp
80107ef9:	89 e5                	mov    %esp,%ebp
80107efb:	83 ec 18             	sub    $0x18,%esp
  int addr;
  int n;

  if(argint(0, &n) < 0)
80107efe:	83 ec 08             	sub    $0x8,%esp
80107f01:	8d 45 f0             	lea    -0x10(%ebp),%eax
80107f04:	50                   	push   %eax
80107f05:	6a 00                	push   $0x0
80107f07:	e8 b1 f0 ff ff       	call   80106fbd <argint>
80107f0c:	83 c4 10             	add    $0x10,%esp
80107f0f:	85 c0                	test   %eax,%eax
80107f11:	79 07                	jns    80107f1a <sys_sbrk+0x22>
    return -1;
80107f13:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80107f18:	eb 28                	jmp    80107f42 <sys_sbrk+0x4a>
  addr = proc->sz;
80107f1a:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80107f20:	8b 00                	mov    (%eax),%eax
80107f22:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(growproc(n) < 0)
80107f25:	8b 45 f0             	mov    -0x10(%ebp),%eax
80107f28:	83 ec 0c             	sub    $0xc,%esp
80107f2b:	50                   	push   %eax
80107f2c:	e8 cf c9 ff ff       	call   80104900 <growproc>
80107f31:	83 c4 10             	add    $0x10,%esp
80107f34:	85 c0                	test   %eax,%eax
80107f36:	79 07                	jns    80107f3f <sys_sbrk+0x47>
    return -1;
80107f38:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80107f3d:	eb 03                	jmp    80107f42 <sys_sbrk+0x4a>
  return addr;
80107f3f:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
80107f42:	c9                   	leave  
80107f43:	c3                   	ret    

80107f44 <sys_sleep>:

int
sys_sleep(void)
{
80107f44:	55                   	push   %ebp
80107f45:	89 e5                	mov    %esp,%ebp
80107f47:	83 ec 18             	sub    $0x18,%esp
  int n;
  uint ticks0;
  
  if(argint(0, &n) < 0)
80107f4a:	83 ec 08             	sub    $0x8,%esp
80107f4d:	8d 45 f0             	lea    -0x10(%ebp),%eax
80107f50:	50                   	push   %eax
80107f51:	6a 00                	push   $0x0
80107f53:	e8 65 f0 ff ff       	call   80106fbd <argint>
80107f58:	83 c4 10             	add    $0x10,%esp
80107f5b:	85 c0                	test   %eax,%eax
80107f5d:	79 07                	jns    80107f66 <sys_sleep+0x22>
    return -1;
80107f5f:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80107f64:	eb 44                	jmp    80107faa <sys_sleep+0x66>
  ticks0 = ticks;
80107f66:	a1 00 79 11 80       	mov    0x80117900,%eax
80107f6b:	89 45 f4             	mov    %eax,-0xc(%ebp)
  while(ticks - ticks0 < n){
80107f6e:	eb 26                	jmp    80107f96 <sys_sleep+0x52>
    if(proc->killed){
80107f70:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80107f76:	8b 40 2c             	mov    0x2c(%eax),%eax
80107f79:	85 c0                	test   %eax,%eax
80107f7b:	74 07                	je     80107f84 <sys_sleep+0x40>
      return -1;
80107f7d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80107f82:	eb 26                	jmp    80107faa <sys_sleep+0x66>
    }
    sleep(&ticks, (struct spinlock *)0);
80107f84:	83 ec 08             	sub    $0x8,%esp
80107f87:	6a 00                	push   $0x0
80107f89:	68 00 79 11 80       	push   $0x80117900
80107f8e:	e8 59 d4 ff ff       	call   801053ec <sleep>
80107f93:	83 c4 10             	add    $0x10,%esp
  uint ticks0;
  
  if(argint(0, &n) < 0)
    return -1;
  ticks0 = ticks;
  while(ticks - ticks0 < n){
80107f96:	a1 00 79 11 80       	mov    0x80117900,%eax
80107f9b:	2b 45 f4             	sub    -0xc(%ebp),%eax
80107f9e:	8b 55 f0             	mov    -0x10(%ebp),%edx
80107fa1:	39 d0                	cmp    %edx,%eax
80107fa3:	72 cb                	jb     80107f70 <sys_sleep+0x2c>
    if(proc->killed){
      return -1;
    }
    sleep(&ticks, (struct spinlock *)0);
  }
  return 0;
80107fa5:	b8 00 00 00 00       	mov    $0x0,%eax
}
80107faa:	c9                   	leave  
80107fab:	c3                   	ret    

80107fac <sys_uptime>:

// return how many clock tick interrupts have occurred
// since start. 
int
sys_uptime(void)
{
80107fac:	55                   	push   %ebp
80107fad:	89 e5                	mov    %esp,%ebp
80107faf:	83 ec 10             	sub    $0x10,%esp
  uint xticks;
  
  xticks = ticks;
80107fb2:	a1 00 79 11 80       	mov    0x80117900,%eax
80107fb7:	89 45 fc             	mov    %eax,-0x4(%ebp)
  return xticks;
80107fba:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80107fbd:	c9                   	leave  
80107fbe:	c3                   	ret    

80107fbf <sys_halt>:

//Turn of the computer
int
sys_halt(void)
{
80107fbf:	55                   	push   %ebp
80107fc0:	89 e5                	mov    %esp,%ebp
80107fc2:	83 ec 08             	sub    $0x8,%esp
  cprintf("Shutting down ...\n");
80107fc5:	83 ec 0c             	sub    $0xc,%esp
80107fc8:	68 6a ad 10 80       	push   $0x8010ad6a
80107fcd:	e8 f4 83 ff ff       	call   801003c6 <cprintf>
80107fd2:	83 c4 10             	add    $0x10,%esp
// outw (0xB004, 0x0 | 0x2000);  // changed in newest version of QEMU
  outw( 0x604, 0x0 | 0x2000);
80107fd5:	83 ec 08             	sub    $0x8,%esp
80107fd8:	68 00 20 00 00       	push   $0x2000
80107fdd:	68 04 06 00 00       	push   $0x604
80107fe2:	e8 83 fe ff ff       	call   80107e6a <outw>
80107fe7:	83 c4 10             	add    $0x10,%esp
  return 0;
80107fea:	b8 00 00 00 00       	mov    $0x0,%eax
}
80107fef:	c9                   	leave  
80107ff0:	c3                   	ret    

80107ff1 <sys_date>:

int
sys_date(void)
{
80107ff1:	55                   	push   %ebp
80107ff2:	89 e5                	mov    %esp,%ebp
80107ff4:	83 ec 18             	sub    $0x18,%esp
  struct rtcdate *d;

  if(argptr(0, (void*)&d, sizeof(*d)) < 0)
80107ff7:	83 ec 04             	sub    $0x4,%esp
80107ffa:	6a 18                	push   $0x18
80107ffc:	8d 45 f4             	lea    -0xc(%ebp),%eax
80107fff:	50                   	push   %eax
80108000:	6a 00                	push   $0x0
80108002:	e8 de ef ff ff       	call   80106fe5 <argptr>
80108007:	83 c4 10             	add    $0x10,%esp
8010800a:	85 c0                	test   %eax,%eax
8010800c:	79 07                	jns    80108015 <sys_date+0x24>
    return -1;
8010800e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80108013:	eb 14                	jmp    80108029 <sys_date+0x38>
  //my P1 code
  cmostime(d);
80108015:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108018:	83 ec 0c             	sub    $0xc,%esp
8010801b:	50                   	push   %eax
8010801c:	e8 45 b2 ff ff       	call   80103266 <cmostime>
80108021:	83 c4 10             	add    $0x10,%esp
  return 0;
80108024:	b8 00 00 00 00       	mov    $0x0,%eax
}
80108029:	c9                   	leave  
8010802a:	c3                   	ret    

8010802b <sys_getuid>:

//========== Project 2 System Calls Start ====================
int
sys_getuid(void)
{
8010802b:	55                   	push   %ebp
8010802c:	89 e5                	mov    %esp,%ebp
  //return the uid in the proc struct
  return proc->uid;
8010802e:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80108034:	8b 40 14             	mov    0x14(%eax),%eax
}
80108037:	5d                   	pop    %ebp
80108038:	c3                   	ret    

80108039 <sys_getgid>:

int
sys_getgid(void)
{
80108039:	55                   	push   %ebp
8010803a:	89 e5                	mov    %esp,%ebp
  //return the gid in the proc struct
  return proc->gid;
8010803c:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80108042:	8b 40 18             	mov    0x18(%eax),%eax
}
80108045:	5d                   	pop    %ebp
80108046:	c3                   	ret    

80108047 <sys_getppid>:

int
sys_getppid(void)
{
80108047:	55                   	push   %ebp
80108048:	89 e5                	mov    %esp,%ebp
  if(proc->parent) //if the proc has a parent return its pid
8010804a:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80108050:	8b 40 1c             	mov    0x1c(%eax),%eax
80108053:	85 c0                	test   %eax,%eax
80108055:	74 0e                	je     80108065 <sys_getppid+0x1e>
  {
     return proc->parent->pid;
80108057:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010805d:	8b 40 1c             	mov    0x1c(%eax),%eax
80108060:	8b 40 10             	mov    0x10(%eax),%eax
80108063:	eb 09                	jmp    8010806e <sys_getppid+0x27>
  }
  else //else proc is a parent and return the pid
  {
    return proc->pid;
80108065:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010806b:	8b 40 10             	mov    0x10(%eax),%eax
  }
}
8010806e:	5d                   	pop    %ebp
8010806f:	c3                   	ret    

80108070 <sys_setuid>:

int
sys_setuid(void)
{
80108070:	55                   	push   %ebp
80108071:	89 e5                	mov    %esp,%ebp
80108073:	83 ec 18             	sub    $0x18,%esp
  int uid;
  
  //retrieve an int off the stack
  if(argint(0, (void*)&uid) < 0)
80108076:	83 ec 08             	sub    $0x8,%esp
80108079:	8d 45 f4             	lea    -0xc(%ebp),%eax
8010807c:	50                   	push   %eax
8010807d:	6a 00                	push   $0x0
8010807f:	e8 39 ef ff ff       	call   80106fbd <argint>
80108084:	83 c4 10             	add    $0x10,%esp
80108087:	85 c0                	test   %eax,%eax
80108089:	79 07                	jns    80108092 <sys_setuid+0x22>
  {
    return -1; //return error if the int is invalid
8010808b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80108090:	eb 29                	jmp    801080bb <sys_setuid+0x4b>
  }
  if(uid >= 0 && uid <= 32767) //check that it's in valid range
80108092:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108095:	85 c0                	test   %eax,%eax
80108097:	78 1d                	js     801080b6 <sys_setuid+0x46>
80108099:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010809c:	3d ff 7f 00 00       	cmp    $0x7fff,%eax
801080a1:	7f 13                	jg     801080b6 <sys_setuid+0x46>
  { 
    proc->uid = uid; //set the uid in the proc struct
801080a3:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801080a9:	8b 55 f4             	mov    -0xc(%ebp),%edx
801080ac:	89 50 14             	mov    %edx,0x14(%eax)
    return 0; //return normal
801080af:	b8 00 00 00 00       	mov    $0x0,%eax
801080b4:	eb 05                	jmp    801080bb <sys_setuid+0x4b>
  }
  else
  { 
    return -1; //uid is invalid number
801080b6:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  }
}
801080bb:	c9                   	leave  
801080bc:	c3                   	ret    

801080bd <sys_setgid>:

int
sys_setgid(void)
{
801080bd:	55                   	push   %ebp
801080be:	89 e5                	mov    %esp,%ebp
801080c0:	83 ec 18             	sub    $0x18,%esp
  int gid;

  //retrieve and int off the stack
  if(argint(0, (void*)&gid) < 0)
801080c3:	83 ec 08             	sub    $0x8,%esp
801080c6:	8d 45 f4             	lea    -0xc(%ebp),%eax
801080c9:	50                   	push   %eax
801080ca:	6a 00                	push   $0x0
801080cc:	e8 ec ee ff ff       	call   80106fbd <argint>
801080d1:	83 c4 10             	add    $0x10,%esp
801080d4:	85 c0                	test   %eax,%eax
801080d6:	79 07                	jns    801080df <sys_setgid+0x22>
  {
    return -1; //return error if the int is invalid
801080d8:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801080dd:	eb 29                	jmp    80108108 <sys_setgid+0x4b>
  }
  if(gid >= 0 && gid <= 32767) //check for validity
801080df:	8b 45 f4             	mov    -0xc(%ebp),%eax
801080e2:	85 c0                	test   %eax,%eax
801080e4:	78 1d                	js     80108103 <sys_setgid+0x46>
801080e6:	8b 45 f4             	mov    -0xc(%ebp),%eax
801080e9:	3d ff 7f 00 00       	cmp    $0x7fff,%eax
801080ee:	7f 13                	jg     80108103 <sys_setgid+0x46>
  {
    proc->gid = gid; //set the gid in the proc struct
801080f0:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801080f6:	8b 55 f4             	mov    -0xc(%ebp),%edx
801080f9:	89 50 18             	mov    %edx,0x18(%eax)
    return 0;
801080fc:	b8 00 00 00 00       	mov    $0x0,%eax
80108101:	eb 05                	jmp    80108108 <sys_setgid+0x4b>
  }
  else
  {
    return -1; //gid id invalid number
80108103:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  }
}
80108108:	c9                   	leave  
80108109:	c3                   	ret    

8010810a <sys_getprocs>:

int
sys_getprocs(void)
{
8010810a:	55                   	push   %ebp
8010810b:	89 e5                	mov    %esp,%ebp
8010810d:	83 ec 18             	sub    $0x18,%esp
  int max;
  struct uproc* table;

  if (argint(0, (void*)&max) < 0)
80108110:	83 ec 08             	sub    $0x8,%esp
80108113:	8d 45 f4             	lea    -0xc(%ebp),%eax
80108116:	50                   	push   %eax
80108117:	6a 00                	push   $0x0
80108119:	e8 9f ee ff ff       	call   80106fbd <argint>
8010811e:	83 c4 10             	add    $0x10,%esp
80108121:	85 c0                	test   %eax,%eax
80108123:	79 07                	jns    8010812c <sys_getprocs+0x22>
    return -1;
80108125:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010812a:	eb 31                	jmp    8010815d <sys_getprocs+0x53>
  if (argptr(1, (void*)&table, sizeof(*table)) < 0)
8010812c:	83 ec 04             	sub    $0x4,%esp
8010812f:	6a 60                	push   $0x60
80108131:	8d 45 f0             	lea    -0x10(%ebp),%eax
80108134:	50                   	push   %eax
80108135:	6a 01                	push   $0x1
80108137:	e8 a9 ee ff ff       	call   80106fe5 <argptr>
8010813c:	83 c4 10             	add    $0x10,%esp
8010813f:	85 c0                	test   %eax,%eax
80108141:	79 07                	jns    8010814a <sys_getprocs+0x40>
    return -1;
80108143:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80108148:	eb 13                	jmp    8010815d <sys_getprocs+0x53>
  return getProcInfo(max, table); // help function to get proc info
8010814a:	8b 55 f0             	mov    -0x10(%ebp),%edx
8010814d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108150:	83 ec 08             	sub    $0x8,%esp
80108153:	52                   	push   %edx
80108154:	50                   	push   %eax
80108155:	e8 b0 d8 ff ff       	call   80105a0a <getProcInfo>
8010815a:	83 c4 10             	add    $0x10,%esp
}
8010815d:	c9                   	leave  
8010815e:	c3                   	ret    

8010815f <sys_setpriority>:
//========== Project 2 System Calls End ====================
int 
sys_setpriority(void)
{
8010815f:	55                   	push   %ebp
80108160:	89 e5                	mov    %esp,%ebp
80108162:	83 ec 18             	sub    $0x18,%esp
  int pid, priority;

  if(argint(0, (void*)&pid) < 0)
80108165:	83 ec 08             	sub    $0x8,%esp
80108168:	8d 45 f4             	lea    -0xc(%ebp),%eax
8010816b:	50                   	push   %eax
8010816c:	6a 00                	push   $0x0
8010816e:	e8 4a ee ff ff       	call   80106fbd <argint>
80108173:	83 c4 10             	add    $0x10,%esp
80108176:	85 c0                	test   %eax,%eax
80108178:	79 07                	jns    80108181 <sys_setpriority+0x22>
    return -1;
8010817a:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010817f:	eb 2f                	jmp    801081b0 <sys_setpriority+0x51>
  if(argint(1, (void*)&priority) < 0)
80108181:	83 ec 08             	sub    $0x8,%esp
80108184:	8d 45 f0             	lea    -0x10(%ebp),%eax
80108187:	50                   	push   %eax
80108188:	6a 01                	push   $0x1
8010818a:	e8 2e ee ff ff       	call   80106fbd <argint>
8010818f:	83 c4 10             	add    $0x10,%esp
80108192:	85 c0                	test   %eax,%eax
80108194:	79 07                	jns    8010819d <sys_setpriority+0x3e>
    return -1;
80108196:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010819b:	eb 13                	jmp    801081b0 <sys_setpriority+0x51>
  return setpriority(pid, priority);
8010819d:	8b 55 f0             	mov    -0x10(%ebp),%edx
801081a0:	8b 45 f4             	mov    -0xc(%ebp),%eax
801081a3:	83 ec 08             	sub    $0x8,%esp
801081a6:	52                   	push   %edx
801081a7:	50                   	push   %eax
801081a8:	e8 58 da ff ff       	call   80105c05 <setpriority>
801081ad:	83 c4 10             	add    $0x10,%esp
}
801081b0:	c9                   	leave  
801081b1:	c3                   	ret    

801081b2 <outb>:
               "memory", "cc");
}

static inline void
outb(ushort port, uchar data)
{
801081b2:	55                   	push   %ebp
801081b3:	89 e5                	mov    %esp,%ebp
801081b5:	83 ec 08             	sub    $0x8,%esp
801081b8:	8b 55 08             	mov    0x8(%ebp),%edx
801081bb:	8b 45 0c             	mov    0xc(%ebp),%eax
801081be:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
801081c2:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801081c5:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
801081c9:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
801081cd:	ee                   	out    %al,(%dx)
}
801081ce:	90                   	nop
801081cf:	c9                   	leave  
801081d0:	c3                   	ret    

801081d1 <timerinit>:
#define TIMER_RATEGEN   0x04    // mode 2, rate generator
#define TIMER_16BIT     0x30    // r/w counter 16 bits, LSB first

void
timerinit(void)
{
801081d1:	55                   	push   %ebp
801081d2:	89 e5                	mov    %esp,%ebp
801081d4:	83 ec 08             	sub    $0x8,%esp
  // Interrupt 100 times/sec.
  outb(TIMER_MODE, TIMER_SEL0 | TIMER_RATEGEN | TIMER_16BIT);
801081d7:	6a 34                	push   $0x34
801081d9:	6a 43                	push   $0x43
801081db:	e8 d2 ff ff ff       	call   801081b2 <outb>
801081e0:	83 c4 08             	add    $0x8,%esp
  outb(IO_TIMER1, TIMER_DIV(100) % 256);
801081e3:	68 9c 00 00 00       	push   $0x9c
801081e8:	6a 40                	push   $0x40
801081ea:	e8 c3 ff ff ff       	call   801081b2 <outb>
801081ef:	83 c4 08             	add    $0x8,%esp
  outb(IO_TIMER1, TIMER_DIV(100) / 256);
801081f2:	6a 2e                	push   $0x2e
801081f4:	6a 40                	push   $0x40
801081f6:	e8 b7 ff ff ff       	call   801081b2 <outb>
801081fb:	83 c4 08             	add    $0x8,%esp
  picenable(IRQ_TIMER);
801081fe:	83 ec 0c             	sub    $0xc,%esp
80108201:	6a 00                	push   $0x0
80108203:	e8 c1 bd ff ff       	call   80103fc9 <picenable>
80108208:	83 c4 10             	add    $0x10,%esp
}
8010820b:	90                   	nop
8010820c:	c9                   	leave  
8010820d:	c3                   	ret    

8010820e <alltraps>:

  # vectors.S sends all traps here.
.globl alltraps
alltraps:
  # Build trap frame.
  pushl %ds
8010820e:	1e                   	push   %ds
  pushl %es
8010820f:	06                   	push   %es
  pushl %fs
80108210:	0f a0                	push   %fs
  pushl %gs
80108212:	0f a8                	push   %gs
  pushal
80108214:	60                   	pusha  
  
  # Set up data and per-cpu segments.
  movw $(SEG_KDATA<<3), %ax
80108215:	66 b8 10 00          	mov    $0x10,%ax
  movw %ax, %ds
80108219:	8e d8                	mov    %eax,%ds
  movw %ax, %es
8010821b:	8e c0                	mov    %eax,%es
  movw $(SEG_KCPU<<3), %ax
8010821d:	66 b8 18 00          	mov    $0x18,%ax
  movw %ax, %fs
80108221:	8e e0                	mov    %eax,%fs
  movw %ax, %gs
80108223:	8e e8                	mov    %eax,%gs

  # Call trap(tf), where tf=%esp
  pushl %esp
80108225:	54                   	push   %esp
  call trap
80108226:	e8 ce 01 00 00       	call   801083f9 <trap>
  addl $4, %esp
8010822b:	83 c4 04             	add    $0x4,%esp

8010822e <trapret>:

  # Return falls through to trapret...
.globl trapret
trapret:
  popal
8010822e:	61                   	popa   
  popl %gs
8010822f:	0f a9                	pop    %gs
  popl %fs
80108231:	0f a1                	pop    %fs
  popl %es
80108233:	07                   	pop    %es
  popl %ds
80108234:	1f                   	pop    %ds
  addl $0x8, %esp  # trapno and errcode
80108235:	83 c4 08             	add    $0x8,%esp
  iret
80108238:	cf                   	iret   

80108239 <atom_inc>:

// Routines added for CS333
// atom_inc() added to simplify handling of ticks global
static inline void
atom_inc(volatile int *num)
{
80108239:	55                   	push   %ebp
8010823a:	89 e5                	mov    %esp,%ebp
  asm volatile ( "lock incl %0" : "=m" (*num));
8010823c:	8b 45 08             	mov    0x8(%ebp),%eax
8010823f:	f0 ff 00             	lock incl (%eax)
}
80108242:	90                   	nop
80108243:	5d                   	pop    %ebp
80108244:	c3                   	ret    

80108245 <lidt>:

struct gatedesc;

static inline void
lidt(struct gatedesc *p, int size)
{
80108245:	55                   	push   %ebp
80108246:	89 e5                	mov    %esp,%ebp
80108248:	83 ec 10             	sub    $0x10,%esp
  volatile ushort pd[3];

  pd[0] = size-1;
8010824b:	8b 45 0c             	mov    0xc(%ebp),%eax
8010824e:	83 e8 01             	sub    $0x1,%eax
80108251:	66 89 45 fa          	mov    %ax,-0x6(%ebp)
  pd[1] = (uint)p;
80108255:	8b 45 08             	mov    0x8(%ebp),%eax
80108258:	66 89 45 fc          	mov    %ax,-0x4(%ebp)
  pd[2] = (uint)p >> 16;
8010825c:	8b 45 08             	mov    0x8(%ebp),%eax
8010825f:	c1 e8 10             	shr    $0x10,%eax
80108262:	66 89 45 fe          	mov    %ax,-0x2(%ebp)

  asm volatile("lidt (%0)" : : "r" (pd));
80108266:	8d 45 fa             	lea    -0x6(%ebp),%eax
80108269:	0f 01 18             	lidtl  (%eax)
}
8010826c:	90                   	nop
8010826d:	c9                   	leave  
8010826e:	c3                   	ret    

8010826f <rcr2>:
  return result;
}

static inline uint
rcr2(void)
{
8010826f:	55                   	push   %ebp
80108270:	89 e5                	mov    %esp,%ebp
80108272:	83 ec 10             	sub    $0x10,%esp
  uint val;
  asm volatile("movl %%cr2,%0" : "=r" (val));
80108275:	0f 20 d0             	mov    %cr2,%eax
80108278:	89 45 fc             	mov    %eax,-0x4(%ebp)
  return val;
8010827b:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
8010827e:	c9                   	leave  
8010827f:	c3                   	ret    

80108280 <tvinit>:
// Software Developer’s Manual, Vol 3A, 8.1.1 Guaranteed Atomic Operations.
uint ticks __attribute__ ((aligned (4)));

void
tvinit(void)
{
80108280:	55                   	push   %ebp
80108281:	89 e5                	mov    %esp,%ebp
80108283:	83 ec 10             	sub    $0x10,%esp
  int i;

  for(i = 0; i < 256; i++)
80108286:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
8010828d:	e9 c3 00 00 00       	jmp    80108355 <tvinit+0xd5>
    SETGATE(idt[i], 0, SEG_KCODE<<3, vectors[i], 0);
80108292:	8b 45 fc             	mov    -0x4(%ebp),%eax
80108295:	8b 04 85 bc d0 10 80 	mov    -0x7fef2f44(,%eax,4),%eax
8010829c:	89 c2                	mov    %eax,%edx
8010829e:	8b 45 fc             	mov    -0x4(%ebp),%eax
801082a1:	66 89 14 c5 00 71 11 	mov    %dx,-0x7fee8f00(,%eax,8)
801082a8:	80 
801082a9:	8b 45 fc             	mov    -0x4(%ebp),%eax
801082ac:	66 c7 04 c5 02 71 11 	movw   $0x8,-0x7fee8efe(,%eax,8)
801082b3:	80 08 00 
801082b6:	8b 45 fc             	mov    -0x4(%ebp),%eax
801082b9:	0f b6 14 c5 04 71 11 	movzbl -0x7fee8efc(,%eax,8),%edx
801082c0:	80 
801082c1:	83 e2 e0             	and    $0xffffffe0,%edx
801082c4:	88 14 c5 04 71 11 80 	mov    %dl,-0x7fee8efc(,%eax,8)
801082cb:	8b 45 fc             	mov    -0x4(%ebp),%eax
801082ce:	0f b6 14 c5 04 71 11 	movzbl -0x7fee8efc(,%eax,8),%edx
801082d5:	80 
801082d6:	83 e2 1f             	and    $0x1f,%edx
801082d9:	88 14 c5 04 71 11 80 	mov    %dl,-0x7fee8efc(,%eax,8)
801082e0:	8b 45 fc             	mov    -0x4(%ebp),%eax
801082e3:	0f b6 14 c5 05 71 11 	movzbl -0x7fee8efb(,%eax,8),%edx
801082ea:	80 
801082eb:	83 e2 f0             	and    $0xfffffff0,%edx
801082ee:	83 ca 0e             	or     $0xe,%edx
801082f1:	88 14 c5 05 71 11 80 	mov    %dl,-0x7fee8efb(,%eax,8)
801082f8:	8b 45 fc             	mov    -0x4(%ebp),%eax
801082fb:	0f b6 14 c5 05 71 11 	movzbl -0x7fee8efb(,%eax,8),%edx
80108302:	80 
80108303:	83 e2 ef             	and    $0xffffffef,%edx
80108306:	88 14 c5 05 71 11 80 	mov    %dl,-0x7fee8efb(,%eax,8)
8010830d:	8b 45 fc             	mov    -0x4(%ebp),%eax
80108310:	0f b6 14 c5 05 71 11 	movzbl -0x7fee8efb(,%eax,8),%edx
80108317:	80 
80108318:	83 e2 9f             	and    $0xffffff9f,%edx
8010831b:	88 14 c5 05 71 11 80 	mov    %dl,-0x7fee8efb(,%eax,8)
80108322:	8b 45 fc             	mov    -0x4(%ebp),%eax
80108325:	0f b6 14 c5 05 71 11 	movzbl -0x7fee8efb(,%eax,8),%edx
8010832c:	80 
8010832d:	83 ca 80             	or     $0xffffff80,%edx
80108330:	88 14 c5 05 71 11 80 	mov    %dl,-0x7fee8efb(,%eax,8)
80108337:	8b 45 fc             	mov    -0x4(%ebp),%eax
8010833a:	8b 04 85 bc d0 10 80 	mov    -0x7fef2f44(,%eax,4),%eax
80108341:	c1 e8 10             	shr    $0x10,%eax
80108344:	89 c2                	mov    %eax,%edx
80108346:	8b 45 fc             	mov    -0x4(%ebp),%eax
80108349:	66 89 14 c5 06 71 11 	mov    %dx,-0x7fee8efa(,%eax,8)
80108350:	80 
void
tvinit(void)
{
  int i;

  for(i = 0; i < 256; i++)
80108351:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
80108355:	81 7d fc ff 00 00 00 	cmpl   $0xff,-0x4(%ebp)
8010835c:	0f 8e 30 ff ff ff    	jle    80108292 <tvinit+0x12>
    SETGATE(idt[i], 0, SEG_KCODE<<3, vectors[i], 0);
  SETGATE(idt[T_SYSCALL], 1, SEG_KCODE<<3, vectors[T_SYSCALL], DPL_USER);
80108362:	a1 bc d1 10 80       	mov    0x8010d1bc,%eax
80108367:	66 a3 00 73 11 80    	mov    %ax,0x80117300
8010836d:	66 c7 05 02 73 11 80 	movw   $0x8,0x80117302
80108374:	08 00 
80108376:	0f b6 05 04 73 11 80 	movzbl 0x80117304,%eax
8010837d:	83 e0 e0             	and    $0xffffffe0,%eax
80108380:	a2 04 73 11 80       	mov    %al,0x80117304
80108385:	0f b6 05 04 73 11 80 	movzbl 0x80117304,%eax
8010838c:	83 e0 1f             	and    $0x1f,%eax
8010838f:	a2 04 73 11 80       	mov    %al,0x80117304
80108394:	0f b6 05 05 73 11 80 	movzbl 0x80117305,%eax
8010839b:	83 c8 0f             	or     $0xf,%eax
8010839e:	a2 05 73 11 80       	mov    %al,0x80117305
801083a3:	0f b6 05 05 73 11 80 	movzbl 0x80117305,%eax
801083aa:	83 e0 ef             	and    $0xffffffef,%eax
801083ad:	a2 05 73 11 80       	mov    %al,0x80117305
801083b2:	0f b6 05 05 73 11 80 	movzbl 0x80117305,%eax
801083b9:	83 c8 60             	or     $0x60,%eax
801083bc:	a2 05 73 11 80       	mov    %al,0x80117305
801083c1:	0f b6 05 05 73 11 80 	movzbl 0x80117305,%eax
801083c8:	83 c8 80             	or     $0xffffff80,%eax
801083cb:	a2 05 73 11 80       	mov    %al,0x80117305
801083d0:	a1 bc d1 10 80       	mov    0x8010d1bc,%eax
801083d5:	c1 e8 10             	shr    $0x10,%eax
801083d8:	66 a3 06 73 11 80    	mov    %ax,0x80117306
  
}
801083de:	90                   	nop
801083df:	c9                   	leave  
801083e0:	c3                   	ret    

801083e1 <idtinit>:

void
idtinit(void)
{
801083e1:	55                   	push   %ebp
801083e2:	89 e5                	mov    %esp,%ebp
  lidt(idt, sizeof(idt));
801083e4:	68 00 08 00 00       	push   $0x800
801083e9:	68 00 71 11 80       	push   $0x80117100
801083ee:	e8 52 fe ff ff       	call   80108245 <lidt>
801083f3:	83 c4 08             	add    $0x8,%esp
}
801083f6:	90                   	nop
801083f7:	c9                   	leave  
801083f8:	c3                   	ret    

801083f9 <trap>:

void
trap(struct trapframe *tf)
{
801083f9:	55                   	push   %ebp
801083fa:	89 e5                	mov    %esp,%ebp
801083fc:	57                   	push   %edi
801083fd:	56                   	push   %esi
801083fe:	53                   	push   %ebx
801083ff:	83 ec 1c             	sub    $0x1c,%esp
  if(tf->trapno == T_SYSCALL){
80108402:	8b 45 08             	mov    0x8(%ebp),%eax
80108405:	8b 40 30             	mov    0x30(%eax),%eax
80108408:	83 f8 40             	cmp    $0x40,%eax
8010840b:	75 3e                	jne    8010844b <trap+0x52>
    if(proc->killed)
8010840d:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80108413:	8b 40 2c             	mov    0x2c(%eax),%eax
80108416:	85 c0                	test   %eax,%eax
80108418:	74 05                	je     8010841f <trap+0x26>
      exit();
8010841a:	e8 f8 c7 ff ff       	call   80104c17 <exit>
    proc->tf = tf;
8010841f:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80108425:	8b 55 08             	mov    0x8(%ebp),%edx
80108428:	89 50 20             	mov    %edx,0x20(%eax)
    syscall();
8010842b:	e8 43 ec ff ff       	call   80107073 <syscall>
    if(proc->killed)
80108430:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80108436:	8b 40 2c             	mov    0x2c(%eax),%eax
80108439:	85 c0                	test   %eax,%eax
8010843b:	0f 84 fe 01 00 00    	je     8010863f <trap+0x246>
      exit();
80108441:	e8 d1 c7 ff ff       	call   80104c17 <exit>
    return;
80108446:	e9 f4 01 00 00       	jmp    8010863f <trap+0x246>
  }

  switch(tf->trapno){
8010844b:	8b 45 08             	mov    0x8(%ebp),%eax
8010844e:	8b 40 30             	mov    0x30(%eax),%eax
80108451:	83 e8 20             	sub    $0x20,%eax
80108454:	83 f8 1f             	cmp    $0x1f,%eax
80108457:	0f 87 a3 00 00 00    	ja     80108500 <trap+0x107>
8010845d:	8b 04 85 20 ae 10 80 	mov    -0x7fef51e0(,%eax,4),%eax
80108464:	ff e0                	jmp    *%eax
  case T_IRQ0 + IRQ_TIMER:
   if(cpu->id == 0){
80108466:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
8010846c:	0f b6 00             	movzbl (%eax),%eax
8010846f:	84 c0                	test   %al,%al
80108471:	75 20                	jne    80108493 <trap+0x9a>
      atom_inc((int *)&ticks);   // guaranteed atomic so no lock necessary
80108473:	83 ec 0c             	sub    $0xc,%esp
80108476:	68 00 79 11 80       	push   $0x80117900
8010847b:	e8 b9 fd ff ff       	call   80108239 <atom_inc>
80108480:	83 c4 10             	add    $0x10,%esp
      wakeup(&ticks);
80108483:	83 ec 0c             	sub    $0xc,%esp
80108486:	68 00 79 11 80       	push   $0x80117900
8010848b:	e8 05 d1 ff ff       	call   80105595 <wakeup>
80108490:	83 c4 10             	add    $0x10,%esp
    }
    lapiceoi();
80108493:	e8 2b ac ff ff       	call   801030c3 <lapiceoi>
    break;
80108498:	e9 1c 01 00 00       	jmp    801085b9 <trap+0x1c0>
  case T_IRQ0 + IRQ_IDE:
    ideintr();
8010849d:	e8 34 a4 ff ff       	call   801028d6 <ideintr>
    lapiceoi();
801084a2:	e8 1c ac ff ff       	call   801030c3 <lapiceoi>
    break;
801084a7:	e9 0d 01 00 00       	jmp    801085b9 <trap+0x1c0>
  case T_IRQ0 + IRQ_IDE+1:
    // Bochs generates spurious IDE1 interrupts.
    break;
  case T_IRQ0 + IRQ_KBD:
    kbdintr();
801084ac:	e8 14 aa ff ff       	call   80102ec5 <kbdintr>
    lapiceoi();
801084b1:	e8 0d ac ff ff       	call   801030c3 <lapiceoi>
    break;
801084b6:	e9 fe 00 00 00       	jmp    801085b9 <trap+0x1c0>
  case T_IRQ0 + IRQ_COM1:
    uartintr();
801084bb:	e8 60 03 00 00       	call   80108820 <uartintr>
    lapiceoi();
801084c0:	e8 fe ab ff ff       	call   801030c3 <lapiceoi>
    break;
801084c5:	e9 ef 00 00 00       	jmp    801085b9 <trap+0x1c0>
  case T_IRQ0 + 7:
  case T_IRQ0 + IRQ_SPURIOUS:
    cprintf("cpu%d: spurious interrupt at %x:%x\n",
801084ca:	8b 45 08             	mov    0x8(%ebp),%eax
801084cd:	8b 48 38             	mov    0x38(%eax),%ecx
            cpu->id, tf->cs, tf->eip);
801084d0:	8b 45 08             	mov    0x8(%ebp),%eax
801084d3:	0f b7 40 3c          	movzwl 0x3c(%eax),%eax
    uartintr();
    lapiceoi();
    break;
  case T_IRQ0 + 7:
  case T_IRQ0 + IRQ_SPURIOUS:
    cprintf("cpu%d: spurious interrupt at %x:%x\n",
801084d7:	0f b7 d0             	movzwl %ax,%edx
            cpu->id, tf->cs, tf->eip);
801084da:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
801084e0:	0f b6 00             	movzbl (%eax),%eax
    uartintr();
    lapiceoi();
    break;
  case T_IRQ0 + 7:
  case T_IRQ0 + IRQ_SPURIOUS:
    cprintf("cpu%d: spurious interrupt at %x:%x\n",
801084e3:	0f b6 c0             	movzbl %al,%eax
801084e6:	51                   	push   %ecx
801084e7:	52                   	push   %edx
801084e8:	50                   	push   %eax
801084e9:	68 80 ad 10 80       	push   $0x8010ad80
801084ee:	e8 d3 7e ff ff       	call   801003c6 <cprintf>
801084f3:	83 c4 10             	add    $0x10,%esp
            cpu->id, tf->cs, tf->eip);
    lapiceoi();
801084f6:	e8 c8 ab ff ff       	call   801030c3 <lapiceoi>
    break;
801084fb:	e9 b9 00 00 00       	jmp    801085b9 <trap+0x1c0>
   
  default:
    if(proc == 0 || (tf->cs&3) == 0){
80108500:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80108506:	85 c0                	test   %eax,%eax
80108508:	74 11                	je     8010851b <trap+0x122>
8010850a:	8b 45 08             	mov    0x8(%ebp),%eax
8010850d:	0f b7 40 3c          	movzwl 0x3c(%eax),%eax
80108511:	0f b7 c0             	movzwl %ax,%eax
80108514:	83 e0 03             	and    $0x3,%eax
80108517:	85 c0                	test   %eax,%eax
80108519:	75 40                	jne    8010855b <trap+0x162>
      // In kernel, it must be our mistake.
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
8010851b:	e8 4f fd ff ff       	call   8010826f <rcr2>
80108520:	89 c3                	mov    %eax,%ebx
80108522:	8b 45 08             	mov    0x8(%ebp),%eax
80108525:	8b 48 38             	mov    0x38(%eax),%ecx
              tf->trapno, cpu->id, tf->eip, rcr2());
80108528:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
8010852e:	0f b6 00             	movzbl (%eax),%eax
    break;
   
  default:
    if(proc == 0 || (tf->cs&3) == 0){
      // In kernel, it must be our mistake.
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
80108531:	0f b6 d0             	movzbl %al,%edx
80108534:	8b 45 08             	mov    0x8(%ebp),%eax
80108537:	8b 40 30             	mov    0x30(%eax),%eax
8010853a:	83 ec 0c             	sub    $0xc,%esp
8010853d:	53                   	push   %ebx
8010853e:	51                   	push   %ecx
8010853f:	52                   	push   %edx
80108540:	50                   	push   %eax
80108541:	68 a4 ad 10 80       	push   $0x8010ada4
80108546:	e8 7b 7e ff ff       	call   801003c6 <cprintf>
8010854b:	83 c4 20             	add    $0x20,%esp
              tf->trapno, cpu->id, tf->eip, rcr2());
      panic("trap");
8010854e:	83 ec 0c             	sub    $0xc,%esp
80108551:	68 d6 ad 10 80       	push   $0x8010add6
80108556:	e8 0b 80 ff ff       	call   80100566 <panic>
    }
    // In user space, assume process misbehaved.
    cprintf("pid %d %s: trap %d err %d on cpu %d "
8010855b:	e8 0f fd ff ff       	call   8010826f <rcr2>
80108560:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80108563:	8b 45 08             	mov    0x8(%ebp),%eax
80108566:	8b 70 38             	mov    0x38(%eax),%esi
            "eip 0x%x addr 0x%x--kill proc\n",
            proc->pid, proc->name, tf->trapno, tf->err, cpu->id, tf->eip, 
80108569:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
8010856f:	0f b6 00             	movzbl (%eax),%eax
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
              tf->trapno, cpu->id, tf->eip, rcr2());
      panic("trap");
    }
    // In user space, assume process misbehaved.
    cprintf("pid %d %s: trap %d err %d on cpu %d "
80108572:	0f b6 d8             	movzbl %al,%ebx
80108575:	8b 45 08             	mov    0x8(%ebp),%eax
80108578:	8b 48 34             	mov    0x34(%eax),%ecx
8010857b:	8b 45 08             	mov    0x8(%ebp),%eax
8010857e:	8b 50 30             	mov    0x30(%eax),%edx
            "eip 0x%x addr 0x%x--kill proc\n",
            proc->pid, proc->name, tf->trapno, tf->err, cpu->id, tf->eip, 
80108581:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80108587:	8d 78 74             	lea    0x74(%eax),%edi
8010858a:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
              tf->trapno, cpu->id, tf->eip, rcr2());
      panic("trap");
    }
    // In user space, assume process misbehaved.
    cprintf("pid %d %s: trap %d err %d on cpu %d "
80108590:	8b 40 10             	mov    0x10(%eax),%eax
80108593:	ff 75 e4             	pushl  -0x1c(%ebp)
80108596:	56                   	push   %esi
80108597:	53                   	push   %ebx
80108598:	51                   	push   %ecx
80108599:	52                   	push   %edx
8010859a:	57                   	push   %edi
8010859b:	50                   	push   %eax
8010859c:	68 dc ad 10 80       	push   $0x8010addc
801085a1:	e8 20 7e ff ff       	call   801003c6 <cprintf>
801085a6:	83 c4 20             	add    $0x20,%esp
            "eip 0x%x addr 0x%x--kill proc\n",
            proc->pid, proc->name, tf->trapno, tf->err, cpu->id, tf->eip, 
            rcr2());
    proc->killed = 1;
801085a9:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801085af:	c7 40 2c 01 00 00 00 	movl   $0x1,0x2c(%eax)
801085b6:	eb 01                	jmp    801085b9 <trap+0x1c0>
    ideintr();
    lapiceoi();
    break;
  case T_IRQ0 + IRQ_IDE+1:
    // Bochs generates spurious IDE1 interrupts.
    break;
801085b8:	90                   	nop
  }

  // Force process exit if it has been killed and is in user space.
  // (If it is still executing in the kernel, let it keep running 
  // until it gets to the regular system call return.)
  if(proc && proc->killed && (tf->cs&3) == DPL_USER)
801085b9:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801085bf:	85 c0                	test   %eax,%eax
801085c1:	74 24                	je     801085e7 <trap+0x1ee>
801085c3:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801085c9:	8b 40 2c             	mov    0x2c(%eax),%eax
801085cc:	85 c0                	test   %eax,%eax
801085ce:	74 17                	je     801085e7 <trap+0x1ee>
801085d0:	8b 45 08             	mov    0x8(%ebp),%eax
801085d3:	0f b7 40 3c          	movzwl 0x3c(%eax),%eax
801085d7:	0f b7 c0             	movzwl %ax,%eax
801085da:	83 e0 03             	and    $0x3,%eax
801085dd:	83 f8 03             	cmp    $0x3,%eax
801085e0:	75 05                	jne    801085e7 <trap+0x1ee>
    exit();
801085e2:	e8 30 c6 ff ff       	call   80104c17 <exit>

  // Force process to give up CPU on clock tick.
  // If interrupts were on while locks held, would need to check nlock.
  if(proc && proc->state == RUNNING && tf->trapno == T_IRQ0+IRQ_TIMER)
801085e7:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801085ed:	85 c0                	test   %eax,%eax
801085ef:	74 1e                	je     8010860f <trap+0x216>
801085f1:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801085f7:	8b 40 0c             	mov    0xc(%eax),%eax
801085fa:	83 f8 04             	cmp    $0x4,%eax
801085fd:	75 10                	jne    8010860f <trap+0x216>
801085ff:	8b 45 08             	mov    0x8(%ebp),%eax
80108602:	8b 40 30             	mov    0x30(%eax),%eax
80108605:	83 f8 20             	cmp    $0x20,%eax
80108608:	75 05                	jne    8010860f <trap+0x216>
    yield();
8010860a:	e8 01 cd ff ff       	call   80105310 <yield>

  // Check if the process has been killed since we yielded
  if(proc && proc->killed && (tf->cs&3) == DPL_USER)
8010860f:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80108615:	85 c0                	test   %eax,%eax
80108617:	74 27                	je     80108640 <trap+0x247>
80108619:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010861f:	8b 40 2c             	mov    0x2c(%eax),%eax
80108622:	85 c0                	test   %eax,%eax
80108624:	74 1a                	je     80108640 <trap+0x247>
80108626:	8b 45 08             	mov    0x8(%ebp),%eax
80108629:	0f b7 40 3c          	movzwl 0x3c(%eax),%eax
8010862d:	0f b7 c0             	movzwl %ax,%eax
80108630:	83 e0 03             	and    $0x3,%eax
80108633:	83 f8 03             	cmp    $0x3,%eax
80108636:	75 08                	jne    80108640 <trap+0x247>
    exit();
80108638:	e8 da c5 ff ff       	call   80104c17 <exit>
8010863d:	eb 01                	jmp    80108640 <trap+0x247>
      exit();
    proc->tf = tf;
    syscall();
    if(proc->killed)
      exit();
    return;
8010863f:	90                   	nop
    yield();

  // Check if the process has been killed since we yielded
  if(proc && proc->killed && (tf->cs&3) == DPL_USER)
    exit();
}
80108640:	8d 65 f4             	lea    -0xc(%ebp),%esp
80108643:	5b                   	pop    %ebx
80108644:	5e                   	pop    %esi
80108645:	5f                   	pop    %edi
80108646:	5d                   	pop    %ebp
80108647:	c3                   	ret    

80108648 <inb>:

// end of CS333 added routines

static inline uchar
inb(ushort port)
{
80108648:	55                   	push   %ebp
80108649:	89 e5                	mov    %esp,%ebp
8010864b:	83 ec 14             	sub    $0x14,%esp
8010864e:	8b 45 08             	mov    0x8(%ebp),%eax
80108651:	66 89 45 ec          	mov    %ax,-0x14(%ebp)
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80108655:	0f b7 45 ec          	movzwl -0x14(%ebp),%eax
80108659:	89 c2                	mov    %eax,%edx
8010865b:	ec                   	in     (%dx),%al
8010865c:	88 45 ff             	mov    %al,-0x1(%ebp)
  return data;
8010865f:	0f b6 45 ff          	movzbl -0x1(%ebp),%eax
}
80108663:	c9                   	leave  
80108664:	c3                   	ret    

80108665 <outb>:
               "memory", "cc");
}

static inline void
outb(ushort port, uchar data)
{
80108665:	55                   	push   %ebp
80108666:	89 e5                	mov    %esp,%ebp
80108668:	83 ec 08             	sub    $0x8,%esp
8010866b:	8b 55 08             	mov    0x8(%ebp),%edx
8010866e:	8b 45 0c             	mov    0xc(%ebp),%eax
80108671:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
80108675:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80108678:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
8010867c:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
80108680:	ee                   	out    %al,(%dx)
}
80108681:	90                   	nop
80108682:	c9                   	leave  
80108683:	c3                   	ret    

80108684 <uartinit>:

static int uart;    // is there a uart?

void
uartinit(void)
{
80108684:	55                   	push   %ebp
80108685:	89 e5                	mov    %esp,%ebp
80108687:	83 ec 18             	sub    $0x18,%esp
  char *p;

  // Turn off the FIFO
  outb(COM1+2, 0);
8010868a:	6a 00                	push   $0x0
8010868c:	68 fa 03 00 00       	push   $0x3fa
80108691:	e8 cf ff ff ff       	call   80108665 <outb>
80108696:	83 c4 08             	add    $0x8,%esp
  
  // 9600 baud, 8 data bits, 1 stop bit, parity off.
  outb(COM1+3, 0x80);    // Unlock divisor
80108699:	68 80 00 00 00       	push   $0x80
8010869e:	68 fb 03 00 00       	push   $0x3fb
801086a3:	e8 bd ff ff ff       	call   80108665 <outb>
801086a8:	83 c4 08             	add    $0x8,%esp
  outb(COM1+0, 115200/9600);
801086ab:	6a 0c                	push   $0xc
801086ad:	68 f8 03 00 00       	push   $0x3f8
801086b2:	e8 ae ff ff ff       	call   80108665 <outb>
801086b7:	83 c4 08             	add    $0x8,%esp
  outb(COM1+1, 0);
801086ba:	6a 00                	push   $0x0
801086bc:	68 f9 03 00 00       	push   $0x3f9
801086c1:	e8 9f ff ff ff       	call   80108665 <outb>
801086c6:	83 c4 08             	add    $0x8,%esp
  outb(COM1+3, 0x03);    // Lock divisor, 8 data bits.
801086c9:	6a 03                	push   $0x3
801086cb:	68 fb 03 00 00       	push   $0x3fb
801086d0:	e8 90 ff ff ff       	call   80108665 <outb>
801086d5:	83 c4 08             	add    $0x8,%esp
  outb(COM1+4, 0);
801086d8:	6a 00                	push   $0x0
801086da:	68 fc 03 00 00       	push   $0x3fc
801086df:	e8 81 ff ff ff       	call   80108665 <outb>
801086e4:	83 c4 08             	add    $0x8,%esp
  outb(COM1+1, 0x01);    // Enable receive interrupts.
801086e7:	6a 01                	push   $0x1
801086e9:	68 f9 03 00 00       	push   $0x3f9
801086ee:	e8 72 ff ff ff       	call   80108665 <outb>
801086f3:	83 c4 08             	add    $0x8,%esp

  // If status is 0xFF, no serial port.
  if(inb(COM1+5) == 0xFF)
801086f6:	68 fd 03 00 00       	push   $0x3fd
801086fb:	e8 48 ff ff ff       	call   80108648 <inb>
80108700:	83 c4 04             	add    $0x4,%esp
80108703:	3c ff                	cmp    $0xff,%al
80108705:	74 6e                	je     80108775 <uartinit+0xf1>
    return;
  uart = 1;
80108707:	c7 05 6c d6 10 80 01 	movl   $0x1,0x8010d66c
8010870e:	00 00 00 

  // Acknowledge pre-existing interrupt conditions;
  // enable interrupts.
  inb(COM1+2);
80108711:	68 fa 03 00 00       	push   $0x3fa
80108716:	e8 2d ff ff ff       	call   80108648 <inb>
8010871b:	83 c4 04             	add    $0x4,%esp
  inb(COM1+0);
8010871e:	68 f8 03 00 00       	push   $0x3f8
80108723:	e8 20 ff ff ff       	call   80108648 <inb>
80108728:	83 c4 04             	add    $0x4,%esp
  picenable(IRQ_COM1);
8010872b:	83 ec 0c             	sub    $0xc,%esp
8010872e:	6a 04                	push   $0x4
80108730:	e8 94 b8 ff ff       	call   80103fc9 <picenable>
80108735:	83 c4 10             	add    $0x10,%esp
  ioapicenable(IRQ_COM1, 0);
80108738:	83 ec 08             	sub    $0x8,%esp
8010873b:	6a 00                	push   $0x0
8010873d:	6a 04                	push   $0x4
8010873f:	e8 34 a4 ff ff       	call   80102b78 <ioapicenable>
80108744:	83 c4 10             	add    $0x10,%esp
  
  // Announce that we're here.
  for(p="xv6...\n"; *p; p++)
80108747:	c7 45 f4 a0 ae 10 80 	movl   $0x8010aea0,-0xc(%ebp)
8010874e:	eb 19                	jmp    80108769 <uartinit+0xe5>
    uartputc(*p);
80108750:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108753:	0f b6 00             	movzbl (%eax),%eax
80108756:	0f be c0             	movsbl %al,%eax
80108759:	83 ec 0c             	sub    $0xc,%esp
8010875c:	50                   	push   %eax
8010875d:	e8 16 00 00 00       	call   80108778 <uartputc>
80108762:	83 c4 10             	add    $0x10,%esp
  inb(COM1+0);
  picenable(IRQ_COM1);
  ioapicenable(IRQ_COM1, 0);
  
  // Announce that we're here.
  for(p="xv6...\n"; *p; p++)
80108765:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80108769:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010876c:	0f b6 00             	movzbl (%eax),%eax
8010876f:	84 c0                	test   %al,%al
80108771:	75 dd                	jne    80108750 <uartinit+0xcc>
80108773:	eb 01                	jmp    80108776 <uartinit+0xf2>
  outb(COM1+4, 0);
  outb(COM1+1, 0x01);    // Enable receive interrupts.

  // If status is 0xFF, no serial port.
  if(inb(COM1+5) == 0xFF)
    return;
80108775:	90                   	nop
  ioapicenable(IRQ_COM1, 0);
  
  // Announce that we're here.
  for(p="xv6...\n"; *p; p++)
    uartputc(*p);
}
80108776:	c9                   	leave  
80108777:	c3                   	ret    

80108778 <uartputc>:

void
uartputc(int c)
{
80108778:	55                   	push   %ebp
80108779:	89 e5                	mov    %esp,%ebp
8010877b:	83 ec 18             	sub    $0x18,%esp
  int i;

  if(!uart)
8010877e:	a1 6c d6 10 80       	mov    0x8010d66c,%eax
80108783:	85 c0                	test   %eax,%eax
80108785:	74 53                	je     801087da <uartputc+0x62>
    return;
  for(i = 0; i < 128 && !(inb(COM1+5) & 0x20); i++)
80108787:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
8010878e:	eb 11                	jmp    801087a1 <uartputc+0x29>
    microdelay(10);
80108790:	83 ec 0c             	sub    $0xc,%esp
80108793:	6a 0a                	push   $0xa
80108795:	e8 44 a9 ff ff       	call   801030de <microdelay>
8010879a:	83 c4 10             	add    $0x10,%esp
{
  int i;

  if(!uart)
    return;
  for(i = 0; i < 128 && !(inb(COM1+5) & 0x20); i++)
8010879d:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
801087a1:	83 7d f4 7f          	cmpl   $0x7f,-0xc(%ebp)
801087a5:	7f 1a                	jg     801087c1 <uartputc+0x49>
801087a7:	83 ec 0c             	sub    $0xc,%esp
801087aa:	68 fd 03 00 00       	push   $0x3fd
801087af:	e8 94 fe ff ff       	call   80108648 <inb>
801087b4:	83 c4 10             	add    $0x10,%esp
801087b7:	0f b6 c0             	movzbl %al,%eax
801087ba:	83 e0 20             	and    $0x20,%eax
801087bd:	85 c0                	test   %eax,%eax
801087bf:	74 cf                	je     80108790 <uartputc+0x18>
    microdelay(10);
  outb(COM1+0, c);
801087c1:	8b 45 08             	mov    0x8(%ebp),%eax
801087c4:	0f b6 c0             	movzbl %al,%eax
801087c7:	83 ec 08             	sub    $0x8,%esp
801087ca:	50                   	push   %eax
801087cb:	68 f8 03 00 00       	push   $0x3f8
801087d0:	e8 90 fe ff ff       	call   80108665 <outb>
801087d5:	83 c4 10             	add    $0x10,%esp
801087d8:	eb 01                	jmp    801087db <uartputc+0x63>
uartputc(int c)
{
  int i;

  if(!uart)
    return;
801087da:	90                   	nop
  for(i = 0; i < 128 && !(inb(COM1+5) & 0x20); i++)
    microdelay(10);
  outb(COM1+0, c);
}
801087db:	c9                   	leave  
801087dc:	c3                   	ret    

801087dd <uartgetc>:

static int
uartgetc(void)
{
801087dd:	55                   	push   %ebp
801087de:	89 e5                	mov    %esp,%ebp
  if(!uart)
801087e0:	a1 6c d6 10 80       	mov    0x8010d66c,%eax
801087e5:	85 c0                	test   %eax,%eax
801087e7:	75 07                	jne    801087f0 <uartgetc+0x13>
    return -1;
801087e9:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801087ee:	eb 2e                	jmp    8010881e <uartgetc+0x41>
  if(!(inb(COM1+5) & 0x01))
801087f0:	68 fd 03 00 00       	push   $0x3fd
801087f5:	e8 4e fe ff ff       	call   80108648 <inb>
801087fa:	83 c4 04             	add    $0x4,%esp
801087fd:	0f b6 c0             	movzbl %al,%eax
80108800:	83 e0 01             	and    $0x1,%eax
80108803:	85 c0                	test   %eax,%eax
80108805:	75 07                	jne    8010880e <uartgetc+0x31>
    return -1;
80108807:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010880c:	eb 10                	jmp    8010881e <uartgetc+0x41>
  return inb(COM1+0);
8010880e:	68 f8 03 00 00       	push   $0x3f8
80108813:	e8 30 fe ff ff       	call   80108648 <inb>
80108818:	83 c4 04             	add    $0x4,%esp
8010881b:	0f b6 c0             	movzbl %al,%eax
}
8010881e:	c9                   	leave  
8010881f:	c3                   	ret    

80108820 <uartintr>:

void
uartintr(void)
{
80108820:	55                   	push   %ebp
80108821:	89 e5                	mov    %esp,%ebp
80108823:	83 ec 08             	sub    $0x8,%esp
  consoleintr(uartgetc);
80108826:	83 ec 0c             	sub    $0xc,%esp
80108829:	68 dd 87 10 80       	push   $0x801087dd
8010882e:	e8 c6 7f ff ff       	call   801007f9 <consoleintr>
80108833:	83 c4 10             	add    $0x10,%esp
}
80108836:	90                   	nop
80108837:	c9                   	leave  
80108838:	c3                   	ret    

80108839 <vector0>:
# generated by vectors.pl - do not edit
# handlers
.globl alltraps
.globl vector0
vector0:
  pushl $0
80108839:	6a 00                	push   $0x0
  pushl $0
8010883b:	6a 00                	push   $0x0
  jmp alltraps
8010883d:	e9 cc f9 ff ff       	jmp    8010820e <alltraps>

80108842 <vector1>:
.globl vector1
vector1:
  pushl $0
80108842:	6a 00                	push   $0x0
  pushl $1
80108844:	6a 01                	push   $0x1
  jmp alltraps
80108846:	e9 c3 f9 ff ff       	jmp    8010820e <alltraps>

8010884b <vector2>:
.globl vector2
vector2:
  pushl $0
8010884b:	6a 00                	push   $0x0
  pushl $2
8010884d:	6a 02                	push   $0x2
  jmp alltraps
8010884f:	e9 ba f9 ff ff       	jmp    8010820e <alltraps>

80108854 <vector3>:
.globl vector3
vector3:
  pushl $0
80108854:	6a 00                	push   $0x0
  pushl $3
80108856:	6a 03                	push   $0x3
  jmp alltraps
80108858:	e9 b1 f9 ff ff       	jmp    8010820e <alltraps>

8010885d <vector4>:
.globl vector4
vector4:
  pushl $0
8010885d:	6a 00                	push   $0x0
  pushl $4
8010885f:	6a 04                	push   $0x4
  jmp alltraps
80108861:	e9 a8 f9 ff ff       	jmp    8010820e <alltraps>

80108866 <vector5>:
.globl vector5
vector5:
  pushl $0
80108866:	6a 00                	push   $0x0
  pushl $5
80108868:	6a 05                	push   $0x5
  jmp alltraps
8010886a:	e9 9f f9 ff ff       	jmp    8010820e <alltraps>

8010886f <vector6>:
.globl vector6
vector6:
  pushl $0
8010886f:	6a 00                	push   $0x0
  pushl $6
80108871:	6a 06                	push   $0x6
  jmp alltraps
80108873:	e9 96 f9 ff ff       	jmp    8010820e <alltraps>

80108878 <vector7>:
.globl vector7
vector7:
  pushl $0
80108878:	6a 00                	push   $0x0
  pushl $7
8010887a:	6a 07                	push   $0x7
  jmp alltraps
8010887c:	e9 8d f9 ff ff       	jmp    8010820e <alltraps>

80108881 <vector8>:
.globl vector8
vector8:
  pushl $8
80108881:	6a 08                	push   $0x8
  jmp alltraps
80108883:	e9 86 f9 ff ff       	jmp    8010820e <alltraps>

80108888 <vector9>:
.globl vector9
vector9:
  pushl $0
80108888:	6a 00                	push   $0x0
  pushl $9
8010888a:	6a 09                	push   $0x9
  jmp alltraps
8010888c:	e9 7d f9 ff ff       	jmp    8010820e <alltraps>

80108891 <vector10>:
.globl vector10
vector10:
  pushl $10
80108891:	6a 0a                	push   $0xa
  jmp alltraps
80108893:	e9 76 f9 ff ff       	jmp    8010820e <alltraps>

80108898 <vector11>:
.globl vector11
vector11:
  pushl $11
80108898:	6a 0b                	push   $0xb
  jmp alltraps
8010889a:	e9 6f f9 ff ff       	jmp    8010820e <alltraps>

8010889f <vector12>:
.globl vector12
vector12:
  pushl $12
8010889f:	6a 0c                	push   $0xc
  jmp alltraps
801088a1:	e9 68 f9 ff ff       	jmp    8010820e <alltraps>

801088a6 <vector13>:
.globl vector13
vector13:
  pushl $13
801088a6:	6a 0d                	push   $0xd
  jmp alltraps
801088a8:	e9 61 f9 ff ff       	jmp    8010820e <alltraps>

801088ad <vector14>:
.globl vector14
vector14:
  pushl $14
801088ad:	6a 0e                	push   $0xe
  jmp alltraps
801088af:	e9 5a f9 ff ff       	jmp    8010820e <alltraps>

801088b4 <vector15>:
.globl vector15
vector15:
  pushl $0
801088b4:	6a 00                	push   $0x0
  pushl $15
801088b6:	6a 0f                	push   $0xf
  jmp alltraps
801088b8:	e9 51 f9 ff ff       	jmp    8010820e <alltraps>

801088bd <vector16>:
.globl vector16
vector16:
  pushl $0
801088bd:	6a 00                	push   $0x0
  pushl $16
801088bf:	6a 10                	push   $0x10
  jmp alltraps
801088c1:	e9 48 f9 ff ff       	jmp    8010820e <alltraps>

801088c6 <vector17>:
.globl vector17
vector17:
  pushl $17
801088c6:	6a 11                	push   $0x11
  jmp alltraps
801088c8:	e9 41 f9 ff ff       	jmp    8010820e <alltraps>

801088cd <vector18>:
.globl vector18
vector18:
  pushl $0
801088cd:	6a 00                	push   $0x0
  pushl $18
801088cf:	6a 12                	push   $0x12
  jmp alltraps
801088d1:	e9 38 f9 ff ff       	jmp    8010820e <alltraps>

801088d6 <vector19>:
.globl vector19
vector19:
  pushl $0
801088d6:	6a 00                	push   $0x0
  pushl $19
801088d8:	6a 13                	push   $0x13
  jmp alltraps
801088da:	e9 2f f9 ff ff       	jmp    8010820e <alltraps>

801088df <vector20>:
.globl vector20
vector20:
  pushl $0
801088df:	6a 00                	push   $0x0
  pushl $20
801088e1:	6a 14                	push   $0x14
  jmp alltraps
801088e3:	e9 26 f9 ff ff       	jmp    8010820e <alltraps>

801088e8 <vector21>:
.globl vector21
vector21:
  pushl $0
801088e8:	6a 00                	push   $0x0
  pushl $21
801088ea:	6a 15                	push   $0x15
  jmp alltraps
801088ec:	e9 1d f9 ff ff       	jmp    8010820e <alltraps>

801088f1 <vector22>:
.globl vector22
vector22:
  pushl $0
801088f1:	6a 00                	push   $0x0
  pushl $22
801088f3:	6a 16                	push   $0x16
  jmp alltraps
801088f5:	e9 14 f9 ff ff       	jmp    8010820e <alltraps>

801088fa <vector23>:
.globl vector23
vector23:
  pushl $0
801088fa:	6a 00                	push   $0x0
  pushl $23
801088fc:	6a 17                	push   $0x17
  jmp alltraps
801088fe:	e9 0b f9 ff ff       	jmp    8010820e <alltraps>

80108903 <vector24>:
.globl vector24
vector24:
  pushl $0
80108903:	6a 00                	push   $0x0
  pushl $24
80108905:	6a 18                	push   $0x18
  jmp alltraps
80108907:	e9 02 f9 ff ff       	jmp    8010820e <alltraps>

8010890c <vector25>:
.globl vector25
vector25:
  pushl $0
8010890c:	6a 00                	push   $0x0
  pushl $25
8010890e:	6a 19                	push   $0x19
  jmp alltraps
80108910:	e9 f9 f8 ff ff       	jmp    8010820e <alltraps>

80108915 <vector26>:
.globl vector26
vector26:
  pushl $0
80108915:	6a 00                	push   $0x0
  pushl $26
80108917:	6a 1a                	push   $0x1a
  jmp alltraps
80108919:	e9 f0 f8 ff ff       	jmp    8010820e <alltraps>

8010891e <vector27>:
.globl vector27
vector27:
  pushl $0
8010891e:	6a 00                	push   $0x0
  pushl $27
80108920:	6a 1b                	push   $0x1b
  jmp alltraps
80108922:	e9 e7 f8 ff ff       	jmp    8010820e <alltraps>

80108927 <vector28>:
.globl vector28
vector28:
  pushl $0
80108927:	6a 00                	push   $0x0
  pushl $28
80108929:	6a 1c                	push   $0x1c
  jmp alltraps
8010892b:	e9 de f8 ff ff       	jmp    8010820e <alltraps>

80108930 <vector29>:
.globl vector29
vector29:
  pushl $0
80108930:	6a 00                	push   $0x0
  pushl $29
80108932:	6a 1d                	push   $0x1d
  jmp alltraps
80108934:	e9 d5 f8 ff ff       	jmp    8010820e <alltraps>

80108939 <vector30>:
.globl vector30
vector30:
  pushl $0
80108939:	6a 00                	push   $0x0
  pushl $30
8010893b:	6a 1e                	push   $0x1e
  jmp alltraps
8010893d:	e9 cc f8 ff ff       	jmp    8010820e <alltraps>

80108942 <vector31>:
.globl vector31
vector31:
  pushl $0
80108942:	6a 00                	push   $0x0
  pushl $31
80108944:	6a 1f                	push   $0x1f
  jmp alltraps
80108946:	e9 c3 f8 ff ff       	jmp    8010820e <alltraps>

8010894b <vector32>:
.globl vector32
vector32:
  pushl $0
8010894b:	6a 00                	push   $0x0
  pushl $32
8010894d:	6a 20                	push   $0x20
  jmp alltraps
8010894f:	e9 ba f8 ff ff       	jmp    8010820e <alltraps>

80108954 <vector33>:
.globl vector33
vector33:
  pushl $0
80108954:	6a 00                	push   $0x0
  pushl $33
80108956:	6a 21                	push   $0x21
  jmp alltraps
80108958:	e9 b1 f8 ff ff       	jmp    8010820e <alltraps>

8010895d <vector34>:
.globl vector34
vector34:
  pushl $0
8010895d:	6a 00                	push   $0x0
  pushl $34
8010895f:	6a 22                	push   $0x22
  jmp alltraps
80108961:	e9 a8 f8 ff ff       	jmp    8010820e <alltraps>

80108966 <vector35>:
.globl vector35
vector35:
  pushl $0
80108966:	6a 00                	push   $0x0
  pushl $35
80108968:	6a 23                	push   $0x23
  jmp alltraps
8010896a:	e9 9f f8 ff ff       	jmp    8010820e <alltraps>

8010896f <vector36>:
.globl vector36
vector36:
  pushl $0
8010896f:	6a 00                	push   $0x0
  pushl $36
80108971:	6a 24                	push   $0x24
  jmp alltraps
80108973:	e9 96 f8 ff ff       	jmp    8010820e <alltraps>

80108978 <vector37>:
.globl vector37
vector37:
  pushl $0
80108978:	6a 00                	push   $0x0
  pushl $37
8010897a:	6a 25                	push   $0x25
  jmp alltraps
8010897c:	e9 8d f8 ff ff       	jmp    8010820e <alltraps>

80108981 <vector38>:
.globl vector38
vector38:
  pushl $0
80108981:	6a 00                	push   $0x0
  pushl $38
80108983:	6a 26                	push   $0x26
  jmp alltraps
80108985:	e9 84 f8 ff ff       	jmp    8010820e <alltraps>

8010898a <vector39>:
.globl vector39
vector39:
  pushl $0
8010898a:	6a 00                	push   $0x0
  pushl $39
8010898c:	6a 27                	push   $0x27
  jmp alltraps
8010898e:	e9 7b f8 ff ff       	jmp    8010820e <alltraps>

80108993 <vector40>:
.globl vector40
vector40:
  pushl $0
80108993:	6a 00                	push   $0x0
  pushl $40
80108995:	6a 28                	push   $0x28
  jmp alltraps
80108997:	e9 72 f8 ff ff       	jmp    8010820e <alltraps>

8010899c <vector41>:
.globl vector41
vector41:
  pushl $0
8010899c:	6a 00                	push   $0x0
  pushl $41
8010899e:	6a 29                	push   $0x29
  jmp alltraps
801089a0:	e9 69 f8 ff ff       	jmp    8010820e <alltraps>

801089a5 <vector42>:
.globl vector42
vector42:
  pushl $0
801089a5:	6a 00                	push   $0x0
  pushl $42
801089a7:	6a 2a                	push   $0x2a
  jmp alltraps
801089a9:	e9 60 f8 ff ff       	jmp    8010820e <alltraps>

801089ae <vector43>:
.globl vector43
vector43:
  pushl $0
801089ae:	6a 00                	push   $0x0
  pushl $43
801089b0:	6a 2b                	push   $0x2b
  jmp alltraps
801089b2:	e9 57 f8 ff ff       	jmp    8010820e <alltraps>

801089b7 <vector44>:
.globl vector44
vector44:
  pushl $0
801089b7:	6a 00                	push   $0x0
  pushl $44
801089b9:	6a 2c                	push   $0x2c
  jmp alltraps
801089bb:	e9 4e f8 ff ff       	jmp    8010820e <alltraps>

801089c0 <vector45>:
.globl vector45
vector45:
  pushl $0
801089c0:	6a 00                	push   $0x0
  pushl $45
801089c2:	6a 2d                	push   $0x2d
  jmp alltraps
801089c4:	e9 45 f8 ff ff       	jmp    8010820e <alltraps>

801089c9 <vector46>:
.globl vector46
vector46:
  pushl $0
801089c9:	6a 00                	push   $0x0
  pushl $46
801089cb:	6a 2e                	push   $0x2e
  jmp alltraps
801089cd:	e9 3c f8 ff ff       	jmp    8010820e <alltraps>

801089d2 <vector47>:
.globl vector47
vector47:
  pushl $0
801089d2:	6a 00                	push   $0x0
  pushl $47
801089d4:	6a 2f                	push   $0x2f
  jmp alltraps
801089d6:	e9 33 f8 ff ff       	jmp    8010820e <alltraps>

801089db <vector48>:
.globl vector48
vector48:
  pushl $0
801089db:	6a 00                	push   $0x0
  pushl $48
801089dd:	6a 30                	push   $0x30
  jmp alltraps
801089df:	e9 2a f8 ff ff       	jmp    8010820e <alltraps>

801089e4 <vector49>:
.globl vector49
vector49:
  pushl $0
801089e4:	6a 00                	push   $0x0
  pushl $49
801089e6:	6a 31                	push   $0x31
  jmp alltraps
801089e8:	e9 21 f8 ff ff       	jmp    8010820e <alltraps>

801089ed <vector50>:
.globl vector50
vector50:
  pushl $0
801089ed:	6a 00                	push   $0x0
  pushl $50
801089ef:	6a 32                	push   $0x32
  jmp alltraps
801089f1:	e9 18 f8 ff ff       	jmp    8010820e <alltraps>

801089f6 <vector51>:
.globl vector51
vector51:
  pushl $0
801089f6:	6a 00                	push   $0x0
  pushl $51
801089f8:	6a 33                	push   $0x33
  jmp alltraps
801089fa:	e9 0f f8 ff ff       	jmp    8010820e <alltraps>

801089ff <vector52>:
.globl vector52
vector52:
  pushl $0
801089ff:	6a 00                	push   $0x0
  pushl $52
80108a01:	6a 34                	push   $0x34
  jmp alltraps
80108a03:	e9 06 f8 ff ff       	jmp    8010820e <alltraps>

80108a08 <vector53>:
.globl vector53
vector53:
  pushl $0
80108a08:	6a 00                	push   $0x0
  pushl $53
80108a0a:	6a 35                	push   $0x35
  jmp alltraps
80108a0c:	e9 fd f7 ff ff       	jmp    8010820e <alltraps>

80108a11 <vector54>:
.globl vector54
vector54:
  pushl $0
80108a11:	6a 00                	push   $0x0
  pushl $54
80108a13:	6a 36                	push   $0x36
  jmp alltraps
80108a15:	e9 f4 f7 ff ff       	jmp    8010820e <alltraps>

80108a1a <vector55>:
.globl vector55
vector55:
  pushl $0
80108a1a:	6a 00                	push   $0x0
  pushl $55
80108a1c:	6a 37                	push   $0x37
  jmp alltraps
80108a1e:	e9 eb f7 ff ff       	jmp    8010820e <alltraps>

80108a23 <vector56>:
.globl vector56
vector56:
  pushl $0
80108a23:	6a 00                	push   $0x0
  pushl $56
80108a25:	6a 38                	push   $0x38
  jmp alltraps
80108a27:	e9 e2 f7 ff ff       	jmp    8010820e <alltraps>

80108a2c <vector57>:
.globl vector57
vector57:
  pushl $0
80108a2c:	6a 00                	push   $0x0
  pushl $57
80108a2e:	6a 39                	push   $0x39
  jmp alltraps
80108a30:	e9 d9 f7 ff ff       	jmp    8010820e <alltraps>

80108a35 <vector58>:
.globl vector58
vector58:
  pushl $0
80108a35:	6a 00                	push   $0x0
  pushl $58
80108a37:	6a 3a                	push   $0x3a
  jmp alltraps
80108a39:	e9 d0 f7 ff ff       	jmp    8010820e <alltraps>

80108a3e <vector59>:
.globl vector59
vector59:
  pushl $0
80108a3e:	6a 00                	push   $0x0
  pushl $59
80108a40:	6a 3b                	push   $0x3b
  jmp alltraps
80108a42:	e9 c7 f7 ff ff       	jmp    8010820e <alltraps>

80108a47 <vector60>:
.globl vector60
vector60:
  pushl $0
80108a47:	6a 00                	push   $0x0
  pushl $60
80108a49:	6a 3c                	push   $0x3c
  jmp alltraps
80108a4b:	e9 be f7 ff ff       	jmp    8010820e <alltraps>

80108a50 <vector61>:
.globl vector61
vector61:
  pushl $0
80108a50:	6a 00                	push   $0x0
  pushl $61
80108a52:	6a 3d                	push   $0x3d
  jmp alltraps
80108a54:	e9 b5 f7 ff ff       	jmp    8010820e <alltraps>

80108a59 <vector62>:
.globl vector62
vector62:
  pushl $0
80108a59:	6a 00                	push   $0x0
  pushl $62
80108a5b:	6a 3e                	push   $0x3e
  jmp alltraps
80108a5d:	e9 ac f7 ff ff       	jmp    8010820e <alltraps>

80108a62 <vector63>:
.globl vector63
vector63:
  pushl $0
80108a62:	6a 00                	push   $0x0
  pushl $63
80108a64:	6a 3f                	push   $0x3f
  jmp alltraps
80108a66:	e9 a3 f7 ff ff       	jmp    8010820e <alltraps>

80108a6b <vector64>:
.globl vector64
vector64:
  pushl $0
80108a6b:	6a 00                	push   $0x0
  pushl $64
80108a6d:	6a 40                	push   $0x40
  jmp alltraps
80108a6f:	e9 9a f7 ff ff       	jmp    8010820e <alltraps>

80108a74 <vector65>:
.globl vector65
vector65:
  pushl $0
80108a74:	6a 00                	push   $0x0
  pushl $65
80108a76:	6a 41                	push   $0x41
  jmp alltraps
80108a78:	e9 91 f7 ff ff       	jmp    8010820e <alltraps>

80108a7d <vector66>:
.globl vector66
vector66:
  pushl $0
80108a7d:	6a 00                	push   $0x0
  pushl $66
80108a7f:	6a 42                	push   $0x42
  jmp alltraps
80108a81:	e9 88 f7 ff ff       	jmp    8010820e <alltraps>

80108a86 <vector67>:
.globl vector67
vector67:
  pushl $0
80108a86:	6a 00                	push   $0x0
  pushl $67
80108a88:	6a 43                	push   $0x43
  jmp alltraps
80108a8a:	e9 7f f7 ff ff       	jmp    8010820e <alltraps>

80108a8f <vector68>:
.globl vector68
vector68:
  pushl $0
80108a8f:	6a 00                	push   $0x0
  pushl $68
80108a91:	6a 44                	push   $0x44
  jmp alltraps
80108a93:	e9 76 f7 ff ff       	jmp    8010820e <alltraps>

80108a98 <vector69>:
.globl vector69
vector69:
  pushl $0
80108a98:	6a 00                	push   $0x0
  pushl $69
80108a9a:	6a 45                	push   $0x45
  jmp alltraps
80108a9c:	e9 6d f7 ff ff       	jmp    8010820e <alltraps>

80108aa1 <vector70>:
.globl vector70
vector70:
  pushl $0
80108aa1:	6a 00                	push   $0x0
  pushl $70
80108aa3:	6a 46                	push   $0x46
  jmp alltraps
80108aa5:	e9 64 f7 ff ff       	jmp    8010820e <alltraps>

80108aaa <vector71>:
.globl vector71
vector71:
  pushl $0
80108aaa:	6a 00                	push   $0x0
  pushl $71
80108aac:	6a 47                	push   $0x47
  jmp alltraps
80108aae:	e9 5b f7 ff ff       	jmp    8010820e <alltraps>

80108ab3 <vector72>:
.globl vector72
vector72:
  pushl $0
80108ab3:	6a 00                	push   $0x0
  pushl $72
80108ab5:	6a 48                	push   $0x48
  jmp alltraps
80108ab7:	e9 52 f7 ff ff       	jmp    8010820e <alltraps>

80108abc <vector73>:
.globl vector73
vector73:
  pushl $0
80108abc:	6a 00                	push   $0x0
  pushl $73
80108abe:	6a 49                	push   $0x49
  jmp alltraps
80108ac0:	e9 49 f7 ff ff       	jmp    8010820e <alltraps>

80108ac5 <vector74>:
.globl vector74
vector74:
  pushl $0
80108ac5:	6a 00                	push   $0x0
  pushl $74
80108ac7:	6a 4a                	push   $0x4a
  jmp alltraps
80108ac9:	e9 40 f7 ff ff       	jmp    8010820e <alltraps>

80108ace <vector75>:
.globl vector75
vector75:
  pushl $0
80108ace:	6a 00                	push   $0x0
  pushl $75
80108ad0:	6a 4b                	push   $0x4b
  jmp alltraps
80108ad2:	e9 37 f7 ff ff       	jmp    8010820e <alltraps>

80108ad7 <vector76>:
.globl vector76
vector76:
  pushl $0
80108ad7:	6a 00                	push   $0x0
  pushl $76
80108ad9:	6a 4c                	push   $0x4c
  jmp alltraps
80108adb:	e9 2e f7 ff ff       	jmp    8010820e <alltraps>

80108ae0 <vector77>:
.globl vector77
vector77:
  pushl $0
80108ae0:	6a 00                	push   $0x0
  pushl $77
80108ae2:	6a 4d                	push   $0x4d
  jmp alltraps
80108ae4:	e9 25 f7 ff ff       	jmp    8010820e <alltraps>

80108ae9 <vector78>:
.globl vector78
vector78:
  pushl $0
80108ae9:	6a 00                	push   $0x0
  pushl $78
80108aeb:	6a 4e                	push   $0x4e
  jmp alltraps
80108aed:	e9 1c f7 ff ff       	jmp    8010820e <alltraps>

80108af2 <vector79>:
.globl vector79
vector79:
  pushl $0
80108af2:	6a 00                	push   $0x0
  pushl $79
80108af4:	6a 4f                	push   $0x4f
  jmp alltraps
80108af6:	e9 13 f7 ff ff       	jmp    8010820e <alltraps>

80108afb <vector80>:
.globl vector80
vector80:
  pushl $0
80108afb:	6a 00                	push   $0x0
  pushl $80
80108afd:	6a 50                	push   $0x50
  jmp alltraps
80108aff:	e9 0a f7 ff ff       	jmp    8010820e <alltraps>

80108b04 <vector81>:
.globl vector81
vector81:
  pushl $0
80108b04:	6a 00                	push   $0x0
  pushl $81
80108b06:	6a 51                	push   $0x51
  jmp alltraps
80108b08:	e9 01 f7 ff ff       	jmp    8010820e <alltraps>

80108b0d <vector82>:
.globl vector82
vector82:
  pushl $0
80108b0d:	6a 00                	push   $0x0
  pushl $82
80108b0f:	6a 52                	push   $0x52
  jmp alltraps
80108b11:	e9 f8 f6 ff ff       	jmp    8010820e <alltraps>

80108b16 <vector83>:
.globl vector83
vector83:
  pushl $0
80108b16:	6a 00                	push   $0x0
  pushl $83
80108b18:	6a 53                	push   $0x53
  jmp alltraps
80108b1a:	e9 ef f6 ff ff       	jmp    8010820e <alltraps>

80108b1f <vector84>:
.globl vector84
vector84:
  pushl $0
80108b1f:	6a 00                	push   $0x0
  pushl $84
80108b21:	6a 54                	push   $0x54
  jmp alltraps
80108b23:	e9 e6 f6 ff ff       	jmp    8010820e <alltraps>

80108b28 <vector85>:
.globl vector85
vector85:
  pushl $0
80108b28:	6a 00                	push   $0x0
  pushl $85
80108b2a:	6a 55                	push   $0x55
  jmp alltraps
80108b2c:	e9 dd f6 ff ff       	jmp    8010820e <alltraps>

80108b31 <vector86>:
.globl vector86
vector86:
  pushl $0
80108b31:	6a 00                	push   $0x0
  pushl $86
80108b33:	6a 56                	push   $0x56
  jmp alltraps
80108b35:	e9 d4 f6 ff ff       	jmp    8010820e <alltraps>

80108b3a <vector87>:
.globl vector87
vector87:
  pushl $0
80108b3a:	6a 00                	push   $0x0
  pushl $87
80108b3c:	6a 57                	push   $0x57
  jmp alltraps
80108b3e:	e9 cb f6 ff ff       	jmp    8010820e <alltraps>

80108b43 <vector88>:
.globl vector88
vector88:
  pushl $0
80108b43:	6a 00                	push   $0x0
  pushl $88
80108b45:	6a 58                	push   $0x58
  jmp alltraps
80108b47:	e9 c2 f6 ff ff       	jmp    8010820e <alltraps>

80108b4c <vector89>:
.globl vector89
vector89:
  pushl $0
80108b4c:	6a 00                	push   $0x0
  pushl $89
80108b4e:	6a 59                	push   $0x59
  jmp alltraps
80108b50:	e9 b9 f6 ff ff       	jmp    8010820e <alltraps>

80108b55 <vector90>:
.globl vector90
vector90:
  pushl $0
80108b55:	6a 00                	push   $0x0
  pushl $90
80108b57:	6a 5a                	push   $0x5a
  jmp alltraps
80108b59:	e9 b0 f6 ff ff       	jmp    8010820e <alltraps>

80108b5e <vector91>:
.globl vector91
vector91:
  pushl $0
80108b5e:	6a 00                	push   $0x0
  pushl $91
80108b60:	6a 5b                	push   $0x5b
  jmp alltraps
80108b62:	e9 a7 f6 ff ff       	jmp    8010820e <alltraps>

80108b67 <vector92>:
.globl vector92
vector92:
  pushl $0
80108b67:	6a 00                	push   $0x0
  pushl $92
80108b69:	6a 5c                	push   $0x5c
  jmp alltraps
80108b6b:	e9 9e f6 ff ff       	jmp    8010820e <alltraps>

80108b70 <vector93>:
.globl vector93
vector93:
  pushl $0
80108b70:	6a 00                	push   $0x0
  pushl $93
80108b72:	6a 5d                	push   $0x5d
  jmp alltraps
80108b74:	e9 95 f6 ff ff       	jmp    8010820e <alltraps>

80108b79 <vector94>:
.globl vector94
vector94:
  pushl $0
80108b79:	6a 00                	push   $0x0
  pushl $94
80108b7b:	6a 5e                	push   $0x5e
  jmp alltraps
80108b7d:	e9 8c f6 ff ff       	jmp    8010820e <alltraps>

80108b82 <vector95>:
.globl vector95
vector95:
  pushl $0
80108b82:	6a 00                	push   $0x0
  pushl $95
80108b84:	6a 5f                	push   $0x5f
  jmp alltraps
80108b86:	e9 83 f6 ff ff       	jmp    8010820e <alltraps>

80108b8b <vector96>:
.globl vector96
vector96:
  pushl $0
80108b8b:	6a 00                	push   $0x0
  pushl $96
80108b8d:	6a 60                	push   $0x60
  jmp alltraps
80108b8f:	e9 7a f6 ff ff       	jmp    8010820e <alltraps>

80108b94 <vector97>:
.globl vector97
vector97:
  pushl $0
80108b94:	6a 00                	push   $0x0
  pushl $97
80108b96:	6a 61                	push   $0x61
  jmp alltraps
80108b98:	e9 71 f6 ff ff       	jmp    8010820e <alltraps>

80108b9d <vector98>:
.globl vector98
vector98:
  pushl $0
80108b9d:	6a 00                	push   $0x0
  pushl $98
80108b9f:	6a 62                	push   $0x62
  jmp alltraps
80108ba1:	e9 68 f6 ff ff       	jmp    8010820e <alltraps>

80108ba6 <vector99>:
.globl vector99
vector99:
  pushl $0
80108ba6:	6a 00                	push   $0x0
  pushl $99
80108ba8:	6a 63                	push   $0x63
  jmp alltraps
80108baa:	e9 5f f6 ff ff       	jmp    8010820e <alltraps>

80108baf <vector100>:
.globl vector100
vector100:
  pushl $0
80108baf:	6a 00                	push   $0x0
  pushl $100
80108bb1:	6a 64                	push   $0x64
  jmp alltraps
80108bb3:	e9 56 f6 ff ff       	jmp    8010820e <alltraps>

80108bb8 <vector101>:
.globl vector101
vector101:
  pushl $0
80108bb8:	6a 00                	push   $0x0
  pushl $101
80108bba:	6a 65                	push   $0x65
  jmp alltraps
80108bbc:	e9 4d f6 ff ff       	jmp    8010820e <alltraps>

80108bc1 <vector102>:
.globl vector102
vector102:
  pushl $0
80108bc1:	6a 00                	push   $0x0
  pushl $102
80108bc3:	6a 66                	push   $0x66
  jmp alltraps
80108bc5:	e9 44 f6 ff ff       	jmp    8010820e <alltraps>

80108bca <vector103>:
.globl vector103
vector103:
  pushl $0
80108bca:	6a 00                	push   $0x0
  pushl $103
80108bcc:	6a 67                	push   $0x67
  jmp alltraps
80108bce:	e9 3b f6 ff ff       	jmp    8010820e <alltraps>

80108bd3 <vector104>:
.globl vector104
vector104:
  pushl $0
80108bd3:	6a 00                	push   $0x0
  pushl $104
80108bd5:	6a 68                	push   $0x68
  jmp alltraps
80108bd7:	e9 32 f6 ff ff       	jmp    8010820e <alltraps>

80108bdc <vector105>:
.globl vector105
vector105:
  pushl $0
80108bdc:	6a 00                	push   $0x0
  pushl $105
80108bde:	6a 69                	push   $0x69
  jmp alltraps
80108be0:	e9 29 f6 ff ff       	jmp    8010820e <alltraps>

80108be5 <vector106>:
.globl vector106
vector106:
  pushl $0
80108be5:	6a 00                	push   $0x0
  pushl $106
80108be7:	6a 6a                	push   $0x6a
  jmp alltraps
80108be9:	e9 20 f6 ff ff       	jmp    8010820e <alltraps>

80108bee <vector107>:
.globl vector107
vector107:
  pushl $0
80108bee:	6a 00                	push   $0x0
  pushl $107
80108bf0:	6a 6b                	push   $0x6b
  jmp alltraps
80108bf2:	e9 17 f6 ff ff       	jmp    8010820e <alltraps>

80108bf7 <vector108>:
.globl vector108
vector108:
  pushl $0
80108bf7:	6a 00                	push   $0x0
  pushl $108
80108bf9:	6a 6c                	push   $0x6c
  jmp alltraps
80108bfb:	e9 0e f6 ff ff       	jmp    8010820e <alltraps>

80108c00 <vector109>:
.globl vector109
vector109:
  pushl $0
80108c00:	6a 00                	push   $0x0
  pushl $109
80108c02:	6a 6d                	push   $0x6d
  jmp alltraps
80108c04:	e9 05 f6 ff ff       	jmp    8010820e <alltraps>

80108c09 <vector110>:
.globl vector110
vector110:
  pushl $0
80108c09:	6a 00                	push   $0x0
  pushl $110
80108c0b:	6a 6e                	push   $0x6e
  jmp alltraps
80108c0d:	e9 fc f5 ff ff       	jmp    8010820e <alltraps>

80108c12 <vector111>:
.globl vector111
vector111:
  pushl $0
80108c12:	6a 00                	push   $0x0
  pushl $111
80108c14:	6a 6f                	push   $0x6f
  jmp alltraps
80108c16:	e9 f3 f5 ff ff       	jmp    8010820e <alltraps>

80108c1b <vector112>:
.globl vector112
vector112:
  pushl $0
80108c1b:	6a 00                	push   $0x0
  pushl $112
80108c1d:	6a 70                	push   $0x70
  jmp alltraps
80108c1f:	e9 ea f5 ff ff       	jmp    8010820e <alltraps>

80108c24 <vector113>:
.globl vector113
vector113:
  pushl $0
80108c24:	6a 00                	push   $0x0
  pushl $113
80108c26:	6a 71                	push   $0x71
  jmp alltraps
80108c28:	e9 e1 f5 ff ff       	jmp    8010820e <alltraps>

80108c2d <vector114>:
.globl vector114
vector114:
  pushl $0
80108c2d:	6a 00                	push   $0x0
  pushl $114
80108c2f:	6a 72                	push   $0x72
  jmp alltraps
80108c31:	e9 d8 f5 ff ff       	jmp    8010820e <alltraps>

80108c36 <vector115>:
.globl vector115
vector115:
  pushl $0
80108c36:	6a 00                	push   $0x0
  pushl $115
80108c38:	6a 73                	push   $0x73
  jmp alltraps
80108c3a:	e9 cf f5 ff ff       	jmp    8010820e <alltraps>

80108c3f <vector116>:
.globl vector116
vector116:
  pushl $0
80108c3f:	6a 00                	push   $0x0
  pushl $116
80108c41:	6a 74                	push   $0x74
  jmp alltraps
80108c43:	e9 c6 f5 ff ff       	jmp    8010820e <alltraps>

80108c48 <vector117>:
.globl vector117
vector117:
  pushl $0
80108c48:	6a 00                	push   $0x0
  pushl $117
80108c4a:	6a 75                	push   $0x75
  jmp alltraps
80108c4c:	e9 bd f5 ff ff       	jmp    8010820e <alltraps>

80108c51 <vector118>:
.globl vector118
vector118:
  pushl $0
80108c51:	6a 00                	push   $0x0
  pushl $118
80108c53:	6a 76                	push   $0x76
  jmp alltraps
80108c55:	e9 b4 f5 ff ff       	jmp    8010820e <alltraps>

80108c5a <vector119>:
.globl vector119
vector119:
  pushl $0
80108c5a:	6a 00                	push   $0x0
  pushl $119
80108c5c:	6a 77                	push   $0x77
  jmp alltraps
80108c5e:	e9 ab f5 ff ff       	jmp    8010820e <alltraps>

80108c63 <vector120>:
.globl vector120
vector120:
  pushl $0
80108c63:	6a 00                	push   $0x0
  pushl $120
80108c65:	6a 78                	push   $0x78
  jmp alltraps
80108c67:	e9 a2 f5 ff ff       	jmp    8010820e <alltraps>

80108c6c <vector121>:
.globl vector121
vector121:
  pushl $0
80108c6c:	6a 00                	push   $0x0
  pushl $121
80108c6e:	6a 79                	push   $0x79
  jmp alltraps
80108c70:	e9 99 f5 ff ff       	jmp    8010820e <alltraps>

80108c75 <vector122>:
.globl vector122
vector122:
  pushl $0
80108c75:	6a 00                	push   $0x0
  pushl $122
80108c77:	6a 7a                	push   $0x7a
  jmp alltraps
80108c79:	e9 90 f5 ff ff       	jmp    8010820e <alltraps>

80108c7e <vector123>:
.globl vector123
vector123:
  pushl $0
80108c7e:	6a 00                	push   $0x0
  pushl $123
80108c80:	6a 7b                	push   $0x7b
  jmp alltraps
80108c82:	e9 87 f5 ff ff       	jmp    8010820e <alltraps>

80108c87 <vector124>:
.globl vector124
vector124:
  pushl $0
80108c87:	6a 00                	push   $0x0
  pushl $124
80108c89:	6a 7c                	push   $0x7c
  jmp alltraps
80108c8b:	e9 7e f5 ff ff       	jmp    8010820e <alltraps>

80108c90 <vector125>:
.globl vector125
vector125:
  pushl $0
80108c90:	6a 00                	push   $0x0
  pushl $125
80108c92:	6a 7d                	push   $0x7d
  jmp alltraps
80108c94:	e9 75 f5 ff ff       	jmp    8010820e <alltraps>

80108c99 <vector126>:
.globl vector126
vector126:
  pushl $0
80108c99:	6a 00                	push   $0x0
  pushl $126
80108c9b:	6a 7e                	push   $0x7e
  jmp alltraps
80108c9d:	e9 6c f5 ff ff       	jmp    8010820e <alltraps>

80108ca2 <vector127>:
.globl vector127
vector127:
  pushl $0
80108ca2:	6a 00                	push   $0x0
  pushl $127
80108ca4:	6a 7f                	push   $0x7f
  jmp alltraps
80108ca6:	e9 63 f5 ff ff       	jmp    8010820e <alltraps>

80108cab <vector128>:
.globl vector128
vector128:
  pushl $0
80108cab:	6a 00                	push   $0x0
  pushl $128
80108cad:	68 80 00 00 00       	push   $0x80
  jmp alltraps
80108cb2:	e9 57 f5 ff ff       	jmp    8010820e <alltraps>

80108cb7 <vector129>:
.globl vector129
vector129:
  pushl $0
80108cb7:	6a 00                	push   $0x0
  pushl $129
80108cb9:	68 81 00 00 00       	push   $0x81
  jmp alltraps
80108cbe:	e9 4b f5 ff ff       	jmp    8010820e <alltraps>

80108cc3 <vector130>:
.globl vector130
vector130:
  pushl $0
80108cc3:	6a 00                	push   $0x0
  pushl $130
80108cc5:	68 82 00 00 00       	push   $0x82
  jmp alltraps
80108cca:	e9 3f f5 ff ff       	jmp    8010820e <alltraps>

80108ccf <vector131>:
.globl vector131
vector131:
  pushl $0
80108ccf:	6a 00                	push   $0x0
  pushl $131
80108cd1:	68 83 00 00 00       	push   $0x83
  jmp alltraps
80108cd6:	e9 33 f5 ff ff       	jmp    8010820e <alltraps>

80108cdb <vector132>:
.globl vector132
vector132:
  pushl $0
80108cdb:	6a 00                	push   $0x0
  pushl $132
80108cdd:	68 84 00 00 00       	push   $0x84
  jmp alltraps
80108ce2:	e9 27 f5 ff ff       	jmp    8010820e <alltraps>

80108ce7 <vector133>:
.globl vector133
vector133:
  pushl $0
80108ce7:	6a 00                	push   $0x0
  pushl $133
80108ce9:	68 85 00 00 00       	push   $0x85
  jmp alltraps
80108cee:	e9 1b f5 ff ff       	jmp    8010820e <alltraps>

80108cf3 <vector134>:
.globl vector134
vector134:
  pushl $0
80108cf3:	6a 00                	push   $0x0
  pushl $134
80108cf5:	68 86 00 00 00       	push   $0x86
  jmp alltraps
80108cfa:	e9 0f f5 ff ff       	jmp    8010820e <alltraps>

80108cff <vector135>:
.globl vector135
vector135:
  pushl $0
80108cff:	6a 00                	push   $0x0
  pushl $135
80108d01:	68 87 00 00 00       	push   $0x87
  jmp alltraps
80108d06:	e9 03 f5 ff ff       	jmp    8010820e <alltraps>

80108d0b <vector136>:
.globl vector136
vector136:
  pushl $0
80108d0b:	6a 00                	push   $0x0
  pushl $136
80108d0d:	68 88 00 00 00       	push   $0x88
  jmp alltraps
80108d12:	e9 f7 f4 ff ff       	jmp    8010820e <alltraps>

80108d17 <vector137>:
.globl vector137
vector137:
  pushl $0
80108d17:	6a 00                	push   $0x0
  pushl $137
80108d19:	68 89 00 00 00       	push   $0x89
  jmp alltraps
80108d1e:	e9 eb f4 ff ff       	jmp    8010820e <alltraps>

80108d23 <vector138>:
.globl vector138
vector138:
  pushl $0
80108d23:	6a 00                	push   $0x0
  pushl $138
80108d25:	68 8a 00 00 00       	push   $0x8a
  jmp alltraps
80108d2a:	e9 df f4 ff ff       	jmp    8010820e <alltraps>

80108d2f <vector139>:
.globl vector139
vector139:
  pushl $0
80108d2f:	6a 00                	push   $0x0
  pushl $139
80108d31:	68 8b 00 00 00       	push   $0x8b
  jmp alltraps
80108d36:	e9 d3 f4 ff ff       	jmp    8010820e <alltraps>

80108d3b <vector140>:
.globl vector140
vector140:
  pushl $0
80108d3b:	6a 00                	push   $0x0
  pushl $140
80108d3d:	68 8c 00 00 00       	push   $0x8c
  jmp alltraps
80108d42:	e9 c7 f4 ff ff       	jmp    8010820e <alltraps>

80108d47 <vector141>:
.globl vector141
vector141:
  pushl $0
80108d47:	6a 00                	push   $0x0
  pushl $141
80108d49:	68 8d 00 00 00       	push   $0x8d
  jmp alltraps
80108d4e:	e9 bb f4 ff ff       	jmp    8010820e <alltraps>

80108d53 <vector142>:
.globl vector142
vector142:
  pushl $0
80108d53:	6a 00                	push   $0x0
  pushl $142
80108d55:	68 8e 00 00 00       	push   $0x8e
  jmp alltraps
80108d5a:	e9 af f4 ff ff       	jmp    8010820e <alltraps>

80108d5f <vector143>:
.globl vector143
vector143:
  pushl $0
80108d5f:	6a 00                	push   $0x0
  pushl $143
80108d61:	68 8f 00 00 00       	push   $0x8f
  jmp alltraps
80108d66:	e9 a3 f4 ff ff       	jmp    8010820e <alltraps>

80108d6b <vector144>:
.globl vector144
vector144:
  pushl $0
80108d6b:	6a 00                	push   $0x0
  pushl $144
80108d6d:	68 90 00 00 00       	push   $0x90
  jmp alltraps
80108d72:	e9 97 f4 ff ff       	jmp    8010820e <alltraps>

80108d77 <vector145>:
.globl vector145
vector145:
  pushl $0
80108d77:	6a 00                	push   $0x0
  pushl $145
80108d79:	68 91 00 00 00       	push   $0x91
  jmp alltraps
80108d7e:	e9 8b f4 ff ff       	jmp    8010820e <alltraps>

80108d83 <vector146>:
.globl vector146
vector146:
  pushl $0
80108d83:	6a 00                	push   $0x0
  pushl $146
80108d85:	68 92 00 00 00       	push   $0x92
  jmp alltraps
80108d8a:	e9 7f f4 ff ff       	jmp    8010820e <alltraps>

80108d8f <vector147>:
.globl vector147
vector147:
  pushl $0
80108d8f:	6a 00                	push   $0x0
  pushl $147
80108d91:	68 93 00 00 00       	push   $0x93
  jmp alltraps
80108d96:	e9 73 f4 ff ff       	jmp    8010820e <alltraps>

80108d9b <vector148>:
.globl vector148
vector148:
  pushl $0
80108d9b:	6a 00                	push   $0x0
  pushl $148
80108d9d:	68 94 00 00 00       	push   $0x94
  jmp alltraps
80108da2:	e9 67 f4 ff ff       	jmp    8010820e <alltraps>

80108da7 <vector149>:
.globl vector149
vector149:
  pushl $0
80108da7:	6a 00                	push   $0x0
  pushl $149
80108da9:	68 95 00 00 00       	push   $0x95
  jmp alltraps
80108dae:	e9 5b f4 ff ff       	jmp    8010820e <alltraps>

80108db3 <vector150>:
.globl vector150
vector150:
  pushl $0
80108db3:	6a 00                	push   $0x0
  pushl $150
80108db5:	68 96 00 00 00       	push   $0x96
  jmp alltraps
80108dba:	e9 4f f4 ff ff       	jmp    8010820e <alltraps>

80108dbf <vector151>:
.globl vector151
vector151:
  pushl $0
80108dbf:	6a 00                	push   $0x0
  pushl $151
80108dc1:	68 97 00 00 00       	push   $0x97
  jmp alltraps
80108dc6:	e9 43 f4 ff ff       	jmp    8010820e <alltraps>

80108dcb <vector152>:
.globl vector152
vector152:
  pushl $0
80108dcb:	6a 00                	push   $0x0
  pushl $152
80108dcd:	68 98 00 00 00       	push   $0x98
  jmp alltraps
80108dd2:	e9 37 f4 ff ff       	jmp    8010820e <alltraps>

80108dd7 <vector153>:
.globl vector153
vector153:
  pushl $0
80108dd7:	6a 00                	push   $0x0
  pushl $153
80108dd9:	68 99 00 00 00       	push   $0x99
  jmp alltraps
80108dde:	e9 2b f4 ff ff       	jmp    8010820e <alltraps>

80108de3 <vector154>:
.globl vector154
vector154:
  pushl $0
80108de3:	6a 00                	push   $0x0
  pushl $154
80108de5:	68 9a 00 00 00       	push   $0x9a
  jmp alltraps
80108dea:	e9 1f f4 ff ff       	jmp    8010820e <alltraps>

80108def <vector155>:
.globl vector155
vector155:
  pushl $0
80108def:	6a 00                	push   $0x0
  pushl $155
80108df1:	68 9b 00 00 00       	push   $0x9b
  jmp alltraps
80108df6:	e9 13 f4 ff ff       	jmp    8010820e <alltraps>

80108dfb <vector156>:
.globl vector156
vector156:
  pushl $0
80108dfb:	6a 00                	push   $0x0
  pushl $156
80108dfd:	68 9c 00 00 00       	push   $0x9c
  jmp alltraps
80108e02:	e9 07 f4 ff ff       	jmp    8010820e <alltraps>

80108e07 <vector157>:
.globl vector157
vector157:
  pushl $0
80108e07:	6a 00                	push   $0x0
  pushl $157
80108e09:	68 9d 00 00 00       	push   $0x9d
  jmp alltraps
80108e0e:	e9 fb f3 ff ff       	jmp    8010820e <alltraps>

80108e13 <vector158>:
.globl vector158
vector158:
  pushl $0
80108e13:	6a 00                	push   $0x0
  pushl $158
80108e15:	68 9e 00 00 00       	push   $0x9e
  jmp alltraps
80108e1a:	e9 ef f3 ff ff       	jmp    8010820e <alltraps>

80108e1f <vector159>:
.globl vector159
vector159:
  pushl $0
80108e1f:	6a 00                	push   $0x0
  pushl $159
80108e21:	68 9f 00 00 00       	push   $0x9f
  jmp alltraps
80108e26:	e9 e3 f3 ff ff       	jmp    8010820e <alltraps>

80108e2b <vector160>:
.globl vector160
vector160:
  pushl $0
80108e2b:	6a 00                	push   $0x0
  pushl $160
80108e2d:	68 a0 00 00 00       	push   $0xa0
  jmp alltraps
80108e32:	e9 d7 f3 ff ff       	jmp    8010820e <alltraps>

80108e37 <vector161>:
.globl vector161
vector161:
  pushl $0
80108e37:	6a 00                	push   $0x0
  pushl $161
80108e39:	68 a1 00 00 00       	push   $0xa1
  jmp alltraps
80108e3e:	e9 cb f3 ff ff       	jmp    8010820e <alltraps>

80108e43 <vector162>:
.globl vector162
vector162:
  pushl $0
80108e43:	6a 00                	push   $0x0
  pushl $162
80108e45:	68 a2 00 00 00       	push   $0xa2
  jmp alltraps
80108e4a:	e9 bf f3 ff ff       	jmp    8010820e <alltraps>

80108e4f <vector163>:
.globl vector163
vector163:
  pushl $0
80108e4f:	6a 00                	push   $0x0
  pushl $163
80108e51:	68 a3 00 00 00       	push   $0xa3
  jmp alltraps
80108e56:	e9 b3 f3 ff ff       	jmp    8010820e <alltraps>

80108e5b <vector164>:
.globl vector164
vector164:
  pushl $0
80108e5b:	6a 00                	push   $0x0
  pushl $164
80108e5d:	68 a4 00 00 00       	push   $0xa4
  jmp alltraps
80108e62:	e9 a7 f3 ff ff       	jmp    8010820e <alltraps>

80108e67 <vector165>:
.globl vector165
vector165:
  pushl $0
80108e67:	6a 00                	push   $0x0
  pushl $165
80108e69:	68 a5 00 00 00       	push   $0xa5
  jmp alltraps
80108e6e:	e9 9b f3 ff ff       	jmp    8010820e <alltraps>

80108e73 <vector166>:
.globl vector166
vector166:
  pushl $0
80108e73:	6a 00                	push   $0x0
  pushl $166
80108e75:	68 a6 00 00 00       	push   $0xa6
  jmp alltraps
80108e7a:	e9 8f f3 ff ff       	jmp    8010820e <alltraps>

80108e7f <vector167>:
.globl vector167
vector167:
  pushl $0
80108e7f:	6a 00                	push   $0x0
  pushl $167
80108e81:	68 a7 00 00 00       	push   $0xa7
  jmp alltraps
80108e86:	e9 83 f3 ff ff       	jmp    8010820e <alltraps>

80108e8b <vector168>:
.globl vector168
vector168:
  pushl $0
80108e8b:	6a 00                	push   $0x0
  pushl $168
80108e8d:	68 a8 00 00 00       	push   $0xa8
  jmp alltraps
80108e92:	e9 77 f3 ff ff       	jmp    8010820e <alltraps>

80108e97 <vector169>:
.globl vector169
vector169:
  pushl $0
80108e97:	6a 00                	push   $0x0
  pushl $169
80108e99:	68 a9 00 00 00       	push   $0xa9
  jmp alltraps
80108e9e:	e9 6b f3 ff ff       	jmp    8010820e <alltraps>

80108ea3 <vector170>:
.globl vector170
vector170:
  pushl $0
80108ea3:	6a 00                	push   $0x0
  pushl $170
80108ea5:	68 aa 00 00 00       	push   $0xaa
  jmp alltraps
80108eaa:	e9 5f f3 ff ff       	jmp    8010820e <alltraps>

80108eaf <vector171>:
.globl vector171
vector171:
  pushl $0
80108eaf:	6a 00                	push   $0x0
  pushl $171
80108eb1:	68 ab 00 00 00       	push   $0xab
  jmp alltraps
80108eb6:	e9 53 f3 ff ff       	jmp    8010820e <alltraps>

80108ebb <vector172>:
.globl vector172
vector172:
  pushl $0
80108ebb:	6a 00                	push   $0x0
  pushl $172
80108ebd:	68 ac 00 00 00       	push   $0xac
  jmp alltraps
80108ec2:	e9 47 f3 ff ff       	jmp    8010820e <alltraps>

80108ec7 <vector173>:
.globl vector173
vector173:
  pushl $0
80108ec7:	6a 00                	push   $0x0
  pushl $173
80108ec9:	68 ad 00 00 00       	push   $0xad
  jmp alltraps
80108ece:	e9 3b f3 ff ff       	jmp    8010820e <alltraps>

80108ed3 <vector174>:
.globl vector174
vector174:
  pushl $0
80108ed3:	6a 00                	push   $0x0
  pushl $174
80108ed5:	68 ae 00 00 00       	push   $0xae
  jmp alltraps
80108eda:	e9 2f f3 ff ff       	jmp    8010820e <alltraps>

80108edf <vector175>:
.globl vector175
vector175:
  pushl $0
80108edf:	6a 00                	push   $0x0
  pushl $175
80108ee1:	68 af 00 00 00       	push   $0xaf
  jmp alltraps
80108ee6:	e9 23 f3 ff ff       	jmp    8010820e <alltraps>

80108eeb <vector176>:
.globl vector176
vector176:
  pushl $0
80108eeb:	6a 00                	push   $0x0
  pushl $176
80108eed:	68 b0 00 00 00       	push   $0xb0
  jmp alltraps
80108ef2:	e9 17 f3 ff ff       	jmp    8010820e <alltraps>

80108ef7 <vector177>:
.globl vector177
vector177:
  pushl $0
80108ef7:	6a 00                	push   $0x0
  pushl $177
80108ef9:	68 b1 00 00 00       	push   $0xb1
  jmp alltraps
80108efe:	e9 0b f3 ff ff       	jmp    8010820e <alltraps>

80108f03 <vector178>:
.globl vector178
vector178:
  pushl $0
80108f03:	6a 00                	push   $0x0
  pushl $178
80108f05:	68 b2 00 00 00       	push   $0xb2
  jmp alltraps
80108f0a:	e9 ff f2 ff ff       	jmp    8010820e <alltraps>

80108f0f <vector179>:
.globl vector179
vector179:
  pushl $0
80108f0f:	6a 00                	push   $0x0
  pushl $179
80108f11:	68 b3 00 00 00       	push   $0xb3
  jmp alltraps
80108f16:	e9 f3 f2 ff ff       	jmp    8010820e <alltraps>

80108f1b <vector180>:
.globl vector180
vector180:
  pushl $0
80108f1b:	6a 00                	push   $0x0
  pushl $180
80108f1d:	68 b4 00 00 00       	push   $0xb4
  jmp alltraps
80108f22:	e9 e7 f2 ff ff       	jmp    8010820e <alltraps>

80108f27 <vector181>:
.globl vector181
vector181:
  pushl $0
80108f27:	6a 00                	push   $0x0
  pushl $181
80108f29:	68 b5 00 00 00       	push   $0xb5
  jmp alltraps
80108f2e:	e9 db f2 ff ff       	jmp    8010820e <alltraps>

80108f33 <vector182>:
.globl vector182
vector182:
  pushl $0
80108f33:	6a 00                	push   $0x0
  pushl $182
80108f35:	68 b6 00 00 00       	push   $0xb6
  jmp alltraps
80108f3a:	e9 cf f2 ff ff       	jmp    8010820e <alltraps>

80108f3f <vector183>:
.globl vector183
vector183:
  pushl $0
80108f3f:	6a 00                	push   $0x0
  pushl $183
80108f41:	68 b7 00 00 00       	push   $0xb7
  jmp alltraps
80108f46:	e9 c3 f2 ff ff       	jmp    8010820e <alltraps>

80108f4b <vector184>:
.globl vector184
vector184:
  pushl $0
80108f4b:	6a 00                	push   $0x0
  pushl $184
80108f4d:	68 b8 00 00 00       	push   $0xb8
  jmp alltraps
80108f52:	e9 b7 f2 ff ff       	jmp    8010820e <alltraps>

80108f57 <vector185>:
.globl vector185
vector185:
  pushl $0
80108f57:	6a 00                	push   $0x0
  pushl $185
80108f59:	68 b9 00 00 00       	push   $0xb9
  jmp alltraps
80108f5e:	e9 ab f2 ff ff       	jmp    8010820e <alltraps>

80108f63 <vector186>:
.globl vector186
vector186:
  pushl $0
80108f63:	6a 00                	push   $0x0
  pushl $186
80108f65:	68 ba 00 00 00       	push   $0xba
  jmp alltraps
80108f6a:	e9 9f f2 ff ff       	jmp    8010820e <alltraps>

80108f6f <vector187>:
.globl vector187
vector187:
  pushl $0
80108f6f:	6a 00                	push   $0x0
  pushl $187
80108f71:	68 bb 00 00 00       	push   $0xbb
  jmp alltraps
80108f76:	e9 93 f2 ff ff       	jmp    8010820e <alltraps>

80108f7b <vector188>:
.globl vector188
vector188:
  pushl $0
80108f7b:	6a 00                	push   $0x0
  pushl $188
80108f7d:	68 bc 00 00 00       	push   $0xbc
  jmp alltraps
80108f82:	e9 87 f2 ff ff       	jmp    8010820e <alltraps>

80108f87 <vector189>:
.globl vector189
vector189:
  pushl $0
80108f87:	6a 00                	push   $0x0
  pushl $189
80108f89:	68 bd 00 00 00       	push   $0xbd
  jmp alltraps
80108f8e:	e9 7b f2 ff ff       	jmp    8010820e <alltraps>

80108f93 <vector190>:
.globl vector190
vector190:
  pushl $0
80108f93:	6a 00                	push   $0x0
  pushl $190
80108f95:	68 be 00 00 00       	push   $0xbe
  jmp alltraps
80108f9a:	e9 6f f2 ff ff       	jmp    8010820e <alltraps>

80108f9f <vector191>:
.globl vector191
vector191:
  pushl $0
80108f9f:	6a 00                	push   $0x0
  pushl $191
80108fa1:	68 bf 00 00 00       	push   $0xbf
  jmp alltraps
80108fa6:	e9 63 f2 ff ff       	jmp    8010820e <alltraps>

80108fab <vector192>:
.globl vector192
vector192:
  pushl $0
80108fab:	6a 00                	push   $0x0
  pushl $192
80108fad:	68 c0 00 00 00       	push   $0xc0
  jmp alltraps
80108fb2:	e9 57 f2 ff ff       	jmp    8010820e <alltraps>

80108fb7 <vector193>:
.globl vector193
vector193:
  pushl $0
80108fb7:	6a 00                	push   $0x0
  pushl $193
80108fb9:	68 c1 00 00 00       	push   $0xc1
  jmp alltraps
80108fbe:	e9 4b f2 ff ff       	jmp    8010820e <alltraps>

80108fc3 <vector194>:
.globl vector194
vector194:
  pushl $0
80108fc3:	6a 00                	push   $0x0
  pushl $194
80108fc5:	68 c2 00 00 00       	push   $0xc2
  jmp alltraps
80108fca:	e9 3f f2 ff ff       	jmp    8010820e <alltraps>

80108fcf <vector195>:
.globl vector195
vector195:
  pushl $0
80108fcf:	6a 00                	push   $0x0
  pushl $195
80108fd1:	68 c3 00 00 00       	push   $0xc3
  jmp alltraps
80108fd6:	e9 33 f2 ff ff       	jmp    8010820e <alltraps>

80108fdb <vector196>:
.globl vector196
vector196:
  pushl $0
80108fdb:	6a 00                	push   $0x0
  pushl $196
80108fdd:	68 c4 00 00 00       	push   $0xc4
  jmp alltraps
80108fe2:	e9 27 f2 ff ff       	jmp    8010820e <alltraps>

80108fe7 <vector197>:
.globl vector197
vector197:
  pushl $0
80108fe7:	6a 00                	push   $0x0
  pushl $197
80108fe9:	68 c5 00 00 00       	push   $0xc5
  jmp alltraps
80108fee:	e9 1b f2 ff ff       	jmp    8010820e <alltraps>

80108ff3 <vector198>:
.globl vector198
vector198:
  pushl $0
80108ff3:	6a 00                	push   $0x0
  pushl $198
80108ff5:	68 c6 00 00 00       	push   $0xc6
  jmp alltraps
80108ffa:	e9 0f f2 ff ff       	jmp    8010820e <alltraps>

80108fff <vector199>:
.globl vector199
vector199:
  pushl $0
80108fff:	6a 00                	push   $0x0
  pushl $199
80109001:	68 c7 00 00 00       	push   $0xc7
  jmp alltraps
80109006:	e9 03 f2 ff ff       	jmp    8010820e <alltraps>

8010900b <vector200>:
.globl vector200
vector200:
  pushl $0
8010900b:	6a 00                	push   $0x0
  pushl $200
8010900d:	68 c8 00 00 00       	push   $0xc8
  jmp alltraps
80109012:	e9 f7 f1 ff ff       	jmp    8010820e <alltraps>

80109017 <vector201>:
.globl vector201
vector201:
  pushl $0
80109017:	6a 00                	push   $0x0
  pushl $201
80109019:	68 c9 00 00 00       	push   $0xc9
  jmp alltraps
8010901e:	e9 eb f1 ff ff       	jmp    8010820e <alltraps>

80109023 <vector202>:
.globl vector202
vector202:
  pushl $0
80109023:	6a 00                	push   $0x0
  pushl $202
80109025:	68 ca 00 00 00       	push   $0xca
  jmp alltraps
8010902a:	e9 df f1 ff ff       	jmp    8010820e <alltraps>

8010902f <vector203>:
.globl vector203
vector203:
  pushl $0
8010902f:	6a 00                	push   $0x0
  pushl $203
80109031:	68 cb 00 00 00       	push   $0xcb
  jmp alltraps
80109036:	e9 d3 f1 ff ff       	jmp    8010820e <alltraps>

8010903b <vector204>:
.globl vector204
vector204:
  pushl $0
8010903b:	6a 00                	push   $0x0
  pushl $204
8010903d:	68 cc 00 00 00       	push   $0xcc
  jmp alltraps
80109042:	e9 c7 f1 ff ff       	jmp    8010820e <alltraps>

80109047 <vector205>:
.globl vector205
vector205:
  pushl $0
80109047:	6a 00                	push   $0x0
  pushl $205
80109049:	68 cd 00 00 00       	push   $0xcd
  jmp alltraps
8010904e:	e9 bb f1 ff ff       	jmp    8010820e <alltraps>

80109053 <vector206>:
.globl vector206
vector206:
  pushl $0
80109053:	6a 00                	push   $0x0
  pushl $206
80109055:	68 ce 00 00 00       	push   $0xce
  jmp alltraps
8010905a:	e9 af f1 ff ff       	jmp    8010820e <alltraps>

8010905f <vector207>:
.globl vector207
vector207:
  pushl $0
8010905f:	6a 00                	push   $0x0
  pushl $207
80109061:	68 cf 00 00 00       	push   $0xcf
  jmp alltraps
80109066:	e9 a3 f1 ff ff       	jmp    8010820e <alltraps>

8010906b <vector208>:
.globl vector208
vector208:
  pushl $0
8010906b:	6a 00                	push   $0x0
  pushl $208
8010906d:	68 d0 00 00 00       	push   $0xd0
  jmp alltraps
80109072:	e9 97 f1 ff ff       	jmp    8010820e <alltraps>

80109077 <vector209>:
.globl vector209
vector209:
  pushl $0
80109077:	6a 00                	push   $0x0
  pushl $209
80109079:	68 d1 00 00 00       	push   $0xd1
  jmp alltraps
8010907e:	e9 8b f1 ff ff       	jmp    8010820e <alltraps>

80109083 <vector210>:
.globl vector210
vector210:
  pushl $0
80109083:	6a 00                	push   $0x0
  pushl $210
80109085:	68 d2 00 00 00       	push   $0xd2
  jmp alltraps
8010908a:	e9 7f f1 ff ff       	jmp    8010820e <alltraps>

8010908f <vector211>:
.globl vector211
vector211:
  pushl $0
8010908f:	6a 00                	push   $0x0
  pushl $211
80109091:	68 d3 00 00 00       	push   $0xd3
  jmp alltraps
80109096:	e9 73 f1 ff ff       	jmp    8010820e <alltraps>

8010909b <vector212>:
.globl vector212
vector212:
  pushl $0
8010909b:	6a 00                	push   $0x0
  pushl $212
8010909d:	68 d4 00 00 00       	push   $0xd4
  jmp alltraps
801090a2:	e9 67 f1 ff ff       	jmp    8010820e <alltraps>

801090a7 <vector213>:
.globl vector213
vector213:
  pushl $0
801090a7:	6a 00                	push   $0x0
  pushl $213
801090a9:	68 d5 00 00 00       	push   $0xd5
  jmp alltraps
801090ae:	e9 5b f1 ff ff       	jmp    8010820e <alltraps>

801090b3 <vector214>:
.globl vector214
vector214:
  pushl $0
801090b3:	6a 00                	push   $0x0
  pushl $214
801090b5:	68 d6 00 00 00       	push   $0xd6
  jmp alltraps
801090ba:	e9 4f f1 ff ff       	jmp    8010820e <alltraps>

801090bf <vector215>:
.globl vector215
vector215:
  pushl $0
801090bf:	6a 00                	push   $0x0
  pushl $215
801090c1:	68 d7 00 00 00       	push   $0xd7
  jmp alltraps
801090c6:	e9 43 f1 ff ff       	jmp    8010820e <alltraps>

801090cb <vector216>:
.globl vector216
vector216:
  pushl $0
801090cb:	6a 00                	push   $0x0
  pushl $216
801090cd:	68 d8 00 00 00       	push   $0xd8
  jmp alltraps
801090d2:	e9 37 f1 ff ff       	jmp    8010820e <alltraps>

801090d7 <vector217>:
.globl vector217
vector217:
  pushl $0
801090d7:	6a 00                	push   $0x0
  pushl $217
801090d9:	68 d9 00 00 00       	push   $0xd9
  jmp alltraps
801090de:	e9 2b f1 ff ff       	jmp    8010820e <alltraps>

801090e3 <vector218>:
.globl vector218
vector218:
  pushl $0
801090e3:	6a 00                	push   $0x0
  pushl $218
801090e5:	68 da 00 00 00       	push   $0xda
  jmp alltraps
801090ea:	e9 1f f1 ff ff       	jmp    8010820e <alltraps>

801090ef <vector219>:
.globl vector219
vector219:
  pushl $0
801090ef:	6a 00                	push   $0x0
  pushl $219
801090f1:	68 db 00 00 00       	push   $0xdb
  jmp alltraps
801090f6:	e9 13 f1 ff ff       	jmp    8010820e <alltraps>

801090fb <vector220>:
.globl vector220
vector220:
  pushl $0
801090fb:	6a 00                	push   $0x0
  pushl $220
801090fd:	68 dc 00 00 00       	push   $0xdc
  jmp alltraps
80109102:	e9 07 f1 ff ff       	jmp    8010820e <alltraps>

80109107 <vector221>:
.globl vector221
vector221:
  pushl $0
80109107:	6a 00                	push   $0x0
  pushl $221
80109109:	68 dd 00 00 00       	push   $0xdd
  jmp alltraps
8010910e:	e9 fb f0 ff ff       	jmp    8010820e <alltraps>

80109113 <vector222>:
.globl vector222
vector222:
  pushl $0
80109113:	6a 00                	push   $0x0
  pushl $222
80109115:	68 de 00 00 00       	push   $0xde
  jmp alltraps
8010911a:	e9 ef f0 ff ff       	jmp    8010820e <alltraps>

8010911f <vector223>:
.globl vector223
vector223:
  pushl $0
8010911f:	6a 00                	push   $0x0
  pushl $223
80109121:	68 df 00 00 00       	push   $0xdf
  jmp alltraps
80109126:	e9 e3 f0 ff ff       	jmp    8010820e <alltraps>

8010912b <vector224>:
.globl vector224
vector224:
  pushl $0
8010912b:	6a 00                	push   $0x0
  pushl $224
8010912d:	68 e0 00 00 00       	push   $0xe0
  jmp alltraps
80109132:	e9 d7 f0 ff ff       	jmp    8010820e <alltraps>

80109137 <vector225>:
.globl vector225
vector225:
  pushl $0
80109137:	6a 00                	push   $0x0
  pushl $225
80109139:	68 e1 00 00 00       	push   $0xe1
  jmp alltraps
8010913e:	e9 cb f0 ff ff       	jmp    8010820e <alltraps>

80109143 <vector226>:
.globl vector226
vector226:
  pushl $0
80109143:	6a 00                	push   $0x0
  pushl $226
80109145:	68 e2 00 00 00       	push   $0xe2
  jmp alltraps
8010914a:	e9 bf f0 ff ff       	jmp    8010820e <alltraps>

8010914f <vector227>:
.globl vector227
vector227:
  pushl $0
8010914f:	6a 00                	push   $0x0
  pushl $227
80109151:	68 e3 00 00 00       	push   $0xe3
  jmp alltraps
80109156:	e9 b3 f0 ff ff       	jmp    8010820e <alltraps>

8010915b <vector228>:
.globl vector228
vector228:
  pushl $0
8010915b:	6a 00                	push   $0x0
  pushl $228
8010915d:	68 e4 00 00 00       	push   $0xe4
  jmp alltraps
80109162:	e9 a7 f0 ff ff       	jmp    8010820e <alltraps>

80109167 <vector229>:
.globl vector229
vector229:
  pushl $0
80109167:	6a 00                	push   $0x0
  pushl $229
80109169:	68 e5 00 00 00       	push   $0xe5
  jmp alltraps
8010916e:	e9 9b f0 ff ff       	jmp    8010820e <alltraps>

80109173 <vector230>:
.globl vector230
vector230:
  pushl $0
80109173:	6a 00                	push   $0x0
  pushl $230
80109175:	68 e6 00 00 00       	push   $0xe6
  jmp alltraps
8010917a:	e9 8f f0 ff ff       	jmp    8010820e <alltraps>

8010917f <vector231>:
.globl vector231
vector231:
  pushl $0
8010917f:	6a 00                	push   $0x0
  pushl $231
80109181:	68 e7 00 00 00       	push   $0xe7
  jmp alltraps
80109186:	e9 83 f0 ff ff       	jmp    8010820e <alltraps>

8010918b <vector232>:
.globl vector232
vector232:
  pushl $0
8010918b:	6a 00                	push   $0x0
  pushl $232
8010918d:	68 e8 00 00 00       	push   $0xe8
  jmp alltraps
80109192:	e9 77 f0 ff ff       	jmp    8010820e <alltraps>

80109197 <vector233>:
.globl vector233
vector233:
  pushl $0
80109197:	6a 00                	push   $0x0
  pushl $233
80109199:	68 e9 00 00 00       	push   $0xe9
  jmp alltraps
8010919e:	e9 6b f0 ff ff       	jmp    8010820e <alltraps>

801091a3 <vector234>:
.globl vector234
vector234:
  pushl $0
801091a3:	6a 00                	push   $0x0
  pushl $234
801091a5:	68 ea 00 00 00       	push   $0xea
  jmp alltraps
801091aa:	e9 5f f0 ff ff       	jmp    8010820e <alltraps>

801091af <vector235>:
.globl vector235
vector235:
  pushl $0
801091af:	6a 00                	push   $0x0
  pushl $235
801091b1:	68 eb 00 00 00       	push   $0xeb
  jmp alltraps
801091b6:	e9 53 f0 ff ff       	jmp    8010820e <alltraps>

801091bb <vector236>:
.globl vector236
vector236:
  pushl $0
801091bb:	6a 00                	push   $0x0
  pushl $236
801091bd:	68 ec 00 00 00       	push   $0xec
  jmp alltraps
801091c2:	e9 47 f0 ff ff       	jmp    8010820e <alltraps>

801091c7 <vector237>:
.globl vector237
vector237:
  pushl $0
801091c7:	6a 00                	push   $0x0
  pushl $237
801091c9:	68 ed 00 00 00       	push   $0xed
  jmp alltraps
801091ce:	e9 3b f0 ff ff       	jmp    8010820e <alltraps>

801091d3 <vector238>:
.globl vector238
vector238:
  pushl $0
801091d3:	6a 00                	push   $0x0
  pushl $238
801091d5:	68 ee 00 00 00       	push   $0xee
  jmp alltraps
801091da:	e9 2f f0 ff ff       	jmp    8010820e <alltraps>

801091df <vector239>:
.globl vector239
vector239:
  pushl $0
801091df:	6a 00                	push   $0x0
  pushl $239
801091e1:	68 ef 00 00 00       	push   $0xef
  jmp alltraps
801091e6:	e9 23 f0 ff ff       	jmp    8010820e <alltraps>

801091eb <vector240>:
.globl vector240
vector240:
  pushl $0
801091eb:	6a 00                	push   $0x0
  pushl $240
801091ed:	68 f0 00 00 00       	push   $0xf0
  jmp alltraps
801091f2:	e9 17 f0 ff ff       	jmp    8010820e <alltraps>

801091f7 <vector241>:
.globl vector241
vector241:
  pushl $0
801091f7:	6a 00                	push   $0x0
  pushl $241
801091f9:	68 f1 00 00 00       	push   $0xf1
  jmp alltraps
801091fe:	e9 0b f0 ff ff       	jmp    8010820e <alltraps>

80109203 <vector242>:
.globl vector242
vector242:
  pushl $0
80109203:	6a 00                	push   $0x0
  pushl $242
80109205:	68 f2 00 00 00       	push   $0xf2
  jmp alltraps
8010920a:	e9 ff ef ff ff       	jmp    8010820e <alltraps>

8010920f <vector243>:
.globl vector243
vector243:
  pushl $0
8010920f:	6a 00                	push   $0x0
  pushl $243
80109211:	68 f3 00 00 00       	push   $0xf3
  jmp alltraps
80109216:	e9 f3 ef ff ff       	jmp    8010820e <alltraps>

8010921b <vector244>:
.globl vector244
vector244:
  pushl $0
8010921b:	6a 00                	push   $0x0
  pushl $244
8010921d:	68 f4 00 00 00       	push   $0xf4
  jmp alltraps
80109222:	e9 e7 ef ff ff       	jmp    8010820e <alltraps>

80109227 <vector245>:
.globl vector245
vector245:
  pushl $0
80109227:	6a 00                	push   $0x0
  pushl $245
80109229:	68 f5 00 00 00       	push   $0xf5
  jmp alltraps
8010922e:	e9 db ef ff ff       	jmp    8010820e <alltraps>

80109233 <vector246>:
.globl vector246
vector246:
  pushl $0
80109233:	6a 00                	push   $0x0
  pushl $246
80109235:	68 f6 00 00 00       	push   $0xf6
  jmp alltraps
8010923a:	e9 cf ef ff ff       	jmp    8010820e <alltraps>

8010923f <vector247>:
.globl vector247
vector247:
  pushl $0
8010923f:	6a 00                	push   $0x0
  pushl $247
80109241:	68 f7 00 00 00       	push   $0xf7
  jmp alltraps
80109246:	e9 c3 ef ff ff       	jmp    8010820e <alltraps>

8010924b <vector248>:
.globl vector248
vector248:
  pushl $0
8010924b:	6a 00                	push   $0x0
  pushl $248
8010924d:	68 f8 00 00 00       	push   $0xf8
  jmp alltraps
80109252:	e9 b7 ef ff ff       	jmp    8010820e <alltraps>

80109257 <vector249>:
.globl vector249
vector249:
  pushl $0
80109257:	6a 00                	push   $0x0
  pushl $249
80109259:	68 f9 00 00 00       	push   $0xf9
  jmp alltraps
8010925e:	e9 ab ef ff ff       	jmp    8010820e <alltraps>

80109263 <vector250>:
.globl vector250
vector250:
  pushl $0
80109263:	6a 00                	push   $0x0
  pushl $250
80109265:	68 fa 00 00 00       	push   $0xfa
  jmp alltraps
8010926a:	e9 9f ef ff ff       	jmp    8010820e <alltraps>

8010926f <vector251>:
.globl vector251
vector251:
  pushl $0
8010926f:	6a 00                	push   $0x0
  pushl $251
80109271:	68 fb 00 00 00       	push   $0xfb
  jmp alltraps
80109276:	e9 93 ef ff ff       	jmp    8010820e <alltraps>

8010927b <vector252>:
.globl vector252
vector252:
  pushl $0
8010927b:	6a 00                	push   $0x0
  pushl $252
8010927d:	68 fc 00 00 00       	push   $0xfc
  jmp alltraps
80109282:	e9 87 ef ff ff       	jmp    8010820e <alltraps>

80109287 <vector253>:
.globl vector253
vector253:
  pushl $0
80109287:	6a 00                	push   $0x0
  pushl $253
80109289:	68 fd 00 00 00       	push   $0xfd
  jmp alltraps
8010928e:	e9 7b ef ff ff       	jmp    8010820e <alltraps>

80109293 <vector254>:
.globl vector254
vector254:
  pushl $0
80109293:	6a 00                	push   $0x0
  pushl $254
80109295:	68 fe 00 00 00       	push   $0xfe
  jmp alltraps
8010929a:	e9 6f ef ff ff       	jmp    8010820e <alltraps>

8010929f <vector255>:
.globl vector255
vector255:
  pushl $0
8010929f:	6a 00                	push   $0x0
  pushl $255
801092a1:	68 ff 00 00 00       	push   $0xff
  jmp alltraps
801092a6:	e9 63 ef ff ff       	jmp    8010820e <alltraps>

801092ab <lgdt>:

struct segdesc;

static inline void
lgdt(struct segdesc *p, int size)
{
801092ab:	55                   	push   %ebp
801092ac:	89 e5                	mov    %esp,%ebp
801092ae:	83 ec 10             	sub    $0x10,%esp
  volatile ushort pd[3];

  pd[0] = size-1;
801092b1:	8b 45 0c             	mov    0xc(%ebp),%eax
801092b4:	83 e8 01             	sub    $0x1,%eax
801092b7:	66 89 45 fa          	mov    %ax,-0x6(%ebp)
  pd[1] = (uint)p;
801092bb:	8b 45 08             	mov    0x8(%ebp),%eax
801092be:	66 89 45 fc          	mov    %ax,-0x4(%ebp)
  pd[2] = (uint)p >> 16;
801092c2:	8b 45 08             	mov    0x8(%ebp),%eax
801092c5:	c1 e8 10             	shr    $0x10,%eax
801092c8:	66 89 45 fe          	mov    %ax,-0x2(%ebp)

  asm volatile("lgdt (%0)" : : "r" (pd));
801092cc:	8d 45 fa             	lea    -0x6(%ebp),%eax
801092cf:	0f 01 10             	lgdtl  (%eax)
}
801092d2:	90                   	nop
801092d3:	c9                   	leave  
801092d4:	c3                   	ret    

801092d5 <ltr>:
  asm volatile("lidt (%0)" : : "r" (pd));
}

static inline void
ltr(ushort sel)
{
801092d5:	55                   	push   %ebp
801092d6:	89 e5                	mov    %esp,%ebp
801092d8:	83 ec 04             	sub    $0x4,%esp
801092db:	8b 45 08             	mov    0x8(%ebp),%eax
801092de:	66 89 45 fc          	mov    %ax,-0x4(%ebp)
  asm volatile("ltr %0" : : "r" (sel));
801092e2:	0f b7 45 fc          	movzwl -0x4(%ebp),%eax
801092e6:	0f 00 d8             	ltr    %ax
}
801092e9:	90                   	nop
801092ea:	c9                   	leave  
801092eb:	c3                   	ret    

801092ec <loadgs>:
  return eflags;
}

static inline void
loadgs(ushort v)
{
801092ec:	55                   	push   %ebp
801092ed:	89 e5                	mov    %esp,%ebp
801092ef:	83 ec 04             	sub    $0x4,%esp
801092f2:	8b 45 08             	mov    0x8(%ebp),%eax
801092f5:	66 89 45 fc          	mov    %ax,-0x4(%ebp)
  asm volatile("movw %0, %%gs" : : "r" (v));
801092f9:	0f b7 45 fc          	movzwl -0x4(%ebp),%eax
801092fd:	8e e8                	mov    %eax,%gs
}
801092ff:	90                   	nop
80109300:	c9                   	leave  
80109301:	c3                   	ret    

80109302 <lcr3>:
  return val;
}

static inline void
lcr3(uint val) 
{
80109302:	55                   	push   %ebp
80109303:	89 e5                	mov    %esp,%ebp
  asm volatile("movl %0,%%cr3" : : "r" (val));
80109305:	8b 45 08             	mov    0x8(%ebp),%eax
80109308:	0f 22 d8             	mov    %eax,%cr3
}
8010930b:	90                   	nop
8010930c:	5d                   	pop    %ebp
8010930d:	c3                   	ret    

8010930e <v2p>:
#define KERNBASE 0x80000000         // First kernel virtual address
#define KERNLINK (KERNBASE+EXTMEM)  // Address where kernel is linked

#ifndef __ASSEMBLER__

static inline uint v2p(void *a) { return ((uint) (a))  - KERNBASE; }
8010930e:	55                   	push   %ebp
8010930f:	89 e5                	mov    %esp,%ebp
80109311:	8b 45 08             	mov    0x8(%ebp),%eax
80109314:	05 00 00 00 80       	add    $0x80000000,%eax
80109319:	5d                   	pop    %ebp
8010931a:	c3                   	ret    

8010931b <p2v>:
static inline void *p2v(uint a) { return (void *) ((a) + KERNBASE); }
8010931b:	55                   	push   %ebp
8010931c:	89 e5                	mov    %esp,%ebp
8010931e:	8b 45 08             	mov    0x8(%ebp),%eax
80109321:	05 00 00 00 80       	add    $0x80000000,%eax
80109326:	5d                   	pop    %ebp
80109327:	c3                   	ret    

80109328 <seginit>:

// Set up CPU's kernel segment descriptors.
// Run once on entry on each CPU.
void
seginit(void)
{
80109328:	55                   	push   %ebp
80109329:	89 e5                	mov    %esp,%ebp
8010932b:	53                   	push   %ebx
8010932c:	83 ec 14             	sub    $0x14,%esp

  // Map "logical" addresses to virtual addresses using identity map.
  // Cannot share a CODE descriptor for both kernel and user
  // because it would have to have DPL_USR, but the CPU forbids
  // an interrupt from CPL=0 to DPL=3.
  c = &cpus[cpunum()];
8010932f:	e8 36 9d ff ff       	call   8010306a <cpunum>
80109334:	69 c0 bc 00 00 00    	imul   $0xbc,%eax,%eax
8010933a:	05 80 43 11 80       	add    $0x80114380,%eax
8010933f:	89 45 f4             	mov    %eax,-0xc(%ebp)
  c->gdt[SEG_KCODE] = SEG(STA_X|STA_R, 0, 0xffffffff, 0);
80109342:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109345:	66 c7 40 78 ff ff    	movw   $0xffff,0x78(%eax)
8010934b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010934e:	66 c7 40 7a 00 00    	movw   $0x0,0x7a(%eax)
80109354:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109357:	c6 40 7c 00          	movb   $0x0,0x7c(%eax)
8010935b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010935e:	0f b6 50 7d          	movzbl 0x7d(%eax),%edx
80109362:	83 e2 f0             	and    $0xfffffff0,%edx
80109365:	83 ca 0a             	or     $0xa,%edx
80109368:	88 50 7d             	mov    %dl,0x7d(%eax)
8010936b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010936e:	0f b6 50 7d          	movzbl 0x7d(%eax),%edx
80109372:	83 ca 10             	or     $0x10,%edx
80109375:	88 50 7d             	mov    %dl,0x7d(%eax)
80109378:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010937b:	0f b6 50 7d          	movzbl 0x7d(%eax),%edx
8010937f:	83 e2 9f             	and    $0xffffff9f,%edx
80109382:	88 50 7d             	mov    %dl,0x7d(%eax)
80109385:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109388:	0f b6 50 7d          	movzbl 0x7d(%eax),%edx
8010938c:	83 ca 80             	or     $0xffffff80,%edx
8010938f:	88 50 7d             	mov    %dl,0x7d(%eax)
80109392:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109395:	0f b6 50 7e          	movzbl 0x7e(%eax),%edx
80109399:	83 ca 0f             	or     $0xf,%edx
8010939c:	88 50 7e             	mov    %dl,0x7e(%eax)
8010939f:	8b 45 f4             	mov    -0xc(%ebp),%eax
801093a2:	0f b6 50 7e          	movzbl 0x7e(%eax),%edx
801093a6:	83 e2 ef             	and    $0xffffffef,%edx
801093a9:	88 50 7e             	mov    %dl,0x7e(%eax)
801093ac:	8b 45 f4             	mov    -0xc(%ebp),%eax
801093af:	0f b6 50 7e          	movzbl 0x7e(%eax),%edx
801093b3:	83 e2 df             	and    $0xffffffdf,%edx
801093b6:	88 50 7e             	mov    %dl,0x7e(%eax)
801093b9:	8b 45 f4             	mov    -0xc(%ebp),%eax
801093bc:	0f b6 50 7e          	movzbl 0x7e(%eax),%edx
801093c0:	83 ca 40             	or     $0x40,%edx
801093c3:	88 50 7e             	mov    %dl,0x7e(%eax)
801093c6:	8b 45 f4             	mov    -0xc(%ebp),%eax
801093c9:	0f b6 50 7e          	movzbl 0x7e(%eax),%edx
801093cd:	83 ca 80             	or     $0xffffff80,%edx
801093d0:	88 50 7e             	mov    %dl,0x7e(%eax)
801093d3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801093d6:	c6 40 7f 00          	movb   $0x0,0x7f(%eax)
  c->gdt[SEG_KDATA] = SEG(STA_W, 0, 0xffffffff, 0);
801093da:	8b 45 f4             	mov    -0xc(%ebp),%eax
801093dd:	66 c7 80 80 00 00 00 	movw   $0xffff,0x80(%eax)
801093e4:	ff ff 
801093e6:	8b 45 f4             	mov    -0xc(%ebp),%eax
801093e9:	66 c7 80 82 00 00 00 	movw   $0x0,0x82(%eax)
801093f0:	00 00 
801093f2:	8b 45 f4             	mov    -0xc(%ebp),%eax
801093f5:	c6 80 84 00 00 00 00 	movb   $0x0,0x84(%eax)
801093fc:	8b 45 f4             	mov    -0xc(%ebp),%eax
801093ff:	0f b6 90 85 00 00 00 	movzbl 0x85(%eax),%edx
80109406:	83 e2 f0             	and    $0xfffffff0,%edx
80109409:	83 ca 02             	or     $0x2,%edx
8010940c:	88 90 85 00 00 00    	mov    %dl,0x85(%eax)
80109412:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109415:	0f b6 90 85 00 00 00 	movzbl 0x85(%eax),%edx
8010941c:	83 ca 10             	or     $0x10,%edx
8010941f:	88 90 85 00 00 00    	mov    %dl,0x85(%eax)
80109425:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109428:	0f b6 90 85 00 00 00 	movzbl 0x85(%eax),%edx
8010942f:	83 e2 9f             	and    $0xffffff9f,%edx
80109432:	88 90 85 00 00 00    	mov    %dl,0x85(%eax)
80109438:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010943b:	0f b6 90 85 00 00 00 	movzbl 0x85(%eax),%edx
80109442:	83 ca 80             	or     $0xffffff80,%edx
80109445:	88 90 85 00 00 00    	mov    %dl,0x85(%eax)
8010944b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010944e:	0f b6 90 86 00 00 00 	movzbl 0x86(%eax),%edx
80109455:	83 ca 0f             	or     $0xf,%edx
80109458:	88 90 86 00 00 00    	mov    %dl,0x86(%eax)
8010945e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109461:	0f b6 90 86 00 00 00 	movzbl 0x86(%eax),%edx
80109468:	83 e2 ef             	and    $0xffffffef,%edx
8010946b:	88 90 86 00 00 00    	mov    %dl,0x86(%eax)
80109471:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109474:	0f b6 90 86 00 00 00 	movzbl 0x86(%eax),%edx
8010947b:	83 e2 df             	and    $0xffffffdf,%edx
8010947e:	88 90 86 00 00 00    	mov    %dl,0x86(%eax)
80109484:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109487:	0f b6 90 86 00 00 00 	movzbl 0x86(%eax),%edx
8010948e:	83 ca 40             	or     $0x40,%edx
80109491:	88 90 86 00 00 00    	mov    %dl,0x86(%eax)
80109497:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010949a:	0f b6 90 86 00 00 00 	movzbl 0x86(%eax),%edx
801094a1:	83 ca 80             	or     $0xffffff80,%edx
801094a4:	88 90 86 00 00 00    	mov    %dl,0x86(%eax)
801094aa:	8b 45 f4             	mov    -0xc(%ebp),%eax
801094ad:	c6 80 87 00 00 00 00 	movb   $0x0,0x87(%eax)
  c->gdt[SEG_UCODE] = SEG(STA_X|STA_R, 0, 0xffffffff, DPL_USER);
801094b4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801094b7:	66 c7 80 90 00 00 00 	movw   $0xffff,0x90(%eax)
801094be:	ff ff 
801094c0:	8b 45 f4             	mov    -0xc(%ebp),%eax
801094c3:	66 c7 80 92 00 00 00 	movw   $0x0,0x92(%eax)
801094ca:	00 00 
801094cc:	8b 45 f4             	mov    -0xc(%ebp),%eax
801094cf:	c6 80 94 00 00 00 00 	movb   $0x0,0x94(%eax)
801094d6:	8b 45 f4             	mov    -0xc(%ebp),%eax
801094d9:	0f b6 90 95 00 00 00 	movzbl 0x95(%eax),%edx
801094e0:	83 e2 f0             	and    $0xfffffff0,%edx
801094e3:	83 ca 0a             	or     $0xa,%edx
801094e6:	88 90 95 00 00 00    	mov    %dl,0x95(%eax)
801094ec:	8b 45 f4             	mov    -0xc(%ebp),%eax
801094ef:	0f b6 90 95 00 00 00 	movzbl 0x95(%eax),%edx
801094f6:	83 ca 10             	or     $0x10,%edx
801094f9:	88 90 95 00 00 00    	mov    %dl,0x95(%eax)
801094ff:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109502:	0f b6 90 95 00 00 00 	movzbl 0x95(%eax),%edx
80109509:	83 ca 60             	or     $0x60,%edx
8010950c:	88 90 95 00 00 00    	mov    %dl,0x95(%eax)
80109512:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109515:	0f b6 90 95 00 00 00 	movzbl 0x95(%eax),%edx
8010951c:	83 ca 80             	or     $0xffffff80,%edx
8010951f:	88 90 95 00 00 00    	mov    %dl,0x95(%eax)
80109525:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109528:	0f b6 90 96 00 00 00 	movzbl 0x96(%eax),%edx
8010952f:	83 ca 0f             	or     $0xf,%edx
80109532:	88 90 96 00 00 00    	mov    %dl,0x96(%eax)
80109538:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010953b:	0f b6 90 96 00 00 00 	movzbl 0x96(%eax),%edx
80109542:	83 e2 ef             	and    $0xffffffef,%edx
80109545:	88 90 96 00 00 00    	mov    %dl,0x96(%eax)
8010954b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010954e:	0f b6 90 96 00 00 00 	movzbl 0x96(%eax),%edx
80109555:	83 e2 df             	and    $0xffffffdf,%edx
80109558:	88 90 96 00 00 00    	mov    %dl,0x96(%eax)
8010955e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109561:	0f b6 90 96 00 00 00 	movzbl 0x96(%eax),%edx
80109568:	83 ca 40             	or     $0x40,%edx
8010956b:	88 90 96 00 00 00    	mov    %dl,0x96(%eax)
80109571:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109574:	0f b6 90 96 00 00 00 	movzbl 0x96(%eax),%edx
8010957b:	83 ca 80             	or     $0xffffff80,%edx
8010957e:	88 90 96 00 00 00    	mov    %dl,0x96(%eax)
80109584:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109587:	c6 80 97 00 00 00 00 	movb   $0x0,0x97(%eax)
  c->gdt[SEG_UDATA] = SEG(STA_W, 0, 0xffffffff, DPL_USER);
8010958e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109591:	66 c7 80 98 00 00 00 	movw   $0xffff,0x98(%eax)
80109598:	ff ff 
8010959a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010959d:	66 c7 80 9a 00 00 00 	movw   $0x0,0x9a(%eax)
801095a4:	00 00 
801095a6:	8b 45 f4             	mov    -0xc(%ebp),%eax
801095a9:	c6 80 9c 00 00 00 00 	movb   $0x0,0x9c(%eax)
801095b0:	8b 45 f4             	mov    -0xc(%ebp),%eax
801095b3:	0f b6 90 9d 00 00 00 	movzbl 0x9d(%eax),%edx
801095ba:	83 e2 f0             	and    $0xfffffff0,%edx
801095bd:	83 ca 02             	or     $0x2,%edx
801095c0:	88 90 9d 00 00 00    	mov    %dl,0x9d(%eax)
801095c6:	8b 45 f4             	mov    -0xc(%ebp),%eax
801095c9:	0f b6 90 9d 00 00 00 	movzbl 0x9d(%eax),%edx
801095d0:	83 ca 10             	or     $0x10,%edx
801095d3:	88 90 9d 00 00 00    	mov    %dl,0x9d(%eax)
801095d9:	8b 45 f4             	mov    -0xc(%ebp),%eax
801095dc:	0f b6 90 9d 00 00 00 	movzbl 0x9d(%eax),%edx
801095e3:	83 ca 60             	or     $0x60,%edx
801095e6:	88 90 9d 00 00 00    	mov    %dl,0x9d(%eax)
801095ec:	8b 45 f4             	mov    -0xc(%ebp),%eax
801095ef:	0f b6 90 9d 00 00 00 	movzbl 0x9d(%eax),%edx
801095f6:	83 ca 80             	or     $0xffffff80,%edx
801095f9:	88 90 9d 00 00 00    	mov    %dl,0x9d(%eax)
801095ff:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109602:	0f b6 90 9e 00 00 00 	movzbl 0x9e(%eax),%edx
80109609:	83 ca 0f             	or     $0xf,%edx
8010960c:	88 90 9e 00 00 00    	mov    %dl,0x9e(%eax)
80109612:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109615:	0f b6 90 9e 00 00 00 	movzbl 0x9e(%eax),%edx
8010961c:	83 e2 ef             	and    $0xffffffef,%edx
8010961f:	88 90 9e 00 00 00    	mov    %dl,0x9e(%eax)
80109625:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109628:	0f b6 90 9e 00 00 00 	movzbl 0x9e(%eax),%edx
8010962f:	83 e2 df             	and    $0xffffffdf,%edx
80109632:	88 90 9e 00 00 00    	mov    %dl,0x9e(%eax)
80109638:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010963b:	0f b6 90 9e 00 00 00 	movzbl 0x9e(%eax),%edx
80109642:	83 ca 40             	or     $0x40,%edx
80109645:	88 90 9e 00 00 00    	mov    %dl,0x9e(%eax)
8010964b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010964e:	0f b6 90 9e 00 00 00 	movzbl 0x9e(%eax),%edx
80109655:	83 ca 80             	or     $0xffffff80,%edx
80109658:	88 90 9e 00 00 00    	mov    %dl,0x9e(%eax)
8010965e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109661:	c6 80 9f 00 00 00 00 	movb   $0x0,0x9f(%eax)

  // Map cpu, and curproc
  c->gdt[SEG_KCPU] = SEG(STA_W, &c->cpu, 8, 0);
80109668:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010966b:	05 b4 00 00 00       	add    $0xb4,%eax
80109670:	89 c3                	mov    %eax,%ebx
80109672:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109675:	05 b4 00 00 00       	add    $0xb4,%eax
8010967a:	c1 e8 10             	shr    $0x10,%eax
8010967d:	89 c2                	mov    %eax,%edx
8010967f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109682:	05 b4 00 00 00       	add    $0xb4,%eax
80109687:	c1 e8 18             	shr    $0x18,%eax
8010968a:	89 c1                	mov    %eax,%ecx
8010968c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010968f:	66 c7 80 88 00 00 00 	movw   $0x0,0x88(%eax)
80109696:	00 00 
80109698:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010969b:	66 89 98 8a 00 00 00 	mov    %bx,0x8a(%eax)
801096a2:	8b 45 f4             	mov    -0xc(%ebp),%eax
801096a5:	88 90 8c 00 00 00    	mov    %dl,0x8c(%eax)
801096ab:	8b 45 f4             	mov    -0xc(%ebp),%eax
801096ae:	0f b6 90 8d 00 00 00 	movzbl 0x8d(%eax),%edx
801096b5:	83 e2 f0             	and    $0xfffffff0,%edx
801096b8:	83 ca 02             	or     $0x2,%edx
801096bb:	88 90 8d 00 00 00    	mov    %dl,0x8d(%eax)
801096c1:	8b 45 f4             	mov    -0xc(%ebp),%eax
801096c4:	0f b6 90 8d 00 00 00 	movzbl 0x8d(%eax),%edx
801096cb:	83 ca 10             	or     $0x10,%edx
801096ce:	88 90 8d 00 00 00    	mov    %dl,0x8d(%eax)
801096d4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801096d7:	0f b6 90 8d 00 00 00 	movzbl 0x8d(%eax),%edx
801096de:	83 e2 9f             	and    $0xffffff9f,%edx
801096e1:	88 90 8d 00 00 00    	mov    %dl,0x8d(%eax)
801096e7:	8b 45 f4             	mov    -0xc(%ebp),%eax
801096ea:	0f b6 90 8d 00 00 00 	movzbl 0x8d(%eax),%edx
801096f1:	83 ca 80             	or     $0xffffff80,%edx
801096f4:	88 90 8d 00 00 00    	mov    %dl,0x8d(%eax)
801096fa:	8b 45 f4             	mov    -0xc(%ebp),%eax
801096fd:	0f b6 90 8e 00 00 00 	movzbl 0x8e(%eax),%edx
80109704:	83 e2 f0             	and    $0xfffffff0,%edx
80109707:	88 90 8e 00 00 00    	mov    %dl,0x8e(%eax)
8010970d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109710:	0f b6 90 8e 00 00 00 	movzbl 0x8e(%eax),%edx
80109717:	83 e2 ef             	and    $0xffffffef,%edx
8010971a:	88 90 8e 00 00 00    	mov    %dl,0x8e(%eax)
80109720:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109723:	0f b6 90 8e 00 00 00 	movzbl 0x8e(%eax),%edx
8010972a:	83 e2 df             	and    $0xffffffdf,%edx
8010972d:	88 90 8e 00 00 00    	mov    %dl,0x8e(%eax)
80109733:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109736:	0f b6 90 8e 00 00 00 	movzbl 0x8e(%eax),%edx
8010973d:	83 ca 40             	or     $0x40,%edx
80109740:	88 90 8e 00 00 00    	mov    %dl,0x8e(%eax)
80109746:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109749:	0f b6 90 8e 00 00 00 	movzbl 0x8e(%eax),%edx
80109750:	83 ca 80             	or     $0xffffff80,%edx
80109753:	88 90 8e 00 00 00    	mov    %dl,0x8e(%eax)
80109759:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010975c:	88 88 8f 00 00 00    	mov    %cl,0x8f(%eax)

  lgdt(c->gdt, sizeof(c->gdt));
80109762:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109765:	83 c0 70             	add    $0x70,%eax
80109768:	83 ec 08             	sub    $0x8,%esp
8010976b:	6a 38                	push   $0x38
8010976d:	50                   	push   %eax
8010976e:	e8 38 fb ff ff       	call   801092ab <lgdt>
80109773:	83 c4 10             	add    $0x10,%esp
  loadgs(SEG_KCPU << 3);
80109776:	83 ec 0c             	sub    $0xc,%esp
80109779:	6a 18                	push   $0x18
8010977b:	e8 6c fb ff ff       	call   801092ec <loadgs>
80109780:	83 c4 10             	add    $0x10,%esp
  
  // Initialize cpu-local storage.
  cpu = c;
80109783:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109786:	65 a3 00 00 00 00    	mov    %eax,%gs:0x0
  proc = 0;
8010978c:	65 c7 05 04 00 00 00 	movl   $0x0,%gs:0x4
80109793:	00 00 00 00 
}
80109797:	90                   	nop
80109798:	8b 5d fc             	mov    -0x4(%ebp),%ebx
8010979b:	c9                   	leave  
8010979c:	c3                   	ret    

8010979d <walkpgdir>:
// Return the address of the PTE in page table pgdir
// that corresponds to virtual address va.  If alloc!=0,
// create any required page table pages.
static pte_t *
walkpgdir(pde_t *pgdir, const void *va, int alloc)
{
8010979d:	55                   	push   %ebp
8010979e:	89 e5                	mov    %esp,%ebp
801097a0:	83 ec 18             	sub    $0x18,%esp
  pde_t *pde;
  pte_t *pgtab;

  pde = &pgdir[PDX(va)];
801097a3:	8b 45 0c             	mov    0xc(%ebp),%eax
801097a6:	c1 e8 16             	shr    $0x16,%eax
801097a9:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
801097b0:	8b 45 08             	mov    0x8(%ebp),%eax
801097b3:	01 d0                	add    %edx,%eax
801097b5:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(*pde & PTE_P){
801097b8:	8b 45 f0             	mov    -0x10(%ebp),%eax
801097bb:	8b 00                	mov    (%eax),%eax
801097bd:	83 e0 01             	and    $0x1,%eax
801097c0:	85 c0                	test   %eax,%eax
801097c2:	74 18                	je     801097dc <walkpgdir+0x3f>
    pgtab = (pte_t*)p2v(PTE_ADDR(*pde));
801097c4:	8b 45 f0             	mov    -0x10(%ebp),%eax
801097c7:	8b 00                	mov    (%eax),%eax
801097c9:	25 00 f0 ff ff       	and    $0xfffff000,%eax
801097ce:	50                   	push   %eax
801097cf:	e8 47 fb ff ff       	call   8010931b <p2v>
801097d4:	83 c4 04             	add    $0x4,%esp
801097d7:	89 45 f4             	mov    %eax,-0xc(%ebp)
801097da:	eb 48                	jmp    80109824 <walkpgdir+0x87>
  } else {
    if(!alloc || (pgtab = (pte_t*)kalloc()) == 0)
801097dc:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
801097e0:	74 0e                	je     801097f0 <walkpgdir+0x53>
801097e2:	e8 1d 95 ff ff       	call   80102d04 <kalloc>
801097e7:	89 45 f4             	mov    %eax,-0xc(%ebp)
801097ea:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801097ee:	75 07                	jne    801097f7 <walkpgdir+0x5a>
      return 0;
801097f0:	b8 00 00 00 00       	mov    $0x0,%eax
801097f5:	eb 44                	jmp    8010983b <walkpgdir+0x9e>
    // Make sure all those PTE_P bits are zero.
    memset(pgtab, 0, PGSIZE);
801097f7:	83 ec 04             	sub    $0x4,%esp
801097fa:	68 00 10 00 00       	push   $0x1000
801097ff:	6a 00                	push   $0x0
80109801:	ff 75 f4             	pushl  -0xc(%ebp)
80109804:	e8 8f d4 ff ff       	call   80106c98 <memset>
80109809:	83 c4 10             	add    $0x10,%esp
    // The permissions here are overly generous, but they can
    // be further restricted by the permissions in the page table 
    // entries, if necessary.
    *pde = v2p(pgtab) | PTE_P | PTE_W | PTE_U;
8010980c:	83 ec 0c             	sub    $0xc,%esp
8010980f:	ff 75 f4             	pushl  -0xc(%ebp)
80109812:	e8 f7 fa ff ff       	call   8010930e <v2p>
80109817:	83 c4 10             	add    $0x10,%esp
8010981a:	83 c8 07             	or     $0x7,%eax
8010981d:	89 c2                	mov    %eax,%edx
8010981f:	8b 45 f0             	mov    -0x10(%ebp),%eax
80109822:	89 10                	mov    %edx,(%eax)
  }
  return &pgtab[PTX(va)];
80109824:	8b 45 0c             	mov    0xc(%ebp),%eax
80109827:	c1 e8 0c             	shr    $0xc,%eax
8010982a:	25 ff 03 00 00       	and    $0x3ff,%eax
8010982f:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
80109836:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109839:	01 d0                	add    %edx,%eax
}
8010983b:	c9                   	leave  
8010983c:	c3                   	ret    

8010983d <mappages>:
// Create PTEs for virtual addresses starting at va that refer to
// physical addresses starting at pa. va and size might not
// be page-aligned.
static int
mappages(pde_t *pgdir, void *va, uint size, uint pa, int perm)
{
8010983d:	55                   	push   %ebp
8010983e:	89 e5                	mov    %esp,%ebp
80109840:	83 ec 18             	sub    $0x18,%esp
  char *a, *last;
  pte_t *pte;
  
  a = (char*)PGROUNDDOWN((uint)va);
80109843:	8b 45 0c             	mov    0xc(%ebp),%eax
80109846:	25 00 f0 ff ff       	and    $0xfffff000,%eax
8010984b:	89 45 f4             	mov    %eax,-0xc(%ebp)
  last = (char*)PGROUNDDOWN(((uint)va) + size - 1);
8010984e:	8b 55 0c             	mov    0xc(%ebp),%edx
80109851:	8b 45 10             	mov    0x10(%ebp),%eax
80109854:	01 d0                	add    %edx,%eax
80109856:	83 e8 01             	sub    $0x1,%eax
80109859:	25 00 f0 ff ff       	and    $0xfffff000,%eax
8010985e:	89 45 f0             	mov    %eax,-0x10(%ebp)
  for(;;){
    if((pte = walkpgdir(pgdir, a, 1)) == 0)
80109861:	83 ec 04             	sub    $0x4,%esp
80109864:	6a 01                	push   $0x1
80109866:	ff 75 f4             	pushl  -0xc(%ebp)
80109869:	ff 75 08             	pushl  0x8(%ebp)
8010986c:	e8 2c ff ff ff       	call   8010979d <walkpgdir>
80109871:	83 c4 10             	add    $0x10,%esp
80109874:	89 45 ec             	mov    %eax,-0x14(%ebp)
80109877:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
8010987b:	75 07                	jne    80109884 <mappages+0x47>
      return -1;
8010987d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80109882:	eb 47                	jmp    801098cb <mappages+0x8e>
    if(*pte & PTE_P)
80109884:	8b 45 ec             	mov    -0x14(%ebp),%eax
80109887:	8b 00                	mov    (%eax),%eax
80109889:	83 e0 01             	and    $0x1,%eax
8010988c:	85 c0                	test   %eax,%eax
8010988e:	74 0d                	je     8010989d <mappages+0x60>
      panic("remap");
80109890:	83 ec 0c             	sub    $0xc,%esp
80109893:	68 a8 ae 10 80       	push   $0x8010aea8
80109898:	e8 c9 6c ff ff       	call   80100566 <panic>
    *pte = pa | perm | PTE_P;
8010989d:	8b 45 18             	mov    0x18(%ebp),%eax
801098a0:	0b 45 14             	or     0x14(%ebp),%eax
801098a3:	83 c8 01             	or     $0x1,%eax
801098a6:	89 c2                	mov    %eax,%edx
801098a8:	8b 45 ec             	mov    -0x14(%ebp),%eax
801098ab:	89 10                	mov    %edx,(%eax)
    if(a == last)
801098ad:	8b 45 f4             	mov    -0xc(%ebp),%eax
801098b0:	3b 45 f0             	cmp    -0x10(%ebp),%eax
801098b3:	74 10                	je     801098c5 <mappages+0x88>
      break;
    a += PGSIZE;
801098b5:	81 45 f4 00 10 00 00 	addl   $0x1000,-0xc(%ebp)
    pa += PGSIZE;
801098bc:	81 45 14 00 10 00 00 	addl   $0x1000,0x14(%ebp)
  }
801098c3:	eb 9c                	jmp    80109861 <mappages+0x24>
      return -1;
    if(*pte & PTE_P)
      panic("remap");
    *pte = pa | perm | PTE_P;
    if(a == last)
      break;
801098c5:	90                   	nop
    a += PGSIZE;
    pa += PGSIZE;
  }
  return 0;
801098c6:	b8 00 00 00 00       	mov    $0x0,%eax
}
801098cb:	c9                   	leave  
801098cc:	c3                   	ret    

801098cd <setupkvm>:
};

// Set up kernel part of a page table.
pde_t*
setupkvm(void)
{
801098cd:	55                   	push   %ebp
801098ce:	89 e5                	mov    %esp,%ebp
801098d0:	53                   	push   %ebx
801098d1:	83 ec 14             	sub    $0x14,%esp
  pde_t *pgdir;
  struct kmap *k;

  if((pgdir = (pde_t*)kalloc()) == 0)
801098d4:	e8 2b 94 ff ff       	call   80102d04 <kalloc>
801098d9:	89 45 f0             	mov    %eax,-0x10(%ebp)
801098dc:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
801098e0:	75 0a                	jne    801098ec <setupkvm+0x1f>
    return 0;
801098e2:	b8 00 00 00 00       	mov    $0x0,%eax
801098e7:	e9 8e 00 00 00       	jmp    8010997a <setupkvm+0xad>
  memset(pgdir, 0, PGSIZE);
801098ec:	83 ec 04             	sub    $0x4,%esp
801098ef:	68 00 10 00 00       	push   $0x1000
801098f4:	6a 00                	push   $0x0
801098f6:	ff 75 f0             	pushl  -0x10(%ebp)
801098f9:	e8 9a d3 ff ff       	call   80106c98 <memset>
801098fe:	83 c4 10             	add    $0x10,%esp
  if (p2v(PHYSTOP) > (void*)DEVSPACE)
80109901:	83 ec 0c             	sub    $0xc,%esp
80109904:	68 00 00 00 0e       	push   $0xe000000
80109909:	e8 0d fa ff ff       	call   8010931b <p2v>
8010990e:	83 c4 10             	add    $0x10,%esp
80109911:	3d 00 00 00 fe       	cmp    $0xfe000000,%eax
80109916:	76 0d                	jbe    80109925 <setupkvm+0x58>
    panic("PHYSTOP too high");
80109918:	83 ec 0c             	sub    $0xc,%esp
8010991b:	68 ae ae 10 80       	push   $0x8010aeae
80109920:	e8 41 6c ff ff       	call   80100566 <panic>
  for(k = kmap; k < &kmap[NELEM(kmap)]; k++)
80109925:	c7 45 f4 c0 d4 10 80 	movl   $0x8010d4c0,-0xc(%ebp)
8010992c:	eb 40                	jmp    8010996e <setupkvm+0xa1>
    if(mappages(pgdir, k->virt, k->phys_end - k->phys_start, 
8010992e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109931:	8b 48 0c             	mov    0xc(%eax),%ecx
                (uint)k->phys_start, k->perm) < 0)
80109934:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109937:	8b 50 04             	mov    0x4(%eax),%edx
    return 0;
  memset(pgdir, 0, PGSIZE);
  if (p2v(PHYSTOP) > (void*)DEVSPACE)
    panic("PHYSTOP too high");
  for(k = kmap; k < &kmap[NELEM(kmap)]; k++)
    if(mappages(pgdir, k->virt, k->phys_end - k->phys_start, 
8010993a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010993d:	8b 58 08             	mov    0x8(%eax),%ebx
80109940:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109943:	8b 40 04             	mov    0x4(%eax),%eax
80109946:	29 c3                	sub    %eax,%ebx
80109948:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010994b:	8b 00                	mov    (%eax),%eax
8010994d:	83 ec 0c             	sub    $0xc,%esp
80109950:	51                   	push   %ecx
80109951:	52                   	push   %edx
80109952:	53                   	push   %ebx
80109953:	50                   	push   %eax
80109954:	ff 75 f0             	pushl  -0x10(%ebp)
80109957:	e8 e1 fe ff ff       	call   8010983d <mappages>
8010995c:	83 c4 20             	add    $0x20,%esp
8010995f:	85 c0                	test   %eax,%eax
80109961:	79 07                	jns    8010996a <setupkvm+0x9d>
                (uint)k->phys_start, k->perm) < 0)
      return 0;
80109963:	b8 00 00 00 00       	mov    $0x0,%eax
80109968:	eb 10                	jmp    8010997a <setupkvm+0xad>
  if((pgdir = (pde_t*)kalloc()) == 0)
    return 0;
  memset(pgdir, 0, PGSIZE);
  if (p2v(PHYSTOP) > (void*)DEVSPACE)
    panic("PHYSTOP too high");
  for(k = kmap; k < &kmap[NELEM(kmap)]; k++)
8010996a:	83 45 f4 10          	addl   $0x10,-0xc(%ebp)
8010996e:	81 7d f4 00 d5 10 80 	cmpl   $0x8010d500,-0xc(%ebp)
80109975:	72 b7                	jb     8010992e <setupkvm+0x61>
    if(mappages(pgdir, k->virt, k->phys_end - k->phys_start, 
                (uint)k->phys_start, k->perm) < 0)
      return 0;
  return pgdir;
80109977:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
8010997a:	8b 5d fc             	mov    -0x4(%ebp),%ebx
8010997d:	c9                   	leave  
8010997e:	c3                   	ret    

8010997f <kvmalloc>:

// Allocate one page table for the machine for the kernel address
// space for scheduler processes.
void
kvmalloc(void)
{
8010997f:	55                   	push   %ebp
80109980:	89 e5                	mov    %esp,%ebp
80109982:	83 ec 08             	sub    $0x8,%esp
  kpgdir = setupkvm();
80109985:	e8 43 ff ff ff       	call   801098cd <setupkvm>
8010998a:	a3 58 79 11 80       	mov    %eax,0x80117958
  switchkvm();
8010998f:	e8 03 00 00 00       	call   80109997 <switchkvm>
}
80109994:	90                   	nop
80109995:	c9                   	leave  
80109996:	c3                   	ret    

80109997 <switchkvm>:

// Switch h/w page table register to the kernel-only page table,
// for when no process is running.
void
switchkvm(void)
{
80109997:	55                   	push   %ebp
80109998:	89 e5                	mov    %esp,%ebp
  lcr3(v2p(kpgdir));   // switch to the kernel page table
8010999a:	a1 58 79 11 80       	mov    0x80117958,%eax
8010999f:	50                   	push   %eax
801099a0:	e8 69 f9 ff ff       	call   8010930e <v2p>
801099a5:	83 c4 04             	add    $0x4,%esp
801099a8:	50                   	push   %eax
801099a9:	e8 54 f9 ff ff       	call   80109302 <lcr3>
801099ae:	83 c4 04             	add    $0x4,%esp
}
801099b1:	90                   	nop
801099b2:	c9                   	leave  
801099b3:	c3                   	ret    

801099b4 <switchuvm>:

// Switch TSS and h/w page table to correspond to process p.
void
switchuvm(struct proc *p)
{
801099b4:	55                   	push   %ebp
801099b5:	89 e5                	mov    %esp,%ebp
801099b7:	56                   	push   %esi
801099b8:	53                   	push   %ebx
  pushcli();
801099b9:	e8 d4 d1 ff ff       	call   80106b92 <pushcli>
  cpu->gdt[SEG_TSS] = SEG16(STS_T32A, &cpu->ts, sizeof(cpu->ts)-1, 0);
801099be:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
801099c4:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
801099cb:	83 c2 08             	add    $0x8,%edx
801099ce:	89 d6                	mov    %edx,%esi
801099d0:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
801099d7:	83 c2 08             	add    $0x8,%edx
801099da:	c1 ea 10             	shr    $0x10,%edx
801099dd:	89 d3                	mov    %edx,%ebx
801099df:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
801099e6:	83 c2 08             	add    $0x8,%edx
801099e9:	c1 ea 18             	shr    $0x18,%edx
801099ec:	89 d1                	mov    %edx,%ecx
801099ee:	66 c7 80 a0 00 00 00 	movw   $0x67,0xa0(%eax)
801099f5:	67 00 
801099f7:	66 89 b0 a2 00 00 00 	mov    %si,0xa2(%eax)
801099fe:	88 98 a4 00 00 00    	mov    %bl,0xa4(%eax)
80109a04:	0f b6 90 a5 00 00 00 	movzbl 0xa5(%eax),%edx
80109a0b:	83 e2 f0             	and    $0xfffffff0,%edx
80109a0e:	83 ca 09             	or     $0x9,%edx
80109a11:	88 90 a5 00 00 00    	mov    %dl,0xa5(%eax)
80109a17:	0f b6 90 a5 00 00 00 	movzbl 0xa5(%eax),%edx
80109a1e:	83 ca 10             	or     $0x10,%edx
80109a21:	88 90 a5 00 00 00    	mov    %dl,0xa5(%eax)
80109a27:	0f b6 90 a5 00 00 00 	movzbl 0xa5(%eax),%edx
80109a2e:	83 e2 9f             	and    $0xffffff9f,%edx
80109a31:	88 90 a5 00 00 00    	mov    %dl,0xa5(%eax)
80109a37:	0f b6 90 a5 00 00 00 	movzbl 0xa5(%eax),%edx
80109a3e:	83 ca 80             	or     $0xffffff80,%edx
80109a41:	88 90 a5 00 00 00    	mov    %dl,0xa5(%eax)
80109a47:	0f b6 90 a6 00 00 00 	movzbl 0xa6(%eax),%edx
80109a4e:	83 e2 f0             	and    $0xfffffff0,%edx
80109a51:	88 90 a6 00 00 00    	mov    %dl,0xa6(%eax)
80109a57:	0f b6 90 a6 00 00 00 	movzbl 0xa6(%eax),%edx
80109a5e:	83 e2 ef             	and    $0xffffffef,%edx
80109a61:	88 90 a6 00 00 00    	mov    %dl,0xa6(%eax)
80109a67:	0f b6 90 a6 00 00 00 	movzbl 0xa6(%eax),%edx
80109a6e:	83 e2 df             	and    $0xffffffdf,%edx
80109a71:	88 90 a6 00 00 00    	mov    %dl,0xa6(%eax)
80109a77:	0f b6 90 a6 00 00 00 	movzbl 0xa6(%eax),%edx
80109a7e:	83 ca 40             	or     $0x40,%edx
80109a81:	88 90 a6 00 00 00    	mov    %dl,0xa6(%eax)
80109a87:	0f b6 90 a6 00 00 00 	movzbl 0xa6(%eax),%edx
80109a8e:	83 e2 7f             	and    $0x7f,%edx
80109a91:	88 90 a6 00 00 00    	mov    %dl,0xa6(%eax)
80109a97:	88 88 a7 00 00 00    	mov    %cl,0xa7(%eax)
  cpu->gdt[SEG_TSS].s = 0;
80109a9d:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80109aa3:	0f b6 90 a5 00 00 00 	movzbl 0xa5(%eax),%edx
80109aaa:	83 e2 ef             	and    $0xffffffef,%edx
80109aad:	88 90 a5 00 00 00    	mov    %dl,0xa5(%eax)
  cpu->ts.ss0 = SEG_KDATA << 3;
80109ab3:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80109ab9:	66 c7 40 10 10 00    	movw   $0x10,0x10(%eax)
  cpu->ts.esp0 = (uint)proc->kstack + KSTACKSIZE;
80109abf:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80109ac5:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
80109acc:	8b 52 08             	mov    0x8(%edx),%edx
80109acf:	81 c2 00 10 00 00    	add    $0x1000,%edx
80109ad5:	89 50 0c             	mov    %edx,0xc(%eax)
  ltr(SEG_TSS << 3);
80109ad8:	83 ec 0c             	sub    $0xc,%esp
80109adb:	6a 30                	push   $0x30
80109add:	e8 f3 f7 ff ff       	call   801092d5 <ltr>
80109ae2:	83 c4 10             	add    $0x10,%esp
  if(p->pgdir == 0)
80109ae5:	8b 45 08             	mov    0x8(%ebp),%eax
80109ae8:	8b 40 04             	mov    0x4(%eax),%eax
80109aeb:	85 c0                	test   %eax,%eax
80109aed:	75 0d                	jne    80109afc <switchuvm+0x148>
    panic("switchuvm: no pgdir");
80109aef:	83 ec 0c             	sub    $0xc,%esp
80109af2:	68 bf ae 10 80       	push   $0x8010aebf
80109af7:	e8 6a 6a ff ff       	call   80100566 <panic>
  lcr3(v2p(p->pgdir));  // switch to new address space
80109afc:	8b 45 08             	mov    0x8(%ebp),%eax
80109aff:	8b 40 04             	mov    0x4(%eax),%eax
80109b02:	83 ec 0c             	sub    $0xc,%esp
80109b05:	50                   	push   %eax
80109b06:	e8 03 f8 ff ff       	call   8010930e <v2p>
80109b0b:	83 c4 10             	add    $0x10,%esp
80109b0e:	83 ec 0c             	sub    $0xc,%esp
80109b11:	50                   	push   %eax
80109b12:	e8 eb f7 ff ff       	call   80109302 <lcr3>
80109b17:	83 c4 10             	add    $0x10,%esp
  popcli();
80109b1a:	e8 b8 d0 ff ff       	call   80106bd7 <popcli>
}
80109b1f:	90                   	nop
80109b20:	8d 65 f8             	lea    -0x8(%ebp),%esp
80109b23:	5b                   	pop    %ebx
80109b24:	5e                   	pop    %esi
80109b25:	5d                   	pop    %ebp
80109b26:	c3                   	ret    

80109b27 <inituvm>:

// Load the initcode into address 0 of pgdir.
// sz must be less than a page.
void
inituvm(pde_t *pgdir, char *init, uint sz)
{
80109b27:	55                   	push   %ebp
80109b28:	89 e5                	mov    %esp,%ebp
80109b2a:	83 ec 18             	sub    $0x18,%esp
  char *mem;
  
  if(sz >= PGSIZE)
80109b2d:	81 7d 10 ff 0f 00 00 	cmpl   $0xfff,0x10(%ebp)
80109b34:	76 0d                	jbe    80109b43 <inituvm+0x1c>
    panic("inituvm: more than a page");
80109b36:	83 ec 0c             	sub    $0xc,%esp
80109b39:	68 d3 ae 10 80       	push   $0x8010aed3
80109b3e:	e8 23 6a ff ff       	call   80100566 <panic>
  mem = kalloc();
80109b43:	e8 bc 91 ff ff       	call   80102d04 <kalloc>
80109b48:	89 45 f4             	mov    %eax,-0xc(%ebp)
  memset(mem, 0, PGSIZE);
80109b4b:	83 ec 04             	sub    $0x4,%esp
80109b4e:	68 00 10 00 00       	push   $0x1000
80109b53:	6a 00                	push   $0x0
80109b55:	ff 75 f4             	pushl  -0xc(%ebp)
80109b58:	e8 3b d1 ff ff       	call   80106c98 <memset>
80109b5d:	83 c4 10             	add    $0x10,%esp
  mappages(pgdir, 0, PGSIZE, v2p(mem), PTE_W|PTE_U);
80109b60:	83 ec 0c             	sub    $0xc,%esp
80109b63:	ff 75 f4             	pushl  -0xc(%ebp)
80109b66:	e8 a3 f7 ff ff       	call   8010930e <v2p>
80109b6b:	83 c4 10             	add    $0x10,%esp
80109b6e:	83 ec 0c             	sub    $0xc,%esp
80109b71:	6a 06                	push   $0x6
80109b73:	50                   	push   %eax
80109b74:	68 00 10 00 00       	push   $0x1000
80109b79:	6a 00                	push   $0x0
80109b7b:	ff 75 08             	pushl  0x8(%ebp)
80109b7e:	e8 ba fc ff ff       	call   8010983d <mappages>
80109b83:	83 c4 20             	add    $0x20,%esp
  memmove(mem, init, sz);
80109b86:	83 ec 04             	sub    $0x4,%esp
80109b89:	ff 75 10             	pushl  0x10(%ebp)
80109b8c:	ff 75 0c             	pushl  0xc(%ebp)
80109b8f:	ff 75 f4             	pushl  -0xc(%ebp)
80109b92:	e8 c0 d1 ff ff       	call   80106d57 <memmove>
80109b97:	83 c4 10             	add    $0x10,%esp
}
80109b9a:	90                   	nop
80109b9b:	c9                   	leave  
80109b9c:	c3                   	ret    

80109b9d <loaduvm>:

// Load a program segment into pgdir.  addr must be page-aligned
// and the pages from addr to addr+sz must already be mapped.
int
loaduvm(pde_t *pgdir, char *addr, struct inode *ip, uint offset, uint sz)
{
80109b9d:	55                   	push   %ebp
80109b9e:	89 e5                	mov    %esp,%ebp
80109ba0:	53                   	push   %ebx
80109ba1:	83 ec 14             	sub    $0x14,%esp
  uint i, pa, n;
  pte_t *pte;

  if((uint) addr % PGSIZE != 0)
80109ba4:	8b 45 0c             	mov    0xc(%ebp),%eax
80109ba7:	25 ff 0f 00 00       	and    $0xfff,%eax
80109bac:	85 c0                	test   %eax,%eax
80109bae:	74 0d                	je     80109bbd <loaduvm+0x20>
    panic("loaduvm: addr must be page aligned");
80109bb0:	83 ec 0c             	sub    $0xc,%esp
80109bb3:	68 f0 ae 10 80       	push   $0x8010aef0
80109bb8:	e8 a9 69 ff ff       	call   80100566 <panic>
  for(i = 0; i < sz; i += PGSIZE){
80109bbd:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80109bc4:	e9 95 00 00 00       	jmp    80109c5e <loaduvm+0xc1>
    if((pte = walkpgdir(pgdir, addr+i, 0)) == 0)
80109bc9:	8b 55 0c             	mov    0xc(%ebp),%edx
80109bcc:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109bcf:	01 d0                	add    %edx,%eax
80109bd1:	83 ec 04             	sub    $0x4,%esp
80109bd4:	6a 00                	push   $0x0
80109bd6:	50                   	push   %eax
80109bd7:	ff 75 08             	pushl  0x8(%ebp)
80109bda:	e8 be fb ff ff       	call   8010979d <walkpgdir>
80109bdf:	83 c4 10             	add    $0x10,%esp
80109be2:	89 45 ec             	mov    %eax,-0x14(%ebp)
80109be5:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
80109be9:	75 0d                	jne    80109bf8 <loaduvm+0x5b>
      panic("loaduvm: address should exist");
80109beb:	83 ec 0c             	sub    $0xc,%esp
80109bee:	68 13 af 10 80       	push   $0x8010af13
80109bf3:	e8 6e 69 ff ff       	call   80100566 <panic>
    pa = PTE_ADDR(*pte);
80109bf8:	8b 45 ec             	mov    -0x14(%ebp),%eax
80109bfb:	8b 00                	mov    (%eax),%eax
80109bfd:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80109c02:	89 45 e8             	mov    %eax,-0x18(%ebp)
    if(sz - i < PGSIZE)
80109c05:	8b 45 18             	mov    0x18(%ebp),%eax
80109c08:	2b 45 f4             	sub    -0xc(%ebp),%eax
80109c0b:	3d ff 0f 00 00       	cmp    $0xfff,%eax
80109c10:	77 0b                	ja     80109c1d <loaduvm+0x80>
      n = sz - i;
80109c12:	8b 45 18             	mov    0x18(%ebp),%eax
80109c15:	2b 45 f4             	sub    -0xc(%ebp),%eax
80109c18:	89 45 f0             	mov    %eax,-0x10(%ebp)
80109c1b:	eb 07                	jmp    80109c24 <loaduvm+0x87>
    else
      n = PGSIZE;
80109c1d:	c7 45 f0 00 10 00 00 	movl   $0x1000,-0x10(%ebp)
    if(readi(ip, p2v(pa), offset+i, n) != n)
80109c24:	8b 55 14             	mov    0x14(%ebp),%edx
80109c27:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109c2a:	8d 1c 02             	lea    (%edx,%eax,1),%ebx
80109c2d:	83 ec 0c             	sub    $0xc,%esp
80109c30:	ff 75 e8             	pushl  -0x18(%ebp)
80109c33:	e8 e3 f6 ff ff       	call   8010931b <p2v>
80109c38:	83 c4 10             	add    $0x10,%esp
80109c3b:	ff 75 f0             	pushl  -0x10(%ebp)
80109c3e:	53                   	push   %ebx
80109c3f:	50                   	push   %eax
80109c40:	ff 75 10             	pushl  0x10(%ebp)
80109c43:	e8 2e 83 ff ff       	call   80101f76 <readi>
80109c48:	83 c4 10             	add    $0x10,%esp
80109c4b:	3b 45 f0             	cmp    -0x10(%ebp),%eax
80109c4e:	74 07                	je     80109c57 <loaduvm+0xba>
      return -1;
80109c50:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80109c55:	eb 18                	jmp    80109c6f <loaduvm+0xd2>
  uint i, pa, n;
  pte_t *pte;

  if((uint) addr % PGSIZE != 0)
    panic("loaduvm: addr must be page aligned");
  for(i = 0; i < sz; i += PGSIZE){
80109c57:	81 45 f4 00 10 00 00 	addl   $0x1000,-0xc(%ebp)
80109c5e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109c61:	3b 45 18             	cmp    0x18(%ebp),%eax
80109c64:	0f 82 5f ff ff ff    	jb     80109bc9 <loaduvm+0x2c>
    else
      n = PGSIZE;
    if(readi(ip, p2v(pa), offset+i, n) != n)
      return -1;
  }
  return 0;
80109c6a:	b8 00 00 00 00       	mov    $0x0,%eax
}
80109c6f:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80109c72:	c9                   	leave  
80109c73:	c3                   	ret    

80109c74 <allocuvm>:

// Allocate page tables and physical memory to grow process from oldsz to
// newsz, which need not be page aligned.  Returns new size or 0 on error.
int
allocuvm(pde_t *pgdir, uint oldsz, uint newsz)
{
80109c74:	55                   	push   %ebp
80109c75:	89 e5                	mov    %esp,%ebp
80109c77:	83 ec 18             	sub    $0x18,%esp
  char *mem;
  uint a;

  if(newsz >= KERNBASE)
80109c7a:	8b 45 10             	mov    0x10(%ebp),%eax
80109c7d:	85 c0                	test   %eax,%eax
80109c7f:	79 0a                	jns    80109c8b <allocuvm+0x17>
    return 0;
80109c81:	b8 00 00 00 00       	mov    $0x0,%eax
80109c86:	e9 b0 00 00 00       	jmp    80109d3b <allocuvm+0xc7>
  if(newsz < oldsz)
80109c8b:	8b 45 10             	mov    0x10(%ebp),%eax
80109c8e:	3b 45 0c             	cmp    0xc(%ebp),%eax
80109c91:	73 08                	jae    80109c9b <allocuvm+0x27>
    return oldsz;
80109c93:	8b 45 0c             	mov    0xc(%ebp),%eax
80109c96:	e9 a0 00 00 00       	jmp    80109d3b <allocuvm+0xc7>

  a = PGROUNDUP(oldsz);
80109c9b:	8b 45 0c             	mov    0xc(%ebp),%eax
80109c9e:	05 ff 0f 00 00       	add    $0xfff,%eax
80109ca3:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80109ca8:	89 45 f4             	mov    %eax,-0xc(%ebp)
  for(; a < newsz; a += PGSIZE){
80109cab:	eb 7f                	jmp    80109d2c <allocuvm+0xb8>
    mem = kalloc();
80109cad:	e8 52 90 ff ff       	call   80102d04 <kalloc>
80109cb2:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if(mem == 0){
80109cb5:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80109cb9:	75 2b                	jne    80109ce6 <allocuvm+0x72>
      cprintf("allocuvm out of memory\n");
80109cbb:	83 ec 0c             	sub    $0xc,%esp
80109cbe:	68 31 af 10 80       	push   $0x8010af31
80109cc3:	e8 fe 66 ff ff       	call   801003c6 <cprintf>
80109cc8:	83 c4 10             	add    $0x10,%esp
      deallocuvm(pgdir, newsz, oldsz);
80109ccb:	83 ec 04             	sub    $0x4,%esp
80109cce:	ff 75 0c             	pushl  0xc(%ebp)
80109cd1:	ff 75 10             	pushl  0x10(%ebp)
80109cd4:	ff 75 08             	pushl  0x8(%ebp)
80109cd7:	e8 61 00 00 00       	call   80109d3d <deallocuvm>
80109cdc:	83 c4 10             	add    $0x10,%esp
      return 0;
80109cdf:	b8 00 00 00 00       	mov    $0x0,%eax
80109ce4:	eb 55                	jmp    80109d3b <allocuvm+0xc7>
    }
    memset(mem, 0, PGSIZE);
80109ce6:	83 ec 04             	sub    $0x4,%esp
80109ce9:	68 00 10 00 00       	push   $0x1000
80109cee:	6a 00                	push   $0x0
80109cf0:	ff 75 f0             	pushl  -0x10(%ebp)
80109cf3:	e8 a0 cf ff ff       	call   80106c98 <memset>
80109cf8:	83 c4 10             	add    $0x10,%esp
    mappages(pgdir, (char*)a, PGSIZE, v2p(mem), PTE_W|PTE_U);
80109cfb:	83 ec 0c             	sub    $0xc,%esp
80109cfe:	ff 75 f0             	pushl  -0x10(%ebp)
80109d01:	e8 08 f6 ff ff       	call   8010930e <v2p>
80109d06:	83 c4 10             	add    $0x10,%esp
80109d09:	89 c2                	mov    %eax,%edx
80109d0b:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109d0e:	83 ec 0c             	sub    $0xc,%esp
80109d11:	6a 06                	push   $0x6
80109d13:	52                   	push   %edx
80109d14:	68 00 10 00 00       	push   $0x1000
80109d19:	50                   	push   %eax
80109d1a:	ff 75 08             	pushl  0x8(%ebp)
80109d1d:	e8 1b fb ff ff       	call   8010983d <mappages>
80109d22:	83 c4 20             	add    $0x20,%esp
    return 0;
  if(newsz < oldsz)
    return oldsz;

  a = PGROUNDUP(oldsz);
  for(; a < newsz; a += PGSIZE){
80109d25:	81 45 f4 00 10 00 00 	addl   $0x1000,-0xc(%ebp)
80109d2c:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109d2f:	3b 45 10             	cmp    0x10(%ebp),%eax
80109d32:	0f 82 75 ff ff ff    	jb     80109cad <allocuvm+0x39>
      return 0;
    }
    memset(mem, 0, PGSIZE);
    mappages(pgdir, (char*)a, PGSIZE, v2p(mem), PTE_W|PTE_U);
  }
  return newsz;
80109d38:	8b 45 10             	mov    0x10(%ebp),%eax
}
80109d3b:	c9                   	leave  
80109d3c:	c3                   	ret    

80109d3d <deallocuvm>:
// newsz.  oldsz and newsz need not be page-aligned, nor does newsz
// need to be less than oldsz.  oldsz can be larger than the actual
// process size.  Returns the new process size.
int
deallocuvm(pde_t *pgdir, uint oldsz, uint newsz)
{
80109d3d:	55                   	push   %ebp
80109d3e:	89 e5                	mov    %esp,%ebp
80109d40:	83 ec 18             	sub    $0x18,%esp
  pte_t *pte;
  uint a, pa;

  if(newsz >= oldsz)
80109d43:	8b 45 10             	mov    0x10(%ebp),%eax
80109d46:	3b 45 0c             	cmp    0xc(%ebp),%eax
80109d49:	72 08                	jb     80109d53 <deallocuvm+0x16>
    return oldsz;
80109d4b:	8b 45 0c             	mov    0xc(%ebp),%eax
80109d4e:	e9 a5 00 00 00       	jmp    80109df8 <deallocuvm+0xbb>

  a = PGROUNDUP(newsz);
80109d53:	8b 45 10             	mov    0x10(%ebp),%eax
80109d56:	05 ff 0f 00 00       	add    $0xfff,%eax
80109d5b:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80109d60:	89 45 f4             	mov    %eax,-0xc(%ebp)
  for(; a  < oldsz; a += PGSIZE){
80109d63:	e9 81 00 00 00       	jmp    80109de9 <deallocuvm+0xac>
    pte = walkpgdir(pgdir, (char*)a, 0);
80109d68:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109d6b:	83 ec 04             	sub    $0x4,%esp
80109d6e:	6a 00                	push   $0x0
80109d70:	50                   	push   %eax
80109d71:	ff 75 08             	pushl  0x8(%ebp)
80109d74:	e8 24 fa ff ff       	call   8010979d <walkpgdir>
80109d79:	83 c4 10             	add    $0x10,%esp
80109d7c:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if(!pte)
80109d7f:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80109d83:	75 09                	jne    80109d8e <deallocuvm+0x51>
      a += (NPTENTRIES - 1) * PGSIZE;
80109d85:	81 45 f4 00 f0 3f 00 	addl   $0x3ff000,-0xc(%ebp)
80109d8c:	eb 54                	jmp    80109de2 <deallocuvm+0xa5>
    else if((*pte & PTE_P) != 0){
80109d8e:	8b 45 f0             	mov    -0x10(%ebp),%eax
80109d91:	8b 00                	mov    (%eax),%eax
80109d93:	83 e0 01             	and    $0x1,%eax
80109d96:	85 c0                	test   %eax,%eax
80109d98:	74 48                	je     80109de2 <deallocuvm+0xa5>
      pa = PTE_ADDR(*pte);
80109d9a:	8b 45 f0             	mov    -0x10(%ebp),%eax
80109d9d:	8b 00                	mov    (%eax),%eax
80109d9f:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80109da4:	89 45 ec             	mov    %eax,-0x14(%ebp)
      if(pa == 0)
80109da7:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
80109dab:	75 0d                	jne    80109dba <deallocuvm+0x7d>
        panic("kfree");
80109dad:	83 ec 0c             	sub    $0xc,%esp
80109db0:	68 49 af 10 80       	push   $0x8010af49
80109db5:	e8 ac 67 ff ff       	call   80100566 <panic>
      char *v = p2v(pa);
80109dba:	83 ec 0c             	sub    $0xc,%esp
80109dbd:	ff 75 ec             	pushl  -0x14(%ebp)
80109dc0:	e8 56 f5 ff ff       	call   8010931b <p2v>
80109dc5:	83 c4 10             	add    $0x10,%esp
80109dc8:	89 45 e8             	mov    %eax,-0x18(%ebp)
      kfree(v);
80109dcb:	83 ec 0c             	sub    $0xc,%esp
80109dce:	ff 75 e8             	pushl  -0x18(%ebp)
80109dd1:	e8 91 8e ff ff       	call   80102c67 <kfree>
80109dd6:	83 c4 10             	add    $0x10,%esp
      *pte = 0;
80109dd9:	8b 45 f0             	mov    -0x10(%ebp),%eax
80109ddc:	c7 00 00 00 00 00    	movl   $0x0,(%eax)

  if(newsz >= oldsz)
    return oldsz;

  a = PGROUNDUP(newsz);
  for(; a  < oldsz; a += PGSIZE){
80109de2:	81 45 f4 00 10 00 00 	addl   $0x1000,-0xc(%ebp)
80109de9:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109dec:	3b 45 0c             	cmp    0xc(%ebp),%eax
80109def:	0f 82 73 ff ff ff    	jb     80109d68 <deallocuvm+0x2b>
      char *v = p2v(pa);
      kfree(v);
      *pte = 0;
    }
  }
  return newsz;
80109df5:	8b 45 10             	mov    0x10(%ebp),%eax
}
80109df8:	c9                   	leave  
80109df9:	c3                   	ret    

80109dfa <freevm>:

// Free a page table and all the physical memory pages
// in the user part.
void
freevm(pde_t *pgdir)
{
80109dfa:	55                   	push   %ebp
80109dfb:	89 e5                	mov    %esp,%ebp
80109dfd:	83 ec 18             	sub    $0x18,%esp
  uint i;

  if(pgdir == 0)
80109e00:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80109e04:	75 0d                	jne    80109e13 <freevm+0x19>
    panic("freevm: no pgdir");
80109e06:	83 ec 0c             	sub    $0xc,%esp
80109e09:	68 4f af 10 80       	push   $0x8010af4f
80109e0e:	e8 53 67 ff ff       	call   80100566 <panic>
  deallocuvm(pgdir, KERNBASE, 0);
80109e13:	83 ec 04             	sub    $0x4,%esp
80109e16:	6a 00                	push   $0x0
80109e18:	68 00 00 00 80       	push   $0x80000000
80109e1d:	ff 75 08             	pushl  0x8(%ebp)
80109e20:	e8 18 ff ff ff       	call   80109d3d <deallocuvm>
80109e25:	83 c4 10             	add    $0x10,%esp
  for(i = 0; i < NPDENTRIES; i++){
80109e28:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80109e2f:	eb 4f                	jmp    80109e80 <freevm+0x86>
    if(pgdir[i] & PTE_P){
80109e31:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109e34:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
80109e3b:	8b 45 08             	mov    0x8(%ebp),%eax
80109e3e:	01 d0                	add    %edx,%eax
80109e40:	8b 00                	mov    (%eax),%eax
80109e42:	83 e0 01             	and    $0x1,%eax
80109e45:	85 c0                	test   %eax,%eax
80109e47:	74 33                	je     80109e7c <freevm+0x82>
      char * v = p2v(PTE_ADDR(pgdir[i]));
80109e49:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109e4c:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
80109e53:	8b 45 08             	mov    0x8(%ebp),%eax
80109e56:	01 d0                	add    %edx,%eax
80109e58:	8b 00                	mov    (%eax),%eax
80109e5a:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80109e5f:	83 ec 0c             	sub    $0xc,%esp
80109e62:	50                   	push   %eax
80109e63:	e8 b3 f4 ff ff       	call   8010931b <p2v>
80109e68:	83 c4 10             	add    $0x10,%esp
80109e6b:	89 45 f0             	mov    %eax,-0x10(%ebp)
      kfree(v);
80109e6e:	83 ec 0c             	sub    $0xc,%esp
80109e71:	ff 75 f0             	pushl  -0x10(%ebp)
80109e74:	e8 ee 8d ff ff       	call   80102c67 <kfree>
80109e79:	83 c4 10             	add    $0x10,%esp
  uint i;

  if(pgdir == 0)
    panic("freevm: no pgdir");
  deallocuvm(pgdir, KERNBASE, 0);
  for(i = 0; i < NPDENTRIES; i++){
80109e7c:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80109e80:	81 7d f4 ff 03 00 00 	cmpl   $0x3ff,-0xc(%ebp)
80109e87:	76 a8                	jbe    80109e31 <freevm+0x37>
    if(pgdir[i] & PTE_P){
      char * v = p2v(PTE_ADDR(pgdir[i]));
      kfree(v);
    }
  }
  kfree((char*)pgdir);
80109e89:	83 ec 0c             	sub    $0xc,%esp
80109e8c:	ff 75 08             	pushl  0x8(%ebp)
80109e8f:	e8 d3 8d ff ff       	call   80102c67 <kfree>
80109e94:	83 c4 10             	add    $0x10,%esp
}
80109e97:	90                   	nop
80109e98:	c9                   	leave  
80109e99:	c3                   	ret    

80109e9a <clearpteu>:

// Clear PTE_U on a page. Used to create an inaccessible
// page beneath the user stack.
void
clearpteu(pde_t *pgdir, char *uva)
{
80109e9a:	55                   	push   %ebp
80109e9b:	89 e5                	mov    %esp,%ebp
80109e9d:	83 ec 18             	sub    $0x18,%esp
  pte_t *pte;

  pte = walkpgdir(pgdir, uva, 0);
80109ea0:	83 ec 04             	sub    $0x4,%esp
80109ea3:	6a 00                	push   $0x0
80109ea5:	ff 75 0c             	pushl  0xc(%ebp)
80109ea8:	ff 75 08             	pushl  0x8(%ebp)
80109eab:	e8 ed f8 ff ff       	call   8010979d <walkpgdir>
80109eb0:	83 c4 10             	add    $0x10,%esp
80109eb3:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(pte == 0)
80109eb6:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80109eba:	75 0d                	jne    80109ec9 <clearpteu+0x2f>
    panic("clearpteu");
80109ebc:	83 ec 0c             	sub    $0xc,%esp
80109ebf:	68 60 af 10 80       	push   $0x8010af60
80109ec4:	e8 9d 66 ff ff       	call   80100566 <panic>
  *pte &= ~PTE_U;
80109ec9:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109ecc:	8b 00                	mov    (%eax),%eax
80109ece:	83 e0 fb             	and    $0xfffffffb,%eax
80109ed1:	89 c2                	mov    %eax,%edx
80109ed3:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109ed6:	89 10                	mov    %edx,(%eax)
}
80109ed8:	90                   	nop
80109ed9:	c9                   	leave  
80109eda:	c3                   	ret    

80109edb <copyuvm>:

// Given a parent process's page table, create a copy
// of it for a child.
pde_t*
copyuvm(pde_t *pgdir, uint sz)
{
80109edb:	55                   	push   %ebp
80109edc:	89 e5                	mov    %esp,%ebp
80109ede:	53                   	push   %ebx
80109edf:	83 ec 24             	sub    $0x24,%esp
  pde_t *d;
  pte_t *pte;
  uint pa, i, flags;
  char *mem;

  if((d = setupkvm()) == 0)
80109ee2:	e8 e6 f9 ff ff       	call   801098cd <setupkvm>
80109ee7:	89 45 f0             	mov    %eax,-0x10(%ebp)
80109eea:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80109eee:	75 0a                	jne    80109efa <copyuvm+0x1f>
    return 0;
80109ef0:	b8 00 00 00 00       	mov    $0x0,%eax
80109ef5:	e9 f8 00 00 00       	jmp    80109ff2 <copyuvm+0x117>
  for(i = 0; i < sz; i += PGSIZE){
80109efa:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80109f01:	e9 c4 00 00 00       	jmp    80109fca <copyuvm+0xef>
    if((pte = walkpgdir(pgdir, (void *) i, 0)) == 0)
80109f06:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109f09:	83 ec 04             	sub    $0x4,%esp
80109f0c:	6a 00                	push   $0x0
80109f0e:	50                   	push   %eax
80109f0f:	ff 75 08             	pushl  0x8(%ebp)
80109f12:	e8 86 f8 ff ff       	call   8010979d <walkpgdir>
80109f17:	83 c4 10             	add    $0x10,%esp
80109f1a:	89 45 ec             	mov    %eax,-0x14(%ebp)
80109f1d:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
80109f21:	75 0d                	jne    80109f30 <copyuvm+0x55>
      panic("copyuvm: pte should exist");
80109f23:	83 ec 0c             	sub    $0xc,%esp
80109f26:	68 6a af 10 80       	push   $0x8010af6a
80109f2b:	e8 36 66 ff ff       	call   80100566 <panic>
    if(!(*pte & PTE_P))
80109f30:	8b 45 ec             	mov    -0x14(%ebp),%eax
80109f33:	8b 00                	mov    (%eax),%eax
80109f35:	83 e0 01             	and    $0x1,%eax
80109f38:	85 c0                	test   %eax,%eax
80109f3a:	75 0d                	jne    80109f49 <copyuvm+0x6e>
      panic("copyuvm: page not present");
80109f3c:	83 ec 0c             	sub    $0xc,%esp
80109f3f:	68 84 af 10 80       	push   $0x8010af84
80109f44:	e8 1d 66 ff ff       	call   80100566 <panic>
    pa = PTE_ADDR(*pte);
80109f49:	8b 45 ec             	mov    -0x14(%ebp),%eax
80109f4c:	8b 00                	mov    (%eax),%eax
80109f4e:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80109f53:	89 45 e8             	mov    %eax,-0x18(%ebp)
    flags = PTE_FLAGS(*pte);
80109f56:	8b 45 ec             	mov    -0x14(%ebp),%eax
80109f59:	8b 00                	mov    (%eax),%eax
80109f5b:	25 ff 0f 00 00       	and    $0xfff,%eax
80109f60:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    if((mem = kalloc()) == 0)
80109f63:	e8 9c 8d ff ff       	call   80102d04 <kalloc>
80109f68:	89 45 e0             	mov    %eax,-0x20(%ebp)
80109f6b:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
80109f6f:	74 6a                	je     80109fdb <copyuvm+0x100>
      goto bad;
    memmove(mem, (char*)p2v(pa), PGSIZE);
80109f71:	83 ec 0c             	sub    $0xc,%esp
80109f74:	ff 75 e8             	pushl  -0x18(%ebp)
80109f77:	e8 9f f3 ff ff       	call   8010931b <p2v>
80109f7c:	83 c4 10             	add    $0x10,%esp
80109f7f:	83 ec 04             	sub    $0x4,%esp
80109f82:	68 00 10 00 00       	push   $0x1000
80109f87:	50                   	push   %eax
80109f88:	ff 75 e0             	pushl  -0x20(%ebp)
80109f8b:	e8 c7 cd ff ff       	call   80106d57 <memmove>
80109f90:	83 c4 10             	add    $0x10,%esp
    if(mappages(d, (void*)i, PGSIZE, v2p(mem), flags) < 0)
80109f93:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
80109f96:	83 ec 0c             	sub    $0xc,%esp
80109f99:	ff 75 e0             	pushl  -0x20(%ebp)
80109f9c:	e8 6d f3 ff ff       	call   8010930e <v2p>
80109fa1:	83 c4 10             	add    $0x10,%esp
80109fa4:	89 c2                	mov    %eax,%edx
80109fa6:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109fa9:	83 ec 0c             	sub    $0xc,%esp
80109fac:	53                   	push   %ebx
80109fad:	52                   	push   %edx
80109fae:	68 00 10 00 00       	push   $0x1000
80109fb3:	50                   	push   %eax
80109fb4:	ff 75 f0             	pushl  -0x10(%ebp)
80109fb7:	e8 81 f8 ff ff       	call   8010983d <mappages>
80109fbc:	83 c4 20             	add    $0x20,%esp
80109fbf:	85 c0                	test   %eax,%eax
80109fc1:	78 1b                	js     80109fde <copyuvm+0x103>
  uint pa, i, flags;
  char *mem;

  if((d = setupkvm()) == 0)
    return 0;
  for(i = 0; i < sz; i += PGSIZE){
80109fc3:	81 45 f4 00 10 00 00 	addl   $0x1000,-0xc(%ebp)
80109fca:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109fcd:	3b 45 0c             	cmp    0xc(%ebp),%eax
80109fd0:	0f 82 30 ff ff ff    	jb     80109f06 <copyuvm+0x2b>
      goto bad;
    memmove(mem, (char*)p2v(pa), PGSIZE);
    if(mappages(d, (void*)i, PGSIZE, v2p(mem), flags) < 0)
      goto bad;
  }
  return d;
80109fd6:	8b 45 f0             	mov    -0x10(%ebp),%eax
80109fd9:	eb 17                	jmp    80109ff2 <copyuvm+0x117>
    if(!(*pte & PTE_P))
      panic("copyuvm: page not present");
    pa = PTE_ADDR(*pte);
    flags = PTE_FLAGS(*pte);
    if((mem = kalloc()) == 0)
      goto bad;
80109fdb:	90                   	nop
80109fdc:	eb 01                	jmp    80109fdf <copyuvm+0x104>
    memmove(mem, (char*)p2v(pa), PGSIZE);
    if(mappages(d, (void*)i, PGSIZE, v2p(mem), flags) < 0)
      goto bad;
80109fde:	90                   	nop
  }
  return d;

bad:
  freevm(d);
80109fdf:	83 ec 0c             	sub    $0xc,%esp
80109fe2:	ff 75 f0             	pushl  -0x10(%ebp)
80109fe5:	e8 10 fe ff ff       	call   80109dfa <freevm>
80109fea:	83 c4 10             	add    $0x10,%esp
  return 0;
80109fed:	b8 00 00 00 00       	mov    $0x0,%eax
}
80109ff2:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80109ff5:	c9                   	leave  
80109ff6:	c3                   	ret    

80109ff7 <uva2ka>:

// Map user virtual address to kernel address.
char*
uva2ka(pde_t *pgdir, char *uva)
{
80109ff7:	55                   	push   %ebp
80109ff8:	89 e5                	mov    %esp,%ebp
80109ffa:	83 ec 18             	sub    $0x18,%esp
  pte_t *pte;

  pte = walkpgdir(pgdir, uva, 0);
80109ffd:	83 ec 04             	sub    $0x4,%esp
8010a000:	6a 00                	push   $0x0
8010a002:	ff 75 0c             	pushl  0xc(%ebp)
8010a005:	ff 75 08             	pushl  0x8(%ebp)
8010a008:	e8 90 f7 ff ff       	call   8010979d <walkpgdir>
8010a00d:	83 c4 10             	add    $0x10,%esp
8010a010:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if((*pte & PTE_P) == 0)
8010a013:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010a016:	8b 00                	mov    (%eax),%eax
8010a018:	83 e0 01             	and    $0x1,%eax
8010a01b:	85 c0                	test   %eax,%eax
8010a01d:	75 07                	jne    8010a026 <uva2ka+0x2f>
    return 0;
8010a01f:	b8 00 00 00 00       	mov    $0x0,%eax
8010a024:	eb 29                	jmp    8010a04f <uva2ka+0x58>
  if((*pte & PTE_U) == 0)
8010a026:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010a029:	8b 00                	mov    (%eax),%eax
8010a02b:	83 e0 04             	and    $0x4,%eax
8010a02e:	85 c0                	test   %eax,%eax
8010a030:	75 07                	jne    8010a039 <uva2ka+0x42>
    return 0;
8010a032:	b8 00 00 00 00       	mov    $0x0,%eax
8010a037:	eb 16                	jmp    8010a04f <uva2ka+0x58>
  return (char*)p2v(PTE_ADDR(*pte));
8010a039:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010a03c:	8b 00                	mov    (%eax),%eax
8010a03e:	25 00 f0 ff ff       	and    $0xfffff000,%eax
8010a043:	83 ec 0c             	sub    $0xc,%esp
8010a046:	50                   	push   %eax
8010a047:	e8 cf f2 ff ff       	call   8010931b <p2v>
8010a04c:	83 c4 10             	add    $0x10,%esp
}
8010a04f:	c9                   	leave  
8010a050:	c3                   	ret    

8010a051 <copyout>:
// Copy len bytes from p to user address va in page table pgdir.
// Most useful when pgdir is not the current page table.
// uva2ka ensures this only works for PTE_U pages.
int
copyout(pde_t *pgdir, uint va, void *p, uint len)
{
8010a051:	55                   	push   %ebp
8010a052:	89 e5                	mov    %esp,%ebp
8010a054:	83 ec 18             	sub    $0x18,%esp
  char *buf, *pa0;
  uint n, va0;

  buf = (char*)p;
8010a057:	8b 45 10             	mov    0x10(%ebp),%eax
8010a05a:	89 45 f4             	mov    %eax,-0xc(%ebp)
  while(len > 0){
8010a05d:	eb 7f                	jmp    8010a0de <copyout+0x8d>
    va0 = (uint)PGROUNDDOWN(va);
8010a05f:	8b 45 0c             	mov    0xc(%ebp),%eax
8010a062:	25 00 f0 ff ff       	and    $0xfffff000,%eax
8010a067:	89 45 ec             	mov    %eax,-0x14(%ebp)
    pa0 = uva2ka(pgdir, (char*)va0);
8010a06a:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010a06d:	83 ec 08             	sub    $0x8,%esp
8010a070:	50                   	push   %eax
8010a071:	ff 75 08             	pushl  0x8(%ebp)
8010a074:	e8 7e ff ff ff       	call   80109ff7 <uva2ka>
8010a079:	83 c4 10             	add    $0x10,%esp
8010a07c:	89 45 e8             	mov    %eax,-0x18(%ebp)
    if(pa0 == 0)
8010a07f:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
8010a083:	75 07                	jne    8010a08c <copyout+0x3b>
      return -1;
8010a085:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010a08a:	eb 61                	jmp    8010a0ed <copyout+0x9c>
    n = PGSIZE - (va - va0);
8010a08c:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010a08f:	2b 45 0c             	sub    0xc(%ebp),%eax
8010a092:	05 00 10 00 00       	add    $0x1000,%eax
8010a097:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if(n > len)
8010a09a:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010a09d:	3b 45 14             	cmp    0x14(%ebp),%eax
8010a0a0:	76 06                	jbe    8010a0a8 <copyout+0x57>
      n = len;
8010a0a2:	8b 45 14             	mov    0x14(%ebp),%eax
8010a0a5:	89 45 f0             	mov    %eax,-0x10(%ebp)
    memmove(pa0 + (va - va0), buf, n);
8010a0a8:	8b 45 0c             	mov    0xc(%ebp),%eax
8010a0ab:	2b 45 ec             	sub    -0x14(%ebp),%eax
8010a0ae:	89 c2                	mov    %eax,%edx
8010a0b0:	8b 45 e8             	mov    -0x18(%ebp),%eax
8010a0b3:	01 d0                	add    %edx,%eax
8010a0b5:	83 ec 04             	sub    $0x4,%esp
8010a0b8:	ff 75 f0             	pushl  -0x10(%ebp)
8010a0bb:	ff 75 f4             	pushl  -0xc(%ebp)
8010a0be:	50                   	push   %eax
8010a0bf:	e8 93 cc ff ff       	call   80106d57 <memmove>
8010a0c4:	83 c4 10             	add    $0x10,%esp
    len -= n;
8010a0c7:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010a0ca:	29 45 14             	sub    %eax,0x14(%ebp)
    buf += n;
8010a0cd:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010a0d0:	01 45 f4             	add    %eax,-0xc(%ebp)
    va = va0 + PGSIZE;
8010a0d3:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010a0d6:	05 00 10 00 00       	add    $0x1000,%eax
8010a0db:	89 45 0c             	mov    %eax,0xc(%ebp)
{
  char *buf, *pa0;
  uint n, va0;

  buf = (char*)p;
  while(len > 0){
8010a0de:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
8010a0e2:	0f 85 77 ff ff ff    	jne    8010a05f <copyout+0xe>
    memmove(pa0 + (va - va0), buf, n);
    len -= n;
    buf += n;
    va = va0 + PGSIZE;
  }
  return 0;
8010a0e8:	b8 00 00 00 00       	mov    $0x0,%eax
}
8010a0ed:	c9                   	leave  
8010a0ee:	c3                   	ret    
