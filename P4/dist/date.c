#include "types.h"
#include "user.h"
#include "date.h"

char* months[] = {"null", "January", "February", "March", "April", "May", "June", "July", "August", "September",
                  "October", "November", "December"};

int main(int argc, char *argv [])
{
  struct rtcdate r;
  if (date(&r)) {
    printf(2, "date_failed\n");
    exit();
  }
  //new code
  printf(1, "%s %d, %d\n", months[r.month], r.day, r.year);
  printf(1, "%d:%d  %d\n", r.hour, r.minute, r.second);
  exit();
}
