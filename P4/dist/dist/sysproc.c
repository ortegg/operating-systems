#include "types.h"
#include "x86.h"
#include "defs.h"
#include "date.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"
#include "uproc.h"

int
sys_fork(void)
{
  return fork();
}

int
sys_exit(void)
{
  exit();
  return 0;  // not reached
}

int
sys_wait(void)
{
  return wait();
}

int
sys_kill(void)
{
  int pid;

  if(argint(0, &pid) < 0)
    return -1;
  return kill(pid);
}

int
sys_getpid(void)
{
  return proc->pid;
}

int
sys_sbrk(void)
{
  int addr;
  int n;

  if(argint(0, &n) < 0)
    return -1;
  addr = proc->sz;
  if(growproc(n) < 0)
    return -1;
  return addr;
}

int
sys_sleep(void)
{
  int n;
  uint ticks0;
  
  if(argint(0, &n) < 0)
    return -1;
  ticks0 = ticks;
  while(ticks - ticks0 < n){
    if(proc->killed){
      return -1;
    }
    sleep(&ticks, (struct spinlock *)0);
  }
  return 0;
}

// return how many clock tick interrupts have occurred
// since start. 
int
sys_uptime(void)
{
  uint xticks;
  
  xticks = ticks;
  return xticks;
}

//Turn of the computer
int
sys_halt(void)
{
  cprintf("Shutting down ...\n");
// outw (0xB004, 0x0 | 0x2000);  // changed in newest version of QEMU
  outw( 0x604, 0x0 | 0x2000);
  return 0;
}

int
sys_date(void)
{
  struct rtcdate *d;

  if(argptr(0, (void*)&d, sizeof(*d)) < 0)
    return -1;
  //my P1 code
  cmostime(d);
  return 0;
}

//========== Project 2 System Calls Start ====================
int
sys_getuid(void)
{
  //return the uid in the proc struct
  return proc->uid;
}

int
sys_getgid(void)
{
  //return the gid in the proc struct
  return proc->gid;
}

int
sys_getppid(void)
{
  if(proc->parent) //if the proc has a parent return its pid
  {
     return proc->parent->pid;
  }
  else //else proc is a parent and return the pid
  {
    return proc->pid;
  }
}

int
sys_setuid(void)
{
  int uid;
  
  //retrieve an int off the stack
  if(argint(0, (void*)&uid) < 0)
  {
    return -1; //return error if the int is invalid
  }
  if(uid >= 0 && uid <= 32767) //check that it's in valid range
  { 
    proc->uid = uid; //set the uid in the proc struct
    return 0; //return normal
  }
  else
  { 
    return -1; //uid is invalid number
  }
}

int
sys_setgid(void)
{
  int gid;

  //retrieve and int off the stack
  if(argint(0, (void*)&gid) < 0)
  {
    return -1; //return error if the int is invalid
  }
  if(gid >= 0 && gid <= 32767) //check for validity
  {
    proc->gid = gid; //set the gid in the proc struct
    return 0;
  }
  else
  {
    return -1; //gid id invalid number
  }
}

int
sys_getprocs(void)
{
  int max;
  struct uproc* table;

  if (argint(0, (void*)&max) < 0)
    return -1;
  if (argptr(1, (void*)&table, sizeof(*table)) < 0)
    return -1;
  return getProcInfo(max, table); // help function to get proc info
}
//========== Project 2 System Calls End ====================
int 
sys_setpriority(void)
{
  int pid, priority;

  if(argint(0, (void*)&pid) < 0)
    return -1;
  if(argint(1, (void*)&priority) < 0)
    return -1;
  return setpriority(pid, priority);
}
