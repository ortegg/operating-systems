#include "types.h"
#include "stat.h"
#include "user.h"

int
main(int argc, char *argv[])
{
#ifdef CS333_P5
  if(argc < 3) 
    exit();

  int fd;
  struct stat st;
  char * path = argv[2];
  char * mode1 = argv[1];

  if((fd = open(path, 0)) < 0) {
    printf(2, "chmod: cannot open %s\n", path);
    exit();
  }
  if(fstat(fd, &st) < 0) {
    printf(2, "chmod: cannot stat %s\n", path);
    close(fd);
    exit();
  }

  int len = strlen(mode1);
  
  if(len != 4){
    printf(2, "chmod: invalid mode length\n");
    close(fd);
    exit();
  }
  if(mode1[0] == 48 || mode1[0] == 49){ 	// 0->48 and 1->49
    for(int i = 0; i < len; ++i){
      if(mode1[i] < 48 || mode1[i] > 55){	// 0->48 and 7->55
        printf(2, "chmod: invalid mode value\n");
        close(fd);
        exit();      
      }     
    }
  }
  else{
    printf(2, "chmod: invalid value\n");
    close(fd);
    exit();
  }
  int mode = atoo(mode1);
  close(fd);

  chmod(path, mode);
#endif
  exit();
}
