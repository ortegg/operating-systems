#include "types.h"
#include "stat.h"
#include "user.h"

int
main(int argc, char *argv[])
{
#ifdef CS333_P5
  if(argc < 3) 
    exit();

  int fd;
  struct stat st;
  char * path = argv[2];
  char * uid1 = argv[1];

  if((fd = open(path, 0)) < 0) {
    printf(2, "chown: cannot open %s\n", path);
    exit();
  }
  if(fstat(fd, &st) < 0) {
    printf(2, "chown: cannot stat %s\n", path);
    close(fd);
    exit();
  }
  int uid = atoi(uid1);
  if(uid < 0 || uid > 32767){
    printf(2, "chown: %d is invalid\n", uid);
    close(fd);
    exit();
  }
  close(fd);
  chown(path, uid);
#endif
  exit();
}
