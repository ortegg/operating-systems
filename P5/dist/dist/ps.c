#include "types.h"
#include "user.h"
#include "uproc.h"

int max[] = {1, 16, 64, 72};
int MAXCOUNT = 4;

// Helper function to print a page table entry
void
printTableEntry(struct uproc* t)
{
  int i, len;
  static int MAXNAME = 12;  // only print up to this many chars of name

  len = strlen(t->name);
  if (len > MAXNAME) {
    t->name[MAXNAME] = '\0';
    len = MAXNAME;
  }
  printf(1, "%d\t%s", t->pid, t->name);
  for (i=len; i<=MAXNAME; i++) printf(1, " ");
  printf(1, "%d", t->uid);
  if (t->uid <100) printf(1, "\t\t");
  else printf(1, "\t");
  printf(1, "%d\t%d\t%d.%d\t%d.%d\t%s\t%d\n",
	t->gid, t->ppid, (t->elapsed_ticks/100),
	 (t->elapsed_ticks%100), (t->CPU_total_ticks/100), (t->CPU_total_ticks%100),
	 t->state, t->size);
  return;
}


int
main(int argc, char* argv[]) {
  int MAX = 64;  // just happens to be the same as NPROC
  int i, rc;
  struct uproc *table;

  table = malloc(MAX * sizeof(struct uproc)); //malloc the table
  if (!table) { //error check malloc
    printf(2, "Error: malloc call failed. %s at line %d\n", __FILE__, __LINE__);
    exit();
  }
  rc = getprocs(MAX, table); //calling the systemcall
  if (rc < 0) { //error check system call
    printf(1, "Error: getprocs call failed. %s at line %d\n", __FILE__, __LINE__);
    exit();
  }
  printf(1, "\nPID\tName        UID        GID     PPID\tElapsed\tCPU\tState\tSize\n");
  for (i=0; i<rc; i++)
    printTableEntry(&table[i]); //print each page table entry
  free(table); //release the memory for the table

  exit();
}

