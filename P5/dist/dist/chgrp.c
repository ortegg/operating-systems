#include "types.h"
#include "stat.h"
#include "user.h"

int
main(int argc, char *argv[])
{
#ifdef CS333_P5
  if(argc < 3) 
    exit();

  int fd;
  struct stat st;
  char * path = argv[2];
  char * gid1 = argv[1];

  if((fd = open(path, 0)) < 0) {
    printf(2, "chgrp: cannot open %s\n", path);
    exit();
  }
  if(fstat(fd, &st) < 0) {
    printf(2, "chgrp: cannot stat %s\n", path);
    close(fd);
    exit();
  }
  int gid = atoi(gid1);
  if(gid < 0 || gid > 32767){
    printf(2, "chown: %d is invalid\n", gid);
    close(fd);
    exit();
  }
  close(fd);
  chgrp(path, gid);
#endif
  exit();
}
