#include "types.h"
#include "defs.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "x86.h"
#include "proc.h"
#include "spinlock.h"
#include "uproc.h"

#ifdef CS333_P3P4
struct StateLists {
  struct proc *ready;
  struct proc *free;
  struct proc *sleep;
  struct proc *zombie;
  struct proc *running;
  struct proc *embryo;
};
#endif

struct {
  struct spinlock lock;
  struct proc proc[NPROC];
  #ifdef CS333_P3P4
  struct StateLists pLists;
  #endif
} ptable;

static struct proc *initproc;

int nextpid = 1;
extern void forkret(void);
extern void trapret(void);

static void wakeup1(void *chan);
#ifdef CS333_P3P4
static void initFreeList(void);
static void addToFreeList(struct proc*);
static void addToReadyList(struct proc*);
static int addToSleepList(struct proc*);
static int removeFromSleepList(struct proc*);
static void addToZombieList(struct proc*);
static int removeFromZombieList(struct proc*);
static void addToEmbryoList(struct proc*);
static int removeFromEmbryoList(struct proc*);
static void addToRunningList(struct proc*);
static int removeFromRunningList(struct proc*);
#endif

void
pinit(void)
{
  initlock(&ptable.lock, "ptable");
}

// Look in the process table for an UNUSED proc.
// If found, change state to EMBRYO and initialize
// state required to run in the kernel.
// Otherwise return 0.
static struct proc*
allocproc(void)
{
  struct proc *p;
  char *sp;

  acquire(&ptable.lock);
  #ifdef CS333_P3P4
  //removeFromFreeList
  p = ptable.pLists.free;
  if (p)
  {
    ptable.pLists.free = p->next;
    p->next = 0;
    goto found;
  }
  #else
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
    if(p->state == UNUSED)
      goto found;
  #endif
  release(&ptable.lock);
  return 0;

found:
  p->state = EMBRYO;
  #ifdef CS333_P3P4
  addToEmbryoList(p);
  #endif
  p->pid = nextpid++;
  release(&ptable.lock);

  // Allocate kernel stack.
  if((p->kstack = kalloc()) == 0){
    #ifdef CS333_P3P4
    acquire(&ptable.lock);
    int rc = removeFromEmbryoList(p);
    if (rc == -1) panic("removeFromEmbryoList failed in allocproc!!!\n");
    p->state = UNUSED;
    addToFreeList(p);
    release(&ptable.lock);
    #endif
    return 0;
  }
  sp = p->kstack + KSTACKSIZE;
  
  // Leave room for trap frame.
  sp -= sizeof *p->tf;
  p->tf = (struct trapframe*)sp;
  
  // Set up new context to start executing at forkret,
  // which returns to trapret.
  sp -= 4;
  *(uint*)sp = (uint)trapret;

  sp -= sizeof *p->context;
  p->context = (struct context*)sp;
  memset(p->context, 0, sizeof *p->context);
  p->context->eip = (uint)forkret;

  p->start_ticks = ticks;
  p->cpu_ticks_in = 0;
  p->cpu_ticks_total = 0;

  return p;
}

// Set up first user process.
void
userinit(void)
{
  struct proc *p;
  extern char _binary_initcode_start[], _binary_initcode_size[];
  
  #ifdef CS333_P3P4
  // init other lists
  acquire(&ptable.lock);
  ptable.pLists.sleep = 0;
  ptable.pLists.zombie = 0;
  ptable.pLists.running = 0;
  ptable.pLists.embryo = 0;
  initFreeList();
  ptable.pLists.ready = 0;
  release(&ptable.lock);
  #endif
  p = allocproc();
  initproc = p;
  if((p->pgdir = setupkvm()) == 0)
    panic("userinit: out of memory?");
  inituvm(p->pgdir, _binary_initcode_start, (int)_binary_initcode_size);
  p->sz = PGSIZE;
  memset(p->tf, 0, sizeof(*p->tf));
  p->tf->cs = (SEG_UCODE << 3) | DPL_USER;
  p->tf->ds = (SEG_UDATA << 3) | DPL_USER;
  p->tf->es = p->tf->ds;
  p->tf->ss = p->tf->ds;
  p->tf->eflags = FL_IF;
  p->tf->esp = PGSIZE;
  p->tf->eip = 0;  // beginning of initcode.S

  safestrcpy(p->name, "initcode", sizeof(p->name));
  p->cwd = namei("/");

//========== Project 2 System Calls Start ====================
  p->uid = UID_DEFAULT; //set initial uid
  p->gid = GID_DEFAULT; //and gids
//========== Project 2 System Calls End ====================

  #ifdef CS333_P3P4
  // init ready list
  acquire(&ptable.lock);
  int rc = removeFromEmbryoList(p);
  if (rc == -1) panic("removeFromEmbryoList failed in userinit!!!\n");
  p->state = RUNNABLE;
  addToReadyList(p);
  release(&ptable.lock);
  #endif
}

// Grow current process's memory by n bytes.
// Return 0 on success, -1 on failure.
int
growproc(int n)
{
  uint sz;
  
  sz = proc->sz;
  if(n > 0){
    if((sz = allocuvm(proc->pgdir, sz, sz + n)) == 0)
      return -1;
  } else if(n < 0){
    if((sz = deallocuvm(proc->pgdir, sz, sz + n)) == 0)
      return -1;
  }
  proc->sz = sz;
  switchuvm(proc);
  return 0;
}

// Create a new process copying p as the parent.
// Sets up stack to return as if from system call.
// Caller must set state of returned proc to RUNNABLE.
int
fork(void)
{
  int i, pid;
  struct proc *np;

  // Allocate process.
  if((np = allocproc()) == 0)
    return -1;

  // Copy process state from p.
  if((np->pgdir = copyuvm(proc->pgdir, proc->sz)) == 0){
    kfree(np->kstack);
    np->kstack = 0;
    #ifdef CS333_P3P4
    acquire(&ptable.lock);
    int rc = removeFromEmbryoList(np);
    if (rc == -1) panic("removeFromEmbryoList failed in fork 1!!!");
    np->state = UNUSED;
    addToFreeList(np);
    release(&ptable.lock);
    #endif
    return -1;
  }
  np->sz = proc->sz;
  np->parent = proc;
  *np->tf = *proc->tf;

  // Clear %eax so that fork returns 0 in the child.
  np->tf->eax = 0;

  for(i = 0; i < NOFILE; i++)
    if(proc->ofile[i])
      np->ofile[i] = filedup(proc->ofile[i]);
  np->cwd = idup(proc->cwd);

  safestrcpy(np->name, proc->name, sizeof(proc->name));
 
  pid = np->pid;
//========== Project 2 System Calls Start ====================
  np->uid = proc->uid; //passing the uid and gid
  np->gid = proc->gid; //on to the next process
  np->cpu_ticks_total = 0;
  np->start_ticks = ticks;
//========== Project 2 System Calls End ====================


  // lock to force the compiler to emit the np->state write last.
  acquire(&ptable.lock);
  #ifdef CS333_P3P4
  int rc = removeFromEmbryoList(np);
  if (rc == -1) panic("removeFromEmbryoList failed in fork 2!!!\n");
  np->state = RUNNABLE;
  addToReadyList(np);
  #endif
  release(&ptable.lock);
  
  return pid;
}

// Exit the current process.  Does not return.
// An exited process remains in the zombie state
// until its parent calls wait() to find out it exited.
#ifndef CS333_P3P4
void
exit(void)
{
  struct proc *p;
  int fd;

  if(proc == initproc)
    panic("init exiting");

  // Close all open files.
  for(fd = 0; fd < NOFILE; fd++){
    if(proc->ofile[fd]){
      fileclose(proc->ofile[fd]);
      proc->ofile[fd] = 0;
    }
  }

  begin_op();
  iput(proc->cwd);
  end_op();
  proc->cwd = 0;

  acquire(&ptable.lock);

  // Parent might be sleeping in wait().
  wakeup1(proc->parent);

  // Pass abandoned children to init.
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->parent == proc){
      p->parent = initproc;
      if(p->state == ZOMBIE)
        wakeup1(initproc);
    }
  }

  // Jump into the scheduler, never to return.
  proc->state = ZOMBIE;
  sched();
  panic("zombie exit");
}
#else
void
exit(void)
{
  struct proc *p;
  int fd;

  if(proc == initproc)
    panic("init exiting");

  // Close all open files.
  for(fd = 0; fd < NOFILE; fd++){
    if(proc->ofile[fd]){
      fileclose(proc->ofile[fd]);
      proc->ofile[fd] = 0;
    }
  }

  begin_op();
  iput(proc->cwd);
  end_op();
  proc->cwd = 0;

  acquire(&ptable.lock);

  // Parent might be sleeping in wait().
  wakeup1(proc->parent);

  // Pass abandoned children to init.
  p = ptable.pLists.ready;
  while (p) {
    if(p->parent == proc) {
      p->parent = initproc;
    }
    p = p->next;
  }
  p = ptable.pLists.sleep;
  while (p) {
    if(p->parent == proc) {
      p->parent = initproc;
    }
    p = p->next;
  }
  p = ptable.pLists.running;
  while (p) {
    if(p->parent == proc) {
      p->parent = initproc;
    }
    p = p->next;
  }
  p = ptable.pLists.zombie;
  while (p) {
    if(p->parent == proc) {
      p->parent = initproc;
      wakeup1(initproc);
    }
    p = p->next;
  }

  // Jump into the scheduler, never to return.
  int rc = removeFromRunningList(proc);
  if (rc == -1) panic("removeFromRunningList failed in exit!!!\n");
  proc->state = ZOMBIE;
  //
  if(!holding(&ptable.lock)) panic("I need the lock to add to the zombie list\n");
  addToZombieList(proc);
  //
  sched();
  panic("zombie exit");
}
#endif

// Wait for a child process to exit and return its pid.
// Return -1 if this process has no children.
#ifndef CS333_P3P4
int
wait(void)
{
  struct proc *p;
  int havekids, pid;

  acquire(&ptable.lock);
  for(;;){
    // Scan through table looking for zombie children.
    havekids = 0;
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
      if(p->parent != proc)
        continue;
      havekids = 1;
      if(p->state == ZOMBIE){
        // Found one.
        pid = p->pid;
        kfree(p->kstack);
        p->kstack = 0;
        freevm(p->pgdir);
        p->state = UNUSED;
        p->pid = 0;
        p->parent = 0;
        p->name[0] = 0;
        p->killed = 0;
        release(&ptable.lock);
        return pid;
      }
    }

    // No point waiting if we don't have any children.
    if(!havekids || proc->killed){
      release(&ptable.lock);
      return -1;
    }

    // Wait for children to exit.  (See wakeup1 call in proc_exit.)
    sleep(proc, &ptable.lock);  //DOC: wait-sleep
  }
}
#else
int
wait(void)
{
  struct proc *p;
  int havekids, pid;

  acquire(&ptable.lock);
  for(;;){
    // Scan through table looking for zombie children.
    havekids = 0;
    p = ptable.pLists.zombie;
    while (p) {
      if(p->parent == proc) {
        havekids = 1;
        int rc = removeFromZombieList(p);
        if (rc == -1) panic("removeFromZombieList failed in wait!!!\n");
        pid = p->pid;
        kfree(p->kstack);
        p->kstack = 0;
        freevm(p->pgdir);
        p->state = UNUSED;
        addToFreeList(p);
        p->pid = 0;
        p->parent = 0;
        p->name[0] = 0;
        p->killed = 0;
        release(&ptable.lock);
        return pid;
      }
      p = p->next;
    }
    if (!havekids) {
      p = ptable.pLists.ready;
      while (p) {
        if(p->parent == proc) {
          havekids = 1;
        }
        p = p->next;
      }
    }
    if (!havekids) {
      p = ptable.pLists.sleep;
      while (p) {
        if(p->parent == proc) {
          havekids = 1;
        }
        p = p->next;
      }
    }
    if (!havekids) {
      p = ptable.pLists.running;
      while (p) {
        if(p->parent == proc) {
          havekids = 1;
        }
        p = p->next;
      }
    }
    // No point waiting if we don't have any children.
    if(!havekids || proc->killed){
      release(&ptable.lock);
      return -1;
    }
    // Wait for children to exit.  (See wakeup1 call in proc_exit.)
    sleep(proc, &ptable.lock);  //DOC: wait-sleep
  }
}
#endif

// Per-CPU process scheduler.
// Each CPU calls scheduler() after setting itself up.
// Scheduler never returns.  It loops, doing:
//  - choose a process to run
//  - swtch to start running that process
//  - eventually that process transfers control
//      via swtch back to the scheduler.
#ifndef CS333_P3P4
// original xv6 scheduler. Use if CS333_P3 NOT defined.
void
scheduler(void)
{
  struct proc *p;
  int idle;  // for checking if processor is idle

  for(;;){
    // Enable interrupts on this processor.
    sti();

    idle = 1;  // assume idle unless we schedule a process
    // Loop over process table looking for process to run.
    acquire(&ptable.lock);
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
      if(p->state != RUNNABLE)
        continue;

      // Switch to chosen process.  It is the process's job
      // to release ptable.lock and then reacquire it
      // before jumping back to us.
      idle = 0;  // not idle this timeslice
      proc = p;
      switchuvm(p);
      // set cpu ticks in when a new process is scheduled
      p->cpu_ticks_in = ticks;
      // anywhere after a process is chosen
      p->state = RUNNING;
      swtch(&cpu->scheduler, proc->context);
      switchkvm();

      // Process is done running for now.
      // It should have changed its p->state before coming back.
      proc = 0;
    }
    release(&ptable.lock);
    // if idle, wait for next interrupt
    if (idle) {
      sti();
      hlt();
    }
  }
}

#else
// CS333_P3 MLFQ scheduler implementation goes here
void
scheduler(void)
{
  struct proc *p;
  int idle;  // for checking if processor is idle

  for(;;){
    // Enable interrupts on this processor.
    sti();

    idle = 1;  // assume idle unless we schedule a process

    acquire(&ptable.lock);
    p = ptable.pLists.ready;
    if (p) 
    {
      ptable.pLists.ready = p->next;
      if(p->state != RUNNABLE) 
      {
        panic("OH NO!!! There is a not RUNNABLE job on the ready list!!!\n");
      }

      // switch to chosen process.  it is the process's job
      // to release ptable.lock and then reacquire it
      // before jumping back to us.
      idle = 0;  // not idle this timeslice
      proc = p;
      switchuvm(p);
      // set cpu ticks in when a new process is scheduled
      p->cpu_ticks_in = ticks;
      // anywhere after a process is chosen
      p->state = RUNNING;
      addToRunningList(p);
      swtch(&cpu->scheduler, proc->context);
      switchkvm();

      // process is done running for now.
      // it should have changed its p->state before coming back.
      proc = 0;
    }
    release(&ptable.lock);
    // if idle, wait for next interrupt
    if (idle) {
      sti();
      hlt();
    }
  }
}
#endif

#ifndef CS333_P3P4
// Enter scheduler.  Must hold only ptable.lock
// and have changed proc->state.
void
sched(void)
{
  int intena;

  if(!holding(&ptable.lock))
    panic("sched ptable.lock");
  if(cpu->ncli != 1)
    panic("sched locks");
  if(proc->state == RUNNING)
    panic("sched running");
  if(readeflags()&FL_IF)
    panic("sched interruptible");
  intena = cpu->intena;
  // update the total time a process is in a cpu
  proc->cpu_ticks_total += (ticks - proc->cpu_ticks_in);
  // everytime the proc leaves
  swtch(&proc->context, cpu->scheduler);
  cpu->intena = intena;
}

#else
void
sched(void)
{
  int intena;

  if(!holding(&ptable.lock))
    panic("sched ptable.lock");
  if(cpu->ncli != 1)
    panic("sched locks");
  if(proc->state == RUNNING)
    panic("sched running");
  if(readeflags()&FL_IF)
    panic("sched interruptible");
  intena = cpu->intena;
  
  // update the total time a process is in a cpu
  proc->cpu_ticks_total += (ticks - proc->cpu_ticks_in);
  //removeFromRunningList(proc);
  //if (rc == -1) panic("removeFromRunningList failed in sched!!!\n");
  // everytime the proc leaves
  swtch(&proc->context, cpu->scheduler);
  cpu->intena = intena;
}
#endif

// Give up the CPU for one scheduling round.
void
yield(void)
{
  acquire(&ptable.lock);  //DOC: yieldlock
  #ifdef CS333_P3P4
  int rc = removeFromRunningList(proc);
  if (rc == -1) panic("removeFromRunningList failed in yield!\n");
  proc->state = RUNNABLE;
  addToReadyList(proc);
  #endif
  sched();
  release(&ptable.lock);
}

// A fork child's very first scheduling by scheduler()
// will swtch here.  "Return" to user space.
void
forkret(void)
{
  static int first = 1;
  // Still holding ptable.lock from scheduler.
  release(&ptable.lock);

  if (first) {
    // Some initialization functions must be run in the context
    // of a regular process (e.g., they call sleep), and thus cannot 
    // be run from main().
    first = 0;
    iinit(ROOTDEV);
    initlog(ROOTDEV);
  }
  
  // Return to "caller", actually trapret (see allocproc).
}

// Atomically release lock and sleep on chan.
// Reacquires lock when awakened.
// 2016/12/28: ticklock removed from xv6. sleep() changed to
// accept a NULL lock to accommodate.
void
sleep(void *chan, struct spinlock *lk)
{
  if(proc == 0)
    panic("sleep");

#ifdef NOTINUSE
  if(lk == 0)
    panic("sleep without lk");
#endif

  // Must acquire ptable.lock in order to
  // change p->state and then call sched.
  // Once we hold ptable.lock, we can be
  // guaranteed that we won't miss any wakeup
  // (wakeup runs with ptable.lock locked),
  // so it's okay to release lk.
  if(lk != &ptable.lock){  //DOC: sleeplock0
    acquire(&ptable.lock);  //DOC: sleeplock1
#ifdef NOTINUSE
    release(lk);
#else
    if (lk) release(lk);
#endif
  }

  // Go to sleep.
  proc->chan = chan;
  #ifdef CS333_P3P4
  int rc = removeFromRunningList(proc);
  if (rc == -1) panic("removeFromRunningList failed in yield!\n");
  proc->state = SLEEPING;
  rc = addToSleepList(proc);
  if (rc == -1) panic("Add to sleep list failed in sleep()\n");
  #endif
  sched();

  // Tidy up.
  proc->chan = 0;

  // Reacquire original lock.
  if(lk != &ptable.lock){  //DOC: sleeplock2
    release(&ptable.lock);
#ifdef NOTINUSE
    acquire(lk);
#else
    if (lk) acquire(lk);
#endif
  }
}

// Wake up all processes sleeping on chan.
// The ptable lock must be held.
#ifndef CS333_P3P4
static void
wakeup1(void *chan)
{
  struct proc *p;

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
  {
    if(p->state == SLEEPING && p->chan == chan)
    {
      p->state = RUNNABLE;
    }
  }
}
#else
static void
wakeup1(void *chan)
{
  if(!holding(&ptable.lock)) panic("Not holding lock in wakeup1!!!!\n");
  if(!ptable.pLists.sleep) 
  {
    return;
  }

  struct proc *p = ptable.pLists.sleep;
  while (p)
  {
    if(p->chan == chan)
    {
      int rc = removeFromSleepList(p);
      if (rc == -1) panic("removeFromSleepList failed in wakeup1!\n");
      p->state = RUNNABLE;
      addToReadyList(p);
    }
    p = p->next;
  }
}
#endif


// Wake up all processes sleeping on chan.
void
wakeup(void *chan)
{
  acquire(&ptable.lock);
  wakeup1(chan);
  release(&ptable.lock);
}

// Kill the process with the given pid.
// Process won't exit until it returns
// to user space (see trap in trap.c).
#ifndef CS333_P3P4
int
kill(int pid)
{
  struct proc *p;

  acquire(&ptable.lock);
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->pid == pid){
      p->killed = 1;
      // Wake process from sleep if necessary.
      if(p->state == SLEEPING)
      {
        p->state = RUNNABLE;
      }
      release(&ptable.lock);
      return 0;
    }
  }
  release(&ptable.lock);
  return -1;
}
#else
int
kill(int pid)
{
  struct proc *p;

  acquire(&ptable.lock);
  p = ptable.pLists.sleep;
  while (p) { 
    if (p->pid == pid){
      p->killed = 1;
      int rc = removeFromSleepList(p);
      if (rc == -1) panic("removeFromSleepList failed in kill!!!\n");
      p->state = RUNNABLE;
      addToReadyList(p);
      release(&ptable.lock);
      return 0;
    }
    p = p->next;
  }
  p = ptable.pLists.zombie;
  while (p) { 
    if (p->pid == pid){
      p->killed = 1;
      release(&ptable.lock);
      return 0;
    }
    p = p->next;
  }
  p = ptable.pLists.ready;
  while (p) { 
    if (p->pid == pid){
      p->killed = 1;
      release(&ptable.lock);
      return 0;
    }
    p = p->next;
  }
  p = ptable.pLists.running;
  while (p) { 
    if (p->pid == pid){
      p->killed = 1;
      release(&ptable.lock);
      return 0;
    }
    p = p->next;
  }
  release(&ptable.lock);
  return -1;
}
#endif

static char *states[] = {
  [UNUSED]    "unused",
  [EMBRYO]    "embryo",
  [SLEEPING]  "sleep ",
  [RUNNABLE]  "runble",
  [RUNNING]   "run   ",
  [ZOMBIE]    "zombie"
};

// Print a process listing to console.  For debugging.
// Runs when user types ^P on console.
// No lock to avoid wedging a stuck machine further.
void
procdump(void)
{
  int i, ppid;
  struct proc *p;
  char *state;
  uint pc[10], ticks0;

  ticks0 = ticks;   // close enough

  cprintf("\nPID\tName        UID        GID     PPID\tElapsed\tCPU\tState\tSize\tPCs\n");

  
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){

    if(p->state == UNUSED)
      continue;
    if(p->state >= 0 && p->state < NELEM(states) && states[p->state])
      state = states[p->state];
    else
      state = "???";

    if (p->parent) ppid = p->parent->pid;
    else ppid = p->pid;
    int len = strlen(p->name);
    cprintf("%d\t%s", p->pid, p->name);
    int MAXNAME = 12;
    for (i=len; i<=MAXNAME; i++) cprintf(" ");
    cprintf("%d", p->uid);
    if (p->uid <100) cprintf("\t\t");
    else cprintf("\t");

    cprintf("%d\t%d\t%d.%d\t%d.%d\t%s\t%d\t",  p->gid, ppid,
		    (ticks0 - p->start_ticks)/100, (ticks0 - p->start_ticks) % 100,
		    p->cpu_ticks_total/100, p->cpu_ticks_total%100, state, p->sz);

    if(p->state == SLEEPING){
      getcallerpcs((uint*)p->context->ebp+2, pc);
      for(i=0; i<10 && pc[i] != 0; i++)
        cprintf(" %p", pc[i]);
    }
    cprintf("\n");
  }
}


int
getProcInfo(int count, struct uproc* table)
{
    uint num = 0, ticks0;
    struct proc *p;

    ticks0 = ticks;   

    acquire(&ptable.lock); //be sure to lock ptable to avoid bad data
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
      if(p->state == UNUSED) //if the proc is unused skip it
        continue;
      if (num == count || num == NPROC) //reach the max of the table
      break;
      //fill the table with the proc info
      table[num].pid = p->pid;
      table[num].uid = p->uid;
      table[num].gid = p->gid;
      table[num].CPU_total_ticks = p->cpu_ticks_total;
      table[num].elapsed_ticks = ticks0 - p->start_ticks;
      if (p->parent)
        table[num].ppid = p->parent->pid;
      else
        table[num].ppid = p->pid;  
      safestrcpy(table[num].state, states[p->state], sizeof(table[num].state));
      table[num].size = p->sz;
      safestrcpy(table[num].name, p->name, sizeof(table[num].name));
      num++; //next proc
    }
  release(&ptable.lock); //be sure to share the lock. Sharing is caring
  return num; //return the number of procs
}

#ifdef CS333_P3P4
// ptable lock is required to call this function!!!
static void 
addToFreeList(struct proc *p)
{
  if (!holding(&ptable.lock)) panic("Where is my lock?! Lock needed in addToFreeList\n");
  if (ptable.pLists.free != 0)
  {
    p->next = ptable.pLists.free;
    ptable.pLists.free = p;
  }
  else
  {
    ptable.pLists.free = p;
    p->next = 0;
  }
  return;
}

// don't call initFreeList without the ptable lock!!
static void 
initFreeList(void)
{
  int i;
  if(!holding(&ptable.lock)) panic("Holding ptable.lock in initFreeList where I shouldn't be\n");
  for (i = NPROC-1; i>=0; --i)
    addToFreeList(&ptable.proc[i]);
  return;
}

int 
recursiveCountFreeList(struct proc *p)
{
  if (!holding(&ptable.lock)) panic("Where is my lock?! Lock needed in recursiveCountFreeList\n");
  if (p == 0) return 0;
  return recursiveCountFreeList(p-> next) + 1;
}

void 
countFreeList(void)
{
  if(holding(&ptable.lock)) panic("Holding ptable.lock in countFreeList where I shouldn't be\n");
  int size = 0;
  acquire(&ptable.lock);
  struct proc *p = ptable.pLists.free;
  size = recursiveCountFreeList(p);
  release(&ptable.lock);
  cprintf("Free List Size: %d processes\n", size);
  return;
}

// ptable lock is required to call this function!!!
static void 
addToReadyList(struct proc *p)
{
  struct proc* temp;
  if (!holding(&ptable.lock)) panic("Where is my lock?! lock is needed in addToReadyList\n");
  
  temp = ptable.pLists.ready;
  if (temp == 0) ptable.pLists.ready = p;
  else 
  {
    while (temp->next != 0) temp = temp->next;
    temp->next = p;
  }
  p->next = 0;
  return;
}

void 
countReadyList(void)
{
  if(holding(&ptable.lock)) panic("Holding ptable.lock in countReadyList where I shouldn't be\n");
  acquire(&ptable.lock);
  struct proc *p = ptable.pLists.ready;
  if (p == 0) {
    cprintf("The Ready List is empty\n");
    release(&ptable.lock);
    return;
  }
  cprintf("Ready List Processes:\n");
  while (p->next) 
  {
    cprintf("%d -> ", p->pid);
    p = p->next;
  }
  cprintf("%d\n", p->pid);
  release(&ptable.lock);
  return;
}

void 
countSleepList(void)
{
  if(holding(&ptable.lock)) panic("Holding ptable.lock in countSleepList where I shouldn't be\n");
  acquire(&ptable.lock);
  if (ptable.pLists.sleep == 0) {
    cprintf("The Sleep List is empty\n");
    release(&ptable.lock);
    return;
  }
  struct proc *p = ptable.pLists.sleep;
  cprintf("Sleep List Processes: \n");
  while (p->next) 
  {
    cprintf("%d -> ", p->pid);
    p = p->next;
  }
  cprintf("%d\n", p->pid);
  release(&ptable.lock);
  return;
}

static int 
addToSleepList(struct proc *p)
{
  if (!holding(&ptable.lock)) panic("Where is my lock?! lock is needed in addToSleepList\n");
  
  if (ptable.pLists.sleep == 0)
  {
    ptable.pLists.sleep = p;
    p->next = 0;
    return 0;
  }
  if (ptable.pLists.sleep != 0)
  {
    p->next = ptable.pLists.sleep;
    ptable.pLists.sleep = p;
    return 0;
  }
  return -1;
}

static int 
removeFromSleepList(struct proc *p)
{
  if (!holding(&ptable.lock)) panic("Where is my lock?! lock is needed in removeFromSleepList\n");
  if (ptable.pLists.sleep->pid == p->pid)
  {
    ptable.pLists.sleep = ptable.pLists.sleep->next;
    p->next = 0;
    return 0;
  }
  struct proc *current = ptable.pLists.sleep->next;
  struct proc *prev = ptable.pLists.sleep;
  while (current)
  {
    if(current->pid == p->pid)
    {
      prev->next = current->next;
      p->next = 0;
      return 0;
    }
    prev = current;
    current = current->next;
  }
  return -1;
}

static void 
addToZombieList(struct proc *p)
{
  if (!holding(&ptable.lock)) panic("Where is my lock?! lock is needed in addToZombieList\n");

  if (ptable.pLists.zombie != 0)
  {  
    p->next = ptable.pLists.zombie;
    ptable.pLists.zombie = p;
    return;
  }
  if (ptable.pLists.zombie == 0)
  {
    ptable.pLists.zombie = p;
    p->next = 0;
    return;
  } 
  return;
}

void 
countZombieList(void)
{
  acquire(&ptable.lock);
  struct proc *temp = ptable.pLists.zombie;
  int ppid;
  if (temp != 0)
  {
    cprintf("Zombie List Processes:\n");
    while (temp->next) 
    {
      ppid = temp->pid;
      if (temp->parent) ppid = temp->parent->pid;
      cprintf("(%d, PPID%d) -> ", temp->pid, ppid);
      temp = temp->next;
    }
    ppid = temp->pid;
    if (temp->parent) ppid = temp->parent->pid;
    cprintf("(%d, PPID%d)\n", temp->pid, ppid);
  }
  else cprintf("The Zombie List is empty\n");
  release(&ptable.lock);
  return;
}

static int
removeFromZombieList(struct proc *p)
{
  if (!holding(&ptable.lock)) panic("Where is my lock?! lock is needed in removeFrompZombieList\n");
  if (ptable.pLists.zombie == 0) panic("Zombie List is empty!\n");
  if (ptable.pLists.zombie->pid == p->pid)
  {
    ptable.pLists.zombie = ptable.pLists.zombie->next;
    p->next = 0;
    return 0;
  }
  struct proc *current = ptable.pLists.zombie->next;
  struct proc *prev = ptable.pLists.zombie;
  while(current)
  {
    if (current->pid == p->pid)
    {
      prev->next = current->next;
      current -> next = 0;
      return 0;
    }
    prev = current;
    current = current -> next;
  }
  return -1;
}

static void 
addToEmbryoList(struct proc *p)
{
  if (!holding(&ptable.lock)) panic("Where is my lock?! lock is needed in assToEmbryoList\n");

  if (ptable.pLists.embryo != 0)
  {  
    p->next = ptable.pLists.embryo;
    ptable.pLists.embryo = p;
    return;
  }
  if (ptable.pLists.embryo == 0)
  {
    ptable.pLists.embryo = p;
    p->next = 0;
    return;
  } 
}

static int 
removeFromEmbryoList(struct proc *p)
{
  if (!holding(&ptable.lock)) panic("Where is my lock?! lock is needed in removeFrompEmbryoList\n");
  if (ptable.pLists.embryo->pid == p->pid)
  {
    ptable.pLists.embryo = ptable.pLists.embryo->next;
    p->next = 0;
    return 0;
  }
  struct proc *current = ptable.pLists.embryo->next;
  struct proc *prev = ptable.pLists.embryo;
  while(current)
  {
    if (current->pid == p->pid)
    {
      prev->next = current->next;
      current -> next = 0;
      return 0;
    }
    prev = current;
    current = current -> next;
  }
  return -1;
}

static void 
addToRunningList(struct proc *p)
{
  if (!holding(&ptable.lock)) panic("Where is my lock?! lock is needed in addToRunningList\n");

  if (ptable.pLists.running != 0)
  {  
    p->next = ptable.pLists.running;
    ptable.pLists.running = p;
    return;
  }
  if (ptable.pLists.running == 0)
  {
    ptable.pLists.running = p;
    p->next = 0;
    return;
  } 
}

static int 
removeFromRunningList(struct proc *p)
{
  if (ptable.pLists.running == 0) return 0;
  if (!holding(&ptable.lock)) panic("Where is my lock?! lock is needed in removeFrompRunningList\n");
  if (ptable.pLists.running->pid == p->pid)
  {
    ptable.pLists.running = ptable.pLists.running->next;
    p->next = 0;
    return 0;
  }
  struct proc *current = ptable.pLists.running->next;
  struct proc *prev = ptable.pLists.running;
  while(current)
  {
    if (current->pid == p->pid)
    {
      prev->next = current->next;
      current->next = 0;
      return 0;
    }
    prev = current;
    current = current -> next;
  }
  return -1;
}

#endif
