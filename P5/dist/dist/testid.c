#include "types.h"
#include "user.h"

int testvalid(void)
{
  // set and show new ones
  printf(1, "\nSetting the uid and gid to 3\n");
  if (setuid(3) < 0)
  {
    printf(2, "SETTING UID TO 3 CAUSED UNEXPECTED ERROR!!!\n");
    return -1;
  }
  if (setgid(3) < 0)
  {
    printf(2, "SETTING GID TO 3 CAUSED UNEXPECTED ERROR!!!\n");
    return -1;
  }
  printf(1, "The new uid is: %d\n", getuid());
  printf(1, "The new gid is: %d\n", getgid());
  return 0;
}

int testinvalid(void)
{
  // set and show illegal values
  printf(1, "\nSetting the uid and gid to 32768\n");
  if (setuid(32768) > 0)
  {
    printf(2, "SETTUNG UID TO 32768 DID NOT CAUSE ERROR!!!\n");
    return -1;
  }
  if (setuid(32768) > 0)
  {
    printf(2, "SETTING GID TO 32768 DID NOT CAUSE ERROR\n");
    return -1;
  }
  printf(1, "The uid is still: %d\n", getuid());
  printf(1, "The gid is still: %d\n", getgid());
  return 0;
}

int testparent(void)
{
  printf(1, "\nGetting the ppid\nThe ppid is: %d\n", getppid());
  return 0;
}

int test(void)
{
  // get current ids
  printf(1, "The current uid is: %d\n", getuid()); 
  printf(1, "The current gid is: %d\n", getgid());

  if (testvalid() < 0)
  {
    printf(2, "TEST FAILED.");
    return -1;
  }
  if (testinvalid() < 0)
  {
    printf(2, "TEST FAILED.");
    return -1;
  }
  testparent();
  return 0;
}

int main()
{
  test();
  exit();
}
