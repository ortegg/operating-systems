#include "uproc.h"
#include "types.h"
#include "user.h"

int 
main(int argc, char * argv[])
{
  static int MAX = 16;		//default value of MAX
  int size;
  struct uproc * table;
  
  if(argc > 1 && argv[1] > 0)
    MAX = atoi(argv[1]);	//user defined value for the MAX parameter
  printf(1, "MAX = %d\n", MAX);
  table = malloc(MAX * sizeof(struct uproc));
  if(!table)
  {
    printf(2, "Error: malloc call failed. %s at line %d\n", "ps.c", 12);
    exit(); 
  } 
  size = getprocs(MAX, table);
  if(size == 0){
    printf(2, "Get table for uproc failed.\n");
    exit();
  }
  printf(1, "PID\tName\tUID\tGID\tPPID\tElapsed\tCPU\tState\tSize\n");
  int x;
  for(x = 0; x < size; ++x){
    printf(1,"%d\t%s\t%d\t%d\t%d\t%d.%d\t%d.%d\t%s\t%d\n", table[x].pid, table[x].name, table[x].uid, table[x].gid, table[x].ppid,
      table[x].elapsed_ticks, table[x].elapsed_ticks1, table[x].CPU_total_ticks, table[x].CPU_total_ticks1, table[x].state, table[x].size);
  }
  exit();
}
