
_stressfs:     file format elf32-i386


Disassembly of section .text:

00000000 <main>:
#include "fs.h"
#include "fcntl.h"

int
main(int argc, char *argv[])
{
   0:	8d 4c 24 04          	lea    0x4(%esp),%ecx
   4:	83 e4 f0             	and    $0xfffffff0,%esp
   7:	ff 71 fc             	pushl  -0x4(%ecx)
   a:	55                   	push   %ebp
   b:	89 e5                	mov    %esp,%ebp
   d:	51                   	push   %ecx
   e:	81 ec 24 02 00 00    	sub    $0x224,%esp
  int fd, i;
  char path[] = "stressfs0";
  14:	c7 45 e6 73 74 72 65 	movl   $0x65727473,-0x1a(%ebp)
  1b:	c7 45 ea 73 73 66 73 	movl   $0x73667373,-0x16(%ebp)
  22:	66 c7 45 ee 30 00    	movw   $0x30,-0x12(%ebp)
  char data[512];

  printf(1, "stressfs starting\n");
  28:	83 ec 08             	sub    $0x8,%esp
  2b:	68 4b 09 00 00       	push   $0x94b
  30:	6a 01                	push   $0x1
  32:	e8 5e 05 00 00       	call   595 <printf>
  37:	83 c4 10             	add    $0x10,%esp
  memset(data, 'a', sizeof(data));
  3a:	83 ec 04             	sub    $0x4,%esp
  3d:	68 00 02 00 00       	push   $0x200
  42:	6a 61                	push   $0x61
  44:	8d 85 e6 fd ff ff    	lea    -0x21a(%ebp),%eax
  4a:	50                   	push   %eax
  4b:	e8 be 01 00 00       	call   20e <memset>
  50:	83 c4 10             	add    $0x10,%esp

  for(i = 0; i < 4; i++)
  53:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  5a:	eb 0d                	jmp    69 <main+0x69>
    if(fork() > 0)
  5c:	e8 6d 03 00 00       	call   3ce <fork>
  61:	85 c0                	test   %eax,%eax
  63:	7f 0c                	jg     71 <main+0x71>
  char data[512];

  printf(1, "stressfs starting\n");
  memset(data, 'a', sizeof(data));

  for(i = 0; i < 4; i++)
  65:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
  69:	83 7d f4 03          	cmpl   $0x3,-0xc(%ebp)
  6d:	7e ed                	jle    5c <main+0x5c>
  6f:	eb 01                	jmp    72 <main+0x72>
    if(fork() > 0)
      break;
  71:	90                   	nop

  printf(1, "write %d\n", i);
  72:	83 ec 04             	sub    $0x4,%esp
  75:	ff 75 f4             	pushl  -0xc(%ebp)
  78:	68 5e 09 00 00       	push   $0x95e
  7d:	6a 01                	push   $0x1
  7f:	e8 11 05 00 00       	call   595 <printf>
  84:	83 c4 10             	add    $0x10,%esp

  path[8] += i;
  87:	0f b6 45 ee          	movzbl -0x12(%ebp),%eax
  8b:	89 c2                	mov    %eax,%edx
  8d:	8b 45 f4             	mov    -0xc(%ebp),%eax
  90:	01 d0                	add    %edx,%eax
  92:	88 45 ee             	mov    %al,-0x12(%ebp)
  fd = open(path, O_CREATE | O_RDWR);
  95:	83 ec 08             	sub    $0x8,%esp
  98:	68 02 02 00 00       	push   $0x202
  9d:	8d 45 e6             	lea    -0x1a(%ebp),%eax
  a0:	50                   	push   %eax
  a1:	e8 70 03 00 00       	call   416 <open>
  a6:	83 c4 10             	add    $0x10,%esp
  a9:	89 45 f0             	mov    %eax,-0x10(%ebp)
  for(i = 0; i < 20; i++)
  ac:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  b3:	eb 1e                	jmp    d3 <main+0xd3>
//    printf(fd, "%d\n", i);
    write(fd, data, sizeof(data));
  b5:	83 ec 04             	sub    $0x4,%esp
  b8:	68 00 02 00 00       	push   $0x200
  bd:	8d 85 e6 fd ff ff    	lea    -0x21a(%ebp),%eax
  c3:	50                   	push   %eax
  c4:	ff 75 f0             	pushl  -0x10(%ebp)
  c7:	e8 2a 03 00 00       	call   3f6 <write>
  cc:	83 c4 10             	add    $0x10,%esp

  printf(1, "write %d\n", i);

  path[8] += i;
  fd = open(path, O_CREATE | O_RDWR);
  for(i = 0; i < 20; i++)
  cf:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
  d3:	83 7d f4 13          	cmpl   $0x13,-0xc(%ebp)
  d7:	7e dc                	jle    b5 <main+0xb5>
//    printf(fd, "%d\n", i);
    write(fd, data, sizeof(data));
  close(fd);
  d9:	83 ec 0c             	sub    $0xc,%esp
  dc:	ff 75 f0             	pushl  -0x10(%ebp)
  df:	e8 1a 03 00 00       	call   3fe <close>
  e4:	83 c4 10             	add    $0x10,%esp

  printf(1, "read\n");
  e7:	83 ec 08             	sub    $0x8,%esp
  ea:	68 68 09 00 00       	push   $0x968
  ef:	6a 01                	push   $0x1
  f1:	e8 9f 04 00 00       	call   595 <printf>
  f6:	83 c4 10             	add    $0x10,%esp

  fd = open(path, O_RDONLY);
  f9:	83 ec 08             	sub    $0x8,%esp
  fc:	6a 00                	push   $0x0
  fe:	8d 45 e6             	lea    -0x1a(%ebp),%eax
 101:	50                   	push   %eax
 102:	e8 0f 03 00 00       	call   416 <open>
 107:	83 c4 10             	add    $0x10,%esp
 10a:	89 45 f0             	mov    %eax,-0x10(%ebp)
  for (i = 0; i < 20; i++)
 10d:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 114:	eb 1e                	jmp    134 <main+0x134>
    read(fd, data, sizeof(data));
 116:	83 ec 04             	sub    $0x4,%esp
 119:	68 00 02 00 00       	push   $0x200
 11e:	8d 85 e6 fd ff ff    	lea    -0x21a(%ebp),%eax
 124:	50                   	push   %eax
 125:	ff 75 f0             	pushl  -0x10(%ebp)
 128:	e8 c1 02 00 00       	call   3ee <read>
 12d:	83 c4 10             	add    $0x10,%esp
  close(fd);

  printf(1, "read\n");

  fd = open(path, O_RDONLY);
  for (i = 0; i < 20; i++)
 130:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
 134:	83 7d f4 13          	cmpl   $0x13,-0xc(%ebp)
 138:	7e dc                	jle    116 <main+0x116>
    read(fd, data, sizeof(data));
  close(fd);
 13a:	83 ec 0c             	sub    $0xc,%esp
 13d:	ff 75 f0             	pushl  -0x10(%ebp)
 140:	e8 b9 02 00 00       	call   3fe <close>
 145:	83 c4 10             	add    $0x10,%esp

  wait();
 148:	e8 91 02 00 00       	call   3de <wait>
  
  exit();
 14d:	e8 84 02 00 00       	call   3d6 <exit>

00000152 <stosb>:
               "cc");
}

static inline void
stosb(void *addr, int data, int cnt)
{
 152:	55                   	push   %ebp
 153:	89 e5                	mov    %esp,%ebp
 155:	57                   	push   %edi
 156:	53                   	push   %ebx
  asm volatile("cld; rep stosb" :
 157:	8b 4d 08             	mov    0x8(%ebp),%ecx
 15a:	8b 55 10             	mov    0x10(%ebp),%edx
 15d:	8b 45 0c             	mov    0xc(%ebp),%eax
 160:	89 cb                	mov    %ecx,%ebx
 162:	89 df                	mov    %ebx,%edi
 164:	89 d1                	mov    %edx,%ecx
 166:	fc                   	cld    
 167:	f3 aa                	rep stos %al,%es:(%edi)
 169:	89 ca                	mov    %ecx,%edx
 16b:	89 fb                	mov    %edi,%ebx
 16d:	89 5d 08             	mov    %ebx,0x8(%ebp)
 170:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
 173:	90                   	nop
 174:	5b                   	pop    %ebx
 175:	5f                   	pop    %edi
 176:	5d                   	pop    %ebp
 177:	c3                   	ret    

00000178 <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, char *t)
{
 178:	55                   	push   %ebp
 179:	89 e5                	mov    %esp,%ebp
 17b:	83 ec 10             	sub    $0x10,%esp
  char *os;

  os = s;
 17e:	8b 45 08             	mov    0x8(%ebp),%eax
 181:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while((*s++ = *t++) != 0)
 184:	90                   	nop
 185:	8b 45 08             	mov    0x8(%ebp),%eax
 188:	8d 50 01             	lea    0x1(%eax),%edx
 18b:	89 55 08             	mov    %edx,0x8(%ebp)
 18e:	8b 55 0c             	mov    0xc(%ebp),%edx
 191:	8d 4a 01             	lea    0x1(%edx),%ecx
 194:	89 4d 0c             	mov    %ecx,0xc(%ebp)
 197:	0f b6 12             	movzbl (%edx),%edx
 19a:	88 10                	mov    %dl,(%eax)
 19c:	0f b6 00             	movzbl (%eax),%eax
 19f:	84 c0                	test   %al,%al
 1a1:	75 e2                	jne    185 <strcpy+0xd>
    ;
  return os;
 1a3:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 1a6:	c9                   	leave  
 1a7:	c3                   	ret    

000001a8 <strcmp>:

int
strcmp(const char *p, const char *q)
{
 1a8:	55                   	push   %ebp
 1a9:	89 e5                	mov    %esp,%ebp
  while(*p && *p == *q)
 1ab:	eb 08                	jmp    1b5 <strcmp+0xd>
    p++, q++;
 1ad:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 1b1:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
  while(*p && *p == *q)
 1b5:	8b 45 08             	mov    0x8(%ebp),%eax
 1b8:	0f b6 00             	movzbl (%eax),%eax
 1bb:	84 c0                	test   %al,%al
 1bd:	74 10                	je     1cf <strcmp+0x27>
 1bf:	8b 45 08             	mov    0x8(%ebp),%eax
 1c2:	0f b6 10             	movzbl (%eax),%edx
 1c5:	8b 45 0c             	mov    0xc(%ebp),%eax
 1c8:	0f b6 00             	movzbl (%eax),%eax
 1cb:	38 c2                	cmp    %al,%dl
 1cd:	74 de                	je     1ad <strcmp+0x5>
    p++, q++;
  return (uchar)*p - (uchar)*q;
 1cf:	8b 45 08             	mov    0x8(%ebp),%eax
 1d2:	0f b6 00             	movzbl (%eax),%eax
 1d5:	0f b6 d0             	movzbl %al,%edx
 1d8:	8b 45 0c             	mov    0xc(%ebp),%eax
 1db:	0f b6 00             	movzbl (%eax),%eax
 1de:	0f b6 c0             	movzbl %al,%eax
 1e1:	29 c2                	sub    %eax,%edx
 1e3:	89 d0                	mov    %edx,%eax
}
 1e5:	5d                   	pop    %ebp
 1e6:	c3                   	ret    

000001e7 <strlen>:

uint
strlen(char *s)
{
 1e7:	55                   	push   %ebp
 1e8:	89 e5                	mov    %esp,%ebp
 1ea:	83 ec 10             	sub    $0x10,%esp
  int n;

  for(n = 0; s[n]; n++)
 1ed:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
 1f4:	eb 04                	jmp    1fa <strlen+0x13>
 1f6:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
 1fa:	8b 55 fc             	mov    -0x4(%ebp),%edx
 1fd:	8b 45 08             	mov    0x8(%ebp),%eax
 200:	01 d0                	add    %edx,%eax
 202:	0f b6 00             	movzbl (%eax),%eax
 205:	84 c0                	test   %al,%al
 207:	75 ed                	jne    1f6 <strlen+0xf>
    ;
  return n;
 209:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 20c:	c9                   	leave  
 20d:	c3                   	ret    

0000020e <memset>:

void*
memset(void *dst, int c, uint n)
{
 20e:	55                   	push   %ebp
 20f:	89 e5                	mov    %esp,%ebp
  stosb(dst, c, n);
 211:	8b 45 10             	mov    0x10(%ebp),%eax
 214:	50                   	push   %eax
 215:	ff 75 0c             	pushl  0xc(%ebp)
 218:	ff 75 08             	pushl  0x8(%ebp)
 21b:	e8 32 ff ff ff       	call   152 <stosb>
 220:	83 c4 0c             	add    $0xc,%esp
  return dst;
 223:	8b 45 08             	mov    0x8(%ebp),%eax
}
 226:	c9                   	leave  
 227:	c3                   	ret    

00000228 <strchr>:

char*
strchr(const char *s, char c)
{
 228:	55                   	push   %ebp
 229:	89 e5                	mov    %esp,%ebp
 22b:	83 ec 04             	sub    $0x4,%esp
 22e:	8b 45 0c             	mov    0xc(%ebp),%eax
 231:	88 45 fc             	mov    %al,-0x4(%ebp)
  for(; *s; s++)
 234:	eb 14                	jmp    24a <strchr+0x22>
    if(*s == c)
 236:	8b 45 08             	mov    0x8(%ebp),%eax
 239:	0f b6 00             	movzbl (%eax),%eax
 23c:	3a 45 fc             	cmp    -0x4(%ebp),%al
 23f:	75 05                	jne    246 <strchr+0x1e>
      return (char*)s;
 241:	8b 45 08             	mov    0x8(%ebp),%eax
 244:	eb 13                	jmp    259 <strchr+0x31>
}

char*
strchr(const char *s, char c)
{
  for(; *s; s++)
 246:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 24a:	8b 45 08             	mov    0x8(%ebp),%eax
 24d:	0f b6 00             	movzbl (%eax),%eax
 250:	84 c0                	test   %al,%al
 252:	75 e2                	jne    236 <strchr+0xe>
    if(*s == c)
      return (char*)s;
  return 0;
 254:	b8 00 00 00 00       	mov    $0x0,%eax
}
 259:	c9                   	leave  
 25a:	c3                   	ret    

0000025b <gets>:

char*
gets(char *buf, int max)
{
 25b:	55                   	push   %ebp
 25c:	89 e5                	mov    %esp,%ebp
 25e:	83 ec 18             	sub    $0x18,%esp
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 261:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 268:	eb 42                	jmp    2ac <gets+0x51>
    cc = read(0, &c, 1);
 26a:	83 ec 04             	sub    $0x4,%esp
 26d:	6a 01                	push   $0x1
 26f:	8d 45 ef             	lea    -0x11(%ebp),%eax
 272:	50                   	push   %eax
 273:	6a 00                	push   $0x0
 275:	e8 74 01 00 00       	call   3ee <read>
 27a:	83 c4 10             	add    $0x10,%esp
 27d:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if(cc < 1)
 280:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 284:	7e 33                	jle    2b9 <gets+0x5e>
      break;
    buf[i++] = c;
 286:	8b 45 f4             	mov    -0xc(%ebp),%eax
 289:	8d 50 01             	lea    0x1(%eax),%edx
 28c:	89 55 f4             	mov    %edx,-0xc(%ebp)
 28f:	89 c2                	mov    %eax,%edx
 291:	8b 45 08             	mov    0x8(%ebp),%eax
 294:	01 c2                	add    %eax,%edx
 296:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 29a:	88 02                	mov    %al,(%edx)
    if(c == '\n' || c == '\r')
 29c:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 2a0:	3c 0a                	cmp    $0xa,%al
 2a2:	74 16                	je     2ba <gets+0x5f>
 2a4:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 2a8:	3c 0d                	cmp    $0xd,%al
 2aa:	74 0e                	je     2ba <gets+0x5f>
gets(char *buf, int max)
{
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 2ac:	8b 45 f4             	mov    -0xc(%ebp),%eax
 2af:	83 c0 01             	add    $0x1,%eax
 2b2:	3b 45 0c             	cmp    0xc(%ebp),%eax
 2b5:	7c b3                	jl     26a <gets+0xf>
 2b7:	eb 01                	jmp    2ba <gets+0x5f>
    cc = read(0, &c, 1);
    if(cc < 1)
      break;
 2b9:	90                   	nop
    buf[i++] = c;
    if(c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 2ba:	8b 55 f4             	mov    -0xc(%ebp),%edx
 2bd:	8b 45 08             	mov    0x8(%ebp),%eax
 2c0:	01 d0                	add    %edx,%eax
 2c2:	c6 00 00             	movb   $0x0,(%eax)
  return buf;
 2c5:	8b 45 08             	mov    0x8(%ebp),%eax
}
 2c8:	c9                   	leave  
 2c9:	c3                   	ret    

000002ca <stat>:

int
stat(char *n, struct stat *st)
{
 2ca:	55                   	push   %ebp
 2cb:	89 e5                	mov    %esp,%ebp
 2cd:	83 ec 18             	sub    $0x18,%esp
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 2d0:	83 ec 08             	sub    $0x8,%esp
 2d3:	6a 00                	push   $0x0
 2d5:	ff 75 08             	pushl  0x8(%ebp)
 2d8:	e8 39 01 00 00       	call   416 <open>
 2dd:	83 c4 10             	add    $0x10,%esp
 2e0:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(fd < 0)
 2e3:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 2e7:	79 07                	jns    2f0 <stat+0x26>
    return -1;
 2e9:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 2ee:	eb 25                	jmp    315 <stat+0x4b>
  r = fstat(fd, st);
 2f0:	83 ec 08             	sub    $0x8,%esp
 2f3:	ff 75 0c             	pushl  0xc(%ebp)
 2f6:	ff 75 f4             	pushl  -0xc(%ebp)
 2f9:	e8 30 01 00 00       	call   42e <fstat>
 2fe:	83 c4 10             	add    $0x10,%esp
 301:	89 45 f0             	mov    %eax,-0x10(%ebp)
  close(fd);
 304:	83 ec 0c             	sub    $0xc,%esp
 307:	ff 75 f4             	pushl  -0xc(%ebp)
 30a:	e8 ef 00 00 00       	call   3fe <close>
 30f:	83 c4 10             	add    $0x10,%esp
  return r;
 312:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
 315:	c9                   	leave  
 316:	c3                   	ret    

00000317 <atoi>:

int
atoi(const char *s)
{
 317:	55                   	push   %ebp
 318:	89 e5                	mov    %esp,%ebp
 31a:	83 ec 10             	sub    $0x10,%esp
  int n, sign;

  n = 0;
 31d:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  while(*s == ' ') s++;
 324:	eb 04                	jmp    32a <atoi+0x13>
 326:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 32a:	8b 45 08             	mov    0x8(%ebp),%eax
 32d:	0f b6 00             	movzbl (%eax),%eax
 330:	3c 20                	cmp    $0x20,%al
 332:	74 f2                	je     326 <atoi+0xf>
  sign = (*s == '-') ? -1 : 1;
 334:	8b 45 08             	mov    0x8(%ebp),%eax
 337:	0f b6 00             	movzbl (%eax),%eax
 33a:	3c 2d                	cmp    $0x2d,%al
 33c:	75 07                	jne    345 <atoi+0x2e>
 33e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 343:	eb 05                	jmp    34a <atoi+0x33>
 345:	b8 01 00 00 00       	mov    $0x1,%eax
 34a:	89 45 f8             	mov    %eax,-0x8(%ebp)
  while('0' <= *s && *s <= '9')
 34d:	eb 25                	jmp    374 <atoi+0x5d>
    n = n*10 + *s++ - '0';
 34f:	8b 55 fc             	mov    -0x4(%ebp),%edx
 352:	89 d0                	mov    %edx,%eax
 354:	c1 e0 02             	shl    $0x2,%eax
 357:	01 d0                	add    %edx,%eax
 359:	01 c0                	add    %eax,%eax
 35b:	89 c1                	mov    %eax,%ecx
 35d:	8b 45 08             	mov    0x8(%ebp),%eax
 360:	8d 50 01             	lea    0x1(%eax),%edx
 363:	89 55 08             	mov    %edx,0x8(%ebp)
 366:	0f b6 00             	movzbl (%eax),%eax
 369:	0f be c0             	movsbl %al,%eax
 36c:	01 c8                	add    %ecx,%eax
 36e:	83 e8 30             	sub    $0x30,%eax
 371:	89 45 fc             	mov    %eax,-0x4(%ebp)
  int n, sign;

  n = 0;
  while(*s == ' ') s++;
  sign = (*s == '-') ? -1 : 1;
  while('0' <= *s && *s <= '9')
 374:	8b 45 08             	mov    0x8(%ebp),%eax
 377:	0f b6 00             	movzbl (%eax),%eax
 37a:	3c 2f                	cmp    $0x2f,%al
 37c:	7e 0a                	jle    388 <atoi+0x71>
 37e:	8b 45 08             	mov    0x8(%ebp),%eax
 381:	0f b6 00             	movzbl (%eax),%eax
 384:	3c 39                	cmp    $0x39,%al
 386:	7e c7                	jle    34f <atoi+0x38>
    n = n*10 + *s++ - '0';
  return sign*n;
 388:	8b 45 f8             	mov    -0x8(%ebp),%eax
 38b:	0f af 45 fc          	imul   -0x4(%ebp),%eax
}
 38f:	c9                   	leave  
 390:	c3                   	ret    

00000391 <memmove>:

void*
memmove(void *vdst, void *vsrc, int n)
{
 391:	55                   	push   %ebp
 392:	89 e5                	mov    %esp,%ebp
 394:	83 ec 10             	sub    $0x10,%esp
  char *dst, *src;
  
  dst = vdst;
 397:	8b 45 08             	mov    0x8(%ebp),%eax
 39a:	89 45 fc             	mov    %eax,-0x4(%ebp)
  src = vsrc;
 39d:	8b 45 0c             	mov    0xc(%ebp),%eax
 3a0:	89 45 f8             	mov    %eax,-0x8(%ebp)
  while(n-- > 0)
 3a3:	eb 17                	jmp    3bc <memmove+0x2b>
    *dst++ = *src++;
 3a5:	8b 45 fc             	mov    -0x4(%ebp),%eax
 3a8:	8d 50 01             	lea    0x1(%eax),%edx
 3ab:	89 55 fc             	mov    %edx,-0x4(%ebp)
 3ae:	8b 55 f8             	mov    -0x8(%ebp),%edx
 3b1:	8d 4a 01             	lea    0x1(%edx),%ecx
 3b4:	89 4d f8             	mov    %ecx,-0x8(%ebp)
 3b7:	0f b6 12             	movzbl (%edx),%edx
 3ba:	88 10                	mov    %dl,(%eax)
{
  char *dst, *src;
  
  dst = vdst;
  src = vsrc;
  while(n-- > 0)
 3bc:	8b 45 10             	mov    0x10(%ebp),%eax
 3bf:	8d 50 ff             	lea    -0x1(%eax),%edx
 3c2:	89 55 10             	mov    %edx,0x10(%ebp)
 3c5:	85 c0                	test   %eax,%eax
 3c7:	7f dc                	jg     3a5 <memmove+0x14>
    *dst++ = *src++;
  return vdst;
 3c9:	8b 45 08             	mov    0x8(%ebp),%eax
}
 3cc:	c9                   	leave  
 3cd:	c3                   	ret    

000003ce <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
 3ce:	b8 01 00 00 00       	mov    $0x1,%eax
 3d3:	cd 40                	int    $0x40
 3d5:	c3                   	ret    

000003d6 <exit>:
SYSCALL(exit)
 3d6:	b8 02 00 00 00       	mov    $0x2,%eax
 3db:	cd 40                	int    $0x40
 3dd:	c3                   	ret    

000003de <wait>:
SYSCALL(wait)
 3de:	b8 03 00 00 00       	mov    $0x3,%eax
 3e3:	cd 40                	int    $0x40
 3e5:	c3                   	ret    

000003e6 <pipe>:
SYSCALL(pipe)
 3e6:	b8 04 00 00 00       	mov    $0x4,%eax
 3eb:	cd 40                	int    $0x40
 3ed:	c3                   	ret    

000003ee <read>:
SYSCALL(read)
 3ee:	b8 05 00 00 00       	mov    $0x5,%eax
 3f3:	cd 40                	int    $0x40
 3f5:	c3                   	ret    

000003f6 <write>:
SYSCALL(write)
 3f6:	b8 10 00 00 00       	mov    $0x10,%eax
 3fb:	cd 40                	int    $0x40
 3fd:	c3                   	ret    

000003fe <close>:
SYSCALL(close)
 3fe:	b8 15 00 00 00       	mov    $0x15,%eax
 403:	cd 40                	int    $0x40
 405:	c3                   	ret    

00000406 <kill>:
SYSCALL(kill)
 406:	b8 06 00 00 00       	mov    $0x6,%eax
 40b:	cd 40                	int    $0x40
 40d:	c3                   	ret    

0000040e <exec>:
SYSCALL(exec)
 40e:	b8 07 00 00 00       	mov    $0x7,%eax
 413:	cd 40                	int    $0x40
 415:	c3                   	ret    

00000416 <open>:
SYSCALL(open)
 416:	b8 0f 00 00 00       	mov    $0xf,%eax
 41b:	cd 40                	int    $0x40
 41d:	c3                   	ret    

0000041e <mknod>:
SYSCALL(mknod)
 41e:	b8 11 00 00 00       	mov    $0x11,%eax
 423:	cd 40                	int    $0x40
 425:	c3                   	ret    

00000426 <unlink>:
SYSCALL(unlink)
 426:	b8 12 00 00 00       	mov    $0x12,%eax
 42b:	cd 40                	int    $0x40
 42d:	c3                   	ret    

0000042e <fstat>:
SYSCALL(fstat)
 42e:	b8 08 00 00 00       	mov    $0x8,%eax
 433:	cd 40                	int    $0x40
 435:	c3                   	ret    

00000436 <link>:
SYSCALL(link)
 436:	b8 13 00 00 00       	mov    $0x13,%eax
 43b:	cd 40                	int    $0x40
 43d:	c3                   	ret    

0000043e <mkdir>:
SYSCALL(mkdir)
 43e:	b8 14 00 00 00       	mov    $0x14,%eax
 443:	cd 40                	int    $0x40
 445:	c3                   	ret    

00000446 <chdir>:
SYSCALL(chdir)
 446:	b8 09 00 00 00       	mov    $0x9,%eax
 44b:	cd 40                	int    $0x40
 44d:	c3                   	ret    

0000044e <dup>:
SYSCALL(dup)
 44e:	b8 0a 00 00 00       	mov    $0xa,%eax
 453:	cd 40                	int    $0x40
 455:	c3                   	ret    

00000456 <getpid>:
SYSCALL(getpid)
 456:	b8 0b 00 00 00       	mov    $0xb,%eax
 45b:	cd 40                	int    $0x40
 45d:	c3                   	ret    

0000045e <sbrk>:
SYSCALL(sbrk)
 45e:	b8 0c 00 00 00       	mov    $0xc,%eax
 463:	cd 40                	int    $0x40
 465:	c3                   	ret    

00000466 <sleep>:
SYSCALL(sleep)
 466:	b8 0d 00 00 00       	mov    $0xd,%eax
 46b:	cd 40                	int    $0x40
 46d:	c3                   	ret    

0000046e <uptime>:
SYSCALL(uptime)
 46e:	b8 0e 00 00 00       	mov    $0xe,%eax
 473:	cd 40                	int    $0x40
 475:	c3                   	ret    

00000476 <halt>:
SYSCALL(halt)
 476:	b8 16 00 00 00       	mov    $0x16,%eax
 47b:	cd 40                	int    $0x40
 47d:	c3                   	ret    

0000047e <date>:
SYSCALL(date)    #added the date system call
 47e:	b8 17 00 00 00       	mov    $0x17,%eax
 483:	cd 40                	int    $0x40
 485:	c3                   	ret    

00000486 <getuid>:
SYSCALL(getuid)
 486:	b8 18 00 00 00       	mov    $0x18,%eax
 48b:	cd 40                	int    $0x40
 48d:	c3                   	ret    

0000048e <getgid>:
SYSCALL(getgid)
 48e:	b8 19 00 00 00       	mov    $0x19,%eax
 493:	cd 40                	int    $0x40
 495:	c3                   	ret    

00000496 <getppid>:
SYSCALL(getppid)
 496:	b8 1a 00 00 00       	mov    $0x1a,%eax
 49b:	cd 40                	int    $0x40
 49d:	c3                   	ret    

0000049e <setuid>:
SYSCALL(setuid)
 49e:	b8 1b 00 00 00       	mov    $0x1b,%eax
 4a3:	cd 40                	int    $0x40
 4a5:	c3                   	ret    

000004a6 <setgid>:
SYSCALL(setgid)
 4a6:	b8 1c 00 00 00       	mov    $0x1c,%eax
 4ab:	cd 40                	int    $0x40
 4ad:	c3                   	ret    

000004ae <getprocs>:
SYSCALL(getprocs)
 4ae:	b8 1d 00 00 00       	mov    $0x1d,%eax
 4b3:	cd 40                	int    $0x40
 4b5:	c3                   	ret    

000004b6 <setpriority>:
SYSCALL(setpriority)
 4b6:	b8 1e 00 00 00       	mov    $0x1e,%eax
 4bb:	cd 40                	int    $0x40
 4bd:	c3                   	ret    

000004be <putc>:
#include "stat.h"
#include "user.h"

static void
putc(int fd, char c)
{
 4be:	55                   	push   %ebp
 4bf:	89 e5                	mov    %esp,%ebp
 4c1:	83 ec 18             	sub    $0x18,%esp
 4c4:	8b 45 0c             	mov    0xc(%ebp),%eax
 4c7:	88 45 f4             	mov    %al,-0xc(%ebp)
  write(fd, &c, 1);
 4ca:	83 ec 04             	sub    $0x4,%esp
 4cd:	6a 01                	push   $0x1
 4cf:	8d 45 f4             	lea    -0xc(%ebp),%eax
 4d2:	50                   	push   %eax
 4d3:	ff 75 08             	pushl  0x8(%ebp)
 4d6:	e8 1b ff ff ff       	call   3f6 <write>
 4db:	83 c4 10             	add    $0x10,%esp
}
 4de:	90                   	nop
 4df:	c9                   	leave  
 4e0:	c3                   	ret    

000004e1 <printint>:

static void
printint(int fd, int xx, int base, int sgn)
{
 4e1:	55                   	push   %ebp
 4e2:	89 e5                	mov    %esp,%ebp
 4e4:	53                   	push   %ebx
 4e5:	83 ec 24             	sub    $0x24,%esp
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 4e8:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  if(sgn && xx < 0){
 4ef:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
 4f3:	74 17                	je     50c <printint+0x2b>
 4f5:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
 4f9:	79 11                	jns    50c <printint+0x2b>
    neg = 1;
 4fb:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
    x = -xx;
 502:	8b 45 0c             	mov    0xc(%ebp),%eax
 505:	f7 d8                	neg    %eax
 507:	89 45 ec             	mov    %eax,-0x14(%ebp)
 50a:	eb 06                	jmp    512 <printint+0x31>
  } else {
    x = xx;
 50c:	8b 45 0c             	mov    0xc(%ebp),%eax
 50f:	89 45 ec             	mov    %eax,-0x14(%ebp)
  }

  i = 0;
 512:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  do{
    buf[i++] = digits[x % base];
 519:	8b 4d f4             	mov    -0xc(%ebp),%ecx
 51c:	8d 41 01             	lea    0x1(%ecx),%eax
 51f:	89 45 f4             	mov    %eax,-0xc(%ebp)
 522:	8b 5d 10             	mov    0x10(%ebp),%ebx
 525:	8b 45 ec             	mov    -0x14(%ebp),%eax
 528:	ba 00 00 00 00       	mov    $0x0,%edx
 52d:	f7 f3                	div    %ebx
 52f:	89 d0                	mov    %edx,%eax
 531:	0f b6 80 c0 0b 00 00 	movzbl 0xbc0(%eax),%eax
 538:	88 44 0d dc          	mov    %al,-0x24(%ebp,%ecx,1)
  }while((x /= base) != 0);
 53c:	8b 5d 10             	mov    0x10(%ebp),%ebx
 53f:	8b 45 ec             	mov    -0x14(%ebp),%eax
 542:	ba 00 00 00 00       	mov    $0x0,%edx
 547:	f7 f3                	div    %ebx
 549:	89 45 ec             	mov    %eax,-0x14(%ebp)
 54c:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 550:	75 c7                	jne    519 <printint+0x38>
  if(neg)
 552:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 556:	74 2d                	je     585 <printint+0xa4>
    buf[i++] = '-';
 558:	8b 45 f4             	mov    -0xc(%ebp),%eax
 55b:	8d 50 01             	lea    0x1(%eax),%edx
 55e:	89 55 f4             	mov    %edx,-0xc(%ebp)
 561:	c6 44 05 dc 2d       	movb   $0x2d,-0x24(%ebp,%eax,1)

  while(--i >= 0)
 566:	eb 1d                	jmp    585 <printint+0xa4>
    putc(fd, buf[i]);
 568:	8d 55 dc             	lea    -0x24(%ebp),%edx
 56b:	8b 45 f4             	mov    -0xc(%ebp),%eax
 56e:	01 d0                	add    %edx,%eax
 570:	0f b6 00             	movzbl (%eax),%eax
 573:	0f be c0             	movsbl %al,%eax
 576:	83 ec 08             	sub    $0x8,%esp
 579:	50                   	push   %eax
 57a:	ff 75 08             	pushl  0x8(%ebp)
 57d:	e8 3c ff ff ff       	call   4be <putc>
 582:	83 c4 10             	add    $0x10,%esp
    buf[i++] = digits[x % base];
  }while((x /= base) != 0);
  if(neg)
    buf[i++] = '-';

  while(--i >= 0)
 585:	83 6d f4 01          	subl   $0x1,-0xc(%ebp)
 589:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 58d:	79 d9                	jns    568 <printint+0x87>
    putc(fd, buf[i]);
}
 58f:	90                   	nop
 590:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 593:	c9                   	leave  
 594:	c3                   	ret    

00000595 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, char *fmt, ...)
{
 595:	55                   	push   %ebp
 596:	89 e5                	mov    %esp,%ebp
 598:	83 ec 28             	sub    $0x28,%esp
  char *s;
  int c, i, state;
  uint *ap;

  state = 0;
 59b:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  ap = (uint*)(void*)&fmt + 1;
 5a2:	8d 45 0c             	lea    0xc(%ebp),%eax
 5a5:	83 c0 04             	add    $0x4,%eax
 5a8:	89 45 e8             	mov    %eax,-0x18(%ebp)
  for(i = 0; fmt[i]; i++){
 5ab:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 5b2:	e9 59 01 00 00       	jmp    710 <printf+0x17b>
    c = fmt[i] & 0xff;
 5b7:	8b 55 0c             	mov    0xc(%ebp),%edx
 5ba:	8b 45 f0             	mov    -0x10(%ebp),%eax
 5bd:	01 d0                	add    %edx,%eax
 5bf:	0f b6 00             	movzbl (%eax),%eax
 5c2:	0f be c0             	movsbl %al,%eax
 5c5:	25 ff 00 00 00       	and    $0xff,%eax
 5ca:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    if(state == 0){
 5cd:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 5d1:	75 2c                	jne    5ff <printf+0x6a>
      if(c == '%'){
 5d3:	83 7d e4 25          	cmpl   $0x25,-0x1c(%ebp)
 5d7:	75 0c                	jne    5e5 <printf+0x50>
        state = '%';
 5d9:	c7 45 ec 25 00 00 00 	movl   $0x25,-0x14(%ebp)
 5e0:	e9 27 01 00 00       	jmp    70c <printf+0x177>
      } else {
        putc(fd, c);
 5e5:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 5e8:	0f be c0             	movsbl %al,%eax
 5eb:	83 ec 08             	sub    $0x8,%esp
 5ee:	50                   	push   %eax
 5ef:	ff 75 08             	pushl  0x8(%ebp)
 5f2:	e8 c7 fe ff ff       	call   4be <putc>
 5f7:	83 c4 10             	add    $0x10,%esp
 5fa:	e9 0d 01 00 00       	jmp    70c <printf+0x177>
      }
    } else if(state == '%'){
 5ff:	83 7d ec 25          	cmpl   $0x25,-0x14(%ebp)
 603:	0f 85 03 01 00 00    	jne    70c <printf+0x177>
      if(c == 'd'){
 609:	83 7d e4 64          	cmpl   $0x64,-0x1c(%ebp)
 60d:	75 1e                	jne    62d <printf+0x98>
        printint(fd, *ap, 10, 1);
 60f:	8b 45 e8             	mov    -0x18(%ebp),%eax
 612:	8b 00                	mov    (%eax),%eax
 614:	6a 01                	push   $0x1
 616:	6a 0a                	push   $0xa
 618:	50                   	push   %eax
 619:	ff 75 08             	pushl  0x8(%ebp)
 61c:	e8 c0 fe ff ff       	call   4e1 <printint>
 621:	83 c4 10             	add    $0x10,%esp
        ap++;
 624:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 628:	e9 d8 00 00 00       	jmp    705 <printf+0x170>
      } else if(c == 'x' || c == 'p'){
 62d:	83 7d e4 78          	cmpl   $0x78,-0x1c(%ebp)
 631:	74 06                	je     639 <printf+0xa4>
 633:	83 7d e4 70          	cmpl   $0x70,-0x1c(%ebp)
 637:	75 1e                	jne    657 <printf+0xc2>
        printint(fd, *ap, 16, 0);
 639:	8b 45 e8             	mov    -0x18(%ebp),%eax
 63c:	8b 00                	mov    (%eax),%eax
 63e:	6a 00                	push   $0x0
 640:	6a 10                	push   $0x10
 642:	50                   	push   %eax
 643:	ff 75 08             	pushl  0x8(%ebp)
 646:	e8 96 fe ff ff       	call   4e1 <printint>
 64b:	83 c4 10             	add    $0x10,%esp
        ap++;
 64e:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 652:	e9 ae 00 00 00       	jmp    705 <printf+0x170>
      } else if(c == 's'){
 657:	83 7d e4 73          	cmpl   $0x73,-0x1c(%ebp)
 65b:	75 43                	jne    6a0 <printf+0x10b>
        s = (char*)*ap;
 65d:	8b 45 e8             	mov    -0x18(%ebp),%eax
 660:	8b 00                	mov    (%eax),%eax
 662:	89 45 f4             	mov    %eax,-0xc(%ebp)
        ap++;
 665:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
        if(s == 0)
 669:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 66d:	75 25                	jne    694 <printf+0xff>
          s = "(null)";
 66f:	c7 45 f4 6e 09 00 00 	movl   $0x96e,-0xc(%ebp)
        while(*s != 0){
 676:	eb 1c                	jmp    694 <printf+0xff>
          putc(fd, *s);
 678:	8b 45 f4             	mov    -0xc(%ebp),%eax
 67b:	0f b6 00             	movzbl (%eax),%eax
 67e:	0f be c0             	movsbl %al,%eax
 681:	83 ec 08             	sub    $0x8,%esp
 684:	50                   	push   %eax
 685:	ff 75 08             	pushl  0x8(%ebp)
 688:	e8 31 fe ff ff       	call   4be <putc>
 68d:	83 c4 10             	add    $0x10,%esp
          s++;
 690:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
      } else if(c == 's'){
        s = (char*)*ap;
        ap++;
        if(s == 0)
          s = "(null)";
        while(*s != 0){
 694:	8b 45 f4             	mov    -0xc(%ebp),%eax
 697:	0f b6 00             	movzbl (%eax),%eax
 69a:	84 c0                	test   %al,%al
 69c:	75 da                	jne    678 <printf+0xe3>
 69e:	eb 65                	jmp    705 <printf+0x170>
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 6a0:	83 7d e4 63          	cmpl   $0x63,-0x1c(%ebp)
 6a4:	75 1d                	jne    6c3 <printf+0x12e>
        putc(fd, *ap);
 6a6:	8b 45 e8             	mov    -0x18(%ebp),%eax
 6a9:	8b 00                	mov    (%eax),%eax
 6ab:	0f be c0             	movsbl %al,%eax
 6ae:	83 ec 08             	sub    $0x8,%esp
 6b1:	50                   	push   %eax
 6b2:	ff 75 08             	pushl  0x8(%ebp)
 6b5:	e8 04 fe ff ff       	call   4be <putc>
 6ba:	83 c4 10             	add    $0x10,%esp
        ap++;
 6bd:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 6c1:	eb 42                	jmp    705 <printf+0x170>
      } else if(c == '%'){
 6c3:	83 7d e4 25          	cmpl   $0x25,-0x1c(%ebp)
 6c7:	75 17                	jne    6e0 <printf+0x14b>
        putc(fd, c);
 6c9:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 6cc:	0f be c0             	movsbl %al,%eax
 6cf:	83 ec 08             	sub    $0x8,%esp
 6d2:	50                   	push   %eax
 6d3:	ff 75 08             	pushl  0x8(%ebp)
 6d6:	e8 e3 fd ff ff       	call   4be <putc>
 6db:	83 c4 10             	add    $0x10,%esp
 6de:	eb 25                	jmp    705 <printf+0x170>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 6e0:	83 ec 08             	sub    $0x8,%esp
 6e3:	6a 25                	push   $0x25
 6e5:	ff 75 08             	pushl  0x8(%ebp)
 6e8:	e8 d1 fd ff ff       	call   4be <putc>
 6ed:	83 c4 10             	add    $0x10,%esp
        putc(fd, c);
 6f0:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 6f3:	0f be c0             	movsbl %al,%eax
 6f6:	83 ec 08             	sub    $0x8,%esp
 6f9:	50                   	push   %eax
 6fa:	ff 75 08             	pushl  0x8(%ebp)
 6fd:	e8 bc fd ff ff       	call   4be <putc>
 702:	83 c4 10             	add    $0x10,%esp
      }
      state = 0;
 705:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
 70c:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
 710:	8b 55 0c             	mov    0xc(%ebp),%edx
 713:	8b 45 f0             	mov    -0x10(%ebp),%eax
 716:	01 d0                	add    %edx,%eax
 718:	0f b6 00             	movzbl (%eax),%eax
 71b:	84 c0                	test   %al,%al
 71d:	0f 85 94 fe ff ff    	jne    5b7 <printf+0x22>
        putc(fd, c);
      }
      state = 0;
    }
  }
}
 723:	90                   	nop
 724:	c9                   	leave  
 725:	c3                   	ret    

00000726 <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 726:	55                   	push   %ebp
 727:	89 e5                	mov    %esp,%ebp
 729:	83 ec 10             	sub    $0x10,%esp
  Header *bp, *p;

  bp = (Header*)ap - 1;
 72c:	8b 45 08             	mov    0x8(%ebp),%eax
 72f:	83 e8 08             	sub    $0x8,%eax
 732:	89 45 f8             	mov    %eax,-0x8(%ebp)
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 735:	a1 dc 0b 00 00       	mov    0xbdc,%eax
 73a:	89 45 fc             	mov    %eax,-0x4(%ebp)
 73d:	eb 24                	jmp    763 <free+0x3d>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 73f:	8b 45 fc             	mov    -0x4(%ebp),%eax
 742:	8b 00                	mov    (%eax),%eax
 744:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 747:	77 12                	ja     75b <free+0x35>
 749:	8b 45 f8             	mov    -0x8(%ebp),%eax
 74c:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 74f:	77 24                	ja     775 <free+0x4f>
 751:	8b 45 fc             	mov    -0x4(%ebp),%eax
 754:	8b 00                	mov    (%eax),%eax
 756:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 759:	77 1a                	ja     775 <free+0x4f>
free(void *ap)
{
  Header *bp, *p;

  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 75b:	8b 45 fc             	mov    -0x4(%ebp),%eax
 75e:	8b 00                	mov    (%eax),%eax
 760:	89 45 fc             	mov    %eax,-0x4(%ebp)
 763:	8b 45 f8             	mov    -0x8(%ebp),%eax
 766:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 769:	76 d4                	jbe    73f <free+0x19>
 76b:	8b 45 fc             	mov    -0x4(%ebp),%eax
 76e:	8b 00                	mov    (%eax),%eax
 770:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 773:	76 ca                	jbe    73f <free+0x19>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if(bp + bp->s.size == p->s.ptr){
 775:	8b 45 f8             	mov    -0x8(%ebp),%eax
 778:	8b 40 04             	mov    0x4(%eax),%eax
 77b:	8d 14 c5 00 00 00 00 	lea    0x0(,%eax,8),%edx
 782:	8b 45 f8             	mov    -0x8(%ebp),%eax
 785:	01 c2                	add    %eax,%edx
 787:	8b 45 fc             	mov    -0x4(%ebp),%eax
 78a:	8b 00                	mov    (%eax),%eax
 78c:	39 c2                	cmp    %eax,%edx
 78e:	75 24                	jne    7b4 <free+0x8e>
    bp->s.size += p->s.ptr->s.size;
 790:	8b 45 f8             	mov    -0x8(%ebp),%eax
 793:	8b 50 04             	mov    0x4(%eax),%edx
 796:	8b 45 fc             	mov    -0x4(%ebp),%eax
 799:	8b 00                	mov    (%eax),%eax
 79b:	8b 40 04             	mov    0x4(%eax),%eax
 79e:	01 c2                	add    %eax,%edx
 7a0:	8b 45 f8             	mov    -0x8(%ebp),%eax
 7a3:	89 50 04             	mov    %edx,0x4(%eax)
    bp->s.ptr = p->s.ptr->s.ptr;
 7a6:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7a9:	8b 00                	mov    (%eax),%eax
 7ab:	8b 10                	mov    (%eax),%edx
 7ad:	8b 45 f8             	mov    -0x8(%ebp),%eax
 7b0:	89 10                	mov    %edx,(%eax)
 7b2:	eb 0a                	jmp    7be <free+0x98>
  } else
    bp->s.ptr = p->s.ptr;
 7b4:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7b7:	8b 10                	mov    (%eax),%edx
 7b9:	8b 45 f8             	mov    -0x8(%ebp),%eax
 7bc:	89 10                	mov    %edx,(%eax)
  if(p + p->s.size == bp){
 7be:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7c1:	8b 40 04             	mov    0x4(%eax),%eax
 7c4:	8d 14 c5 00 00 00 00 	lea    0x0(,%eax,8),%edx
 7cb:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7ce:	01 d0                	add    %edx,%eax
 7d0:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 7d3:	75 20                	jne    7f5 <free+0xcf>
    p->s.size += bp->s.size;
 7d5:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7d8:	8b 50 04             	mov    0x4(%eax),%edx
 7db:	8b 45 f8             	mov    -0x8(%ebp),%eax
 7de:	8b 40 04             	mov    0x4(%eax),%eax
 7e1:	01 c2                	add    %eax,%edx
 7e3:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7e6:	89 50 04             	mov    %edx,0x4(%eax)
    p->s.ptr = bp->s.ptr;
 7e9:	8b 45 f8             	mov    -0x8(%ebp),%eax
 7ec:	8b 10                	mov    (%eax),%edx
 7ee:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7f1:	89 10                	mov    %edx,(%eax)
 7f3:	eb 08                	jmp    7fd <free+0xd7>
  } else
    p->s.ptr = bp;
 7f5:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7f8:	8b 55 f8             	mov    -0x8(%ebp),%edx
 7fb:	89 10                	mov    %edx,(%eax)
  freep = p;
 7fd:	8b 45 fc             	mov    -0x4(%ebp),%eax
 800:	a3 dc 0b 00 00       	mov    %eax,0xbdc
}
 805:	90                   	nop
 806:	c9                   	leave  
 807:	c3                   	ret    

00000808 <morecore>:

static Header*
morecore(uint nu)
{
 808:	55                   	push   %ebp
 809:	89 e5                	mov    %esp,%ebp
 80b:	83 ec 18             	sub    $0x18,%esp
  char *p;
  Header *hp;

  if(nu < 4096)
 80e:	81 7d 08 ff 0f 00 00 	cmpl   $0xfff,0x8(%ebp)
 815:	77 07                	ja     81e <morecore+0x16>
    nu = 4096;
 817:	c7 45 08 00 10 00 00 	movl   $0x1000,0x8(%ebp)
  p = sbrk(nu * sizeof(Header));
 81e:	8b 45 08             	mov    0x8(%ebp),%eax
 821:	c1 e0 03             	shl    $0x3,%eax
 824:	83 ec 0c             	sub    $0xc,%esp
 827:	50                   	push   %eax
 828:	e8 31 fc ff ff       	call   45e <sbrk>
 82d:	83 c4 10             	add    $0x10,%esp
 830:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(p == (char*)-1)
 833:	83 7d f4 ff          	cmpl   $0xffffffff,-0xc(%ebp)
 837:	75 07                	jne    840 <morecore+0x38>
    return 0;
 839:	b8 00 00 00 00       	mov    $0x0,%eax
 83e:	eb 26                	jmp    866 <morecore+0x5e>
  hp = (Header*)p;
 840:	8b 45 f4             	mov    -0xc(%ebp),%eax
 843:	89 45 f0             	mov    %eax,-0x10(%ebp)
  hp->s.size = nu;
 846:	8b 45 f0             	mov    -0x10(%ebp),%eax
 849:	8b 55 08             	mov    0x8(%ebp),%edx
 84c:	89 50 04             	mov    %edx,0x4(%eax)
  free((void*)(hp + 1));
 84f:	8b 45 f0             	mov    -0x10(%ebp),%eax
 852:	83 c0 08             	add    $0x8,%eax
 855:	83 ec 0c             	sub    $0xc,%esp
 858:	50                   	push   %eax
 859:	e8 c8 fe ff ff       	call   726 <free>
 85e:	83 c4 10             	add    $0x10,%esp
  return freep;
 861:	a1 dc 0b 00 00       	mov    0xbdc,%eax
}
 866:	c9                   	leave  
 867:	c3                   	ret    

00000868 <malloc>:

void*
malloc(uint nbytes)
{
 868:	55                   	push   %ebp
 869:	89 e5                	mov    %esp,%ebp
 86b:	83 ec 18             	sub    $0x18,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 86e:	8b 45 08             	mov    0x8(%ebp),%eax
 871:	83 c0 07             	add    $0x7,%eax
 874:	c1 e8 03             	shr    $0x3,%eax
 877:	83 c0 01             	add    $0x1,%eax
 87a:	89 45 ec             	mov    %eax,-0x14(%ebp)
  if((prevp = freep) == 0){
 87d:	a1 dc 0b 00 00       	mov    0xbdc,%eax
 882:	89 45 f0             	mov    %eax,-0x10(%ebp)
 885:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 889:	75 23                	jne    8ae <malloc+0x46>
    base.s.ptr = freep = prevp = &base;
 88b:	c7 45 f0 d4 0b 00 00 	movl   $0xbd4,-0x10(%ebp)
 892:	8b 45 f0             	mov    -0x10(%ebp),%eax
 895:	a3 dc 0b 00 00       	mov    %eax,0xbdc
 89a:	a1 dc 0b 00 00       	mov    0xbdc,%eax
 89f:	a3 d4 0b 00 00       	mov    %eax,0xbd4
    base.s.size = 0;
 8a4:	c7 05 d8 0b 00 00 00 	movl   $0x0,0xbd8
 8ab:	00 00 00 
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 8ae:	8b 45 f0             	mov    -0x10(%ebp),%eax
 8b1:	8b 00                	mov    (%eax),%eax
 8b3:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if(p->s.size >= nunits){
 8b6:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8b9:	8b 40 04             	mov    0x4(%eax),%eax
 8bc:	3b 45 ec             	cmp    -0x14(%ebp),%eax
 8bf:	72 4d                	jb     90e <malloc+0xa6>
      if(p->s.size == nunits)
 8c1:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8c4:	8b 40 04             	mov    0x4(%eax),%eax
 8c7:	3b 45 ec             	cmp    -0x14(%ebp),%eax
 8ca:	75 0c                	jne    8d8 <malloc+0x70>
        prevp->s.ptr = p->s.ptr;
 8cc:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8cf:	8b 10                	mov    (%eax),%edx
 8d1:	8b 45 f0             	mov    -0x10(%ebp),%eax
 8d4:	89 10                	mov    %edx,(%eax)
 8d6:	eb 26                	jmp    8fe <malloc+0x96>
      else {
        p->s.size -= nunits;
 8d8:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8db:	8b 40 04             	mov    0x4(%eax),%eax
 8de:	2b 45 ec             	sub    -0x14(%ebp),%eax
 8e1:	89 c2                	mov    %eax,%edx
 8e3:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8e6:	89 50 04             	mov    %edx,0x4(%eax)
        p += p->s.size;
 8e9:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8ec:	8b 40 04             	mov    0x4(%eax),%eax
 8ef:	c1 e0 03             	shl    $0x3,%eax
 8f2:	01 45 f4             	add    %eax,-0xc(%ebp)
        p->s.size = nunits;
 8f5:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8f8:	8b 55 ec             	mov    -0x14(%ebp),%edx
 8fb:	89 50 04             	mov    %edx,0x4(%eax)
      }
      freep = prevp;
 8fe:	8b 45 f0             	mov    -0x10(%ebp),%eax
 901:	a3 dc 0b 00 00       	mov    %eax,0xbdc
      return (void*)(p + 1);
 906:	8b 45 f4             	mov    -0xc(%ebp),%eax
 909:	83 c0 08             	add    $0x8,%eax
 90c:	eb 3b                	jmp    949 <malloc+0xe1>
    }
    if(p == freep)
 90e:	a1 dc 0b 00 00       	mov    0xbdc,%eax
 913:	39 45 f4             	cmp    %eax,-0xc(%ebp)
 916:	75 1e                	jne    936 <malloc+0xce>
      if((p = morecore(nunits)) == 0)
 918:	83 ec 0c             	sub    $0xc,%esp
 91b:	ff 75 ec             	pushl  -0x14(%ebp)
 91e:	e8 e5 fe ff ff       	call   808 <morecore>
 923:	83 c4 10             	add    $0x10,%esp
 926:	89 45 f4             	mov    %eax,-0xc(%ebp)
 929:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 92d:	75 07                	jne    936 <malloc+0xce>
        return 0;
 92f:	b8 00 00 00 00       	mov    $0x0,%eax
 934:	eb 13                	jmp    949 <malloc+0xe1>
  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
  if((prevp = freep) == 0){
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 936:	8b 45 f4             	mov    -0xc(%ebp),%eax
 939:	89 45 f0             	mov    %eax,-0x10(%ebp)
 93c:	8b 45 f4             	mov    -0xc(%ebp),%eax
 93f:	8b 00                	mov    (%eax),%eax
 941:	89 45 f4             	mov    %eax,-0xc(%ebp)
      return (void*)(p + 1);
    }
    if(p == freep)
      if((p = morecore(nunits)) == 0)
        return 0;
  }
 944:	e9 6d ff ff ff       	jmp    8b6 <malloc+0x4e>
}
 949:	c9                   	leave  
 94a:	c3                   	ret    
