
_ps:     file format elf32-i386


Disassembly of section .text:

00000000 <main>:
#include "types.h"
#include "user.h"

int 
main(int argc, char * argv[])
{
   0:	8d 4c 24 04          	lea    0x4(%esp),%ecx
   4:	83 e4 f0             	and    $0xfffffff0,%esp
   7:	ff 71 fc             	pushl  -0x4(%ecx)
   a:	55                   	push   %ebp
   b:	89 e5                	mov    %esp,%ebp
   d:	57                   	push   %edi
   e:	56                   	push   %esi
   f:	53                   	push   %ebx
  10:	51                   	push   %ecx
  11:	83 ec 38             	sub    $0x38,%esp
  14:	89 c8                	mov    %ecx,%eax
  static int MAX = 16;		//default value of MAX
  int size;
  struct uproc * table;
  
  if(argc > 1 && argv[1] > 0)
  16:	83 38 01             	cmpl   $0x1,(%eax)
  19:	7e 25                	jle    40 <main+0x40>
  1b:	8b 50 04             	mov    0x4(%eax),%edx
  1e:	83 c2 04             	add    $0x4,%edx
  21:	8b 12                	mov    (%edx),%edx
  23:	85 d2                	test   %edx,%edx
  25:	74 19                	je     40 <main+0x40>
    MAX = atoi(argv[1]);	//user defined value for the MAX parameter
  27:	8b 40 04             	mov    0x4(%eax),%eax
  2a:	83 c0 04             	add    $0x4,%eax
  2d:	8b 00                	mov    (%eax),%eax
  2f:	83 ec 0c             	sub    $0xc,%esp
  32:	50                   	push   %eax
  33:	e8 58 03 00 00       	call   390 <atoi>
  38:	83 c4 10             	add    $0x10,%esp
  3b:	a3 cc 0c 00 00       	mov    %eax,0xccc
  printf(1, "MAX = %d\n", MAX);
  40:	a1 cc 0c 00 00       	mov    0xccc,%eax
  45:	83 ec 04             	sub    $0x4,%esp
  48:	50                   	push   %eax
  49:	68 c4 09 00 00       	push   $0x9c4
  4e:	6a 01                	push   $0x1
  50:	e8 b9 05 00 00       	call   60e <printf>
  55:	83 c4 10             	add    $0x10,%esp
  table = malloc(MAX * sizeof(struct uproc));
  58:	a1 cc 0c 00 00       	mov    0xccc,%eax
  5d:	6b c0 64             	imul   $0x64,%eax,%eax
  60:	83 ec 0c             	sub    $0xc,%esp
  63:	50                   	push   %eax
  64:	e8 78 08 00 00       	call   8e1 <malloc>
  69:	83 c4 10             	add    $0x10,%esp
  6c:	89 45 e0             	mov    %eax,-0x20(%ebp)
  if(!table)
  6f:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
  73:	75 1b                	jne    90 <main+0x90>
  {
    printf(2, "Error: malloc call failed. %s at line %d\n", "ps.c", 12);
  75:	6a 0c                	push   $0xc
  77:	68 ce 09 00 00       	push   $0x9ce
  7c:	68 d4 09 00 00       	push   $0x9d4
  81:	6a 02                	push   $0x2
  83:	e8 86 05 00 00       	call   60e <printf>
  88:	83 c4 10             	add    $0x10,%esp
    exit(); 
  8b:	e8 bf 03 00 00       	call   44f <exit>
  } 
  size = getprocs(MAX, table);
  90:	a1 cc 0c 00 00       	mov    0xccc,%eax
  95:	83 ec 08             	sub    $0x8,%esp
  98:	ff 75 e0             	pushl  -0x20(%ebp)
  9b:	50                   	push   %eax
  9c:	e8 86 04 00 00       	call   527 <getprocs>
  a1:	83 c4 10             	add    $0x10,%esp
  a4:	89 45 dc             	mov    %eax,-0x24(%ebp)
  if(size == 0){
  a7:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  ab:	75 17                	jne    c4 <main+0xc4>
    printf(2, "Get table for uproc failed.\n");
  ad:	83 ec 08             	sub    $0x8,%esp
  b0:	68 fe 09 00 00       	push   $0x9fe
  b5:	6a 02                	push   $0x2
  b7:	e8 52 05 00 00       	call   60e <printf>
  bc:	83 c4 10             	add    $0x10,%esp
    exit();
  bf:	e8 8b 03 00 00       	call   44f <exit>
  }
  printf(1, "PID\tName\tUID\tGID\tPPID\tElapsed\tCPU\tState\tSize\n");
  c4:	83 ec 08             	sub    $0x8,%esp
  c7:	68 1c 0a 00 00       	push   $0xa1c
  cc:	6a 01                	push   $0x1
  ce:	e8 3b 05 00 00       	call   60e <printf>
  d3:	83 c4 10             	add    $0x10,%esp
  int x;
  for(x = 0; x < size; ++x){
  d6:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
  dd:	e9 d8 00 00 00       	jmp    1ba <main+0x1ba>
    printf(1,"%d\t%s\t%d\t%d\t%d\t%d.%d\t%d.%d\t%s\t%d\n", table[x].pid, table[x].name, table[x].uid, table[x].gid, table[x].ppid,
      table[x].elapsed_ticks, table[x].elapsed_ticks1, table[x].CPU_total_ticks, table[x].CPU_total_ticks1, table[x].state, table[x].size);
  e2:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  e5:	6b d0 64             	imul   $0x64,%eax,%edx
  e8:	8b 45 e0             	mov    -0x20(%ebp),%eax
  eb:	01 d0                	add    %edx,%eax
    exit();
  }
  printf(1, "PID\tName\tUID\tGID\tPPID\tElapsed\tCPU\tState\tSize\n");
  int x;
  for(x = 0; x < size; ++x){
    printf(1,"%d\t%s\t%d\t%d\t%d\t%d.%d\t%d.%d\t%s\t%d\n", table[x].pid, table[x].name, table[x].uid, table[x].gid, table[x].ppid,
  ed:	8b 40 40             	mov    0x40(%eax),%eax
  f0:	89 45 d4             	mov    %eax,-0x2c(%ebp)
      table[x].elapsed_ticks, table[x].elapsed_ticks1, table[x].CPU_total_ticks, table[x].CPU_total_ticks1, table[x].state, table[x].size);
  f3:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  f6:	6b d0 64             	imul   $0x64,%eax,%edx
  f9:	8b 45 e0             	mov    -0x20(%ebp),%eax
  fc:	01 d0                	add    %edx,%eax
  fe:	8d 58 20             	lea    0x20(%eax),%ebx
 101:	89 5d d0             	mov    %ebx,-0x30(%ebp)
 104:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 107:	6b d0 64             	imul   $0x64,%eax,%edx
 10a:	8b 45 e0             	mov    -0x20(%ebp),%eax
 10d:	01 d0                	add    %edx,%eax
    exit();
  }
  printf(1, "PID\tName\tUID\tGID\tPPID\tElapsed\tCPU\tState\tSize\n");
  int x;
  for(x = 0; x < size; ++x){
    printf(1,"%d\t%s\t%d\t%d\t%d\t%d.%d\t%d.%d\t%s\t%d\n", table[x].pid, table[x].name, table[x].uid, table[x].gid, table[x].ppid,
 10f:	8b 70 1c             	mov    0x1c(%eax),%esi
 112:	89 75 cc             	mov    %esi,-0x34(%ebp)
      table[x].elapsed_ticks, table[x].elapsed_ticks1, table[x].CPU_total_ticks, table[x].CPU_total_ticks1, table[x].state, table[x].size);
 115:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 118:	6b d0 64             	imul   $0x64,%eax,%edx
 11b:	8b 45 e0             	mov    -0x20(%ebp),%eax
 11e:	01 d0                	add    %edx,%eax
    exit();
  }
  printf(1, "PID\tName\tUID\tGID\tPPID\tElapsed\tCPU\tState\tSize\n");
  int x;
  for(x = 0; x < size; ++x){
    printf(1,"%d\t%s\t%d\t%d\t%d\t%d.%d\t%d.%d\t%s\t%d\n", table[x].pid, table[x].name, table[x].uid, table[x].gid, table[x].ppid,
 120:	8b 78 18             	mov    0x18(%eax),%edi
 123:	89 7d c8             	mov    %edi,-0x38(%ebp)
      table[x].elapsed_ticks, table[x].elapsed_ticks1, table[x].CPU_total_ticks, table[x].CPU_total_ticks1, table[x].state, table[x].size);
 126:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 129:	6b d0 64             	imul   $0x64,%eax,%edx
 12c:	8b 45 e0             	mov    -0x20(%ebp),%eax
 12f:	01 d0                	add    %edx,%eax
    exit();
  }
  printf(1, "PID\tName\tUID\tGID\tPPID\tElapsed\tCPU\tState\tSize\n");
  int x;
  for(x = 0; x < size; ++x){
    printf(1,"%d\t%s\t%d\t%d\t%d\t%d.%d\t%d.%d\t%s\t%d\n", table[x].pid, table[x].name, table[x].uid, table[x].gid, table[x].ppid,
 131:	8b 48 14             	mov    0x14(%eax),%ecx
 134:	89 4d c4             	mov    %ecx,-0x3c(%ebp)
      table[x].elapsed_ticks, table[x].elapsed_ticks1, table[x].CPU_total_ticks, table[x].CPU_total_ticks1, table[x].state, table[x].size);
 137:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 13a:	6b d0 64             	imul   $0x64,%eax,%edx
 13d:	8b 45 e0             	mov    -0x20(%ebp),%eax
 140:	01 d0                	add    %edx,%eax
    exit();
  }
  printf(1, "PID\tName\tUID\tGID\tPPID\tElapsed\tCPU\tState\tSize\n");
  int x;
  for(x = 0; x < size; ++x){
    printf(1,"%d\t%s\t%d\t%d\t%d\t%d.%d\t%d.%d\t%s\t%d\n", table[x].pid, table[x].name, table[x].uid, table[x].gid, table[x].ppid,
 142:	8b 50 10             	mov    0x10(%eax),%edx
 145:	89 55 c0             	mov    %edx,-0x40(%ebp)
 148:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 14b:	6b d0 64             	imul   $0x64,%eax,%edx
 14e:	8b 45 e0             	mov    -0x20(%ebp),%eax
 151:	01 d0                	add    %edx,%eax
 153:	8b 78 0c             	mov    0xc(%eax),%edi
 156:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 159:	6b d0 64             	imul   $0x64,%eax,%edx
 15c:	8b 45 e0             	mov    -0x20(%ebp),%eax
 15f:	01 d0                	add    %edx,%eax
 161:	8b 70 08             	mov    0x8(%eax),%esi
 164:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 167:	6b d0 64             	imul   $0x64,%eax,%edx
 16a:	8b 45 e0             	mov    -0x20(%ebp),%eax
 16d:	01 d0                	add    %edx,%eax
 16f:	8b 58 04             	mov    0x4(%eax),%ebx
 172:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 175:	6b d0 64             	imul   $0x64,%eax,%edx
 178:	8b 45 e0             	mov    -0x20(%ebp),%eax
 17b:	01 d0                	add    %edx,%eax
 17d:	8d 48 44             	lea    0x44(%eax),%ecx
 180:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 183:	6b d0 64             	imul   $0x64,%eax,%edx
 186:	8b 45 e0             	mov    -0x20(%ebp),%eax
 189:	01 d0                	add    %edx,%eax
 18b:	8b 00                	mov    (%eax),%eax
 18d:	83 ec 0c             	sub    $0xc,%esp
 190:	ff 75 d4             	pushl  -0x2c(%ebp)
 193:	ff 75 d0             	pushl  -0x30(%ebp)
 196:	ff 75 cc             	pushl  -0x34(%ebp)
 199:	ff 75 c8             	pushl  -0x38(%ebp)
 19c:	ff 75 c4             	pushl  -0x3c(%ebp)
 19f:	ff 75 c0             	pushl  -0x40(%ebp)
 1a2:	57                   	push   %edi
 1a3:	56                   	push   %esi
 1a4:	53                   	push   %ebx
 1a5:	51                   	push   %ecx
 1a6:	50                   	push   %eax
 1a7:	68 4c 0a 00 00       	push   $0xa4c
 1ac:	6a 01                	push   $0x1
 1ae:	e8 5b 04 00 00       	call   60e <printf>
 1b3:	83 c4 40             	add    $0x40,%esp
    printf(2, "Get table for uproc failed.\n");
    exit();
  }
  printf(1, "PID\tName\tUID\tGID\tPPID\tElapsed\tCPU\tState\tSize\n");
  int x;
  for(x = 0; x < size; ++x){
 1b6:	83 45 e4 01          	addl   $0x1,-0x1c(%ebp)
 1ba:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 1bd:	3b 45 dc             	cmp    -0x24(%ebp),%eax
 1c0:	0f 8c 1c ff ff ff    	jl     e2 <main+0xe2>
    printf(1,"%d\t%s\t%d\t%d\t%d\t%d.%d\t%d.%d\t%s\t%d\n", table[x].pid, table[x].name, table[x].uid, table[x].gid, table[x].ppid,
      table[x].elapsed_ticks, table[x].elapsed_ticks1, table[x].CPU_total_ticks, table[x].CPU_total_ticks1, table[x].state, table[x].size);
  }
  exit();
 1c6:	e8 84 02 00 00       	call   44f <exit>

000001cb <stosb>:
               "cc");
}

static inline void
stosb(void *addr, int data, int cnt)
{
 1cb:	55                   	push   %ebp
 1cc:	89 e5                	mov    %esp,%ebp
 1ce:	57                   	push   %edi
 1cf:	53                   	push   %ebx
  asm volatile("cld; rep stosb" :
 1d0:	8b 4d 08             	mov    0x8(%ebp),%ecx
 1d3:	8b 55 10             	mov    0x10(%ebp),%edx
 1d6:	8b 45 0c             	mov    0xc(%ebp),%eax
 1d9:	89 cb                	mov    %ecx,%ebx
 1db:	89 df                	mov    %ebx,%edi
 1dd:	89 d1                	mov    %edx,%ecx
 1df:	fc                   	cld    
 1e0:	f3 aa                	rep stos %al,%es:(%edi)
 1e2:	89 ca                	mov    %ecx,%edx
 1e4:	89 fb                	mov    %edi,%ebx
 1e6:	89 5d 08             	mov    %ebx,0x8(%ebp)
 1e9:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
 1ec:	90                   	nop
 1ed:	5b                   	pop    %ebx
 1ee:	5f                   	pop    %edi
 1ef:	5d                   	pop    %ebp
 1f0:	c3                   	ret    

000001f1 <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, char *t)
{
 1f1:	55                   	push   %ebp
 1f2:	89 e5                	mov    %esp,%ebp
 1f4:	83 ec 10             	sub    $0x10,%esp
  char *os;

  os = s;
 1f7:	8b 45 08             	mov    0x8(%ebp),%eax
 1fa:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while((*s++ = *t++) != 0)
 1fd:	90                   	nop
 1fe:	8b 45 08             	mov    0x8(%ebp),%eax
 201:	8d 50 01             	lea    0x1(%eax),%edx
 204:	89 55 08             	mov    %edx,0x8(%ebp)
 207:	8b 55 0c             	mov    0xc(%ebp),%edx
 20a:	8d 4a 01             	lea    0x1(%edx),%ecx
 20d:	89 4d 0c             	mov    %ecx,0xc(%ebp)
 210:	0f b6 12             	movzbl (%edx),%edx
 213:	88 10                	mov    %dl,(%eax)
 215:	0f b6 00             	movzbl (%eax),%eax
 218:	84 c0                	test   %al,%al
 21a:	75 e2                	jne    1fe <strcpy+0xd>
    ;
  return os;
 21c:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 21f:	c9                   	leave  
 220:	c3                   	ret    

00000221 <strcmp>:

int
strcmp(const char *p, const char *q)
{
 221:	55                   	push   %ebp
 222:	89 e5                	mov    %esp,%ebp
  while(*p && *p == *q)
 224:	eb 08                	jmp    22e <strcmp+0xd>
    p++, q++;
 226:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 22a:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
  while(*p && *p == *q)
 22e:	8b 45 08             	mov    0x8(%ebp),%eax
 231:	0f b6 00             	movzbl (%eax),%eax
 234:	84 c0                	test   %al,%al
 236:	74 10                	je     248 <strcmp+0x27>
 238:	8b 45 08             	mov    0x8(%ebp),%eax
 23b:	0f b6 10             	movzbl (%eax),%edx
 23e:	8b 45 0c             	mov    0xc(%ebp),%eax
 241:	0f b6 00             	movzbl (%eax),%eax
 244:	38 c2                	cmp    %al,%dl
 246:	74 de                	je     226 <strcmp+0x5>
    p++, q++;
  return (uchar)*p - (uchar)*q;
 248:	8b 45 08             	mov    0x8(%ebp),%eax
 24b:	0f b6 00             	movzbl (%eax),%eax
 24e:	0f b6 d0             	movzbl %al,%edx
 251:	8b 45 0c             	mov    0xc(%ebp),%eax
 254:	0f b6 00             	movzbl (%eax),%eax
 257:	0f b6 c0             	movzbl %al,%eax
 25a:	29 c2                	sub    %eax,%edx
 25c:	89 d0                	mov    %edx,%eax
}
 25e:	5d                   	pop    %ebp
 25f:	c3                   	ret    

00000260 <strlen>:

uint
strlen(char *s)
{
 260:	55                   	push   %ebp
 261:	89 e5                	mov    %esp,%ebp
 263:	83 ec 10             	sub    $0x10,%esp
  int n;

  for(n = 0; s[n]; n++)
 266:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
 26d:	eb 04                	jmp    273 <strlen+0x13>
 26f:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
 273:	8b 55 fc             	mov    -0x4(%ebp),%edx
 276:	8b 45 08             	mov    0x8(%ebp),%eax
 279:	01 d0                	add    %edx,%eax
 27b:	0f b6 00             	movzbl (%eax),%eax
 27e:	84 c0                	test   %al,%al
 280:	75 ed                	jne    26f <strlen+0xf>
    ;
  return n;
 282:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 285:	c9                   	leave  
 286:	c3                   	ret    

00000287 <memset>:

void*
memset(void *dst, int c, uint n)
{
 287:	55                   	push   %ebp
 288:	89 e5                	mov    %esp,%ebp
  stosb(dst, c, n);
 28a:	8b 45 10             	mov    0x10(%ebp),%eax
 28d:	50                   	push   %eax
 28e:	ff 75 0c             	pushl  0xc(%ebp)
 291:	ff 75 08             	pushl  0x8(%ebp)
 294:	e8 32 ff ff ff       	call   1cb <stosb>
 299:	83 c4 0c             	add    $0xc,%esp
  return dst;
 29c:	8b 45 08             	mov    0x8(%ebp),%eax
}
 29f:	c9                   	leave  
 2a0:	c3                   	ret    

000002a1 <strchr>:

char*
strchr(const char *s, char c)
{
 2a1:	55                   	push   %ebp
 2a2:	89 e5                	mov    %esp,%ebp
 2a4:	83 ec 04             	sub    $0x4,%esp
 2a7:	8b 45 0c             	mov    0xc(%ebp),%eax
 2aa:	88 45 fc             	mov    %al,-0x4(%ebp)
  for(; *s; s++)
 2ad:	eb 14                	jmp    2c3 <strchr+0x22>
    if(*s == c)
 2af:	8b 45 08             	mov    0x8(%ebp),%eax
 2b2:	0f b6 00             	movzbl (%eax),%eax
 2b5:	3a 45 fc             	cmp    -0x4(%ebp),%al
 2b8:	75 05                	jne    2bf <strchr+0x1e>
      return (char*)s;
 2ba:	8b 45 08             	mov    0x8(%ebp),%eax
 2bd:	eb 13                	jmp    2d2 <strchr+0x31>
}

char*
strchr(const char *s, char c)
{
  for(; *s; s++)
 2bf:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 2c3:	8b 45 08             	mov    0x8(%ebp),%eax
 2c6:	0f b6 00             	movzbl (%eax),%eax
 2c9:	84 c0                	test   %al,%al
 2cb:	75 e2                	jne    2af <strchr+0xe>
    if(*s == c)
      return (char*)s;
  return 0;
 2cd:	b8 00 00 00 00       	mov    $0x0,%eax
}
 2d2:	c9                   	leave  
 2d3:	c3                   	ret    

000002d4 <gets>:

char*
gets(char *buf, int max)
{
 2d4:	55                   	push   %ebp
 2d5:	89 e5                	mov    %esp,%ebp
 2d7:	83 ec 18             	sub    $0x18,%esp
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 2da:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 2e1:	eb 42                	jmp    325 <gets+0x51>
    cc = read(0, &c, 1);
 2e3:	83 ec 04             	sub    $0x4,%esp
 2e6:	6a 01                	push   $0x1
 2e8:	8d 45 ef             	lea    -0x11(%ebp),%eax
 2eb:	50                   	push   %eax
 2ec:	6a 00                	push   $0x0
 2ee:	e8 74 01 00 00       	call   467 <read>
 2f3:	83 c4 10             	add    $0x10,%esp
 2f6:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if(cc < 1)
 2f9:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 2fd:	7e 33                	jle    332 <gets+0x5e>
      break;
    buf[i++] = c;
 2ff:	8b 45 f4             	mov    -0xc(%ebp),%eax
 302:	8d 50 01             	lea    0x1(%eax),%edx
 305:	89 55 f4             	mov    %edx,-0xc(%ebp)
 308:	89 c2                	mov    %eax,%edx
 30a:	8b 45 08             	mov    0x8(%ebp),%eax
 30d:	01 c2                	add    %eax,%edx
 30f:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 313:	88 02                	mov    %al,(%edx)
    if(c == '\n' || c == '\r')
 315:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 319:	3c 0a                	cmp    $0xa,%al
 31b:	74 16                	je     333 <gets+0x5f>
 31d:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 321:	3c 0d                	cmp    $0xd,%al
 323:	74 0e                	je     333 <gets+0x5f>
gets(char *buf, int max)
{
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 325:	8b 45 f4             	mov    -0xc(%ebp),%eax
 328:	83 c0 01             	add    $0x1,%eax
 32b:	3b 45 0c             	cmp    0xc(%ebp),%eax
 32e:	7c b3                	jl     2e3 <gets+0xf>
 330:	eb 01                	jmp    333 <gets+0x5f>
    cc = read(0, &c, 1);
    if(cc < 1)
      break;
 332:	90                   	nop
    buf[i++] = c;
    if(c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 333:	8b 55 f4             	mov    -0xc(%ebp),%edx
 336:	8b 45 08             	mov    0x8(%ebp),%eax
 339:	01 d0                	add    %edx,%eax
 33b:	c6 00 00             	movb   $0x0,(%eax)
  return buf;
 33e:	8b 45 08             	mov    0x8(%ebp),%eax
}
 341:	c9                   	leave  
 342:	c3                   	ret    

00000343 <stat>:

int
stat(char *n, struct stat *st)
{
 343:	55                   	push   %ebp
 344:	89 e5                	mov    %esp,%ebp
 346:	83 ec 18             	sub    $0x18,%esp
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 349:	83 ec 08             	sub    $0x8,%esp
 34c:	6a 00                	push   $0x0
 34e:	ff 75 08             	pushl  0x8(%ebp)
 351:	e8 39 01 00 00       	call   48f <open>
 356:	83 c4 10             	add    $0x10,%esp
 359:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(fd < 0)
 35c:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 360:	79 07                	jns    369 <stat+0x26>
    return -1;
 362:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 367:	eb 25                	jmp    38e <stat+0x4b>
  r = fstat(fd, st);
 369:	83 ec 08             	sub    $0x8,%esp
 36c:	ff 75 0c             	pushl  0xc(%ebp)
 36f:	ff 75 f4             	pushl  -0xc(%ebp)
 372:	e8 30 01 00 00       	call   4a7 <fstat>
 377:	83 c4 10             	add    $0x10,%esp
 37a:	89 45 f0             	mov    %eax,-0x10(%ebp)
  close(fd);
 37d:	83 ec 0c             	sub    $0xc,%esp
 380:	ff 75 f4             	pushl  -0xc(%ebp)
 383:	e8 ef 00 00 00       	call   477 <close>
 388:	83 c4 10             	add    $0x10,%esp
  return r;
 38b:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
 38e:	c9                   	leave  
 38f:	c3                   	ret    

00000390 <atoi>:

int
atoi(const char *s)
{
 390:	55                   	push   %ebp
 391:	89 e5                	mov    %esp,%ebp
 393:	83 ec 10             	sub    $0x10,%esp
  int n, sign;

  n = 0;
 396:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  while(*s == ' ') s++;
 39d:	eb 04                	jmp    3a3 <atoi+0x13>
 39f:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 3a3:	8b 45 08             	mov    0x8(%ebp),%eax
 3a6:	0f b6 00             	movzbl (%eax),%eax
 3a9:	3c 20                	cmp    $0x20,%al
 3ab:	74 f2                	je     39f <atoi+0xf>
  sign = (*s == '-') ? -1 : 1;
 3ad:	8b 45 08             	mov    0x8(%ebp),%eax
 3b0:	0f b6 00             	movzbl (%eax),%eax
 3b3:	3c 2d                	cmp    $0x2d,%al
 3b5:	75 07                	jne    3be <atoi+0x2e>
 3b7:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 3bc:	eb 05                	jmp    3c3 <atoi+0x33>
 3be:	b8 01 00 00 00       	mov    $0x1,%eax
 3c3:	89 45 f8             	mov    %eax,-0x8(%ebp)
  while('0' <= *s && *s <= '9')
 3c6:	eb 25                	jmp    3ed <atoi+0x5d>
    n = n*10 + *s++ - '0';
 3c8:	8b 55 fc             	mov    -0x4(%ebp),%edx
 3cb:	89 d0                	mov    %edx,%eax
 3cd:	c1 e0 02             	shl    $0x2,%eax
 3d0:	01 d0                	add    %edx,%eax
 3d2:	01 c0                	add    %eax,%eax
 3d4:	89 c1                	mov    %eax,%ecx
 3d6:	8b 45 08             	mov    0x8(%ebp),%eax
 3d9:	8d 50 01             	lea    0x1(%eax),%edx
 3dc:	89 55 08             	mov    %edx,0x8(%ebp)
 3df:	0f b6 00             	movzbl (%eax),%eax
 3e2:	0f be c0             	movsbl %al,%eax
 3e5:	01 c8                	add    %ecx,%eax
 3e7:	83 e8 30             	sub    $0x30,%eax
 3ea:	89 45 fc             	mov    %eax,-0x4(%ebp)
  int n, sign;

  n = 0;
  while(*s == ' ') s++;
  sign = (*s == '-') ? -1 : 1;
  while('0' <= *s && *s <= '9')
 3ed:	8b 45 08             	mov    0x8(%ebp),%eax
 3f0:	0f b6 00             	movzbl (%eax),%eax
 3f3:	3c 2f                	cmp    $0x2f,%al
 3f5:	7e 0a                	jle    401 <atoi+0x71>
 3f7:	8b 45 08             	mov    0x8(%ebp),%eax
 3fa:	0f b6 00             	movzbl (%eax),%eax
 3fd:	3c 39                	cmp    $0x39,%al
 3ff:	7e c7                	jle    3c8 <atoi+0x38>
    n = n*10 + *s++ - '0';
  return sign*n;
 401:	8b 45 f8             	mov    -0x8(%ebp),%eax
 404:	0f af 45 fc          	imul   -0x4(%ebp),%eax
}
 408:	c9                   	leave  
 409:	c3                   	ret    

0000040a <memmove>:

void*
memmove(void *vdst, void *vsrc, int n)
{
 40a:	55                   	push   %ebp
 40b:	89 e5                	mov    %esp,%ebp
 40d:	83 ec 10             	sub    $0x10,%esp
  char *dst, *src;
  
  dst = vdst;
 410:	8b 45 08             	mov    0x8(%ebp),%eax
 413:	89 45 fc             	mov    %eax,-0x4(%ebp)
  src = vsrc;
 416:	8b 45 0c             	mov    0xc(%ebp),%eax
 419:	89 45 f8             	mov    %eax,-0x8(%ebp)
  while(n-- > 0)
 41c:	eb 17                	jmp    435 <memmove+0x2b>
    *dst++ = *src++;
 41e:	8b 45 fc             	mov    -0x4(%ebp),%eax
 421:	8d 50 01             	lea    0x1(%eax),%edx
 424:	89 55 fc             	mov    %edx,-0x4(%ebp)
 427:	8b 55 f8             	mov    -0x8(%ebp),%edx
 42a:	8d 4a 01             	lea    0x1(%edx),%ecx
 42d:	89 4d f8             	mov    %ecx,-0x8(%ebp)
 430:	0f b6 12             	movzbl (%edx),%edx
 433:	88 10                	mov    %dl,(%eax)
{
  char *dst, *src;
  
  dst = vdst;
  src = vsrc;
  while(n-- > 0)
 435:	8b 45 10             	mov    0x10(%ebp),%eax
 438:	8d 50 ff             	lea    -0x1(%eax),%edx
 43b:	89 55 10             	mov    %edx,0x10(%ebp)
 43e:	85 c0                	test   %eax,%eax
 440:	7f dc                	jg     41e <memmove+0x14>
    *dst++ = *src++;
  return vdst;
 442:	8b 45 08             	mov    0x8(%ebp),%eax
}
 445:	c9                   	leave  
 446:	c3                   	ret    

00000447 <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
 447:	b8 01 00 00 00       	mov    $0x1,%eax
 44c:	cd 40                	int    $0x40
 44e:	c3                   	ret    

0000044f <exit>:
SYSCALL(exit)
 44f:	b8 02 00 00 00       	mov    $0x2,%eax
 454:	cd 40                	int    $0x40
 456:	c3                   	ret    

00000457 <wait>:
SYSCALL(wait)
 457:	b8 03 00 00 00       	mov    $0x3,%eax
 45c:	cd 40                	int    $0x40
 45e:	c3                   	ret    

0000045f <pipe>:
SYSCALL(pipe)
 45f:	b8 04 00 00 00       	mov    $0x4,%eax
 464:	cd 40                	int    $0x40
 466:	c3                   	ret    

00000467 <read>:
SYSCALL(read)
 467:	b8 05 00 00 00       	mov    $0x5,%eax
 46c:	cd 40                	int    $0x40
 46e:	c3                   	ret    

0000046f <write>:
SYSCALL(write)
 46f:	b8 10 00 00 00       	mov    $0x10,%eax
 474:	cd 40                	int    $0x40
 476:	c3                   	ret    

00000477 <close>:
SYSCALL(close)
 477:	b8 15 00 00 00       	mov    $0x15,%eax
 47c:	cd 40                	int    $0x40
 47e:	c3                   	ret    

0000047f <kill>:
SYSCALL(kill)
 47f:	b8 06 00 00 00       	mov    $0x6,%eax
 484:	cd 40                	int    $0x40
 486:	c3                   	ret    

00000487 <exec>:
SYSCALL(exec)
 487:	b8 07 00 00 00       	mov    $0x7,%eax
 48c:	cd 40                	int    $0x40
 48e:	c3                   	ret    

0000048f <open>:
SYSCALL(open)
 48f:	b8 0f 00 00 00       	mov    $0xf,%eax
 494:	cd 40                	int    $0x40
 496:	c3                   	ret    

00000497 <mknod>:
SYSCALL(mknod)
 497:	b8 11 00 00 00       	mov    $0x11,%eax
 49c:	cd 40                	int    $0x40
 49e:	c3                   	ret    

0000049f <unlink>:
SYSCALL(unlink)
 49f:	b8 12 00 00 00       	mov    $0x12,%eax
 4a4:	cd 40                	int    $0x40
 4a6:	c3                   	ret    

000004a7 <fstat>:
SYSCALL(fstat)
 4a7:	b8 08 00 00 00       	mov    $0x8,%eax
 4ac:	cd 40                	int    $0x40
 4ae:	c3                   	ret    

000004af <link>:
SYSCALL(link)
 4af:	b8 13 00 00 00       	mov    $0x13,%eax
 4b4:	cd 40                	int    $0x40
 4b6:	c3                   	ret    

000004b7 <mkdir>:
SYSCALL(mkdir)
 4b7:	b8 14 00 00 00       	mov    $0x14,%eax
 4bc:	cd 40                	int    $0x40
 4be:	c3                   	ret    

000004bf <chdir>:
SYSCALL(chdir)
 4bf:	b8 09 00 00 00       	mov    $0x9,%eax
 4c4:	cd 40                	int    $0x40
 4c6:	c3                   	ret    

000004c7 <dup>:
SYSCALL(dup)
 4c7:	b8 0a 00 00 00       	mov    $0xa,%eax
 4cc:	cd 40                	int    $0x40
 4ce:	c3                   	ret    

000004cf <getpid>:
SYSCALL(getpid)
 4cf:	b8 0b 00 00 00       	mov    $0xb,%eax
 4d4:	cd 40                	int    $0x40
 4d6:	c3                   	ret    

000004d7 <sbrk>:
SYSCALL(sbrk)
 4d7:	b8 0c 00 00 00       	mov    $0xc,%eax
 4dc:	cd 40                	int    $0x40
 4de:	c3                   	ret    

000004df <sleep>:
SYSCALL(sleep)
 4df:	b8 0d 00 00 00       	mov    $0xd,%eax
 4e4:	cd 40                	int    $0x40
 4e6:	c3                   	ret    

000004e7 <uptime>:
SYSCALL(uptime)
 4e7:	b8 0e 00 00 00       	mov    $0xe,%eax
 4ec:	cd 40                	int    $0x40
 4ee:	c3                   	ret    

000004ef <halt>:
SYSCALL(halt)
 4ef:	b8 16 00 00 00       	mov    $0x16,%eax
 4f4:	cd 40                	int    $0x40
 4f6:	c3                   	ret    

000004f7 <date>:
SYSCALL(date)    #added the date system call
 4f7:	b8 17 00 00 00       	mov    $0x17,%eax
 4fc:	cd 40                	int    $0x40
 4fe:	c3                   	ret    

000004ff <getuid>:
SYSCALL(getuid)
 4ff:	b8 18 00 00 00       	mov    $0x18,%eax
 504:	cd 40                	int    $0x40
 506:	c3                   	ret    

00000507 <getgid>:
SYSCALL(getgid)
 507:	b8 19 00 00 00       	mov    $0x19,%eax
 50c:	cd 40                	int    $0x40
 50e:	c3                   	ret    

0000050f <getppid>:
SYSCALL(getppid)
 50f:	b8 1a 00 00 00       	mov    $0x1a,%eax
 514:	cd 40                	int    $0x40
 516:	c3                   	ret    

00000517 <setuid>:
SYSCALL(setuid)
 517:	b8 1b 00 00 00       	mov    $0x1b,%eax
 51c:	cd 40                	int    $0x40
 51e:	c3                   	ret    

0000051f <setgid>:
SYSCALL(setgid)
 51f:	b8 1c 00 00 00       	mov    $0x1c,%eax
 524:	cd 40                	int    $0x40
 526:	c3                   	ret    

00000527 <getprocs>:
SYSCALL(getprocs)
 527:	b8 1d 00 00 00       	mov    $0x1d,%eax
 52c:	cd 40                	int    $0x40
 52e:	c3                   	ret    

0000052f <setpriority>:
SYSCALL(setpriority)
 52f:	b8 1e 00 00 00       	mov    $0x1e,%eax
 534:	cd 40                	int    $0x40
 536:	c3                   	ret    

00000537 <putc>:
#include "stat.h"
#include "user.h"

static void
putc(int fd, char c)
{
 537:	55                   	push   %ebp
 538:	89 e5                	mov    %esp,%ebp
 53a:	83 ec 18             	sub    $0x18,%esp
 53d:	8b 45 0c             	mov    0xc(%ebp),%eax
 540:	88 45 f4             	mov    %al,-0xc(%ebp)
  write(fd, &c, 1);
 543:	83 ec 04             	sub    $0x4,%esp
 546:	6a 01                	push   $0x1
 548:	8d 45 f4             	lea    -0xc(%ebp),%eax
 54b:	50                   	push   %eax
 54c:	ff 75 08             	pushl  0x8(%ebp)
 54f:	e8 1b ff ff ff       	call   46f <write>
 554:	83 c4 10             	add    $0x10,%esp
}
 557:	90                   	nop
 558:	c9                   	leave  
 559:	c3                   	ret    

0000055a <printint>:

static void
printint(int fd, int xx, int base, int sgn)
{
 55a:	55                   	push   %ebp
 55b:	89 e5                	mov    %esp,%ebp
 55d:	53                   	push   %ebx
 55e:	83 ec 24             	sub    $0x24,%esp
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 561:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  if(sgn && xx < 0){
 568:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
 56c:	74 17                	je     585 <printint+0x2b>
 56e:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
 572:	79 11                	jns    585 <printint+0x2b>
    neg = 1;
 574:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
    x = -xx;
 57b:	8b 45 0c             	mov    0xc(%ebp),%eax
 57e:	f7 d8                	neg    %eax
 580:	89 45 ec             	mov    %eax,-0x14(%ebp)
 583:	eb 06                	jmp    58b <printint+0x31>
  } else {
    x = xx;
 585:	8b 45 0c             	mov    0xc(%ebp),%eax
 588:	89 45 ec             	mov    %eax,-0x14(%ebp)
  }

  i = 0;
 58b:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  do{
    buf[i++] = digits[x % base];
 592:	8b 4d f4             	mov    -0xc(%ebp),%ecx
 595:	8d 41 01             	lea    0x1(%ecx),%eax
 598:	89 45 f4             	mov    %eax,-0xc(%ebp)
 59b:	8b 5d 10             	mov    0x10(%ebp),%ebx
 59e:	8b 45 ec             	mov    -0x14(%ebp),%eax
 5a1:	ba 00 00 00 00       	mov    $0x0,%edx
 5a6:	f7 f3                	div    %ebx
 5a8:	89 d0                	mov    %edx,%eax
 5aa:	0f b6 80 d0 0c 00 00 	movzbl 0xcd0(%eax),%eax
 5b1:	88 44 0d dc          	mov    %al,-0x24(%ebp,%ecx,1)
  }while((x /= base) != 0);
 5b5:	8b 5d 10             	mov    0x10(%ebp),%ebx
 5b8:	8b 45 ec             	mov    -0x14(%ebp),%eax
 5bb:	ba 00 00 00 00       	mov    $0x0,%edx
 5c0:	f7 f3                	div    %ebx
 5c2:	89 45 ec             	mov    %eax,-0x14(%ebp)
 5c5:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 5c9:	75 c7                	jne    592 <printint+0x38>
  if(neg)
 5cb:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 5cf:	74 2d                	je     5fe <printint+0xa4>
    buf[i++] = '-';
 5d1:	8b 45 f4             	mov    -0xc(%ebp),%eax
 5d4:	8d 50 01             	lea    0x1(%eax),%edx
 5d7:	89 55 f4             	mov    %edx,-0xc(%ebp)
 5da:	c6 44 05 dc 2d       	movb   $0x2d,-0x24(%ebp,%eax,1)

  while(--i >= 0)
 5df:	eb 1d                	jmp    5fe <printint+0xa4>
    putc(fd, buf[i]);
 5e1:	8d 55 dc             	lea    -0x24(%ebp),%edx
 5e4:	8b 45 f4             	mov    -0xc(%ebp),%eax
 5e7:	01 d0                	add    %edx,%eax
 5e9:	0f b6 00             	movzbl (%eax),%eax
 5ec:	0f be c0             	movsbl %al,%eax
 5ef:	83 ec 08             	sub    $0x8,%esp
 5f2:	50                   	push   %eax
 5f3:	ff 75 08             	pushl  0x8(%ebp)
 5f6:	e8 3c ff ff ff       	call   537 <putc>
 5fb:	83 c4 10             	add    $0x10,%esp
    buf[i++] = digits[x % base];
  }while((x /= base) != 0);
  if(neg)
    buf[i++] = '-';

  while(--i >= 0)
 5fe:	83 6d f4 01          	subl   $0x1,-0xc(%ebp)
 602:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 606:	79 d9                	jns    5e1 <printint+0x87>
    putc(fd, buf[i]);
}
 608:	90                   	nop
 609:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 60c:	c9                   	leave  
 60d:	c3                   	ret    

0000060e <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, char *fmt, ...)
{
 60e:	55                   	push   %ebp
 60f:	89 e5                	mov    %esp,%ebp
 611:	83 ec 28             	sub    $0x28,%esp
  char *s;
  int c, i, state;
  uint *ap;

  state = 0;
 614:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  ap = (uint*)(void*)&fmt + 1;
 61b:	8d 45 0c             	lea    0xc(%ebp),%eax
 61e:	83 c0 04             	add    $0x4,%eax
 621:	89 45 e8             	mov    %eax,-0x18(%ebp)
  for(i = 0; fmt[i]; i++){
 624:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 62b:	e9 59 01 00 00       	jmp    789 <printf+0x17b>
    c = fmt[i] & 0xff;
 630:	8b 55 0c             	mov    0xc(%ebp),%edx
 633:	8b 45 f0             	mov    -0x10(%ebp),%eax
 636:	01 d0                	add    %edx,%eax
 638:	0f b6 00             	movzbl (%eax),%eax
 63b:	0f be c0             	movsbl %al,%eax
 63e:	25 ff 00 00 00       	and    $0xff,%eax
 643:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    if(state == 0){
 646:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 64a:	75 2c                	jne    678 <printf+0x6a>
      if(c == '%'){
 64c:	83 7d e4 25          	cmpl   $0x25,-0x1c(%ebp)
 650:	75 0c                	jne    65e <printf+0x50>
        state = '%';
 652:	c7 45 ec 25 00 00 00 	movl   $0x25,-0x14(%ebp)
 659:	e9 27 01 00 00       	jmp    785 <printf+0x177>
      } else {
        putc(fd, c);
 65e:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 661:	0f be c0             	movsbl %al,%eax
 664:	83 ec 08             	sub    $0x8,%esp
 667:	50                   	push   %eax
 668:	ff 75 08             	pushl  0x8(%ebp)
 66b:	e8 c7 fe ff ff       	call   537 <putc>
 670:	83 c4 10             	add    $0x10,%esp
 673:	e9 0d 01 00 00       	jmp    785 <printf+0x177>
      }
    } else if(state == '%'){
 678:	83 7d ec 25          	cmpl   $0x25,-0x14(%ebp)
 67c:	0f 85 03 01 00 00    	jne    785 <printf+0x177>
      if(c == 'd'){
 682:	83 7d e4 64          	cmpl   $0x64,-0x1c(%ebp)
 686:	75 1e                	jne    6a6 <printf+0x98>
        printint(fd, *ap, 10, 1);
 688:	8b 45 e8             	mov    -0x18(%ebp),%eax
 68b:	8b 00                	mov    (%eax),%eax
 68d:	6a 01                	push   $0x1
 68f:	6a 0a                	push   $0xa
 691:	50                   	push   %eax
 692:	ff 75 08             	pushl  0x8(%ebp)
 695:	e8 c0 fe ff ff       	call   55a <printint>
 69a:	83 c4 10             	add    $0x10,%esp
        ap++;
 69d:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 6a1:	e9 d8 00 00 00       	jmp    77e <printf+0x170>
      } else if(c == 'x' || c == 'p'){
 6a6:	83 7d e4 78          	cmpl   $0x78,-0x1c(%ebp)
 6aa:	74 06                	je     6b2 <printf+0xa4>
 6ac:	83 7d e4 70          	cmpl   $0x70,-0x1c(%ebp)
 6b0:	75 1e                	jne    6d0 <printf+0xc2>
        printint(fd, *ap, 16, 0);
 6b2:	8b 45 e8             	mov    -0x18(%ebp),%eax
 6b5:	8b 00                	mov    (%eax),%eax
 6b7:	6a 00                	push   $0x0
 6b9:	6a 10                	push   $0x10
 6bb:	50                   	push   %eax
 6bc:	ff 75 08             	pushl  0x8(%ebp)
 6bf:	e8 96 fe ff ff       	call   55a <printint>
 6c4:	83 c4 10             	add    $0x10,%esp
        ap++;
 6c7:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 6cb:	e9 ae 00 00 00       	jmp    77e <printf+0x170>
      } else if(c == 's'){
 6d0:	83 7d e4 73          	cmpl   $0x73,-0x1c(%ebp)
 6d4:	75 43                	jne    719 <printf+0x10b>
        s = (char*)*ap;
 6d6:	8b 45 e8             	mov    -0x18(%ebp),%eax
 6d9:	8b 00                	mov    (%eax),%eax
 6db:	89 45 f4             	mov    %eax,-0xc(%ebp)
        ap++;
 6de:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
        if(s == 0)
 6e2:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 6e6:	75 25                	jne    70d <printf+0xff>
          s = "(null)";
 6e8:	c7 45 f4 6e 0a 00 00 	movl   $0xa6e,-0xc(%ebp)
        while(*s != 0){
 6ef:	eb 1c                	jmp    70d <printf+0xff>
          putc(fd, *s);
 6f1:	8b 45 f4             	mov    -0xc(%ebp),%eax
 6f4:	0f b6 00             	movzbl (%eax),%eax
 6f7:	0f be c0             	movsbl %al,%eax
 6fa:	83 ec 08             	sub    $0x8,%esp
 6fd:	50                   	push   %eax
 6fe:	ff 75 08             	pushl  0x8(%ebp)
 701:	e8 31 fe ff ff       	call   537 <putc>
 706:	83 c4 10             	add    $0x10,%esp
          s++;
 709:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
      } else if(c == 's'){
        s = (char*)*ap;
        ap++;
        if(s == 0)
          s = "(null)";
        while(*s != 0){
 70d:	8b 45 f4             	mov    -0xc(%ebp),%eax
 710:	0f b6 00             	movzbl (%eax),%eax
 713:	84 c0                	test   %al,%al
 715:	75 da                	jne    6f1 <printf+0xe3>
 717:	eb 65                	jmp    77e <printf+0x170>
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 719:	83 7d e4 63          	cmpl   $0x63,-0x1c(%ebp)
 71d:	75 1d                	jne    73c <printf+0x12e>
        putc(fd, *ap);
 71f:	8b 45 e8             	mov    -0x18(%ebp),%eax
 722:	8b 00                	mov    (%eax),%eax
 724:	0f be c0             	movsbl %al,%eax
 727:	83 ec 08             	sub    $0x8,%esp
 72a:	50                   	push   %eax
 72b:	ff 75 08             	pushl  0x8(%ebp)
 72e:	e8 04 fe ff ff       	call   537 <putc>
 733:	83 c4 10             	add    $0x10,%esp
        ap++;
 736:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 73a:	eb 42                	jmp    77e <printf+0x170>
      } else if(c == '%'){
 73c:	83 7d e4 25          	cmpl   $0x25,-0x1c(%ebp)
 740:	75 17                	jne    759 <printf+0x14b>
        putc(fd, c);
 742:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 745:	0f be c0             	movsbl %al,%eax
 748:	83 ec 08             	sub    $0x8,%esp
 74b:	50                   	push   %eax
 74c:	ff 75 08             	pushl  0x8(%ebp)
 74f:	e8 e3 fd ff ff       	call   537 <putc>
 754:	83 c4 10             	add    $0x10,%esp
 757:	eb 25                	jmp    77e <printf+0x170>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 759:	83 ec 08             	sub    $0x8,%esp
 75c:	6a 25                	push   $0x25
 75e:	ff 75 08             	pushl  0x8(%ebp)
 761:	e8 d1 fd ff ff       	call   537 <putc>
 766:	83 c4 10             	add    $0x10,%esp
        putc(fd, c);
 769:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 76c:	0f be c0             	movsbl %al,%eax
 76f:	83 ec 08             	sub    $0x8,%esp
 772:	50                   	push   %eax
 773:	ff 75 08             	pushl  0x8(%ebp)
 776:	e8 bc fd ff ff       	call   537 <putc>
 77b:	83 c4 10             	add    $0x10,%esp
      }
      state = 0;
 77e:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
 785:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
 789:	8b 55 0c             	mov    0xc(%ebp),%edx
 78c:	8b 45 f0             	mov    -0x10(%ebp),%eax
 78f:	01 d0                	add    %edx,%eax
 791:	0f b6 00             	movzbl (%eax),%eax
 794:	84 c0                	test   %al,%al
 796:	0f 85 94 fe ff ff    	jne    630 <printf+0x22>
        putc(fd, c);
      }
      state = 0;
    }
  }
}
 79c:	90                   	nop
 79d:	c9                   	leave  
 79e:	c3                   	ret    

0000079f <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 79f:	55                   	push   %ebp
 7a0:	89 e5                	mov    %esp,%ebp
 7a2:	83 ec 10             	sub    $0x10,%esp
  Header *bp, *p;

  bp = (Header*)ap - 1;
 7a5:	8b 45 08             	mov    0x8(%ebp),%eax
 7a8:	83 e8 08             	sub    $0x8,%eax
 7ab:	89 45 f8             	mov    %eax,-0x8(%ebp)
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 7ae:	a1 ec 0c 00 00       	mov    0xcec,%eax
 7b3:	89 45 fc             	mov    %eax,-0x4(%ebp)
 7b6:	eb 24                	jmp    7dc <free+0x3d>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 7b8:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7bb:	8b 00                	mov    (%eax),%eax
 7bd:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 7c0:	77 12                	ja     7d4 <free+0x35>
 7c2:	8b 45 f8             	mov    -0x8(%ebp),%eax
 7c5:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 7c8:	77 24                	ja     7ee <free+0x4f>
 7ca:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7cd:	8b 00                	mov    (%eax),%eax
 7cf:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 7d2:	77 1a                	ja     7ee <free+0x4f>
free(void *ap)
{
  Header *bp, *p;

  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 7d4:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7d7:	8b 00                	mov    (%eax),%eax
 7d9:	89 45 fc             	mov    %eax,-0x4(%ebp)
 7dc:	8b 45 f8             	mov    -0x8(%ebp),%eax
 7df:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 7e2:	76 d4                	jbe    7b8 <free+0x19>
 7e4:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7e7:	8b 00                	mov    (%eax),%eax
 7e9:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 7ec:	76 ca                	jbe    7b8 <free+0x19>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if(bp + bp->s.size == p->s.ptr){
 7ee:	8b 45 f8             	mov    -0x8(%ebp),%eax
 7f1:	8b 40 04             	mov    0x4(%eax),%eax
 7f4:	8d 14 c5 00 00 00 00 	lea    0x0(,%eax,8),%edx
 7fb:	8b 45 f8             	mov    -0x8(%ebp),%eax
 7fe:	01 c2                	add    %eax,%edx
 800:	8b 45 fc             	mov    -0x4(%ebp),%eax
 803:	8b 00                	mov    (%eax),%eax
 805:	39 c2                	cmp    %eax,%edx
 807:	75 24                	jne    82d <free+0x8e>
    bp->s.size += p->s.ptr->s.size;
 809:	8b 45 f8             	mov    -0x8(%ebp),%eax
 80c:	8b 50 04             	mov    0x4(%eax),%edx
 80f:	8b 45 fc             	mov    -0x4(%ebp),%eax
 812:	8b 00                	mov    (%eax),%eax
 814:	8b 40 04             	mov    0x4(%eax),%eax
 817:	01 c2                	add    %eax,%edx
 819:	8b 45 f8             	mov    -0x8(%ebp),%eax
 81c:	89 50 04             	mov    %edx,0x4(%eax)
    bp->s.ptr = p->s.ptr->s.ptr;
 81f:	8b 45 fc             	mov    -0x4(%ebp),%eax
 822:	8b 00                	mov    (%eax),%eax
 824:	8b 10                	mov    (%eax),%edx
 826:	8b 45 f8             	mov    -0x8(%ebp),%eax
 829:	89 10                	mov    %edx,(%eax)
 82b:	eb 0a                	jmp    837 <free+0x98>
  } else
    bp->s.ptr = p->s.ptr;
 82d:	8b 45 fc             	mov    -0x4(%ebp),%eax
 830:	8b 10                	mov    (%eax),%edx
 832:	8b 45 f8             	mov    -0x8(%ebp),%eax
 835:	89 10                	mov    %edx,(%eax)
  if(p + p->s.size == bp){
 837:	8b 45 fc             	mov    -0x4(%ebp),%eax
 83a:	8b 40 04             	mov    0x4(%eax),%eax
 83d:	8d 14 c5 00 00 00 00 	lea    0x0(,%eax,8),%edx
 844:	8b 45 fc             	mov    -0x4(%ebp),%eax
 847:	01 d0                	add    %edx,%eax
 849:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 84c:	75 20                	jne    86e <free+0xcf>
    p->s.size += bp->s.size;
 84e:	8b 45 fc             	mov    -0x4(%ebp),%eax
 851:	8b 50 04             	mov    0x4(%eax),%edx
 854:	8b 45 f8             	mov    -0x8(%ebp),%eax
 857:	8b 40 04             	mov    0x4(%eax),%eax
 85a:	01 c2                	add    %eax,%edx
 85c:	8b 45 fc             	mov    -0x4(%ebp),%eax
 85f:	89 50 04             	mov    %edx,0x4(%eax)
    p->s.ptr = bp->s.ptr;
 862:	8b 45 f8             	mov    -0x8(%ebp),%eax
 865:	8b 10                	mov    (%eax),%edx
 867:	8b 45 fc             	mov    -0x4(%ebp),%eax
 86a:	89 10                	mov    %edx,(%eax)
 86c:	eb 08                	jmp    876 <free+0xd7>
  } else
    p->s.ptr = bp;
 86e:	8b 45 fc             	mov    -0x4(%ebp),%eax
 871:	8b 55 f8             	mov    -0x8(%ebp),%edx
 874:	89 10                	mov    %edx,(%eax)
  freep = p;
 876:	8b 45 fc             	mov    -0x4(%ebp),%eax
 879:	a3 ec 0c 00 00       	mov    %eax,0xcec
}
 87e:	90                   	nop
 87f:	c9                   	leave  
 880:	c3                   	ret    

00000881 <morecore>:

static Header*
morecore(uint nu)
{
 881:	55                   	push   %ebp
 882:	89 e5                	mov    %esp,%ebp
 884:	83 ec 18             	sub    $0x18,%esp
  char *p;
  Header *hp;

  if(nu < 4096)
 887:	81 7d 08 ff 0f 00 00 	cmpl   $0xfff,0x8(%ebp)
 88e:	77 07                	ja     897 <morecore+0x16>
    nu = 4096;
 890:	c7 45 08 00 10 00 00 	movl   $0x1000,0x8(%ebp)
  p = sbrk(nu * sizeof(Header));
 897:	8b 45 08             	mov    0x8(%ebp),%eax
 89a:	c1 e0 03             	shl    $0x3,%eax
 89d:	83 ec 0c             	sub    $0xc,%esp
 8a0:	50                   	push   %eax
 8a1:	e8 31 fc ff ff       	call   4d7 <sbrk>
 8a6:	83 c4 10             	add    $0x10,%esp
 8a9:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(p == (char*)-1)
 8ac:	83 7d f4 ff          	cmpl   $0xffffffff,-0xc(%ebp)
 8b0:	75 07                	jne    8b9 <morecore+0x38>
    return 0;
 8b2:	b8 00 00 00 00       	mov    $0x0,%eax
 8b7:	eb 26                	jmp    8df <morecore+0x5e>
  hp = (Header*)p;
 8b9:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8bc:	89 45 f0             	mov    %eax,-0x10(%ebp)
  hp->s.size = nu;
 8bf:	8b 45 f0             	mov    -0x10(%ebp),%eax
 8c2:	8b 55 08             	mov    0x8(%ebp),%edx
 8c5:	89 50 04             	mov    %edx,0x4(%eax)
  free((void*)(hp + 1));
 8c8:	8b 45 f0             	mov    -0x10(%ebp),%eax
 8cb:	83 c0 08             	add    $0x8,%eax
 8ce:	83 ec 0c             	sub    $0xc,%esp
 8d1:	50                   	push   %eax
 8d2:	e8 c8 fe ff ff       	call   79f <free>
 8d7:	83 c4 10             	add    $0x10,%esp
  return freep;
 8da:	a1 ec 0c 00 00       	mov    0xcec,%eax
}
 8df:	c9                   	leave  
 8e0:	c3                   	ret    

000008e1 <malloc>:

void*
malloc(uint nbytes)
{
 8e1:	55                   	push   %ebp
 8e2:	89 e5                	mov    %esp,%ebp
 8e4:	83 ec 18             	sub    $0x18,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 8e7:	8b 45 08             	mov    0x8(%ebp),%eax
 8ea:	83 c0 07             	add    $0x7,%eax
 8ed:	c1 e8 03             	shr    $0x3,%eax
 8f0:	83 c0 01             	add    $0x1,%eax
 8f3:	89 45 ec             	mov    %eax,-0x14(%ebp)
  if((prevp = freep) == 0){
 8f6:	a1 ec 0c 00 00       	mov    0xcec,%eax
 8fb:	89 45 f0             	mov    %eax,-0x10(%ebp)
 8fe:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 902:	75 23                	jne    927 <malloc+0x46>
    base.s.ptr = freep = prevp = &base;
 904:	c7 45 f0 e4 0c 00 00 	movl   $0xce4,-0x10(%ebp)
 90b:	8b 45 f0             	mov    -0x10(%ebp),%eax
 90e:	a3 ec 0c 00 00       	mov    %eax,0xcec
 913:	a1 ec 0c 00 00       	mov    0xcec,%eax
 918:	a3 e4 0c 00 00       	mov    %eax,0xce4
    base.s.size = 0;
 91d:	c7 05 e8 0c 00 00 00 	movl   $0x0,0xce8
 924:	00 00 00 
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 927:	8b 45 f0             	mov    -0x10(%ebp),%eax
 92a:	8b 00                	mov    (%eax),%eax
 92c:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if(p->s.size >= nunits){
 92f:	8b 45 f4             	mov    -0xc(%ebp),%eax
 932:	8b 40 04             	mov    0x4(%eax),%eax
 935:	3b 45 ec             	cmp    -0x14(%ebp),%eax
 938:	72 4d                	jb     987 <malloc+0xa6>
      if(p->s.size == nunits)
 93a:	8b 45 f4             	mov    -0xc(%ebp),%eax
 93d:	8b 40 04             	mov    0x4(%eax),%eax
 940:	3b 45 ec             	cmp    -0x14(%ebp),%eax
 943:	75 0c                	jne    951 <malloc+0x70>
        prevp->s.ptr = p->s.ptr;
 945:	8b 45 f4             	mov    -0xc(%ebp),%eax
 948:	8b 10                	mov    (%eax),%edx
 94a:	8b 45 f0             	mov    -0x10(%ebp),%eax
 94d:	89 10                	mov    %edx,(%eax)
 94f:	eb 26                	jmp    977 <malloc+0x96>
      else {
        p->s.size -= nunits;
 951:	8b 45 f4             	mov    -0xc(%ebp),%eax
 954:	8b 40 04             	mov    0x4(%eax),%eax
 957:	2b 45 ec             	sub    -0x14(%ebp),%eax
 95a:	89 c2                	mov    %eax,%edx
 95c:	8b 45 f4             	mov    -0xc(%ebp),%eax
 95f:	89 50 04             	mov    %edx,0x4(%eax)
        p += p->s.size;
 962:	8b 45 f4             	mov    -0xc(%ebp),%eax
 965:	8b 40 04             	mov    0x4(%eax),%eax
 968:	c1 e0 03             	shl    $0x3,%eax
 96b:	01 45 f4             	add    %eax,-0xc(%ebp)
        p->s.size = nunits;
 96e:	8b 45 f4             	mov    -0xc(%ebp),%eax
 971:	8b 55 ec             	mov    -0x14(%ebp),%edx
 974:	89 50 04             	mov    %edx,0x4(%eax)
      }
      freep = prevp;
 977:	8b 45 f0             	mov    -0x10(%ebp),%eax
 97a:	a3 ec 0c 00 00       	mov    %eax,0xcec
      return (void*)(p + 1);
 97f:	8b 45 f4             	mov    -0xc(%ebp),%eax
 982:	83 c0 08             	add    $0x8,%eax
 985:	eb 3b                	jmp    9c2 <malloc+0xe1>
    }
    if(p == freep)
 987:	a1 ec 0c 00 00       	mov    0xcec,%eax
 98c:	39 45 f4             	cmp    %eax,-0xc(%ebp)
 98f:	75 1e                	jne    9af <malloc+0xce>
      if((p = morecore(nunits)) == 0)
 991:	83 ec 0c             	sub    $0xc,%esp
 994:	ff 75 ec             	pushl  -0x14(%ebp)
 997:	e8 e5 fe ff ff       	call   881 <morecore>
 99c:	83 c4 10             	add    $0x10,%esp
 99f:	89 45 f4             	mov    %eax,-0xc(%ebp)
 9a2:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 9a6:	75 07                	jne    9af <malloc+0xce>
        return 0;
 9a8:	b8 00 00 00 00       	mov    $0x0,%eax
 9ad:	eb 13                	jmp    9c2 <malloc+0xe1>
  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
  if((prevp = freep) == 0){
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 9af:	8b 45 f4             	mov    -0xc(%ebp),%eax
 9b2:	89 45 f0             	mov    %eax,-0x10(%ebp)
 9b5:	8b 45 f4             	mov    -0xc(%ebp),%eax
 9b8:	8b 00                	mov    (%eax),%eax
 9ba:	89 45 f4             	mov    %eax,-0xc(%ebp)
      return (void*)(p + 1);
    }
    if(p == freep)
      if((p = morecore(nunits)) == 0)
        return 0;
  }
 9bd:	e9 6d ff ff ff       	jmp    92f <malloc+0x4e>
}
 9c2:	c9                   	leave  
 9c3:	c3                   	ret    
