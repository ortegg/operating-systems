
_cat:     file format elf32-i386


Disassembly of section .text:

00000000 <cat>:

char buf[512];

void
cat(int fd)
{
   0:	55                   	push   %ebp
   1:	89 e5                	mov    %esp,%ebp
   3:	83 ec 18             	sub    $0x18,%esp
  int n;

  while((n = read(fd, buf, sizeof(buf))) > 0)
   6:	eb 15                	jmp    1d <cat+0x1d>
    write(1, buf, n);
   8:	83 ec 04             	sub    $0x4,%esp
   b:	ff 75 f4             	pushl  -0xc(%ebp)
   e:	68 e0 0b 00 00       	push   $0xbe0
  13:	6a 01                	push   $0x1
  15:	e8 99 03 00 00       	call   3b3 <write>
  1a:	83 c4 10             	add    $0x10,%esp
void
cat(int fd)
{
  int n;

  while((n = read(fd, buf, sizeof(buf))) > 0)
  1d:	83 ec 04             	sub    $0x4,%esp
  20:	68 00 02 00 00       	push   $0x200
  25:	68 e0 0b 00 00       	push   $0xbe0
  2a:	ff 75 08             	pushl  0x8(%ebp)
  2d:	e8 79 03 00 00       	call   3ab <read>
  32:	83 c4 10             	add    $0x10,%esp
  35:	89 45 f4             	mov    %eax,-0xc(%ebp)
  38:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
  3c:	7f ca                	jg     8 <cat+0x8>
    write(1, buf, n);
  if(n < 0){
  3e:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
  42:	79 17                	jns    5b <cat+0x5b>
    printf(1, "cat: read error\n");
  44:	83 ec 08             	sub    $0x8,%esp
  47:	68 08 09 00 00       	push   $0x908
  4c:	6a 01                	push   $0x1
  4e:	e8 ff 04 00 00       	call   552 <printf>
  53:	83 c4 10             	add    $0x10,%esp
    exit();
  56:	e8 38 03 00 00       	call   393 <exit>
  }
}
  5b:	90                   	nop
  5c:	c9                   	leave  
  5d:	c3                   	ret    

0000005e <main>:

int
main(int argc, char *argv[])
{
  5e:	8d 4c 24 04          	lea    0x4(%esp),%ecx
  62:	83 e4 f0             	and    $0xfffffff0,%esp
  65:	ff 71 fc             	pushl  -0x4(%ecx)
  68:	55                   	push   %ebp
  69:	89 e5                	mov    %esp,%ebp
  6b:	53                   	push   %ebx
  6c:	51                   	push   %ecx
  6d:	83 ec 10             	sub    $0x10,%esp
  70:	89 cb                	mov    %ecx,%ebx
  int fd, i;

  if(argc <= 1){
  72:	83 3b 01             	cmpl   $0x1,(%ebx)
  75:	7f 12                	jg     89 <main+0x2b>
    cat(0);
  77:	83 ec 0c             	sub    $0xc,%esp
  7a:	6a 00                	push   $0x0
  7c:	e8 7f ff ff ff       	call   0 <cat>
  81:	83 c4 10             	add    $0x10,%esp
    exit();
  84:	e8 0a 03 00 00       	call   393 <exit>
  }

  for(i = 1; i < argc; i++){
  89:	c7 45 f4 01 00 00 00 	movl   $0x1,-0xc(%ebp)
  90:	eb 71                	jmp    103 <main+0xa5>
    if((fd = open(argv[i], 0)) < 0){
  92:	8b 45 f4             	mov    -0xc(%ebp),%eax
  95:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  9c:	8b 43 04             	mov    0x4(%ebx),%eax
  9f:	01 d0                	add    %edx,%eax
  a1:	8b 00                	mov    (%eax),%eax
  a3:	83 ec 08             	sub    $0x8,%esp
  a6:	6a 00                	push   $0x0
  a8:	50                   	push   %eax
  a9:	e8 25 03 00 00       	call   3d3 <open>
  ae:	83 c4 10             	add    $0x10,%esp
  b1:	89 45 f0             	mov    %eax,-0x10(%ebp)
  b4:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
  b8:	79 29                	jns    e3 <main+0x85>
      printf(1, "cat: cannot open %s\n", argv[i]);
  ba:	8b 45 f4             	mov    -0xc(%ebp),%eax
  bd:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  c4:	8b 43 04             	mov    0x4(%ebx),%eax
  c7:	01 d0                	add    %edx,%eax
  c9:	8b 00                	mov    (%eax),%eax
  cb:	83 ec 04             	sub    $0x4,%esp
  ce:	50                   	push   %eax
  cf:	68 19 09 00 00       	push   $0x919
  d4:	6a 01                	push   $0x1
  d6:	e8 77 04 00 00       	call   552 <printf>
  db:	83 c4 10             	add    $0x10,%esp
      exit();
  de:	e8 b0 02 00 00       	call   393 <exit>
    }
    cat(fd);
  e3:	83 ec 0c             	sub    $0xc,%esp
  e6:	ff 75 f0             	pushl  -0x10(%ebp)
  e9:	e8 12 ff ff ff       	call   0 <cat>
  ee:	83 c4 10             	add    $0x10,%esp
    close(fd);
  f1:	83 ec 0c             	sub    $0xc,%esp
  f4:	ff 75 f0             	pushl  -0x10(%ebp)
  f7:	e8 bf 02 00 00       	call   3bb <close>
  fc:	83 c4 10             	add    $0x10,%esp
  if(argc <= 1){
    cat(0);
    exit();
  }

  for(i = 1; i < argc; i++){
  ff:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
 103:	8b 45 f4             	mov    -0xc(%ebp),%eax
 106:	3b 03                	cmp    (%ebx),%eax
 108:	7c 88                	jl     92 <main+0x34>
      exit();
    }
    cat(fd);
    close(fd);
  }
  exit();
 10a:	e8 84 02 00 00       	call   393 <exit>

0000010f <stosb>:
               "cc");
}

static inline void
stosb(void *addr, int data, int cnt)
{
 10f:	55                   	push   %ebp
 110:	89 e5                	mov    %esp,%ebp
 112:	57                   	push   %edi
 113:	53                   	push   %ebx
  asm volatile("cld; rep stosb" :
 114:	8b 4d 08             	mov    0x8(%ebp),%ecx
 117:	8b 55 10             	mov    0x10(%ebp),%edx
 11a:	8b 45 0c             	mov    0xc(%ebp),%eax
 11d:	89 cb                	mov    %ecx,%ebx
 11f:	89 df                	mov    %ebx,%edi
 121:	89 d1                	mov    %edx,%ecx
 123:	fc                   	cld    
 124:	f3 aa                	rep stos %al,%es:(%edi)
 126:	89 ca                	mov    %ecx,%edx
 128:	89 fb                	mov    %edi,%ebx
 12a:	89 5d 08             	mov    %ebx,0x8(%ebp)
 12d:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
 130:	90                   	nop
 131:	5b                   	pop    %ebx
 132:	5f                   	pop    %edi
 133:	5d                   	pop    %ebp
 134:	c3                   	ret    

00000135 <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, char *t)
{
 135:	55                   	push   %ebp
 136:	89 e5                	mov    %esp,%ebp
 138:	83 ec 10             	sub    $0x10,%esp
  char *os;

  os = s;
 13b:	8b 45 08             	mov    0x8(%ebp),%eax
 13e:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while((*s++ = *t++) != 0)
 141:	90                   	nop
 142:	8b 45 08             	mov    0x8(%ebp),%eax
 145:	8d 50 01             	lea    0x1(%eax),%edx
 148:	89 55 08             	mov    %edx,0x8(%ebp)
 14b:	8b 55 0c             	mov    0xc(%ebp),%edx
 14e:	8d 4a 01             	lea    0x1(%edx),%ecx
 151:	89 4d 0c             	mov    %ecx,0xc(%ebp)
 154:	0f b6 12             	movzbl (%edx),%edx
 157:	88 10                	mov    %dl,(%eax)
 159:	0f b6 00             	movzbl (%eax),%eax
 15c:	84 c0                	test   %al,%al
 15e:	75 e2                	jne    142 <strcpy+0xd>
    ;
  return os;
 160:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 163:	c9                   	leave  
 164:	c3                   	ret    

00000165 <strcmp>:

int
strcmp(const char *p, const char *q)
{
 165:	55                   	push   %ebp
 166:	89 e5                	mov    %esp,%ebp
  while(*p && *p == *q)
 168:	eb 08                	jmp    172 <strcmp+0xd>
    p++, q++;
 16a:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 16e:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
  while(*p && *p == *q)
 172:	8b 45 08             	mov    0x8(%ebp),%eax
 175:	0f b6 00             	movzbl (%eax),%eax
 178:	84 c0                	test   %al,%al
 17a:	74 10                	je     18c <strcmp+0x27>
 17c:	8b 45 08             	mov    0x8(%ebp),%eax
 17f:	0f b6 10             	movzbl (%eax),%edx
 182:	8b 45 0c             	mov    0xc(%ebp),%eax
 185:	0f b6 00             	movzbl (%eax),%eax
 188:	38 c2                	cmp    %al,%dl
 18a:	74 de                	je     16a <strcmp+0x5>
    p++, q++;
  return (uchar)*p - (uchar)*q;
 18c:	8b 45 08             	mov    0x8(%ebp),%eax
 18f:	0f b6 00             	movzbl (%eax),%eax
 192:	0f b6 d0             	movzbl %al,%edx
 195:	8b 45 0c             	mov    0xc(%ebp),%eax
 198:	0f b6 00             	movzbl (%eax),%eax
 19b:	0f b6 c0             	movzbl %al,%eax
 19e:	29 c2                	sub    %eax,%edx
 1a0:	89 d0                	mov    %edx,%eax
}
 1a2:	5d                   	pop    %ebp
 1a3:	c3                   	ret    

000001a4 <strlen>:

uint
strlen(char *s)
{
 1a4:	55                   	push   %ebp
 1a5:	89 e5                	mov    %esp,%ebp
 1a7:	83 ec 10             	sub    $0x10,%esp
  int n;

  for(n = 0; s[n]; n++)
 1aa:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
 1b1:	eb 04                	jmp    1b7 <strlen+0x13>
 1b3:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
 1b7:	8b 55 fc             	mov    -0x4(%ebp),%edx
 1ba:	8b 45 08             	mov    0x8(%ebp),%eax
 1bd:	01 d0                	add    %edx,%eax
 1bf:	0f b6 00             	movzbl (%eax),%eax
 1c2:	84 c0                	test   %al,%al
 1c4:	75 ed                	jne    1b3 <strlen+0xf>
    ;
  return n;
 1c6:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 1c9:	c9                   	leave  
 1ca:	c3                   	ret    

000001cb <memset>:

void*
memset(void *dst, int c, uint n)
{
 1cb:	55                   	push   %ebp
 1cc:	89 e5                	mov    %esp,%ebp
  stosb(dst, c, n);
 1ce:	8b 45 10             	mov    0x10(%ebp),%eax
 1d1:	50                   	push   %eax
 1d2:	ff 75 0c             	pushl  0xc(%ebp)
 1d5:	ff 75 08             	pushl  0x8(%ebp)
 1d8:	e8 32 ff ff ff       	call   10f <stosb>
 1dd:	83 c4 0c             	add    $0xc,%esp
  return dst;
 1e0:	8b 45 08             	mov    0x8(%ebp),%eax
}
 1e3:	c9                   	leave  
 1e4:	c3                   	ret    

000001e5 <strchr>:

char*
strchr(const char *s, char c)
{
 1e5:	55                   	push   %ebp
 1e6:	89 e5                	mov    %esp,%ebp
 1e8:	83 ec 04             	sub    $0x4,%esp
 1eb:	8b 45 0c             	mov    0xc(%ebp),%eax
 1ee:	88 45 fc             	mov    %al,-0x4(%ebp)
  for(; *s; s++)
 1f1:	eb 14                	jmp    207 <strchr+0x22>
    if(*s == c)
 1f3:	8b 45 08             	mov    0x8(%ebp),%eax
 1f6:	0f b6 00             	movzbl (%eax),%eax
 1f9:	3a 45 fc             	cmp    -0x4(%ebp),%al
 1fc:	75 05                	jne    203 <strchr+0x1e>
      return (char*)s;
 1fe:	8b 45 08             	mov    0x8(%ebp),%eax
 201:	eb 13                	jmp    216 <strchr+0x31>
}

char*
strchr(const char *s, char c)
{
  for(; *s; s++)
 203:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 207:	8b 45 08             	mov    0x8(%ebp),%eax
 20a:	0f b6 00             	movzbl (%eax),%eax
 20d:	84 c0                	test   %al,%al
 20f:	75 e2                	jne    1f3 <strchr+0xe>
    if(*s == c)
      return (char*)s;
  return 0;
 211:	b8 00 00 00 00       	mov    $0x0,%eax
}
 216:	c9                   	leave  
 217:	c3                   	ret    

00000218 <gets>:

char*
gets(char *buf, int max)
{
 218:	55                   	push   %ebp
 219:	89 e5                	mov    %esp,%ebp
 21b:	83 ec 18             	sub    $0x18,%esp
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 21e:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 225:	eb 42                	jmp    269 <gets+0x51>
    cc = read(0, &c, 1);
 227:	83 ec 04             	sub    $0x4,%esp
 22a:	6a 01                	push   $0x1
 22c:	8d 45 ef             	lea    -0x11(%ebp),%eax
 22f:	50                   	push   %eax
 230:	6a 00                	push   $0x0
 232:	e8 74 01 00 00       	call   3ab <read>
 237:	83 c4 10             	add    $0x10,%esp
 23a:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if(cc < 1)
 23d:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 241:	7e 33                	jle    276 <gets+0x5e>
      break;
    buf[i++] = c;
 243:	8b 45 f4             	mov    -0xc(%ebp),%eax
 246:	8d 50 01             	lea    0x1(%eax),%edx
 249:	89 55 f4             	mov    %edx,-0xc(%ebp)
 24c:	89 c2                	mov    %eax,%edx
 24e:	8b 45 08             	mov    0x8(%ebp),%eax
 251:	01 c2                	add    %eax,%edx
 253:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 257:	88 02                	mov    %al,(%edx)
    if(c == '\n' || c == '\r')
 259:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 25d:	3c 0a                	cmp    $0xa,%al
 25f:	74 16                	je     277 <gets+0x5f>
 261:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 265:	3c 0d                	cmp    $0xd,%al
 267:	74 0e                	je     277 <gets+0x5f>
gets(char *buf, int max)
{
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 269:	8b 45 f4             	mov    -0xc(%ebp),%eax
 26c:	83 c0 01             	add    $0x1,%eax
 26f:	3b 45 0c             	cmp    0xc(%ebp),%eax
 272:	7c b3                	jl     227 <gets+0xf>
 274:	eb 01                	jmp    277 <gets+0x5f>
    cc = read(0, &c, 1);
    if(cc < 1)
      break;
 276:	90                   	nop
    buf[i++] = c;
    if(c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 277:	8b 55 f4             	mov    -0xc(%ebp),%edx
 27a:	8b 45 08             	mov    0x8(%ebp),%eax
 27d:	01 d0                	add    %edx,%eax
 27f:	c6 00 00             	movb   $0x0,(%eax)
  return buf;
 282:	8b 45 08             	mov    0x8(%ebp),%eax
}
 285:	c9                   	leave  
 286:	c3                   	ret    

00000287 <stat>:

int
stat(char *n, struct stat *st)
{
 287:	55                   	push   %ebp
 288:	89 e5                	mov    %esp,%ebp
 28a:	83 ec 18             	sub    $0x18,%esp
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 28d:	83 ec 08             	sub    $0x8,%esp
 290:	6a 00                	push   $0x0
 292:	ff 75 08             	pushl  0x8(%ebp)
 295:	e8 39 01 00 00       	call   3d3 <open>
 29a:	83 c4 10             	add    $0x10,%esp
 29d:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(fd < 0)
 2a0:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 2a4:	79 07                	jns    2ad <stat+0x26>
    return -1;
 2a6:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 2ab:	eb 25                	jmp    2d2 <stat+0x4b>
  r = fstat(fd, st);
 2ad:	83 ec 08             	sub    $0x8,%esp
 2b0:	ff 75 0c             	pushl  0xc(%ebp)
 2b3:	ff 75 f4             	pushl  -0xc(%ebp)
 2b6:	e8 30 01 00 00       	call   3eb <fstat>
 2bb:	83 c4 10             	add    $0x10,%esp
 2be:	89 45 f0             	mov    %eax,-0x10(%ebp)
  close(fd);
 2c1:	83 ec 0c             	sub    $0xc,%esp
 2c4:	ff 75 f4             	pushl  -0xc(%ebp)
 2c7:	e8 ef 00 00 00       	call   3bb <close>
 2cc:	83 c4 10             	add    $0x10,%esp
  return r;
 2cf:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
 2d2:	c9                   	leave  
 2d3:	c3                   	ret    

000002d4 <atoi>:

int
atoi(const char *s)
{
 2d4:	55                   	push   %ebp
 2d5:	89 e5                	mov    %esp,%ebp
 2d7:	83 ec 10             	sub    $0x10,%esp
  int n, sign;

  n = 0;
 2da:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  while(*s == ' ') s++;
 2e1:	eb 04                	jmp    2e7 <atoi+0x13>
 2e3:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 2e7:	8b 45 08             	mov    0x8(%ebp),%eax
 2ea:	0f b6 00             	movzbl (%eax),%eax
 2ed:	3c 20                	cmp    $0x20,%al
 2ef:	74 f2                	je     2e3 <atoi+0xf>
  sign = (*s == '-') ? -1 : 1;
 2f1:	8b 45 08             	mov    0x8(%ebp),%eax
 2f4:	0f b6 00             	movzbl (%eax),%eax
 2f7:	3c 2d                	cmp    $0x2d,%al
 2f9:	75 07                	jne    302 <atoi+0x2e>
 2fb:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 300:	eb 05                	jmp    307 <atoi+0x33>
 302:	b8 01 00 00 00       	mov    $0x1,%eax
 307:	89 45 f8             	mov    %eax,-0x8(%ebp)
  while('0' <= *s && *s <= '9')
 30a:	eb 25                	jmp    331 <atoi+0x5d>
    n = n*10 + *s++ - '0';
 30c:	8b 55 fc             	mov    -0x4(%ebp),%edx
 30f:	89 d0                	mov    %edx,%eax
 311:	c1 e0 02             	shl    $0x2,%eax
 314:	01 d0                	add    %edx,%eax
 316:	01 c0                	add    %eax,%eax
 318:	89 c1                	mov    %eax,%ecx
 31a:	8b 45 08             	mov    0x8(%ebp),%eax
 31d:	8d 50 01             	lea    0x1(%eax),%edx
 320:	89 55 08             	mov    %edx,0x8(%ebp)
 323:	0f b6 00             	movzbl (%eax),%eax
 326:	0f be c0             	movsbl %al,%eax
 329:	01 c8                	add    %ecx,%eax
 32b:	83 e8 30             	sub    $0x30,%eax
 32e:	89 45 fc             	mov    %eax,-0x4(%ebp)
  int n, sign;

  n = 0;
  while(*s == ' ') s++;
  sign = (*s == '-') ? -1 : 1;
  while('0' <= *s && *s <= '9')
 331:	8b 45 08             	mov    0x8(%ebp),%eax
 334:	0f b6 00             	movzbl (%eax),%eax
 337:	3c 2f                	cmp    $0x2f,%al
 339:	7e 0a                	jle    345 <atoi+0x71>
 33b:	8b 45 08             	mov    0x8(%ebp),%eax
 33e:	0f b6 00             	movzbl (%eax),%eax
 341:	3c 39                	cmp    $0x39,%al
 343:	7e c7                	jle    30c <atoi+0x38>
    n = n*10 + *s++ - '0';
  return sign*n;
 345:	8b 45 f8             	mov    -0x8(%ebp),%eax
 348:	0f af 45 fc          	imul   -0x4(%ebp),%eax
}
 34c:	c9                   	leave  
 34d:	c3                   	ret    

0000034e <memmove>:

void*
memmove(void *vdst, void *vsrc, int n)
{
 34e:	55                   	push   %ebp
 34f:	89 e5                	mov    %esp,%ebp
 351:	83 ec 10             	sub    $0x10,%esp
  char *dst, *src;
  
  dst = vdst;
 354:	8b 45 08             	mov    0x8(%ebp),%eax
 357:	89 45 fc             	mov    %eax,-0x4(%ebp)
  src = vsrc;
 35a:	8b 45 0c             	mov    0xc(%ebp),%eax
 35d:	89 45 f8             	mov    %eax,-0x8(%ebp)
  while(n-- > 0)
 360:	eb 17                	jmp    379 <memmove+0x2b>
    *dst++ = *src++;
 362:	8b 45 fc             	mov    -0x4(%ebp),%eax
 365:	8d 50 01             	lea    0x1(%eax),%edx
 368:	89 55 fc             	mov    %edx,-0x4(%ebp)
 36b:	8b 55 f8             	mov    -0x8(%ebp),%edx
 36e:	8d 4a 01             	lea    0x1(%edx),%ecx
 371:	89 4d f8             	mov    %ecx,-0x8(%ebp)
 374:	0f b6 12             	movzbl (%edx),%edx
 377:	88 10                	mov    %dl,(%eax)
{
  char *dst, *src;
  
  dst = vdst;
  src = vsrc;
  while(n-- > 0)
 379:	8b 45 10             	mov    0x10(%ebp),%eax
 37c:	8d 50 ff             	lea    -0x1(%eax),%edx
 37f:	89 55 10             	mov    %edx,0x10(%ebp)
 382:	85 c0                	test   %eax,%eax
 384:	7f dc                	jg     362 <memmove+0x14>
    *dst++ = *src++;
  return vdst;
 386:	8b 45 08             	mov    0x8(%ebp),%eax
}
 389:	c9                   	leave  
 38a:	c3                   	ret    

0000038b <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
 38b:	b8 01 00 00 00       	mov    $0x1,%eax
 390:	cd 40                	int    $0x40
 392:	c3                   	ret    

00000393 <exit>:
SYSCALL(exit)
 393:	b8 02 00 00 00       	mov    $0x2,%eax
 398:	cd 40                	int    $0x40
 39a:	c3                   	ret    

0000039b <wait>:
SYSCALL(wait)
 39b:	b8 03 00 00 00       	mov    $0x3,%eax
 3a0:	cd 40                	int    $0x40
 3a2:	c3                   	ret    

000003a3 <pipe>:
SYSCALL(pipe)
 3a3:	b8 04 00 00 00       	mov    $0x4,%eax
 3a8:	cd 40                	int    $0x40
 3aa:	c3                   	ret    

000003ab <read>:
SYSCALL(read)
 3ab:	b8 05 00 00 00       	mov    $0x5,%eax
 3b0:	cd 40                	int    $0x40
 3b2:	c3                   	ret    

000003b3 <write>:
SYSCALL(write)
 3b3:	b8 10 00 00 00       	mov    $0x10,%eax
 3b8:	cd 40                	int    $0x40
 3ba:	c3                   	ret    

000003bb <close>:
SYSCALL(close)
 3bb:	b8 15 00 00 00       	mov    $0x15,%eax
 3c0:	cd 40                	int    $0x40
 3c2:	c3                   	ret    

000003c3 <kill>:
SYSCALL(kill)
 3c3:	b8 06 00 00 00       	mov    $0x6,%eax
 3c8:	cd 40                	int    $0x40
 3ca:	c3                   	ret    

000003cb <exec>:
SYSCALL(exec)
 3cb:	b8 07 00 00 00       	mov    $0x7,%eax
 3d0:	cd 40                	int    $0x40
 3d2:	c3                   	ret    

000003d3 <open>:
SYSCALL(open)
 3d3:	b8 0f 00 00 00       	mov    $0xf,%eax
 3d8:	cd 40                	int    $0x40
 3da:	c3                   	ret    

000003db <mknod>:
SYSCALL(mknod)
 3db:	b8 11 00 00 00       	mov    $0x11,%eax
 3e0:	cd 40                	int    $0x40
 3e2:	c3                   	ret    

000003e3 <unlink>:
SYSCALL(unlink)
 3e3:	b8 12 00 00 00       	mov    $0x12,%eax
 3e8:	cd 40                	int    $0x40
 3ea:	c3                   	ret    

000003eb <fstat>:
SYSCALL(fstat)
 3eb:	b8 08 00 00 00       	mov    $0x8,%eax
 3f0:	cd 40                	int    $0x40
 3f2:	c3                   	ret    

000003f3 <link>:
SYSCALL(link)
 3f3:	b8 13 00 00 00       	mov    $0x13,%eax
 3f8:	cd 40                	int    $0x40
 3fa:	c3                   	ret    

000003fb <mkdir>:
SYSCALL(mkdir)
 3fb:	b8 14 00 00 00       	mov    $0x14,%eax
 400:	cd 40                	int    $0x40
 402:	c3                   	ret    

00000403 <chdir>:
SYSCALL(chdir)
 403:	b8 09 00 00 00       	mov    $0x9,%eax
 408:	cd 40                	int    $0x40
 40a:	c3                   	ret    

0000040b <dup>:
SYSCALL(dup)
 40b:	b8 0a 00 00 00       	mov    $0xa,%eax
 410:	cd 40                	int    $0x40
 412:	c3                   	ret    

00000413 <getpid>:
SYSCALL(getpid)
 413:	b8 0b 00 00 00       	mov    $0xb,%eax
 418:	cd 40                	int    $0x40
 41a:	c3                   	ret    

0000041b <sbrk>:
SYSCALL(sbrk)
 41b:	b8 0c 00 00 00       	mov    $0xc,%eax
 420:	cd 40                	int    $0x40
 422:	c3                   	ret    

00000423 <sleep>:
SYSCALL(sleep)
 423:	b8 0d 00 00 00       	mov    $0xd,%eax
 428:	cd 40                	int    $0x40
 42a:	c3                   	ret    

0000042b <uptime>:
SYSCALL(uptime)
 42b:	b8 0e 00 00 00       	mov    $0xe,%eax
 430:	cd 40                	int    $0x40
 432:	c3                   	ret    

00000433 <halt>:
SYSCALL(halt)
 433:	b8 16 00 00 00       	mov    $0x16,%eax
 438:	cd 40                	int    $0x40
 43a:	c3                   	ret    

0000043b <date>:
SYSCALL(date)    #added the date system call
 43b:	b8 17 00 00 00       	mov    $0x17,%eax
 440:	cd 40                	int    $0x40
 442:	c3                   	ret    

00000443 <getuid>:
SYSCALL(getuid)
 443:	b8 18 00 00 00       	mov    $0x18,%eax
 448:	cd 40                	int    $0x40
 44a:	c3                   	ret    

0000044b <getgid>:
SYSCALL(getgid)
 44b:	b8 19 00 00 00       	mov    $0x19,%eax
 450:	cd 40                	int    $0x40
 452:	c3                   	ret    

00000453 <getppid>:
SYSCALL(getppid)
 453:	b8 1a 00 00 00       	mov    $0x1a,%eax
 458:	cd 40                	int    $0x40
 45a:	c3                   	ret    

0000045b <setuid>:
SYSCALL(setuid)
 45b:	b8 1b 00 00 00       	mov    $0x1b,%eax
 460:	cd 40                	int    $0x40
 462:	c3                   	ret    

00000463 <setgid>:
SYSCALL(setgid)
 463:	b8 1c 00 00 00       	mov    $0x1c,%eax
 468:	cd 40                	int    $0x40
 46a:	c3                   	ret    

0000046b <getprocs>:
SYSCALL(getprocs)
 46b:	b8 1d 00 00 00       	mov    $0x1d,%eax
 470:	cd 40                	int    $0x40
 472:	c3                   	ret    

00000473 <setpriority>:
SYSCALL(setpriority)
 473:	b8 1e 00 00 00       	mov    $0x1e,%eax
 478:	cd 40                	int    $0x40
 47a:	c3                   	ret    

0000047b <putc>:
#include "stat.h"
#include "user.h"

static void
putc(int fd, char c)
{
 47b:	55                   	push   %ebp
 47c:	89 e5                	mov    %esp,%ebp
 47e:	83 ec 18             	sub    $0x18,%esp
 481:	8b 45 0c             	mov    0xc(%ebp),%eax
 484:	88 45 f4             	mov    %al,-0xc(%ebp)
  write(fd, &c, 1);
 487:	83 ec 04             	sub    $0x4,%esp
 48a:	6a 01                	push   $0x1
 48c:	8d 45 f4             	lea    -0xc(%ebp),%eax
 48f:	50                   	push   %eax
 490:	ff 75 08             	pushl  0x8(%ebp)
 493:	e8 1b ff ff ff       	call   3b3 <write>
 498:	83 c4 10             	add    $0x10,%esp
}
 49b:	90                   	nop
 49c:	c9                   	leave  
 49d:	c3                   	ret    

0000049e <printint>:

static void
printint(int fd, int xx, int base, int sgn)
{
 49e:	55                   	push   %ebp
 49f:	89 e5                	mov    %esp,%ebp
 4a1:	53                   	push   %ebx
 4a2:	83 ec 24             	sub    $0x24,%esp
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 4a5:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  if(sgn && xx < 0){
 4ac:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
 4b0:	74 17                	je     4c9 <printint+0x2b>
 4b2:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
 4b6:	79 11                	jns    4c9 <printint+0x2b>
    neg = 1;
 4b8:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
    x = -xx;
 4bf:	8b 45 0c             	mov    0xc(%ebp),%eax
 4c2:	f7 d8                	neg    %eax
 4c4:	89 45 ec             	mov    %eax,-0x14(%ebp)
 4c7:	eb 06                	jmp    4cf <printint+0x31>
  } else {
    x = xx;
 4c9:	8b 45 0c             	mov    0xc(%ebp),%eax
 4cc:	89 45 ec             	mov    %eax,-0x14(%ebp)
  }

  i = 0;
 4cf:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  do{
    buf[i++] = digits[x % base];
 4d6:	8b 4d f4             	mov    -0xc(%ebp),%ecx
 4d9:	8d 41 01             	lea    0x1(%ecx),%eax
 4dc:	89 45 f4             	mov    %eax,-0xc(%ebp)
 4df:	8b 5d 10             	mov    0x10(%ebp),%ebx
 4e2:	8b 45 ec             	mov    -0x14(%ebp),%eax
 4e5:	ba 00 00 00 00       	mov    $0x0,%edx
 4ea:	f7 f3                	div    %ebx
 4ec:	89 d0                	mov    %edx,%eax
 4ee:	0f b6 80 a4 0b 00 00 	movzbl 0xba4(%eax),%eax
 4f5:	88 44 0d dc          	mov    %al,-0x24(%ebp,%ecx,1)
  }while((x /= base) != 0);
 4f9:	8b 5d 10             	mov    0x10(%ebp),%ebx
 4fc:	8b 45 ec             	mov    -0x14(%ebp),%eax
 4ff:	ba 00 00 00 00       	mov    $0x0,%edx
 504:	f7 f3                	div    %ebx
 506:	89 45 ec             	mov    %eax,-0x14(%ebp)
 509:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 50d:	75 c7                	jne    4d6 <printint+0x38>
  if(neg)
 50f:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 513:	74 2d                	je     542 <printint+0xa4>
    buf[i++] = '-';
 515:	8b 45 f4             	mov    -0xc(%ebp),%eax
 518:	8d 50 01             	lea    0x1(%eax),%edx
 51b:	89 55 f4             	mov    %edx,-0xc(%ebp)
 51e:	c6 44 05 dc 2d       	movb   $0x2d,-0x24(%ebp,%eax,1)

  while(--i >= 0)
 523:	eb 1d                	jmp    542 <printint+0xa4>
    putc(fd, buf[i]);
 525:	8d 55 dc             	lea    -0x24(%ebp),%edx
 528:	8b 45 f4             	mov    -0xc(%ebp),%eax
 52b:	01 d0                	add    %edx,%eax
 52d:	0f b6 00             	movzbl (%eax),%eax
 530:	0f be c0             	movsbl %al,%eax
 533:	83 ec 08             	sub    $0x8,%esp
 536:	50                   	push   %eax
 537:	ff 75 08             	pushl  0x8(%ebp)
 53a:	e8 3c ff ff ff       	call   47b <putc>
 53f:	83 c4 10             	add    $0x10,%esp
    buf[i++] = digits[x % base];
  }while((x /= base) != 0);
  if(neg)
    buf[i++] = '-';

  while(--i >= 0)
 542:	83 6d f4 01          	subl   $0x1,-0xc(%ebp)
 546:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 54a:	79 d9                	jns    525 <printint+0x87>
    putc(fd, buf[i]);
}
 54c:	90                   	nop
 54d:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 550:	c9                   	leave  
 551:	c3                   	ret    

00000552 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, char *fmt, ...)
{
 552:	55                   	push   %ebp
 553:	89 e5                	mov    %esp,%ebp
 555:	83 ec 28             	sub    $0x28,%esp
  char *s;
  int c, i, state;
  uint *ap;

  state = 0;
 558:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  ap = (uint*)(void*)&fmt + 1;
 55f:	8d 45 0c             	lea    0xc(%ebp),%eax
 562:	83 c0 04             	add    $0x4,%eax
 565:	89 45 e8             	mov    %eax,-0x18(%ebp)
  for(i = 0; fmt[i]; i++){
 568:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 56f:	e9 59 01 00 00       	jmp    6cd <printf+0x17b>
    c = fmt[i] & 0xff;
 574:	8b 55 0c             	mov    0xc(%ebp),%edx
 577:	8b 45 f0             	mov    -0x10(%ebp),%eax
 57a:	01 d0                	add    %edx,%eax
 57c:	0f b6 00             	movzbl (%eax),%eax
 57f:	0f be c0             	movsbl %al,%eax
 582:	25 ff 00 00 00       	and    $0xff,%eax
 587:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    if(state == 0){
 58a:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 58e:	75 2c                	jne    5bc <printf+0x6a>
      if(c == '%'){
 590:	83 7d e4 25          	cmpl   $0x25,-0x1c(%ebp)
 594:	75 0c                	jne    5a2 <printf+0x50>
        state = '%';
 596:	c7 45 ec 25 00 00 00 	movl   $0x25,-0x14(%ebp)
 59d:	e9 27 01 00 00       	jmp    6c9 <printf+0x177>
      } else {
        putc(fd, c);
 5a2:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 5a5:	0f be c0             	movsbl %al,%eax
 5a8:	83 ec 08             	sub    $0x8,%esp
 5ab:	50                   	push   %eax
 5ac:	ff 75 08             	pushl  0x8(%ebp)
 5af:	e8 c7 fe ff ff       	call   47b <putc>
 5b4:	83 c4 10             	add    $0x10,%esp
 5b7:	e9 0d 01 00 00       	jmp    6c9 <printf+0x177>
      }
    } else if(state == '%'){
 5bc:	83 7d ec 25          	cmpl   $0x25,-0x14(%ebp)
 5c0:	0f 85 03 01 00 00    	jne    6c9 <printf+0x177>
      if(c == 'd'){
 5c6:	83 7d e4 64          	cmpl   $0x64,-0x1c(%ebp)
 5ca:	75 1e                	jne    5ea <printf+0x98>
        printint(fd, *ap, 10, 1);
 5cc:	8b 45 e8             	mov    -0x18(%ebp),%eax
 5cf:	8b 00                	mov    (%eax),%eax
 5d1:	6a 01                	push   $0x1
 5d3:	6a 0a                	push   $0xa
 5d5:	50                   	push   %eax
 5d6:	ff 75 08             	pushl  0x8(%ebp)
 5d9:	e8 c0 fe ff ff       	call   49e <printint>
 5de:	83 c4 10             	add    $0x10,%esp
        ap++;
 5e1:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 5e5:	e9 d8 00 00 00       	jmp    6c2 <printf+0x170>
      } else if(c == 'x' || c == 'p'){
 5ea:	83 7d e4 78          	cmpl   $0x78,-0x1c(%ebp)
 5ee:	74 06                	je     5f6 <printf+0xa4>
 5f0:	83 7d e4 70          	cmpl   $0x70,-0x1c(%ebp)
 5f4:	75 1e                	jne    614 <printf+0xc2>
        printint(fd, *ap, 16, 0);
 5f6:	8b 45 e8             	mov    -0x18(%ebp),%eax
 5f9:	8b 00                	mov    (%eax),%eax
 5fb:	6a 00                	push   $0x0
 5fd:	6a 10                	push   $0x10
 5ff:	50                   	push   %eax
 600:	ff 75 08             	pushl  0x8(%ebp)
 603:	e8 96 fe ff ff       	call   49e <printint>
 608:	83 c4 10             	add    $0x10,%esp
        ap++;
 60b:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 60f:	e9 ae 00 00 00       	jmp    6c2 <printf+0x170>
      } else if(c == 's'){
 614:	83 7d e4 73          	cmpl   $0x73,-0x1c(%ebp)
 618:	75 43                	jne    65d <printf+0x10b>
        s = (char*)*ap;
 61a:	8b 45 e8             	mov    -0x18(%ebp),%eax
 61d:	8b 00                	mov    (%eax),%eax
 61f:	89 45 f4             	mov    %eax,-0xc(%ebp)
        ap++;
 622:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
        if(s == 0)
 626:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 62a:	75 25                	jne    651 <printf+0xff>
          s = "(null)";
 62c:	c7 45 f4 2e 09 00 00 	movl   $0x92e,-0xc(%ebp)
        while(*s != 0){
 633:	eb 1c                	jmp    651 <printf+0xff>
          putc(fd, *s);
 635:	8b 45 f4             	mov    -0xc(%ebp),%eax
 638:	0f b6 00             	movzbl (%eax),%eax
 63b:	0f be c0             	movsbl %al,%eax
 63e:	83 ec 08             	sub    $0x8,%esp
 641:	50                   	push   %eax
 642:	ff 75 08             	pushl  0x8(%ebp)
 645:	e8 31 fe ff ff       	call   47b <putc>
 64a:	83 c4 10             	add    $0x10,%esp
          s++;
 64d:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
      } else if(c == 's'){
        s = (char*)*ap;
        ap++;
        if(s == 0)
          s = "(null)";
        while(*s != 0){
 651:	8b 45 f4             	mov    -0xc(%ebp),%eax
 654:	0f b6 00             	movzbl (%eax),%eax
 657:	84 c0                	test   %al,%al
 659:	75 da                	jne    635 <printf+0xe3>
 65b:	eb 65                	jmp    6c2 <printf+0x170>
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 65d:	83 7d e4 63          	cmpl   $0x63,-0x1c(%ebp)
 661:	75 1d                	jne    680 <printf+0x12e>
        putc(fd, *ap);
 663:	8b 45 e8             	mov    -0x18(%ebp),%eax
 666:	8b 00                	mov    (%eax),%eax
 668:	0f be c0             	movsbl %al,%eax
 66b:	83 ec 08             	sub    $0x8,%esp
 66e:	50                   	push   %eax
 66f:	ff 75 08             	pushl  0x8(%ebp)
 672:	e8 04 fe ff ff       	call   47b <putc>
 677:	83 c4 10             	add    $0x10,%esp
        ap++;
 67a:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 67e:	eb 42                	jmp    6c2 <printf+0x170>
      } else if(c == '%'){
 680:	83 7d e4 25          	cmpl   $0x25,-0x1c(%ebp)
 684:	75 17                	jne    69d <printf+0x14b>
        putc(fd, c);
 686:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 689:	0f be c0             	movsbl %al,%eax
 68c:	83 ec 08             	sub    $0x8,%esp
 68f:	50                   	push   %eax
 690:	ff 75 08             	pushl  0x8(%ebp)
 693:	e8 e3 fd ff ff       	call   47b <putc>
 698:	83 c4 10             	add    $0x10,%esp
 69b:	eb 25                	jmp    6c2 <printf+0x170>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 69d:	83 ec 08             	sub    $0x8,%esp
 6a0:	6a 25                	push   $0x25
 6a2:	ff 75 08             	pushl  0x8(%ebp)
 6a5:	e8 d1 fd ff ff       	call   47b <putc>
 6aa:	83 c4 10             	add    $0x10,%esp
        putc(fd, c);
 6ad:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 6b0:	0f be c0             	movsbl %al,%eax
 6b3:	83 ec 08             	sub    $0x8,%esp
 6b6:	50                   	push   %eax
 6b7:	ff 75 08             	pushl  0x8(%ebp)
 6ba:	e8 bc fd ff ff       	call   47b <putc>
 6bf:	83 c4 10             	add    $0x10,%esp
      }
      state = 0;
 6c2:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
 6c9:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
 6cd:	8b 55 0c             	mov    0xc(%ebp),%edx
 6d0:	8b 45 f0             	mov    -0x10(%ebp),%eax
 6d3:	01 d0                	add    %edx,%eax
 6d5:	0f b6 00             	movzbl (%eax),%eax
 6d8:	84 c0                	test   %al,%al
 6da:	0f 85 94 fe ff ff    	jne    574 <printf+0x22>
        putc(fd, c);
      }
      state = 0;
    }
  }
}
 6e0:	90                   	nop
 6e1:	c9                   	leave  
 6e2:	c3                   	ret    

000006e3 <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 6e3:	55                   	push   %ebp
 6e4:	89 e5                	mov    %esp,%ebp
 6e6:	83 ec 10             	sub    $0x10,%esp
  Header *bp, *p;

  bp = (Header*)ap - 1;
 6e9:	8b 45 08             	mov    0x8(%ebp),%eax
 6ec:	83 e8 08             	sub    $0x8,%eax
 6ef:	89 45 f8             	mov    %eax,-0x8(%ebp)
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 6f2:	a1 c8 0b 00 00       	mov    0xbc8,%eax
 6f7:	89 45 fc             	mov    %eax,-0x4(%ebp)
 6fa:	eb 24                	jmp    720 <free+0x3d>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 6fc:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6ff:	8b 00                	mov    (%eax),%eax
 701:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 704:	77 12                	ja     718 <free+0x35>
 706:	8b 45 f8             	mov    -0x8(%ebp),%eax
 709:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 70c:	77 24                	ja     732 <free+0x4f>
 70e:	8b 45 fc             	mov    -0x4(%ebp),%eax
 711:	8b 00                	mov    (%eax),%eax
 713:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 716:	77 1a                	ja     732 <free+0x4f>
free(void *ap)
{
  Header *bp, *p;

  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 718:	8b 45 fc             	mov    -0x4(%ebp),%eax
 71b:	8b 00                	mov    (%eax),%eax
 71d:	89 45 fc             	mov    %eax,-0x4(%ebp)
 720:	8b 45 f8             	mov    -0x8(%ebp),%eax
 723:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 726:	76 d4                	jbe    6fc <free+0x19>
 728:	8b 45 fc             	mov    -0x4(%ebp),%eax
 72b:	8b 00                	mov    (%eax),%eax
 72d:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 730:	76 ca                	jbe    6fc <free+0x19>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if(bp + bp->s.size == p->s.ptr){
 732:	8b 45 f8             	mov    -0x8(%ebp),%eax
 735:	8b 40 04             	mov    0x4(%eax),%eax
 738:	8d 14 c5 00 00 00 00 	lea    0x0(,%eax,8),%edx
 73f:	8b 45 f8             	mov    -0x8(%ebp),%eax
 742:	01 c2                	add    %eax,%edx
 744:	8b 45 fc             	mov    -0x4(%ebp),%eax
 747:	8b 00                	mov    (%eax),%eax
 749:	39 c2                	cmp    %eax,%edx
 74b:	75 24                	jne    771 <free+0x8e>
    bp->s.size += p->s.ptr->s.size;
 74d:	8b 45 f8             	mov    -0x8(%ebp),%eax
 750:	8b 50 04             	mov    0x4(%eax),%edx
 753:	8b 45 fc             	mov    -0x4(%ebp),%eax
 756:	8b 00                	mov    (%eax),%eax
 758:	8b 40 04             	mov    0x4(%eax),%eax
 75b:	01 c2                	add    %eax,%edx
 75d:	8b 45 f8             	mov    -0x8(%ebp),%eax
 760:	89 50 04             	mov    %edx,0x4(%eax)
    bp->s.ptr = p->s.ptr->s.ptr;
 763:	8b 45 fc             	mov    -0x4(%ebp),%eax
 766:	8b 00                	mov    (%eax),%eax
 768:	8b 10                	mov    (%eax),%edx
 76a:	8b 45 f8             	mov    -0x8(%ebp),%eax
 76d:	89 10                	mov    %edx,(%eax)
 76f:	eb 0a                	jmp    77b <free+0x98>
  } else
    bp->s.ptr = p->s.ptr;
 771:	8b 45 fc             	mov    -0x4(%ebp),%eax
 774:	8b 10                	mov    (%eax),%edx
 776:	8b 45 f8             	mov    -0x8(%ebp),%eax
 779:	89 10                	mov    %edx,(%eax)
  if(p + p->s.size == bp){
 77b:	8b 45 fc             	mov    -0x4(%ebp),%eax
 77e:	8b 40 04             	mov    0x4(%eax),%eax
 781:	8d 14 c5 00 00 00 00 	lea    0x0(,%eax,8),%edx
 788:	8b 45 fc             	mov    -0x4(%ebp),%eax
 78b:	01 d0                	add    %edx,%eax
 78d:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 790:	75 20                	jne    7b2 <free+0xcf>
    p->s.size += bp->s.size;
 792:	8b 45 fc             	mov    -0x4(%ebp),%eax
 795:	8b 50 04             	mov    0x4(%eax),%edx
 798:	8b 45 f8             	mov    -0x8(%ebp),%eax
 79b:	8b 40 04             	mov    0x4(%eax),%eax
 79e:	01 c2                	add    %eax,%edx
 7a0:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7a3:	89 50 04             	mov    %edx,0x4(%eax)
    p->s.ptr = bp->s.ptr;
 7a6:	8b 45 f8             	mov    -0x8(%ebp),%eax
 7a9:	8b 10                	mov    (%eax),%edx
 7ab:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7ae:	89 10                	mov    %edx,(%eax)
 7b0:	eb 08                	jmp    7ba <free+0xd7>
  } else
    p->s.ptr = bp;
 7b2:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7b5:	8b 55 f8             	mov    -0x8(%ebp),%edx
 7b8:	89 10                	mov    %edx,(%eax)
  freep = p;
 7ba:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7bd:	a3 c8 0b 00 00       	mov    %eax,0xbc8
}
 7c2:	90                   	nop
 7c3:	c9                   	leave  
 7c4:	c3                   	ret    

000007c5 <morecore>:

static Header*
morecore(uint nu)
{
 7c5:	55                   	push   %ebp
 7c6:	89 e5                	mov    %esp,%ebp
 7c8:	83 ec 18             	sub    $0x18,%esp
  char *p;
  Header *hp;

  if(nu < 4096)
 7cb:	81 7d 08 ff 0f 00 00 	cmpl   $0xfff,0x8(%ebp)
 7d2:	77 07                	ja     7db <morecore+0x16>
    nu = 4096;
 7d4:	c7 45 08 00 10 00 00 	movl   $0x1000,0x8(%ebp)
  p = sbrk(nu * sizeof(Header));
 7db:	8b 45 08             	mov    0x8(%ebp),%eax
 7de:	c1 e0 03             	shl    $0x3,%eax
 7e1:	83 ec 0c             	sub    $0xc,%esp
 7e4:	50                   	push   %eax
 7e5:	e8 31 fc ff ff       	call   41b <sbrk>
 7ea:	83 c4 10             	add    $0x10,%esp
 7ed:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(p == (char*)-1)
 7f0:	83 7d f4 ff          	cmpl   $0xffffffff,-0xc(%ebp)
 7f4:	75 07                	jne    7fd <morecore+0x38>
    return 0;
 7f6:	b8 00 00 00 00       	mov    $0x0,%eax
 7fb:	eb 26                	jmp    823 <morecore+0x5e>
  hp = (Header*)p;
 7fd:	8b 45 f4             	mov    -0xc(%ebp),%eax
 800:	89 45 f0             	mov    %eax,-0x10(%ebp)
  hp->s.size = nu;
 803:	8b 45 f0             	mov    -0x10(%ebp),%eax
 806:	8b 55 08             	mov    0x8(%ebp),%edx
 809:	89 50 04             	mov    %edx,0x4(%eax)
  free((void*)(hp + 1));
 80c:	8b 45 f0             	mov    -0x10(%ebp),%eax
 80f:	83 c0 08             	add    $0x8,%eax
 812:	83 ec 0c             	sub    $0xc,%esp
 815:	50                   	push   %eax
 816:	e8 c8 fe ff ff       	call   6e3 <free>
 81b:	83 c4 10             	add    $0x10,%esp
  return freep;
 81e:	a1 c8 0b 00 00       	mov    0xbc8,%eax
}
 823:	c9                   	leave  
 824:	c3                   	ret    

00000825 <malloc>:

void*
malloc(uint nbytes)
{
 825:	55                   	push   %ebp
 826:	89 e5                	mov    %esp,%ebp
 828:	83 ec 18             	sub    $0x18,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 82b:	8b 45 08             	mov    0x8(%ebp),%eax
 82e:	83 c0 07             	add    $0x7,%eax
 831:	c1 e8 03             	shr    $0x3,%eax
 834:	83 c0 01             	add    $0x1,%eax
 837:	89 45 ec             	mov    %eax,-0x14(%ebp)
  if((prevp = freep) == 0){
 83a:	a1 c8 0b 00 00       	mov    0xbc8,%eax
 83f:	89 45 f0             	mov    %eax,-0x10(%ebp)
 842:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 846:	75 23                	jne    86b <malloc+0x46>
    base.s.ptr = freep = prevp = &base;
 848:	c7 45 f0 c0 0b 00 00 	movl   $0xbc0,-0x10(%ebp)
 84f:	8b 45 f0             	mov    -0x10(%ebp),%eax
 852:	a3 c8 0b 00 00       	mov    %eax,0xbc8
 857:	a1 c8 0b 00 00       	mov    0xbc8,%eax
 85c:	a3 c0 0b 00 00       	mov    %eax,0xbc0
    base.s.size = 0;
 861:	c7 05 c4 0b 00 00 00 	movl   $0x0,0xbc4
 868:	00 00 00 
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 86b:	8b 45 f0             	mov    -0x10(%ebp),%eax
 86e:	8b 00                	mov    (%eax),%eax
 870:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if(p->s.size >= nunits){
 873:	8b 45 f4             	mov    -0xc(%ebp),%eax
 876:	8b 40 04             	mov    0x4(%eax),%eax
 879:	3b 45 ec             	cmp    -0x14(%ebp),%eax
 87c:	72 4d                	jb     8cb <malloc+0xa6>
      if(p->s.size == nunits)
 87e:	8b 45 f4             	mov    -0xc(%ebp),%eax
 881:	8b 40 04             	mov    0x4(%eax),%eax
 884:	3b 45 ec             	cmp    -0x14(%ebp),%eax
 887:	75 0c                	jne    895 <malloc+0x70>
        prevp->s.ptr = p->s.ptr;
 889:	8b 45 f4             	mov    -0xc(%ebp),%eax
 88c:	8b 10                	mov    (%eax),%edx
 88e:	8b 45 f0             	mov    -0x10(%ebp),%eax
 891:	89 10                	mov    %edx,(%eax)
 893:	eb 26                	jmp    8bb <malloc+0x96>
      else {
        p->s.size -= nunits;
 895:	8b 45 f4             	mov    -0xc(%ebp),%eax
 898:	8b 40 04             	mov    0x4(%eax),%eax
 89b:	2b 45 ec             	sub    -0x14(%ebp),%eax
 89e:	89 c2                	mov    %eax,%edx
 8a0:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8a3:	89 50 04             	mov    %edx,0x4(%eax)
        p += p->s.size;
 8a6:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8a9:	8b 40 04             	mov    0x4(%eax),%eax
 8ac:	c1 e0 03             	shl    $0x3,%eax
 8af:	01 45 f4             	add    %eax,-0xc(%ebp)
        p->s.size = nunits;
 8b2:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8b5:	8b 55 ec             	mov    -0x14(%ebp),%edx
 8b8:	89 50 04             	mov    %edx,0x4(%eax)
      }
      freep = prevp;
 8bb:	8b 45 f0             	mov    -0x10(%ebp),%eax
 8be:	a3 c8 0b 00 00       	mov    %eax,0xbc8
      return (void*)(p + 1);
 8c3:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8c6:	83 c0 08             	add    $0x8,%eax
 8c9:	eb 3b                	jmp    906 <malloc+0xe1>
    }
    if(p == freep)
 8cb:	a1 c8 0b 00 00       	mov    0xbc8,%eax
 8d0:	39 45 f4             	cmp    %eax,-0xc(%ebp)
 8d3:	75 1e                	jne    8f3 <malloc+0xce>
      if((p = morecore(nunits)) == 0)
 8d5:	83 ec 0c             	sub    $0xc,%esp
 8d8:	ff 75 ec             	pushl  -0x14(%ebp)
 8db:	e8 e5 fe ff ff       	call   7c5 <morecore>
 8e0:	83 c4 10             	add    $0x10,%esp
 8e3:	89 45 f4             	mov    %eax,-0xc(%ebp)
 8e6:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 8ea:	75 07                	jne    8f3 <malloc+0xce>
        return 0;
 8ec:	b8 00 00 00 00       	mov    $0x0,%eax
 8f1:	eb 13                	jmp    906 <malloc+0xe1>
  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
  if((prevp = freep) == 0){
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 8f3:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8f6:	89 45 f0             	mov    %eax,-0x10(%ebp)
 8f9:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8fc:	8b 00                	mov    (%eax),%eax
 8fe:	89 45 f4             	mov    %eax,-0xc(%ebp)
      return (void*)(p + 1);
    }
    if(p == freep)
      if((p = morecore(nunits)) == 0)
        return 0;
  }
 901:	e9 6d ff ff ff       	jmp    873 <malloc+0x4e>
}
 906:	c9                   	leave  
 907:	c3                   	ret    
