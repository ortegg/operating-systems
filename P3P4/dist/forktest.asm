
_forktest:     file format elf32-i386


Disassembly of section .text:

00000000 <printf>:

#define N  1000

void
printf(int fd, char *s, ...)
{
   0:	55                   	push   %ebp
   1:	89 e5                	mov    %esp,%ebp
   3:	83 ec 08             	sub    $0x8,%esp
  write(fd, s, strlen(s));
   6:	83 ec 0c             	sub    $0xc,%esp
   9:	ff 75 0c             	pushl  0xc(%ebp)
   c:	e8 ec 01 00 00       	call   1fd <strlen>
  11:	83 c4 10             	add    $0x10,%esp
  14:	83 ec 04             	sub    $0x4,%esp
  17:	50                   	push   %eax
  18:	ff 75 0c             	pushl  0xc(%ebp)
  1b:	ff 75 08             	pushl  0x8(%ebp)
  1e:	e8 e9 03 00 00       	call   40c <write>
  23:	83 c4 10             	add    $0x10,%esp
}
  26:	90                   	nop
  27:	c9                   	leave  
  28:	c3                   	ret    

00000029 <forktest>:

void
forktest(void)
{
  29:	55                   	push   %ebp
  2a:	89 e5                	mov    %esp,%ebp
  2c:	83 ec 18             	sub    $0x18,%esp
  int n, pid;

  int x = 5;
  2f:	c7 45 f0 05 00 00 00 	movl   $0x5,-0x10(%ebp)
  printf(1, "fork test\n");
  36:	83 ec 08             	sub    $0x8,%esp
  39:	68 d4 04 00 00       	push   $0x4d4
  3e:	6a 01                	push   $0x1
  40:	e8 bb ff ff ff       	call   0 <printf>
  45:	83 c4 10             	add    $0x10,%esp

    printf(2, "blah %d\n", x);
  48:	83 ec 04             	sub    $0x4,%esp
  4b:	ff 75 f0             	pushl  -0x10(%ebp)
  4e:	68 df 04 00 00       	push   $0x4df
  53:	6a 02                	push   $0x2
  55:	e8 a6 ff ff ff       	call   0 <printf>
  5a:	83 c4 10             	add    $0x10,%esp
  for(n=0; n<N; n++){
  5d:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  64:	eb 32                	jmp    98 <forktest+0x6f>
    pid = fork();
  66:	e8 79 03 00 00       	call   3e4 <fork>
  6b:	89 45 ec             	mov    %eax,-0x14(%ebp)
    printf(1, "fork returned %d \n", pid);
  6e:	83 ec 04             	sub    $0x4,%esp
  71:	ff 75 ec             	pushl  -0x14(%ebp)
  74:	68 e8 04 00 00       	push   $0x4e8
  79:	6a 01                	push   $0x1
  7b:	e8 80 ff ff ff       	call   0 <printf>
  80:	83 c4 10             	add    $0x10,%esp
    if(pid < 0)
  83:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
  87:	78 1a                	js     a3 <forktest+0x7a>
      break;
    if(pid == 0)
  89:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
  8d:	75 05                	jne    94 <forktest+0x6b>
      exit();
  8f:	e8 58 03 00 00       	call   3ec <exit>

  int x = 5;
  printf(1, "fork test\n");

    printf(2, "blah %d\n", x);
  for(n=0; n<N; n++){
  94:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
  98:	81 7d f4 e7 03 00 00 	cmpl   $0x3e7,-0xc(%ebp)
  9f:	7e c5                	jle    66 <forktest+0x3d>
  a1:	eb 01                	jmp    a4 <forktest+0x7b>
    pid = fork();
    printf(1, "fork returned %d \n", pid);
    if(pid < 0)
      break;
  a3:	90                   	nop
    if(pid == 0)
      exit();
  }
  printf(1, "fork..\n"); 
  a4:	83 ec 08             	sub    $0x8,%esp
  a7:	68 fb 04 00 00       	push   $0x4fb
  ac:	6a 01                	push   $0x1
  ae:	e8 4d ff ff ff       	call   0 <printf>
  b3:	83 c4 10             	add    $0x10,%esp
  
  if(n == N){
  b6:	81 7d f4 e8 03 00 00 	cmpl   $0x3e8,-0xc(%ebp)
  bd:	75 52                	jne    111 <forktest+0xe8>
    printf(1, "fork claimed to work N times!\n", N);
  bf:	83 ec 04             	sub    $0x4,%esp
  c2:	68 e8 03 00 00       	push   $0x3e8
  c7:	68 04 05 00 00       	push   $0x504
  cc:	6a 01                	push   $0x1
  ce:	e8 2d ff ff ff       	call   0 <printf>
  d3:	83 c4 10             	add    $0x10,%esp
    exit();
  d6:	e8 11 03 00 00       	call   3ec <exit>
  }
  
  for(; n > 0; n--){
    printf(1, "reaping child!\n");
  db:	83 ec 08             	sub    $0x8,%esp
  de:	68 23 05 00 00       	push   $0x523
  e3:	6a 01                	push   $0x1
  e5:	e8 16 ff ff ff       	call   0 <printf>
  ea:	83 c4 10             	add    $0x10,%esp
    if(wait() < 0){
  ed:	e8 02 03 00 00       	call   3f4 <wait>
  f2:	85 c0                	test   %eax,%eax
  f4:	79 17                	jns    10d <forktest+0xe4>
      printf(1, "wait stopped early\n");
  f6:	83 ec 08             	sub    $0x8,%esp
  f9:	68 33 05 00 00       	push   $0x533
  fe:	6a 01                	push   $0x1
 100:	e8 fb fe ff ff       	call   0 <printf>
 105:	83 c4 10             	add    $0x10,%esp
      exit();
 108:	e8 df 02 00 00       	call   3ec <exit>
  if(n == N){
    printf(1, "fork claimed to work N times!\n", N);
    exit();
  }
  
  for(; n > 0; n--){
 10d:	83 6d f4 01          	subl   $0x1,-0xc(%ebp)
 111:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 115:	7f c4                	jg     db <forktest+0xb2>
      printf(1, "wait stopped early\n");
      exit();
    }
  }
  
  if(wait() != -1){
 117:	e8 d8 02 00 00       	call   3f4 <wait>
 11c:	83 f8 ff             	cmp    $0xffffffff,%eax
 11f:	74 17                	je     138 <forktest+0x10f>
    printf(1, "wait got too many\n");
 121:	83 ec 08             	sub    $0x8,%esp
 124:	68 47 05 00 00       	push   $0x547
 129:	6a 01                	push   $0x1
 12b:	e8 d0 fe ff ff       	call   0 <printf>
 130:	83 c4 10             	add    $0x10,%esp
    exit();
 133:	e8 b4 02 00 00       	call   3ec <exit>
  }
  
  printf(1, "fork test OK\n");
 138:	83 ec 08             	sub    $0x8,%esp
 13b:	68 5a 05 00 00       	push   $0x55a
 140:	6a 01                	push   $0x1
 142:	e8 b9 fe ff ff       	call   0 <printf>
 147:	83 c4 10             	add    $0x10,%esp
}
 14a:	90                   	nop
 14b:	c9                   	leave  
 14c:	c3                   	ret    

0000014d <main>:

int
main(void)
{
 14d:	8d 4c 24 04          	lea    0x4(%esp),%ecx
 151:	83 e4 f0             	and    $0xfffffff0,%esp
 154:	ff 71 fc             	pushl  -0x4(%ecx)
 157:	55                   	push   %ebp
 158:	89 e5                	mov    %esp,%ebp
 15a:	51                   	push   %ecx
 15b:	83 ec 04             	sub    $0x4,%esp
  forktest();
 15e:	e8 c6 fe ff ff       	call   29 <forktest>
  exit();
 163:	e8 84 02 00 00       	call   3ec <exit>

00000168 <stosb>:
               "cc");
}

static inline void
stosb(void *addr, int data, int cnt)
{
 168:	55                   	push   %ebp
 169:	89 e5                	mov    %esp,%ebp
 16b:	57                   	push   %edi
 16c:	53                   	push   %ebx
  asm volatile("cld; rep stosb" :
 16d:	8b 4d 08             	mov    0x8(%ebp),%ecx
 170:	8b 55 10             	mov    0x10(%ebp),%edx
 173:	8b 45 0c             	mov    0xc(%ebp),%eax
 176:	89 cb                	mov    %ecx,%ebx
 178:	89 df                	mov    %ebx,%edi
 17a:	89 d1                	mov    %edx,%ecx
 17c:	fc                   	cld    
 17d:	f3 aa                	rep stos %al,%es:(%edi)
 17f:	89 ca                	mov    %ecx,%edx
 181:	89 fb                	mov    %edi,%ebx
 183:	89 5d 08             	mov    %ebx,0x8(%ebp)
 186:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
 189:	90                   	nop
 18a:	5b                   	pop    %ebx
 18b:	5f                   	pop    %edi
 18c:	5d                   	pop    %ebp
 18d:	c3                   	ret    

0000018e <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, char *t)
{
 18e:	55                   	push   %ebp
 18f:	89 e5                	mov    %esp,%ebp
 191:	83 ec 10             	sub    $0x10,%esp
  char *os;

  os = s;
 194:	8b 45 08             	mov    0x8(%ebp),%eax
 197:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while((*s++ = *t++) != 0)
 19a:	90                   	nop
 19b:	8b 45 08             	mov    0x8(%ebp),%eax
 19e:	8d 50 01             	lea    0x1(%eax),%edx
 1a1:	89 55 08             	mov    %edx,0x8(%ebp)
 1a4:	8b 55 0c             	mov    0xc(%ebp),%edx
 1a7:	8d 4a 01             	lea    0x1(%edx),%ecx
 1aa:	89 4d 0c             	mov    %ecx,0xc(%ebp)
 1ad:	0f b6 12             	movzbl (%edx),%edx
 1b0:	88 10                	mov    %dl,(%eax)
 1b2:	0f b6 00             	movzbl (%eax),%eax
 1b5:	84 c0                	test   %al,%al
 1b7:	75 e2                	jne    19b <strcpy+0xd>
    ;
  return os;
 1b9:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 1bc:	c9                   	leave  
 1bd:	c3                   	ret    

000001be <strcmp>:

int
strcmp(const char *p, const char *q)
{
 1be:	55                   	push   %ebp
 1bf:	89 e5                	mov    %esp,%ebp
  while(*p && *p == *q)
 1c1:	eb 08                	jmp    1cb <strcmp+0xd>
    p++, q++;
 1c3:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 1c7:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
  while(*p && *p == *q)
 1cb:	8b 45 08             	mov    0x8(%ebp),%eax
 1ce:	0f b6 00             	movzbl (%eax),%eax
 1d1:	84 c0                	test   %al,%al
 1d3:	74 10                	je     1e5 <strcmp+0x27>
 1d5:	8b 45 08             	mov    0x8(%ebp),%eax
 1d8:	0f b6 10             	movzbl (%eax),%edx
 1db:	8b 45 0c             	mov    0xc(%ebp),%eax
 1de:	0f b6 00             	movzbl (%eax),%eax
 1e1:	38 c2                	cmp    %al,%dl
 1e3:	74 de                	je     1c3 <strcmp+0x5>
    p++, q++;
  return (uchar)*p - (uchar)*q;
 1e5:	8b 45 08             	mov    0x8(%ebp),%eax
 1e8:	0f b6 00             	movzbl (%eax),%eax
 1eb:	0f b6 d0             	movzbl %al,%edx
 1ee:	8b 45 0c             	mov    0xc(%ebp),%eax
 1f1:	0f b6 00             	movzbl (%eax),%eax
 1f4:	0f b6 c0             	movzbl %al,%eax
 1f7:	29 c2                	sub    %eax,%edx
 1f9:	89 d0                	mov    %edx,%eax
}
 1fb:	5d                   	pop    %ebp
 1fc:	c3                   	ret    

000001fd <strlen>:

uint
strlen(char *s)
{
 1fd:	55                   	push   %ebp
 1fe:	89 e5                	mov    %esp,%ebp
 200:	83 ec 10             	sub    $0x10,%esp
  int n;

  for(n = 0; s[n]; n++)
 203:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
 20a:	eb 04                	jmp    210 <strlen+0x13>
 20c:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
 210:	8b 55 fc             	mov    -0x4(%ebp),%edx
 213:	8b 45 08             	mov    0x8(%ebp),%eax
 216:	01 d0                	add    %edx,%eax
 218:	0f b6 00             	movzbl (%eax),%eax
 21b:	84 c0                	test   %al,%al
 21d:	75 ed                	jne    20c <strlen+0xf>
    ;
  return n;
 21f:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 222:	c9                   	leave  
 223:	c3                   	ret    

00000224 <memset>:

void*
memset(void *dst, int c, uint n)
{
 224:	55                   	push   %ebp
 225:	89 e5                	mov    %esp,%ebp
  stosb(dst, c, n);
 227:	8b 45 10             	mov    0x10(%ebp),%eax
 22a:	50                   	push   %eax
 22b:	ff 75 0c             	pushl  0xc(%ebp)
 22e:	ff 75 08             	pushl  0x8(%ebp)
 231:	e8 32 ff ff ff       	call   168 <stosb>
 236:	83 c4 0c             	add    $0xc,%esp
  return dst;
 239:	8b 45 08             	mov    0x8(%ebp),%eax
}
 23c:	c9                   	leave  
 23d:	c3                   	ret    

0000023e <strchr>:

char*
strchr(const char *s, char c)
{
 23e:	55                   	push   %ebp
 23f:	89 e5                	mov    %esp,%ebp
 241:	83 ec 04             	sub    $0x4,%esp
 244:	8b 45 0c             	mov    0xc(%ebp),%eax
 247:	88 45 fc             	mov    %al,-0x4(%ebp)
  for(; *s; s++)
 24a:	eb 14                	jmp    260 <strchr+0x22>
    if(*s == c)
 24c:	8b 45 08             	mov    0x8(%ebp),%eax
 24f:	0f b6 00             	movzbl (%eax),%eax
 252:	3a 45 fc             	cmp    -0x4(%ebp),%al
 255:	75 05                	jne    25c <strchr+0x1e>
      return (char*)s;
 257:	8b 45 08             	mov    0x8(%ebp),%eax
 25a:	eb 13                	jmp    26f <strchr+0x31>
}

char*
strchr(const char *s, char c)
{
  for(; *s; s++)
 25c:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 260:	8b 45 08             	mov    0x8(%ebp),%eax
 263:	0f b6 00             	movzbl (%eax),%eax
 266:	84 c0                	test   %al,%al
 268:	75 e2                	jne    24c <strchr+0xe>
    if(*s == c)
      return (char*)s;
  return 0;
 26a:	b8 00 00 00 00       	mov    $0x0,%eax
}
 26f:	c9                   	leave  
 270:	c3                   	ret    

00000271 <gets>:

char*
gets(char *buf, int max)
{
 271:	55                   	push   %ebp
 272:	89 e5                	mov    %esp,%ebp
 274:	83 ec 18             	sub    $0x18,%esp
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 277:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 27e:	eb 42                	jmp    2c2 <gets+0x51>
    cc = read(0, &c, 1);
 280:	83 ec 04             	sub    $0x4,%esp
 283:	6a 01                	push   $0x1
 285:	8d 45 ef             	lea    -0x11(%ebp),%eax
 288:	50                   	push   %eax
 289:	6a 00                	push   $0x0
 28b:	e8 74 01 00 00       	call   404 <read>
 290:	83 c4 10             	add    $0x10,%esp
 293:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if(cc < 1)
 296:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 29a:	7e 33                	jle    2cf <gets+0x5e>
      break;
    buf[i++] = c;
 29c:	8b 45 f4             	mov    -0xc(%ebp),%eax
 29f:	8d 50 01             	lea    0x1(%eax),%edx
 2a2:	89 55 f4             	mov    %edx,-0xc(%ebp)
 2a5:	89 c2                	mov    %eax,%edx
 2a7:	8b 45 08             	mov    0x8(%ebp),%eax
 2aa:	01 c2                	add    %eax,%edx
 2ac:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 2b0:	88 02                	mov    %al,(%edx)
    if(c == '\n' || c == '\r')
 2b2:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 2b6:	3c 0a                	cmp    $0xa,%al
 2b8:	74 16                	je     2d0 <gets+0x5f>
 2ba:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 2be:	3c 0d                	cmp    $0xd,%al
 2c0:	74 0e                	je     2d0 <gets+0x5f>
gets(char *buf, int max)
{
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 2c2:	8b 45 f4             	mov    -0xc(%ebp),%eax
 2c5:	83 c0 01             	add    $0x1,%eax
 2c8:	3b 45 0c             	cmp    0xc(%ebp),%eax
 2cb:	7c b3                	jl     280 <gets+0xf>
 2cd:	eb 01                	jmp    2d0 <gets+0x5f>
    cc = read(0, &c, 1);
    if(cc < 1)
      break;
 2cf:	90                   	nop
    buf[i++] = c;
    if(c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 2d0:	8b 55 f4             	mov    -0xc(%ebp),%edx
 2d3:	8b 45 08             	mov    0x8(%ebp),%eax
 2d6:	01 d0                	add    %edx,%eax
 2d8:	c6 00 00             	movb   $0x0,(%eax)
  return buf;
 2db:	8b 45 08             	mov    0x8(%ebp),%eax
}
 2de:	c9                   	leave  
 2df:	c3                   	ret    

000002e0 <stat>:

int
stat(char *n, struct stat *st)
{
 2e0:	55                   	push   %ebp
 2e1:	89 e5                	mov    %esp,%ebp
 2e3:	83 ec 18             	sub    $0x18,%esp
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 2e6:	83 ec 08             	sub    $0x8,%esp
 2e9:	6a 00                	push   $0x0
 2eb:	ff 75 08             	pushl  0x8(%ebp)
 2ee:	e8 39 01 00 00       	call   42c <open>
 2f3:	83 c4 10             	add    $0x10,%esp
 2f6:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(fd < 0)
 2f9:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 2fd:	79 07                	jns    306 <stat+0x26>
    return -1;
 2ff:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 304:	eb 25                	jmp    32b <stat+0x4b>
  r = fstat(fd, st);
 306:	83 ec 08             	sub    $0x8,%esp
 309:	ff 75 0c             	pushl  0xc(%ebp)
 30c:	ff 75 f4             	pushl  -0xc(%ebp)
 30f:	e8 30 01 00 00       	call   444 <fstat>
 314:	83 c4 10             	add    $0x10,%esp
 317:	89 45 f0             	mov    %eax,-0x10(%ebp)
  close(fd);
 31a:	83 ec 0c             	sub    $0xc,%esp
 31d:	ff 75 f4             	pushl  -0xc(%ebp)
 320:	e8 ef 00 00 00       	call   414 <close>
 325:	83 c4 10             	add    $0x10,%esp
  return r;
 328:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
 32b:	c9                   	leave  
 32c:	c3                   	ret    

0000032d <atoi>:

int
atoi(const char *s)
{
 32d:	55                   	push   %ebp
 32e:	89 e5                	mov    %esp,%ebp
 330:	83 ec 10             	sub    $0x10,%esp
  int n, sign;

  n = 0;
 333:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  while(*s == ' ') s++;
 33a:	eb 04                	jmp    340 <atoi+0x13>
 33c:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 340:	8b 45 08             	mov    0x8(%ebp),%eax
 343:	0f b6 00             	movzbl (%eax),%eax
 346:	3c 20                	cmp    $0x20,%al
 348:	74 f2                	je     33c <atoi+0xf>
  sign = (*s == '-') ? -1 : 1;
 34a:	8b 45 08             	mov    0x8(%ebp),%eax
 34d:	0f b6 00             	movzbl (%eax),%eax
 350:	3c 2d                	cmp    $0x2d,%al
 352:	75 07                	jne    35b <atoi+0x2e>
 354:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 359:	eb 05                	jmp    360 <atoi+0x33>
 35b:	b8 01 00 00 00       	mov    $0x1,%eax
 360:	89 45 f8             	mov    %eax,-0x8(%ebp)
  while('0' <= *s && *s <= '9')
 363:	eb 25                	jmp    38a <atoi+0x5d>
    n = n*10 + *s++ - '0';
 365:	8b 55 fc             	mov    -0x4(%ebp),%edx
 368:	89 d0                	mov    %edx,%eax
 36a:	c1 e0 02             	shl    $0x2,%eax
 36d:	01 d0                	add    %edx,%eax
 36f:	01 c0                	add    %eax,%eax
 371:	89 c1                	mov    %eax,%ecx
 373:	8b 45 08             	mov    0x8(%ebp),%eax
 376:	8d 50 01             	lea    0x1(%eax),%edx
 379:	89 55 08             	mov    %edx,0x8(%ebp)
 37c:	0f b6 00             	movzbl (%eax),%eax
 37f:	0f be c0             	movsbl %al,%eax
 382:	01 c8                	add    %ecx,%eax
 384:	83 e8 30             	sub    $0x30,%eax
 387:	89 45 fc             	mov    %eax,-0x4(%ebp)
  int n, sign;

  n = 0;
  while(*s == ' ') s++;
  sign = (*s == '-') ? -1 : 1;
  while('0' <= *s && *s <= '9')
 38a:	8b 45 08             	mov    0x8(%ebp),%eax
 38d:	0f b6 00             	movzbl (%eax),%eax
 390:	3c 2f                	cmp    $0x2f,%al
 392:	7e 0a                	jle    39e <atoi+0x71>
 394:	8b 45 08             	mov    0x8(%ebp),%eax
 397:	0f b6 00             	movzbl (%eax),%eax
 39a:	3c 39                	cmp    $0x39,%al
 39c:	7e c7                	jle    365 <atoi+0x38>
    n = n*10 + *s++ - '0';
  return sign*n;
 39e:	8b 45 f8             	mov    -0x8(%ebp),%eax
 3a1:	0f af 45 fc          	imul   -0x4(%ebp),%eax
}
 3a5:	c9                   	leave  
 3a6:	c3                   	ret    

000003a7 <memmove>:

void*
memmove(void *vdst, void *vsrc, int n)
{
 3a7:	55                   	push   %ebp
 3a8:	89 e5                	mov    %esp,%ebp
 3aa:	83 ec 10             	sub    $0x10,%esp
  char *dst, *src;
  
  dst = vdst;
 3ad:	8b 45 08             	mov    0x8(%ebp),%eax
 3b0:	89 45 fc             	mov    %eax,-0x4(%ebp)
  src = vsrc;
 3b3:	8b 45 0c             	mov    0xc(%ebp),%eax
 3b6:	89 45 f8             	mov    %eax,-0x8(%ebp)
  while(n-- > 0)
 3b9:	eb 17                	jmp    3d2 <memmove+0x2b>
    *dst++ = *src++;
 3bb:	8b 45 fc             	mov    -0x4(%ebp),%eax
 3be:	8d 50 01             	lea    0x1(%eax),%edx
 3c1:	89 55 fc             	mov    %edx,-0x4(%ebp)
 3c4:	8b 55 f8             	mov    -0x8(%ebp),%edx
 3c7:	8d 4a 01             	lea    0x1(%edx),%ecx
 3ca:	89 4d f8             	mov    %ecx,-0x8(%ebp)
 3cd:	0f b6 12             	movzbl (%edx),%edx
 3d0:	88 10                	mov    %dl,(%eax)
{
  char *dst, *src;
  
  dst = vdst;
  src = vsrc;
  while(n-- > 0)
 3d2:	8b 45 10             	mov    0x10(%ebp),%eax
 3d5:	8d 50 ff             	lea    -0x1(%eax),%edx
 3d8:	89 55 10             	mov    %edx,0x10(%ebp)
 3db:	85 c0                	test   %eax,%eax
 3dd:	7f dc                	jg     3bb <memmove+0x14>
    *dst++ = *src++;
  return vdst;
 3df:	8b 45 08             	mov    0x8(%ebp),%eax
}
 3e2:	c9                   	leave  
 3e3:	c3                   	ret    

000003e4 <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
 3e4:	b8 01 00 00 00       	mov    $0x1,%eax
 3e9:	cd 40                	int    $0x40
 3eb:	c3                   	ret    

000003ec <exit>:
SYSCALL(exit)
 3ec:	b8 02 00 00 00       	mov    $0x2,%eax
 3f1:	cd 40                	int    $0x40
 3f3:	c3                   	ret    

000003f4 <wait>:
SYSCALL(wait)
 3f4:	b8 03 00 00 00       	mov    $0x3,%eax
 3f9:	cd 40                	int    $0x40
 3fb:	c3                   	ret    

000003fc <pipe>:
SYSCALL(pipe)
 3fc:	b8 04 00 00 00       	mov    $0x4,%eax
 401:	cd 40                	int    $0x40
 403:	c3                   	ret    

00000404 <read>:
SYSCALL(read)
 404:	b8 05 00 00 00       	mov    $0x5,%eax
 409:	cd 40                	int    $0x40
 40b:	c3                   	ret    

0000040c <write>:
SYSCALL(write)
 40c:	b8 10 00 00 00       	mov    $0x10,%eax
 411:	cd 40                	int    $0x40
 413:	c3                   	ret    

00000414 <close>:
SYSCALL(close)
 414:	b8 15 00 00 00       	mov    $0x15,%eax
 419:	cd 40                	int    $0x40
 41b:	c3                   	ret    

0000041c <kill>:
SYSCALL(kill)
 41c:	b8 06 00 00 00       	mov    $0x6,%eax
 421:	cd 40                	int    $0x40
 423:	c3                   	ret    

00000424 <exec>:
SYSCALL(exec)
 424:	b8 07 00 00 00       	mov    $0x7,%eax
 429:	cd 40                	int    $0x40
 42b:	c3                   	ret    

0000042c <open>:
SYSCALL(open)
 42c:	b8 0f 00 00 00       	mov    $0xf,%eax
 431:	cd 40                	int    $0x40
 433:	c3                   	ret    

00000434 <mknod>:
SYSCALL(mknod)
 434:	b8 11 00 00 00       	mov    $0x11,%eax
 439:	cd 40                	int    $0x40
 43b:	c3                   	ret    

0000043c <unlink>:
SYSCALL(unlink)
 43c:	b8 12 00 00 00       	mov    $0x12,%eax
 441:	cd 40                	int    $0x40
 443:	c3                   	ret    

00000444 <fstat>:
SYSCALL(fstat)
 444:	b8 08 00 00 00       	mov    $0x8,%eax
 449:	cd 40                	int    $0x40
 44b:	c3                   	ret    

0000044c <link>:
SYSCALL(link)
 44c:	b8 13 00 00 00       	mov    $0x13,%eax
 451:	cd 40                	int    $0x40
 453:	c3                   	ret    

00000454 <mkdir>:
SYSCALL(mkdir)
 454:	b8 14 00 00 00       	mov    $0x14,%eax
 459:	cd 40                	int    $0x40
 45b:	c3                   	ret    

0000045c <chdir>:
SYSCALL(chdir)
 45c:	b8 09 00 00 00       	mov    $0x9,%eax
 461:	cd 40                	int    $0x40
 463:	c3                   	ret    

00000464 <dup>:
SYSCALL(dup)
 464:	b8 0a 00 00 00       	mov    $0xa,%eax
 469:	cd 40                	int    $0x40
 46b:	c3                   	ret    

0000046c <getpid>:
SYSCALL(getpid)
 46c:	b8 0b 00 00 00       	mov    $0xb,%eax
 471:	cd 40                	int    $0x40
 473:	c3                   	ret    

00000474 <sbrk>:
SYSCALL(sbrk)
 474:	b8 0c 00 00 00       	mov    $0xc,%eax
 479:	cd 40                	int    $0x40
 47b:	c3                   	ret    

0000047c <sleep>:
SYSCALL(sleep)
 47c:	b8 0d 00 00 00       	mov    $0xd,%eax
 481:	cd 40                	int    $0x40
 483:	c3                   	ret    

00000484 <uptime>:
SYSCALL(uptime)
 484:	b8 0e 00 00 00       	mov    $0xe,%eax
 489:	cd 40                	int    $0x40
 48b:	c3                   	ret    

0000048c <halt>:
SYSCALL(halt)
 48c:	b8 16 00 00 00       	mov    $0x16,%eax
 491:	cd 40                	int    $0x40
 493:	c3                   	ret    

00000494 <date>:
SYSCALL(date)    #added the date system call
 494:	b8 17 00 00 00       	mov    $0x17,%eax
 499:	cd 40                	int    $0x40
 49b:	c3                   	ret    

0000049c <getuid>:
SYSCALL(getuid)
 49c:	b8 18 00 00 00       	mov    $0x18,%eax
 4a1:	cd 40                	int    $0x40
 4a3:	c3                   	ret    

000004a4 <getgid>:
SYSCALL(getgid)
 4a4:	b8 19 00 00 00       	mov    $0x19,%eax
 4a9:	cd 40                	int    $0x40
 4ab:	c3                   	ret    

000004ac <getppid>:
SYSCALL(getppid)
 4ac:	b8 1a 00 00 00       	mov    $0x1a,%eax
 4b1:	cd 40                	int    $0x40
 4b3:	c3                   	ret    

000004b4 <setuid>:
SYSCALL(setuid)
 4b4:	b8 1b 00 00 00       	mov    $0x1b,%eax
 4b9:	cd 40                	int    $0x40
 4bb:	c3                   	ret    

000004bc <setgid>:
SYSCALL(setgid)
 4bc:	b8 1c 00 00 00       	mov    $0x1c,%eax
 4c1:	cd 40                	int    $0x40
 4c3:	c3                   	ret    

000004c4 <getprocs>:
SYSCALL(getprocs)
 4c4:	b8 1d 00 00 00       	mov    $0x1d,%eax
 4c9:	cd 40                	int    $0x40
 4cb:	c3                   	ret    

000004cc <setpriority>:
SYSCALL(setpriority)
 4cc:	b8 1e 00 00 00       	mov    $0x1e,%eax
 4d1:	cd 40                	int    $0x40
 4d3:	c3                   	ret    
