
_ps-test:     file format elf32-i386


Disassembly of section .text:

00000000 <forktest>:
#include "user.h"
#define TPS 100

void
forktest(int N)
{
   0:	55                   	push   %ebp
   1:	89 e5                	mov    %esp,%ebp
   3:	83 ec 18             	sub    $0x18,%esp
  int n, pid;

  printf(1, "fork test\n");
   6:	83 ec 08             	sub    $0x8,%esp
   9:	68 10 09 00 00       	push   $0x910
   e:	6a 01                	push   $0x1
  10:	e8 42 05 00 00       	call   557 <printf>
  15:	83 c4 10             	add    $0x10,%esp

  for(n=0; n<N; n++){
  18:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  1f:	eb 2d                	jmp    4e <forktest+0x4e>
    pid = fork();
  21:	e8 6a 03 00 00       	call   390 <fork>
  26:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if(pid < 0)
  29:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
  2d:	78 29                	js     58 <forktest+0x58>
      break;
    if(pid == 0) {
  2f:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
  33:	75 15                	jne    4a <forktest+0x4a>
      sleep(10*TPS);
  35:	83 ec 0c             	sub    $0xc,%esp
  38:	68 e8 03 00 00       	push   $0x3e8
  3d:	e8 e6 03 00 00       	call   428 <sleep>
  42:	83 c4 10             	add    $0x10,%esp
      exit();
  45:	e8 4e 03 00 00       	call   398 <exit>
{
  int n, pid;

  printf(1, "fork test\n");

  for(n=0; n<N; n++){
  4a:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
  4e:	8b 45 f4             	mov    -0xc(%ebp),%eax
  51:	3b 45 08             	cmp    0x8(%ebp),%eax
  54:	7c cb                	jl     21 <forktest+0x21>
  56:	eb 27                	jmp    7f <forktest+0x7f>
    pid = fork();
    if(pid < 0)
      break;
  58:	90                   	nop
      sleep(10*TPS);
      exit();
    }
  }
  
  for(; n > 0; n--){
  59:	eb 24                	jmp    7f <forktest+0x7f>
    if(wait() < 0){
  5b:	e8 40 03 00 00       	call   3a0 <wait>
  60:	85 c0                	test   %eax,%eax
  62:	79 17                	jns    7b <forktest+0x7b>
      printf(1, "wait stopped early\n");
  64:	83 ec 08             	sub    $0x8,%esp
  67:	68 1b 09 00 00       	push   $0x91b
  6c:	6a 01                	push   $0x1
  6e:	e8 e4 04 00 00       	call   557 <printf>
  73:	83 c4 10             	add    $0x10,%esp
      exit();
  76:	e8 1d 03 00 00       	call   398 <exit>
      sleep(10*TPS);
      exit();
    }
  }
  
  for(; n > 0; n--){
  7b:	83 6d f4 01          	subl   $0x1,-0xc(%ebp)
  7f:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
  83:	7f d6                	jg     5b <forktest+0x5b>
      printf(1, "wait stopped early\n");
      exit();
    }
  }
  
  if(wait() != -1){
  85:	e8 16 03 00 00       	call   3a0 <wait>
  8a:	83 f8 ff             	cmp    $0xffffffff,%eax
  8d:	74 17                	je     a6 <forktest+0xa6>
    printf(1, "wait got too many\n");
  8f:	83 ec 08             	sub    $0x8,%esp
  92:	68 2f 09 00 00       	push   $0x92f
  97:	6a 01                	push   $0x1
  99:	e8 b9 04 00 00       	call   557 <printf>
  9e:	83 c4 10             	add    $0x10,%esp
    exit();
  a1:	e8 f2 02 00 00       	call   398 <exit>
  }
  
  printf(1, "fork test OK\n");
  a6:	83 ec 08             	sub    $0x8,%esp
  a9:	68 42 09 00 00       	push   $0x942
  ae:	6a 01                	push   $0x1
  b0:	e8 a2 04 00 00       	call   557 <printf>
  b5:	83 c4 10             	add    $0x10,%esp
}
  b8:	90                   	nop
  b9:	c9                   	leave  
  ba:	c3                   	ret    

000000bb <main>:

int
main(int argc, char **argv)
{
  bb:	8d 4c 24 04          	lea    0x4(%esp),%ecx
  bf:	83 e4 f0             	and    $0xfffffff0,%esp
  c2:	ff 71 fc             	pushl  -0x4(%ecx)
  c5:	55                   	push   %ebp
  c6:	89 e5                	mov    %esp,%ebp
  c8:	51                   	push   %ecx
  c9:	83 ec 14             	sub    $0x14,%esp
  cc:	89 c8                	mov    %ecx,%eax
  int N;

  if (argc == 1) {
  ce:	83 38 01             	cmpl   $0x1,(%eax)
  d1:	75 17                	jne    ea <main+0x2f>
    printf(2, "Enter number of processes to create\n");
  d3:	83 ec 08             	sub    $0x8,%esp
  d6:	68 50 09 00 00       	push   $0x950
  db:	6a 02                	push   $0x2
  dd:	e8 75 04 00 00       	call   557 <printf>
  e2:	83 c4 10             	add    $0x10,%esp
    exit();
  e5:	e8 ae 02 00 00       	call   398 <exit>
  }

  N = atoi(argv[1]);
  ea:	8b 40 04             	mov    0x4(%eax),%eax
  ed:	83 c0 04             	add    $0x4,%eax
  f0:	8b 00                	mov    (%eax),%eax
  f2:	83 ec 0c             	sub    $0xc,%esp
  f5:	50                   	push   %eax
  f6:	e8 de 01 00 00       	call   2d9 <atoi>
  fb:	83 c4 10             	add    $0x10,%esp
  fe:	89 45 f4             	mov    %eax,-0xc(%ebp)
  forktest(N);
 101:	83 ec 0c             	sub    $0xc,%esp
 104:	ff 75 f4             	pushl  -0xc(%ebp)
 107:	e8 f4 fe ff ff       	call   0 <forktest>
 10c:	83 c4 10             	add    $0x10,%esp
  exit();
 10f:	e8 84 02 00 00       	call   398 <exit>

00000114 <stosb>:
               "cc");
}

static inline void
stosb(void *addr, int data, int cnt)
{
 114:	55                   	push   %ebp
 115:	89 e5                	mov    %esp,%ebp
 117:	57                   	push   %edi
 118:	53                   	push   %ebx
  asm volatile("cld; rep stosb" :
 119:	8b 4d 08             	mov    0x8(%ebp),%ecx
 11c:	8b 55 10             	mov    0x10(%ebp),%edx
 11f:	8b 45 0c             	mov    0xc(%ebp),%eax
 122:	89 cb                	mov    %ecx,%ebx
 124:	89 df                	mov    %ebx,%edi
 126:	89 d1                	mov    %edx,%ecx
 128:	fc                   	cld    
 129:	f3 aa                	rep stos %al,%es:(%edi)
 12b:	89 ca                	mov    %ecx,%edx
 12d:	89 fb                	mov    %edi,%ebx
 12f:	89 5d 08             	mov    %ebx,0x8(%ebp)
 132:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
 135:	90                   	nop
 136:	5b                   	pop    %ebx
 137:	5f                   	pop    %edi
 138:	5d                   	pop    %ebp
 139:	c3                   	ret    

0000013a <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, char *t)
{
 13a:	55                   	push   %ebp
 13b:	89 e5                	mov    %esp,%ebp
 13d:	83 ec 10             	sub    $0x10,%esp
  char *os;

  os = s;
 140:	8b 45 08             	mov    0x8(%ebp),%eax
 143:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while((*s++ = *t++) != 0)
 146:	90                   	nop
 147:	8b 45 08             	mov    0x8(%ebp),%eax
 14a:	8d 50 01             	lea    0x1(%eax),%edx
 14d:	89 55 08             	mov    %edx,0x8(%ebp)
 150:	8b 55 0c             	mov    0xc(%ebp),%edx
 153:	8d 4a 01             	lea    0x1(%edx),%ecx
 156:	89 4d 0c             	mov    %ecx,0xc(%ebp)
 159:	0f b6 12             	movzbl (%edx),%edx
 15c:	88 10                	mov    %dl,(%eax)
 15e:	0f b6 00             	movzbl (%eax),%eax
 161:	84 c0                	test   %al,%al
 163:	75 e2                	jne    147 <strcpy+0xd>
    ;
  return os;
 165:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 168:	c9                   	leave  
 169:	c3                   	ret    

0000016a <strcmp>:

int
strcmp(const char *p, const char *q)
{
 16a:	55                   	push   %ebp
 16b:	89 e5                	mov    %esp,%ebp
  while(*p && *p == *q)
 16d:	eb 08                	jmp    177 <strcmp+0xd>
    p++, q++;
 16f:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 173:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
  while(*p && *p == *q)
 177:	8b 45 08             	mov    0x8(%ebp),%eax
 17a:	0f b6 00             	movzbl (%eax),%eax
 17d:	84 c0                	test   %al,%al
 17f:	74 10                	je     191 <strcmp+0x27>
 181:	8b 45 08             	mov    0x8(%ebp),%eax
 184:	0f b6 10             	movzbl (%eax),%edx
 187:	8b 45 0c             	mov    0xc(%ebp),%eax
 18a:	0f b6 00             	movzbl (%eax),%eax
 18d:	38 c2                	cmp    %al,%dl
 18f:	74 de                	je     16f <strcmp+0x5>
    p++, q++;
  return (uchar)*p - (uchar)*q;
 191:	8b 45 08             	mov    0x8(%ebp),%eax
 194:	0f b6 00             	movzbl (%eax),%eax
 197:	0f b6 d0             	movzbl %al,%edx
 19a:	8b 45 0c             	mov    0xc(%ebp),%eax
 19d:	0f b6 00             	movzbl (%eax),%eax
 1a0:	0f b6 c0             	movzbl %al,%eax
 1a3:	29 c2                	sub    %eax,%edx
 1a5:	89 d0                	mov    %edx,%eax
}
 1a7:	5d                   	pop    %ebp
 1a8:	c3                   	ret    

000001a9 <strlen>:

uint
strlen(char *s)
{
 1a9:	55                   	push   %ebp
 1aa:	89 e5                	mov    %esp,%ebp
 1ac:	83 ec 10             	sub    $0x10,%esp
  int n;

  for(n = 0; s[n]; n++)
 1af:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
 1b6:	eb 04                	jmp    1bc <strlen+0x13>
 1b8:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
 1bc:	8b 55 fc             	mov    -0x4(%ebp),%edx
 1bf:	8b 45 08             	mov    0x8(%ebp),%eax
 1c2:	01 d0                	add    %edx,%eax
 1c4:	0f b6 00             	movzbl (%eax),%eax
 1c7:	84 c0                	test   %al,%al
 1c9:	75 ed                	jne    1b8 <strlen+0xf>
    ;
  return n;
 1cb:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 1ce:	c9                   	leave  
 1cf:	c3                   	ret    

000001d0 <memset>:

void*
memset(void *dst, int c, uint n)
{
 1d0:	55                   	push   %ebp
 1d1:	89 e5                	mov    %esp,%ebp
  stosb(dst, c, n);
 1d3:	8b 45 10             	mov    0x10(%ebp),%eax
 1d6:	50                   	push   %eax
 1d7:	ff 75 0c             	pushl  0xc(%ebp)
 1da:	ff 75 08             	pushl  0x8(%ebp)
 1dd:	e8 32 ff ff ff       	call   114 <stosb>
 1e2:	83 c4 0c             	add    $0xc,%esp
  return dst;
 1e5:	8b 45 08             	mov    0x8(%ebp),%eax
}
 1e8:	c9                   	leave  
 1e9:	c3                   	ret    

000001ea <strchr>:

char*
strchr(const char *s, char c)
{
 1ea:	55                   	push   %ebp
 1eb:	89 e5                	mov    %esp,%ebp
 1ed:	83 ec 04             	sub    $0x4,%esp
 1f0:	8b 45 0c             	mov    0xc(%ebp),%eax
 1f3:	88 45 fc             	mov    %al,-0x4(%ebp)
  for(; *s; s++)
 1f6:	eb 14                	jmp    20c <strchr+0x22>
    if(*s == c)
 1f8:	8b 45 08             	mov    0x8(%ebp),%eax
 1fb:	0f b6 00             	movzbl (%eax),%eax
 1fe:	3a 45 fc             	cmp    -0x4(%ebp),%al
 201:	75 05                	jne    208 <strchr+0x1e>
      return (char*)s;
 203:	8b 45 08             	mov    0x8(%ebp),%eax
 206:	eb 13                	jmp    21b <strchr+0x31>
}

char*
strchr(const char *s, char c)
{
  for(; *s; s++)
 208:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 20c:	8b 45 08             	mov    0x8(%ebp),%eax
 20f:	0f b6 00             	movzbl (%eax),%eax
 212:	84 c0                	test   %al,%al
 214:	75 e2                	jne    1f8 <strchr+0xe>
    if(*s == c)
      return (char*)s;
  return 0;
 216:	b8 00 00 00 00       	mov    $0x0,%eax
}
 21b:	c9                   	leave  
 21c:	c3                   	ret    

0000021d <gets>:

char*
gets(char *buf, int max)
{
 21d:	55                   	push   %ebp
 21e:	89 e5                	mov    %esp,%ebp
 220:	83 ec 18             	sub    $0x18,%esp
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 223:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 22a:	eb 42                	jmp    26e <gets+0x51>
    cc = read(0, &c, 1);
 22c:	83 ec 04             	sub    $0x4,%esp
 22f:	6a 01                	push   $0x1
 231:	8d 45 ef             	lea    -0x11(%ebp),%eax
 234:	50                   	push   %eax
 235:	6a 00                	push   $0x0
 237:	e8 74 01 00 00       	call   3b0 <read>
 23c:	83 c4 10             	add    $0x10,%esp
 23f:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if(cc < 1)
 242:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 246:	7e 33                	jle    27b <gets+0x5e>
      break;
    buf[i++] = c;
 248:	8b 45 f4             	mov    -0xc(%ebp),%eax
 24b:	8d 50 01             	lea    0x1(%eax),%edx
 24e:	89 55 f4             	mov    %edx,-0xc(%ebp)
 251:	89 c2                	mov    %eax,%edx
 253:	8b 45 08             	mov    0x8(%ebp),%eax
 256:	01 c2                	add    %eax,%edx
 258:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 25c:	88 02                	mov    %al,(%edx)
    if(c == '\n' || c == '\r')
 25e:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 262:	3c 0a                	cmp    $0xa,%al
 264:	74 16                	je     27c <gets+0x5f>
 266:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 26a:	3c 0d                	cmp    $0xd,%al
 26c:	74 0e                	je     27c <gets+0x5f>
gets(char *buf, int max)
{
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 26e:	8b 45 f4             	mov    -0xc(%ebp),%eax
 271:	83 c0 01             	add    $0x1,%eax
 274:	3b 45 0c             	cmp    0xc(%ebp),%eax
 277:	7c b3                	jl     22c <gets+0xf>
 279:	eb 01                	jmp    27c <gets+0x5f>
    cc = read(0, &c, 1);
    if(cc < 1)
      break;
 27b:	90                   	nop
    buf[i++] = c;
    if(c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 27c:	8b 55 f4             	mov    -0xc(%ebp),%edx
 27f:	8b 45 08             	mov    0x8(%ebp),%eax
 282:	01 d0                	add    %edx,%eax
 284:	c6 00 00             	movb   $0x0,(%eax)
  return buf;
 287:	8b 45 08             	mov    0x8(%ebp),%eax
}
 28a:	c9                   	leave  
 28b:	c3                   	ret    

0000028c <stat>:

int
stat(char *n, struct stat *st)
{
 28c:	55                   	push   %ebp
 28d:	89 e5                	mov    %esp,%ebp
 28f:	83 ec 18             	sub    $0x18,%esp
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 292:	83 ec 08             	sub    $0x8,%esp
 295:	6a 00                	push   $0x0
 297:	ff 75 08             	pushl  0x8(%ebp)
 29a:	e8 39 01 00 00       	call   3d8 <open>
 29f:	83 c4 10             	add    $0x10,%esp
 2a2:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(fd < 0)
 2a5:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 2a9:	79 07                	jns    2b2 <stat+0x26>
    return -1;
 2ab:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 2b0:	eb 25                	jmp    2d7 <stat+0x4b>
  r = fstat(fd, st);
 2b2:	83 ec 08             	sub    $0x8,%esp
 2b5:	ff 75 0c             	pushl  0xc(%ebp)
 2b8:	ff 75 f4             	pushl  -0xc(%ebp)
 2bb:	e8 30 01 00 00       	call   3f0 <fstat>
 2c0:	83 c4 10             	add    $0x10,%esp
 2c3:	89 45 f0             	mov    %eax,-0x10(%ebp)
  close(fd);
 2c6:	83 ec 0c             	sub    $0xc,%esp
 2c9:	ff 75 f4             	pushl  -0xc(%ebp)
 2cc:	e8 ef 00 00 00       	call   3c0 <close>
 2d1:	83 c4 10             	add    $0x10,%esp
  return r;
 2d4:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
 2d7:	c9                   	leave  
 2d8:	c3                   	ret    

000002d9 <atoi>:

int
atoi(const char *s)
{
 2d9:	55                   	push   %ebp
 2da:	89 e5                	mov    %esp,%ebp
 2dc:	83 ec 10             	sub    $0x10,%esp
  int n, sign;

  n = 0;
 2df:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  while(*s == ' ') s++;
 2e6:	eb 04                	jmp    2ec <atoi+0x13>
 2e8:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 2ec:	8b 45 08             	mov    0x8(%ebp),%eax
 2ef:	0f b6 00             	movzbl (%eax),%eax
 2f2:	3c 20                	cmp    $0x20,%al
 2f4:	74 f2                	je     2e8 <atoi+0xf>
  sign = (*s == '-') ? -1 : 1;
 2f6:	8b 45 08             	mov    0x8(%ebp),%eax
 2f9:	0f b6 00             	movzbl (%eax),%eax
 2fc:	3c 2d                	cmp    $0x2d,%al
 2fe:	75 07                	jne    307 <atoi+0x2e>
 300:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 305:	eb 05                	jmp    30c <atoi+0x33>
 307:	b8 01 00 00 00       	mov    $0x1,%eax
 30c:	89 45 f8             	mov    %eax,-0x8(%ebp)
  while('0' <= *s && *s <= '9')
 30f:	eb 25                	jmp    336 <atoi+0x5d>
    n = n*10 + *s++ - '0';
 311:	8b 55 fc             	mov    -0x4(%ebp),%edx
 314:	89 d0                	mov    %edx,%eax
 316:	c1 e0 02             	shl    $0x2,%eax
 319:	01 d0                	add    %edx,%eax
 31b:	01 c0                	add    %eax,%eax
 31d:	89 c1                	mov    %eax,%ecx
 31f:	8b 45 08             	mov    0x8(%ebp),%eax
 322:	8d 50 01             	lea    0x1(%eax),%edx
 325:	89 55 08             	mov    %edx,0x8(%ebp)
 328:	0f b6 00             	movzbl (%eax),%eax
 32b:	0f be c0             	movsbl %al,%eax
 32e:	01 c8                	add    %ecx,%eax
 330:	83 e8 30             	sub    $0x30,%eax
 333:	89 45 fc             	mov    %eax,-0x4(%ebp)
  int n, sign;

  n = 0;
  while(*s == ' ') s++;
  sign = (*s == '-') ? -1 : 1;
  while('0' <= *s && *s <= '9')
 336:	8b 45 08             	mov    0x8(%ebp),%eax
 339:	0f b6 00             	movzbl (%eax),%eax
 33c:	3c 2f                	cmp    $0x2f,%al
 33e:	7e 0a                	jle    34a <atoi+0x71>
 340:	8b 45 08             	mov    0x8(%ebp),%eax
 343:	0f b6 00             	movzbl (%eax),%eax
 346:	3c 39                	cmp    $0x39,%al
 348:	7e c7                	jle    311 <atoi+0x38>
    n = n*10 + *s++ - '0';
  return sign*n;
 34a:	8b 45 f8             	mov    -0x8(%ebp),%eax
 34d:	0f af 45 fc          	imul   -0x4(%ebp),%eax
}
 351:	c9                   	leave  
 352:	c3                   	ret    

00000353 <memmove>:

void*
memmove(void *vdst, void *vsrc, int n)
{
 353:	55                   	push   %ebp
 354:	89 e5                	mov    %esp,%ebp
 356:	83 ec 10             	sub    $0x10,%esp
  char *dst, *src;
  
  dst = vdst;
 359:	8b 45 08             	mov    0x8(%ebp),%eax
 35c:	89 45 fc             	mov    %eax,-0x4(%ebp)
  src = vsrc;
 35f:	8b 45 0c             	mov    0xc(%ebp),%eax
 362:	89 45 f8             	mov    %eax,-0x8(%ebp)
  while(n-- > 0)
 365:	eb 17                	jmp    37e <memmove+0x2b>
    *dst++ = *src++;
 367:	8b 45 fc             	mov    -0x4(%ebp),%eax
 36a:	8d 50 01             	lea    0x1(%eax),%edx
 36d:	89 55 fc             	mov    %edx,-0x4(%ebp)
 370:	8b 55 f8             	mov    -0x8(%ebp),%edx
 373:	8d 4a 01             	lea    0x1(%edx),%ecx
 376:	89 4d f8             	mov    %ecx,-0x8(%ebp)
 379:	0f b6 12             	movzbl (%edx),%edx
 37c:	88 10                	mov    %dl,(%eax)
{
  char *dst, *src;
  
  dst = vdst;
  src = vsrc;
  while(n-- > 0)
 37e:	8b 45 10             	mov    0x10(%ebp),%eax
 381:	8d 50 ff             	lea    -0x1(%eax),%edx
 384:	89 55 10             	mov    %edx,0x10(%ebp)
 387:	85 c0                	test   %eax,%eax
 389:	7f dc                	jg     367 <memmove+0x14>
    *dst++ = *src++;
  return vdst;
 38b:	8b 45 08             	mov    0x8(%ebp),%eax
}
 38e:	c9                   	leave  
 38f:	c3                   	ret    

00000390 <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
 390:	b8 01 00 00 00       	mov    $0x1,%eax
 395:	cd 40                	int    $0x40
 397:	c3                   	ret    

00000398 <exit>:
SYSCALL(exit)
 398:	b8 02 00 00 00       	mov    $0x2,%eax
 39d:	cd 40                	int    $0x40
 39f:	c3                   	ret    

000003a0 <wait>:
SYSCALL(wait)
 3a0:	b8 03 00 00 00       	mov    $0x3,%eax
 3a5:	cd 40                	int    $0x40
 3a7:	c3                   	ret    

000003a8 <pipe>:
SYSCALL(pipe)
 3a8:	b8 04 00 00 00       	mov    $0x4,%eax
 3ad:	cd 40                	int    $0x40
 3af:	c3                   	ret    

000003b0 <read>:
SYSCALL(read)
 3b0:	b8 05 00 00 00       	mov    $0x5,%eax
 3b5:	cd 40                	int    $0x40
 3b7:	c3                   	ret    

000003b8 <write>:
SYSCALL(write)
 3b8:	b8 10 00 00 00       	mov    $0x10,%eax
 3bd:	cd 40                	int    $0x40
 3bf:	c3                   	ret    

000003c0 <close>:
SYSCALL(close)
 3c0:	b8 15 00 00 00       	mov    $0x15,%eax
 3c5:	cd 40                	int    $0x40
 3c7:	c3                   	ret    

000003c8 <kill>:
SYSCALL(kill)
 3c8:	b8 06 00 00 00       	mov    $0x6,%eax
 3cd:	cd 40                	int    $0x40
 3cf:	c3                   	ret    

000003d0 <exec>:
SYSCALL(exec)
 3d0:	b8 07 00 00 00       	mov    $0x7,%eax
 3d5:	cd 40                	int    $0x40
 3d7:	c3                   	ret    

000003d8 <open>:
SYSCALL(open)
 3d8:	b8 0f 00 00 00       	mov    $0xf,%eax
 3dd:	cd 40                	int    $0x40
 3df:	c3                   	ret    

000003e0 <mknod>:
SYSCALL(mknod)
 3e0:	b8 11 00 00 00       	mov    $0x11,%eax
 3e5:	cd 40                	int    $0x40
 3e7:	c3                   	ret    

000003e8 <unlink>:
SYSCALL(unlink)
 3e8:	b8 12 00 00 00       	mov    $0x12,%eax
 3ed:	cd 40                	int    $0x40
 3ef:	c3                   	ret    

000003f0 <fstat>:
SYSCALL(fstat)
 3f0:	b8 08 00 00 00       	mov    $0x8,%eax
 3f5:	cd 40                	int    $0x40
 3f7:	c3                   	ret    

000003f8 <link>:
SYSCALL(link)
 3f8:	b8 13 00 00 00       	mov    $0x13,%eax
 3fd:	cd 40                	int    $0x40
 3ff:	c3                   	ret    

00000400 <mkdir>:
SYSCALL(mkdir)
 400:	b8 14 00 00 00       	mov    $0x14,%eax
 405:	cd 40                	int    $0x40
 407:	c3                   	ret    

00000408 <chdir>:
SYSCALL(chdir)
 408:	b8 09 00 00 00       	mov    $0x9,%eax
 40d:	cd 40                	int    $0x40
 40f:	c3                   	ret    

00000410 <dup>:
SYSCALL(dup)
 410:	b8 0a 00 00 00       	mov    $0xa,%eax
 415:	cd 40                	int    $0x40
 417:	c3                   	ret    

00000418 <getpid>:
SYSCALL(getpid)
 418:	b8 0b 00 00 00       	mov    $0xb,%eax
 41d:	cd 40                	int    $0x40
 41f:	c3                   	ret    

00000420 <sbrk>:
SYSCALL(sbrk)
 420:	b8 0c 00 00 00       	mov    $0xc,%eax
 425:	cd 40                	int    $0x40
 427:	c3                   	ret    

00000428 <sleep>:
SYSCALL(sleep)
 428:	b8 0d 00 00 00       	mov    $0xd,%eax
 42d:	cd 40                	int    $0x40
 42f:	c3                   	ret    

00000430 <uptime>:
SYSCALL(uptime)
 430:	b8 0e 00 00 00       	mov    $0xe,%eax
 435:	cd 40                	int    $0x40
 437:	c3                   	ret    

00000438 <halt>:
SYSCALL(halt)
 438:	b8 16 00 00 00       	mov    $0x16,%eax
 43d:	cd 40                	int    $0x40
 43f:	c3                   	ret    

00000440 <date>:
SYSCALL(date)    #added the date system call
 440:	b8 17 00 00 00       	mov    $0x17,%eax
 445:	cd 40                	int    $0x40
 447:	c3                   	ret    

00000448 <getuid>:
SYSCALL(getuid)
 448:	b8 18 00 00 00       	mov    $0x18,%eax
 44d:	cd 40                	int    $0x40
 44f:	c3                   	ret    

00000450 <getgid>:
SYSCALL(getgid)
 450:	b8 19 00 00 00       	mov    $0x19,%eax
 455:	cd 40                	int    $0x40
 457:	c3                   	ret    

00000458 <getppid>:
SYSCALL(getppid)
 458:	b8 1a 00 00 00       	mov    $0x1a,%eax
 45d:	cd 40                	int    $0x40
 45f:	c3                   	ret    

00000460 <setuid>:
SYSCALL(setuid)
 460:	b8 1b 00 00 00       	mov    $0x1b,%eax
 465:	cd 40                	int    $0x40
 467:	c3                   	ret    

00000468 <setgid>:
SYSCALL(setgid)
 468:	b8 1c 00 00 00       	mov    $0x1c,%eax
 46d:	cd 40                	int    $0x40
 46f:	c3                   	ret    

00000470 <getprocs>:
SYSCALL(getprocs)
 470:	b8 1d 00 00 00       	mov    $0x1d,%eax
 475:	cd 40                	int    $0x40
 477:	c3                   	ret    

00000478 <setpriority>:
SYSCALL(setpriority)
 478:	b8 1e 00 00 00       	mov    $0x1e,%eax
 47d:	cd 40                	int    $0x40
 47f:	c3                   	ret    

00000480 <putc>:
#include "stat.h"
#include "user.h"

static void
putc(int fd, char c)
{
 480:	55                   	push   %ebp
 481:	89 e5                	mov    %esp,%ebp
 483:	83 ec 18             	sub    $0x18,%esp
 486:	8b 45 0c             	mov    0xc(%ebp),%eax
 489:	88 45 f4             	mov    %al,-0xc(%ebp)
  write(fd, &c, 1);
 48c:	83 ec 04             	sub    $0x4,%esp
 48f:	6a 01                	push   $0x1
 491:	8d 45 f4             	lea    -0xc(%ebp),%eax
 494:	50                   	push   %eax
 495:	ff 75 08             	pushl  0x8(%ebp)
 498:	e8 1b ff ff ff       	call   3b8 <write>
 49d:	83 c4 10             	add    $0x10,%esp
}
 4a0:	90                   	nop
 4a1:	c9                   	leave  
 4a2:	c3                   	ret    

000004a3 <printint>:

static void
printint(int fd, int xx, int base, int sgn)
{
 4a3:	55                   	push   %ebp
 4a4:	89 e5                	mov    %esp,%ebp
 4a6:	53                   	push   %ebx
 4a7:	83 ec 24             	sub    $0x24,%esp
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 4aa:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  if(sgn && xx < 0){
 4b1:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
 4b5:	74 17                	je     4ce <printint+0x2b>
 4b7:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
 4bb:	79 11                	jns    4ce <printint+0x2b>
    neg = 1;
 4bd:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
    x = -xx;
 4c4:	8b 45 0c             	mov    0xc(%ebp),%eax
 4c7:	f7 d8                	neg    %eax
 4c9:	89 45 ec             	mov    %eax,-0x14(%ebp)
 4cc:	eb 06                	jmp    4d4 <printint+0x31>
  } else {
    x = xx;
 4ce:	8b 45 0c             	mov    0xc(%ebp),%eax
 4d1:	89 45 ec             	mov    %eax,-0x14(%ebp)
  }

  i = 0;
 4d4:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  do{
    buf[i++] = digits[x % base];
 4db:	8b 4d f4             	mov    -0xc(%ebp),%ecx
 4de:	8d 41 01             	lea    0x1(%ecx),%eax
 4e1:	89 45 f4             	mov    %eax,-0xc(%ebp)
 4e4:	8b 5d 10             	mov    0x10(%ebp),%ebx
 4e7:	8b 45 ec             	mov    -0x14(%ebp),%eax
 4ea:	ba 00 00 00 00       	mov    $0x0,%edx
 4ef:	f7 f3                	div    %ebx
 4f1:	89 d0                	mov    %edx,%eax
 4f3:	0f b6 80 e4 0b 00 00 	movzbl 0xbe4(%eax),%eax
 4fa:	88 44 0d dc          	mov    %al,-0x24(%ebp,%ecx,1)
  }while((x /= base) != 0);
 4fe:	8b 5d 10             	mov    0x10(%ebp),%ebx
 501:	8b 45 ec             	mov    -0x14(%ebp),%eax
 504:	ba 00 00 00 00       	mov    $0x0,%edx
 509:	f7 f3                	div    %ebx
 50b:	89 45 ec             	mov    %eax,-0x14(%ebp)
 50e:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 512:	75 c7                	jne    4db <printint+0x38>
  if(neg)
 514:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 518:	74 2d                	je     547 <printint+0xa4>
    buf[i++] = '-';
 51a:	8b 45 f4             	mov    -0xc(%ebp),%eax
 51d:	8d 50 01             	lea    0x1(%eax),%edx
 520:	89 55 f4             	mov    %edx,-0xc(%ebp)
 523:	c6 44 05 dc 2d       	movb   $0x2d,-0x24(%ebp,%eax,1)

  while(--i >= 0)
 528:	eb 1d                	jmp    547 <printint+0xa4>
    putc(fd, buf[i]);
 52a:	8d 55 dc             	lea    -0x24(%ebp),%edx
 52d:	8b 45 f4             	mov    -0xc(%ebp),%eax
 530:	01 d0                	add    %edx,%eax
 532:	0f b6 00             	movzbl (%eax),%eax
 535:	0f be c0             	movsbl %al,%eax
 538:	83 ec 08             	sub    $0x8,%esp
 53b:	50                   	push   %eax
 53c:	ff 75 08             	pushl  0x8(%ebp)
 53f:	e8 3c ff ff ff       	call   480 <putc>
 544:	83 c4 10             	add    $0x10,%esp
    buf[i++] = digits[x % base];
  }while((x /= base) != 0);
  if(neg)
    buf[i++] = '-';

  while(--i >= 0)
 547:	83 6d f4 01          	subl   $0x1,-0xc(%ebp)
 54b:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 54f:	79 d9                	jns    52a <printint+0x87>
    putc(fd, buf[i]);
}
 551:	90                   	nop
 552:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 555:	c9                   	leave  
 556:	c3                   	ret    

00000557 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, char *fmt, ...)
{
 557:	55                   	push   %ebp
 558:	89 e5                	mov    %esp,%ebp
 55a:	83 ec 28             	sub    $0x28,%esp
  char *s;
  int c, i, state;
  uint *ap;

  state = 0;
 55d:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  ap = (uint*)(void*)&fmt + 1;
 564:	8d 45 0c             	lea    0xc(%ebp),%eax
 567:	83 c0 04             	add    $0x4,%eax
 56a:	89 45 e8             	mov    %eax,-0x18(%ebp)
  for(i = 0; fmt[i]; i++){
 56d:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 574:	e9 59 01 00 00       	jmp    6d2 <printf+0x17b>
    c = fmt[i] & 0xff;
 579:	8b 55 0c             	mov    0xc(%ebp),%edx
 57c:	8b 45 f0             	mov    -0x10(%ebp),%eax
 57f:	01 d0                	add    %edx,%eax
 581:	0f b6 00             	movzbl (%eax),%eax
 584:	0f be c0             	movsbl %al,%eax
 587:	25 ff 00 00 00       	and    $0xff,%eax
 58c:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    if(state == 0){
 58f:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 593:	75 2c                	jne    5c1 <printf+0x6a>
      if(c == '%'){
 595:	83 7d e4 25          	cmpl   $0x25,-0x1c(%ebp)
 599:	75 0c                	jne    5a7 <printf+0x50>
        state = '%';
 59b:	c7 45 ec 25 00 00 00 	movl   $0x25,-0x14(%ebp)
 5a2:	e9 27 01 00 00       	jmp    6ce <printf+0x177>
      } else {
        putc(fd, c);
 5a7:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 5aa:	0f be c0             	movsbl %al,%eax
 5ad:	83 ec 08             	sub    $0x8,%esp
 5b0:	50                   	push   %eax
 5b1:	ff 75 08             	pushl  0x8(%ebp)
 5b4:	e8 c7 fe ff ff       	call   480 <putc>
 5b9:	83 c4 10             	add    $0x10,%esp
 5bc:	e9 0d 01 00 00       	jmp    6ce <printf+0x177>
      }
    } else if(state == '%'){
 5c1:	83 7d ec 25          	cmpl   $0x25,-0x14(%ebp)
 5c5:	0f 85 03 01 00 00    	jne    6ce <printf+0x177>
      if(c == 'd'){
 5cb:	83 7d e4 64          	cmpl   $0x64,-0x1c(%ebp)
 5cf:	75 1e                	jne    5ef <printf+0x98>
        printint(fd, *ap, 10, 1);
 5d1:	8b 45 e8             	mov    -0x18(%ebp),%eax
 5d4:	8b 00                	mov    (%eax),%eax
 5d6:	6a 01                	push   $0x1
 5d8:	6a 0a                	push   $0xa
 5da:	50                   	push   %eax
 5db:	ff 75 08             	pushl  0x8(%ebp)
 5de:	e8 c0 fe ff ff       	call   4a3 <printint>
 5e3:	83 c4 10             	add    $0x10,%esp
        ap++;
 5e6:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 5ea:	e9 d8 00 00 00       	jmp    6c7 <printf+0x170>
      } else if(c == 'x' || c == 'p'){
 5ef:	83 7d e4 78          	cmpl   $0x78,-0x1c(%ebp)
 5f3:	74 06                	je     5fb <printf+0xa4>
 5f5:	83 7d e4 70          	cmpl   $0x70,-0x1c(%ebp)
 5f9:	75 1e                	jne    619 <printf+0xc2>
        printint(fd, *ap, 16, 0);
 5fb:	8b 45 e8             	mov    -0x18(%ebp),%eax
 5fe:	8b 00                	mov    (%eax),%eax
 600:	6a 00                	push   $0x0
 602:	6a 10                	push   $0x10
 604:	50                   	push   %eax
 605:	ff 75 08             	pushl  0x8(%ebp)
 608:	e8 96 fe ff ff       	call   4a3 <printint>
 60d:	83 c4 10             	add    $0x10,%esp
        ap++;
 610:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 614:	e9 ae 00 00 00       	jmp    6c7 <printf+0x170>
      } else if(c == 's'){
 619:	83 7d e4 73          	cmpl   $0x73,-0x1c(%ebp)
 61d:	75 43                	jne    662 <printf+0x10b>
        s = (char*)*ap;
 61f:	8b 45 e8             	mov    -0x18(%ebp),%eax
 622:	8b 00                	mov    (%eax),%eax
 624:	89 45 f4             	mov    %eax,-0xc(%ebp)
        ap++;
 627:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
        if(s == 0)
 62b:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 62f:	75 25                	jne    656 <printf+0xff>
          s = "(null)";
 631:	c7 45 f4 75 09 00 00 	movl   $0x975,-0xc(%ebp)
        while(*s != 0){
 638:	eb 1c                	jmp    656 <printf+0xff>
          putc(fd, *s);
 63a:	8b 45 f4             	mov    -0xc(%ebp),%eax
 63d:	0f b6 00             	movzbl (%eax),%eax
 640:	0f be c0             	movsbl %al,%eax
 643:	83 ec 08             	sub    $0x8,%esp
 646:	50                   	push   %eax
 647:	ff 75 08             	pushl  0x8(%ebp)
 64a:	e8 31 fe ff ff       	call   480 <putc>
 64f:	83 c4 10             	add    $0x10,%esp
          s++;
 652:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
      } else if(c == 's'){
        s = (char*)*ap;
        ap++;
        if(s == 0)
          s = "(null)";
        while(*s != 0){
 656:	8b 45 f4             	mov    -0xc(%ebp),%eax
 659:	0f b6 00             	movzbl (%eax),%eax
 65c:	84 c0                	test   %al,%al
 65e:	75 da                	jne    63a <printf+0xe3>
 660:	eb 65                	jmp    6c7 <printf+0x170>
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 662:	83 7d e4 63          	cmpl   $0x63,-0x1c(%ebp)
 666:	75 1d                	jne    685 <printf+0x12e>
        putc(fd, *ap);
 668:	8b 45 e8             	mov    -0x18(%ebp),%eax
 66b:	8b 00                	mov    (%eax),%eax
 66d:	0f be c0             	movsbl %al,%eax
 670:	83 ec 08             	sub    $0x8,%esp
 673:	50                   	push   %eax
 674:	ff 75 08             	pushl  0x8(%ebp)
 677:	e8 04 fe ff ff       	call   480 <putc>
 67c:	83 c4 10             	add    $0x10,%esp
        ap++;
 67f:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 683:	eb 42                	jmp    6c7 <printf+0x170>
      } else if(c == '%'){
 685:	83 7d e4 25          	cmpl   $0x25,-0x1c(%ebp)
 689:	75 17                	jne    6a2 <printf+0x14b>
        putc(fd, c);
 68b:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 68e:	0f be c0             	movsbl %al,%eax
 691:	83 ec 08             	sub    $0x8,%esp
 694:	50                   	push   %eax
 695:	ff 75 08             	pushl  0x8(%ebp)
 698:	e8 e3 fd ff ff       	call   480 <putc>
 69d:	83 c4 10             	add    $0x10,%esp
 6a0:	eb 25                	jmp    6c7 <printf+0x170>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 6a2:	83 ec 08             	sub    $0x8,%esp
 6a5:	6a 25                	push   $0x25
 6a7:	ff 75 08             	pushl  0x8(%ebp)
 6aa:	e8 d1 fd ff ff       	call   480 <putc>
 6af:	83 c4 10             	add    $0x10,%esp
        putc(fd, c);
 6b2:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 6b5:	0f be c0             	movsbl %al,%eax
 6b8:	83 ec 08             	sub    $0x8,%esp
 6bb:	50                   	push   %eax
 6bc:	ff 75 08             	pushl  0x8(%ebp)
 6bf:	e8 bc fd ff ff       	call   480 <putc>
 6c4:	83 c4 10             	add    $0x10,%esp
      }
      state = 0;
 6c7:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
 6ce:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
 6d2:	8b 55 0c             	mov    0xc(%ebp),%edx
 6d5:	8b 45 f0             	mov    -0x10(%ebp),%eax
 6d8:	01 d0                	add    %edx,%eax
 6da:	0f b6 00             	movzbl (%eax),%eax
 6dd:	84 c0                	test   %al,%al
 6df:	0f 85 94 fe ff ff    	jne    579 <printf+0x22>
        putc(fd, c);
      }
      state = 0;
    }
  }
}
 6e5:	90                   	nop
 6e6:	c9                   	leave  
 6e7:	c3                   	ret    

000006e8 <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 6e8:	55                   	push   %ebp
 6e9:	89 e5                	mov    %esp,%ebp
 6eb:	83 ec 10             	sub    $0x10,%esp
  Header *bp, *p;

  bp = (Header*)ap - 1;
 6ee:	8b 45 08             	mov    0x8(%ebp),%eax
 6f1:	83 e8 08             	sub    $0x8,%eax
 6f4:	89 45 f8             	mov    %eax,-0x8(%ebp)
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 6f7:	a1 00 0c 00 00       	mov    0xc00,%eax
 6fc:	89 45 fc             	mov    %eax,-0x4(%ebp)
 6ff:	eb 24                	jmp    725 <free+0x3d>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 701:	8b 45 fc             	mov    -0x4(%ebp),%eax
 704:	8b 00                	mov    (%eax),%eax
 706:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 709:	77 12                	ja     71d <free+0x35>
 70b:	8b 45 f8             	mov    -0x8(%ebp),%eax
 70e:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 711:	77 24                	ja     737 <free+0x4f>
 713:	8b 45 fc             	mov    -0x4(%ebp),%eax
 716:	8b 00                	mov    (%eax),%eax
 718:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 71b:	77 1a                	ja     737 <free+0x4f>
free(void *ap)
{
  Header *bp, *p;

  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 71d:	8b 45 fc             	mov    -0x4(%ebp),%eax
 720:	8b 00                	mov    (%eax),%eax
 722:	89 45 fc             	mov    %eax,-0x4(%ebp)
 725:	8b 45 f8             	mov    -0x8(%ebp),%eax
 728:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 72b:	76 d4                	jbe    701 <free+0x19>
 72d:	8b 45 fc             	mov    -0x4(%ebp),%eax
 730:	8b 00                	mov    (%eax),%eax
 732:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 735:	76 ca                	jbe    701 <free+0x19>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if(bp + bp->s.size == p->s.ptr){
 737:	8b 45 f8             	mov    -0x8(%ebp),%eax
 73a:	8b 40 04             	mov    0x4(%eax),%eax
 73d:	8d 14 c5 00 00 00 00 	lea    0x0(,%eax,8),%edx
 744:	8b 45 f8             	mov    -0x8(%ebp),%eax
 747:	01 c2                	add    %eax,%edx
 749:	8b 45 fc             	mov    -0x4(%ebp),%eax
 74c:	8b 00                	mov    (%eax),%eax
 74e:	39 c2                	cmp    %eax,%edx
 750:	75 24                	jne    776 <free+0x8e>
    bp->s.size += p->s.ptr->s.size;
 752:	8b 45 f8             	mov    -0x8(%ebp),%eax
 755:	8b 50 04             	mov    0x4(%eax),%edx
 758:	8b 45 fc             	mov    -0x4(%ebp),%eax
 75b:	8b 00                	mov    (%eax),%eax
 75d:	8b 40 04             	mov    0x4(%eax),%eax
 760:	01 c2                	add    %eax,%edx
 762:	8b 45 f8             	mov    -0x8(%ebp),%eax
 765:	89 50 04             	mov    %edx,0x4(%eax)
    bp->s.ptr = p->s.ptr->s.ptr;
 768:	8b 45 fc             	mov    -0x4(%ebp),%eax
 76b:	8b 00                	mov    (%eax),%eax
 76d:	8b 10                	mov    (%eax),%edx
 76f:	8b 45 f8             	mov    -0x8(%ebp),%eax
 772:	89 10                	mov    %edx,(%eax)
 774:	eb 0a                	jmp    780 <free+0x98>
  } else
    bp->s.ptr = p->s.ptr;
 776:	8b 45 fc             	mov    -0x4(%ebp),%eax
 779:	8b 10                	mov    (%eax),%edx
 77b:	8b 45 f8             	mov    -0x8(%ebp),%eax
 77e:	89 10                	mov    %edx,(%eax)
  if(p + p->s.size == bp){
 780:	8b 45 fc             	mov    -0x4(%ebp),%eax
 783:	8b 40 04             	mov    0x4(%eax),%eax
 786:	8d 14 c5 00 00 00 00 	lea    0x0(,%eax,8),%edx
 78d:	8b 45 fc             	mov    -0x4(%ebp),%eax
 790:	01 d0                	add    %edx,%eax
 792:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 795:	75 20                	jne    7b7 <free+0xcf>
    p->s.size += bp->s.size;
 797:	8b 45 fc             	mov    -0x4(%ebp),%eax
 79a:	8b 50 04             	mov    0x4(%eax),%edx
 79d:	8b 45 f8             	mov    -0x8(%ebp),%eax
 7a0:	8b 40 04             	mov    0x4(%eax),%eax
 7a3:	01 c2                	add    %eax,%edx
 7a5:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7a8:	89 50 04             	mov    %edx,0x4(%eax)
    p->s.ptr = bp->s.ptr;
 7ab:	8b 45 f8             	mov    -0x8(%ebp),%eax
 7ae:	8b 10                	mov    (%eax),%edx
 7b0:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7b3:	89 10                	mov    %edx,(%eax)
 7b5:	eb 08                	jmp    7bf <free+0xd7>
  } else
    p->s.ptr = bp;
 7b7:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7ba:	8b 55 f8             	mov    -0x8(%ebp),%edx
 7bd:	89 10                	mov    %edx,(%eax)
  freep = p;
 7bf:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7c2:	a3 00 0c 00 00       	mov    %eax,0xc00
}
 7c7:	90                   	nop
 7c8:	c9                   	leave  
 7c9:	c3                   	ret    

000007ca <morecore>:

static Header*
morecore(uint nu)
{
 7ca:	55                   	push   %ebp
 7cb:	89 e5                	mov    %esp,%ebp
 7cd:	83 ec 18             	sub    $0x18,%esp
  char *p;
  Header *hp;

  if(nu < 4096)
 7d0:	81 7d 08 ff 0f 00 00 	cmpl   $0xfff,0x8(%ebp)
 7d7:	77 07                	ja     7e0 <morecore+0x16>
    nu = 4096;
 7d9:	c7 45 08 00 10 00 00 	movl   $0x1000,0x8(%ebp)
  p = sbrk(nu * sizeof(Header));
 7e0:	8b 45 08             	mov    0x8(%ebp),%eax
 7e3:	c1 e0 03             	shl    $0x3,%eax
 7e6:	83 ec 0c             	sub    $0xc,%esp
 7e9:	50                   	push   %eax
 7ea:	e8 31 fc ff ff       	call   420 <sbrk>
 7ef:	83 c4 10             	add    $0x10,%esp
 7f2:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(p == (char*)-1)
 7f5:	83 7d f4 ff          	cmpl   $0xffffffff,-0xc(%ebp)
 7f9:	75 07                	jne    802 <morecore+0x38>
    return 0;
 7fb:	b8 00 00 00 00       	mov    $0x0,%eax
 800:	eb 26                	jmp    828 <morecore+0x5e>
  hp = (Header*)p;
 802:	8b 45 f4             	mov    -0xc(%ebp),%eax
 805:	89 45 f0             	mov    %eax,-0x10(%ebp)
  hp->s.size = nu;
 808:	8b 45 f0             	mov    -0x10(%ebp),%eax
 80b:	8b 55 08             	mov    0x8(%ebp),%edx
 80e:	89 50 04             	mov    %edx,0x4(%eax)
  free((void*)(hp + 1));
 811:	8b 45 f0             	mov    -0x10(%ebp),%eax
 814:	83 c0 08             	add    $0x8,%eax
 817:	83 ec 0c             	sub    $0xc,%esp
 81a:	50                   	push   %eax
 81b:	e8 c8 fe ff ff       	call   6e8 <free>
 820:	83 c4 10             	add    $0x10,%esp
  return freep;
 823:	a1 00 0c 00 00       	mov    0xc00,%eax
}
 828:	c9                   	leave  
 829:	c3                   	ret    

0000082a <malloc>:

void*
malloc(uint nbytes)
{
 82a:	55                   	push   %ebp
 82b:	89 e5                	mov    %esp,%ebp
 82d:	83 ec 18             	sub    $0x18,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 830:	8b 45 08             	mov    0x8(%ebp),%eax
 833:	83 c0 07             	add    $0x7,%eax
 836:	c1 e8 03             	shr    $0x3,%eax
 839:	83 c0 01             	add    $0x1,%eax
 83c:	89 45 ec             	mov    %eax,-0x14(%ebp)
  if((prevp = freep) == 0){
 83f:	a1 00 0c 00 00       	mov    0xc00,%eax
 844:	89 45 f0             	mov    %eax,-0x10(%ebp)
 847:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 84b:	75 23                	jne    870 <malloc+0x46>
    base.s.ptr = freep = prevp = &base;
 84d:	c7 45 f0 f8 0b 00 00 	movl   $0xbf8,-0x10(%ebp)
 854:	8b 45 f0             	mov    -0x10(%ebp),%eax
 857:	a3 00 0c 00 00       	mov    %eax,0xc00
 85c:	a1 00 0c 00 00       	mov    0xc00,%eax
 861:	a3 f8 0b 00 00       	mov    %eax,0xbf8
    base.s.size = 0;
 866:	c7 05 fc 0b 00 00 00 	movl   $0x0,0xbfc
 86d:	00 00 00 
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 870:	8b 45 f0             	mov    -0x10(%ebp),%eax
 873:	8b 00                	mov    (%eax),%eax
 875:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if(p->s.size >= nunits){
 878:	8b 45 f4             	mov    -0xc(%ebp),%eax
 87b:	8b 40 04             	mov    0x4(%eax),%eax
 87e:	3b 45 ec             	cmp    -0x14(%ebp),%eax
 881:	72 4d                	jb     8d0 <malloc+0xa6>
      if(p->s.size == nunits)
 883:	8b 45 f4             	mov    -0xc(%ebp),%eax
 886:	8b 40 04             	mov    0x4(%eax),%eax
 889:	3b 45 ec             	cmp    -0x14(%ebp),%eax
 88c:	75 0c                	jne    89a <malloc+0x70>
        prevp->s.ptr = p->s.ptr;
 88e:	8b 45 f4             	mov    -0xc(%ebp),%eax
 891:	8b 10                	mov    (%eax),%edx
 893:	8b 45 f0             	mov    -0x10(%ebp),%eax
 896:	89 10                	mov    %edx,(%eax)
 898:	eb 26                	jmp    8c0 <malloc+0x96>
      else {
        p->s.size -= nunits;
 89a:	8b 45 f4             	mov    -0xc(%ebp),%eax
 89d:	8b 40 04             	mov    0x4(%eax),%eax
 8a0:	2b 45 ec             	sub    -0x14(%ebp),%eax
 8a3:	89 c2                	mov    %eax,%edx
 8a5:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8a8:	89 50 04             	mov    %edx,0x4(%eax)
        p += p->s.size;
 8ab:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8ae:	8b 40 04             	mov    0x4(%eax),%eax
 8b1:	c1 e0 03             	shl    $0x3,%eax
 8b4:	01 45 f4             	add    %eax,-0xc(%ebp)
        p->s.size = nunits;
 8b7:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8ba:	8b 55 ec             	mov    -0x14(%ebp),%edx
 8bd:	89 50 04             	mov    %edx,0x4(%eax)
      }
      freep = prevp;
 8c0:	8b 45 f0             	mov    -0x10(%ebp),%eax
 8c3:	a3 00 0c 00 00       	mov    %eax,0xc00
      return (void*)(p + 1);
 8c8:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8cb:	83 c0 08             	add    $0x8,%eax
 8ce:	eb 3b                	jmp    90b <malloc+0xe1>
    }
    if(p == freep)
 8d0:	a1 00 0c 00 00       	mov    0xc00,%eax
 8d5:	39 45 f4             	cmp    %eax,-0xc(%ebp)
 8d8:	75 1e                	jne    8f8 <malloc+0xce>
      if((p = morecore(nunits)) == 0)
 8da:	83 ec 0c             	sub    $0xc,%esp
 8dd:	ff 75 ec             	pushl  -0x14(%ebp)
 8e0:	e8 e5 fe ff ff       	call   7ca <morecore>
 8e5:	83 c4 10             	add    $0x10,%esp
 8e8:	89 45 f4             	mov    %eax,-0xc(%ebp)
 8eb:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 8ef:	75 07                	jne    8f8 <malloc+0xce>
        return 0;
 8f1:	b8 00 00 00 00       	mov    $0x0,%eax
 8f6:	eb 13                	jmp    90b <malloc+0xe1>
  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
  if((prevp = freep) == 0){
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 8f8:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8fb:	89 45 f0             	mov    %eax,-0x10(%ebp)
 8fe:	8b 45 f4             	mov    -0xc(%ebp),%eax
 901:	8b 00                	mov    (%eax),%eax
 903:	89 45 f4             	mov    %eax,-0xc(%ebp)
      return (void*)(p + 1);
    }
    if(p == freep)
      if((p = morecore(nunits)) == 0)
        return 0;
  }
 906:	e9 6d ff ff ff       	jmp    878 <malloc+0x4e>
}
 90b:	c9                   	leave  
 90c:	c3                   	ret    
