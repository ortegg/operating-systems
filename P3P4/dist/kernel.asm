
kernel:     file format elf32-i386


Disassembly of section .text:

80100000 <multiboot_header>:
80100000:	02 b0 ad 1b 00 00    	add    0x1bad(%eax),%dh
80100006:	00 00                	add    %al,(%eax)
80100008:	fe 4f 52             	decb   0x52(%edi)
8010000b:	e4                   	.byte 0xe4

8010000c <entry>:

# Entering xv6 on boot processor, with paging off.
.globl entry
entry:
  # Turn on page size extension for 4Mbyte pages
  movl    %cr4, %eax
8010000c:	0f 20 e0             	mov    %cr4,%eax
  orl     $(CR4_PSE), %eax
8010000f:	83 c8 10             	or     $0x10,%eax
  movl    %eax, %cr4
80100012:	0f 22 e0             	mov    %eax,%cr4
  # Set page directory
  movl    $(V2P_WO(entrypgdir)), %eax
80100015:	b8 00 b0 10 00       	mov    $0x10b000,%eax
  movl    %eax, %cr3
8010001a:	0f 22 d8             	mov    %eax,%cr3
  # Turn on paging.
  movl    %cr0, %eax
8010001d:	0f 20 c0             	mov    %cr0,%eax
  orl     $(CR0_PG|CR0_WP), %eax
80100020:	0d 00 00 01 80       	or     $0x80010000,%eax
  movl    %eax, %cr0
80100025:	0f 22 c0             	mov    %eax,%cr0

  # Set up the stack pointer.
  movl $(stack + KSTACKSIZE), %esp
80100028:	bc 70 d6 10 80       	mov    $0x8010d670,%esp

  # Jump to main(), and switch to executing at
  # high addresses. The indirect call is needed because
  # the assembler produces a PC-relative instruction
  # for a direct jump.
  mov $main, %eax
8010002d:	b8 2d 39 10 80       	mov    $0x8010392d,%eax
  jmp *%eax
80100032:	ff e0                	jmp    *%eax

80100034 <binit>:
  struct buf head;
} bcache;

void
binit(void)
{
80100034:	55                   	push   %ebp
80100035:	89 e5                	mov    %esp,%ebp
80100037:	83 ec 18             	sub    $0x18,%esp
  struct buf *b;

  initlock(&bcache.lock, "bcache");
8010003a:	83 ec 08             	sub    $0x8,%esp
8010003d:	68 04 92 10 80       	push   $0x80109204
80100042:	68 80 d6 10 80       	push   $0x8010d680
80100047:	e8 da 5a 00 00       	call   80105b26 <initlock>
8010004c:	83 c4 10             	add    $0x10,%esp

  // Create linked list of buffers
  bcache.head.prev = &bcache.head;
8010004f:	c7 05 90 15 11 80 84 	movl   $0x80111584,0x80111590
80100056:	15 11 80 
  bcache.head.next = &bcache.head;
80100059:	c7 05 94 15 11 80 84 	movl   $0x80111584,0x80111594
80100060:	15 11 80 
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
80100063:	c7 45 f4 b4 d6 10 80 	movl   $0x8010d6b4,-0xc(%ebp)
8010006a:	eb 3a                	jmp    801000a6 <binit+0x72>
    b->next = bcache.head.next;
8010006c:	8b 15 94 15 11 80    	mov    0x80111594,%edx
80100072:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100075:	89 50 10             	mov    %edx,0x10(%eax)
    b->prev = &bcache.head;
80100078:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010007b:	c7 40 0c 84 15 11 80 	movl   $0x80111584,0xc(%eax)
    b->dev = -1;
80100082:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100085:	c7 40 04 ff ff ff ff 	movl   $0xffffffff,0x4(%eax)
    bcache.head.next->prev = b;
8010008c:	a1 94 15 11 80       	mov    0x80111594,%eax
80100091:	8b 55 f4             	mov    -0xc(%ebp),%edx
80100094:	89 50 0c             	mov    %edx,0xc(%eax)
    bcache.head.next = b;
80100097:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010009a:	a3 94 15 11 80       	mov    %eax,0x80111594
  initlock(&bcache.lock, "bcache");

  // Create linked list of buffers
  bcache.head.prev = &bcache.head;
  bcache.head.next = &bcache.head;
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
8010009f:	81 45 f4 18 02 00 00 	addl   $0x218,-0xc(%ebp)
801000a6:	b8 84 15 11 80       	mov    $0x80111584,%eax
801000ab:	39 45 f4             	cmp    %eax,-0xc(%ebp)
801000ae:	72 bc                	jb     8010006c <binit+0x38>
    b->prev = &bcache.head;
    b->dev = -1;
    bcache.head.next->prev = b;
    bcache.head.next = b;
  }
}
801000b0:	90                   	nop
801000b1:	c9                   	leave  
801000b2:	c3                   	ret    

801000b3 <bget>:
// Look through buffer cache for block on device dev.
// If not found, allocate a buffer.
// In either case, return B_BUSY buffer.
static struct buf*
bget(uint dev, uint blockno)
{
801000b3:	55                   	push   %ebp
801000b4:	89 e5                	mov    %esp,%ebp
801000b6:	83 ec 18             	sub    $0x18,%esp
  struct buf *b;

  acquire(&bcache.lock);
801000b9:	83 ec 0c             	sub    $0xc,%esp
801000bc:	68 80 d6 10 80       	push   $0x8010d680
801000c1:	e8 82 5a 00 00       	call   80105b48 <acquire>
801000c6:	83 c4 10             	add    $0x10,%esp

 loop:
  // Is the block already cached?
  for(b = bcache.head.next; b != &bcache.head; b = b->next){
801000c9:	a1 94 15 11 80       	mov    0x80111594,%eax
801000ce:	89 45 f4             	mov    %eax,-0xc(%ebp)
801000d1:	eb 67                	jmp    8010013a <bget+0x87>
    if(b->dev == dev && b->blockno == blockno){
801000d3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801000d6:	8b 40 04             	mov    0x4(%eax),%eax
801000d9:	3b 45 08             	cmp    0x8(%ebp),%eax
801000dc:	75 53                	jne    80100131 <bget+0x7e>
801000de:	8b 45 f4             	mov    -0xc(%ebp),%eax
801000e1:	8b 40 08             	mov    0x8(%eax),%eax
801000e4:	3b 45 0c             	cmp    0xc(%ebp),%eax
801000e7:	75 48                	jne    80100131 <bget+0x7e>
      if(!(b->flags & B_BUSY)){
801000e9:	8b 45 f4             	mov    -0xc(%ebp),%eax
801000ec:	8b 00                	mov    (%eax),%eax
801000ee:	83 e0 01             	and    $0x1,%eax
801000f1:	85 c0                	test   %eax,%eax
801000f3:	75 27                	jne    8010011c <bget+0x69>
        b->flags |= B_BUSY;
801000f5:	8b 45 f4             	mov    -0xc(%ebp),%eax
801000f8:	8b 00                	mov    (%eax),%eax
801000fa:	83 c8 01             	or     $0x1,%eax
801000fd:	89 c2                	mov    %eax,%edx
801000ff:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100102:	89 10                	mov    %edx,(%eax)
        release(&bcache.lock);
80100104:	83 ec 0c             	sub    $0xc,%esp
80100107:	68 80 d6 10 80       	push   $0x8010d680
8010010c:	e8 9e 5a 00 00       	call   80105baf <release>
80100111:	83 c4 10             	add    $0x10,%esp
        return b;
80100114:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100117:	e9 98 00 00 00       	jmp    801001b4 <bget+0x101>
      }
      sleep(b, &bcache.lock);
8010011c:	83 ec 08             	sub    $0x8,%esp
8010011f:	68 80 d6 10 80       	push   $0x8010d680
80100124:	ff 75 f4             	pushl  -0xc(%ebp)
80100127:	e8 38 52 00 00       	call   80105364 <sleep>
8010012c:	83 c4 10             	add    $0x10,%esp
      goto loop;
8010012f:	eb 98                	jmp    801000c9 <bget+0x16>

  acquire(&bcache.lock);

 loop:
  // Is the block already cached?
  for(b = bcache.head.next; b != &bcache.head; b = b->next){
80100131:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100134:	8b 40 10             	mov    0x10(%eax),%eax
80100137:	89 45 f4             	mov    %eax,-0xc(%ebp)
8010013a:	81 7d f4 84 15 11 80 	cmpl   $0x80111584,-0xc(%ebp)
80100141:	75 90                	jne    801000d3 <bget+0x20>
  }

  // Not cached; recycle some non-busy and clean buffer.
  // "clean" because B_DIRTY and !B_BUSY means log.c
  // hasn't yet committed the changes to the buffer.
  for(b = bcache.head.prev; b != &bcache.head; b = b->prev){
80100143:	a1 90 15 11 80       	mov    0x80111590,%eax
80100148:	89 45 f4             	mov    %eax,-0xc(%ebp)
8010014b:	eb 51                	jmp    8010019e <bget+0xeb>
    if((b->flags & B_BUSY) == 0 && (b->flags & B_DIRTY) == 0){
8010014d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100150:	8b 00                	mov    (%eax),%eax
80100152:	83 e0 01             	and    $0x1,%eax
80100155:	85 c0                	test   %eax,%eax
80100157:	75 3c                	jne    80100195 <bget+0xe2>
80100159:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010015c:	8b 00                	mov    (%eax),%eax
8010015e:	83 e0 04             	and    $0x4,%eax
80100161:	85 c0                	test   %eax,%eax
80100163:	75 30                	jne    80100195 <bget+0xe2>
      b->dev = dev;
80100165:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100168:	8b 55 08             	mov    0x8(%ebp),%edx
8010016b:	89 50 04             	mov    %edx,0x4(%eax)
      b->blockno = blockno;
8010016e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100171:	8b 55 0c             	mov    0xc(%ebp),%edx
80100174:	89 50 08             	mov    %edx,0x8(%eax)
      b->flags = B_BUSY;
80100177:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010017a:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
      release(&bcache.lock);
80100180:	83 ec 0c             	sub    $0xc,%esp
80100183:	68 80 d6 10 80       	push   $0x8010d680
80100188:	e8 22 5a 00 00       	call   80105baf <release>
8010018d:	83 c4 10             	add    $0x10,%esp
      return b;
80100190:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100193:	eb 1f                	jmp    801001b4 <bget+0x101>
  }

  // Not cached; recycle some non-busy and clean buffer.
  // "clean" because B_DIRTY and !B_BUSY means log.c
  // hasn't yet committed the changes to the buffer.
  for(b = bcache.head.prev; b != &bcache.head; b = b->prev){
80100195:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100198:	8b 40 0c             	mov    0xc(%eax),%eax
8010019b:	89 45 f4             	mov    %eax,-0xc(%ebp)
8010019e:	81 7d f4 84 15 11 80 	cmpl   $0x80111584,-0xc(%ebp)
801001a5:	75 a6                	jne    8010014d <bget+0x9a>
      b->flags = B_BUSY;
      release(&bcache.lock);
      return b;
    }
  }
  panic("bget: no buffers");
801001a7:	83 ec 0c             	sub    $0xc,%esp
801001aa:	68 0b 92 10 80       	push   $0x8010920b
801001af:	e8 b2 03 00 00       	call   80100566 <panic>
}
801001b4:	c9                   	leave  
801001b5:	c3                   	ret    

801001b6 <bread>:

// Return a B_BUSY buf with the contents of the indicated block.
struct buf*
bread(uint dev, uint blockno)
{
801001b6:	55                   	push   %ebp
801001b7:	89 e5                	mov    %esp,%ebp
801001b9:	83 ec 18             	sub    $0x18,%esp
  struct buf *b;

  b = bget(dev, blockno);
801001bc:	83 ec 08             	sub    $0x8,%esp
801001bf:	ff 75 0c             	pushl  0xc(%ebp)
801001c2:	ff 75 08             	pushl  0x8(%ebp)
801001c5:	e8 e9 fe ff ff       	call   801000b3 <bget>
801001ca:	83 c4 10             	add    $0x10,%esp
801001cd:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(!(b->flags & B_VALID)) {
801001d0:	8b 45 f4             	mov    -0xc(%ebp),%eax
801001d3:	8b 00                	mov    (%eax),%eax
801001d5:	83 e0 02             	and    $0x2,%eax
801001d8:	85 c0                	test   %eax,%eax
801001da:	75 0e                	jne    801001ea <bread+0x34>
    iderw(b);
801001dc:	83 ec 0c             	sub    $0xc,%esp
801001df:	ff 75 f4             	pushl  -0xc(%ebp)
801001e2:	e8 c4 27 00 00       	call   801029ab <iderw>
801001e7:	83 c4 10             	add    $0x10,%esp
  }
  return b;
801001ea:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
801001ed:	c9                   	leave  
801001ee:	c3                   	ret    

801001ef <bwrite>:

// Write b's contents to disk.  Must be B_BUSY.
void
bwrite(struct buf *b)
{
801001ef:	55                   	push   %ebp
801001f0:	89 e5                	mov    %esp,%ebp
801001f2:	83 ec 08             	sub    $0x8,%esp
  if((b->flags & B_BUSY) == 0)
801001f5:	8b 45 08             	mov    0x8(%ebp),%eax
801001f8:	8b 00                	mov    (%eax),%eax
801001fa:	83 e0 01             	and    $0x1,%eax
801001fd:	85 c0                	test   %eax,%eax
801001ff:	75 0d                	jne    8010020e <bwrite+0x1f>
    panic("bwrite");
80100201:	83 ec 0c             	sub    $0xc,%esp
80100204:	68 1c 92 10 80       	push   $0x8010921c
80100209:	e8 58 03 00 00       	call   80100566 <panic>
  b->flags |= B_DIRTY;
8010020e:	8b 45 08             	mov    0x8(%ebp),%eax
80100211:	8b 00                	mov    (%eax),%eax
80100213:	83 c8 04             	or     $0x4,%eax
80100216:	89 c2                	mov    %eax,%edx
80100218:	8b 45 08             	mov    0x8(%ebp),%eax
8010021b:	89 10                	mov    %edx,(%eax)
  iderw(b);
8010021d:	83 ec 0c             	sub    $0xc,%esp
80100220:	ff 75 08             	pushl  0x8(%ebp)
80100223:	e8 83 27 00 00       	call   801029ab <iderw>
80100228:	83 c4 10             	add    $0x10,%esp
}
8010022b:	90                   	nop
8010022c:	c9                   	leave  
8010022d:	c3                   	ret    

8010022e <brelse>:

// Release a B_BUSY buffer.
// Move to the head of the MRU list.
void
brelse(struct buf *b)
{
8010022e:	55                   	push   %ebp
8010022f:	89 e5                	mov    %esp,%ebp
80100231:	83 ec 08             	sub    $0x8,%esp
  if((b->flags & B_BUSY) == 0)
80100234:	8b 45 08             	mov    0x8(%ebp),%eax
80100237:	8b 00                	mov    (%eax),%eax
80100239:	83 e0 01             	and    $0x1,%eax
8010023c:	85 c0                	test   %eax,%eax
8010023e:	75 0d                	jne    8010024d <brelse+0x1f>
    panic("brelse");
80100240:	83 ec 0c             	sub    $0xc,%esp
80100243:	68 23 92 10 80       	push   $0x80109223
80100248:	e8 19 03 00 00       	call   80100566 <panic>

  acquire(&bcache.lock);
8010024d:	83 ec 0c             	sub    $0xc,%esp
80100250:	68 80 d6 10 80       	push   $0x8010d680
80100255:	e8 ee 58 00 00       	call   80105b48 <acquire>
8010025a:	83 c4 10             	add    $0x10,%esp

  b->next->prev = b->prev;
8010025d:	8b 45 08             	mov    0x8(%ebp),%eax
80100260:	8b 40 10             	mov    0x10(%eax),%eax
80100263:	8b 55 08             	mov    0x8(%ebp),%edx
80100266:	8b 52 0c             	mov    0xc(%edx),%edx
80100269:	89 50 0c             	mov    %edx,0xc(%eax)
  b->prev->next = b->next;
8010026c:	8b 45 08             	mov    0x8(%ebp),%eax
8010026f:	8b 40 0c             	mov    0xc(%eax),%eax
80100272:	8b 55 08             	mov    0x8(%ebp),%edx
80100275:	8b 52 10             	mov    0x10(%edx),%edx
80100278:	89 50 10             	mov    %edx,0x10(%eax)
  b->next = bcache.head.next;
8010027b:	8b 15 94 15 11 80    	mov    0x80111594,%edx
80100281:	8b 45 08             	mov    0x8(%ebp),%eax
80100284:	89 50 10             	mov    %edx,0x10(%eax)
  b->prev = &bcache.head;
80100287:	8b 45 08             	mov    0x8(%ebp),%eax
8010028a:	c7 40 0c 84 15 11 80 	movl   $0x80111584,0xc(%eax)
  bcache.head.next->prev = b;
80100291:	a1 94 15 11 80       	mov    0x80111594,%eax
80100296:	8b 55 08             	mov    0x8(%ebp),%edx
80100299:	89 50 0c             	mov    %edx,0xc(%eax)
  bcache.head.next = b;
8010029c:	8b 45 08             	mov    0x8(%ebp),%eax
8010029f:	a3 94 15 11 80       	mov    %eax,0x80111594

  b->flags &= ~B_BUSY;
801002a4:	8b 45 08             	mov    0x8(%ebp),%eax
801002a7:	8b 00                	mov    (%eax),%eax
801002a9:	83 e0 fe             	and    $0xfffffffe,%eax
801002ac:	89 c2                	mov    %eax,%edx
801002ae:	8b 45 08             	mov    0x8(%ebp),%eax
801002b1:	89 10                	mov    %edx,(%eax)
  wakeup(b);
801002b3:	83 ec 0c             	sub    $0xc,%esp
801002b6:	ff 75 08             	pushl  0x8(%ebp)
801002b9:	e8 bb 51 00 00       	call   80105479 <wakeup>
801002be:	83 c4 10             	add    $0x10,%esp

  release(&bcache.lock);
801002c1:	83 ec 0c             	sub    $0xc,%esp
801002c4:	68 80 d6 10 80       	push   $0x8010d680
801002c9:	e8 e1 58 00 00       	call   80105baf <release>
801002ce:	83 c4 10             	add    $0x10,%esp
}
801002d1:	90                   	nop
801002d2:	c9                   	leave  
801002d3:	c3                   	ret    

801002d4 <inb>:

// end of CS333 added routines

static inline uchar
inb(ushort port)
{
801002d4:	55                   	push   %ebp
801002d5:	89 e5                	mov    %esp,%ebp
801002d7:	83 ec 14             	sub    $0x14,%esp
801002da:	8b 45 08             	mov    0x8(%ebp),%eax
801002dd:	66 89 45 ec          	mov    %ax,-0x14(%ebp)
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801002e1:	0f b7 45 ec          	movzwl -0x14(%ebp),%eax
801002e5:	89 c2                	mov    %eax,%edx
801002e7:	ec                   	in     (%dx),%al
801002e8:	88 45 ff             	mov    %al,-0x1(%ebp)
  return data;
801002eb:	0f b6 45 ff          	movzbl -0x1(%ebp),%eax
}
801002ef:	c9                   	leave  
801002f0:	c3                   	ret    

801002f1 <outb>:
               "memory", "cc");
}

static inline void
outb(ushort port, uchar data)
{
801002f1:	55                   	push   %ebp
801002f2:	89 e5                	mov    %esp,%ebp
801002f4:	83 ec 08             	sub    $0x8,%esp
801002f7:	8b 55 08             	mov    0x8(%ebp),%edx
801002fa:	8b 45 0c             	mov    0xc(%ebp),%eax
801002fd:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
80100301:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80100304:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
80100308:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
8010030c:	ee                   	out    %al,(%dx)
}
8010030d:	90                   	nop
8010030e:	c9                   	leave  
8010030f:	c3                   	ret    

80100310 <cli>:
  asm volatile("movw %0, %%gs" : : "r" (v));
}

static inline void
cli(void)
{
80100310:	55                   	push   %ebp
80100311:	89 e5                	mov    %esp,%ebp
  asm volatile("cli");
80100313:	fa                   	cli    
}
80100314:	90                   	nop
80100315:	5d                   	pop    %ebp
80100316:	c3                   	ret    

80100317 <printint>:
  int locking;
} cons;

static void
printint(int xx, int base, int sign)
{
80100317:	55                   	push   %ebp
80100318:	89 e5                	mov    %esp,%ebp
8010031a:	53                   	push   %ebx
8010031b:	83 ec 24             	sub    $0x24,%esp
  static char digits[] = "0123456789abcdef";
  char buf[16];
  int i;
  uint x;

  if(sign && (sign = xx < 0))
8010031e:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80100322:	74 1c                	je     80100340 <printint+0x29>
80100324:	8b 45 08             	mov    0x8(%ebp),%eax
80100327:	c1 e8 1f             	shr    $0x1f,%eax
8010032a:	0f b6 c0             	movzbl %al,%eax
8010032d:	89 45 10             	mov    %eax,0x10(%ebp)
80100330:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80100334:	74 0a                	je     80100340 <printint+0x29>
    x = -xx;
80100336:	8b 45 08             	mov    0x8(%ebp),%eax
80100339:	f7 d8                	neg    %eax
8010033b:	89 45 f0             	mov    %eax,-0x10(%ebp)
8010033e:	eb 06                	jmp    80100346 <printint+0x2f>
  else
    x = xx;
80100340:	8b 45 08             	mov    0x8(%ebp),%eax
80100343:	89 45 f0             	mov    %eax,-0x10(%ebp)

  i = 0;
80100346:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  do{
    buf[i++] = digits[x % base];
8010034d:	8b 4d f4             	mov    -0xc(%ebp),%ecx
80100350:	8d 41 01             	lea    0x1(%ecx),%eax
80100353:	89 45 f4             	mov    %eax,-0xc(%ebp)
80100356:	8b 5d 0c             	mov    0xc(%ebp),%ebx
80100359:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010035c:	ba 00 00 00 00       	mov    $0x0,%edx
80100361:	f7 f3                	div    %ebx
80100363:	89 d0                	mov    %edx,%eax
80100365:	0f b6 80 04 a0 10 80 	movzbl -0x7fef5ffc(%eax),%eax
8010036c:	88 44 0d e0          	mov    %al,-0x20(%ebp,%ecx,1)
  }while((x /= base) != 0);
80100370:	8b 5d 0c             	mov    0xc(%ebp),%ebx
80100373:	8b 45 f0             	mov    -0x10(%ebp),%eax
80100376:	ba 00 00 00 00       	mov    $0x0,%edx
8010037b:	f7 f3                	div    %ebx
8010037d:	89 45 f0             	mov    %eax,-0x10(%ebp)
80100380:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80100384:	75 c7                	jne    8010034d <printint+0x36>

  if(sign)
80100386:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
8010038a:	74 2a                	je     801003b6 <printint+0x9f>
    buf[i++] = '-';
8010038c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010038f:	8d 50 01             	lea    0x1(%eax),%edx
80100392:	89 55 f4             	mov    %edx,-0xc(%ebp)
80100395:	c6 44 05 e0 2d       	movb   $0x2d,-0x20(%ebp,%eax,1)

  while(--i >= 0)
8010039a:	eb 1a                	jmp    801003b6 <printint+0x9f>
    consputc(buf[i]);
8010039c:	8d 55 e0             	lea    -0x20(%ebp),%edx
8010039f:	8b 45 f4             	mov    -0xc(%ebp),%eax
801003a2:	01 d0                	add    %edx,%eax
801003a4:	0f b6 00             	movzbl (%eax),%eax
801003a7:	0f be c0             	movsbl %al,%eax
801003aa:	83 ec 0c             	sub    $0xc,%esp
801003ad:	50                   	push   %eax
801003ae:	e8 df 03 00 00       	call   80100792 <consputc>
801003b3:	83 c4 10             	add    $0x10,%esp
  }while((x /= base) != 0);

  if(sign)
    buf[i++] = '-';

  while(--i >= 0)
801003b6:	83 6d f4 01          	subl   $0x1,-0xc(%ebp)
801003ba:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801003be:	79 dc                	jns    8010039c <printint+0x85>
    consputc(buf[i]);
}
801003c0:	90                   	nop
801003c1:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801003c4:	c9                   	leave  
801003c5:	c3                   	ret    

801003c6 <cprintf>:

// Print to the console. only understands %d, %x, %p, %s.
void
cprintf(char *fmt, ...)
{
801003c6:	55                   	push   %ebp
801003c7:	89 e5                	mov    %esp,%ebp
801003c9:	83 ec 28             	sub    $0x28,%esp
  int i, c, locking;
  uint *argp;
  char *s;

  locking = cons.locking;
801003cc:	a1 14 c6 10 80       	mov    0x8010c614,%eax
801003d1:	89 45 e8             	mov    %eax,-0x18(%ebp)
  if(locking)
801003d4:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
801003d8:	74 10                	je     801003ea <cprintf+0x24>
    acquire(&cons.lock);
801003da:	83 ec 0c             	sub    $0xc,%esp
801003dd:	68 e0 c5 10 80       	push   $0x8010c5e0
801003e2:	e8 61 57 00 00       	call   80105b48 <acquire>
801003e7:	83 c4 10             	add    $0x10,%esp

  if (fmt == 0)
801003ea:	8b 45 08             	mov    0x8(%ebp),%eax
801003ed:	85 c0                	test   %eax,%eax
801003ef:	75 0d                	jne    801003fe <cprintf+0x38>
    panic("null fmt");
801003f1:	83 ec 0c             	sub    $0xc,%esp
801003f4:	68 2a 92 10 80       	push   $0x8010922a
801003f9:	e8 68 01 00 00       	call   80100566 <panic>

  argp = (uint*)(void*)(&fmt + 1);
801003fe:	8d 45 0c             	lea    0xc(%ebp),%eax
80100401:	89 45 f0             	mov    %eax,-0x10(%ebp)
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
80100404:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
8010040b:	e9 1a 01 00 00       	jmp    8010052a <cprintf+0x164>
    if(c != '%'){
80100410:	83 7d e4 25          	cmpl   $0x25,-0x1c(%ebp)
80100414:	74 13                	je     80100429 <cprintf+0x63>
      consputc(c);
80100416:	83 ec 0c             	sub    $0xc,%esp
80100419:	ff 75 e4             	pushl  -0x1c(%ebp)
8010041c:	e8 71 03 00 00       	call   80100792 <consputc>
80100421:	83 c4 10             	add    $0x10,%esp
      continue;
80100424:	e9 fd 00 00 00       	jmp    80100526 <cprintf+0x160>
    }
    c = fmt[++i] & 0xff;
80100429:	8b 55 08             	mov    0x8(%ebp),%edx
8010042c:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80100430:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100433:	01 d0                	add    %edx,%eax
80100435:	0f b6 00             	movzbl (%eax),%eax
80100438:	0f be c0             	movsbl %al,%eax
8010043b:	25 ff 00 00 00       	and    $0xff,%eax
80100440:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    if(c == 0)
80100443:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
80100447:	0f 84 ff 00 00 00    	je     8010054c <cprintf+0x186>
      break;
    switch(c){
8010044d:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100450:	83 f8 70             	cmp    $0x70,%eax
80100453:	74 47                	je     8010049c <cprintf+0xd6>
80100455:	83 f8 70             	cmp    $0x70,%eax
80100458:	7f 13                	jg     8010046d <cprintf+0xa7>
8010045a:	83 f8 25             	cmp    $0x25,%eax
8010045d:	0f 84 98 00 00 00    	je     801004fb <cprintf+0x135>
80100463:	83 f8 64             	cmp    $0x64,%eax
80100466:	74 14                	je     8010047c <cprintf+0xb6>
80100468:	e9 9d 00 00 00       	jmp    8010050a <cprintf+0x144>
8010046d:	83 f8 73             	cmp    $0x73,%eax
80100470:	74 47                	je     801004b9 <cprintf+0xf3>
80100472:	83 f8 78             	cmp    $0x78,%eax
80100475:	74 25                	je     8010049c <cprintf+0xd6>
80100477:	e9 8e 00 00 00       	jmp    8010050a <cprintf+0x144>
    case 'd':
      printint(*argp++, 10, 1);
8010047c:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010047f:	8d 50 04             	lea    0x4(%eax),%edx
80100482:	89 55 f0             	mov    %edx,-0x10(%ebp)
80100485:	8b 00                	mov    (%eax),%eax
80100487:	83 ec 04             	sub    $0x4,%esp
8010048a:	6a 01                	push   $0x1
8010048c:	6a 0a                	push   $0xa
8010048e:	50                   	push   %eax
8010048f:	e8 83 fe ff ff       	call   80100317 <printint>
80100494:	83 c4 10             	add    $0x10,%esp
      break;
80100497:	e9 8a 00 00 00       	jmp    80100526 <cprintf+0x160>
    case 'x':
    case 'p':
      printint(*argp++, 16, 0);
8010049c:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010049f:	8d 50 04             	lea    0x4(%eax),%edx
801004a2:	89 55 f0             	mov    %edx,-0x10(%ebp)
801004a5:	8b 00                	mov    (%eax),%eax
801004a7:	83 ec 04             	sub    $0x4,%esp
801004aa:	6a 00                	push   $0x0
801004ac:	6a 10                	push   $0x10
801004ae:	50                   	push   %eax
801004af:	e8 63 fe ff ff       	call   80100317 <printint>
801004b4:	83 c4 10             	add    $0x10,%esp
      break;
801004b7:	eb 6d                	jmp    80100526 <cprintf+0x160>
    case 's':
      if((s = (char*)*argp++) == 0)
801004b9:	8b 45 f0             	mov    -0x10(%ebp),%eax
801004bc:	8d 50 04             	lea    0x4(%eax),%edx
801004bf:	89 55 f0             	mov    %edx,-0x10(%ebp)
801004c2:	8b 00                	mov    (%eax),%eax
801004c4:	89 45 ec             	mov    %eax,-0x14(%ebp)
801004c7:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
801004cb:	75 22                	jne    801004ef <cprintf+0x129>
        s = "(null)";
801004cd:	c7 45 ec 33 92 10 80 	movl   $0x80109233,-0x14(%ebp)
      for(; *s; s++)
801004d4:	eb 19                	jmp    801004ef <cprintf+0x129>
        consputc(*s);
801004d6:	8b 45 ec             	mov    -0x14(%ebp),%eax
801004d9:	0f b6 00             	movzbl (%eax),%eax
801004dc:	0f be c0             	movsbl %al,%eax
801004df:	83 ec 0c             	sub    $0xc,%esp
801004e2:	50                   	push   %eax
801004e3:	e8 aa 02 00 00       	call   80100792 <consputc>
801004e8:	83 c4 10             	add    $0x10,%esp
      printint(*argp++, 16, 0);
      break;
    case 's':
      if((s = (char*)*argp++) == 0)
        s = "(null)";
      for(; *s; s++)
801004eb:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
801004ef:	8b 45 ec             	mov    -0x14(%ebp),%eax
801004f2:	0f b6 00             	movzbl (%eax),%eax
801004f5:	84 c0                	test   %al,%al
801004f7:	75 dd                	jne    801004d6 <cprintf+0x110>
        consputc(*s);
      break;
801004f9:	eb 2b                	jmp    80100526 <cprintf+0x160>
    case '%':
      consputc('%');
801004fb:	83 ec 0c             	sub    $0xc,%esp
801004fe:	6a 25                	push   $0x25
80100500:	e8 8d 02 00 00       	call   80100792 <consputc>
80100505:	83 c4 10             	add    $0x10,%esp
      break;
80100508:	eb 1c                	jmp    80100526 <cprintf+0x160>
    default:
      // Print unknown % sequence to draw attention.
      consputc('%');
8010050a:	83 ec 0c             	sub    $0xc,%esp
8010050d:	6a 25                	push   $0x25
8010050f:	e8 7e 02 00 00       	call   80100792 <consputc>
80100514:	83 c4 10             	add    $0x10,%esp
      consputc(c);
80100517:	83 ec 0c             	sub    $0xc,%esp
8010051a:	ff 75 e4             	pushl  -0x1c(%ebp)
8010051d:	e8 70 02 00 00       	call   80100792 <consputc>
80100522:	83 c4 10             	add    $0x10,%esp
      break;
80100525:	90                   	nop

  if (fmt == 0)
    panic("null fmt");

  argp = (uint*)(void*)(&fmt + 1);
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
80100526:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
8010052a:	8b 55 08             	mov    0x8(%ebp),%edx
8010052d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100530:	01 d0                	add    %edx,%eax
80100532:	0f b6 00             	movzbl (%eax),%eax
80100535:	0f be c0             	movsbl %al,%eax
80100538:	25 ff 00 00 00       	and    $0xff,%eax
8010053d:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80100540:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
80100544:	0f 85 c6 fe ff ff    	jne    80100410 <cprintf+0x4a>
8010054a:	eb 01                	jmp    8010054d <cprintf+0x187>
      consputc(c);
      continue;
    }
    c = fmt[++i] & 0xff;
    if(c == 0)
      break;
8010054c:	90                   	nop
      consputc(c);
      break;
    }
  }

  if(locking)
8010054d:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
80100551:	74 10                	je     80100563 <cprintf+0x19d>
    release(&cons.lock);
80100553:	83 ec 0c             	sub    $0xc,%esp
80100556:	68 e0 c5 10 80       	push   $0x8010c5e0
8010055b:	e8 4f 56 00 00       	call   80105baf <release>
80100560:	83 c4 10             	add    $0x10,%esp
}
80100563:	90                   	nop
80100564:	c9                   	leave  
80100565:	c3                   	ret    

80100566 <panic>:

void
panic(char *s)
{
80100566:	55                   	push   %ebp
80100567:	89 e5                	mov    %esp,%ebp
80100569:	83 ec 38             	sub    $0x38,%esp
  int i;
  uint pcs[10];
  
  cli();
8010056c:	e8 9f fd ff ff       	call   80100310 <cli>
  cons.locking = 0;
80100571:	c7 05 14 c6 10 80 00 	movl   $0x0,0x8010c614
80100578:	00 00 00 
  cprintf("cpu%d: panic: ", cpu->id);
8010057b:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80100581:	0f b6 00             	movzbl (%eax),%eax
80100584:	0f b6 c0             	movzbl %al,%eax
80100587:	83 ec 08             	sub    $0x8,%esp
8010058a:	50                   	push   %eax
8010058b:	68 3a 92 10 80       	push   $0x8010923a
80100590:	e8 31 fe ff ff       	call   801003c6 <cprintf>
80100595:	83 c4 10             	add    $0x10,%esp
  cprintf(s);
80100598:	8b 45 08             	mov    0x8(%ebp),%eax
8010059b:	83 ec 0c             	sub    $0xc,%esp
8010059e:	50                   	push   %eax
8010059f:	e8 22 fe ff ff       	call   801003c6 <cprintf>
801005a4:	83 c4 10             	add    $0x10,%esp
  cprintf("\n");
801005a7:	83 ec 0c             	sub    $0xc,%esp
801005aa:	68 49 92 10 80       	push   $0x80109249
801005af:	e8 12 fe ff ff       	call   801003c6 <cprintf>
801005b4:	83 c4 10             	add    $0x10,%esp
  getcallerpcs(&s, pcs);
801005b7:	83 ec 08             	sub    $0x8,%esp
801005ba:	8d 45 cc             	lea    -0x34(%ebp),%eax
801005bd:	50                   	push   %eax
801005be:	8d 45 08             	lea    0x8(%ebp),%eax
801005c1:	50                   	push   %eax
801005c2:	e8 3a 56 00 00       	call   80105c01 <getcallerpcs>
801005c7:	83 c4 10             	add    $0x10,%esp
  for(i=0; i<10; i++)
801005ca:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
801005d1:	eb 1c                	jmp    801005ef <panic+0x89>
    cprintf(" %p", pcs[i]);
801005d3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801005d6:	8b 44 85 cc          	mov    -0x34(%ebp,%eax,4),%eax
801005da:	83 ec 08             	sub    $0x8,%esp
801005dd:	50                   	push   %eax
801005de:	68 4b 92 10 80       	push   $0x8010924b
801005e3:	e8 de fd ff ff       	call   801003c6 <cprintf>
801005e8:	83 c4 10             	add    $0x10,%esp
  cons.locking = 0;
  cprintf("cpu%d: panic: ", cpu->id);
  cprintf(s);
  cprintf("\n");
  getcallerpcs(&s, pcs);
  for(i=0; i<10; i++)
801005eb:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
801005ef:	83 7d f4 09          	cmpl   $0x9,-0xc(%ebp)
801005f3:	7e de                	jle    801005d3 <panic+0x6d>
    cprintf(" %p", pcs[i]);
  panicked = 1; // freeze other CPU
801005f5:	c7 05 c0 c5 10 80 01 	movl   $0x1,0x8010c5c0
801005fc:	00 00 00 
  for(;;)
    ;
801005ff:	eb fe                	jmp    801005ff <panic+0x99>

80100601 <cgaputc>:
#define CRTPORT 0x3d4
static ushort *crt = (ushort*)P2V(0xb8000);  // CGA memory

static void
cgaputc(int c)
{
80100601:	55                   	push   %ebp
80100602:	89 e5                	mov    %esp,%ebp
80100604:	83 ec 18             	sub    $0x18,%esp
  int pos;
  
  // Cursor position: col + 80*row.
  outb(CRTPORT, 14);
80100607:	6a 0e                	push   $0xe
80100609:	68 d4 03 00 00       	push   $0x3d4
8010060e:	e8 de fc ff ff       	call   801002f1 <outb>
80100613:	83 c4 08             	add    $0x8,%esp
  pos = inb(CRTPORT+1) << 8;
80100616:	68 d5 03 00 00       	push   $0x3d5
8010061b:	e8 b4 fc ff ff       	call   801002d4 <inb>
80100620:	83 c4 04             	add    $0x4,%esp
80100623:	0f b6 c0             	movzbl %al,%eax
80100626:	c1 e0 08             	shl    $0x8,%eax
80100629:	89 45 f4             	mov    %eax,-0xc(%ebp)
  outb(CRTPORT, 15);
8010062c:	6a 0f                	push   $0xf
8010062e:	68 d4 03 00 00       	push   $0x3d4
80100633:	e8 b9 fc ff ff       	call   801002f1 <outb>
80100638:	83 c4 08             	add    $0x8,%esp
  pos |= inb(CRTPORT+1);
8010063b:	68 d5 03 00 00       	push   $0x3d5
80100640:	e8 8f fc ff ff       	call   801002d4 <inb>
80100645:	83 c4 04             	add    $0x4,%esp
80100648:	0f b6 c0             	movzbl %al,%eax
8010064b:	09 45 f4             	or     %eax,-0xc(%ebp)

  if(c == '\n')
8010064e:	83 7d 08 0a          	cmpl   $0xa,0x8(%ebp)
80100652:	75 30                	jne    80100684 <cgaputc+0x83>
    pos += 80 - pos%80;
80100654:	8b 4d f4             	mov    -0xc(%ebp),%ecx
80100657:	ba 67 66 66 66       	mov    $0x66666667,%edx
8010065c:	89 c8                	mov    %ecx,%eax
8010065e:	f7 ea                	imul   %edx
80100660:	c1 fa 05             	sar    $0x5,%edx
80100663:	89 c8                	mov    %ecx,%eax
80100665:	c1 f8 1f             	sar    $0x1f,%eax
80100668:	29 c2                	sub    %eax,%edx
8010066a:	89 d0                	mov    %edx,%eax
8010066c:	c1 e0 02             	shl    $0x2,%eax
8010066f:	01 d0                	add    %edx,%eax
80100671:	c1 e0 04             	shl    $0x4,%eax
80100674:	29 c1                	sub    %eax,%ecx
80100676:	89 ca                	mov    %ecx,%edx
80100678:	b8 50 00 00 00       	mov    $0x50,%eax
8010067d:	29 d0                	sub    %edx,%eax
8010067f:	01 45 f4             	add    %eax,-0xc(%ebp)
80100682:	eb 34                	jmp    801006b8 <cgaputc+0xb7>
  else if(c == BACKSPACE){
80100684:	81 7d 08 00 01 00 00 	cmpl   $0x100,0x8(%ebp)
8010068b:	75 0c                	jne    80100699 <cgaputc+0x98>
    if(pos > 0) --pos;
8010068d:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80100691:	7e 25                	jle    801006b8 <cgaputc+0xb7>
80100693:	83 6d f4 01          	subl   $0x1,-0xc(%ebp)
80100697:	eb 1f                	jmp    801006b8 <cgaputc+0xb7>
  } else
    crt[pos++] = (c&0xff) | 0x0700;  // black on white
80100699:	8b 0d 00 a0 10 80    	mov    0x8010a000,%ecx
8010069f:	8b 45 f4             	mov    -0xc(%ebp),%eax
801006a2:	8d 50 01             	lea    0x1(%eax),%edx
801006a5:	89 55 f4             	mov    %edx,-0xc(%ebp)
801006a8:	01 c0                	add    %eax,%eax
801006aa:	01 c8                	add    %ecx,%eax
801006ac:	8b 55 08             	mov    0x8(%ebp),%edx
801006af:	0f b6 d2             	movzbl %dl,%edx
801006b2:	80 ce 07             	or     $0x7,%dh
801006b5:	66 89 10             	mov    %dx,(%eax)

  if(pos < 0 || pos > 25*80)
801006b8:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801006bc:	78 09                	js     801006c7 <cgaputc+0xc6>
801006be:	81 7d f4 d0 07 00 00 	cmpl   $0x7d0,-0xc(%ebp)
801006c5:	7e 0d                	jle    801006d4 <cgaputc+0xd3>
    panic("pos under/overflow");
801006c7:	83 ec 0c             	sub    $0xc,%esp
801006ca:	68 4f 92 10 80       	push   $0x8010924f
801006cf:	e8 92 fe ff ff       	call   80100566 <panic>
  
  if((pos/80) >= 24){  // Scroll up.
801006d4:	81 7d f4 7f 07 00 00 	cmpl   $0x77f,-0xc(%ebp)
801006db:	7e 4c                	jle    80100729 <cgaputc+0x128>
    memmove(crt, crt+80, sizeof(crt[0])*23*80);
801006dd:	a1 00 a0 10 80       	mov    0x8010a000,%eax
801006e2:	8d 90 a0 00 00 00    	lea    0xa0(%eax),%edx
801006e8:	a1 00 a0 10 80       	mov    0x8010a000,%eax
801006ed:	83 ec 04             	sub    $0x4,%esp
801006f0:	68 60 0e 00 00       	push   $0xe60
801006f5:	52                   	push   %edx
801006f6:	50                   	push   %eax
801006f7:	e8 6e 57 00 00       	call   80105e6a <memmove>
801006fc:	83 c4 10             	add    $0x10,%esp
    pos -= 80;
801006ff:	83 6d f4 50          	subl   $0x50,-0xc(%ebp)
    memset(crt+pos, 0, sizeof(crt[0])*(24*80 - pos));
80100703:	b8 80 07 00 00       	mov    $0x780,%eax
80100708:	2b 45 f4             	sub    -0xc(%ebp),%eax
8010070b:	8d 14 00             	lea    (%eax,%eax,1),%edx
8010070e:	a1 00 a0 10 80       	mov    0x8010a000,%eax
80100713:	8b 4d f4             	mov    -0xc(%ebp),%ecx
80100716:	01 c9                	add    %ecx,%ecx
80100718:	01 c8                	add    %ecx,%eax
8010071a:	83 ec 04             	sub    $0x4,%esp
8010071d:	52                   	push   %edx
8010071e:	6a 00                	push   $0x0
80100720:	50                   	push   %eax
80100721:	e8 85 56 00 00       	call   80105dab <memset>
80100726:	83 c4 10             	add    $0x10,%esp
  }
  
  outb(CRTPORT, 14);
80100729:	83 ec 08             	sub    $0x8,%esp
8010072c:	6a 0e                	push   $0xe
8010072e:	68 d4 03 00 00       	push   $0x3d4
80100733:	e8 b9 fb ff ff       	call   801002f1 <outb>
80100738:	83 c4 10             	add    $0x10,%esp
  outb(CRTPORT+1, pos>>8);
8010073b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010073e:	c1 f8 08             	sar    $0x8,%eax
80100741:	0f b6 c0             	movzbl %al,%eax
80100744:	83 ec 08             	sub    $0x8,%esp
80100747:	50                   	push   %eax
80100748:	68 d5 03 00 00       	push   $0x3d5
8010074d:	e8 9f fb ff ff       	call   801002f1 <outb>
80100752:	83 c4 10             	add    $0x10,%esp
  outb(CRTPORT, 15);
80100755:	83 ec 08             	sub    $0x8,%esp
80100758:	6a 0f                	push   $0xf
8010075a:	68 d4 03 00 00       	push   $0x3d4
8010075f:	e8 8d fb ff ff       	call   801002f1 <outb>
80100764:	83 c4 10             	add    $0x10,%esp
  outb(CRTPORT+1, pos);
80100767:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010076a:	0f b6 c0             	movzbl %al,%eax
8010076d:	83 ec 08             	sub    $0x8,%esp
80100770:	50                   	push   %eax
80100771:	68 d5 03 00 00       	push   $0x3d5
80100776:	e8 76 fb ff ff       	call   801002f1 <outb>
8010077b:	83 c4 10             	add    $0x10,%esp
  crt[pos] = ' ' | 0x0700;
8010077e:	a1 00 a0 10 80       	mov    0x8010a000,%eax
80100783:	8b 55 f4             	mov    -0xc(%ebp),%edx
80100786:	01 d2                	add    %edx,%edx
80100788:	01 d0                	add    %edx,%eax
8010078a:	66 c7 00 20 07       	movw   $0x720,(%eax)
}
8010078f:	90                   	nop
80100790:	c9                   	leave  
80100791:	c3                   	ret    

80100792 <consputc>:

void
consputc(int c)
{
80100792:	55                   	push   %ebp
80100793:	89 e5                	mov    %esp,%ebp
80100795:	83 ec 08             	sub    $0x8,%esp
  if(panicked){
80100798:	a1 c0 c5 10 80       	mov    0x8010c5c0,%eax
8010079d:	85 c0                	test   %eax,%eax
8010079f:	74 07                	je     801007a8 <consputc+0x16>
    cli();
801007a1:	e8 6a fb ff ff       	call   80100310 <cli>
    for(;;)
      ;
801007a6:	eb fe                	jmp    801007a6 <consputc+0x14>
  }

  if(c == BACKSPACE){
801007a8:	81 7d 08 00 01 00 00 	cmpl   $0x100,0x8(%ebp)
801007af:	75 29                	jne    801007da <consputc+0x48>
    uartputc('\b'); uartputc(' '); uartputc('\b');
801007b1:	83 ec 0c             	sub    $0xc,%esp
801007b4:	6a 08                	push   $0x8
801007b6:	e8 d2 70 00 00       	call   8010788d <uartputc>
801007bb:	83 c4 10             	add    $0x10,%esp
801007be:	83 ec 0c             	sub    $0xc,%esp
801007c1:	6a 20                	push   $0x20
801007c3:	e8 c5 70 00 00       	call   8010788d <uartputc>
801007c8:	83 c4 10             	add    $0x10,%esp
801007cb:	83 ec 0c             	sub    $0xc,%esp
801007ce:	6a 08                	push   $0x8
801007d0:	e8 b8 70 00 00       	call   8010788d <uartputc>
801007d5:	83 c4 10             	add    $0x10,%esp
801007d8:	eb 0e                	jmp    801007e8 <consputc+0x56>
  } else
    uartputc(c);
801007da:	83 ec 0c             	sub    $0xc,%esp
801007dd:	ff 75 08             	pushl  0x8(%ebp)
801007e0:	e8 a8 70 00 00       	call   8010788d <uartputc>
801007e5:	83 c4 10             	add    $0x10,%esp
  cgaputc(c);
801007e8:	83 ec 0c             	sub    $0xc,%esp
801007eb:	ff 75 08             	pushl  0x8(%ebp)
801007ee:	e8 0e fe ff ff       	call   80100601 <cgaputc>
801007f3:	83 c4 10             	add    $0x10,%esp
}
801007f6:	90                   	nop
801007f7:	c9                   	leave  
801007f8:	c3                   	ret    

801007f9 <consoleintr>:

#define C(x)  ((x)-'@')  // Control-x

void
consoleintr(int (*getc)(void))
{
801007f9:	55                   	push   %ebp
801007fa:	89 e5                	mov    %esp,%ebp
801007fc:	83 ec 28             	sub    $0x28,%esp
  int c, doprocdump = 0;
801007ff:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  int doprintready = 0;
80100806:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  int doprintfree = 0;
8010080d:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  int doprintzombie = 0;
80100814:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)
  int doprintsleep = 0;
8010081b:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)


  acquire(&cons.lock);
80100822:	83 ec 0c             	sub    $0xc,%esp
80100825:	68 e0 c5 10 80       	push   $0x8010c5e0
8010082a:	e8 19 53 00 00       	call   80105b48 <acquire>
8010082f:	83 c4 10             	add    $0x10,%esp
  while((c = getc()) >= 0){
80100832:	e9 9a 01 00 00       	jmp    801009d1 <consoleintr+0x1d8>
    switch(c){
80100837:	8b 45 e0             	mov    -0x20(%ebp),%eax
8010083a:	83 f8 12             	cmp    $0x12,%eax
8010083d:	74 44                	je     80100883 <consoleintr+0x8a>
8010083f:	83 f8 12             	cmp    $0x12,%eax
80100842:	7f 18                	jg     8010085c <consoleintr+0x63>
80100844:	83 f8 08             	cmp    $0x8,%eax
80100847:	0f 84 bd 00 00 00    	je     8010090a <consoleintr+0x111>
8010084d:	83 f8 10             	cmp    $0x10,%eax
80100850:	74 61                	je     801008b3 <consoleintr+0xba>
80100852:	83 f8 06             	cmp    $0x6,%eax
80100855:	74 38                	je     8010088f <consoleintr+0x96>
80100857:	e9 e3 00 00 00       	jmp    8010093f <consoleintr+0x146>
8010085c:	83 f8 15             	cmp    $0x15,%eax
8010085f:	74 7b                	je     801008dc <consoleintr+0xe3>
80100861:	83 f8 15             	cmp    $0x15,%eax
80100864:	7f 0a                	jg     80100870 <consoleintr+0x77>
80100866:	83 f8 13             	cmp    $0x13,%eax
80100869:	74 30                	je     8010089b <consoleintr+0xa2>
8010086b:	e9 cf 00 00 00       	jmp    8010093f <consoleintr+0x146>
80100870:	83 f8 1a             	cmp    $0x1a,%eax
80100873:	74 32                	je     801008a7 <consoleintr+0xae>
80100875:	83 f8 7f             	cmp    $0x7f,%eax
80100878:	0f 84 8c 00 00 00    	je     8010090a <consoleintr+0x111>
8010087e:	e9 bc 00 00 00       	jmp    8010093f <consoleintr+0x146>
    case C('R'):
      doprintready = 1;
80100883:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
      break;
8010088a:	e9 42 01 00 00       	jmp    801009d1 <consoleintr+0x1d8>
    case C('F'):
      doprintfree = 1;
8010088f:	c7 45 ec 01 00 00 00 	movl   $0x1,-0x14(%ebp)
      break;
80100896:	e9 36 01 00 00       	jmp    801009d1 <consoleintr+0x1d8>
    case C('S'):
      doprintsleep = 1;
8010089b:	c7 45 e4 01 00 00 00 	movl   $0x1,-0x1c(%ebp)
      break;
801008a2:	e9 2a 01 00 00       	jmp    801009d1 <consoleintr+0x1d8>
    case C('Z'):
      doprintzombie = 1;
801008a7:	c7 45 e8 01 00 00 00 	movl   $0x1,-0x18(%ebp)
      break;
801008ae:	e9 1e 01 00 00       	jmp    801009d1 <consoleintr+0x1d8>
    case C('P'):  // Process listing.
      doprocdump = 1;   // procdump() locks cons.lock indirectly; invoke later
801008b3:	c7 45 f4 01 00 00 00 	movl   $0x1,-0xc(%ebp)
      break;
801008ba:	e9 12 01 00 00       	jmp    801009d1 <consoleintr+0x1d8>
    case C('U'):  // Kill line.
      while(input.e != input.w &&
            input.buf[(input.e-1) % INPUT_BUF] != '\n'){
        input.e--;
801008bf:	a1 28 18 11 80       	mov    0x80111828,%eax
801008c4:	83 e8 01             	sub    $0x1,%eax
801008c7:	a3 28 18 11 80       	mov    %eax,0x80111828
        consputc(BACKSPACE);
801008cc:	83 ec 0c             	sub    $0xc,%esp
801008cf:	68 00 01 00 00       	push   $0x100
801008d4:	e8 b9 fe ff ff       	call   80100792 <consputc>
801008d9:	83 c4 10             	add    $0x10,%esp
      break;
    case C('P'):  // Process listing.
      doprocdump = 1;   // procdump() locks cons.lock indirectly; invoke later
      break;
    case C('U'):  // Kill line.
      while(input.e != input.w &&
801008dc:	8b 15 28 18 11 80    	mov    0x80111828,%edx
801008e2:	a1 24 18 11 80       	mov    0x80111824,%eax
801008e7:	39 c2                	cmp    %eax,%edx
801008e9:	0f 84 e2 00 00 00    	je     801009d1 <consoleintr+0x1d8>
            input.buf[(input.e-1) % INPUT_BUF] != '\n'){
801008ef:	a1 28 18 11 80       	mov    0x80111828,%eax
801008f4:	83 e8 01             	sub    $0x1,%eax
801008f7:	83 e0 7f             	and    $0x7f,%eax
801008fa:	0f b6 80 a0 17 11 80 	movzbl -0x7feee860(%eax),%eax
      break;
    case C('P'):  // Process listing.
      doprocdump = 1;   // procdump() locks cons.lock indirectly; invoke later
      break;
    case C('U'):  // Kill line.
      while(input.e != input.w &&
80100901:	3c 0a                	cmp    $0xa,%al
80100903:	75 ba                	jne    801008bf <consoleintr+0xc6>
            input.buf[(input.e-1) % INPUT_BUF] != '\n'){
        input.e--;
        consputc(BACKSPACE);
      }
      break;
80100905:	e9 c7 00 00 00       	jmp    801009d1 <consoleintr+0x1d8>
    case C('H'): case '\x7f':  // Backspace
      if(input.e != input.w){
8010090a:	8b 15 28 18 11 80    	mov    0x80111828,%edx
80100910:	a1 24 18 11 80       	mov    0x80111824,%eax
80100915:	39 c2                	cmp    %eax,%edx
80100917:	0f 84 b4 00 00 00    	je     801009d1 <consoleintr+0x1d8>
        input.e--;
8010091d:	a1 28 18 11 80       	mov    0x80111828,%eax
80100922:	83 e8 01             	sub    $0x1,%eax
80100925:	a3 28 18 11 80       	mov    %eax,0x80111828
        consputc(BACKSPACE);
8010092a:	83 ec 0c             	sub    $0xc,%esp
8010092d:	68 00 01 00 00       	push   $0x100
80100932:	e8 5b fe ff ff       	call   80100792 <consputc>
80100937:	83 c4 10             	add    $0x10,%esp
      }
      break;
8010093a:	e9 92 00 00 00       	jmp    801009d1 <consoleintr+0x1d8>
    default:
      if(c != 0 && input.e-input.r < INPUT_BUF){
8010093f:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
80100943:	0f 84 87 00 00 00    	je     801009d0 <consoleintr+0x1d7>
80100949:	8b 15 28 18 11 80    	mov    0x80111828,%edx
8010094f:	a1 20 18 11 80       	mov    0x80111820,%eax
80100954:	29 c2                	sub    %eax,%edx
80100956:	89 d0                	mov    %edx,%eax
80100958:	83 f8 7f             	cmp    $0x7f,%eax
8010095b:	77 73                	ja     801009d0 <consoleintr+0x1d7>
        c = (c == '\r') ? '\n' : c;
8010095d:	83 7d e0 0d          	cmpl   $0xd,-0x20(%ebp)
80100961:	74 05                	je     80100968 <consoleintr+0x16f>
80100963:	8b 45 e0             	mov    -0x20(%ebp),%eax
80100966:	eb 05                	jmp    8010096d <consoleintr+0x174>
80100968:	b8 0a 00 00 00       	mov    $0xa,%eax
8010096d:	89 45 e0             	mov    %eax,-0x20(%ebp)
        input.buf[input.e++ % INPUT_BUF] = c;
80100970:	a1 28 18 11 80       	mov    0x80111828,%eax
80100975:	8d 50 01             	lea    0x1(%eax),%edx
80100978:	89 15 28 18 11 80    	mov    %edx,0x80111828
8010097e:	83 e0 7f             	and    $0x7f,%eax
80100981:	8b 55 e0             	mov    -0x20(%ebp),%edx
80100984:	88 90 a0 17 11 80    	mov    %dl,-0x7feee860(%eax)
        consputc(c);
8010098a:	83 ec 0c             	sub    $0xc,%esp
8010098d:	ff 75 e0             	pushl  -0x20(%ebp)
80100990:	e8 fd fd ff ff       	call   80100792 <consputc>
80100995:	83 c4 10             	add    $0x10,%esp
        if(c == '\n' || c == C('D') || input.e == input.r+INPUT_BUF){
80100998:	83 7d e0 0a          	cmpl   $0xa,-0x20(%ebp)
8010099c:	74 18                	je     801009b6 <consoleintr+0x1bd>
8010099e:	83 7d e0 04          	cmpl   $0x4,-0x20(%ebp)
801009a2:	74 12                	je     801009b6 <consoleintr+0x1bd>
801009a4:	a1 28 18 11 80       	mov    0x80111828,%eax
801009a9:	8b 15 20 18 11 80    	mov    0x80111820,%edx
801009af:	83 ea 80             	sub    $0xffffff80,%edx
801009b2:	39 d0                	cmp    %edx,%eax
801009b4:	75 1a                	jne    801009d0 <consoleintr+0x1d7>
          input.w = input.e;
801009b6:	a1 28 18 11 80       	mov    0x80111828,%eax
801009bb:	a3 24 18 11 80       	mov    %eax,0x80111824
          wakeup(&input.r);
801009c0:	83 ec 0c             	sub    $0xc,%esp
801009c3:	68 20 18 11 80       	push   $0x80111820
801009c8:	e8 ac 4a 00 00       	call   80105479 <wakeup>
801009cd:	83 c4 10             	add    $0x10,%esp
        }
      }
      break;
801009d0:	90                   	nop
  int doprintzombie = 0;
  int doprintsleep = 0;


  acquire(&cons.lock);
  while((c = getc()) >= 0){
801009d1:	8b 45 08             	mov    0x8(%ebp),%eax
801009d4:	ff d0                	call   *%eax
801009d6:	89 45 e0             	mov    %eax,-0x20(%ebp)
801009d9:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
801009dd:	0f 89 54 fe ff ff    	jns    80100837 <consoleintr+0x3e>
        }
      }
      break;
    }
  }
  release(&cons.lock);
801009e3:	83 ec 0c             	sub    $0xc,%esp
801009e6:	68 e0 c5 10 80       	push   $0x8010c5e0
801009eb:	e8 bf 51 00 00       	call   80105baf <release>
801009f0:	83 c4 10             	add    $0x10,%esp
  if(doprocdump) {
801009f3:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801009f7:	74 05                	je     801009fe <consoleintr+0x205>
    procdump();  // now call procdump() wo. cons.lock held
801009f9:	e8 4d 4b 00 00       	call   8010554b <procdump>
  }
  if(doprintfree) {
801009fe:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
80100a02:	74 05                	je     80100a09 <consoleintr+0x210>
    printfree();  
80100a04:	e8 4e 4d 00 00       	call   80105757 <printfree>
  }
  if(doprintready) {
80100a09:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80100a0d:	74 05                	je     80100a14 <consoleintr+0x21b>
    printready();  
80100a0f:	e8 14 4d 00 00       	call   80105728 <printready>
  }
  if(doprintsleep) {
80100a14:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
80100a18:	74 05                	je     80100a1f <consoleintr+0x226>
    printsleep(); 
80100a1a:	e8 65 4d 00 00       	call   80105784 <printsleep>
  }
  if(doprintzombie) {
80100a1f:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
80100a23:	74 05                	je     80100a2a <consoleintr+0x231>
    printzombie();  
80100a25:	e8 89 4d 00 00       	call   801057b3 <printzombie>
  }
}
80100a2a:	90                   	nop
80100a2b:	c9                   	leave  
80100a2c:	c3                   	ret    

80100a2d <consoleread>:

int
consoleread(struct inode *ip, char *dst, int n)
{
80100a2d:	55                   	push   %ebp
80100a2e:	89 e5                	mov    %esp,%ebp
80100a30:	83 ec 18             	sub    $0x18,%esp
  uint target;
  int c;

  iunlock(ip);
80100a33:	83 ec 0c             	sub    $0xc,%esp
80100a36:	ff 75 08             	pushl  0x8(%ebp)
80100a39:	e8 28 11 00 00       	call   80101b66 <iunlock>
80100a3e:	83 c4 10             	add    $0x10,%esp
  target = n;
80100a41:	8b 45 10             	mov    0x10(%ebp),%eax
80100a44:	89 45 f4             	mov    %eax,-0xc(%ebp)
  acquire(&cons.lock);
80100a47:	83 ec 0c             	sub    $0xc,%esp
80100a4a:	68 e0 c5 10 80       	push   $0x8010c5e0
80100a4f:	e8 f4 50 00 00       	call   80105b48 <acquire>
80100a54:	83 c4 10             	add    $0x10,%esp
  while(n > 0){
80100a57:	e9 ac 00 00 00       	jmp    80100b08 <consoleread+0xdb>
    while(input.r == input.w){
      if(proc->killed){
80100a5c:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80100a62:	8b 40 24             	mov    0x24(%eax),%eax
80100a65:	85 c0                	test   %eax,%eax
80100a67:	74 28                	je     80100a91 <consoleread+0x64>
        release(&cons.lock);
80100a69:	83 ec 0c             	sub    $0xc,%esp
80100a6c:	68 e0 c5 10 80       	push   $0x8010c5e0
80100a71:	e8 39 51 00 00       	call   80105baf <release>
80100a76:	83 c4 10             	add    $0x10,%esp
        ilock(ip);
80100a79:	83 ec 0c             	sub    $0xc,%esp
80100a7c:	ff 75 08             	pushl  0x8(%ebp)
80100a7f:	e8 84 0f 00 00       	call   80101a08 <ilock>
80100a84:	83 c4 10             	add    $0x10,%esp
        return -1;
80100a87:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80100a8c:	e9 ab 00 00 00       	jmp    80100b3c <consoleread+0x10f>
      }
      sleep(&input.r, &cons.lock);
80100a91:	83 ec 08             	sub    $0x8,%esp
80100a94:	68 e0 c5 10 80       	push   $0x8010c5e0
80100a99:	68 20 18 11 80       	push   $0x80111820
80100a9e:	e8 c1 48 00 00       	call   80105364 <sleep>
80100aa3:	83 c4 10             	add    $0x10,%esp

  iunlock(ip);
  target = n;
  acquire(&cons.lock);
  while(n > 0){
    while(input.r == input.w){
80100aa6:	8b 15 20 18 11 80    	mov    0x80111820,%edx
80100aac:	a1 24 18 11 80       	mov    0x80111824,%eax
80100ab1:	39 c2                	cmp    %eax,%edx
80100ab3:	74 a7                	je     80100a5c <consoleread+0x2f>
        ilock(ip);
        return -1;
      }
      sleep(&input.r, &cons.lock);
    }
    c = input.buf[input.r++ % INPUT_BUF];
80100ab5:	a1 20 18 11 80       	mov    0x80111820,%eax
80100aba:	8d 50 01             	lea    0x1(%eax),%edx
80100abd:	89 15 20 18 11 80    	mov    %edx,0x80111820
80100ac3:	83 e0 7f             	and    $0x7f,%eax
80100ac6:	0f b6 80 a0 17 11 80 	movzbl -0x7feee860(%eax),%eax
80100acd:	0f be c0             	movsbl %al,%eax
80100ad0:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if(c == C('D')){  // EOF
80100ad3:	83 7d f0 04          	cmpl   $0x4,-0x10(%ebp)
80100ad7:	75 17                	jne    80100af0 <consoleread+0xc3>
      if(n < target){
80100ad9:	8b 45 10             	mov    0x10(%ebp),%eax
80100adc:	3b 45 f4             	cmp    -0xc(%ebp),%eax
80100adf:	73 2f                	jae    80100b10 <consoleread+0xe3>
        // Save ^D for next time, to make sure
        // caller gets a 0-byte result.
        input.r--;
80100ae1:	a1 20 18 11 80       	mov    0x80111820,%eax
80100ae6:	83 e8 01             	sub    $0x1,%eax
80100ae9:	a3 20 18 11 80       	mov    %eax,0x80111820
      }
      break;
80100aee:	eb 20                	jmp    80100b10 <consoleread+0xe3>
    }
    *dst++ = c;
80100af0:	8b 45 0c             	mov    0xc(%ebp),%eax
80100af3:	8d 50 01             	lea    0x1(%eax),%edx
80100af6:	89 55 0c             	mov    %edx,0xc(%ebp)
80100af9:	8b 55 f0             	mov    -0x10(%ebp),%edx
80100afc:	88 10                	mov    %dl,(%eax)
    --n;
80100afe:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
    if(c == '\n')
80100b02:	83 7d f0 0a          	cmpl   $0xa,-0x10(%ebp)
80100b06:	74 0b                	je     80100b13 <consoleread+0xe6>
  int c;

  iunlock(ip);
  target = n;
  acquire(&cons.lock);
  while(n > 0){
80100b08:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80100b0c:	7f 98                	jg     80100aa6 <consoleread+0x79>
80100b0e:	eb 04                	jmp    80100b14 <consoleread+0xe7>
      if(n < target){
        // Save ^D for next time, to make sure
        // caller gets a 0-byte result.
        input.r--;
      }
      break;
80100b10:	90                   	nop
80100b11:	eb 01                	jmp    80100b14 <consoleread+0xe7>
    }
    *dst++ = c;
    --n;
    if(c == '\n')
      break;
80100b13:	90                   	nop
  }
  release(&cons.lock);
80100b14:	83 ec 0c             	sub    $0xc,%esp
80100b17:	68 e0 c5 10 80       	push   $0x8010c5e0
80100b1c:	e8 8e 50 00 00       	call   80105baf <release>
80100b21:	83 c4 10             	add    $0x10,%esp
  ilock(ip);
80100b24:	83 ec 0c             	sub    $0xc,%esp
80100b27:	ff 75 08             	pushl  0x8(%ebp)
80100b2a:	e8 d9 0e 00 00       	call   80101a08 <ilock>
80100b2f:	83 c4 10             	add    $0x10,%esp

  return target - n;
80100b32:	8b 45 10             	mov    0x10(%ebp),%eax
80100b35:	8b 55 f4             	mov    -0xc(%ebp),%edx
80100b38:	29 c2                	sub    %eax,%edx
80100b3a:	89 d0                	mov    %edx,%eax
}
80100b3c:	c9                   	leave  
80100b3d:	c3                   	ret    

80100b3e <consolewrite>:

int
consolewrite(struct inode *ip, char *buf, int n)
{
80100b3e:	55                   	push   %ebp
80100b3f:	89 e5                	mov    %esp,%ebp
80100b41:	83 ec 18             	sub    $0x18,%esp
  int i;

  iunlock(ip);
80100b44:	83 ec 0c             	sub    $0xc,%esp
80100b47:	ff 75 08             	pushl  0x8(%ebp)
80100b4a:	e8 17 10 00 00       	call   80101b66 <iunlock>
80100b4f:	83 c4 10             	add    $0x10,%esp
  acquire(&cons.lock);
80100b52:	83 ec 0c             	sub    $0xc,%esp
80100b55:	68 e0 c5 10 80       	push   $0x8010c5e0
80100b5a:	e8 e9 4f 00 00       	call   80105b48 <acquire>
80100b5f:	83 c4 10             	add    $0x10,%esp
  for(i = 0; i < n; i++)
80100b62:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80100b69:	eb 21                	jmp    80100b8c <consolewrite+0x4e>
    consputc(buf[i] & 0xff);
80100b6b:	8b 55 f4             	mov    -0xc(%ebp),%edx
80100b6e:	8b 45 0c             	mov    0xc(%ebp),%eax
80100b71:	01 d0                	add    %edx,%eax
80100b73:	0f b6 00             	movzbl (%eax),%eax
80100b76:	0f be c0             	movsbl %al,%eax
80100b79:	0f b6 c0             	movzbl %al,%eax
80100b7c:	83 ec 0c             	sub    $0xc,%esp
80100b7f:	50                   	push   %eax
80100b80:	e8 0d fc ff ff       	call   80100792 <consputc>
80100b85:	83 c4 10             	add    $0x10,%esp
{
  int i;

  iunlock(ip);
  acquire(&cons.lock);
  for(i = 0; i < n; i++)
80100b88:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80100b8c:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100b8f:	3b 45 10             	cmp    0x10(%ebp),%eax
80100b92:	7c d7                	jl     80100b6b <consolewrite+0x2d>
    consputc(buf[i] & 0xff);
  release(&cons.lock);
80100b94:	83 ec 0c             	sub    $0xc,%esp
80100b97:	68 e0 c5 10 80       	push   $0x8010c5e0
80100b9c:	e8 0e 50 00 00       	call   80105baf <release>
80100ba1:	83 c4 10             	add    $0x10,%esp
  ilock(ip);
80100ba4:	83 ec 0c             	sub    $0xc,%esp
80100ba7:	ff 75 08             	pushl  0x8(%ebp)
80100baa:	e8 59 0e 00 00       	call   80101a08 <ilock>
80100baf:	83 c4 10             	add    $0x10,%esp

  return n;
80100bb2:	8b 45 10             	mov    0x10(%ebp),%eax
}
80100bb5:	c9                   	leave  
80100bb6:	c3                   	ret    

80100bb7 <consoleinit>:

void
consoleinit(void)
{
80100bb7:	55                   	push   %ebp
80100bb8:	89 e5                	mov    %esp,%ebp
80100bba:	83 ec 08             	sub    $0x8,%esp
  initlock(&cons.lock, "console");
80100bbd:	83 ec 08             	sub    $0x8,%esp
80100bc0:	68 62 92 10 80       	push   $0x80109262
80100bc5:	68 e0 c5 10 80       	push   $0x8010c5e0
80100bca:	e8 57 4f 00 00       	call   80105b26 <initlock>
80100bcf:	83 c4 10             	add    $0x10,%esp

  devsw[CONSOLE].write = consolewrite;
80100bd2:	c7 05 ec 21 11 80 3e 	movl   $0x80100b3e,0x801121ec
80100bd9:	0b 10 80 
  devsw[CONSOLE].read = consoleread;
80100bdc:	c7 05 e8 21 11 80 2d 	movl   $0x80100a2d,0x801121e8
80100be3:	0a 10 80 
  cons.locking = 1;
80100be6:	c7 05 14 c6 10 80 01 	movl   $0x1,0x8010c614
80100bed:	00 00 00 

  picenable(IRQ_KBD);
80100bf0:	83 ec 0c             	sub    $0xc,%esp
80100bf3:	6a 01                	push   $0x1
80100bf5:	e8 cf 33 00 00       	call   80103fc9 <picenable>
80100bfa:	83 c4 10             	add    $0x10,%esp
  ioapicenable(IRQ_KBD, 0);
80100bfd:	83 ec 08             	sub    $0x8,%esp
80100c00:	6a 00                	push   $0x0
80100c02:	6a 01                	push   $0x1
80100c04:	e8 6f 1f 00 00       	call   80102b78 <ioapicenable>
80100c09:	83 c4 10             	add    $0x10,%esp
}
80100c0c:	90                   	nop
80100c0d:	c9                   	leave  
80100c0e:	c3                   	ret    

80100c0f <exec>:
#include "x86.h"
#include "elf.h"

int
exec(char *path, char **argv)
{
80100c0f:	55                   	push   %ebp
80100c10:	89 e5                	mov    %esp,%ebp
80100c12:	81 ec 18 01 00 00    	sub    $0x118,%esp
  struct elfhdr elf;
  struct inode *ip;
  struct proghdr ph;
  pde_t *pgdir, *oldpgdir;

  begin_op();
80100c18:	e8 ce 29 00 00       	call   801035eb <begin_op>
  if((ip = namei(path)) == 0){
80100c1d:	83 ec 0c             	sub    $0xc,%esp
80100c20:	ff 75 08             	pushl  0x8(%ebp)
80100c23:	e8 9e 19 00 00       	call   801025c6 <namei>
80100c28:	83 c4 10             	add    $0x10,%esp
80100c2b:	89 45 d8             	mov    %eax,-0x28(%ebp)
80100c2e:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
80100c32:	75 0f                	jne    80100c43 <exec+0x34>
    end_op();
80100c34:	e8 3e 2a 00 00       	call   80103677 <end_op>
    return -1;
80100c39:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80100c3e:	e9 ce 03 00 00       	jmp    80101011 <exec+0x402>
  }
  ilock(ip);
80100c43:	83 ec 0c             	sub    $0xc,%esp
80100c46:	ff 75 d8             	pushl  -0x28(%ebp)
80100c49:	e8 ba 0d 00 00       	call   80101a08 <ilock>
80100c4e:	83 c4 10             	add    $0x10,%esp
  pgdir = 0;
80100c51:	c7 45 d4 00 00 00 00 	movl   $0x0,-0x2c(%ebp)

  // Check ELF header
  if(readi(ip, (char*)&elf, 0, sizeof(elf)) < sizeof(elf))
80100c58:	6a 34                	push   $0x34
80100c5a:	6a 00                	push   $0x0
80100c5c:	8d 85 0c ff ff ff    	lea    -0xf4(%ebp),%eax
80100c62:	50                   	push   %eax
80100c63:	ff 75 d8             	pushl  -0x28(%ebp)
80100c66:	e8 0b 13 00 00       	call   80101f76 <readi>
80100c6b:	83 c4 10             	add    $0x10,%esp
80100c6e:	83 f8 33             	cmp    $0x33,%eax
80100c71:	0f 86 49 03 00 00    	jbe    80100fc0 <exec+0x3b1>
    goto bad;
  if(elf.magic != ELF_MAGIC)
80100c77:	8b 85 0c ff ff ff    	mov    -0xf4(%ebp),%eax
80100c7d:	3d 7f 45 4c 46       	cmp    $0x464c457f,%eax
80100c82:	0f 85 3b 03 00 00    	jne    80100fc3 <exec+0x3b4>
    goto bad;

  if((pgdir = setupkvm()) == 0)
80100c88:	e8 55 7d 00 00       	call   801089e2 <setupkvm>
80100c8d:	89 45 d4             	mov    %eax,-0x2c(%ebp)
80100c90:	83 7d d4 00          	cmpl   $0x0,-0x2c(%ebp)
80100c94:	0f 84 2c 03 00 00    	je     80100fc6 <exec+0x3b7>
    goto bad;

  // Load program into memory.
  sz = 0;
80100c9a:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
80100ca1:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
80100ca8:	8b 85 28 ff ff ff    	mov    -0xd8(%ebp),%eax
80100cae:	89 45 e8             	mov    %eax,-0x18(%ebp)
80100cb1:	e9 ab 00 00 00       	jmp    80100d61 <exec+0x152>
    if(readi(ip, (char*)&ph, off, sizeof(ph)) != sizeof(ph))
80100cb6:	8b 45 e8             	mov    -0x18(%ebp),%eax
80100cb9:	6a 20                	push   $0x20
80100cbb:	50                   	push   %eax
80100cbc:	8d 85 ec fe ff ff    	lea    -0x114(%ebp),%eax
80100cc2:	50                   	push   %eax
80100cc3:	ff 75 d8             	pushl  -0x28(%ebp)
80100cc6:	e8 ab 12 00 00       	call   80101f76 <readi>
80100ccb:	83 c4 10             	add    $0x10,%esp
80100cce:	83 f8 20             	cmp    $0x20,%eax
80100cd1:	0f 85 f2 02 00 00    	jne    80100fc9 <exec+0x3ba>
      goto bad;
    if(ph.type != ELF_PROG_LOAD)
80100cd7:	8b 85 ec fe ff ff    	mov    -0x114(%ebp),%eax
80100cdd:	83 f8 01             	cmp    $0x1,%eax
80100ce0:	75 71                	jne    80100d53 <exec+0x144>
      continue;
    if(ph.memsz < ph.filesz)
80100ce2:	8b 95 00 ff ff ff    	mov    -0x100(%ebp),%edx
80100ce8:	8b 85 fc fe ff ff    	mov    -0x104(%ebp),%eax
80100cee:	39 c2                	cmp    %eax,%edx
80100cf0:	0f 82 d6 02 00 00    	jb     80100fcc <exec+0x3bd>
      goto bad;
    if((sz = allocuvm(pgdir, sz, ph.vaddr + ph.memsz)) == 0)
80100cf6:	8b 95 f4 fe ff ff    	mov    -0x10c(%ebp),%edx
80100cfc:	8b 85 00 ff ff ff    	mov    -0x100(%ebp),%eax
80100d02:	01 d0                	add    %edx,%eax
80100d04:	83 ec 04             	sub    $0x4,%esp
80100d07:	50                   	push   %eax
80100d08:	ff 75 e0             	pushl  -0x20(%ebp)
80100d0b:	ff 75 d4             	pushl  -0x2c(%ebp)
80100d0e:	e8 76 80 00 00       	call   80108d89 <allocuvm>
80100d13:	83 c4 10             	add    $0x10,%esp
80100d16:	89 45 e0             	mov    %eax,-0x20(%ebp)
80100d19:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
80100d1d:	0f 84 ac 02 00 00    	je     80100fcf <exec+0x3c0>
      goto bad;
    if(loaduvm(pgdir, (char*)ph.vaddr, ip, ph.off, ph.filesz) < 0)
80100d23:	8b 95 fc fe ff ff    	mov    -0x104(%ebp),%edx
80100d29:	8b 85 f0 fe ff ff    	mov    -0x110(%ebp),%eax
80100d2f:	8b 8d f4 fe ff ff    	mov    -0x10c(%ebp),%ecx
80100d35:	83 ec 0c             	sub    $0xc,%esp
80100d38:	52                   	push   %edx
80100d39:	50                   	push   %eax
80100d3a:	ff 75 d8             	pushl  -0x28(%ebp)
80100d3d:	51                   	push   %ecx
80100d3e:	ff 75 d4             	pushl  -0x2c(%ebp)
80100d41:	e8 6c 7f 00 00       	call   80108cb2 <loaduvm>
80100d46:	83 c4 20             	add    $0x20,%esp
80100d49:	85 c0                	test   %eax,%eax
80100d4b:	0f 88 81 02 00 00    	js     80100fd2 <exec+0x3c3>
80100d51:	eb 01                	jmp    80100d54 <exec+0x145>
  sz = 0;
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
    if(readi(ip, (char*)&ph, off, sizeof(ph)) != sizeof(ph))
      goto bad;
    if(ph.type != ELF_PROG_LOAD)
      continue;
80100d53:	90                   	nop
  if((pgdir = setupkvm()) == 0)
    goto bad;

  // Load program into memory.
  sz = 0;
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
80100d54:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
80100d58:	8b 45 e8             	mov    -0x18(%ebp),%eax
80100d5b:	83 c0 20             	add    $0x20,%eax
80100d5e:	89 45 e8             	mov    %eax,-0x18(%ebp)
80100d61:	0f b7 85 38 ff ff ff 	movzwl -0xc8(%ebp),%eax
80100d68:	0f b7 c0             	movzwl %ax,%eax
80100d6b:	3b 45 ec             	cmp    -0x14(%ebp),%eax
80100d6e:	0f 8f 42 ff ff ff    	jg     80100cb6 <exec+0xa7>
    if((sz = allocuvm(pgdir, sz, ph.vaddr + ph.memsz)) == 0)
      goto bad;
    if(loaduvm(pgdir, (char*)ph.vaddr, ip, ph.off, ph.filesz) < 0)
      goto bad;
  }
  iunlockput(ip);
80100d74:	83 ec 0c             	sub    $0xc,%esp
80100d77:	ff 75 d8             	pushl  -0x28(%ebp)
80100d7a:	e8 49 0f 00 00       	call   80101cc8 <iunlockput>
80100d7f:	83 c4 10             	add    $0x10,%esp
  end_op();
80100d82:	e8 f0 28 00 00       	call   80103677 <end_op>
  ip = 0;
80100d87:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)

  // Allocate two pages at the next page boundary.
  // Make the first inaccessible.  Use the second as the user stack.
  sz = PGROUNDUP(sz);
80100d8e:	8b 45 e0             	mov    -0x20(%ebp),%eax
80100d91:	05 ff 0f 00 00       	add    $0xfff,%eax
80100d96:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80100d9b:	89 45 e0             	mov    %eax,-0x20(%ebp)
  if((sz = allocuvm(pgdir, sz, sz + 2*PGSIZE)) == 0)
80100d9e:	8b 45 e0             	mov    -0x20(%ebp),%eax
80100da1:	05 00 20 00 00       	add    $0x2000,%eax
80100da6:	83 ec 04             	sub    $0x4,%esp
80100da9:	50                   	push   %eax
80100daa:	ff 75 e0             	pushl  -0x20(%ebp)
80100dad:	ff 75 d4             	pushl  -0x2c(%ebp)
80100db0:	e8 d4 7f 00 00       	call   80108d89 <allocuvm>
80100db5:	83 c4 10             	add    $0x10,%esp
80100db8:	89 45 e0             	mov    %eax,-0x20(%ebp)
80100dbb:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
80100dbf:	0f 84 10 02 00 00    	je     80100fd5 <exec+0x3c6>
    goto bad;
  clearpteu(pgdir, (char*)(sz - 2*PGSIZE));
80100dc5:	8b 45 e0             	mov    -0x20(%ebp),%eax
80100dc8:	2d 00 20 00 00       	sub    $0x2000,%eax
80100dcd:	83 ec 08             	sub    $0x8,%esp
80100dd0:	50                   	push   %eax
80100dd1:	ff 75 d4             	pushl  -0x2c(%ebp)
80100dd4:	e8 d6 81 00 00       	call   80108faf <clearpteu>
80100dd9:	83 c4 10             	add    $0x10,%esp
  sp = sz;
80100ddc:	8b 45 e0             	mov    -0x20(%ebp),%eax
80100ddf:	89 45 dc             	mov    %eax,-0x24(%ebp)

  // Push argument strings, prepare rest of stack in ustack.
  for(argc = 0; argv[argc]; argc++) {
80100de2:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
80100de9:	e9 96 00 00 00       	jmp    80100e84 <exec+0x275>
    if(argc >= MAXARG)
80100dee:	83 7d e4 1f          	cmpl   $0x1f,-0x1c(%ebp)
80100df2:	0f 87 e0 01 00 00    	ja     80100fd8 <exec+0x3c9>
      goto bad;
    sp = (sp - (strlen(argv[argc]) + 1)) & ~3;
80100df8:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100dfb:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
80100e02:	8b 45 0c             	mov    0xc(%ebp),%eax
80100e05:	01 d0                	add    %edx,%eax
80100e07:	8b 00                	mov    (%eax),%eax
80100e09:	83 ec 0c             	sub    $0xc,%esp
80100e0c:	50                   	push   %eax
80100e0d:	e8 e6 51 00 00       	call   80105ff8 <strlen>
80100e12:	83 c4 10             	add    $0x10,%esp
80100e15:	89 c2                	mov    %eax,%edx
80100e17:	8b 45 dc             	mov    -0x24(%ebp),%eax
80100e1a:	29 d0                	sub    %edx,%eax
80100e1c:	83 e8 01             	sub    $0x1,%eax
80100e1f:	83 e0 fc             	and    $0xfffffffc,%eax
80100e22:	89 45 dc             	mov    %eax,-0x24(%ebp)
    if(copyout(pgdir, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
80100e25:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100e28:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
80100e2f:	8b 45 0c             	mov    0xc(%ebp),%eax
80100e32:	01 d0                	add    %edx,%eax
80100e34:	8b 00                	mov    (%eax),%eax
80100e36:	83 ec 0c             	sub    $0xc,%esp
80100e39:	50                   	push   %eax
80100e3a:	e8 b9 51 00 00       	call   80105ff8 <strlen>
80100e3f:	83 c4 10             	add    $0x10,%esp
80100e42:	83 c0 01             	add    $0x1,%eax
80100e45:	89 c1                	mov    %eax,%ecx
80100e47:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100e4a:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
80100e51:	8b 45 0c             	mov    0xc(%ebp),%eax
80100e54:	01 d0                	add    %edx,%eax
80100e56:	8b 00                	mov    (%eax),%eax
80100e58:	51                   	push   %ecx
80100e59:	50                   	push   %eax
80100e5a:	ff 75 dc             	pushl  -0x24(%ebp)
80100e5d:	ff 75 d4             	pushl  -0x2c(%ebp)
80100e60:	e8 01 83 00 00       	call   80109166 <copyout>
80100e65:	83 c4 10             	add    $0x10,%esp
80100e68:	85 c0                	test   %eax,%eax
80100e6a:	0f 88 6b 01 00 00    	js     80100fdb <exec+0x3cc>
      goto bad;
    ustack[3+argc] = sp;
80100e70:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100e73:	8d 50 03             	lea    0x3(%eax),%edx
80100e76:	8b 45 dc             	mov    -0x24(%ebp),%eax
80100e79:	89 84 95 40 ff ff ff 	mov    %eax,-0xc0(%ebp,%edx,4)
    goto bad;
  clearpteu(pgdir, (char*)(sz - 2*PGSIZE));
  sp = sz;

  // Push argument strings, prepare rest of stack in ustack.
  for(argc = 0; argv[argc]; argc++) {
80100e80:	83 45 e4 01          	addl   $0x1,-0x1c(%ebp)
80100e84:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100e87:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
80100e8e:	8b 45 0c             	mov    0xc(%ebp),%eax
80100e91:	01 d0                	add    %edx,%eax
80100e93:	8b 00                	mov    (%eax),%eax
80100e95:	85 c0                	test   %eax,%eax
80100e97:	0f 85 51 ff ff ff    	jne    80100dee <exec+0x1df>
    sp = (sp - (strlen(argv[argc]) + 1)) & ~3;
    if(copyout(pgdir, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
      goto bad;
    ustack[3+argc] = sp;
  }
  ustack[3+argc] = 0;
80100e9d:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100ea0:	83 c0 03             	add    $0x3,%eax
80100ea3:	c7 84 85 40 ff ff ff 	movl   $0x0,-0xc0(%ebp,%eax,4)
80100eaa:	00 00 00 00 

  ustack[0] = 0xffffffff;  // fake return PC
80100eae:	c7 85 40 ff ff ff ff 	movl   $0xffffffff,-0xc0(%ebp)
80100eb5:	ff ff ff 
  ustack[1] = argc;
80100eb8:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100ebb:	89 85 44 ff ff ff    	mov    %eax,-0xbc(%ebp)
  ustack[2] = sp - (argc+1)*4;  // argv pointer
80100ec1:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100ec4:	83 c0 01             	add    $0x1,%eax
80100ec7:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
80100ece:	8b 45 dc             	mov    -0x24(%ebp),%eax
80100ed1:	29 d0                	sub    %edx,%eax
80100ed3:	89 85 48 ff ff ff    	mov    %eax,-0xb8(%ebp)

  sp -= (3+argc+1) * 4;
80100ed9:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100edc:	83 c0 04             	add    $0x4,%eax
80100edf:	c1 e0 02             	shl    $0x2,%eax
80100ee2:	29 45 dc             	sub    %eax,-0x24(%ebp)
  if(copyout(pgdir, sp, ustack, (3+argc+1)*4) < 0)
80100ee5:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100ee8:	83 c0 04             	add    $0x4,%eax
80100eeb:	c1 e0 02             	shl    $0x2,%eax
80100eee:	50                   	push   %eax
80100eef:	8d 85 40 ff ff ff    	lea    -0xc0(%ebp),%eax
80100ef5:	50                   	push   %eax
80100ef6:	ff 75 dc             	pushl  -0x24(%ebp)
80100ef9:	ff 75 d4             	pushl  -0x2c(%ebp)
80100efc:	e8 65 82 00 00       	call   80109166 <copyout>
80100f01:	83 c4 10             	add    $0x10,%esp
80100f04:	85 c0                	test   %eax,%eax
80100f06:	0f 88 d2 00 00 00    	js     80100fde <exec+0x3cf>
    goto bad;

  // Save program name for debugging.
  for(last=s=path; *s; s++)
80100f0c:	8b 45 08             	mov    0x8(%ebp),%eax
80100f0f:	89 45 f4             	mov    %eax,-0xc(%ebp)
80100f12:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100f15:	89 45 f0             	mov    %eax,-0x10(%ebp)
80100f18:	eb 17                	jmp    80100f31 <exec+0x322>
    if(*s == '/')
80100f1a:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100f1d:	0f b6 00             	movzbl (%eax),%eax
80100f20:	3c 2f                	cmp    $0x2f,%al
80100f22:	75 09                	jne    80100f2d <exec+0x31e>
      last = s+1;
80100f24:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100f27:	83 c0 01             	add    $0x1,%eax
80100f2a:	89 45 f0             	mov    %eax,-0x10(%ebp)
  sp -= (3+argc+1) * 4;
  if(copyout(pgdir, sp, ustack, (3+argc+1)*4) < 0)
    goto bad;

  // Save program name for debugging.
  for(last=s=path; *s; s++)
80100f2d:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80100f31:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100f34:	0f b6 00             	movzbl (%eax),%eax
80100f37:	84 c0                	test   %al,%al
80100f39:	75 df                	jne    80100f1a <exec+0x30b>
    if(*s == '/')
      last = s+1;
  safestrcpy(proc->name, last, sizeof(proc->name));
80100f3b:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80100f41:	83 c0 6c             	add    $0x6c,%eax
80100f44:	83 ec 04             	sub    $0x4,%esp
80100f47:	6a 10                	push   $0x10
80100f49:	ff 75 f0             	pushl  -0x10(%ebp)
80100f4c:	50                   	push   %eax
80100f4d:	e8 5c 50 00 00       	call   80105fae <safestrcpy>
80100f52:	83 c4 10             	add    $0x10,%esp

  // Commit to the user image.
  oldpgdir = proc->pgdir;
80100f55:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80100f5b:	8b 40 04             	mov    0x4(%eax),%eax
80100f5e:	89 45 d0             	mov    %eax,-0x30(%ebp)
  proc->pgdir = pgdir;
80100f61:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80100f67:	8b 55 d4             	mov    -0x2c(%ebp),%edx
80100f6a:	89 50 04             	mov    %edx,0x4(%eax)
  proc->sz = sz;
80100f6d:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80100f73:	8b 55 e0             	mov    -0x20(%ebp),%edx
80100f76:	89 10                	mov    %edx,(%eax)
  proc->tf->eip = elf.entry;  // main
80100f78:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80100f7e:	8b 40 18             	mov    0x18(%eax),%eax
80100f81:	8b 95 24 ff ff ff    	mov    -0xdc(%ebp),%edx
80100f87:	89 50 38             	mov    %edx,0x38(%eax)
  proc->tf->esp = sp;
80100f8a:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80100f90:	8b 40 18             	mov    0x18(%eax),%eax
80100f93:	8b 55 dc             	mov    -0x24(%ebp),%edx
80100f96:	89 50 44             	mov    %edx,0x44(%eax)
  switchuvm(proc);
80100f99:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80100f9f:	83 ec 0c             	sub    $0xc,%esp
80100fa2:	50                   	push   %eax
80100fa3:	e8 21 7b 00 00       	call   80108ac9 <switchuvm>
80100fa8:	83 c4 10             	add    $0x10,%esp
  freevm(oldpgdir);
80100fab:	83 ec 0c             	sub    $0xc,%esp
80100fae:	ff 75 d0             	pushl  -0x30(%ebp)
80100fb1:	e8 59 7f 00 00       	call   80108f0f <freevm>
80100fb6:	83 c4 10             	add    $0x10,%esp
  return 0;
80100fb9:	b8 00 00 00 00       	mov    $0x0,%eax
80100fbe:	eb 51                	jmp    80101011 <exec+0x402>
  ilock(ip);
  pgdir = 0;

  // Check ELF header
  if(readi(ip, (char*)&elf, 0, sizeof(elf)) < sizeof(elf))
    goto bad;
80100fc0:	90                   	nop
80100fc1:	eb 1c                	jmp    80100fdf <exec+0x3d0>
  if(elf.magic != ELF_MAGIC)
    goto bad;
80100fc3:	90                   	nop
80100fc4:	eb 19                	jmp    80100fdf <exec+0x3d0>

  if((pgdir = setupkvm()) == 0)
    goto bad;
80100fc6:	90                   	nop
80100fc7:	eb 16                	jmp    80100fdf <exec+0x3d0>

  // Load program into memory.
  sz = 0;
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
    if(readi(ip, (char*)&ph, off, sizeof(ph)) != sizeof(ph))
      goto bad;
80100fc9:	90                   	nop
80100fca:	eb 13                	jmp    80100fdf <exec+0x3d0>
    if(ph.type != ELF_PROG_LOAD)
      continue;
    if(ph.memsz < ph.filesz)
      goto bad;
80100fcc:	90                   	nop
80100fcd:	eb 10                	jmp    80100fdf <exec+0x3d0>
    if((sz = allocuvm(pgdir, sz, ph.vaddr + ph.memsz)) == 0)
      goto bad;
80100fcf:	90                   	nop
80100fd0:	eb 0d                	jmp    80100fdf <exec+0x3d0>
    if(loaduvm(pgdir, (char*)ph.vaddr, ip, ph.off, ph.filesz) < 0)
      goto bad;
80100fd2:	90                   	nop
80100fd3:	eb 0a                	jmp    80100fdf <exec+0x3d0>

  // Allocate two pages at the next page boundary.
  // Make the first inaccessible.  Use the second as the user stack.
  sz = PGROUNDUP(sz);
  if((sz = allocuvm(pgdir, sz, sz + 2*PGSIZE)) == 0)
    goto bad;
80100fd5:	90                   	nop
80100fd6:	eb 07                	jmp    80100fdf <exec+0x3d0>
  sp = sz;

  // Push argument strings, prepare rest of stack in ustack.
  for(argc = 0; argv[argc]; argc++) {
    if(argc >= MAXARG)
      goto bad;
80100fd8:	90                   	nop
80100fd9:	eb 04                	jmp    80100fdf <exec+0x3d0>
    sp = (sp - (strlen(argv[argc]) + 1)) & ~3;
    if(copyout(pgdir, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
      goto bad;
80100fdb:	90                   	nop
80100fdc:	eb 01                	jmp    80100fdf <exec+0x3d0>
  ustack[1] = argc;
  ustack[2] = sp - (argc+1)*4;  // argv pointer

  sp -= (3+argc+1) * 4;
  if(copyout(pgdir, sp, ustack, (3+argc+1)*4) < 0)
    goto bad;
80100fde:	90                   	nop
  switchuvm(proc);
  freevm(oldpgdir);
  return 0;

 bad:
  if(pgdir)
80100fdf:	83 7d d4 00          	cmpl   $0x0,-0x2c(%ebp)
80100fe3:	74 0e                	je     80100ff3 <exec+0x3e4>
    freevm(pgdir);
80100fe5:	83 ec 0c             	sub    $0xc,%esp
80100fe8:	ff 75 d4             	pushl  -0x2c(%ebp)
80100feb:	e8 1f 7f 00 00       	call   80108f0f <freevm>
80100ff0:	83 c4 10             	add    $0x10,%esp
  if(ip){
80100ff3:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
80100ff7:	74 13                	je     8010100c <exec+0x3fd>
    iunlockput(ip);
80100ff9:	83 ec 0c             	sub    $0xc,%esp
80100ffc:	ff 75 d8             	pushl  -0x28(%ebp)
80100fff:	e8 c4 0c 00 00       	call   80101cc8 <iunlockput>
80101004:	83 c4 10             	add    $0x10,%esp
    end_op();
80101007:	e8 6b 26 00 00       	call   80103677 <end_op>
  }
  return -1;
8010100c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80101011:	c9                   	leave  
80101012:	c3                   	ret    

80101013 <fileinit>:
  struct file file[NFILE];
} ftable;

void
fileinit(void)
{
80101013:	55                   	push   %ebp
80101014:	89 e5                	mov    %esp,%ebp
80101016:	83 ec 08             	sub    $0x8,%esp
  initlock(&ftable.lock, "ftable");
80101019:	83 ec 08             	sub    $0x8,%esp
8010101c:	68 6a 92 10 80       	push   $0x8010926a
80101021:	68 40 18 11 80       	push   $0x80111840
80101026:	e8 fb 4a 00 00       	call   80105b26 <initlock>
8010102b:	83 c4 10             	add    $0x10,%esp
}
8010102e:	90                   	nop
8010102f:	c9                   	leave  
80101030:	c3                   	ret    

80101031 <filealloc>:

// Allocate a file structure.
struct file*
filealloc(void)
{
80101031:	55                   	push   %ebp
80101032:	89 e5                	mov    %esp,%ebp
80101034:	83 ec 18             	sub    $0x18,%esp
  struct file *f;

  acquire(&ftable.lock);
80101037:	83 ec 0c             	sub    $0xc,%esp
8010103a:	68 40 18 11 80       	push   $0x80111840
8010103f:	e8 04 4b 00 00       	call   80105b48 <acquire>
80101044:	83 c4 10             	add    $0x10,%esp
  for(f = ftable.file; f < ftable.file + NFILE; f++){
80101047:	c7 45 f4 74 18 11 80 	movl   $0x80111874,-0xc(%ebp)
8010104e:	eb 2d                	jmp    8010107d <filealloc+0x4c>
    if(f->ref == 0){
80101050:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101053:	8b 40 04             	mov    0x4(%eax),%eax
80101056:	85 c0                	test   %eax,%eax
80101058:	75 1f                	jne    80101079 <filealloc+0x48>
      f->ref = 1;
8010105a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010105d:	c7 40 04 01 00 00 00 	movl   $0x1,0x4(%eax)
      release(&ftable.lock);
80101064:	83 ec 0c             	sub    $0xc,%esp
80101067:	68 40 18 11 80       	push   $0x80111840
8010106c:	e8 3e 4b 00 00       	call   80105baf <release>
80101071:	83 c4 10             	add    $0x10,%esp
      return f;
80101074:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101077:	eb 23                	jmp    8010109c <filealloc+0x6b>
filealloc(void)
{
  struct file *f;

  acquire(&ftable.lock);
  for(f = ftable.file; f < ftable.file + NFILE; f++){
80101079:	83 45 f4 18          	addl   $0x18,-0xc(%ebp)
8010107d:	b8 d4 21 11 80       	mov    $0x801121d4,%eax
80101082:	39 45 f4             	cmp    %eax,-0xc(%ebp)
80101085:	72 c9                	jb     80101050 <filealloc+0x1f>
      f->ref = 1;
      release(&ftable.lock);
      return f;
    }
  }
  release(&ftable.lock);
80101087:	83 ec 0c             	sub    $0xc,%esp
8010108a:	68 40 18 11 80       	push   $0x80111840
8010108f:	e8 1b 4b 00 00       	call   80105baf <release>
80101094:	83 c4 10             	add    $0x10,%esp
  return 0;
80101097:	b8 00 00 00 00       	mov    $0x0,%eax
}
8010109c:	c9                   	leave  
8010109d:	c3                   	ret    

8010109e <filedup>:

// Increment ref count for file f.
struct file*
filedup(struct file *f)
{
8010109e:	55                   	push   %ebp
8010109f:	89 e5                	mov    %esp,%ebp
801010a1:	83 ec 08             	sub    $0x8,%esp
  acquire(&ftable.lock);
801010a4:	83 ec 0c             	sub    $0xc,%esp
801010a7:	68 40 18 11 80       	push   $0x80111840
801010ac:	e8 97 4a 00 00       	call   80105b48 <acquire>
801010b1:	83 c4 10             	add    $0x10,%esp
  if(f->ref < 1)
801010b4:	8b 45 08             	mov    0x8(%ebp),%eax
801010b7:	8b 40 04             	mov    0x4(%eax),%eax
801010ba:	85 c0                	test   %eax,%eax
801010bc:	7f 0d                	jg     801010cb <filedup+0x2d>
    panic("filedup");
801010be:	83 ec 0c             	sub    $0xc,%esp
801010c1:	68 71 92 10 80       	push   $0x80109271
801010c6:	e8 9b f4 ff ff       	call   80100566 <panic>
  f->ref++;
801010cb:	8b 45 08             	mov    0x8(%ebp),%eax
801010ce:	8b 40 04             	mov    0x4(%eax),%eax
801010d1:	8d 50 01             	lea    0x1(%eax),%edx
801010d4:	8b 45 08             	mov    0x8(%ebp),%eax
801010d7:	89 50 04             	mov    %edx,0x4(%eax)
  release(&ftable.lock);
801010da:	83 ec 0c             	sub    $0xc,%esp
801010dd:	68 40 18 11 80       	push   $0x80111840
801010e2:	e8 c8 4a 00 00       	call   80105baf <release>
801010e7:	83 c4 10             	add    $0x10,%esp
  return f;
801010ea:	8b 45 08             	mov    0x8(%ebp),%eax
}
801010ed:	c9                   	leave  
801010ee:	c3                   	ret    

801010ef <fileclose>:

// Close file f.  (Decrement ref count, close when reaches 0.)
void
fileclose(struct file *f)
{
801010ef:	55                   	push   %ebp
801010f0:	89 e5                	mov    %esp,%ebp
801010f2:	83 ec 28             	sub    $0x28,%esp
  struct file ff;

  acquire(&ftable.lock);
801010f5:	83 ec 0c             	sub    $0xc,%esp
801010f8:	68 40 18 11 80       	push   $0x80111840
801010fd:	e8 46 4a 00 00       	call   80105b48 <acquire>
80101102:	83 c4 10             	add    $0x10,%esp
  if(f->ref < 1)
80101105:	8b 45 08             	mov    0x8(%ebp),%eax
80101108:	8b 40 04             	mov    0x4(%eax),%eax
8010110b:	85 c0                	test   %eax,%eax
8010110d:	7f 0d                	jg     8010111c <fileclose+0x2d>
    panic("fileclose");
8010110f:	83 ec 0c             	sub    $0xc,%esp
80101112:	68 79 92 10 80       	push   $0x80109279
80101117:	e8 4a f4 ff ff       	call   80100566 <panic>
  if(--f->ref > 0){
8010111c:	8b 45 08             	mov    0x8(%ebp),%eax
8010111f:	8b 40 04             	mov    0x4(%eax),%eax
80101122:	8d 50 ff             	lea    -0x1(%eax),%edx
80101125:	8b 45 08             	mov    0x8(%ebp),%eax
80101128:	89 50 04             	mov    %edx,0x4(%eax)
8010112b:	8b 45 08             	mov    0x8(%ebp),%eax
8010112e:	8b 40 04             	mov    0x4(%eax),%eax
80101131:	85 c0                	test   %eax,%eax
80101133:	7e 15                	jle    8010114a <fileclose+0x5b>
    release(&ftable.lock);
80101135:	83 ec 0c             	sub    $0xc,%esp
80101138:	68 40 18 11 80       	push   $0x80111840
8010113d:	e8 6d 4a 00 00       	call   80105baf <release>
80101142:	83 c4 10             	add    $0x10,%esp
80101145:	e9 8b 00 00 00       	jmp    801011d5 <fileclose+0xe6>
    return;
  }
  ff = *f;
8010114a:	8b 45 08             	mov    0x8(%ebp),%eax
8010114d:	8b 10                	mov    (%eax),%edx
8010114f:	89 55 e0             	mov    %edx,-0x20(%ebp)
80101152:	8b 50 04             	mov    0x4(%eax),%edx
80101155:	89 55 e4             	mov    %edx,-0x1c(%ebp)
80101158:	8b 50 08             	mov    0x8(%eax),%edx
8010115b:	89 55 e8             	mov    %edx,-0x18(%ebp)
8010115e:	8b 50 0c             	mov    0xc(%eax),%edx
80101161:	89 55 ec             	mov    %edx,-0x14(%ebp)
80101164:	8b 50 10             	mov    0x10(%eax),%edx
80101167:	89 55 f0             	mov    %edx,-0x10(%ebp)
8010116a:	8b 40 14             	mov    0x14(%eax),%eax
8010116d:	89 45 f4             	mov    %eax,-0xc(%ebp)
  f->ref = 0;
80101170:	8b 45 08             	mov    0x8(%ebp),%eax
80101173:	c7 40 04 00 00 00 00 	movl   $0x0,0x4(%eax)
  f->type = FD_NONE;
8010117a:	8b 45 08             	mov    0x8(%ebp),%eax
8010117d:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
  release(&ftable.lock);
80101183:	83 ec 0c             	sub    $0xc,%esp
80101186:	68 40 18 11 80       	push   $0x80111840
8010118b:	e8 1f 4a 00 00       	call   80105baf <release>
80101190:	83 c4 10             	add    $0x10,%esp
  
  if(ff.type == FD_PIPE)
80101193:	8b 45 e0             	mov    -0x20(%ebp),%eax
80101196:	83 f8 01             	cmp    $0x1,%eax
80101199:	75 19                	jne    801011b4 <fileclose+0xc5>
    pipeclose(ff.pipe, ff.writable);
8010119b:	0f b6 45 e9          	movzbl -0x17(%ebp),%eax
8010119f:	0f be d0             	movsbl %al,%edx
801011a2:	8b 45 ec             	mov    -0x14(%ebp),%eax
801011a5:	83 ec 08             	sub    $0x8,%esp
801011a8:	52                   	push   %edx
801011a9:	50                   	push   %eax
801011aa:	e8 83 30 00 00       	call   80104232 <pipeclose>
801011af:	83 c4 10             	add    $0x10,%esp
801011b2:	eb 21                	jmp    801011d5 <fileclose+0xe6>
  else if(ff.type == FD_INODE){
801011b4:	8b 45 e0             	mov    -0x20(%ebp),%eax
801011b7:	83 f8 02             	cmp    $0x2,%eax
801011ba:	75 19                	jne    801011d5 <fileclose+0xe6>
    begin_op();
801011bc:	e8 2a 24 00 00       	call   801035eb <begin_op>
    iput(ff.ip);
801011c1:	8b 45 f0             	mov    -0x10(%ebp),%eax
801011c4:	83 ec 0c             	sub    $0xc,%esp
801011c7:	50                   	push   %eax
801011c8:	e8 0b 0a 00 00       	call   80101bd8 <iput>
801011cd:	83 c4 10             	add    $0x10,%esp
    end_op();
801011d0:	e8 a2 24 00 00       	call   80103677 <end_op>
  }
}
801011d5:	c9                   	leave  
801011d6:	c3                   	ret    

801011d7 <filestat>:

// Get metadata about file f.
int
filestat(struct file *f, struct stat *st)
{
801011d7:	55                   	push   %ebp
801011d8:	89 e5                	mov    %esp,%ebp
801011da:	83 ec 08             	sub    $0x8,%esp
  if(f->type == FD_INODE){
801011dd:	8b 45 08             	mov    0x8(%ebp),%eax
801011e0:	8b 00                	mov    (%eax),%eax
801011e2:	83 f8 02             	cmp    $0x2,%eax
801011e5:	75 40                	jne    80101227 <filestat+0x50>
    ilock(f->ip);
801011e7:	8b 45 08             	mov    0x8(%ebp),%eax
801011ea:	8b 40 10             	mov    0x10(%eax),%eax
801011ed:	83 ec 0c             	sub    $0xc,%esp
801011f0:	50                   	push   %eax
801011f1:	e8 12 08 00 00       	call   80101a08 <ilock>
801011f6:	83 c4 10             	add    $0x10,%esp
    stati(f->ip, st);
801011f9:	8b 45 08             	mov    0x8(%ebp),%eax
801011fc:	8b 40 10             	mov    0x10(%eax),%eax
801011ff:	83 ec 08             	sub    $0x8,%esp
80101202:	ff 75 0c             	pushl  0xc(%ebp)
80101205:	50                   	push   %eax
80101206:	e8 25 0d 00 00       	call   80101f30 <stati>
8010120b:	83 c4 10             	add    $0x10,%esp
    iunlock(f->ip);
8010120e:	8b 45 08             	mov    0x8(%ebp),%eax
80101211:	8b 40 10             	mov    0x10(%eax),%eax
80101214:	83 ec 0c             	sub    $0xc,%esp
80101217:	50                   	push   %eax
80101218:	e8 49 09 00 00       	call   80101b66 <iunlock>
8010121d:	83 c4 10             	add    $0x10,%esp
    return 0;
80101220:	b8 00 00 00 00       	mov    $0x0,%eax
80101225:	eb 05                	jmp    8010122c <filestat+0x55>
  }
  return -1;
80101227:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
8010122c:	c9                   	leave  
8010122d:	c3                   	ret    

8010122e <fileread>:

// Read from file f.
int
fileread(struct file *f, char *addr, int n)
{
8010122e:	55                   	push   %ebp
8010122f:	89 e5                	mov    %esp,%ebp
80101231:	83 ec 18             	sub    $0x18,%esp
  int r;

  if(f->readable == 0)
80101234:	8b 45 08             	mov    0x8(%ebp),%eax
80101237:	0f b6 40 08          	movzbl 0x8(%eax),%eax
8010123b:	84 c0                	test   %al,%al
8010123d:	75 0a                	jne    80101249 <fileread+0x1b>
    return -1;
8010123f:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80101244:	e9 9b 00 00 00       	jmp    801012e4 <fileread+0xb6>
  if(f->type == FD_PIPE)
80101249:	8b 45 08             	mov    0x8(%ebp),%eax
8010124c:	8b 00                	mov    (%eax),%eax
8010124e:	83 f8 01             	cmp    $0x1,%eax
80101251:	75 1a                	jne    8010126d <fileread+0x3f>
    return piperead(f->pipe, addr, n);
80101253:	8b 45 08             	mov    0x8(%ebp),%eax
80101256:	8b 40 0c             	mov    0xc(%eax),%eax
80101259:	83 ec 04             	sub    $0x4,%esp
8010125c:	ff 75 10             	pushl  0x10(%ebp)
8010125f:	ff 75 0c             	pushl  0xc(%ebp)
80101262:	50                   	push   %eax
80101263:	e8 72 31 00 00       	call   801043da <piperead>
80101268:	83 c4 10             	add    $0x10,%esp
8010126b:	eb 77                	jmp    801012e4 <fileread+0xb6>
  if(f->type == FD_INODE){
8010126d:	8b 45 08             	mov    0x8(%ebp),%eax
80101270:	8b 00                	mov    (%eax),%eax
80101272:	83 f8 02             	cmp    $0x2,%eax
80101275:	75 60                	jne    801012d7 <fileread+0xa9>
    ilock(f->ip);
80101277:	8b 45 08             	mov    0x8(%ebp),%eax
8010127a:	8b 40 10             	mov    0x10(%eax),%eax
8010127d:	83 ec 0c             	sub    $0xc,%esp
80101280:	50                   	push   %eax
80101281:	e8 82 07 00 00       	call   80101a08 <ilock>
80101286:	83 c4 10             	add    $0x10,%esp
    if((r = readi(f->ip, addr, f->off, n)) > 0)
80101289:	8b 4d 10             	mov    0x10(%ebp),%ecx
8010128c:	8b 45 08             	mov    0x8(%ebp),%eax
8010128f:	8b 50 14             	mov    0x14(%eax),%edx
80101292:	8b 45 08             	mov    0x8(%ebp),%eax
80101295:	8b 40 10             	mov    0x10(%eax),%eax
80101298:	51                   	push   %ecx
80101299:	52                   	push   %edx
8010129a:	ff 75 0c             	pushl  0xc(%ebp)
8010129d:	50                   	push   %eax
8010129e:	e8 d3 0c 00 00       	call   80101f76 <readi>
801012a3:	83 c4 10             	add    $0x10,%esp
801012a6:	89 45 f4             	mov    %eax,-0xc(%ebp)
801012a9:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801012ad:	7e 11                	jle    801012c0 <fileread+0x92>
      f->off += r;
801012af:	8b 45 08             	mov    0x8(%ebp),%eax
801012b2:	8b 50 14             	mov    0x14(%eax),%edx
801012b5:	8b 45 f4             	mov    -0xc(%ebp),%eax
801012b8:	01 c2                	add    %eax,%edx
801012ba:	8b 45 08             	mov    0x8(%ebp),%eax
801012bd:	89 50 14             	mov    %edx,0x14(%eax)
    iunlock(f->ip);
801012c0:	8b 45 08             	mov    0x8(%ebp),%eax
801012c3:	8b 40 10             	mov    0x10(%eax),%eax
801012c6:	83 ec 0c             	sub    $0xc,%esp
801012c9:	50                   	push   %eax
801012ca:	e8 97 08 00 00       	call   80101b66 <iunlock>
801012cf:	83 c4 10             	add    $0x10,%esp
    return r;
801012d2:	8b 45 f4             	mov    -0xc(%ebp),%eax
801012d5:	eb 0d                	jmp    801012e4 <fileread+0xb6>
  }
  panic("fileread");
801012d7:	83 ec 0c             	sub    $0xc,%esp
801012da:	68 83 92 10 80       	push   $0x80109283
801012df:	e8 82 f2 ff ff       	call   80100566 <panic>
}
801012e4:	c9                   	leave  
801012e5:	c3                   	ret    

801012e6 <filewrite>:

// Write to file f.
int
filewrite(struct file *f, char *addr, int n)
{
801012e6:	55                   	push   %ebp
801012e7:	89 e5                	mov    %esp,%ebp
801012e9:	53                   	push   %ebx
801012ea:	83 ec 14             	sub    $0x14,%esp
  int r;

  if(f->writable == 0)
801012ed:	8b 45 08             	mov    0x8(%ebp),%eax
801012f0:	0f b6 40 09          	movzbl 0x9(%eax),%eax
801012f4:	84 c0                	test   %al,%al
801012f6:	75 0a                	jne    80101302 <filewrite+0x1c>
    return -1;
801012f8:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801012fd:	e9 1b 01 00 00       	jmp    8010141d <filewrite+0x137>
  if(f->type == FD_PIPE)
80101302:	8b 45 08             	mov    0x8(%ebp),%eax
80101305:	8b 00                	mov    (%eax),%eax
80101307:	83 f8 01             	cmp    $0x1,%eax
8010130a:	75 1d                	jne    80101329 <filewrite+0x43>
    return pipewrite(f->pipe, addr, n);
8010130c:	8b 45 08             	mov    0x8(%ebp),%eax
8010130f:	8b 40 0c             	mov    0xc(%eax),%eax
80101312:	83 ec 04             	sub    $0x4,%esp
80101315:	ff 75 10             	pushl  0x10(%ebp)
80101318:	ff 75 0c             	pushl  0xc(%ebp)
8010131b:	50                   	push   %eax
8010131c:	e8 bb 2f 00 00       	call   801042dc <pipewrite>
80101321:	83 c4 10             	add    $0x10,%esp
80101324:	e9 f4 00 00 00       	jmp    8010141d <filewrite+0x137>
  if(f->type == FD_INODE){
80101329:	8b 45 08             	mov    0x8(%ebp),%eax
8010132c:	8b 00                	mov    (%eax),%eax
8010132e:	83 f8 02             	cmp    $0x2,%eax
80101331:	0f 85 d9 00 00 00    	jne    80101410 <filewrite+0x12a>
    // the maximum log transaction size, including
    // i-node, indirect block, allocation blocks,
    // and 2 blocks of slop for non-aligned writes.
    // this really belongs lower down, since writei()
    // might be writing a device like the console.
    int max = ((LOGSIZE-1-1-2) / 2) * 512;
80101337:	c7 45 ec 00 1a 00 00 	movl   $0x1a00,-0x14(%ebp)
    int i = 0;
8010133e:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
    while(i < n){
80101345:	e9 a3 00 00 00       	jmp    801013ed <filewrite+0x107>
      int n1 = n - i;
8010134a:	8b 45 10             	mov    0x10(%ebp),%eax
8010134d:	2b 45 f4             	sub    -0xc(%ebp),%eax
80101350:	89 45 f0             	mov    %eax,-0x10(%ebp)
      if(n1 > max)
80101353:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101356:	3b 45 ec             	cmp    -0x14(%ebp),%eax
80101359:	7e 06                	jle    80101361 <filewrite+0x7b>
        n1 = max;
8010135b:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010135e:	89 45 f0             	mov    %eax,-0x10(%ebp)

      begin_op();
80101361:	e8 85 22 00 00       	call   801035eb <begin_op>
      ilock(f->ip);
80101366:	8b 45 08             	mov    0x8(%ebp),%eax
80101369:	8b 40 10             	mov    0x10(%eax),%eax
8010136c:	83 ec 0c             	sub    $0xc,%esp
8010136f:	50                   	push   %eax
80101370:	e8 93 06 00 00       	call   80101a08 <ilock>
80101375:	83 c4 10             	add    $0x10,%esp
      if ((r = writei(f->ip, addr + i, f->off, n1)) > 0)
80101378:	8b 4d f0             	mov    -0x10(%ebp),%ecx
8010137b:	8b 45 08             	mov    0x8(%ebp),%eax
8010137e:	8b 50 14             	mov    0x14(%eax),%edx
80101381:	8b 5d f4             	mov    -0xc(%ebp),%ebx
80101384:	8b 45 0c             	mov    0xc(%ebp),%eax
80101387:	01 c3                	add    %eax,%ebx
80101389:	8b 45 08             	mov    0x8(%ebp),%eax
8010138c:	8b 40 10             	mov    0x10(%eax),%eax
8010138f:	51                   	push   %ecx
80101390:	52                   	push   %edx
80101391:	53                   	push   %ebx
80101392:	50                   	push   %eax
80101393:	e8 35 0d 00 00       	call   801020cd <writei>
80101398:	83 c4 10             	add    $0x10,%esp
8010139b:	89 45 e8             	mov    %eax,-0x18(%ebp)
8010139e:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
801013a2:	7e 11                	jle    801013b5 <filewrite+0xcf>
        f->off += r;
801013a4:	8b 45 08             	mov    0x8(%ebp),%eax
801013a7:	8b 50 14             	mov    0x14(%eax),%edx
801013aa:	8b 45 e8             	mov    -0x18(%ebp),%eax
801013ad:	01 c2                	add    %eax,%edx
801013af:	8b 45 08             	mov    0x8(%ebp),%eax
801013b2:	89 50 14             	mov    %edx,0x14(%eax)
      iunlock(f->ip);
801013b5:	8b 45 08             	mov    0x8(%ebp),%eax
801013b8:	8b 40 10             	mov    0x10(%eax),%eax
801013bb:	83 ec 0c             	sub    $0xc,%esp
801013be:	50                   	push   %eax
801013bf:	e8 a2 07 00 00       	call   80101b66 <iunlock>
801013c4:	83 c4 10             	add    $0x10,%esp
      end_op();
801013c7:	e8 ab 22 00 00       	call   80103677 <end_op>

      if(r < 0)
801013cc:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
801013d0:	78 29                	js     801013fb <filewrite+0x115>
        break;
      if(r != n1)
801013d2:	8b 45 e8             	mov    -0x18(%ebp),%eax
801013d5:	3b 45 f0             	cmp    -0x10(%ebp),%eax
801013d8:	74 0d                	je     801013e7 <filewrite+0x101>
        panic("short filewrite");
801013da:	83 ec 0c             	sub    $0xc,%esp
801013dd:	68 8c 92 10 80       	push   $0x8010928c
801013e2:	e8 7f f1 ff ff       	call   80100566 <panic>
      i += r;
801013e7:	8b 45 e8             	mov    -0x18(%ebp),%eax
801013ea:	01 45 f4             	add    %eax,-0xc(%ebp)
    // and 2 blocks of slop for non-aligned writes.
    // this really belongs lower down, since writei()
    // might be writing a device like the console.
    int max = ((LOGSIZE-1-1-2) / 2) * 512;
    int i = 0;
    while(i < n){
801013ed:	8b 45 f4             	mov    -0xc(%ebp),%eax
801013f0:	3b 45 10             	cmp    0x10(%ebp),%eax
801013f3:	0f 8c 51 ff ff ff    	jl     8010134a <filewrite+0x64>
801013f9:	eb 01                	jmp    801013fc <filewrite+0x116>
        f->off += r;
      iunlock(f->ip);
      end_op();

      if(r < 0)
        break;
801013fb:	90                   	nop
      if(r != n1)
        panic("short filewrite");
      i += r;
    }
    return i == n ? n : -1;
801013fc:	8b 45 f4             	mov    -0xc(%ebp),%eax
801013ff:	3b 45 10             	cmp    0x10(%ebp),%eax
80101402:	75 05                	jne    80101409 <filewrite+0x123>
80101404:	8b 45 10             	mov    0x10(%ebp),%eax
80101407:	eb 14                	jmp    8010141d <filewrite+0x137>
80101409:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010140e:	eb 0d                	jmp    8010141d <filewrite+0x137>
  }
  panic("filewrite");
80101410:	83 ec 0c             	sub    $0xc,%esp
80101413:	68 9c 92 10 80       	push   $0x8010929c
80101418:	e8 49 f1 ff ff       	call   80100566 <panic>
}
8010141d:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80101420:	c9                   	leave  
80101421:	c3                   	ret    

80101422 <readsb>:
struct superblock sb;   // there should be one per dev, but we run with one dev

// Read the super block.
void
readsb(int dev, struct superblock *sb)
{
80101422:	55                   	push   %ebp
80101423:	89 e5                	mov    %esp,%ebp
80101425:	83 ec 18             	sub    $0x18,%esp
  struct buf *bp;
  
  bp = bread(dev, 1);
80101428:	8b 45 08             	mov    0x8(%ebp),%eax
8010142b:	83 ec 08             	sub    $0x8,%esp
8010142e:	6a 01                	push   $0x1
80101430:	50                   	push   %eax
80101431:	e8 80 ed ff ff       	call   801001b6 <bread>
80101436:	83 c4 10             	add    $0x10,%esp
80101439:	89 45 f4             	mov    %eax,-0xc(%ebp)
  memmove(sb, bp->data, sizeof(*sb));
8010143c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010143f:	83 c0 18             	add    $0x18,%eax
80101442:	83 ec 04             	sub    $0x4,%esp
80101445:	6a 1c                	push   $0x1c
80101447:	50                   	push   %eax
80101448:	ff 75 0c             	pushl  0xc(%ebp)
8010144b:	e8 1a 4a 00 00       	call   80105e6a <memmove>
80101450:	83 c4 10             	add    $0x10,%esp
  brelse(bp);
80101453:	83 ec 0c             	sub    $0xc,%esp
80101456:	ff 75 f4             	pushl  -0xc(%ebp)
80101459:	e8 d0 ed ff ff       	call   8010022e <brelse>
8010145e:	83 c4 10             	add    $0x10,%esp
}
80101461:	90                   	nop
80101462:	c9                   	leave  
80101463:	c3                   	ret    

80101464 <bzero>:

// Zero a block.
static void
bzero(int dev, int bno)
{
80101464:	55                   	push   %ebp
80101465:	89 e5                	mov    %esp,%ebp
80101467:	83 ec 18             	sub    $0x18,%esp
  struct buf *bp;
  
  bp = bread(dev, bno);
8010146a:	8b 55 0c             	mov    0xc(%ebp),%edx
8010146d:	8b 45 08             	mov    0x8(%ebp),%eax
80101470:	83 ec 08             	sub    $0x8,%esp
80101473:	52                   	push   %edx
80101474:	50                   	push   %eax
80101475:	e8 3c ed ff ff       	call   801001b6 <bread>
8010147a:	83 c4 10             	add    $0x10,%esp
8010147d:	89 45 f4             	mov    %eax,-0xc(%ebp)
  memset(bp->data, 0, BSIZE);
80101480:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101483:	83 c0 18             	add    $0x18,%eax
80101486:	83 ec 04             	sub    $0x4,%esp
80101489:	68 00 02 00 00       	push   $0x200
8010148e:	6a 00                	push   $0x0
80101490:	50                   	push   %eax
80101491:	e8 15 49 00 00       	call   80105dab <memset>
80101496:	83 c4 10             	add    $0x10,%esp
  log_write(bp);
80101499:	83 ec 0c             	sub    $0xc,%esp
8010149c:	ff 75 f4             	pushl  -0xc(%ebp)
8010149f:	e8 7f 23 00 00       	call   80103823 <log_write>
801014a4:	83 c4 10             	add    $0x10,%esp
  brelse(bp);
801014a7:	83 ec 0c             	sub    $0xc,%esp
801014aa:	ff 75 f4             	pushl  -0xc(%ebp)
801014ad:	e8 7c ed ff ff       	call   8010022e <brelse>
801014b2:	83 c4 10             	add    $0x10,%esp
}
801014b5:	90                   	nop
801014b6:	c9                   	leave  
801014b7:	c3                   	ret    

801014b8 <balloc>:
// Blocks. 

// Allocate a zeroed disk block.
static uint
balloc(uint dev)
{
801014b8:	55                   	push   %ebp
801014b9:	89 e5                	mov    %esp,%ebp
801014bb:	83 ec 18             	sub    $0x18,%esp
  int b, bi, m;
  struct buf *bp;

  bp = 0;
801014be:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  for(b = 0; b < sb.size; b += BPB){
801014c5:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
801014cc:	e9 13 01 00 00       	jmp    801015e4 <balloc+0x12c>
    bp = bread(dev, BBLOCK(b, sb));
801014d1:	8b 45 f4             	mov    -0xc(%ebp),%eax
801014d4:	8d 90 ff 0f 00 00    	lea    0xfff(%eax),%edx
801014da:	85 c0                	test   %eax,%eax
801014dc:	0f 48 c2             	cmovs  %edx,%eax
801014df:	c1 f8 0c             	sar    $0xc,%eax
801014e2:	89 c2                	mov    %eax,%edx
801014e4:	a1 58 22 11 80       	mov    0x80112258,%eax
801014e9:	01 d0                	add    %edx,%eax
801014eb:	83 ec 08             	sub    $0x8,%esp
801014ee:	50                   	push   %eax
801014ef:	ff 75 08             	pushl  0x8(%ebp)
801014f2:	e8 bf ec ff ff       	call   801001b6 <bread>
801014f7:	83 c4 10             	add    $0x10,%esp
801014fa:	89 45 ec             	mov    %eax,-0x14(%ebp)
    for(bi = 0; bi < BPB && b + bi < sb.size; bi++){
801014fd:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
80101504:	e9 a6 00 00 00       	jmp    801015af <balloc+0xf7>
      m = 1 << (bi % 8);
80101509:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010150c:	99                   	cltd   
8010150d:	c1 ea 1d             	shr    $0x1d,%edx
80101510:	01 d0                	add    %edx,%eax
80101512:	83 e0 07             	and    $0x7,%eax
80101515:	29 d0                	sub    %edx,%eax
80101517:	ba 01 00 00 00       	mov    $0x1,%edx
8010151c:	89 c1                	mov    %eax,%ecx
8010151e:	d3 e2                	shl    %cl,%edx
80101520:	89 d0                	mov    %edx,%eax
80101522:	89 45 e8             	mov    %eax,-0x18(%ebp)
      if((bp->data[bi/8] & m) == 0){  // Is block free?
80101525:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101528:	8d 50 07             	lea    0x7(%eax),%edx
8010152b:	85 c0                	test   %eax,%eax
8010152d:	0f 48 c2             	cmovs  %edx,%eax
80101530:	c1 f8 03             	sar    $0x3,%eax
80101533:	89 c2                	mov    %eax,%edx
80101535:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101538:	0f b6 44 10 18       	movzbl 0x18(%eax,%edx,1),%eax
8010153d:	0f b6 c0             	movzbl %al,%eax
80101540:	23 45 e8             	and    -0x18(%ebp),%eax
80101543:	85 c0                	test   %eax,%eax
80101545:	75 64                	jne    801015ab <balloc+0xf3>
        bp->data[bi/8] |= m;  // Mark block in use.
80101547:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010154a:	8d 50 07             	lea    0x7(%eax),%edx
8010154d:	85 c0                	test   %eax,%eax
8010154f:	0f 48 c2             	cmovs  %edx,%eax
80101552:	c1 f8 03             	sar    $0x3,%eax
80101555:	8b 55 ec             	mov    -0x14(%ebp),%edx
80101558:	0f b6 54 02 18       	movzbl 0x18(%edx,%eax,1),%edx
8010155d:	89 d1                	mov    %edx,%ecx
8010155f:	8b 55 e8             	mov    -0x18(%ebp),%edx
80101562:	09 ca                	or     %ecx,%edx
80101564:	89 d1                	mov    %edx,%ecx
80101566:	8b 55 ec             	mov    -0x14(%ebp),%edx
80101569:	88 4c 02 18          	mov    %cl,0x18(%edx,%eax,1)
        log_write(bp);
8010156d:	83 ec 0c             	sub    $0xc,%esp
80101570:	ff 75 ec             	pushl  -0x14(%ebp)
80101573:	e8 ab 22 00 00       	call   80103823 <log_write>
80101578:	83 c4 10             	add    $0x10,%esp
        brelse(bp);
8010157b:	83 ec 0c             	sub    $0xc,%esp
8010157e:	ff 75 ec             	pushl  -0x14(%ebp)
80101581:	e8 a8 ec ff ff       	call   8010022e <brelse>
80101586:	83 c4 10             	add    $0x10,%esp
        bzero(dev, b + bi);
80101589:	8b 55 f4             	mov    -0xc(%ebp),%edx
8010158c:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010158f:	01 c2                	add    %eax,%edx
80101591:	8b 45 08             	mov    0x8(%ebp),%eax
80101594:	83 ec 08             	sub    $0x8,%esp
80101597:	52                   	push   %edx
80101598:	50                   	push   %eax
80101599:	e8 c6 fe ff ff       	call   80101464 <bzero>
8010159e:	83 c4 10             	add    $0x10,%esp
        return b + bi;
801015a1:	8b 55 f4             	mov    -0xc(%ebp),%edx
801015a4:	8b 45 f0             	mov    -0x10(%ebp),%eax
801015a7:	01 d0                	add    %edx,%eax
801015a9:	eb 57                	jmp    80101602 <balloc+0x14a>
  struct buf *bp;

  bp = 0;
  for(b = 0; b < sb.size; b += BPB){
    bp = bread(dev, BBLOCK(b, sb));
    for(bi = 0; bi < BPB && b + bi < sb.size; bi++){
801015ab:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
801015af:	81 7d f0 ff 0f 00 00 	cmpl   $0xfff,-0x10(%ebp)
801015b6:	7f 17                	jg     801015cf <balloc+0x117>
801015b8:	8b 55 f4             	mov    -0xc(%ebp),%edx
801015bb:	8b 45 f0             	mov    -0x10(%ebp),%eax
801015be:	01 d0                	add    %edx,%eax
801015c0:	89 c2                	mov    %eax,%edx
801015c2:	a1 40 22 11 80       	mov    0x80112240,%eax
801015c7:	39 c2                	cmp    %eax,%edx
801015c9:	0f 82 3a ff ff ff    	jb     80101509 <balloc+0x51>
        brelse(bp);
        bzero(dev, b + bi);
        return b + bi;
      }
    }
    brelse(bp);
801015cf:	83 ec 0c             	sub    $0xc,%esp
801015d2:	ff 75 ec             	pushl  -0x14(%ebp)
801015d5:	e8 54 ec ff ff       	call   8010022e <brelse>
801015da:	83 c4 10             	add    $0x10,%esp
{
  int b, bi, m;
  struct buf *bp;

  bp = 0;
  for(b = 0; b < sb.size; b += BPB){
801015dd:	81 45 f4 00 10 00 00 	addl   $0x1000,-0xc(%ebp)
801015e4:	8b 15 40 22 11 80    	mov    0x80112240,%edx
801015ea:	8b 45 f4             	mov    -0xc(%ebp),%eax
801015ed:	39 c2                	cmp    %eax,%edx
801015ef:	0f 87 dc fe ff ff    	ja     801014d1 <balloc+0x19>
        return b + bi;
      }
    }
    brelse(bp);
  }
  panic("balloc: out of blocks");
801015f5:	83 ec 0c             	sub    $0xc,%esp
801015f8:	68 a8 92 10 80       	push   $0x801092a8
801015fd:	e8 64 ef ff ff       	call   80100566 <panic>
}
80101602:	c9                   	leave  
80101603:	c3                   	ret    

80101604 <bfree>:

// Free a disk block.
static void
bfree(int dev, uint b)
{
80101604:	55                   	push   %ebp
80101605:	89 e5                	mov    %esp,%ebp
80101607:	83 ec 18             	sub    $0x18,%esp
  struct buf *bp;
  int bi, m;

  readsb(dev, &sb);
8010160a:	83 ec 08             	sub    $0x8,%esp
8010160d:	68 40 22 11 80       	push   $0x80112240
80101612:	ff 75 08             	pushl  0x8(%ebp)
80101615:	e8 08 fe ff ff       	call   80101422 <readsb>
8010161a:	83 c4 10             	add    $0x10,%esp
  bp = bread(dev, BBLOCK(b, sb));
8010161d:	8b 45 0c             	mov    0xc(%ebp),%eax
80101620:	c1 e8 0c             	shr    $0xc,%eax
80101623:	89 c2                	mov    %eax,%edx
80101625:	a1 58 22 11 80       	mov    0x80112258,%eax
8010162a:	01 c2                	add    %eax,%edx
8010162c:	8b 45 08             	mov    0x8(%ebp),%eax
8010162f:	83 ec 08             	sub    $0x8,%esp
80101632:	52                   	push   %edx
80101633:	50                   	push   %eax
80101634:	e8 7d eb ff ff       	call   801001b6 <bread>
80101639:	83 c4 10             	add    $0x10,%esp
8010163c:	89 45 f4             	mov    %eax,-0xc(%ebp)
  bi = b % BPB;
8010163f:	8b 45 0c             	mov    0xc(%ebp),%eax
80101642:	25 ff 0f 00 00       	and    $0xfff,%eax
80101647:	89 45 f0             	mov    %eax,-0x10(%ebp)
  m = 1 << (bi % 8);
8010164a:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010164d:	99                   	cltd   
8010164e:	c1 ea 1d             	shr    $0x1d,%edx
80101651:	01 d0                	add    %edx,%eax
80101653:	83 e0 07             	and    $0x7,%eax
80101656:	29 d0                	sub    %edx,%eax
80101658:	ba 01 00 00 00       	mov    $0x1,%edx
8010165d:	89 c1                	mov    %eax,%ecx
8010165f:	d3 e2                	shl    %cl,%edx
80101661:	89 d0                	mov    %edx,%eax
80101663:	89 45 ec             	mov    %eax,-0x14(%ebp)
  if((bp->data[bi/8] & m) == 0)
80101666:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101669:	8d 50 07             	lea    0x7(%eax),%edx
8010166c:	85 c0                	test   %eax,%eax
8010166e:	0f 48 c2             	cmovs  %edx,%eax
80101671:	c1 f8 03             	sar    $0x3,%eax
80101674:	89 c2                	mov    %eax,%edx
80101676:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101679:	0f b6 44 10 18       	movzbl 0x18(%eax,%edx,1),%eax
8010167e:	0f b6 c0             	movzbl %al,%eax
80101681:	23 45 ec             	and    -0x14(%ebp),%eax
80101684:	85 c0                	test   %eax,%eax
80101686:	75 0d                	jne    80101695 <bfree+0x91>
    panic("freeing free block");
80101688:	83 ec 0c             	sub    $0xc,%esp
8010168b:	68 be 92 10 80       	push   $0x801092be
80101690:	e8 d1 ee ff ff       	call   80100566 <panic>
  bp->data[bi/8] &= ~m;
80101695:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101698:	8d 50 07             	lea    0x7(%eax),%edx
8010169b:	85 c0                	test   %eax,%eax
8010169d:	0f 48 c2             	cmovs  %edx,%eax
801016a0:	c1 f8 03             	sar    $0x3,%eax
801016a3:	8b 55 f4             	mov    -0xc(%ebp),%edx
801016a6:	0f b6 54 02 18       	movzbl 0x18(%edx,%eax,1),%edx
801016ab:	89 d1                	mov    %edx,%ecx
801016ad:	8b 55 ec             	mov    -0x14(%ebp),%edx
801016b0:	f7 d2                	not    %edx
801016b2:	21 ca                	and    %ecx,%edx
801016b4:	89 d1                	mov    %edx,%ecx
801016b6:	8b 55 f4             	mov    -0xc(%ebp),%edx
801016b9:	88 4c 02 18          	mov    %cl,0x18(%edx,%eax,1)
  log_write(bp);
801016bd:	83 ec 0c             	sub    $0xc,%esp
801016c0:	ff 75 f4             	pushl  -0xc(%ebp)
801016c3:	e8 5b 21 00 00       	call   80103823 <log_write>
801016c8:	83 c4 10             	add    $0x10,%esp
  brelse(bp);
801016cb:	83 ec 0c             	sub    $0xc,%esp
801016ce:	ff 75 f4             	pushl  -0xc(%ebp)
801016d1:	e8 58 eb ff ff       	call   8010022e <brelse>
801016d6:	83 c4 10             	add    $0x10,%esp
}
801016d9:	90                   	nop
801016da:	c9                   	leave  
801016db:	c3                   	ret    

801016dc <iinit>:
  struct inode inode[NINODE];
} icache;

void
iinit(int dev)
{
801016dc:	55                   	push   %ebp
801016dd:	89 e5                	mov    %esp,%ebp
801016df:	57                   	push   %edi
801016e0:	56                   	push   %esi
801016e1:	53                   	push   %ebx
801016e2:	83 ec 1c             	sub    $0x1c,%esp
  initlock(&icache.lock, "icache");
801016e5:	83 ec 08             	sub    $0x8,%esp
801016e8:	68 d1 92 10 80       	push   $0x801092d1
801016ed:	68 60 22 11 80       	push   $0x80112260
801016f2:	e8 2f 44 00 00       	call   80105b26 <initlock>
801016f7:	83 c4 10             	add    $0x10,%esp
  readsb(dev, &sb);
801016fa:	83 ec 08             	sub    $0x8,%esp
801016fd:	68 40 22 11 80       	push   $0x80112240
80101702:	ff 75 08             	pushl  0x8(%ebp)
80101705:	e8 18 fd ff ff       	call   80101422 <readsb>
8010170a:	83 c4 10             	add    $0x10,%esp
  cprintf("sb: size %d nblocks %d ninodes %d nlog %d logstart %d inodestart %d bmap start %d\n", sb.size,
8010170d:	a1 58 22 11 80       	mov    0x80112258,%eax
80101712:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80101715:	8b 3d 54 22 11 80    	mov    0x80112254,%edi
8010171b:	8b 35 50 22 11 80    	mov    0x80112250,%esi
80101721:	8b 1d 4c 22 11 80    	mov    0x8011224c,%ebx
80101727:	8b 0d 48 22 11 80    	mov    0x80112248,%ecx
8010172d:	8b 15 44 22 11 80    	mov    0x80112244,%edx
80101733:	a1 40 22 11 80       	mov    0x80112240,%eax
80101738:	ff 75 e4             	pushl  -0x1c(%ebp)
8010173b:	57                   	push   %edi
8010173c:	56                   	push   %esi
8010173d:	53                   	push   %ebx
8010173e:	51                   	push   %ecx
8010173f:	52                   	push   %edx
80101740:	50                   	push   %eax
80101741:	68 d8 92 10 80       	push   $0x801092d8
80101746:	e8 7b ec ff ff       	call   801003c6 <cprintf>
8010174b:	83 c4 20             	add    $0x20,%esp
          sb.nblocks, sb.ninodes, sb.nlog, sb.logstart, sb.inodestart, sb.bmapstart);
}
8010174e:	90                   	nop
8010174f:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101752:	5b                   	pop    %ebx
80101753:	5e                   	pop    %esi
80101754:	5f                   	pop    %edi
80101755:	5d                   	pop    %ebp
80101756:	c3                   	ret    

80101757 <ialloc>:

// Allocate a new inode with the given type on device dev.
// A free inode has a type of zero.
struct inode*
ialloc(uint dev, short type)
{
80101757:	55                   	push   %ebp
80101758:	89 e5                	mov    %esp,%ebp
8010175a:	83 ec 28             	sub    $0x28,%esp
8010175d:	8b 45 0c             	mov    0xc(%ebp),%eax
80101760:	66 89 45 e4          	mov    %ax,-0x1c(%ebp)
  int inum;
  struct buf *bp;
  struct dinode *dip;

  for(inum = 1; inum < sb.ninodes; inum++){
80101764:	c7 45 f4 01 00 00 00 	movl   $0x1,-0xc(%ebp)
8010176b:	e9 9e 00 00 00       	jmp    8010180e <ialloc+0xb7>
    bp = bread(dev, IBLOCK(inum, sb));
80101770:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101773:	c1 e8 03             	shr    $0x3,%eax
80101776:	89 c2                	mov    %eax,%edx
80101778:	a1 54 22 11 80       	mov    0x80112254,%eax
8010177d:	01 d0                	add    %edx,%eax
8010177f:	83 ec 08             	sub    $0x8,%esp
80101782:	50                   	push   %eax
80101783:	ff 75 08             	pushl  0x8(%ebp)
80101786:	e8 2b ea ff ff       	call   801001b6 <bread>
8010178b:	83 c4 10             	add    $0x10,%esp
8010178e:	89 45 f0             	mov    %eax,-0x10(%ebp)
    dip = (struct dinode*)bp->data + inum%IPB;
80101791:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101794:	8d 50 18             	lea    0x18(%eax),%edx
80101797:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010179a:	83 e0 07             	and    $0x7,%eax
8010179d:	c1 e0 06             	shl    $0x6,%eax
801017a0:	01 d0                	add    %edx,%eax
801017a2:	89 45 ec             	mov    %eax,-0x14(%ebp)
    if(dip->type == 0){  // a free inode
801017a5:	8b 45 ec             	mov    -0x14(%ebp),%eax
801017a8:	0f b7 00             	movzwl (%eax),%eax
801017ab:	66 85 c0             	test   %ax,%ax
801017ae:	75 4c                	jne    801017fc <ialloc+0xa5>
      memset(dip, 0, sizeof(*dip));
801017b0:	83 ec 04             	sub    $0x4,%esp
801017b3:	6a 40                	push   $0x40
801017b5:	6a 00                	push   $0x0
801017b7:	ff 75 ec             	pushl  -0x14(%ebp)
801017ba:	e8 ec 45 00 00       	call   80105dab <memset>
801017bf:	83 c4 10             	add    $0x10,%esp
      dip->type = type;
801017c2:	8b 45 ec             	mov    -0x14(%ebp),%eax
801017c5:	0f b7 55 e4          	movzwl -0x1c(%ebp),%edx
801017c9:	66 89 10             	mov    %dx,(%eax)
      log_write(bp);   // mark it allocated on the disk
801017cc:	83 ec 0c             	sub    $0xc,%esp
801017cf:	ff 75 f0             	pushl  -0x10(%ebp)
801017d2:	e8 4c 20 00 00       	call   80103823 <log_write>
801017d7:	83 c4 10             	add    $0x10,%esp
      brelse(bp);
801017da:	83 ec 0c             	sub    $0xc,%esp
801017dd:	ff 75 f0             	pushl  -0x10(%ebp)
801017e0:	e8 49 ea ff ff       	call   8010022e <brelse>
801017e5:	83 c4 10             	add    $0x10,%esp
      return iget(dev, inum);
801017e8:	8b 45 f4             	mov    -0xc(%ebp),%eax
801017eb:	83 ec 08             	sub    $0x8,%esp
801017ee:	50                   	push   %eax
801017ef:	ff 75 08             	pushl  0x8(%ebp)
801017f2:	e8 f8 00 00 00       	call   801018ef <iget>
801017f7:	83 c4 10             	add    $0x10,%esp
801017fa:	eb 30                	jmp    8010182c <ialloc+0xd5>
    }
    brelse(bp);
801017fc:	83 ec 0c             	sub    $0xc,%esp
801017ff:	ff 75 f0             	pushl  -0x10(%ebp)
80101802:	e8 27 ea ff ff       	call   8010022e <brelse>
80101807:	83 c4 10             	add    $0x10,%esp
{
  int inum;
  struct buf *bp;
  struct dinode *dip;

  for(inum = 1; inum < sb.ninodes; inum++){
8010180a:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
8010180e:	8b 15 48 22 11 80    	mov    0x80112248,%edx
80101814:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101817:	39 c2                	cmp    %eax,%edx
80101819:	0f 87 51 ff ff ff    	ja     80101770 <ialloc+0x19>
      brelse(bp);
      return iget(dev, inum);
    }
    brelse(bp);
  }
  panic("ialloc: no inodes");
8010181f:	83 ec 0c             	sub    $0xc,%esp
80101822:	68 2b 93 10 80       	push   $0x8010932b
80101827:	e8 3a ed ff ff       	call   80100566 <panic>
}
8010182c:	c9                   	leave  
8010182d:	c3                   	ret    

8010182e <iupdate>:

// Copy a modified in-memory inode to disk.
void
iupdate(struct inode *ip)
{
8010182e:	55                   	push   %ebp
8010182f:	89 e5                	mov    %esp,%ebp
80101831:	83 ec 18             	sub    $0x18,%esp
  struct buf *bp;
  struct dinode *dip;

  bp = bread(ip->dev, IBLOCK(ip->inum, sb));
80101834:	8b 45 08             	mov    0x8(%ebp),%eax
80101837:	8b 40 04             	mov    0x4(%eax),%eax
8010183a:	c1 e8 03             	shr    $0x3,%eax
8010183d:	89 c2                	mov    %eax,%edx
8010183f:	a1 54 22 11 80       	mov    0x80112254,%eax
80101844:	01 c2                	add    %eax,%edx
80101846:	8b 45 08             	mov    0x8(%ebp),%eax
80101849:	8b 00                	mov    (%eax),%eax
8010184b:	83 ec 08             	sub    $0x8,%esp
8010184e:	52                   	push   %edx
8010184f:	50                   	push   %eax
80101850:	e8 61 e9 ff ff       	call   801001b6 <bread>
80101855:	83 c4 10             	add    $0x10,%esp
80101858:	89 45 f4             	mov    %eax,-0xc(%ebp)
  dip = (struct dinode*)bp->data + ip->inum%IPB;
8010185b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010185e:	8d 50 18             	lea    0x18(%eax),%edx
80101861:	8b 45 08             	mov    0x8(%ebp),%eax
80101864:	8b 40 04             	mov    0x4(%eax),%eax
80101867:	83 e0 07             	and    $0x7,%eax
8010186a:	c1 e0 06             	shl    $0x6,%eax
8010186d:	01 d0                	add    %edx,%eax
8010186f:	89 45 f0             	mov    %eax,-0x10(%ebp)
  dip->type = ip->type;
80101872:	8b 45 08             	mov    0x8(%ebp),%eax
80101875:	0f b7 50 10          	movzwl 0x10(%eax),%edx
80101879:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010187c:	66 89 10             	mov    %dx,(%eax)
  dip->major = ip->major;
8010187f:	8b 45 08             	mov    0x8(%ebp),%eax
80101882:	0f b7 50 12          	movzwl 0x12(%eax),%edx
80101886:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101889:	66 89 50 02          	mov    %dx,0x2(%eax)
  dip->minor = ip->minor;
8010188d:	8b 45 08             	mov    0x8(%ebp),%eax
80101890:	0f b7 50 14          	movzwl 0x14(%eax),%edx
80101894:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101897:	66 89 50 04          	mov    %dx,0x4(%eax)
  dip->nlink = ip->nlink;
8010189b:	8b 45 08             	mov    0x8(%ebp),%eax
8010189e:	0f b7 50 16          	movzwl 0x16(%eax),%edx
801018a2:	8b 45 f0             	mov    -0x10(%ebp),%eax
801018a5:	66 89 50 06          	mov    %dx,0x6(%eax)
  dip->size = ip->size;
801018a9:	8b 45 08             	mov    0x8(%ebp),%eax
801018ac:	8b 50 18             	mov    0x18(%eax),%edx
801018af:	8b 45 f0             	mov    -0x10(%ebp),%eax
801018b2:	89 50 08             	mov    %edx,0x8(%eax)
  memmove(dip->addrs, ip->addrs, sizeof(ip->addrs));
801018b5:	8b 45 08             	mov    0x8(%ebp),%eax
801018b8:	8d 50 1c             	lea    0x1c(%eax),%edx
801018bb:	8b 45 f0             	mov    -0x10(%ebp),%eax
801018be:	83 c0 0c             	add    $0xc,%eax
801018c1:	83 ec 04             	sub    $0x4,%esp
801018c4:	6a 34                	push   $0x34
801018c6:	52                   	push   %edx
801018c7:	50                   	push   %eax
801018c8:	e8 9d 45 00 00       	call   80105e6a <memmove>
801018cd:	83 c4 10             	add    $0x10,%esp
  log_write(bp);
801018d0:	83 ec 0c             	sub    $0xc,%esp
801018d3:	ff 75 f4             	pushl  -0xc(%ebp)
801018d6:	e8 48 1f 00 00       	call   80103823 <log_write>
801018db:	83 c4 10             	add    $0x10,%esp
  brelse(bp);
801018de:	83 ec 0c             	sub    $0xc,%esp
801018e1:	ff 75 f4             	pushl  -0xc(%ebp)
801018e4:	e8 45 e9 ff ff       	call   8010022e <brelse>
801018e9:	83 c4 10             	add    $0x10,%esp
}
801018ec:	90                   	nop
801018ed:	c9                   	leave  
801018ee:	c3                   	ret    

801018ef <iget>:
// Find the inode with number inum on device dev
// and return the in-memory copy. Does not lock
// the inode and does not read it from disk.
static struct inode*
iget(uint dev, uint inum)
{
801018ef:	55                   	push   %ebp
801018f0:	89 e5                	mov    %esp,%ebp
801018f2:	83 ec 18             	sub    $0x18,%esp
  struct inode *ip, *empty;

  acquire(&icache.lock);
801018f5:	83 ec 0c             	sub    $0xc,%esp
801018f8:	68 60 22 11 80       	push   $0x80112260
801018fd:	e8 46 42 00 00       	call   80105b48 <acquire>
80101902:	83 c4 10             	add    $0x10,%esp

  // Is the inode already cached?
  empty = 0;
80101905:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
8010190c:	c7 45 f4 94 22 11 80 	movl   $0x80112294,-0xc(%ebp)
80101913:	eb 5d                	jmp    80101972 <iget+0x83>
    if(ip->ref > 0 && ip->dev == dev && ip->inum == inum){
80101915:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101918:	8b 40 08             	mov    0x8(%eax),%eax
8010191b:	85 c0                	test   %eax,%eax
8010191d:	7e 39                	jle    80101958 <iget+0x69>
8010191f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101922:	8b 00                	mov    (%eax),%eax
80101924:	3b 45 08             	cmp    0x8(%ebp),%eax
80101927:	75 2f                	jne    80101958 <iget+0x69>
80101929:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010192c:	8b 40 04             	mov    0x4(%eax),%eax
8010192f:	3b 45 0c             	cmp    0xc(%ebp),%eax
80101932:	75 24                	jne    80101958 <iget+0x69>
      ip->ref++;
80101934:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101937:	8b 40 08             	mov    0x8(%eax),%eax
8010193a:	8d 50 01             	lea    0x1(%eax),%edx
8010193d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101940:	89 50 08             	mov    %edx,0x8(%eax)
      release(&icache.lock);
80101943:	83 ec 0c             	sub    $0xc,%esp
80101946:	68 60 22 11 80       	push   $0x80112260
8010194b:	e8 5f 42 00 00       	call   80105baf <release>
80101950:	83 c4 10             	add    $0x10,%esp
      return ip;
80101953:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101956:	eb 74                	jmp    801019cc <iget+0xdd>
    }
    if(empty == 0 && ip->ref == 0)    // Remember empty slot.
80101958:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
8010195c:	75 10                	jne    8010196e <iget+0x7f>
8010195e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101961:	8b 40 08             	mov    0x8(%eax),%eax
80101964:	85 c0                	test   %eax,%eax
80101966:	75 06                	jne    8010196e <iget+0x7f>
      empty = ip;
80101968:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010196b:	89 45 f0             	mov    %eax,-0x10(%ebp)

  acquire(&icache.lock);

  // Is the inode already cached?
  empty = 0;
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
8010196e:	83 45 f4 50          	addl   $0x50,-0xc(%ebp)
80101972:	81 7d f4 34 32 11 80 	cmpl   $0x80113234,-0xc(%ebp)
80101979:	72 9a                	jb     80101915 <iget+0x26>
    if(empty == 0 && ip->ref == 0)    // Remember empty slot.
      empty = ip;
  }

  // Recycle an inode cache entry.
  if(empty == 0)
8010197b:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
8010197f:	75 0d                	jne    8010198e <iget+0x9f>
    panic("iget: no inodes");
80101981:	83 ec 0c             	sub    $0xc,%esp
80101984:	68 3d 93 10 80       	push   $0x8010933d
80101989:	e8 d8 eb ff ff       	call   80100566 <panic>

  ip = empty;
8010198e:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101991:	89 45 f4             	mov    %eax,-0xc(%ebp)
  ip->dev = dev;
80101994:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101997:	8b 55 08             	mov    0x8(%ebp),%edx
8010199a:	89 10                	mov    %edx,(%eax)
  ip->inum = inum;
8010199c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010199f:	8b 55 0c             	mov    0xc(%ebp),%edx
801019a2:	89 50 04             	mov    %edx,0x4(%eax)
  ip->ref = 1;
801019a5:	8b 45 f4             	mov    -0xc(%ebp),%eax
801019a8:	c7 40 08 01 00 00 00 	movl   $0x1,0x8(%eax)
  ip->flags = 0;
801019af:	8b 45 f4             	mov    -0xc(%ebp),%eax
801019b2:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
  release(&icache.lock);
801019b9:	83 ec 0c             	sub    $0xc,%esp
801019bc:	68 60 22 11 80       	push   $0x80112260
801019c1:	e8 e9 41 00 00       	call   80105baf <release>
801019c6:	83 c4 10             	add    $0x10,%esp

  return ip;
801019c9:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
801019cc:	c9                   	leave  
801019cd:	c3                   	ret    

801019ce <idup>:

// Increment reference count for ip.
// Returns ip to enable ip = idup(ip1) idiom.
struct inode*
idup(struct inode *ip)
{
801019ce:	55                   	push   %ebp
801019cf:	89 e5                	mov    %esp,%ebp
801019d1:	83 ec 08             	sub    $0x8,%esp
  acquire(&icache.lock);
801019d4:	83 ec 0c             	sub    $0xc,%esp
801019d7:	68 60 22 11 80       	push   $0x80112260
801019dc:	e8 67 41 00 00       	call   80105b48 <acquire>
801019e1:	83 c4 10             	add    $0x10,%esp
  ip->ref++;
801019e4:	8b 45 08             	mov    0x8(%ebp),%eax
801019e7:	8b 40 08             	mov    0x8(%eax),%eax
801019ea:	8d 50 01             	lea    0x1(%eax),%edx
801019ed:	8b 45 08             	mov    0x8(%ebp),%eax
801019f0:	89 50 08             	mov    %edx,0x8(%eax)
  release(&icache.lock);
801019f3:	83 ec 0c             	sub    $0xc,%esp
801019f6:	68 60 22 11 80       	push   $0x80112260
801019fb:	e8 af 41 00 00       	call   80105baf <release>
80101a00:	83 c4 10             	add    $0x10,%esp
  return ip;
80101a03:	8b 45 08             	mov    0x8(%ebp),%eax
}
80101a06:	c9                   	leave  
80101a07:	c3                   	ret    

80101a08 <ilock>:

// Lock the given inode.
// Reads the inode from disk if necessary.
void
ilock(struct inode *ip)
{
80101a08:	55                   	push   %ebp
80101a09:	89 e5                	mov    %esp,%ebp
80101a0b:	83 ec 18             	sub    $0x18,%esp
  struct buf *bp;
  struct dinode *dip;

  if(ip == 0 || ip->ref < 1)
80101a0e:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80101a12:	74 0a                	je     80101a1e <ilock+0x16>
80101a14:	8b 45 08             	mov    0x8(%ebp),%eax
80101a17:	8b 40 08             	mov    0x8(%eax),%eax
80101a1a:	85 c0                	test   %eax,%eax
80101a1c:	7f 0d                	jg     80101a2b <ilock+0x23>
    panic("ilock");
80101a1e:	83 ec 0c             	sub    $0xc,%esp
80101a21:	68 4d 93 10 80       	push   $0x8010934d
80101a26:	e8 3b eb ff ff       	call   80100566 <panic>

  acquire(&icache.lock);
80101a2b:	83 ec 0c             	sub    $0xc,%esp
80101a2e:	68 60 22 11 80       	push   $0x80112260
80101a33:	e8 10 41 00 00       	call   80105b48 <acquire>
80101a38:	83 c4 10             	add    $0x10,%esp
  while(ip->flags & I_BUSY)
80101a3b:	eb 13                	jmp    80101a50 <ilock+0x48>
    sleep(ip, &icache.lock);
80101a3d:	83 ec 08             	sub    $0x8,%esp
80101a40:	68 60 22 11 80       	push   $0x80112260
80101a45:	ff 75 08             	pushl  0x8(%ebp)
80101a48:	e8 17 39 00 00       	call   80105364 <sleep>
80101a4d:	83 c4 10             	add    $0x10,%esp

  if(ip == 0 || ip->ref < 1)
    panic("ilock");

  acquire(&icache.lock);
  while(ip->flags & I_BUSY)
80101a50:	8b 45 08             	mov    0x8(%ebp),%eax
80101a53:	8b 40 0c             	mov    0xc(%eax),%eax
80101a56:	83 e0 01             	and    $0x1,%eax
80101a59:	85 c0                	test   %eax,%eax
80101a5b:	75 e0                	jne    80101a3d <ilock+0x35>
    sleep(ip, &icache.lock);
  ip->flags |= I_BUSY;
80101a5d:	8b 45 08             	mov    0x8(%ebp),%eax
80101a60:	8b 40 0c             	mov    0xc(%eax),%eax
80101a63:	83 c8 01             	or     $0x1,%eax
80101a66:	89 c2                	mov    %eax,%edx
80101a68:	8b 45 08             	mov    0x8(%ebp),%eax
80101a6b:	89 50 0c             	mov    %edx,0xc(%eax)
  release(&icache.lock);
80101a6e:	83 ec 0c             	sub    $0xc,%esp
80101a71:	68 60 22 11 80       	push   $0x80112260
80101a76:	e8 34 41 00 00       	call   80105baf <release>
80101a7b:	83 c4 10             	add    $0x10,%esp

  if(!(ip->flags & I_VALID)){
80101a7e:	8b 45 08             	mov    0x8(%ebp),%eax
80101a81:	8b 40 0c             	mov    0xc(%eax),%eax
80101a84:	83 e0 02             	and    $0x2,%eax
80101a87:	85 c0                	test   %eax,%eax
80101a89:	0f 85 d4 00 00 00    	jne    80101b63 <ilock+0x15b>
    bp = bread(ip->dev, IBLOCK(ip->inum, sb));
80101a8f:	8b 45 08             	mov    0x8(%ebp),%eax
80101a92:	8b 40 04             	mov    0x4(%eax),%eax
80101a95:	c1 e8 03             	shr    $0x3,%eax
80101a98:	89 c2                	mov    %eax,%edx
80101a9a:	a1 54 22 11 80       	mov    0x80112254,%eax
80101a9f:	01 c2                	add    %eax,%edx
80101aa1:	8b 45 08             	mov    0x8(%ebp),%eax
80101aa4:	8b 00                	mov    (%eax),%eax
80101aa6:	83 ec 08             	sub    $0x8,%esp
80101aa9:	52                   	push   %edx
80101aaa:	50                   	push   %eax
80101aab:	e8 06 e7 ff ff       	call   801001b6 <bread>
80101ab0:	83 c4 10             	add    $0x10,%esp
80101ab3:	89 45 f4             	mov    %eax,-0xc(%ebp)
    dip = (struct dinode*)bp->data + ip->inum%IPB;
80101ab6:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101ab9:	8d 50 18             	lea    0x18(%eax),%edx
80101abc:	8b 45 08             	mov    0x8(%ebp),%eax
80101abf:	8b 40 04             	mov    0x4(%eax),%eax
80101ac2:	83 e0 07             	and    $0x7,%eax
80101ac5:	c1 e0 06             	shl    $0x6,%eax
80101ac8:	01 d0                	add    %edx,%eax
80101aca:	89 45 f0             	mov    %eax,-0x10(%ebp)
    ip->type = dip->type;
80101acd:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101ad0:	0f b7 10             	movzwl (%eax),%edx
80101ad3:	8b 45 08             	mov    0x8(%ebp),%eax
80101ad6:	66 89 50 10          	mov    %dx,0x10(%eax)
    ip->major = dip->major;
80101ada:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101add:	0f b7 50 02          	movzwl 0x2(%eax),%edx
80101ae1:	8b 45 08             	mov    0x8(%ebp),%eax
80101ae4:	66 89 50 12          	mov    %dx,0x12(%eax)
    ip->minor = dip->minor;
80101ae8:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101aeb:	0f b7 50 04          	movzwl 0x4(%eax),%edx
80101aef:	8b 45 08             	mov    0x8(%ebp),%eax
80101af2:	66 89 50 14          	mov    %dx,0x14(%eax)
    ip->nlink = dip->nlink;
80101af6:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101af9:	0f b7 50 06          	movzwl 0x6(%eax),%edx
80101afd:	8b 45 08             	mov    0x8(%ebp),%eax
80101b00:	66 89 50 16          	mov    %dx,0x16(%eax)
    ip->size = dip->size;
80101b04:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101b07:	8b 50 08             	mov    0x8(%eax),%edx
80101b0a:	8b 45 08             	mov    0x8(%ebp),%eax
80101b0d:	89 50 18             	mov    %edx,0x18(%eax)
    memmove(ip->addrs, dip->addrs, sizeof(ip->addrs));
80101b10:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101b13:	8d 50 0c             	lea    0xc(%eax),%edx
80101b16:	8b 45 08             	mov    0x8(%ebp),%eax
80101b19:	83 c0 1c             	add    $0x1c,%eax
80101b1c:	83 ec 04             	sub    $0x4,%esp
80101b1f:	6a 34                	push   $0x34
80101b21:	52                   	push   %edx
80101b22:	50                   	push   %eax
80101b23:	e8 42 43 00 00       	call   80105e6a <memmove>
80101b28:	83 c4 10             	add    $0x10,%esp
    brelse(bp);
80101b2b:	83 ec 0c             	sub    $0xc,%esp
80101b2e:	ff 75 f4             	pushl  -0xc(%ebp)
80101b31:	e8 f8 e6 ff ff       	call   8010022e <brelse>
80101b36:	83 c4 10             	add    $0x10,%esp
    ip->flags |= I_VALID;
80101b39:	8b 45 08             	mov    0x8(%ebp),%eax
80101b3c:	8b 40 0c             	mov    0xc(%eax),%eax
80101b3f:	83 c8 02             	or     $0x2,%eax
80101b42:	89 c2                	mov    %eax,%edx
80101b44:	8b 45 08             	mov    0x8(%ebp),%eax
80101b47:	89 50 0c             	mov    %edx,0xc(%eax)
    if(ip->type == 0)
80101b4a:	8b 45 08             	mov    0x8(%ebp),%eax
80101b4d:	0f b7 40 10          	movzwl 0x10(%eax),%eax
80101b51:	66 85 c0             	test   %ax,%ax
80101b54:	75 0d                	jne    80101b63 <ilock+0x15b>
      panic("ilock: no type");
80101b56:	83 ec 0c             	sub    $0xc,%esp
80101b59:	68 53 93 10 80       	push   $0x80109353
80101b5e:	e8 03 ea ff ff       	call   80100566 <panic>
  }
}
80101b63:	90                   	nop
80101b64:	c9                   	leave  
80101b65:	c3                   	ret    

80101b66 <iunlock>:

// Unlock the given inode.
void
iunlock(struct inode *ip)
{
80101b66:	55                   	push   %ebp
80101b67:	89 e5                	mov    %esp,%ebp
80101b69:	83 ec 08             	sub    $0x8,%esp
  if(ip == 0 || !(ip->flags & I_BUSY) || ip->ref < 1)
80101b6c:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80101b70:	74 17                	je     80101b89 <iunlock+0x23>
80101b72:	8b 45 08             	mov    0x8(%ebp),%eax
80101b75:	8b 40 0c             	mov    0xc(%eax),%eax
80101b78:	83 e0 01             	and    $0x1,%eax
80101b7b:	85 c0                	test   %eax,%eax
80101b7d:	74 0a                	je     80101b89 <iunlock+0x23>
80101b7f:	8b 45 08             	mov    0x8(%ebp),%eax
80101b82:	8b 40 08             	mov    0x8(%eax),%eax
80101b85:	85 c0                	test   %eax,%eax
80101b87:	7f 0d                	jg     80101b96 <iunlock+0x30>
    panic("iunlock");
80101b89:	83 ec 0c             	sub    $0xc,%esp
80101b8c:	68 62 93 10 80       	push   $0x80109362
80101b91:	e8 d0 e9 ff ff       	call   80100566 <panic>

  acquire(&icache.lock);
80101b96:	83 ec 0c             	sub    $0xc,%esp
80101b99:	68 60 22 11 80       	push   $0x80112260
80101b9e:	e8 a5 3f 00 00       	call   80105b48 <acquire>
80101ba3:	83 c4 10             	add    $0x10,%esp
  ip->flags &= ~I_BUSY;
80101ba6:	8b 45 08             	mov    0x8(%ebp),%eax
80101ba9:	8b 40 0c             	mov    0xc(%eax),%eax
80101bac:	83 e0 fe             	and    $0xfffffffe,%eax
80101baf:	89 c2                	mov    %eax,%edx
80101bb1:	8b 45 08             	mov    0x8(%ebp),%eax
80101bb4:	89 50 0c             	mov    %edx,0xc(%eax)
  wakeup(ip);
80101bb7:	83 ec 0c             	sub    $0xc,%esp
80101bba:	ff 75 08             	pushl  0x8(%ebp)
80101bbd:	e8 b7 38 00 00       	call   80105479 <wakeup>
80101bc2:	83 c4 10             	add    $0x10,%esp
  release(&icache.lock);
80101bc5:	83 ec 0c             	sub    $0xc,%esp
80101bc8:	68 60 22 11 80       	push   $0x80112260
80101bcd:	e8 dd 3f 00 00       	call   80105baf <release>
80101bd2:	83 c4 10             	add    $0x10,%esp
}
80101bd5:	90                   	nop
80101bd6:	c9                   	leave  
80101bd7:	c3                   	ret    

80101bd8 <iput>:
// to it, free the inode (and its content) on disk.
// All calls to iput() must be inside a transaction in
// case it has to free the inode.
void
iput(struct inode *ip)
{
80101bd8:	55                   	push   %ebp
80101bd9:	89 e5                	mov    %esp,%ebp
80101bdb:	83 ec 08             	sub    $0x8,%esp
  acquire(&icache.lock);
80101bde:	83 ec 0c             	sub    $0xc,%esp
80101be1:	68 60 22 11 80       	push   $0x80112260
80101be6:	e8 5d 3f 00 00       	call   80105b48 <acquire>
80101beb:	83 c4 10             	add    $0x10,%esp
  if(ip->ref == 1 && (ip->flags & I_VALID) && ip->nlink == 0){
80101bee:	8b 45 08             	mov    0x8(%ebp),%eax
80101bf1:	8b 40 08             	mov    0x8(%eax),%eax
80101bf4:	83 f8 01             	cmp    $0x1,%eax
80101bf7:	0f 85 a9 00 00 00    	jne    80101ca6 <iput+0xce>
80101bfd:	8b 45 08             	mov    0x8(%ebp),%eax
80101c00:	8b 40 0c             	mov    0xc(%eax),%eax
80101c03:	83 e0 02             	and    $0x2,%eax
80101c06:	85 c0                	test   %eax,%eax
80101c08:	0f 84 98 00 00 00    	je     80101ca6 <iput+0xce>
80101c0e:	8b 45 08             	mov    0x8(%ebp),%eax
80101c11:	0f b7 40 16          	movzwl 0x16(%eax),%eax
80101c15:	66 85 c0             	test   %ax,%ax
80101c18:	0f 85 88 00 00 00    	jne    80101ca6 <iput+0xce>
    // inode has no links and no other references: truncate and free.
    if(ip->flags & I_BUSY)
80101c1e:	8b 45 08             	mov    0x8(%ebp),%eax
80101c21:	8b 40 0c             	mov    0xc(%eax),%eax
80101c24:	83 e0 01             	and    $0x1,%eax
80101c27:	85 c0                	test   %eax,%eax
80101c29:	74 0d                	je     80101c38 <iput+0x60>
      panic("iput busy");
80101c2b:	83 ec 0c             	sub    $0xc,%esp
80101c2e:	68 6a 93 10 80       	push   $0x8010936a
80101c33:	e8 2e e9 ff ff       	call   80100566 <panic>
    ip->flags |= I_BUSY;
80101c38:	8b 45 08             	mov    0x8(%ebp),%eax
80101c3b:	8b 40 0c             	mov    0xc(%eax),%eax
80101c3e:	83 c8 01             	or     $0x1,%eax
80101c41:	89 c2                	mov    %eax,%edx
80101c43:	8b 45 08             	mov    0x8(%ebp),%eax
80101c46:	89 50 0c             	mov    %edx,0xc(%eax)
    release(&icache.lock);
80101c49:	83 ec 0c             	sub    $0xc,%esp
80101c4c:	68 60 22 11 80       	push   $0x80112260
80101c51:	e8 59 3f 00 00       	call   80105baf <release>
80101c56:	83 c4 10             	add    $0x10,%esp
    itrunc(ip);
80101c59:	83 ec 0c             	sub    $0xc,%esp
80101c5c:	ff 75 08             	pushl  0x8(%ebp)
80101c5f:	e8 a8 01 00 00       	call   80101e0c <itrunc>
80101c64:	83 c4 10             	add    $0x10,%esp
    ip->type = 0;
80101c67:	8b 45 08             	mov    0x8(%ebp),%eax
80101c6a:	66 c7 40 10 00 00    	movw   $0x0,0x10(%eax)
    iupdate(ip);
80101c70:	83 ec 0c             	sub    $0xc,%esp
80101c73:	ff 75 08             	pushl  0x8(%ebp)
80101c76:	e8 b3 fb ff ff       	call   8010182e <iupdate>
80101c7b:	83 c4 10             	add    $0x10,%esp
    acquire(&icache.lock);
80101c7e:	83 ec 0c             	sub    $0xc,%esp
80101c81:	68 60 22 11 80       	push   $0x80112260
80101c86:	e8 bd 3e 00 00       	call   80105b48 <acquire>
80101c8b:	83 c4 10             	add    $0x10,%esp
    ip->flags = 0;
80101c8e:	8b 45 08             	mov    0x8(%ebp),%eax
80101c91:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
    wakeup(ip);
80101c98:	83 ec 0c             	sub    $0xc,%esp
80101c9b:	ff 75 08             	pushl  0x8(%ebp)
80101c9e:	e8 d6 37 00 00       	call   80105479 <wakeup>
80101ca3:	83 c4 10             	add    $0x10,%esp
  }
  ip->ref--;
80101ca6:	8b 45 08             	mov    0x8(%ebp),%eax
80101ca9:	8b 40 08             	mov    0x8(%eax),%eax
80101cac:	8d 50 ff             	lea    -0x1(%eax),%edx
80101caf:	8b 45 08             	mov    0x8(%ebp),%eax
80101cb2:	89 50 08             	mov    %edx,0x8(%eax)
  release(&icache.lock);
80101cb5:	83 ec 0c             	sub    $0xc,%esp
80101cb8:	68 60 22 11 80       	push   $0x80112260
80101cbd:	e8 ed 3e 00 00       	call   80105baf <release>
80101cc2:	83 c4 10             	add    $0x10,%esp
}
80101cc5:	90                   	nop
80101cc6:	c9                   	leave  
80101cc7:	c3                   	ret    

80101cc8 <iunlockput>:

// Common idiom: unlock, then put.
void
iunlockput(struct inode *ip)
{
80101cc8:	55                   	push   %ebp
80101cc9:	89 e5                	mov    %esp,%ebp
80101ccb:	83 ec 08             	sub    $0x8,%esp
  iunlock(ip);
80101cce:	83 ec 0c             	sub    $0xc,%esp
80101cd1:	ff 75 08             	pushl  0x8(%ebp)
80101cd4:	e8 8d fe ff ff       	call   80101b66 <iunlock>
80101cd9:	83 c4 10             	add    $0x10,%esp
  iput(ip);
80101cdc:	83 ec 0c             	sub    $0xc,%esp
80101cdf:	ff 75 08             	pushl  0x8(%ebp)
80101ce2:	e8 f1 fe ff ff       	call   80101bd8 <iput>
80101ce7:	83 c4 10             	add    $0x10,%esp
}
80101cea:	90                   	nop
80101ceb:	c9                   	leave  
80101cec:	c3                   	ret    

80101ced <bmap>:

// Return the disk block address of the nth block in inode ip.
// If there is no such block, bmap allocates one.
static uint
bmap(struct inode *ip, uint bn)
{
80101ced:	55                   	push   %ebp
80101cee:	89 e5                	mov    %esp,%ebp
80101cf0:	53                   	push   %ebx
80101cf1:	83 ec 14             	sub    $0x14,%esp
  uint addr, *a;
  struct buf *bp;

  if(bn < NDIRECT){
80101cf4:	83 7d 0c 0b          	cmpl   $0xb,0xc(%ebp)
80101cf8:	77 42                	ja     80101d3c <bmap+0x4f>
    if((addr = ip->addrs[bn]) == 0)
80101cfa:	8b 45 08             	mov    0x8(%ebp),%eax
80101cfd:	8b 55 0c             	mov    0xc(%ebp),%edx
80101d00:	83 c2 04             	add    $0x4,%edx
80101d03:	8b 44 90 0c          	mov    0xc(%eax,%edx,4),%eax
80101d07:	89 45 f4             	mov    %eax,-0xc(%ebp)
80101d0a:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80101d0e:	75 24                	jne    80101d34 <bmap+0x47>
      ip->addrs[bn] = addr = balloc(ip->dev);
80101d10:	8b 45 08             	mov    0x8(%ebp),%eax
80101d13:	8b 00                	mov    (%eax),%eax
80101d15:	83 ec 0c             	sub    $0xc,%esp
80101d18:	50                   	push   %eax
80101d19:	e8 9a f7 ff ff       	call   801014b8 <balloc>
80101d1e:	83 c4 10             	add    $0x10,%esp
80101d21:	89 45 f4             	mov    %eax,-0xc(%ebp)
80101d24:	8b 45 08             	mov    0x8(%ebp),%eax
80101d27:	8b 55 0c             	mov    0xc(%ebp),%edx
80101d2a:	8d 4a 04             	lea    0x4(%edx),%ecx
80101d2d:	8b 55 f4             	mov    -0xc(%ebp),%edx
80101d30:	89 54 88 0c          	mov    %edx,0xc(%eax,%ecx,4)
    return addr;
80101d34:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101d37:	e9 cb 00 00 00       	jmp    80101e07 <bmap+0x11a>
  }
  bn -= NDIRECT;
80101d3c:	83 6d 0c 0c          	subl   $0xc,0xc(%ebp)

  if(bn < NINDIRECT){
80101d40:	83 7d 0c 7f          	cmpl   $0x7f,0xc(%ebp)
80101d44:	0f 87 b0 00 00 00    	ja     80101dfa <bmap+0x10d>
    // Load indirect block, allocating if necessary.
    if((addr = ip->addrs[NDIRECT]) == 0)
80101d4a:	8b 45 08             	mov    0x8(%ebp),%eax
80101d4d:	8b 40 4c             	mov    0x4c(%eax),%eax
80101d50:	89 45 f4             	mov    %eax,-0xc(%ebp)
80101d53:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80101d57:	75 1d                	jne    80101d76 <bmap+0x89>
      ip->addrs[NDIRECT] = addr = balloc(ip->dev);
80101d59:	8b 45 08             	mov    0x8(%ebp),%eax
80101d5c:	8b 00                	mov    (%eax),%eax
80101d5e:	83 ec 0c             	sub    $0xc,%esp
80101d61:	50                   	push   %eax
80101d62:	e8 51 f7 ff ff       	call   801014b8 <balloc>
80101d67:	83 c4 10             	add    $0x10,%esp
80101d6a:	89 45 f4             	mov    %eax,-0xc(%ebp)
80101d6d:	8b 45 08             	mov    0x8(%ebp),%eax
80101d70:	8b 55 f4             	mov    -0xc(%ebp),%edx
80101d73:	89 50 4c             	mov    %edx,0x4c(%eax)
    bp = bread(ip->dev, addr);
80101d76:	8b 45 08             	mov    0x8(%ebp),%eax
80101d79:	8b 00                	mov    (%eax),%eax
80101d7b:	83 ec 08             	sub    $0x8,%esp
80101d7e:	ff 75 f4             	pushl  -0xc(%ebp)
80101d81:	50                   	push   %eax
80101d82:	e8 2f e4 ff ff       	call   801001b6 <bread>
80101d87:	83 c4 10             	add    $0x10,%esp
80101d8a:	89 45 f0             	mov    %eax,-0x10(%ebp)
    a = (uint*)bp->data;
80101d8d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101d90:	83 c0 18             	add    $0x18,%eax
80101d93:	89 45 ec             	mov    %eax,-0x14(%ebp)
    if((addr = a[bn]) == 0){
80101d96:	8b 45 0c             	mov    0xc(%ebp),%eax
80101d99:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
80101da0:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101da3:	01 d0                	add    %edx,%eax
80101da5:	8b 00                	mov    (%eax),%eax
80101da7:	89 45 f4             	mov    %eax,-0xc(%ebp)
80101daa:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80101dae:	75 37                	jne    80101de7 <bmap+0xfa>
      a[bn] = addr = balloc(ip->dev);
80101db0:	8b 45 0c             	mov    0xc(%ebp),%eax
80101db3:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
80101dba:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101dbd:	8d 1c 02             	lea    (%edx,%eax,1),%ebx
80101dc0:	8b 45 08             	mov    0x8(%ebp),%eax
80101dc3:	8b 00                	mov    (%eax),%eax
80101dc5:	83 ec 0c             	sub    $0xc,%esp
80101dc8:	50                   	push   %eax
80101dc9:	e8 ea f6 ff ff       	call   801014b8 <balloc>
80101dce:	83 c4 10             	add    $0x10,%esp
80101dd1:	89 45 f4             	mov    %eax,-0xc(%ebp)
80101dd4:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101dd7:	89 03                	mov    %eax,(%ebx)
      log_write(bp);
80101dd9:	83 ec 0c             	sub    $0xc,%esp
80101ddc:	ff 75 f0             	pushl  -0x10(%ebp)
80101ddf:	e8 3f 1a 00 00       	call   80103823 <log_write>
80101de4:	83 c4 10             	add    $0x10,%esp
    }
    brelse(bp);
80101de7:	83 ec 0c             	sub    $0xc,%esp
80101dea:	ff 75 f0             	pushl  -0x10(%ebp)
80101ded:	e8 3c e4 ff ff       	call   8010022e <brelse>
80101df2:	83 c4 10             	add    $0x10,%esp
    return addr;
80101df5:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101df8:	eb 0d                	jmp    80101e07 <bmap+0x11a>
  }

  panic("bmap: out of range");
80101dfa:	83 ec 0c             	sub    $0xc,%esp
80101dfd:	68 74 93 10 80       	push   $0x80109374
80101e02:	e8 5f e7 ff ff       	call   80100566 <panic>
}
80101e07:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80101e0a:	c9                   	leave  
80101e0b:	c3                   	ret    

80101e0c <itrunc>:
// to it (no directory entries referring to it)
// and has no in-memory reference to it (is
// not an open file or current directory).
static void
itrunc(struct inode *ip)
{
80101e0c:	55                   	push   %ebp
80101e0d:	89 e5                	mov    %esp,%ebp
80101e0f:	83 ec 18             	sub    $0x18,%esp
  int i, j;
  struct buf *bp;
  uint *a;

  for(i = 0; i < NDIRECT; i++){
80101e12:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80101e19:	eb 45                	jmp    80101e60 <itrunc+0x54>
    if(ip->addrs[i]){
80101e1b:	8b 45 08             	mov    0x8(%ebp),%eax
80101e1e:	8b 55 f4             	mov    -0xc(%ebp),%edx
80101e21:	83 c2 04             	add    $0x4,%edx
80101e24:	8b 44 90 0c          	mov    0xc(%eax,%edx,4),%eax
80101e28:	85 c0                	test   %eax,%eax
80101e2a:	74 30                	je     80101e5c <itrunc+0x50>
      bfree(ip->dev, ip->addrs[i]);
80101e2c:	8b 45 08             	mov    0x8(%ebp),%eax
80101e2f:	8b 55 f4             	mov    -0xc(%ebp),%edx
80101e32:	83 c2 04             	add    $0x4,%edx
80101e35:	8b 44 90 0c          	mov    0xc(%eax,%edx,4),%eax
80101e39:	8b 55 08             	mov    0x8(%ebp),%edx
80101e3c:	8b 12                	mov    (%edx),%edx
80101e3e:	83 ec 08             	sub    $0x8,%esp
80101e41:	50                   	push   %eax
80101e42:	52                   	push   %edx
80101e43:	e8 bc f7 ff ff       	call   80101604 <bfree>
80101e48:	83 c4 10             	add    $0x10,%esp
      ip->addrs[i] = 0;
80101e4b:	8b 45 08             	mov    0x8(%ebp),%eax
80101e4e:	8b 55 f4             	mov    -0xc(%ebp),%edx
80101e51:	83 c2 04             	add    $0x4,%edx
80101e54:	c7 44 90 0c 00 00 00 	movl   $0x0,0xc(%eax,%edx,4)
80101e5b:	00 
{
  int i, j;
  struct buf *bp;
  uint *a;

  for(i = 0; i < NDIRECT; i++){
80101e5c:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80101e60:	83 7d f4 0b          	cmpl   $0xb,-0xc(%ebp)
80101e64:	7e b5                	jle    80101e1b <itrunc+0xf>
      bfree(ip->dev, ip->addrs[i]);
      ip->addrs[i] = 0;
    }
  }
  
  if(ip->addrs[NDIRECT]){
80101e66:	8b 45 08             	mov    0x8(%ebp),%eax
80101e69:	8b 40 4c             	mov    0x4c(%eax),%eax
80101e6c:	85 c0                	test   %eax,%eax
80101e6e:	0f 84 a1 00 00 00    	je     80101f15 <itrunc+0x109>
    bp = bread(ip->dev, ip->addrs[NDIRECT]);
80101e74:	8b 45 08             	mov    0x8(%ebp),%eax
80101e77:	8b 50 4c             	mov    0x4c(%eax),%edx
80101e7a:	8b 45 08             	mov    0x8(%ebp),%eax
80101e7d:	8b 00                	mov    (%eax),%eax
80101e7f:	83 ec 08             	sub    $0x8,%esp
80101e82:	52                   	push   %edx
80101e83:	50                   	push   %eax
80101e84:	e8 2d e3 ff ff       	call   801001b6 <bread>
80101e89:	83 c4 10             	add    $0x10,%esp
80101e8c:	89 45 ec             	mov    %eax,-0x14(%ebp)
    a = (uint*)bp->data;
80101e8f:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101e92:	83 c0 18             	add    $0x18,%eax
80101e95:	89 45 e8             	mov    %eax,-0x18(%ebp)
    for(j = 0; j < NINDIRECT; j++){
80101e98:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
80101e9f:	eb 3c                	jmp    80101edd <itrunc+0xd1>
      if(a[j])
80101ea1:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101ea4:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
80101eab:	8b 45 e8             	mov    -0x18(%ebp),%eax
80101eae:	01 d0                	add    %edx,%eax
80101eb0:	8b 00                	mov    (%eax),%eax
80101eb2:	85 c0                	test   %eax,%eax
80101eb4:	74 23                	je     80101ed9 <itrunc+0xcd>
        bfree(ip->dev, a[j]);
80101eb6:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101eb9:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
80101ec0:	8b 45 e8             	mov    -0x18(%ebp),%eax
80101ec3:	01 d0                	add    %edx,%eax
80101ec5:	8b 00                	mov    (%eax),%eax
80101ec7:	8b 55 08             	mov    0x8(%ebp),%edx
80101eca:	8b 12                	mov    (%edx),%edx
80101ecc:	83 ec 08             	sub    $0x8,%esp
80101ecf:	50                   	push   %eax
80101ed0:	52                   	push   %edx
80101ed1:	e8 2e f7 ff ff       	call   80101604 <bfree>
80101ed6:	83 c4 10             	add    $0x10,%esp
  }
  
  if(ip->addrs[NDIRECT]){
    bp = bread(ip->dev, ip->addrs[NDIRECT]);
    a = (uint*)bp->data;
    for(j = 0; j < NINDIRECT; j++){
80101ed9:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
80101edd:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101ee0:	83 f8 7f             	cmp    $0x7f,%eax
80101ee3:	76 bc                	jbe    80101ea1 <itrunc+0x95>
      if(a[j])
        bfree(ip->dev, a[j]);
    }
    brelse(bp);
80101ee5:	83 ec 0c             	sub    $0xc,%esp
80101ee8:	ff 75 ec             	pushl  -0x14(%ebp)
80101eeb:	e8 3e e3 ff ff       	call   8010022e <brelse>
80101ef0:	83 c4 10             	add    $0x10,%esp
    bfree(ip->dev, ip->addrs[NDIRECT]);
80101ef3:	8b 45 08             	mov    0x8(%ebp),%eax
80101ef6:	8b 40 4c             	mov    0x4c(%eax),%eax
80101ef9:	8b 55 08             	mov    0x8(%ebp),%edx
80101efc:	8b 12                	mov    (%edx),%edx
80101efe:	83 ec 08             	sub    $0x8,%esp
80101f01:	50                   	push   %eax
80101f02:	52                   	push   %edx
80101f03:	e8 fc f6 ff ff       	call   80101604 <bfree>
80101f08:	83 c4 10             	add    $0x10,%esp
    ip->addrs[NDIRECT] = 0;
80101f0b:	8b 45 08             	mov    0x8(%ebp),%eax
80101f0e:	c7 40 4c 00 00 00 00 	movl   $0x0,0x4c(%eax)
  }

  ip->size = 0;
80101f15:	8b 45 08             	mov    0x8(%ebp),%eax
80101f18:	c7 40 18 00 00 00 00 	movl   $0x0,0x18(%eax)
  iupdate(ip);
80101f1f:	83 ec 0c             	sub    $0xc,%esp
80101f22:	ff 75 08             	pushl  0x8(%ebp)
80101f25:	e8 04 f9 ff ff       	call   8010182e <iupdate>
80101f2a:	83 c4 10             	add    $0x10,%esp
}
80101f2d:	90                   	nop
80101f2e:	c9                   	leave  
80101f2f:	c3                   	ret    

80101f30 <stati>:

// Copy stat information from inode.
void
stati(struct inode *ip, struct stat *st)
{
80101f30:	55                   	push   %ebp
80101f31:	89 e5                	mov    %esp,%ebp
  st->dev = ip->dev;
80101f33:	8b 45 08             	mov    0x8(%ebp),%eax
80101f36:	8b 00                	mov    (%eax),%eax
80101f38:	89 c2                	mov    %eax,%edx
80101f3a:	8b 45 0c             	mov    0xc(%ebp),%eax
80101f3d:	89 50 04             	mov    %edx,0x4(%eax)
  st->ino = ip->inum;
80101f40:	8b 45 08             	mov    0x8(%ebp),%eax
80101f43:	8b 50 04             	mov    0x4(%eax),%edx
80101f46:	8b 45 0c             	mov    0xc(%ebp),%eax
80101f49:	89 50 08             	mov    %edx,0x8(%eax)
  st->type = ip->type;
80101f4c:	8b 45 08             	mov    0x8(%ebp),%eax
80101f4f:	0f b7 50 10          	movzwl 0x10(%eax),%edx
80101f53:	8b 45 0c             	mov    0xc(%ebp),%eax
80101f56:	66 89 10             	mov    %dx,(%eax)
  st->nlink = ip->nlink;
80101f59:	8b 45 08             	mov    0x8(%ebp),%eax
80101f5c:	0f b7 50 16          	movzwl 0x16(%eax),%edx
80101f60:	8b 45 0c             	mov    0xc(%ebp),%eax
80101f63:	66 89 50 0c          	mov    %dx,0xc(%eax)
  st->size = ip->size;
80101f67:	8b 45 08             	mov    0x8(%ebp),%eax
80101f6a:	8b 50 18             	mov    0x18(%eax),%edx
80101f6d:	8b 45 0c             	mov    0xc(%ebp),%eax
80101f70:	89 50 10             	mov    %edx,0x10(%eax)
}
80101f73:	90                   	nop
80101f74:	5d                   	pop    %ebp
80101f75:	c3                   	ret    

80101f76 <readi>:

// Read data from inode.
int
readi(struct inode *ip, char *dst, uint off, uint n)
{
80101f76:	55                   	push   %ebp
80101f77:	89 e5                	mov    %esp,%ebp
80101f79:	83 ec 18             	sub    $0x18,%esp
  uint tot, m;
  struct buf *bp;

  if(ip->type == T_DEV){
80101f7c:	8b 45 08             	mov    0x8(%ebp),%eax
80101f7f:	0f b7 40 10          	movzwl 0x10(%eax),%eax
80101f83:	66 83 f8 03          	cmp    $0x3,%ax
80101f87:	75 5c                	jne    80101fe5 <readi+0x6f>
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].read)
80101f89:	8b 45 08             	mov    0x8(%ebp),%eax
80101f8c:	0f b7 40 12          	movzwl 0x12(%eax),%eax
80101f90:	66 85 c0             	test   %ax,%ax
80101f93:	78 20                	js     80101fb5 <readi+0x3f>
80101f95:	8b 45 08             	mov    0x8(%ebp),%eax
80101f98:	0f b7 40 12          	movzwl 0x12(%eax),%eax
80101f9c:	66 83 f8 09          	cmp    $0x9,%ax
80101fa0:	7f 13                	jg     80101fb5 <readi+0x3f>
80101fa2:	8b 45 08             	mov    0x8(%ebp),%eax
80101fa5:	0f b7 40 12          	movzwl 0x12(%eax),%eax
80101fa9:	98                   	cwtl   
80101faa:	8b 04 c5 e0 21 11 80 	mov    -0x7feede20(,%eax,8),%eax
80101fb1:	85 c0                	test   %eax,%eax
80101fb3:	75 0a                	jne    80101fbf <readi+0x49>
      return -1;
80101fb5:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80101fba:	e9 0c 01 00 00       	jmp    801020cb <readi+0x155>
    return devsw[ip->major].read(ip, dst, n);
80101fbf:	8b 45 08             	mov    0x8(%ebp),%eax
80101fc2:	0f b7 40 12          	movzwl 0x12(%eax),%eax
80101fc6:	98                   	cwtl   
80101fc7:	8b 04 c5 e0 21 11 80 	mov    -0x7feede20(,%eax,8),%eax
80101fce:	8b 55 14             	mov    0x14(%ebp),%edx
80101fd1:	83 ec 04             	sub    $0x4,%esp
80101fd4:	52                   	push   %edx
80101fd5:	ff 75 0c             	pushl  0xc(%ebp)
80101fd8:	ff 75 08             	pushl  0x8(%ebp)
80101fdb:	ff d0                	call   *%eax
80101fdd:	83 c4 10             	add    $0x10,%esp
80101fe0:	e9 e6 00 00 00       	jmp    801020cb <readi+0x155>
  }

  if(off > ip->size || off + n < off)
80101fe5:	8b 45 08             	mov    0x8(%ebp),%eax
80101fe8:	8b 40 18             	mov    0x18(%eax),%eax
80101feb:	3b 45 10             	cmp    0x10(%ebp),%eax
80101fee:	72 0d                	jb     80101ffd <readi+0x87>
80101ff0:	8b 55 10             	mov    0x10(%ebp),%edx
80101ff3:	8b 45 14             	mov    0x14(%ebp),%eax
80101ff6:	01 d0                	add    %edx,%eax
80101ff8:	3b 45 10             	cmp    0x10(%ebp),%eax
80101ffb:	73 0a                	jae    80102007 <readi+0x91>
    return -1;
80101ffd:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80102002:	e9 c4 00 00 00       	jmp    801020cb <readi+0x155>
  if(off + n > ip->size)
80102007:	8b 55 10             	mov    0x10(%ebp),%edx
8010200a:	8b 45 14             	mov    0x14(%ebp),%eax
8010200d:	01 c2                	add    %eax,%edx
8010200f:	8b 45 08             	mov    0x8(%ebp),%eax
80102012:	8b 40 18             	mov    0x18(%eax),%eax
80102015:	39 c2                	cmp    %eax,%edx
80102017:	76 0c                	jbe    80102025 <readi+0xaf>
    n = ip->size - off;
80102019:	8b 45 08             	mov    0x8(%ebp),%eax
8010201c:	8b 40 18             	mov    0x18(%eax),%eax
8010201f:	2b 45 10             	sub    0x10(%ebp),%eax
80102022:	89 45 14             	mov    %eax,0x14(%ebp)

  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
80102025:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
8010202c:	e9 8b 00 00 00       	jmp    801020bc <readi+0x146>
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
80102031:	8b 45 10             	mov    0x10(%ebp),%eax
80102034:	c1 e8 09             	shr    $0x9,%eax
80102037:	83 ec 08             	sub    $0x8,%esp
8010203a:	50                   	push   %eax
8010203b:	ff 75 08             	pushl  0x8(%ebp)
8010203e:	e8 aa fc ff ff       	call   80101ced <bmap>
80102043:	83 c4 10             	add    $0x10,%esp
80102046:	89 c2                	mov    %eax,%edx
80102048:	8b 45 08             	mov    0x8(%ebp),%eax
8010204b:	8b 00                	mov    (%eax),%eax
8010204d:	83 ec 08             	sub    $0x8,%esp
80102050:	52                   	push   %edx
80102051:	50                   	push   %eax
80102052:	e8 5f e1 ff ff       	call   801001b6 <bread>
80102057:	83 c4 10             	add    $0x10,%esp
8010205a:	89 45 f0             	mov    %eax,-0x10(%ebp)
    m = min(n - tot, BSIZE - off%BSIZE);
8010205d:	8b 45 10             	mov    0x10(%ebp),%eax
80102060:	25 ff 01 00 00       	and    $0x1ff,%eax
80102065:	ba 00 02 00 00       	mov    $0x200,%edx
8010206a:	29 c2                	sub    %eax,%edx
8010206c:	8b 45 14             	mov    0x14(%ebp),%eax
8010206f:	2b 45 f4             	sub    -0xc(%ebp),%eax
80102072:	39 c2                	cmp    %eax,%edx
80102074:	0f 46 c2             	cmovbe %edx,%eax
80102077:	89 45 ec             	mov    %eax,-0x14(%ebp)
    memmove(dst, bp->data + off%BSIZE, m);
8010207a:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010207d:	8d 50 18             	lea    0x18(%eax),%edx
80102080:	8b 45 10             	mov    0x10(%ebp),%eax
80102083:	25 ff 01 00 00       	and    $0x1ff,%eax
80102088:	01 d0                	add    %edx,%eax
8010208a:	83 ec 04             	sub    $0x4,%esp
8010208d:	ff 75 ec             	pushl  -0x14(%ebp)
80102090:	50                   	push   %eax
80102091:	ff 75 0c             	pushl  0xc(%ebp)
80102094:	e8 d1 3d 00 00       	call   80105e6a <memmove>
80102099:	83 c4 10             	add    $0x10,%esp
    brelse(bp);
8010209c:	83 ec 0c             	sub    $0xc,%esp
8010209f:	ff 75 f0             	pushl  -0x10(%ebp)
801020a2:	e8 87 e1 ff ff       	call   8010022e <brelse>
801020a7:	83 c4 10             	add    $0x10,%esp
  if(off > ip->size || off + n < off)
    return -1;
  if(off + n > ip->size)
    n = ip->size - off;

  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
801020aa:	8b 45 ec             	mov    -0x14(%ebp),%eax
801020ad:	01 45 f4             	add    %eax,-0xc(%ebp)
801020b0:	8b 45 ec             	mov    -0x14(%ebp),%eax
801020b3:	01 45 10             	add    %eax,0x10(%ebp)
801020b6:	8b 45 ec             	mov    -0x14(%ebp),%eax
801020b9:	01 45 0c             	add    %eax,0xc(%ebp)
801020bc:	8b 45 f4             	mov    -0xc(%ebp),%eax
801020bf:	3b 45 14             	cmp    0x14(%ebp),%eax
801020c2:	0f 82 69 ff ff ff    	jb     80102031 <readi+0xbb>
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
    m = min(n - tot, BSIZE - off%BSIZE);
    memmove(dst, bp->data + off%BSIZE, m);
    brelse(bp);
  }
  return n;
801020c8:	8b 45 14             	mov    0x14(%ebp),%eax
}
801020cb:	c9                   	leave  
801020cc:	c3                   	ret    

801020cd <writei>:

// Write data to inode.
int
writei(struct inode *ip, char *src, uint off, uint n)
{
801020cd:	55                   	push   %ebp
801020ce:	89 e5                	mov    %esp,%ebp
801020d0:	83 ec 18             	sub    $0x18,%esp
  uint tot, m;
  struct buf *bp;

  if(ip->type == T_DEV){
801020d3:	8b 45 08             	mov    0x8(%ebp),%eax
801020d6:	0f b7 40 10          	movzwl 0x10(%eax),%eax
801020da:	66 83 f8 03          	cmp    $0x3,%ax
801020de:	75 5c                	jne    8010213c <writei+0x6f>
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].write)
801020e0:	8b 45 08             	mov    0x8(%ebp),%eax
801020e3:	0f b7 40 12          	movzwl 0x12(%eax),%eax
801020e7:	66 85 c0             	test   %ax,%ax
801020ea:	78 20                	js     8010210c <writei+0x3f>
801020ec:	8b 45 08             	mov    0x8(%ebp),%eax
801020ef:	0f b7 40 12          	movzwl 0x12(%eax),%eax
801020f3:	66 83 f8 09          	cmp    $0x9,%ax
801020f7:	7f 13                	jg     8010210c <writei+0x3f>
801020f9:	8b 45 08             	mov    0x8(%ebp),%eax
801020fc:	0f b7 40 12          	movzwl 0x12(%eax),%eax
80102100:	98                   	cwtl   
80102101:	8b 04 c5 e4 21 11 80 	mov    -0x7feede1c(,%eax,8),%eax
80102108:	85 c0                	test   %eax,%eax
8010210a:	75 0a                	jne    80102116 <writei+0x49>
      return -1;
8010210c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80102111:	e9 3d 01 00 00       	jmp    80102253 <writei+0x186>
    return devsw[ip->major].write(ip, src, n);
80102116:	8b 45 08             	mov    0x8(%ebp),%eax
80102119:	0f b7 40 12          	movzwl 0x12(%eax),%eax
8010211d:	98                   	cwtl   
8010211e:	8b 04 c5 e4 21 11 80 	mov    -0x7feede1c(,%eax,8),%eax
80102125:	8b 55 14             	mov    0x14(%ebp),%edx
80102128:	83 ec 04             	sub    $0x4,%esp
8010212b:	52                   	push   %edx
8010212c:	ff 75 0c             	pushl  0xc(%ebp)
8010212f:	ff 75 08             	pushl  0x8(%ebp)
80102132:	ff d0                	call   *%eax
80102134:	83 c4 10             	add    $0x10,%esp
80102137:	e9 17 01 00 00       	jmp    80102253 <writei+0x186>
  }

  if(off > ip->size || off + n < off)
8010213c:	8b 45 08             	mov    0x8(%ebp),%eax
8010213f:	8b 40 18             	mov    0x18(%eax),%eax
80102142:	3b 45 10             	cmp    0x10(%ebp),%eax
80102145:	72 0d                	jb     80102154 <writei+0x87>
80102147:	8b 55 10             	mov    0x10(%ebp),%edx
8010214a:	8b 45 14             	mov    0x14(%ebp),%eax
8010214d:	01 d0                	add    %edx,%eax
8010214f:	3b 45 10             	cmp    0x10(%ebp),%eax
80102152:	73 0a                	jae    8010215e <writei+0x91>
    return -1;
80102154:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80102159:	e9 f5 00 00 00       	jmp    80102253 <writei+0x186>
  if(off + n > MAXFILE*BSIZE)
8010215e:	8b 55 10             	mov    0x10(%ebp),%edx
80102161:	8b 45 14             	mov    0x14(%ebp),%eax
80102164:	01 d0                	add    %edx,%eax
80102166:	3d 00 18 01 00       	cmp    $0x11800,%eax
8010216b:	76 0a                	jbe    80102177 <writei+0xaa>
    return -1;
8010216d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80102172:	e9 dc 00 00 00       	jmp    80102253 <writei+0x186>

  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
80102177:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
8010217e:	e9 99 00 00 00       	jmp    8010221c <writei+0x14f>
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
80102183:	8b 45 10             	mov    0x10(%ebp),%eax
80102186:	c1 e8 09             	shr    $0x9,%eax
80102189:	83 ec 08             	sub    $0x8,%esp
8010218c:	50                   	push   %eax
8010218d:	ff 75 08             	pushl  0x8(%ebp)
80102190:	e8 58 fb ff ff       	call   80101ced <bmap>
80102195:	83 c4 10             	add    $0x10,%esp
80102198:	89 c2                	mov    %eax,%edx
8010219a:	8b 45 08             	mov    0x8(%ebp),%eax
8010219d:	8b 00                	mov    (%eax),%eax
8010219f:	83 ec 08             	sub    $0x8,%esp
801021a2:	52                   	push   %edx
801021a3:	50                   	push   %eax
801021a4:	e8 0d e0 ff ff       	call   801001b6 <bread>
801021a9:	83 c4 10             	add    $0x10,%esp
801021ac:	89 45 f0             	mov    %eax,-0x10(%ebp)
    m = min(n - tot, BSIZE - off%BSIZE);
801021af:	8b 45 10             	mov    0x10(%ebp),%eax
801021b2:	25 ff 01 00 00       	and    $0x1ff,%eax
801021b7:	ba 00 02 00 00       	mov    $0x200,%edx
801021bc:	29 c2                	sub    %eax,%edx
801021be:	8b 45 14             	mov    0x14(%ebp),%eax
801021c1:	2b 45 f4             	sub    -0xc(%ebp),%eax
801021c4:	39 c2                	cmp    %eax,%edx
801021c6:	0f 46 c2             	cmovbe %edx,%eax
801021c9:	89 45 ec             	mov    %eax,-0x14(%ebp)
    memmove(bp->data + off%BSIZE, src, m);
801021cc:	8b 45 f0             	mov    -0x10(%ebp),%eax
801021cf:	8d 50 18             	lea    0x18(%eax),%edx
801021d2:	8b 45 10             	mov    0x10(%ebp),%eax
801021d5:	25 ff 01 00 00       	and    $0x1ff,%eax
801021da:	01 d0                	add    %edx,%eax
801021dc:	83 ec 04             	sub    $0x4,%esp
801021df:	ff 75 ec             	pushl  -0x14(%ebp)
801021e2:	ff 75 0c             	pushl  0xc(%ebp)
801021e5:	50                   	push   %eax
801021e6:	e8 7f 3c 00 00       	call   80105e6a <memmove>
801021eb:	83 c4 10             	add    $0x10,%esp
    log_write(bp);
801021ee:	83 ec 0c             	sub    $0xc,%esp
801021f1:	ff 75 f0             	pushl  -0x10(%ebp)
801021f4:	e8 2a 16 00 00       	call   80103823 <log_write>
801021f9:	83 c4 10             	add    $0x10,%esp
    brelse(bp);
801021fc:	83 ec 0c             	sub    $0xc,%esp
801021ff:	ff 75 f0             	pushl  -0x10(%ebp)
80102202:	e8 27 e0 ff ff       	call   8010022e <brelse>
80102207:	83 c4 10             	add    $0x10,%esp
  if(off > ip->size || off + n < off)
    return -1;
  if(off + n > MAXFILE*BSIZE)
    return -1;

  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
8010220a:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010220d:	01 45 f4             	add    %eax,-0xc(%ebp)
80102210:	8b 45 ec             	mov    -0x14(%ebp),%eax
80102213:	01 45 10             	add    %eax,0x10(%ebp)
80102216:	8b 45 ec             	mov    -0x14(%ebp),%eax
80102219:	01 45 0c             	add    %eax,0xc(%ebp)
8010221c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010221f:	3b 45 14             	cmp    0x14(%ebp),%eax
80102222:	0f 82 5b ff ff ff    	jb     80102183 <writei+0xb6>
    memmove(bp->data + off%BSIZE, src, m);
    log_write(bp);
    brelse(bp);
  }

  if(n > 0 && off > ip->size){
80102228:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
8010222c:	74 22                	je     80102250 <writei+0x183>
8010222e:	8b 45 08             	mov    0x8(%ebp),%eax
80102231:	8b 40 18             	mov    0x18(%eax),%eax
80102234:	3b 45 10             	cmp    0x10(%ebp),%eax
80102237:	73 17                	jae    80102250 <writei+0x183>
    ip->size = off;
80102239:	8b 45 08             	mov    0x8(%ebp),%eax
8010223c:	8b 55 10             	mov    0x10(%ebp),%edx
8010223f:	89 50 18             	mov    %edx,0x18(%eax)
    iupdate(ip);
80102242:	83 ec 0c             	sub    $0xc,%esp
80102245:	ff 75 08             	pushl  0x8(%ebp)
80102248:	e8 e1 f5 ff ff       	call   8010182e <iupdate>
8010224d:	83 c4 10             	add    $0x10,%esp
  }
  return n;
80102250:	8b 45 14             	mov    0x14(%ebp),%eax
}
80102253:	c9                   	leave  
80102254:	c3                   	ret    

80102255 <namecmp>:

// Directories

int
namecmp(const char *s, const char *t)
{
80102255:	55                   	push   %ebp
80102256:	89 e5                	mov    %esp,%ebp
80102258:	83 ec 08             	sub    $0x8,%esp
  return strncmp(s, t, DIRSIZ);
8010225b:	83 ec 04             	sub    $0x4,%esp
8010225e:	6a 0e                	push   $0xe
80102260:	ff 75 0c             	pushl  0xc(%ebp)
80102263:	ff 75 08             	pushl  0x8(%ebp)
80102266:	e8 95 3c 00 00       	call   80105f00 <strncmp>
8010226b:	83 c4 10             	add    $0x10,%esp
}
8010226e:	c9                   	leave  
8010226f:	c3                   	ret    

80102270 <dirlookup>:

// Look for a directory entry in a directory.
// If found, set *poff to byte offset of entry.
struct inode*
dirlookup(struct inode *dp, char *name, uint *poff)
{
80102270:	55                   	push   %ebp
80102271:	89 e5                	mov    %esp,%ebp
80102273:	83 ec 28             	sub    $0x28,%esp
  uint off, inum;
  struct dirent de;

  if(dp->type != T_DIR)
80102276:	8b 45 08             	mov    0x8(%ebp),%eax
80102279:	0f b7 40 10          	movzwl 0x10(%eax),%eax
8010227d:	66 83 f8 01          	cmp    $0x1,%ax
80102281:	74 0d                	je     80102290 <dirlookup+0x20>
    panic("dirlookup not DIR");
80102283:	83 ec 0c             	sub    $0xc,%esp
80102286:	68 87 93 10 80       	push   $0x80109387
8010228b:	e8 d6 e2 ff ff       	call   80100566 <panic>

  for(off = 0; off < dp->size; off += sizeof(de)){
80102290:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80102297:	eb 7b                	jmp    80102314 <dirlookup+0xa4>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80102299:	6a 10                	push   $0x10
8010229b:	ff 75 f4             	pushl  -0xc(%ebp)
8010229e:	8d 45 e0             	lea    -0x20(%ebp),%eax
801022a1:	50                   	push   %eax
801022a2:	ff 75 08             	pushl  0x8(%ebp)
801022a5:	e8 cc fc ff ff       	call   80101f76 <readi>
801022aa:	83 c4 10             	add    $0x10,%esp
801022ad:	83 f8 10             	cmp    $0x10,%eax
801022b0:	74 0d                	je     801022bf <dirlookup+0x4f>
      panic("dirlink read");
801022b2:	83 ec 0c             	sub    $0xc,%esp
801022b5:	68 99 93 10 80       	push   $0x80109399
801022ba:	e8 a7 e2 ff ff       	call   80100566 <panic>
    if(de.inum == 0)
801022bf:	0f b7 45 e0          	movzwl -0x20(%ebp),%eax
801022c3:	66 85 c0             	test   %ax,%ax
801022c6:	74 47                	je     8010230f <dirlookup+0x9f>
      continue;
    if(namecmp(name, de.name) == 0){
801022c8:	83 ec 08             	sub    $0x8,%esp
801022cb:	8d 45 e0             	lea    -0x20(%ebp),%eax
801022ce:	83 c0 02             	add    $0x2,%eax
801022d1:	50                   	push   %eax
801022d2:	ff 75 0c             	pushl  0xc(%ebp)
801022d5:	e8 7b ff ff ff       	call   80102255 <namecmp>
801022da:	83 c4 10             	add    $0x10,%esp
801022dd:	85 c0                	test   %eax,%eax
801022df:	75 2f                	jne    80102310 <dirlookup+0xa0>
      // entry matches path element
      if(poff)
801022e1:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
801022e5:	74 08                	je     801022ef <dirlookup+0x7f>
        *poff = off;
801022e7:	8b 45 10             	mov    0x10(%ebp),%eax
801022ea:	8b 55 f4             	mov    -0xc(%ebp),%edx
801022ed:	89 10                	mov    %edx,(%eax)
      inum = de.inum;
801022ef:	0f b7 45 e0          	movzwl -0x20(%ebp),%eax
801022f3:	0f b7 c0             	movzwl %ax,%eax
801022f6:	89 45 f0             	mov    %eax,-0x10(%ebp)
      return iget(dp->dev, inum);
801022f9:	8b 45 08             	mov    0x8(%ebp),%eax
801022fc:	8b 00                	mov    (%eax),%eax
801022fe:	83 ec 08             	sub    $0x8,%esp
80102301:	ff 75 f0             	pushl  -0x10(%ebp)
80102304:	50                   	push   %eax
80102305:	e8 e5 f5 ff ff       	call   801018ef <iget>
8010230a:	83 c4 10             	add    $0x10,%esp
8010230d:	eb 19                	jmp    80102328 <dirlookup+0xb8>

  for(off = 0; off < dp->size; off += sizeof(de)){
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
      panic("dirlink read");
    if(de.inum == 0)
      continue;
8010230f:	90                   	nop
  struct dirent de;

  if(dp->type != T_DIR)
    panic("dirlookup not DIR");

  for(off = 0; off < dp->size; off += sizeof(de)){
80102310:	83 45 f4 10          	addl   $0x10,-0xc(%ebp)
80102314:	8b 45 08             	mov    0x8(%ebp),%eax
80102317:	8b 40 18             	mov    0x18(%eax),%eax
8010231a:	3b 45 f4             	cmp    -0xc(%ebp),%eax
8010231d:	0f 87 76 ff ff ff    	ja     80102299 <dirlookup+0x29>
      inum = de.inum;
      return iget(dp->dev, inum);
    }
  }

  return 0;
80102323:	b8 00 00 00 00       	mov    $0x0,%eax
}
80102328:	c9                   	leave  
80102329:	c3                   	ret    

8010232a <dirlink>:

// Write a new directory entry (name, inum) into the directory dp.
int
dirlink(struct inode *dp, char *name, uint inum)
{
8010232a:	55                   	push   %ebp
8010232b:	89 e5                	mov    %esp,%ebp
8010232d:	83 ec 28             	sub    $0x28,%esp
  int off;
  struct dirent de;
  struct inode *ip;

  // Check that name is not present.
  if((ip = dirlookup(dp, name, 0)) != 0){
80102330:	83 ec 04             	sub    $0x4,%esp
80102333:	6a 00                	push   $0x0
80102335:	ff 75 0c             	pushl  0xc(%ebp)
80102338:	ff 75 08             	pushl  0x8(%ebp)
8010233b:	e8 30 ff ff ff       	call   80102270 <dirlookup>
80102340:	83 c4 10             	add    $0x10,%esp
80102343:	89 45 f0             	mov    %eax,-0x10(%ebp)
80102346:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
8010234a:	74 18                	je     80102364 <dirlink+0x3a>
    iput(ip);
8010234c:	83 ec 0c             	sub    $0xc,%esp
8010234f:	ff 75 f0             	pushl  -0x10(%ebp)
80102352:	e8 81 f8 ff ff       	call   80101bd8 <iput>
80102357:	83 c4 10             	add    $0x10,%esp
    return -1;
8010235a:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010235f:	e9 9c 00 00 00       	jmp    80102400 <dirlink+0xd6>
  }

  // Look for an empty dirent.
  for(off = 0; off < dp->size; off += sizeof(de)){
80102364:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
8010236b:	eb 39                	jmp    801023a6 <dirlink+0x7c>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
8010236d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102370:	6a 10                	push   $0x10
80102372:	50                   	push   %eax
80102373:	8d 45 e0             	lea    -0x20(%ebp),%eax
80102376:	50                   	push   %eax
80102377:	ff 75 08             	pushl  0x8(%ebp)
8010237a:	e8 f7 fb ff ff       	call   80101f76 <readi>
8010237f:	83 c4 10             	add    $0x10,%esp
80102382:	83 f8 10             	cmp    $0x10,%eax
80102385:	74 0d                	je     80102394 <dirlink+0x6a>
      panic("dirlink read");
80102387:	83 ec 0c             	sub    $0xc,%esp
8010238a:	68 99 93 10 80       	push   $0x80109399
8010238f:	e8 d2 e1 ff ff       	call   80100566 <panic>
    if(de.inum == 0)
80102394:	0f b7 45 e0          	movzwl -0x20(%ebp),%eax
80102398:	66 85 c0             	test   %ax,%ax
8010239b:	74 18                	je     801023b5 <dirlink+0x8b>
    iput(ip);
    return -1;
  }

  // Look for an empty dirent.
  for(off = 0; off < dp->size; off += sizeof(de)){
8010239d:	8b 45 f4             	mov    -0xc(%ebp),%eax
801023a0:	83 c0 10             	add    $0x10,%eax
801023a3:	89 45 f4             	mov    %eax,-0xc(%ebp)
801023a6:	8b 45 08             	mov    0x8(%ebp),%eax
801023a9:	8b 50 18             	mov    0x18(%eax),%edx
801023ac:	8b 45 f4             	mov    -0xc(%ebp),%eax
801023af:	39 c2                	cmp    %eax,%edx
801023b1:	77 ba                	ja     8010236d <dirlink+0x43>
801023b3:	eb 01                	jmp    801023b6 <dirlink+0x8c>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
      panic("dirlink read");
    if(de.inum == 0)
      break;
801023b5:	90                   	nop
  }

  strncpy(de.name, name, DIRSIZ);
801023b6:	83 ec 04             	sub    $0x4,%esp
801023b9:	6a 0e                	push   $0xe
801023bb:	ff 75 0c             	pushl  0xc(%ebp)
801023be:	8d 45 e0             	lea    -0x20(%ebp),%eax
801023c1:	83 c0 02             	add    $0x2,%eax
801023c4:	50                   	push   %eax
801023c5:	e8 8c 3b 00 00       	call   80105f56 <strncpy>
801023ca:	83 c4 10             	add    $0x10,%esp
  de.inum = inum;
801023cd:	8b 45 10             	mov    0x10(%ebp),%eax
801023d0:	66 89 45 e0          	mov    %ax,-0x20(%ebp)
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
801023d4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801023d7:	6a 10                	push   $0x10
801023d9:	50                   	push   %eax
801023da:	8d 45 e0             	lea    -0x20(%ebp),%eax
801023dd:	50                   	push   %eax
801023de:	ff 75 08             	pushl  0x8(%ebp)
801023e1:	e8 e7 fc ff ff       	call   801020cd <writei>
801023e6:	83 c4 10             	add    $0x10,%esp
801023e9:	83 f8 10             	cmp    $0x10,%eax
801023ec:	74 0d                	je     801023fb <dirlink+0xd1>
    panic("dirlink");
801023ee:	83 ec 0c             	sub    $0xc,%esp
801023f1:	68 a6 93 10 80       	push   $0x801093a6
801023f6:	e8 6b e1 ff ff       	call   80100566 <panic>
  
  return 0;
801023fb:	b8 00 00 00 00       	mov    $0x0,%eax
}
80102400:	c9                   	leave  
80102401:	c3                   	ret    

80102402 <skipelem>:
//   skipelem("a", name) = "", setting name = "a"
//   skipelem("", name) = skipelem("////", name) = 0
//
static char*
skipelem(char *path, char *name)
{
80102402:	55                   	push   %ebp
80102403:	89 e5                	mov    %esp,%ebp
80102405:	83 ec 18             	sub    $0x18,%esp
  char *s;
  int len;

  while(*path == '/')
80102408:	eb 04                	jmp    8010240e <skipelem+0xc>
    path++;
8010240a:	83 45 08 01          	addl   $0x1,0x8(%ebp)
skipelem(char *path, char *name)
{
  char *s;
  int len;

  while(*path == '/')
8010240e:	8b 45 08             	mov    0x8(%ebp),%eax
80102411:	0f b6 00             	movzbl (%eax),%eax
80102414:	3c 2f                	cmp    $0x2f,%al
80102416:	74 f2                	je     8010240a <skipelem+0x8>
    path++;
  if(*path == 0)
80102418:	8b 45 08             	mov    0x8(%ebp),%eax
8010241b:	0f b6 00             	movzbl (%eax),%eax
8010241e:	84 c0                	test   %al,%al
80102420:	75 07                	jne    80102429 <skipelem+0x27>
    return 0;
80102422:	b8 00 00 00 00       	mov    $0x0,%eax
80102427:	eb 7b                	jmp    801024a4 <skipelem+0xa2>
  s = path;
80102429:	8b 45 08             	mov    0x8(%ebp),%eax
8010242c:	89 45 f4             	mov    %eax,-0xc(%ebp)
  while(*path != '/' && *path != 0)
8010242f:	eb 04                	jmp    80102435 <skipelem+0x33>
    path++;
80102431:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  while(*path == '/')
    path++;
  if(*path == 0)
    return 0;
  s = path;
  while(*path != '/' && *path != 0)
80102435:	8b 45 08             	mov    0x8(%ebp),%eax
80102438:	0f b6 00             	movzbl (%eax),%eax
8010243b:	3c 2f                	cmp    $0x2f,%al
8010243d:	74 0a                	je     80102449 <skipelem+0x47>
8010243f:	8b 45 08             	mov    0x8(%ebp),%eax
80102442:	0f b6 00             	movzbl (%eax),%eax
80102445:	84 c0                	test   %al,%al
80102447:	75 e8                	jne    80102431 <skipelem+0x2f>
    path++;
  len = path - s;
80102449:	8b 55 08             	mov    0x8(%ebp),%edx
8010244c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010244f:	29 c2                	sub    %eax,%edx
80102451:	89 d0                	mov    %edx,%eax
80102453:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(len >= DIRSIZ)
80102456:	83 7d f0 0d          	cmpl   $0xd,-0x10(%ebp)
8010245a:	7e 15                	jle    80102471 <skipelem+0x6f>
    memmove(name, s, DIRSIZ);
8010245c:	83 ec 04             	sub    $0x4,%esp
8010245f:	6a 0e                	push   $0xe
80102461:	ff 75 f4             	pushl  -0xc(%ebp)
80102464:	ff 75 0c             	pushl  0xc(%ebp)
80102467:	e8 fe 39 00 00       	call   80105e6a <memmove>
8010246c:	83 c4 10             	add    $0x10,%esp
8010246f:	eb 26                	jmp    80102497 <skipelem+0x95>
  else {
    memmove(name, s, len);
80102471:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102474:	83 ec 04             	sub    $0x4,%esp
80102477:	50                   	push   %eax
80102478:	ff 75 f4             	pushl  -0xc(%ebp)
8010247b:	ff 75 0c             	pushl  0xc(%ebp)
8010247e:	e8 e7 39 00 00       	call   80105e6a <memmove>
80102483:	83 c4 10             	add    $0x10,%esp
    name[len] = 0;
80102486:	8b 55 f0             	mov    -0x10(%ebp),%edx
80102489:	8b 45 0c             	mov    0xc(%ebp),%eax
8010248c:	01 d0                	add    %edx,%eax
8010248e:	c6 00 00             	movb   $0x0,(%eax)
  }
  while(*path == '/')
80102491:	eb 04                	jmp    80102497 <skipelem+0x95>
    path++;
80102493:	83 45 08 01          	addl   $0x1,0x8(%ebp)
    memmove(name, s, DIRSIZ);
  else {
    memmove(name, s, len);
    name[len] = 0;
  }
  while(*path == '/')
80102497:	8b 45 08             	mov    0x8(%ebp),%eax
8010249a:	0f b6 00             	movzbl (%eax),%eax
8010249d:	3c 2f                	cmp    $0x2f,%al
8010249f:	74 f2                	je     80102493 <skipelem+0x91>
    path++;
  return path;
801024a1:	8b 45 08             	mov    0x8(%ebp),%eax
}
801024a4:	c9                   	leave  
801024a5:	c3                   	ret    

801024a6 <namex>:
// If parent != 0, return the inode for the parent and copy the final
// path element into name, which must have room for DIRSIZ bytes.
// Must be called inside a transaction since it calls iput().
static struct inode*
namex(char *path, int nameiparent, char *name)
{
801024a6:	55                   	push   %ebp
801024a7:	89 e5                	mov    %esp,%ebp
801024a9:	83 ec 18             	sub    $0x18,%esp
  struct inode *ip, *next;

  if(*path == '/')
801024ac:	8b 45 08             	mov    0x8(%ebp),%eax
801024af:	0f b6 00             	movzbl (%eax),%eax
801024b2:	3c 2f                	cmp    $0x2f,%al
801024b4:	75 17                	jne    801024cd <namex+0x27>
    ip = iget(ROOTDEV, ROOTINO);
801024b6:	83 ec 08             	sub    $0x8,%esp
801024b9:	6a 01                	push   $0x1
801024bb:	6a 01                	push   $0x1
801024bd:	e8 2d f4 ff ff       	call   801018ef <iget>
801024c2:	83 c4 10             	add    $0x10,%esp
801024c5:	89 45 f4             	mov    %eax,-0xc(%ebp)
801024c8:	e9 bb 00 00 00       	jmp    80102588 <namex+0xe2>
  else
    ip = idup(proc->cwd);
801024cd:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801024d3:	8b 40 68             	mov    0x68(%eax),%eax
801024d6:	83 ec 0c             	sub    $0xc,%esp
801024d9:	50                   	push   %eax
801024da:	e8 ef f4 ff ff       	call   801019ce <idup>
801024df:	83 c4 10             	add    $0x10,%esp
801024e2:	89 45 f4             	mov    %eax,-0xc(%ebp)

  while((path = skipelem(path, name)) != 0){
801024e5:	e9 9e 00 00 00       	jmp    80102588 <namex+0xe2>
    ilock(ip);
801024ea:	83 ec 0c             	sub    $0xc,%esp
801024ed:	ff 75 f4             	pushl  -0xc(%ebp)
801024f0:	e8 13 f5 ff ff       	call   80101a08 <ilock>
801024f5:	83 c4 10             	add    $0x10,%esp
    if(ip->type != T_DIR){
801024f8:	8b 45 f4             	mov    -0xc(%ebp),%eax
801024fb:	0f b7 40 10          	movzwl 0x10(%eax),%eax
801024ff:	66 83 f8 01          	cmp    $0x1,%ax
80102503:	74 18                	je     8010251d <namex+0x77>
      iunlockput(ip);
80102505:	83 ec 0c             	sub    $0xc,%esp
80102508:	ff 75 f4             	pushl  -0xc(%ebp)
8010250b:	e8 b8 f7 ff ff       	call   80101cc8 <iunlockput>
80102510:	83 c4 10             	add    $0x10,%esp
      return 0;
80102513:	b8 00 00 00 00       	mov    $0x0,%eax
80102518:	e9 a7 00 00 00       	jmp    801025c4 <namex+0x11e>
    }
    if(nameiparent && *path == '\0'){
8010251d:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
80102521:	74 20                	je     80102543 <namex+0x9d>
80102523:	8b 45 08             	mov    0x8(%ebp),%eax
80102526:	0f b6 00             	movzbl (%eax),%eax
80102529:	84 c0                	test   %al,%al
8010252b:	75 16                	jne    80102543 <namex+0x9d>
      // Stop one level early.
      iunlock(ip);
8010252d:	83 ec 0c             	sub    $0xc,%esp
80102530:	ff 75 f4             	pushl  -0xc(%ebp)
80102533:	e8 2e f6 ff ff       	call   80101b66 <iunlock>
80102538:	83 c4 10             	add    $0x10,%esp
      return ip;
8010253b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010253e:	e9 81 00 00 00       	jmp    801025c4 <namex+0x11e>
    }
    if((next = dirlookup(ip, name, 0)) == 0){
80102543:	83 ec 04             	sub    $0x4,%esp
80102546:	6a 00                	push   $0x0
80102548:	ff 75 10             	pushl  0x10(%ebp)
8010254b:	ff 75 f4             	pushl  -0xc(%ebp)
8010254e:	e8 1d fd ff ff       	call   80102270 <dirlookup>
80102553:	83 c4 10             	add    $0x10,%esp
80102556:	89 45 f0             	mov    %eax,-0x10(%ebp)
80102559:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
8010255d:	75 15                	jne    80102574 <namex+0xce>
      iunlockput(ip);
8010255f:	83 ec 0c             	sub    $0xc,%esp
80102562:	ff 75 f4             	pushl  -0xc(%ebp)
80102565:	e8 5e f7 ff ff       	call   80101cc8 <iunlockput>
8010256a:	83 c4 10             	add    $0x10,%esp
      return 0;
8010256d:	b8 00 00 00 00       	mov    $0x0,%eax
80102572:	eb 50                	jmp    801025c4 <namex+0x11e>
    }
    iunlockput(ip);
80102574:	83 ec 0c             	sub    $0xc,%esp
80102577:	ff 75 f4             	pushl  -0xc(%ebp)
8010257a:	e8 49 f7 ff ff       	call   80101cc8 <iunlockput>
8010257f:	83 c4 10             	add    $0x10,%esp
    ip = next;
80102582:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102585:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(*path == '/')
    ip = iget(ROOTDEV, ROOTINO);
  else
    ip = idup(proc->cwd);

  while((path = skipelem(path, name)) != 0){
80102588:	83 ec 08             	sub    $0x8,%esp
8010258b:	ff 75 10             	pushl  0x10(%ebp)
8010258e:	ff 75 08             	pushl  0x8(%ebp)
80102591:	e8 6c fe ff ff       	call   80102402 <skipelem>
80102596:	83 c4 10             	add    $0x10,%esp
80102599:	89 45 08             	mov    %eax,0x8(%ebp)
8010259c:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
801025a0:	0f 85 44 ff ff ff    	jne    801024ea <namex+0x44>
      return 0;
    }
    iunlockput(ip);
    ip = next;
  }
  if(nameiparent){
801025a6:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
801025aa:	74 15                	je     801025c1 <namex+0x11b>
    iput(ip);
801025ac:	83 ec 0c             	sub    $0xc,%esp
801025af:	ff 75 f4             	pushl  -0xc(%ebp)
801025b2:	e8 21 f6 ff ff       	call   80101bd8 <iput>
801025b7:	83 c4 10             	add    $0x10,%esp
    return 0;
801025ba:	b8 00 00 00 00       	mov    $0x0,%eax
801025bf:	eb 03                	jmp    801025c4 <namex+0x11e>
  }
  return ip;
801025c1:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
801025c4:	c9                   	leave  
801025c5:	c3                   	ret    

801025c6 <namei>:

struct inode*
namei(char *path)
{
801025c6:	55                   	push   %ebp
801025c7:	89 e5                	mov    %esp,%ebp
801025c9:	83 ec 18             	sub    $0x18,%esp
  char name[DIRSIZ];
  return namex(path, 0, name);
801025cc:	83 ec 04             	sub    $0x4,%esp
801025cf:	8d 45 ea             	lea    -0x16(%ebp),%eax
801025d2:	50                   	push   %eax
801025d3:	6a 00                	push   $0x0
801025d5:	ff 75 08             	pushl  0x8(%ebp)
801025d8:	e8 c9 fe ff ff       	call   801024a6 <namex>
801025dd:	83 c4 10             	add    $0x10,%esp
}
801025e0:	c9                   	leave  
801025e1:	c3                   	ret    

801025e2 <nameiparent>:

struct inode*
nameiparent(char *path, char *name)
{
801025e2:	55                   	push   %ebp
801025e3:	89 e5                	mov    %esp,%ebp
801025e5:	83 ec 08             	sub    $0x8,%esp
  return namex(path, 1, name);
801025e8:	83 ec 04             	sub    $0x4,%esp
801025eb:	ff 75 0c             	pushl  0xc(%ebp)
801025ee:	6a 01                	push   $0x1
801025f0:	ff 75 08             	pushl  0x8(%ebp)
801025f3:	e8 ae fe ff ff       	call   801024a6 <namex>
801025f8:	83 c4 10             	add    $0x10,%esp
}
801025fb:	c9                   	leave  
801025fc:	c3                   	ret    

801025fd <inb>:

// end of CS333 added routines

static inline uchar
inb(ushort port)
{
801025fd:	55                   	push   %ebp
801025fe:	89 e5                	mov    %esp,%ebp
80102600:	83 ec 14             	sub    $0x14,%esp
80102603:	8b 45 08             	mov    0x8(%ebp),%eax
80102606:	66 89 45 ec          	mov    %ax,-0x14(%ebp)
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
8010260a:	0f b7 45 ec          	movzwl -0x14(%ebp),%eax
8010260e:	89 c2                	mov    %eax,%edx
80102610:	ec                   	in     (%dx),%al
80102611:	88 45 ff             	mov    %al,-0x1(%ebp)
  return data;
80102614:	0f b6 45 ff          	movzbl -0x1(%ebp),%eax
}
80102618:	c9                   	leave  
80102619:	c3                   	ret    

8010261a <insl>:

static inline void
insl(int port, void *addr, int cnt)
{
8010261a:	55                   	push   %ebp
8010261b:	89 e5                	mov    %esp,%ebp
8010261d:	57                   	push   %edi
8010261e:	53                   	push   %ebx
  asm volatile("cld; rep insl" :
8010261f:	8b 55 08             	mov    0x8(%ebp),%edx
80102622:	8b 4d 0c             	mov    0xc(%ebp),%ecx
80102625:	8b 45 10             	mov    0x10(%ebp),%eax
80102628:	89 cb                	mov    %ecx,%ebx
8010262a:	89 df                	mov    %ebx,%edi
8010262c:	89 c1                	mov    %eax,%ecx
8010262e:	fc                   	cld    
8010262f:	f3 6d                	rep insl (%dx),%es:(%edi)
80102631:	89 c8                	mov    %ecx,%eax
80102633:	89 fb                	mov    %edi,%ebx
80102635:	89 5d 0c             	mov    %ebx,0xc(%ebp)
80102638:	89 45 10             	mov    %eax,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "d" (port), "0" (addr), "1" (cnt) :
               "memory", "cc");
}
8010263b:	90                   	nop
8010263c:	5b                   	pop    %ebx
8010263d:	5f                   	pop    %edi
8010263e:	5d                   	pop    %ebp
8010263f:	c3                   	ret    

80102640 <outb>:

static inline void
outb(ushort port, uchar data)
{
80102640:	55                   	push   %ebp
80102641:	89 e5                	mov    %esp,%ebp
80102643:	83 ec 08             	sub    $0x8,%esp
80102646:	8b 55 08             	mov    0x8(%ebp),%edx
80102649:	8b 45 0c             	mov    0xc(%ebp),%eax
8010264c:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
80102650:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102653:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
80102657:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
8010265b:	ee                   	out    %al,(%dx)
}
8010265c:	90                   	nop
8010265d:	c9                   	leave  
8010265e:	c3                   	ret    

8010265f <outsl>:
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
}

static inline void
outsl(int port, const void *addr, int cnt)
{
8010265f:	55                   	push   %ebp
80102660:	89 e5                	mov    %esp,%ebp
80102662:	56                   	push   %esi
80102663:	53                   	push   %ebx
  asm volatile("cld; rep outsl" :
80102664:	8b 55 08             	mov    0x8(%ebp),%edx
80102667:	8b 4d 0c             	mov    0xc(%ebp),%ecx
8010266a:	8b 45 10             	mov    0x10(%ebp),%eax
8010266d:	89 cb                	mov    %ecx,%ebx
8010266f:	89 de                	mov    %ebx,%esi
80102671:	89 c1                	mov    %eax,%ecx
80102673:	fc                   	cld    
80102674:	f3 6f                	rep outsl %ds:(%esi),(%dx)
80102676:	89 c8                	mov    %ecx,%eax
80102678:	89 f3                	mov    %esi,%ebx
8010267a:	89 5d 0c             	mov    %ebx,0xc(%ebp)
8010267d:	89 45 10             	mov    %eax,0x10(%ebp)
               "=S" (addr), "=c" (cnt) :
               "d" (port), "0" (addr), "1" (cnt) :
               "cc");
}
80102680:	90                   	nop
80102681:	5b                   	pop    %ebx
80102682:	5e                   	pop    %esi
80102683:	5d                   	pop    %ebp
80102684:	c3                   	ret    

80102685 <idewait>:
static void idestart(struct buf*);

// Wait for IDE disk to become ready.
static int
idewait(int checkerr)
{
80102685:	55                   	push   %ebp
80102686:	89 e5                	mov    %esp,%ebp
80102688:	83 ec 10             	sub    $0x10,%esp
  int r;

  while(((r = inb(0x1f7)) & (IDE_BSY|IDE_DRDY)) != IDE_DRDY) 
8010268b:	90                   	nop
8010268c:	68 f7 01 00 00       	push   $0x1f7
80102691:	e8 67 ff ff ff       	call   801025fd <inb>
80102696:	83 c4 04             	add    $0x4,%esp
80102699:	0f b6 c0             	movzbl %al,%eax
8010269c:	89 45 fc             	mov    %eax,-0x4(%ebp)
8010269f:	8b 45 fc             	mov    -0x4(%ebp),%eax
801026a2:	25 c0 00 00 00       	and    $0xc0,%eax
801026a7:	83 f8 40             	cmp    $0x40,%eax
801026aa:	75 e0                	jne    8010268c <idewait+0x7>
    ;
  if(checkerr && (r & (IDE_DF|IDE_ERR)) != 0)
801026ac:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
801026b0:	74 11                	je     801026c3 <idewait+0x3e>
801026b2:	8b 45 fc             	mov    -0x4(%ebp),%eax
801026b5:	83 e0 21             	and    $0x21,%eax
801026b8:	85 c0                	test   %eax,%eax
801026ba:	74 07                	je     801026c3 <idewait+0x3e>
    return -1;
801026bc:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801026c1:	eb 05                	jmp    801026c8 <idewait+0x43>
  return 0;
801026c3:	b8 00 00 00 00       	mov    $0x0,%eax
}
801026c8:	c9                   	leave  
801026c9:	c3                   	ret    

801026ca <ideinit>:

void
ideinit(void)
{
801026ca:	55                   	push   %ebp
801026cb:	89 e5                	mov    %esp,%ebp
801026cd:	83 ec 18             	sub    $0x18,%esp
  int i;
  
  initlock(&idelock, "ide");
801026d0:	83 ec 08             	sub    $0x8,%esp
801026d3:	68 ae 93 10 80       	push   $0x801093ae
801026d8:	68 20 c6 10 80       	push   $0x8010c620
801026dd:	e8 44 34 00 00       	call   80105b26 <initlock>
801026e2:	83 c4 10             	add    $0x10,%esp
  picenable(IRQ_IDE);
801026e5:	83 ec 0c             	sub    $0xc,%esp
801026e8:	6a 0e                	push   $0xe
801026ea:	e8 da 18 00 00       	call   80103fc9 <picenable>
801026ef:	83 c4 10             	add    $0x10,%esp
  ioapicenable(IRQ_IDE, ncpu - 1);
801026f2:	a1 60 39 11 80       	mov    0x80113960,%eax
801026f7:	83 e8 01             	sub    $0x1,%eax
801026fa:	83 ec 08             	sub    $0x8,%esp
801026fd:	50                   	push   %eax
801026fe:	6a 0e                	push   $0xe
80102700:	e8 73 04 00 00       	call   80102b78 <ioapicenable>
80102705:	83 c4 10             	add    $0x10,%esp
  idewait(0);
80102708:	83 ec 0c             	sub    $0xc,%esp
8010270b:	6a 00                	push   $0x0
8010270d:	e8 73 ff ff ff       	call   80102685 <idewait>
80102712:	83 c4 10             	add    $0x10,%esp
  
  // Check if disk 1 is present
  outb(0x1f6, 0xe0 | (1<<4));
80102715:	83 ec 08             	sub    $0x8,%esp
80102718:	68 f0 00 00 00       	push   $0xf0
8010271d:	68 f6 01 00 00       	push   $0x1f6
80102722:	e8 19 ff ff ff       	call   80102640 <outb>
80102727:	83 c4 10             	add    $0x10,%esp
  for(i=0; i<1000; i++){
8010272a:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80102731:	eb 24                	jmp    80102757 <ideinit+0x8d>
    if(inb(0x1f7) != 0){
80102733:	83 ec 0c             	sub    $0xc,%esp
80102736:	68 f7 01 00 00       	push   $0x1f7
8010273b:	e8 bd fe ff ff       	call   801025fd <inb>
80102740:	83 c4 10             	add    $0x10,%esp
80102743:	84 c0                	test   %al,%al
80102745:	74 0c                	je     80102753 <ideinit+0x89>
      havedisk1 = 1;
80102747:	c7 05 58 c6 10 80 01 	movl   $0x1,0x8010c658
8010274e:	00 00 00 
      break;
80102751:	eb 0d                	jmp    80102760 <ideinit+0x96>
  ioapicenable(IRQ_IDE, ncpu - 1);
  idewait(0);
  
  // Check if disk 1 is present
  outb(0x1f6, 0xe0 | (1<<4));
  for(i=0; i<1000; i++){
80102753:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80102757:	81 7d f4 e7 03 00 00 	cmpl   $0x3e7,-0xc(%ebp)
8010275e:	7e d3                	jle    80102733 <ideinit+0x69>
      break;
    }
  }
  
  // Switch back to disk 0.
  outb(0x1f6, 0xe0 | (0<<4));
80102760:	83 ec 08             	sub    $0x8,%esp
80102763:	68 e0 00 00 00       	push   $0xe0
80102768:	68 f6 01 00 00       	push   $0x1f6
8010276d:	e8 ce fe ff ff       	call   80102640 <outb>
80102772:	83 c4 10             	add    $0x10,%esp
}
80102775:	90                   	nop
80102776:	c9                   	leave  
80102777:	c3                   	ret    

80102778 <idestart>:

// Start the request for b.  Caller must hold idelock.
static void
idestart(struct buf *b)
{
80102778:	55                   	push   %ebp
80102779:	89 e5                	mov    %esp,%ebp
8010277b:	83 ec 18             	sub    $0x18,%esp
  if(b == 0)
8010277e:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80102782:	75 0d                	jne    80102791 <idestart+0x19>
    panic("idestart");
80102784:	83 ec 0c             	sub    $0xc,%esp
80102787:	68 b2 93 10 80       	push   $0x801093b2
8010278c:	e8 d5 dd ff ff       	call   80100566 <panic>
  if(b->blockno >= FSSIZE)
80102791:	8b 45 08             	mov    0x8(%ebp),%eax
80102794:	8b 40 08             	mov    0x8(%eax),%eax
80102797:	3d cf 07 00 00       	cmp    $0x7cf,%eax
8010279c:	76 0d                	jbe    801027ab <idestart+0x33>
    panic("incorrect blockno");
8010279e:	83 ec 0c             	sub    $0xc,%esp
801027a1:	68 bb 93 10 80       	push   $0x801093bb
801027a6:	e8 bb dd ff ff       	call   80100566 <panic>
  int sector_per_block =  BSIZE/SECTOR_SIZE;
801027ab:	c7 45 f4 01 00 00 00 	movl   $0x1,-0xc(%ebp)
  int sector = b->blockno * sector_per_block;
801027b2:	8b 45 08             	mov    0x8(%ebp),%eax
801027b5:	8b 50 08             	mov    0x8(%eax),%edx
801027b8:	8b 45 f4             	mov    -0xc(%ebp),%eax
801027bb:	0f af c2             	imul   %edx,%eax
801027be:	89 45 f0             	mov    %eax,-0x10(%ebp)

  if (sector_per_block > 7) panic("idestart");
801027c1:	83 7d f4 07          	cmpl   $0x7,-0xc(%ebp)
801027c5:	7e 0d                	jle    801027d4 <idestart+0x5c>
801027c7:	83 ec 0c             	sub    $0xc,%esp
801027ca:	68 b2 93 10 80       	push   $0x801093b2
801027cf:	e8 92 dd ff ff       	call   80100566 <panic>
  
  idewait(0);
801027d4:	83 ec 0c             	sub    $0xc,%esp
801027d7:	6a 00                	push   $0x0
801027d9:	e8 a7 fe ff ff       	call   80102685 <idewait>
801027de:	83 c4 10             	add    $0x10,%esp
  outb(0x3f6, 0);  // generate interrupt
801027e1:	83 ec 08             	sub    $0x8,%esp
801027e4:	6a 00                	push   $0x0
801027e6:	68 f6 03 00 00       	push   $0x3f6
801027eb:	e8 50 fe ff ff       	call   80102640 <outb>
801027f0:	83 c4 10             	add    $0x10,%esp
  outb(0x1f2, sector_per_block);  // number of sectors
801027f3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801027f6:	0f b6 c0             	movzbl %al,%eax
801027f9:	83 ec 08             	sub    $0x8,%esp
801027fc:	50                   	push   %eax
801027fd:	68 f2 01 00 00       	push   $0x1f2
80102802:	e8 39 fe ff ff       	call   80102640 <outb>
80102807:	83 c4 10             	add    $0x10,%esp
  outb(0x1f3, sector & 0xff);
8010280a:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010280d:	0f b6 c0             	movzbl %al,%eax
80102810:	83 ec 08             	sub    $0x8,%esp
80102813:	50                   	push   %eax
80102814:	68 f3 01 00 00       	push   $0x1f3
80102819:	e8 22 fe ff ff       	call   80102640 <outb>
8010281e:	83 c4 10             	add    $0x10,%esp
  outb(0x1f4, (sector >> 8) & 0xff);
80102821:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102824:	c1 f8 08             	sar    $0x8,%eax
80102827:	0f b6 c0             	movzbl %al,%eax
8010282a:	83 ec 08             	sub    $0x8,%esp
8010282d:	50                   	push   %eax
8010282e:	68 f4 01 00 00       	push   $0x1f4
80102833:	e8 08 fe ff ff       	call   80102640 <outb>
80102838:	83 c4 10             	add    $0x10,%esp
  outb(0x1f5, (sector >> 16) & 0xff);
8010283b:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010283e:	c1 f8 10             	sar    $0x10,%eax
80102841:	0f b6 c0             	movzbl %al,%eax
80102844:	83 ec 08             	sub    $0x8,%esp
80102847:	50                   	push   %eax
80102848:	68 f5 01 00 00       	push   $0x1f5
8010284d:	e8 ee fd ff ff       	call   80102640 <outb>
80102852:	83 c4 10             	add    $0x10,%esp
  outb(0x1f6, 0xe0 | ((b->dev&1)<<4) | ((sector>>24)&0x0f));
80102855:	8b 45 08             	mov    0x8(%ebp),%eax
80102858:	8b 40 04             	mov    0x4(%eax),%eax
8010285b:	83 e0 01             	and    $0x1,%eax
8010285e:	c1 e0 04             	shl    $0x4,%eax
80102861:	89 c2                	mov    %eax,%edx
80102863:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102866:	c1 f8 18             	sar    $0x18,%eax
80102869:	83 e0 0f             	and    $0xf,%eax
8010286c:	09 d0                	or     %edx,%eax
8010286e:	83 c8 e0             	or     $0xffffffe0,%eax
80102871:	0f b6 c0             	movzbl %al,%eax
80102874:	83 ec 08             	sub    $0x8,%esp
80102877:	50                   	push   %eax
80102878:	68 f6 01 00 00       	push   $0x1f6
8010287d:	e8 be fd ff ff       	call   80102640 <outb>
80102882:	83 c4 10             	add    $0x10,%esp
  if(b->flags & B_DIRTY){
80102885:	8b 45 08             	mov    0x8(%ebp),%eax
80102888:	8b 00                	mov    (%eax),%eax
8010288a:	83 e0 04             	and    $0x4,%eax
8010288d:	85 c0                	test   %eax,%eax
8010288f:	74 30                	je     801028c1 <idestart+0x149>
    outb(0x1f7, IDE_CMD_WRITE);
80102891:	83 ec 08             	sub    $0x8,%esp
80102894:	6a 30                	push   $0x30
80102896:	68 f7 01 00 00       	push   $0x1f7
8010289b:	e8 a0 fd ff ff       	call   80102640 <outb>
801028a0:	83 c4 10             	add    $0x10,%esp
    outsl(0x1f0, b->data, BSIZE/4);
801028a3:	8b 45 08             	mov    0x8(%ebp),%eax
801028a6:	83 c0 18             	add    $0x18,%eax
801028a9:	83 ec 04             	sub    $0x4,%esp
801028ac:	68 80 00 00 00       	push   $0x80
801028b1:	50                   	push   %eax
801028b2:	68 f0 01 00 00       	push   $0x1f0
801028b7:	e8 a3 fd ff ff       	call   8010265f <outsl>
801028bc:	83 c4 10             	add    $0x10,%esp
  } else {
    outb(0x1f7, IDE_CMD_READ);
  }
}
801028bf:	eb 12                	jmp    801028d3 <idestart+0x15b>
  outb(0x1f6, 0xe0 | ((b->dev&1)<<4) | ((sector>>24)&0x0f));
  if(b->flags & B_DIRTY){
    outb(0x1f7, IDE_CMD_WRITE);
    outsl(0x1f0, b->data, BSIZE/4);
  } else {
    outb(0x1f7, IDE_CMD_READ);
801028c1:	83 ec 08             	sub    $0x8,%esp
801028c4:	6a 20                	push   $0x20
801028c6:	68 f7 01 00 00       	push   $0x1f7
801028cb:	e8 70 fd ff ff       	call   80102640 <outb>
801028d0:	83 c4 10             	add    $0x10,%esp
  }
}
801028d3:	90                   	nop
801028d4:	c9                   	leave  
801028d5:	c3                   	ret    

801028d6 <ideintr>:

// Interrupt handler.
void
ideintr(void)
{
801028d6:	55                   	push   %ebp
801028d7:	89 e5                	mov    %esp,%ebp
801028d9:	83 ec 18             	sub    $0x18,%esp
  struct buf *b;

  // First queued buffer is the active request.
  acquire(&idelock);
801028dc:	83 ec 0c             	sub    $0xc,%esp
801028df:	68 20 c6 10 80       	push   $0x8010c620
801028e4:	e8 5f 32 00 00       	call   80105b48 <acquire>
801028e9:	83 c4 10             	add    $0x10,%esp
  if((b = idequeue) == 0){
801028ec:	a1 54 c6 10 80       	mov    0x8010c654,%eax
801028f1:	89 45 f4             	mov    %eax,-0xc(%ebp)
801028f4:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801028f8:	75 15                	jne    8010290f <ideintr+0x39>
    release(&idelock);
801028fa:	83 ec 0c             	sub    $0xc,%esp
801028fd:	68 20 c6 10 80       	push   $0x8010c620
80102902:	e8 a8 32 00 00       	call   80105baf <release>
80102907:	83 c4 10             	add    $0x10,%esp
    // cprintf("spurious IDE interrupt\n");
    return;
8010290a:	e9 9a 00 00 00       	jmp    801029a9 <ideintr+0xd3>
  }
  idequeue = b->qnext;
8010290f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102912:	8b 40 14             	mov    0x14(%eax),%eax
80102915:	a3 54 c6 10 80       	mov    %eax,0x8010c654

  // Read data if needed.
  if(!(b->flags & B_DIRTY) && idewait(1) >= 0)
8010291a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010291d:	8b 00                	mov    (%eax),%eax
8010291f:	83 e0 04             	and    $0x4,%eax
80102922:	85 c0                	test   %eax,%eax
80102924:	75 2d                	jne    80102953 <ideintr+0x7d>
80102926:	83 ec 0c             	sub    $0xc,%esp
80102929:	6a 01                	push   $0x1
8010292b:	e8 55 fd ff ff       	call   80102685 <idewait>
80102930:	83 c4 10             	add    $0x10,%esp
80102933:	85 c0                	test   %eax,%eax
80102935:	78 1c                	js     80102953 <ideintr+0x7d>
    insl(0x1f0, b->data, BSIZE/4);
80102937:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010293a:	83 c0 18             	add    $0x18,%eax
8010293d:	83 ec 04             	sub    $0x4,%esp
80102940:	68 80 00 00 00       	push   $0x80
80102945:	50                   	push   %eax
80102946:	68 f0 01 00 00       	push   $0x1f0
8010294b:	e8 ca fc ff ff       	call   8010261a <insl>
80102950:	83 c4 10             	add    $0x10,%esp
  
  // Wake process waiting for this buf.
  b->flags |= B_VALID;
80102953:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102956:	8b 00                	mov    (%eax),%eax
80102958:	83 c8 02             	or     $0x2,%eax
8010295b:	89 c2                	mov    %eax,%edx
8010295d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102960:	89 10                	mov    %edx,(%eax)
  b->flags &= ~B_DIRTY;
80102962:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102965:	8b 00                	mov    (%eax),%eax
80102967:	83 e0 fb             	and    $0xfffffffb,%eax
8010296a:	89 c2                	mov    %eax,%edx
8010296c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010296f:	89 10                	mov    %edx,(%eax)
  wakeup(b);
80102971:	83 ec 0c             	sub    $0xc,%esp
80102974:	ff 75 f4             	pushl  -0xc(%ebp)
80102977:	e8 fd 2a 00 00       	call   80105479 <wakeup>
8010297c:	83 c4 10             	add    $0x10,%esp
  
  // Start disk on next buf in queue.
  if(idequeue != 0)
8010297f:	a1 54 c6 10 80       	mov    0x8010c654,%eax
80102984:	85 c0                	test   %eax,%eax
80102986:	74 11                	je     80102999 <ideintr+0xc3>
    idestart(idequeue);
80102988:	a1 54 c6 10 80       	mov    0x8010c654,%eax
8010298d:	83 ec 0c             	sub    $0xc,%esp
80102990:	50                   	push   %eax
80102991:	e8 e2 fd ff ff       	call   80102778 <idestart>
80102996:	83 c4 10             	add    $0x10,%esp

  release(&idelock);
80102999:	83 ec 0c             	sub    $0xc,%esp
8010299c:	68 20 c6 10 80       	push   $0x8010c620
801029a1:	e8 09 32 00 00       	call   80105baf <release>
801029a6:	83 c4 10             	add    $0x10,%esp
}
801029a9:	c9                   	leave  
801029aa:	c3                   	ret    

801029ab <iderw>:
// Sync buf with disk. 
// If B_DIRTY is set, write buf to disk, clear B_DIRTY, set B_VALID.
// Else if B_VALID is not set, read buf from disk, set B_VALID.
void
iderw(struct buf *b)
{
801029ab:	55                   	push   %ebp
801029ac:	89 e5                	mov    %esp,%ebp
801029ae:	83 ec 18             	sub    $0x18,%esp
  struct buf **pp;

  if(!(b->flags & B_BUSY))
801029b1:	8b 45 08             	mov    0x8(%ebp),%eax
801029b4:	8b 00                	mov    (%eax),%eax
801029b6:	83 e0 01             	and    $0x1,%eax
801029b9:	85 c0                	test   %eax,%eax
801029bb:	75 0d                	jne    801029ca <iderw+0x1f>
    panic("iderw: buf not busy");
801029bd:	83 ec 0c             	sub    $0xc,%esp
801029c0:	68 cd 93 10 80       	push   $0x801093cd
801029c5:	e8 9c db ff ff       	call   80100566 <panic>
  if((b->flags & (B_VALID|B_DIRTY)) == B_VALID)
801029ca:	8b 45 08             	mov    0x8(%ebp),%eax
801029cd:	8b 00                	mov    (%eax),%eax
801029cf:	83 e0 06             	and    $0x6,%eax
801029d2:	83 f8 02             	cmp    $0x2,%eax
801029d5:	75 0d                	jne    801029e4 <iderw+0x39>
    panic("iderw: nothing to do");
801029d7:	83 ec 0c             	sub    $0xc,%esp
801029da:	68 e1 93 10 80       	push   $0x801093e1
801029df:	e8 82 db ff ff       	call   80100566 <panic>
  if(b->dev != 0 && !havedisk1)
801029e4:	8b 45 08             	mov    0x8(%ebp),%eax
801029e7:	8b 40 04             	mov    0x4(%eax),%eax
801029ea:	85 c0                	test   %eax,%eax
801029ec:	74 16                	je     80102a04 <iderw+0x59>
801029ee:	a1 58 c6 10 80       	mov    0x8010c658,%eax
801029f3:	85 c0                	test   %eax,%eax
801029f5:	75 0d                	jne    80102a04 <iderw+0x59>
    panic("iderw: ide disk 1 not present");
801029f7:	83 ec 0c             	sub    $0xc,%esp
801029fa:	68 f6 93 10 80       	push   $0x801093f6
801029ff:	e8 62 db ff ff       	call   80100566 <panic>

  acquire(&idelock);  //DOC:acquire-lock
80102a04:	83 ec 0c             	sub    $0xc,%esp
80102a07:	68 20 c6 10 80       	push   $0x8010c620
80102a0c:	e8 37 31 00 00       	call   80105b48 <acquire>
80102a11:	83 c4 10             	add    $0x10,%esp

  // Append b to idequeue.
  b->qnext = 0;
80102a14:	8b 45 08             	mov    0x8(%ebp),%eax
80102a17:	c7 40 14 00 00 00 00 	movl   $0x0,0x14(%eax)
  for(pp=&idequeue; *pp; pp=&(*pp)->qnext)  //DOC:insert-queue
80102a1e:	c7 45 f4 54 c6 10 80 	movl   $0x8010c654,-0xc(%ebp)
80102a25:	eb 0b                	jmp    80102a32 <iderw+0x87>
80102a27:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102a2a:	8b 00                	mov    (%eax),%eax
80102a2c:	83 c0 14             	add    $0x14,%eax
80102a2f:	89 45 f4             	mov    %eax,-0xc(%ebp)
80102a32:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102a35:	8b 00                	mov    (%eax),%eax
80102a37:	85 c0                	test   %eax,%eax
80102a39:	75 ec                	jne    80102a27 <iderw+0x7c>
    ;
  *pp = b;
80102a3b:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102a3e:	8b 55 08             	mov    0x8(%ebp),%edx
80102a41:	89 10                	mov    %edx,(%eax)
  
  // Start disk if necessary.
  if(idequeue == b)
80102a43:	a1 54 c6 10 80       	mov    0x8010c654,%eax
80102a48:	3b 45 08             	cmp    0x8(%ebp),%eax
80102a4b:	75 23                	jne    80102a70 <iderw+0xc5>
    idestart(b);
80102a4d:	83 ec 0c             	sub    $0xc,%esp
80102a50:	ff 75 08             	pushl  0x8(%ebp)
80102a53:	e8 20 fd ff ff       	call   80102778 <idestart>
80102a58:	83 c4 10             	add    $0x10,%esp
  
  // Wait for request to finish.
  while((b->flags & (B_VALID|B_DIRTY)) != B_VALID){
80102a5b:	eb 13                	jmp    80102a70 <iderw+0xc5>
    sleep(b, &idelock);
80102a5d:	83 ec 08             	sub    $0x8,%esp
80102a60:	68 20 c6 10 80       	push   $0x8010c620
80102a65:	ff 75 08             	pushl  0x8(%ebp)
80102a68:	e8 f7 28 00 00       	call   80105364 <sleep>
80102a6d:	83 c4 10             	add    $0x10,%esp
  // Start disk if necessary.
  if(idequeue == b)
    idestart(b);
  
  // Wait for request to finish.
  while((b->flags & (B_VALID|B_DIRTY)) != B_VALID){
80102a70:	8b 45 08             	mov    0x8(%ebp),%eax
80102a73:	8b 00                	mov    (%eax),%eax
80102a75:	83 e0 06             	and    $0x6,%eax
80102a78:	83 f8 02             	cmp    $0x2,%eax
80102a7b:	75 e0                	jne    80102a5d <iderw+0xb2>
    sleep(b, &idelock);
  }

  release(&idelock);
80102a7d:	83 ec 0c             	sub    $0xc,%esp
80102a80:	68 20 c6 10 80       	push   $0x8010c620
80102a85:	e8 25 31 00 00       	call   80105baf <release>
80102a8a:	83 c4 10             	add    $0x10,%esp
}
80102a8d:	90                   	nop
80102a8e:	c9                   	leave  
80102a8f:	c3                   	ret    

80102a90 <ioapicread>:
  uint data;
};

static uint
ioapicread(int reg)
{
80102a90:	55                   	push   %ebp
80102a91:	89 e5                	mov    %esp,%ebp
  ioapic->reg = reg;
80102a93:	a1 34 32 11 80       	mov    0x80113234,%eax
80102a98:	8b 55 08             	mov    0x8(%ebp),%edx
80102a9b:	89 10                	mov    %edx,(%eax)
  return ioapic->data;
80102a9d:	a1 34 32 11 80       	mov    0x80113234,%eax
80102aa2:	8b 40 10             	mov    0x10(%eax),%eax
}
80102aa5:	5d                   	pop    %ebp
80102aa6:	c3                   	ret    

80102aa7 <ioapicwrite>:

static void
ioapicwrite(int reg, uint data)
{
80102aa7:	55                   	push   %ebp
80102aa8:	89 e5                	mov    %esp,%ebp
  ioapic->reg = reg;
80102aaa:	a1 34 32 11 80       	mov    0x80113234,%eax
80102aaf:	8b 55 08             	mov    0x8(%ebp),%edx
80102ab2:	89 10                	mov    %edx,(%eax)
  ioapic->data = data;
80102ab4:	a1 34 32 11 80       	mov    0x80113234,%eax
80102ab9:	8b 55 0c             	mov    0xc(%ebp),%edx
80102abc:	89 50 10             	mov    %edx,0x10(%eax)
}
80102abf:	90                   	nop
80102ac0:	5d                   	pop    %ebp
80102ac1:	c3                   	ret    

80102ac2 <ioapicinit>:

void
ioapicinit(void)
{
80102ac2:	55                   	push   %ebp
80102ac3:	89 e5                	mov    %esp,%ebp
80102ac5:	83 ec 18             	sub    $0x18,%esp
  int i, id, maxintr;

  if(!ismp)
80102ac8:	a1 64 33 11 80       	mov    0x80113364,%eax
80102acd:	85 c0                	test   %eax,%eax
80102acf:	0f 84 a0 00 00 00    	je     80102b75 <ioapicinit+0xb3>
    return;

  ioapic = (volatile struct ioapic*)IOAPIC;
80102ad5:	c7 05 34 32 11 80 00 	movl   $0xfec00000,0x80113234
80102adc:	00 c0 fe 
  maxintr = (ioapicread(REG_VER) >> 16) & 0xFF;
80102adf:	6a 01                	push   $0x1
80102ae1:	e8 aa ff ff ff       	call   80102a90 <ioapicread>
80102ae6:	83 c4 04             	add    $0x4,%esp
80102ae9:	c1 e8 10             	shr    $0x10,%eax
80102aec:	25 ff 00 00 00       	and    $0xff,%eax
80102af1:	89 45 f0             	mov    %eax,-0x10(%ebp)
  id = ioapicread(REG_ID) >> 24;
80102af4:	6a 00                	push   $0x0
80102af6:	e8 95 ff ff ff       	call   80102a90 <ioapicread>
80102afb:	83 c4 04             	add    $0x4,%esp
80102afe:	c1 e8 18             	shr    $0x18,%eax
80102b01:	89 45 ec             	mov    %eax,-0x14(%ebp)
  if(id != ioapicid)
80102b04:	0f b6 05 60 33 11 80 	movzbl 0x80113360,%eax
80102b0b:	0f b6 c0             	movzbl %al,%eax
80102b0e:	3b 45 ec             	cmp    -0x14(%ebp),%eax
80102b11:	74 10                	je     80102b23 <ioapicinit+0x61>
    cprintf("ioapicinit: id isn't equal to ioapicid; not a MP\n");
80102b13:	83 ec 0c             	sub    $0xc,%esp
80102b16:	68 14 94 10 80       	push   $0x80109414
80102b1b:	e8 a6 d8 ff ff       	call   801003c6 <cprintf>
80102b20:	83 c4 10             	add    $0x10,%esp

  // Mark all interrupts edge-triggered, active high, disabled,
  // and not routed to any CPUs.
  for(i = 0; i <= maxintr; i++){
80102b23:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80102b2a:	eb 3f                	jmp    80102b6b <ioapicinit+0xa9>
    ioapicwrite(REG_TABLE+2*i, INT_DISABLED | (T_IRQ0 + i));
80102b2c:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102b2f:	83 c0 20             	add    $0x20,%eax
80102b32:	0d 00 00 01 00       	or     $0x10000,%eax
80102b37:	89 c2                	mov    %eax,%edx
80102b39:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102b3c:	83 c0 08             	add    $0x8,%eax
80102b3f:	01 c0                	add    %eax,%eax
80102b41:	83 ec 08             	sub    $0x8,%esp
80102b44:	52                   	push   %edx
80102b45:	50                   	push   %eax
80102b46:	e8 5c ff ff ff       	call   80102aa7 <ioapicwrite>
80102b4b:	83 c4 10             	add    $0x10,%esp
    ioapicwrite(REG_TABLE+2*i+1, 0);
80102b4e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102b51:	83 c0 08             	add    $0x8,%eax
80102b54:	01 c0                	add    %eax,%eax
80102b56:	83 c0 01             	add    $0x1,%eax
80102b59:	83 ec 08             	sub    $0x8,%esp
80102b5c:	6a 00                	push   $0x0
80102b5e:	50                   	push   %eax
80102b5f:	e8 43 ff ff ff       	call   80102aa7 <ioapicwrite>
80102b64:	83 c4 10             	add    $0x10,%esp
  if(id != ioapicid)
    cprintf("ioapicinit: id isn't equal to ioapicid; not a MP\n");

  // Mark all interrupts edge-triggered, active high, disabled,
  // and not routed to any CPUs.
  for(i = 0; i <= maxintr; i++){
80102b67:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80102b6b:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102b6e:	3b 45 f0             	cmp    -0x10(%ebp),%eax
80102b71:	7e b9                	jle    80102b2c <ioapicinit+0x6a>
80102b73:	eb 01                	jmp    80102b76 <ioapicinit+0xb4>
ioapicinit(void)
{
  int i, id, maxintr;

  if(!ismp)
    return;
80102b75:	90                   	nop
  // and not routed to any CPUs.
  for(i = 0; i <= maxintr; i++){
    ioapicwrite(REG_TABLE+2*i, INT_DISABLED | (T_IRQ0 + i));
    ioapicwrite(REG_TABLE+2*i+1, 0);
  }
}
80102b76:	c9                   	leave  
80102b77:	c3                   	ret    

80102b78 <ioapicenable>:

void
ioapicenable(int irq, int cpunum)
{
80102b78:	55                   	push   %ebp
80102b79:	89 e5                	mov    %esp,%ebp
  if(!ismp)
80102b7b:	a1 64 33 11 80       	mov    0x80113364,%eax
80102b80:	85 c0                	test   %eax,%eax
80102b82:	74 39                	je     80102bbd <ioapicenable+0x45>
    return;

  // Mark interrupt edge-triggered, active high,
  // enabled, and routed to the given cpunum,
  // which happens to be that cpu's APIC ID.
  ioapicwrite(REG_TABLE+2*irq, T_IRQ0 + irq);
80102b84:	8b 45 08             	mov    0x8(%ebp),%eax
80102b87:	83 c0 20             	add    $0x20,%eax
80102b8a:	89 c2                	mov    %eax,%edx
80102b8c:	8b 45 08             	mov    0x8(%ebp),%eax
80102b8f:	83 c0 08             	add    $0x8,%eax
80102b92:	01 c0                	add    %eax,%eax
80102b94:	52                   	push   %edx
80102b95:	50                   	push   %eax
80102b96:	e8 0c ff ff ff       	call   80102aa7 <ioapicwrite>
80102b9b:	83 c4 08             	add    $0x8,%esp
  ioapicwrite(REG_TABLE+2*irq+1, cpunum << 24);
80102b9e:	8b 45 0c             	mov    0xc(%ebp),%eax
80102ba1:	c1 e0 18             	shl    $0x18,%eax
80102ba4:	89 c2                	mov    %eax,%edx
80102ba6:	8b 45 08             	mov    0x8(%ebp),%eax
80102ba9:	83 c0 08             	add    $0x8,%eax
80102bac:	01 c0                	add    %eax,%eax
80102bae:	83 c0 01             	add    $0x1,%eax
80102bb1:	52                   	push   %edx
80102bb2:	50                   	push   %eax
80102bb3:	e8 ef fe ff ff       	call   80102aa7 <ioapicwrite>
80102bb8:	83 c4 08             	add    $0x8,%esp
80102bbb:	eb 01                	jmp    80102bbe <ioapicenable+0x46>

void
ioapicenable(int irq, int cpunum)
{
  if(!ismp)
    return;
80102bbd:	90                   	nop
  // Mark interrupt edge-triggered, active high,
  // enabled, and routed to the given cpunum,
  // which happens to be that cpu's APIC ID.
  ioapicwrite(REG_TABLE+2*irq, T_IRQ0 + irq);
  ioapicwrite(REG_TABLE+2*irq+1, cpunum << 24);
}
80102bbe:	c9                   	leave  
80102bbf:	c3                   	ret    

80102bc0 <v2p>:
#define KERNBASE 0x80000000         // First kernel virtual address
#define KERNLINK (KERNBASE+EXTMEM)  // Address where kernel is linked

#ifndef __ASSEMBLER__

static inline uint v2p(void *a) { return ((uint) (a))  - KERNBASE; }
80102bc0:	55                   	push   %ebp
80102bc1:	89 e5                	mov    %esp,%ebp
80102bc3:	8b 45 08             	mov    0x8(%ebp),%eax
80102bc6:	05 00 00 00 80       	add    $0x80000000,%eax
80102bcb:	5d                   	pop    %ebp
80102bcc:	c3                   	ret    

80102bcd <kinit1>:
// the pages mapped by entrypgdir on free list.
// 2. main() calls kinit2() with the rest of the physical pages
// after installing a full page table that maps them on all cores.
void
kinit1(void *vstart, void *vend)
{
80102bcd:	55                   	push   %ebp
80102bce:	89 e5                	mov    %esp,%ebp
80102bd0:	83 ec 08             	sub    $0x8,%esp
  initlock(&kmem.lock, "kmem");
80102bd3:	83 ec 08             	sub    $0x8,%esp
80102bd6:	68 46 94 10 80       	push   $0x80109446
80102bdb:	68 40 32 11 80       	push   $0x80113240
80102be0:	e8 41 2f 00 00       	call   80105b26 <initlock>
80102be5:	83 c4 10             	add    $0x10,%esp
  kmem.use_lock = 0;
80102be8:	c7 05 74 32 11 80 00 	movl   $0x0,0x80113274
80102bef:	00 00 00 
  freerange(vstart, vend);
80102bf2:	83 ec 08             	sub    $0x8,%esp
80102bf5:	ff 75 0c             	pushl  0xc(%ebp)
80102bf8:	ff 75 08             	pushl  0x8(%ebp)
80102bfb:	e8 2a 00 00 00       	call   80102c2a <freerange>
80102c00:	83 c4 10             	add    $0x10,%esp
}
80102c03:	90                   	nop
80102c04:	c9                   	leave  
80102c05:	c3                   	ret    

80102c06 <kinit2>:

void
kinit2(void *vstart, void *vend)
{
80102c06:	55                   	push   %ebp
80102c07:	89 e5                	mov    %esp,%ebp
80102c09:	83 ec 08             	sub    $0x8,%esp
  freerange(vstart, vend);
80102c0c:	83 ec 08             	sub    $0x8,%esp
80102c0f:	ff 75 0c             	pushl  0xc(%ebp)
80102c12:	ff 75 08             	pushl  0x8(%ebp)
80102c15:	e8 10 00 00 00       	call   80102c2a <freerange>
80102c1a:	83 c4 10             	add    $0x10,%esp
  kmem.use_lock = 1;
80102c1d:	c7 05 74 32 11 80 01 	movl   $0x1,0x80113274
80102c24:	00 00 00 
}
80102c27:	90                   	nop
80102c28:	c9                   	leave  
80102c29:	c3                   	ret    

80102c2a <freerange>:

void
freerange(void *vstart, void *vend)
{
80102c2a:	55                   	push   %ebp
80102c2b:	89 e5                	mov    %esp,%ebp
80102c2d:	83 ec 18             	sub    $0x18,%esp
  char *p;
  p = (char*)PGROUNDUP((uint)vstart);
80102c30:	8b 45 08             	mov    0x8(%ebp),%eax
80102c33:	05 ff 0f 00 00       	add    $0xfff,%eax
80102c38:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80102c3d:	89 45 f4             	mov    %eax,-0xc(%ebp)
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
80102c40:	eb 15                	jmp    80102c57 <freerange+0x2d>
    kfree(p);
80102c42:	83 ec 0c             	sub    $0xc,%esp
80102c45:	ff 75 f4             	pushl  -0xc(%ebp)
80102c48:	e8 1a 00 00 00       	call   80102c67 <kfree>
80102c4d:	83 c4 10             	add    $0x10,%esp
void
freerange(void *vstart, void *vend)
{
  char *p;
  p = (char*)PGROUNDUP((uint)vstart);
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
80102c50:	81 45 f4 00 10 00 00 	addl   $0x1000,-0xc(%ebp)
80102c57:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102c5a:	05 00 10 00 00       	add    $0x1000,%eax
80102c5f:	3b 45 0c             	cmp    0xc(%ebp),%eax
80102c62:	76 de                	jbe    80102c42 <freerange+0x18>
    kfree(p);
}
80102c64:	90                   	nop
80102c65:	c9                   	leave  
80102c66:	c3                   	ret    

80102c67 <kfree>:
// which normally should have been returned by a
// call to kalloc().  (The exception is when
// initializing the allocator; see kinit above.)
void
kfree(char *v)
{
80102c67:	55                   	push   %ebp
80102c68:	89 e5                	mov    %esp,%ebp
80102c6a:	83 ec 18             	sub    $0x18,%esp
  struct run *r;

  if((uint)v % PGSIZE || v < end || v2p(v) >= PHYSTOP)
80102c6d:	8b 45 08             	mov    0x8(%ebp),%eax
80102c70:	25 ff 0f 00 00       	and    $0xfff,%eax
80102c75:	85 c0                	test   %eax,%eax
80102c77:	75 1b                	jne    80102c94 <kfree+0x2d>
80102c79:	81 7d 08 3c 68 11 80 	cmpl   $0x8011683c,0x8(%ebp)
80102c80:	72 12                	jb     80102c94 <kfree+0x2d>
80102c82:	ff 75 08             	pushl  0x8(%ebp)
80102c85:	e8 36 ff ff ff       	call   80102bc0 <v2p>
80102c8a:	83 c4 04             	add    $0x4,%esp
80102c8d:	3d ff ff ff 0d       	cmp    $0xdffffff,%eax
80102c92:	76 0d                	jbe    80102ca1 <kfree+0x3a>
    panic("kfree");
80102c94:	83 ec 0c             	sub    $0xc,%esp
80102c97:	68 4b 94 10 80       	push   $0x8010944b
80102c9c:	e8 c5 d8 ff ff       	call   80100566 <panic>

  // Fill with junk to catch dangling refs.
  memset(v, 1, PGSIZE);
80102ca1:	83 ec 04             	sub    $0x4,%esp
80102ca4:	68 00 10 00 00       	push   $0x1000
80102ca9:	6a 01                	push   $0x1
80102cab:	ff 75 08             	pushl  0x8(%ebp)
80102cae:	e8 f8 30 00 00       	call   80105dab <memset>
80102cb3:	83 c4 10             	add    $0x10,%esp

  if(kmem.use_lock)
80102cb6:	a1 74 32 11 80       	mov    0x80113274,%eax
80102cbb:	85 c0                	test   %eax,%eax
80102cbd:	74 10                	je     80102ccf <kfree+0x68>
    acquire(&kmem.lock);
80102cbf:	83 ec 0c             	sub    $0xc,%esp
80102cc2:	68 40 32 11 80       	push   $0x80113240
80102cc7:	e8 7c 2e 00 00       	call   80105b48 <acquire>
80102ccc:	83 c4 10             	add    $0x10,%esp
  r = (struct run*)v;
80102ccf:	8b 45 08             	mov    0x8(%ebp),%eax
80102cd2:	89 45 f4             	mov    %eax,-0xc(%ebp)
  r->next = kmem.freelist;
80102cd5:	8b 15 78 32 11 80    	mov    0x80113278,%edx
80102cdb:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102cde:	89 10                	mov    %edx,(%eax)
  kmem.freelist = r;
80102ce0:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102ce3:	a3 78 32 11 80       	mov    %eax,0x80113278
  if(kmem.use_lock)
80102ce8:	a1 74 32 11 80       	mov    0x80113274,%eax
80102ced:	85 c0                	test   %eax,%eax
80102cef:	74 10                	je     80102d01 <kfree+0x9a>
    release(&kmem.lock);
80102cf1:	83 ec 0c             	sub    $0xc,%esp
80102cf4:	68 40 32 11 80       	push   $0x80113240
80102cf9:	e8 b1 2e 00 00       	call   80105baf <release>
80102cfe:	83 c4 10             	add    $0x10,%esp
}
80102d01:	90                   	nop
80102d02:	c9                   	leave  
80102d03:	c3                   	ret    

80102d04 <kalloc>:
// Allocate one 4096-byte page of physical memory.
// Returns a pointer that the kernel can use.
// Returns 0 if the memory cannot be allocated.
char*
kalloc(void)
{
80102d04:	55                   	push   %ebp
80102d05:	89 e5                	mov    %esp,%ebp
80102d07:	83 ec 18             	sub    $0x18,%esp
  struct run *r;

  if(kmem.use_lock)
80102d0a:	a1 74 32 11 80       	mov    0x80113274,%eax
80102d0f:	85 c0                	test   %eax,%eax
80102d11:	74 10                	je     80102d23 <kalloc+0x1f>
    acquire(&kmem.lock);
80102d13:	83 ec 0c             	sub    $0xc,%esp
80102d16:	68 40 32 11 80       	push   $0x80113240
80102d1b:	e8 28 2e 00 00       	call   80105b48 <acquire>
80102d20:	83 c4 10             	add    $0x10,%esp
  r = kmem.freelist;
80102d23:	a1 78 32 11 80       	mov    0x80113278,%eax
80102d28:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(r)
80102d2b:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80102d2f:	74 0a                	je     80102d3b <kalloc+0x37>
    kmem.freelist = r->next;
80102d31:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102d34:	8b 00                	mov    (%eax),%eax
80102d36:	a3 78 32 11 80       	mov    %eax,0x80113278
  if(kmem.use_lock)
80102d3b:	a1 74 32 11 80       	mov    0x80113274,%eax
80102d40:	85 c0                	test   %eax,%eax
80102d42:	74 10                	je     80102d54 <kalloc+0x50>
    release(&kmem.lock);
80102d44:	83 ec 0c             	sub    $0xc,%esp
80102d47:	68 40 32 11 80       	push   $0x80113240
80102d4c:	e8 5e 2e 00 00       	call   80105baf <release>
80102d51:	83 c4 10             	add    $0x10,%esp
  return (char*)r;
80102d54:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
80102d57:	c9                   	leave  
80102d58:	c3                   	ret    

80102d59 <inb>:

// end of CS333 added routines

static inline uchar
inb(ushort port)
{
80102d59:	55                   	push   %ebp
80102d5a:	89 e5                	mov    %esp,%ebp
80102d5c:	83 ec 14             	sub    $0x14,%esp
80102d5f:	8b 45 08             	mov    0x8(%ebp),%eax
80102d62:	66 89 45 ec          	mov    %ax,-0x14(%ebp)
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102d66:	0f b7 45 ec          	movzwl -0x14(%ebp),%eax
80102d6a:	89 c2                	mov    %eax,%edx
80102d6c:	ec                   	in     (%dx),%al
80102d6d:	88 45 ff             	mov    %al,-0x1(%ebp)
  return data;
80102d70:	0f b6 45 ff          	movzbl -0x1(%ebp),%eax
}
80102d74:	c9                   	leave  
80102d75:	c3                   	ret    

80102d76 <kbdgetc>:
#include "defs.h"
#include "kbd.h"

int
kbdgetc(void)
{
80102d76:	55                   	push   %ebp
80102d77:	89 e5                	mov    %esp,%ebp
80102d79:	83 ec 10             	sub    $0x10,%esp
  static uchar *charcode[4] = {
    normalmap, shiftmap, ctlmap, ctlmap
  };
  uint st, data, c;

  st = inb(KBSTATP);
80102d7c:	6a 64                	push   $0x64
80102d7e:	e8 d6 ff ff ff       	call   80102d59 <inb>
80102d83:	83 c4 04             	add    $0x4,%esp
80102d86:	0f b6 c0             	movzbl %al,%eax
80102d89:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if((st & KBS_DIB) == 0)
80102d8c:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102d8f:	83 e0 01             	and    $0x1,%eax
80102d92:	85 c0                	test   %eax,%eax
80102d94:	75 0a                	jne    80102da0 <kbdgetc+0x2a>
    return -1;
80102d96:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80102d9b:	e9 23 01 00 00       	jmp    80102ec3 <kbdgetc+0x14d>
  data = inb(KBDATAP);
80102da0:	6a 60                	push   $0x60
80102da2:	e8 b2 ff ff ff       	call   80102d59 <inb>
80102da7:	83 c4 04             	add    $0x4,%esp
80102daa:	0f b6 c0             	movzbl %al,%eax
80102dad:	89 45 fc             	mov    %eax,-0x4(%ebp)

  if(data == 0xE0){
80102db0:	81 7d fc e0 00 00 00 	cmpl   $0xe0,-0x4(%ebp)
80102db7:	75 17                	jne    80102dd0 <kbdgetc+0x5a>
    shift |= E0ESC;
80102db9:	a1 5c c6 10 80       	mov    0x8010c65c,%eax
80102dbe:	83 c8 40             	or     $0x40,%eax
80102dc1:	a3 5c c6 10 80       	mov    %eax,0x8010c65c
    return 0;
80102dc6:	b8 00 00 00 00       	mov    $0x0,%eax
80102dcb:	e9 f3 00 00 00       	jmp    80102ec3 <kbdgetc+0x14d>
  } else if(data & 0x80){
80102dd0:	8b 45 fc             	mov    -0x4(%ebp),%eax
80102dd3:	25 80 00 00 00       	and    $0x80,%eax
80102dd8:	85 c0                	test   %eax,%eax
80102dda:	74 45                	je     80102e21 <kbdgetc+0xab>
    // Key released
    data = (shift & E0ESC ? data : data & 0x7F);
80102ddc:	a1 5c c6 10 80       	mov    0x8010c65c,%eax
80102de1:	83 e0 40             	and    $0x40,%eax
80102de4:	85 c0                	test   %eax,%eax
80102de6:	75 08                	jne    80102df0 <kbdgetc+0x7a>
80102de8:	8b 45 fc             	mov    -0x4(%ebp),%eax
80102deb:	83 e0 7f             	and    $0x7f,%eax
80102dee:	eb 03                	jmp    80102df3 <kbdgetc+0x7d>
80102df0:	8b 45 fc             	mov    -0x4(%ebp),%eax
80102df3:	89 45 fc             	mov    %eax,-0x4(%ebp)
    shift &= ~(shiftcode[data] | E0ESC);
80102df6:	8b 45 fc             	mov    -0x4(%ebp),%eax
80102df9:	05 20 a0 10 80       	add    $0x8010a020,%eax
80102dfe:	0f b6 00             	movzbl (%eax),%eax
80102e01:	83 c8 40             	or     $0x40,%eax
80102e04:	0f b6 c0             	movzbl %al,%eax
80102e07:	f7 d0                	not    %eax
80102e09:	89 c2                	mov    %eax,%edx
80102e0b:	a1 5c c6 10 80       	mov    0x8010c65c,%eax
80102e10:	21 d0                	and    %edx,%eax
80102e12:	a3 5c c6 10 80       	mov    %eax,0x8010c65c
    return 0;
80102e17:	b8 00 00 00 00       	mov    $0x0,%eax
80102e1c:	e9 a2 00 00 00       	jmp    80102ec3 <kbdgetc+0x14d>
  } else if(shift & E0ESC){
80102e21:	a1 5c c6 10 80       	mov    0x8010c65c,%eax
80102e26:	83 e0 40             	and    $0x40,%eax
80102e29:	85 c0                	test   %eax,%eax
80102e2b:	74 14                	je     80102e41 <kbdgetc+0xcb>
    // Last character was an E0 escape; or with 0x80
    data |= 0x80;
80102e2d:	81 4d fc 80 00 00 00 	orl    $0x80,-0x4(%ebp)
    shift &= ~E0ESC;
80102e34:	a1 5c c6 10 80       	mov    0x8010c65c,%eax
80102e39:	83 e0 bf             	and    $0xffffffbf,%eax
80102e3c:	a3 5c c6 10 80       	mov    %eax,0x8010c65c
  }

  shift |= shiftcode[data];
80102e41:	8b 45 fc             	mov    -0x4(%ebp),%eax
80102e44:	05 20 a0 10 80       	add    $0x8010a020,%eax
80102e49:	0f b6 00             	movzbl (%eax),%eax
80102e4c:	0f b6 d0             	movzbl %al,%edx
80102e4f:	a1 5c c6 10 80       	mov    0x8010c65c,%eax
80102e54:	09 d0                	or     %edx,%eax
80102e56:	a3 5c c6 10 80       	mov    %eax,0x8010c65c
  shift ^= togglecode[data];
80102e5b:	8b 45 fc             	mov    -0x4(%ebp),%eax
80102e5e:	05 20 a1 10 80       	add    $0x8010a120,%eax
80102e63:	0f b6 00             	movzbl (%eax),%eax
80102e66:	0f b6 d0             	movzbl %al,%edx
80102e69:	a1 5c c6 10 80       	mov    0x8010c65c,%eax
80102e6e:	31 d0                	xor    %edx,%eax
80102e70:	a3 5c c6 10 80       	mov    %eax,0x8010c65c
  c = charcode[shift & (CTL | SHIFT)][data];
80102e75:	a1 5c c6 10 80       	mov    0x8010c65c,%eax
80102e7a:	83 e0 03             	and    $0x3,%eax
80102e7d:	8b 14 85 20 a5 10 80 	mov    -0x7fef5ae0(,%eax,4),%edx
80102e84:	8b 45 fc             	mov    -0x4(%ebp),%eax
80102e87:	01 d0                	add    %edx,%eax
80102e89:	0f b6 00             	movzbl (%eax),%eax
80102e8c:	0f b6 c0             	movzbl %al,%eax
80102e8f:	89 45 f8             	mov    %eax,-0x8(%ebp)
  if(shift & CAPSLOCK){
80102e92:	a1 5c c6 10 80       	mov    0x8010c65c,%eax
80102e97:	83 e0 08             	and    $0x8,%eax
80102e9a:	85 c0                	test   %eax,%eax
80102e9c:	74 22                	je     80102ec0 <kbdgetc+0x14a>
    if('a' <= c && c <= 'z')
80102e9e:	83 7d f8 60          	cmpl   $0x60,-0x8(%ebp)
80102ea2:	76 0c                	jbe    80102eb0 <kbdgetc+0x13a>
80102ea4:	83 7d f8 7a          	cmpl   $0x7a,-0x8(%ebp)
80102ea8:	77 06                	ja     80102eb0 <kbdgetc+0x13a>
      c += 'A' - 'a';
80102eaa:	83 6d f8 20          	subl   $0x20,-0x8(%ebp)
80102eae:	eb 10                	jmp    80102ec0 <kbdgetc+0x14a>
    else if('A' <= c && c <= 'Z')
80102eb0:	83 7d f8 40          	cmpl   $0x40,-0x8(%ebp)
80102eb4:	76 0a                	jbe    80102ec0 <kbdgetc+0x14a>
80102eb6:	83 7d f8 5a          	cmpl   $0x5a,-0x8(%ebp)
80102eba:	77 04                	ja     80102ec0 <kbdgetc+0x14a>
      c += 'a' - 'A';
80102ebc:	83 45 f8 20          	addl   $0x20,-0x8(%ebp)
  }
  return c;
80102ec0:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
80102ec3:	c9                   	leave  
80102ec4:	c3                   	ret    

80102ec5 <kbdintr>:

void
kbdintr(void)
{
80102ec5:	55                   	push   %ebp
80102ec6:	89 e5                	mov    %esp,%ebp
80102ec8:	83 ec 08             	sub    $0x8,%esp
  consoleintr(kbdgetc);
80102ecb:	83 ec 0c             	sub    $0xc,%esp
80102ece:	68 76 2d 10 80       	push   $0x80102d76
80102ed3:	e8 21 d9 ff ff       	call   801007f9 <consoleintr>
80102ed8:	83 c4 10             	add    $0x10,%esp
}
80102edb:	90                   	nop
80102edc:	c9                   	leave  
80102edd:	c3                   	ret    

80102ede <inb>:

// end of CS333 added routines

static inline uchar
inb(ushort port)
{
80102ede:	55                   	push   %ebp
80102edf:	89 e5                	mov    %esp,%ebp
80102ee1:	83 ec 14             	sub    $0x14,%esp
80102ee4:	8b 45 08             	mov    0x8(%ebp),%eax
80102ee7:	66 89 45 ec          	mov    %ax,-0x14(%ebp)
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102eeb:	0f b7 45 ec          	movzwl -0x14(%ebp),%eax
80102eef:	89 c2                	mov    %eax,%edx
80102ef1:	ec                   	in     (%dx),%al
80102ef2:	88 45 ff             	mov    %al,-0x1(%ebp)
  return data;
80102ef5:	0f b6 45 ff          	movzbl -0x1(%ebp),%eax
}
80102ef9:	c9                   	leave  
80102efa:	c3                   	ret    

80102efb <outb>:
               "memory", "cc");
}

static inline void
outb(ushort port, uchar data)
{
80102efb:	55                   	push   %ebp
80102efc:	89 e5                	mov    %esp,%ebp
80102efe:	83 ec 08             	sub    $0x8,%esp
80102f01:	8b 55 08             	mov    0x8(%ebp),%edx
80102f04:	8b 45 0c             	mov    0xc(%ebp),%eax
80102f07:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
80102f0b:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102f0e:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
80102f12:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
80102f16:	ee                   	out    %al,(%dx)
}
80102f17:	90                   	nop
80102f18:	c9                   	leave  
80102f19:	c3                   	ret    

80102f1a <readeflags>:
  asm volatile("ltr %0" : : "r" (sel));
}

static inline uint
readeflags(void)
{
80102f1a:	55                   	push   %ebp
80102f1b:	89 e5                	mov    %esp,%ebp
80102f1d:	83 ec 10             	sub    $0x10,%esp
  uint eflags;
  asm volatile("pushfl; popl %0" : "=r" (eflags));
80102f20:	9c                   	pushf  
80102f21:	58                   	pop    %eax
80102f22:	89 45 fc             	mov    %eax,-0x4(%ebp)
  return eflags;
80102f25:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80102f28:	c9                   	leave  
80102f29:	c3                   	ret    

80102f2a <lapicw>:

volatile uint *lapic;  // Initialized in mp.c

static void
lapicw(int index, int value)
{
80102f2a:	55                   	push   %ebp
80102f2b:	89 e5                	mov    %esp,%ebp
  lapic[index] = value;
80102f2d:	a1 7c 32 11 80       	mov    0x8011327c,%eax
80102f32:	8b 55 08             	mov    0x8(%ebp),%edx
80102f35:	c1 e2 02             	shl    $0x2,%edx
80102f38:	01 c2                	add    %eax,%edx
80102f3a:	8b 45 0c             	mov    0xc(%ebp),%eax
80102f3d:	89 02                	mov    %eax,(%edx)
  lapic[ID];  // wait for write to finish, by reading
80102f3f:	a1 7c 32 11 80       	mov    0x8011327c,%eax
80102f44:	83 c0 20             	add    $0x20,%eax
80102f47:	8b 00                	mov    (%eax),%eax
}
80102f49:	90                   	nop
80102f4a:	5d                   	pop    %ebp
80102f4b:	c3                   	ret    

80102f4c <lapicinit>:

void
lapicinit(void)
{
80102f4c:	55                   	push   %ebp
80102f4d:	89 e5                	mov    %esp,%ebp
  if(!lapic) 
80102f4f:	a1 7c 32 11 80       	mov    0x8011327c,%eax
80102f54:	85 c0                	test   %eax,%eax
80102f56:	0f 84 0b 01 00 00    	je     80103067 <lapicinit+0x11b>
    return;

  // Enable local APIC; set spurious interrupt vector.
  lapicw(SVR, ENABLE | (T_IRQ0 + IRQ_SPURIOUS));
80102f5c:	68 3f 01 00 00       	push   $0x13f
80102f61:	6a 3c                	push   $0x3c
80102f63:	e8 c2 ff ff ff       	call   80102f2a <lapicw>
80102f68:	83 c4 08             	add    $0x8,%esp

  // The timer repeatedly counts down at bus frequency
  // from lapic[TICR] and then issues an interrupt.  
  // If xv6 cared more about precise timekeeping,
  // TICR would be calibrated using an external time source.
  lapicw(TDCR, X1);
80102f6b:	6a 0b                	push   $0xb
80102f6d:	68 f8 00 00 00       	push   $0xf8
80102f72:	e8 b3 ff ff ff       	call   80102f2a <lapicw>
80102f77:	83 c4 08             	add    $0x8,%esp
  lapicw(TIMER, PERIODIC | (T_IRQ0 + IRQ_TIMER));
80102f7a:	68 20 00 02 00       	push   $0x20020
80102f7f:	68 c8 00 00 00       	push   $0xc8
80102f84:	e8 a1 ff ff ff       	call   80102f2a <lapicw>
80102f89:	83 c4 08             	add    $0x8,%esp
  lapicw(TICR, 10000000); 
80102f8c:	68 80 96 98 00       	push   $0x989680
80102f91:	68 e0 00 00 00       	push   $0xe0
80102f96:	e8 8f ff ff ff       	call   80102f2a <lapicw>
80102f9b:	83 c4 08             	add    $0x8,%esp

  // Disable logical interrupt lines.
  lapicw(LINT0, MASKED);
80102f9e:	68 00 00 01 00       	push   $0x10000
80102fa3:	68 d4 00 00 00       	push   $0xd4
80102fa8:	e8 7d ff ff ff       	call   80102f2a <lapicw>
80102fad:	83 c4 08             	add    $0x8,%esp
  lapicw(LINT1, MASKED);
80102fb0:	68 00 00 01 00       	push   $0x10000
80102fb5:	68 d8 00 00 00       	push   $0xd8
80102fba:	e8 6b ff ff ff       	call   80102f2a <lapicw>
80102fbf:	83 c4 08             	add    $0x8,%esp

  // Disable performance counter overflow interrupts
  // on machines that provide that interrupt entry.
  if(((lapic[VER]>>16) & 0xFF) >= 4)
80102fc2:	a1 7c 32 11 80       	mov    0x8011327c,%eax
80102fc7:	83 c0 30             	add    $0x30,%eax
80102fca:	8b 00                	mov    (%eax),%eax
80102fcc:	c1 e8 10             	shr    $0x10,%eax
80102fcf:	0f b6 c0             	movzbl %al,%eax
80102fd2:	83 f8 03             	cmp    $0x3,%eax
80102fd5:	76 12                	jbe    80102fe9 <lapicinit+0x9d>
    lapicw(PCINT, MASKED);
80102fd7:	68 00 00 01 00       	push   $0x10000
80102fdc:	68 d0 00 00 00       	push   $0xd0
80102fe1:	e8 44 ff ff ff       	call   80102f2a <lapicw>
80102fe6:	83 c4 08             	add    $0x8,%esp

  // Map error interrupt to IRQ_ERROR.
  lapicw(ERROR, T_IRQ0 + IRQ_ERROR);
80102fe9:	6a 33                	push   $0x33
80102feb:	68 dc 00 00 00       	push   $0xdc
80102ff0:	e8 35 ff ff ff       	call   80102f2a <lapicw>
80102ff5:	83 c4 08             	add    $0x8,%esp

  // Clear error status register (requires back-to-back writes).
  lapicw(ESR, 0);
80102ff8:	6a 00                	push   $0x0
80102ffa:	68 a0 00 00 00       	push   $0xa0
80102fff:	e8 26 ff ff ff       	call   80102f2a <lapicw>
80103004:	83 c4 08             	add    $0x8,%esp
  lapicw(ESR, 0);
80103007:	6a 00                	push   $0x0
80103009:	68 a0 00 00 00       	push   $0xa0
8010300e:	e8 17 ff ff ff       	call   80102f2a <lapicw>
80103013:	83 c4 08             	add    $0x8,%esp

  // Ack any outstanding interrupts.
  lapicw(EOI, 0);
80103016:	6a 00                	push   $0x0
80103018:	6a 2c                	push   $0x2c
8010301a:	e8 0b ff ff ff       	call   80102f2a <lapicw>
8010301f:	83 c4 08             	add    $0x8,%esp

  // Send an Init Level De-Assert to synchronise arbitration ID's.
  lapicw(ICRHI, 0);
80103022:	6a 00                	push   $0x0
80103024:	68 c4 00 00 00       	push   $0xc4
80103029:	e8 fc fe ff ff       	call   80102f2a <lapicw>
8010302e:	83 c4 08             	add    $0x8,%esp
  lapicw(ICRLO, BCAST | INIT | LEVEL);
80103031:	68 00 85 08 00       	push   $0x88500
80103036:	68 c0 00 00 00       	push   $0xc0
8010303b:	e8 ea fe ff ff       	call   80102f2a <lapicw>
80103040:	83 c4 08             	add    $0x8,%esp
  while(lapic[ICRLO] & DELIVS)
80103043:	90                   	nop
80103044:	a1 7c 32 11 80       	mov    0x8011327c,%eax
80103049:	05 00 03 00 00       	add    $0x300,%eax
8010304e:	8b 00                	mov    (%eax),%eax
80103050:	25 00 10 00 00       	and    $0x1000,%eax
80103055:	85 c0                	test   %eax,%eax
80103057:	75 eb                	jne    80103044 <lapicinit+0xf8>
    ;

  // Enable interrupts on the APIC (but not on the processor).
  lapicw(TPR, 0);
80103059:	6a 00                	push   $0x0
8010305b:	6a 20                	push   $0x20
8010305d:	e8 c8 fe ff ff       	call   80102f2a <lapicw>
80103062:	83 c4 08             	add    $0x8,%esp
80103065:	eb 01                	jmp    80103068 <lapicinit+0x11c>

void
lapicinit(void)
{
  if(!lapic) 
    return;
80103067:	90                   	nop
  while(lapic[ICRLO] & DELIVS)
    ;

  // Enable interrupts on the APIC (but not on the processor).
  lapicw(TPR, 0);
}
80103068:	c9                   	leave  
80103069:	c3                   	ret    

8010306a <cpunum>:

int
cpunum(void)
{
8010306a:	55                   	push   %ebp
8010306b:	89 e5                	mov    %esp,%ebp
8010306d:	83 ec 08             	sub    $0x8,%esp
  // Cannot call cpu when interrupts are enabled:
  // result not guaranteed to last long enough to be used!
  // Would prefer to panic but even printing is chancy here:
  // almost everything, including cprintf and panic, calls cpu,
  // often indirectly through acquire and release.
  if(readeflags()&FL_IF){
80103070:	e8 a5 fe ff ff       	call   80102f1a <readeflags>
80103075:	25 00 02 00 00       	and    $0x200,%eax
8010307a:	85 c0                	test   %eax,%eax
8010307c:	74 26                	je     801030a4 <cpunum+0x3a>
    static int n;
    if(n++ == 0)
8010307e:	a1 60 c6 10 80       	mov    0x8010c660,%eax
80103083:	8d 50 01             	lea    0x1(%eax),%edx
80103086:	89 15 60 c6 10 80    	mov    %edx,0x8010c660
8010308c:	85 c0                	test   %eax,%eax
8010308e:	75 14                	jne    801030a4 <cpunum+0x3a>
      cprintf("cpu called from %x with interrupts enabled\n",
80103090:	8b 45 04             	mov    0x4(%ebp),%eax
80103093:	83 ec 08             	sub    $0x8,%esp
80103096:	50                   	push   %eax
80103097:	68 54 94 10 80       	push   $0x80109454
8010309c:	e8 25 d3 ff ff       	call   801003c6 <cprintf>
801030a1:	83 c4 10             	add    $0x10,%esp
        __builtin_return_address(0));
  }

  if(lapic)
801030a4:	a1 7c 32 11 80       	mov    0x8011327c,%eax
801030a9:	85 c0                	test   %eax,%eax
801030ab:	74 0f                	je     801030bc <cpunum+0x52>
    return lapic[ID]>>24;
801030ad:	a1 7c 32 11 80       	mov    0x8011327c,%eax
801030b2:	83 c0 20             	add    $0x20,%eax
801030b5:	8b 00                	mov    (%eax),%eax
801030b7:	c1 e8 18             	shr    $0x18,%eax
801030ba:	eb 05                	jmp    801030c1 <cpunum+0x57>
  return 0;
801030bc:	b8 00 00 00 00       	mov    $0x0,%eax
}
801030c1:	c9                   	leave  
801030c2:	c3                   	ret    

801030c3 <lapiceoi>:

// Acknowledge interrupt.
void
lapiceoi(void)
{
801030c3:	55                   	push   %ebp
801030c4:	89 e5                	mov    %esp,%ebp
  if(lapic)
801030c6:	a1 7c 32 11 80       	mov    0x8011327c,%eax
801030cb:	85 c0                	test   %eax,%eax
801030cd:	74 0c                	je     801030db <lapiceoi+0x18>
    lapicw(EOI, 0);
801030cf:	6a 00                	push   $0x0
801030d1:	6a 2c                	push   $0x2c
801030d3:	e8 52 fe ff ff       	call   80102f2a <lapicw>
801030d8:	83 c4 08             	add    $0x8,%esp
}
801030db:	90                   	nop
801030dc:	c9                   	leave  
801030dd:	c3                   	ret    

801030de <microdelay>:

// Spin for a given number of microseconds.
// On real hardware would want to tune this dynamically.
void
microdelay(int us)
{
801030de:	55                   	push   %ebp
801030df:	89 e5                	mov    %esp,%ebp
}
801030e1:	90                   	nop
801030e2:	5d                   	pop    %ebp
801030e3:	c3                   	ret    

801030e4 <lapicstartap>:

// Start additional processor running entry code at addr.
// See Appendix B of MultiProcessor Specification.
void
lapicstartap(uchar apicid, uint addr)
{
801030e4:	55                   	push   %ebp
801030e5:	89 e5                	mov    %esp,%ebp
801030e7:	83 ec 14             	sub    $0x14,%esp
801030ea:	8b 45 08             	mov    0x8(%ebp),%eax
801030ed:	88 45 ec             	mov    %al,-0x14(%ebp)
  ushort *wrv;
  
  // "The BSP must initialize CMOS shutdown code to 0AH
  // and the warm reset vector (DWORD based at 40:67) to point at
  // the AP startup code prior to the [universal startup algorithm]."
  outb(CMOS_PORT, 0xF);  // offset 0xF is shutdown code
801030f0:	6a 0f                	push   $0xf
801030f2:	6a 70                	push   $0x70
801030f4:	e8 02 fe ff ff       	call   80102efb <outb>
801030f9:	83 c4 08             	add    $0x8,%esp
  outb(CMOS_PORT+1, 0x0A);
801030fc:	6a 0a                	push   $0xa
801030fe:	6a 71                	push   $0x71
80103100:	e8 f6 fd ff ff       	call   80102efb <outb>
80103105:	83 c4 08             	add    $0x8,%esp
  wrv = (ushort*)P2V((0x40<<4 | 0x67));  // Warm reset vector
80103108:	c7 45 f8 67 04 00 80 	movl   $0x80000467,-0x8(%ebp)
  wrv[0] = 0;
8010310f:	8b 45 f8             	mov    -0x8(%ebp),%eax
80103112:	66 c7 00 00 00       	movw   $0x0,(%eax)
  wrv[1] = addr >> 4;
80103117:	8b 45 f8             	mov    -0x8(%ebp),%eax
8010311a:	83 c0 02             	add    $0x2,%eax
8010311d:	8b 55 0c             	mov    0xc(%ebp),%edx
80103120:	c1 ea 04             	shr    $0x4,%edx
80103123:	66 89 10             	mov    %dx,(%eax)

  // "Universal startup algorithm."
  // Send INIT (level-triggered) interrupt to reset other CPU.
  lapicw(ICRHI, apicid<<24);
80103126:	0f b6 45 ec          	movzbl -0x14(%ebp),%eax
8010312a:	c1 e0 18             	shl    $0x18,%eax
8010312d:	50                   	push   %eax
8010312e:	68 c4 00 00 00       	push   $0xc4
80103133:	e8 f2 fd ff ff       	call   80102f2a <lapicw>
80103138:	83 c4 08             	add    $0x8,%esp
  lapicw(ICRLO, INIT | LEVEL | ASSERT);
8010313b:	68 00 c5 00 00       	push   $0xc500
80103140:	68 c0 00 00 00       	push   $0xc0
80103145:	e8 e0 fd ff ff       	call   80102f2a <lapicw>
8010314a:	83 c4 08             	add    $0x8,%esp
  microdelay(200);
8010314d:	68 c8 00 00 00       	push   $0xc8
80103152:	e8 87 ff ff ff       	call   801030de <microdelay>
80103157:	83 c4 04             	add    $0x4,%esp
  lapicw(ICRLO, INIT | LEVEL);
8010315a:	68 00 85 00 00       	push   $0x8500
8010315f:	68 c0 00 00 00       	push   $0xc0
80103164:	e8 c1 fd ff ff       	call   80102f2a <lapicw>
80103169:	83 c4 08             	add    $0x8,%esp
  microdelay(100);    // should be 10ms, but too slow in Bochs!
8010316c:	6a 64                	push   $0x64
8010316e:	e8 6b ff ff ff       	call   801030de <microdelay>
80103173:	83 c4 04             	add    $0x4,%esp
  // Send startup IPI (twice!) to enter code.
  // Regular hardware is supposed to only accept a STARTUP
  // when it is in the halted state due to an INIT.  So the second
  // should be ignored, but it is part of the official Intel algorithm.
  // Bochs complains about the second one.  Too bad for Bochs.
  for(i = 0; i < 2; i++){
80103176:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
8010317d:	eb 3d                	jmp    801031bc <lapicstartap+0xd8>
    lapicw(ICRHI, apicid<<24);
8010317f:	0f b6 45 ec          	movzbl -0x14(%ebp),%eax
80103183:	c1 e0 18             	shl    $0x18,%eax
80103186:	50                   	push   %eax
80103187:	68 c4 00 00 00       	push   $0xc4
8010318c:	e8 99 fd ff ff       	call   80102f2a <lapicw>
80103191:	83 c4 08             	add    $0x8,%esp
    lapicw(ICRLO, STARTUP | (addr>>12));
80103194:	8b 45 0c             	mov    0xc(%ebp),%eax
80103197:	c1 e8 0c             	shr    $0xc,%eax
8010319a:	80 cc 06             	or     $0x6,%ah
8010319d:	50                   	push   %eax
8010319e:	68 c0 00 00 00       	push   $0xc0
801031a3:	e8 82 fd ff ff       	call   80102f2a <lapicw>
801031a8:	83 c4 08             	add    $0x8,%esp
    microdelay(200);
801031ab:	68 c8 00 00 00       	push   $0xc8
801031b0:	e8 29 ff ff ff       	call   801030de <microdelay>
801031b5:	83 c4 04             	add    $0x4,%esp
  // Send startup IPI (twice!) to enter code.
  // Regular hardware is supposed to only accept a STARTUP
  // when it is in the halted state due to an INIT.  So the second
  // should be ignored, but it is part of the official Intel algorithm.
  // Bochs complains about the second one.  Too bad for Bochs.
  for(i = 0; i < 2; i++){
801031b8:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
801031bc:	83 7d fc 01          	cmpl   $0x1,-0x4(%ebp)
801031c0:	7e bd                	jle    8010317f <lapicstartap+0x9b>
    lapicw(ICRHI, apicid<<24);
    lapicw(ICRLO, STARTUP | (addr>>12));
    microdelay(200);
  }
}
801031c2:	90                   	nop
801031c3:	c9                   	leave  
801031c4:	c3                   	ret    

801031c5 <cmos_read>:
#define DAY     0x07
#define MONTH   0x08
#define YEAR    0x09

static uint cmos_read(uint reg)
{
801031c5:	55                   	push   %ebp
801031c6:	89 e5                	mov    %esp,%ebp
  outb(CMOS_PORT,  reg);
801031c8:	8b 45 08             	mov    0x8(%ebp),%eax
801031cb:	0f b6 c0             	movzbl %al,%eax
801031ce:	50                   	push   %eax
801031cf:	6a 70                	push   $0x70
801031d1:	e8 25 fd ff ff       	call   80102efb <outb>
801031d6:	83 c4 08             	add    $0x8,%esp
  microdelay(200);
801031d9:	68 c8 00 00 00       	push   $0xc8
801031de:	e8 fb fe ff ff       	call   801030de <microdelay>
801031e3:	83 c4 04             	add    $0x4,%esp

  return inb(CMOS_RETURN);
801031e6:	6a 71                	push   $0x71
801031e8:	e8 f1 fc ff ff       	call   80102ede <inb>
801031ed:	83 c4 04             	add    $0x4,%esp
801031f0:	0f b6 c0             	movzbl %al,%eax
}
801031f3:	c9                   	leave  
801031f4:	c3                   	ret    

801031f5 <fill_rtcdate>:

static void fill_rtcdate(struct rtcdate *r)
{
801031f5:	55                   	push   %ebp
801031f6:	89 e5                	mov    %esp,%ebp
  r->second = cmos_read(SECS);
801031f8:	6a 00                	push   $0x0
801031fa:	e8 c6 ff ff ff       	call   801031c5 <cmos_read>
801031ff:	83 c4 04             	add    $0x4,%esp
80103202:	89 c2                	mov    %eax,%edx
80103204:	8b 45 08             	mov    0x8(%ebp),%eax
80103207:	89 10                	mov    %edx,(%eax)
  r->minute = cmos_read(MINS);
80103209:	6a 02                	push   $0x2
8010320b:	e8 b5 ff ff ff       	call   801031c5 <cmos_read>
80103210:	83 c4 04             	add    $0x4,%esp
80103213:	89 c2                	mov    %eax,%edx
80103215:	8b 45 08             	mov    0x8(%ebp),%eax
80103218:	89 50 04             	mov    %edx,0x4(%eax)
  r->hour   = cmos_read(HOURS);
8010321b:	6a 04                	push   $0x4
8010321d:	e8 a3 ff ff ff       	call   801031c5 <cmos_read>
80103222:	83 c4 04             	add    $0x4,%esp
80103225:	89 c2                	mov    %eax,%edx
80103227:	8b 45 08             	mov    0x8(%ebp),%eax
8010322a:	89 50 08             	mov    %edx,0x8(%eax)
  r->day    = cmos_read(DAY);
8010322d:	6a 07                	push   $0x7
8010322f:	e8 91 ff ff ff       	call   801031c5 <cmos_read>
80103234:	83 c4 04             	add    $0x4,%esp
80103237:	89 c2                	mov    %eax,%edx
80103239:	8b 45 08             	mov    0x8(%ebp),%eax
8010323c:	89 50 0c             	mov    %edx,0xc(%eax)
  r->month  = cmos_read(MONTH);
8010323f:	6a 08                	push   $0x8
80103241:	e8 7f ff ff ff       	call   801031c5 <cmos_read>
80103246:	83 c4 04             	add    $0x4,%esp
80103249:	89 c2                	mov    %eax,%edx
8010324b:	8b 45 08             	mov    0x8(%ebp),%eax
8010324e:	89 50 10             	mov    %edx,0x10(%eax)
  r->year   = cmos_read(YEAR);
80103251:	6a 09                	push   $0x9
80103253:	e8 6d ff ff ff       	call   801031c5 <cmos_read>
80103258:	83 c4 04             	add    $0x4,%esp
8010325b:	89 c2                	mov    %eax,%edx
8010325d:	8b 45 08             	mov    0x8(%ebp),%eax
80103260:	89 50 14             	mov    %edx,0x14(%eax)
}
80103263:	90                   	nop
80103264:	c9                   	leave  
80103265:	c3                   	ret    

80103266 <cmostime>:

// qemu seems to use 24-hour GWT and the values are BCD encoded
void cmostime(struct rtcdate *r)
{
80103266:	55                   	push   %ebp
80103267:	89 e5                	mov    %esp,%ebp
80103269:	83 ec 48             	sub    $0x48,%esp
  struct rtcdate t1, t2;
  int sb, bcd;

  sb = cmos_read(CMOS_STATB);
8010326c:	6a 0b                	push   $0xb
8010326e:	e8 52 ff ff ff       	call   801031c5 <cmos_read>
80103273:	83 c4 04             	add    $0x4,%esp
80103276:	89 45 f4             	mov    %eax,-0xc(%ebp)

  bcd = (sb & (1 << 2)) == 0;
80103279:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010327c:	83 e0 04             	and    $0x4,%eax
8010327f:	85 c0                	test   %eax,%eax
80103281:	0f 94 c0             	sete   %al
80103284:	0f b6 c0             	movzbl %al,%eax
80103287:	89 45 f0             	mov    %eax,-0x10(%ebp)

  // make sure CMOS doesn't modify time while we read it
  for (;;) {
    fill_rtcdate(&t1);
8010328a:	8d 45 d8             	lea    -0x28(%ebp),%eax
8010328d:	50                   	push   %eax
8010328e:	e8 62 ff ff ff       	call   801031f5 <fill_rtcdate>
80103293:	83 c4 04             	add    $0x4,%esp
    if (cmos_read(CMOS_STATA) & CMOS_UIP)
80103296:	6a 0a                	push   $0xa
80103298:	e8 28 ff ff ff       	call   801031c5 <cmos_read>
8010329d:	83 c4 04             	add    $0x4,%esp
801032a0:	25 80 00 00 00       	and    $0x80,%eax
801032a5:	85 c0                	test   %eax,%eax
801032a7:	75 27                	jne    801032d0 <cmostime+0x6a>
        continue;
    fill_rtcdate(&t2);
801032a9:	8d 45 c0             	lea    -0x40(%ebp),%eax
801032ac:	50                   	push   %eax
801032ad:	e8 43 ff ff ff       	call   801031f5 <fill_rtcdate>
801032b2:	83 c4 04             	add    $0x4,%esp
    if (memcmp(&t1, &t2, sizeof(t1)) == 0)
801032b5:	83 ec 04             	sub    $0x4,%esp
801032b8:	6a 18                	push   $0x18
801032ba:	8d 45 c0             	lea    -0x40(%ebp),%eax
801032bd:	50                   	push   %eax
801032be:	8d 45 d8             	lea    -0x28(%ebp),%eax
801032c1:	50                   	push   %eax
801032c2:	e8 4b 2b 00 00       	call   80105e12 <memcmp>
801032c7:	83 c4 10             	add    $0x10,%esp
801032ca:	85 c0                	test   %eax,%eax
801032cc:	74 05                	je     801032d3 <cmostime+0x6d>
801032ce:	eb ba                	jmp    8010328a <cmostime+0x24>

  // make sure CMOS doesn't modify time while we read it
  for (;;) {
    fill_rtcdate(&t1);
    if (cmos_read(CMOS_STATA) & CMOS_UIP)
        continue;
801032d0:	90                   	nop
    fill_rtcdate(&t2);
    if (memcmp(&t1, &t2, sizeof(t1)) == 0)
      break;
  }
801032d1:	eb b7                	jmp    8010328a <cmostime+0x24>
    fill_rtcdate(&t1);
    if (cmos_read(CMOS_STATA) & CMOS_UIP)
        continue;
    fill_rtcdate(&t2);
    if (memcmp(&t1, &t2, sizeof(t1)) == 0)
      break;
801032d3:	90                   	nop
  }

  // convert
  if (bcd) {
801032d4:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
801032d8:	0f 84 b4 00 00 00    	je     80103392 <cmostime+0x12c>
#define    CONV(x)     (t1.x = ((t1.x >> 4) * 10) + (t1.x & 0xf))
    CONV(second);
801032de:	8b 45 d8             	mov    -0x28(%ebp),%eax
801032e1:	c1 e8 04             	shr    $0x4,%eax
801032e4:	89 c2                	mov    %eax,%edx
801032e6:	89 d0                	mov    %edx,%eax
801032e8:	c1 e0 02             	shl    $0x2,%eax
801032eb:	01 d0                	add    %edx,%eax
801032ed:	01 c0                	add    %eax,%eax
801032ef:	89 c2                	mov    %eax,%edx
801032f1:	8b 45 d8             	mov    -0x28(%ebp),%eax
801032f4:	83 e0 0f             	and    $0xf,%eax
801032f7:	01 d0                	add    %edx,%eax
801032f9:	89 45 d8             	mov    %eax,-0x28(%ebp)
    CONV(minute);
801032fc:	8b 45 dc             	mov    -0x24(%ebp),%eax
801032ff:	c1 e8 04             	shr    $0x4,%eax
80103302:	89 c2                	mov    %eax,%edx
80103304:	89 d0                	mov    %edx,%eax
80103306:	c1 e0 02             	shl    $0x2,%eax
80103309:	01 d0                	add    %edx,%eax
8010330b:	01 c0                	add    %eax,%eax
8010330d:	89 c2                	mov    %eax,%edx
8010330f:	8b 45 dc             	mov    -0x24(%ebp),%eax
80103312:	83 e0 0f             	and    $0xf,%eax
80103315:	01 d0                	add    %edx,%eax
80103317:	89 45 dc             	mov    %eax,-0x24(%ebp)
    CONV(hour  );
8010331a:	8b 45 e0             	mov    -0x20(%ebp),%eax
8010331d:	c1 e8 04             	shr    $0x4,%eax
80103320:	89 c2                	mov    %eax,%edx
80103322:	89 d0                	mov    %edx,%eax
80103324:	c1 e0 02             	shl    $0x2,%eax
80103327:	01 d0                	add    %edx,%eax
80103329:	01 c0                	add    %eax,%eax
8010332b:	89 c2                	mov    %eax,%edx
8010332d:	8b 45 e0             	mov    -0x20(%ebp),%eax
80103330:	83 e0 0f             	and    $0xf,%eax
80103333:	01 d0                	add    %edx,%eax
80103335:	89 45 e0             	mov    %eax,-0x20(%ebp)
    CONV(day   );
80103338:	8b 45 e4             	mov    -0x1c(%ebp),%eax
8010333b:	c1 e8 04             	shr    $0x4,%eax
8010333e:	89 c2                	mov    %eax,%edx
80103340:	89 d0                	mov    %edx,%eax
80103342:	c1 e0 02             	shl    $0x2,%eax
80103345:	01 d0                	add    %edx,%eax
80103347:	01 c0                	add    %eax,%eax
80103349:	89 c2                	mov    %eax,%edx
8010334b:	8b 45 e4             	mov    -0x1c(%ebp),%eax
8010334e:	83 e0 0f             	and    $0xf,%eax
80103351:	01 d0                	add    %edx,%eax
80103353:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    CONV(month );
80103356:	8b 45 e8             	mov    -0x18(%ebp),%eax
80103359:	c1 e8 04             	shr    $0x4,%eax
8010335c:	89 c2                	mov    %eax,%edx
8010335e:	89 d0                	mov    %edx,%eax
80103360:	c1 e0 02             	shl    $0x2,%eax
80103363:	01 d0                	add    %edx,%eax
80103365:	01 c0                	add    %eax,%eax
80103367:	89 c2                	mov    %eax,%edx
80103369:	8b 45 e8             	mov    -0x18(%ebp),%eax
8010336c:	83 e0 0f             	and    $0xf,%eax
8010336f:	01 d0                	add    %edx,%eax
80103371:	89 45 e8             	mov    %eax,-0x18(%ebp)
    CONV(year  );
80103374:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103377:	c1 e8 04             	shr    $0x4,%eax
8010337a:	89 c2                	mov    %eax,%edx
8010337c:	89 d0                	mov    %edx,%eax
8010337e:	c1 e0 02             	shl    $0x2,%eax
80103381:	01 d0                	add    %edx,%eax
80103383:	01 c0                	add    %eax,%eax
80103385:	89 c2                	mov    %eax,%edx
80103387:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010338a:	83 e0 0f             	and    $0xf,%eax
8010338d:	01 d0                	add    %edx,%eax
8010338f:	89 45 ec             	mov    %eax,-0x14(%ebp)
#undef     CONV
  }

  *r = t1;
80103392:	8b 45 08             	mov    0x8(%ebp),%eax
80103395:	8b 55 d8             	mov    -0x28(%ebp),%edx
80103398:	89 10                	mov    %edx,(%eax)
8010339a:	8b 55 dc             	mov    -0x24(%ebp),%edx
8010339d:	89 50 04             	mov    %edx,0x4(%eax)
801033a0:	8b 55 e0             	mov    -0x20(%ebp),%edx
801033a3:	89 50 08             	mov    %edx,0x8(%eax)
801033a6:	8b 55 e4             	mov    -0x1c(%ebp),%edx
801033a9:	89 50 0c             	mov    %edx,0xc(%eax)
801033ac:	8b 55 e8             	mov    -0x18(%ebp),%edx
801033af:	89 50 10             	mov    %edx,0x10(%eax)
801033b2:	8b 55 ec             	mov    -0x14(%ebp),%edx
801033b5:	89 50 14             	mov    %edx,0x14(%eax)
  r->year += 2000;
801033b8:	8b 45 08             	mov    0x8(%ebp),%eax
801033bb:	8b 40 14             	mov    0x14(%eax),%eax
801033be:	8d 90 d0 07 00 00    	lea    0x7d0(%eax),%edx
801033c4:	8b 45 08             	mov    0x8(%ebp),%eax
801033c7:	89 50 14             	mov    %edx,0x14(%eax)
}
801033ca:	90                   	nop
801033cb:	c9                   	leave  
801033cc:	c3                   	ret    

801033cd <initlog>:
static void recover_from_log(void);
static void commit();

void
initlog(int dev)
{
801033cd:	55                   	push   %ebp
801033ce:	89 e5                	mov    %esp,%ebp
801033d0:	83 ec 28             	sub    $0x28,%esp
  if (sizeof(struct logheader) >= BSIZE)
    panic("initlog: too big logheader");

  struct superblock sb;
  initlock(&log.lock, "log");
801033d3:	83 ec 08             	sub    $0x8,%esp
801033d6:	68 80 94 10 80       	push   $0x80109480
801033db:	68 80 32 11 80       	push   $0x80113280
801033e0:	e8 41 27 00 00       	call   80105b26 <initlock>
801033e5:	83 c4 10             	add    $0x10,%esp
  readsb(dev, &sb);
801033e8:	83 ec 08             	sub    $0x8,%esp
801033eb:	8d 45 dc             	lea    -0x24(%ebp),%eax
801033ee:	50                   	push   %eax
801033ef:	ff 75 08             	pushl  0x8(%ebp)
801033f2:	e8 2b e0 ff ff       	call   80101422 <readsb>
801033f7:	83 c4 10             	add    $0x10,%esp
  log.start = sb.logstart;
801033fa:	8b 45 ec             	mov    -0x14(%ebp),%eax
801033fd:	a3 b4 32 11 80       	mov    %eax,0x801132b4
  log.size = sb.nlog;
80103402:	8b 45 e8             	mov    -0x18(%ebp),%eax
80103405:	a3 b8 32 11 80       	mov    %eax,0x801132b8
  log.dev = dev;
8010340a:	8b 45 08             	mov    0x8(%ebp),%eax
8010340d:	a3 c4 32 11 80       	mov    %eax,0x801132c4
  recover_from_log();
80103412:	e8 b2 01 00 00       	call   801035c9 <recover_from_log>
}
80103417:	90                   	nop
80103418:	c9                   	leave  
80103419:	c3                   	ret    

8010341a <install_trans>:

// Copy committed blocks from log to their home location
static void 
install_trans(void)
{
8010341a:	55                   	push   %ebp
8010341b:	89 e5                	mov    %esp,%ebp
8010341d:	83 ec 18             	sub    $0x18,%esp
  int tail;

  for (tail = 0; tail < log.lh.n; tail++) {
80103420:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80103427:	e9 95 00 00 00       	jmp    801034c1 <install_trans+0xa7>
    struct buf *lbuf = bread(log.dev, log.start+tail+1); // read log block
8010342c:	8b 15 b4 32 11 80    	mov    0x801132b4,%edx
80103432:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103435:	01 d0                	add    %edx,%eax
80103437:	83 c0 01             	add    $0x1,%eax
8010343a:	89 c2                	mov    %eax,%edx
8010343c:	a1 c4 32 11 80       	mov    0x801132c4,%eax
80103441:	83 ec 08             	sub    $0x8,%esp
80103444:	52                   	push   %edx
80103445:	50                   	push   %eax
80103446:	e8 6b cd ff ff       	call   801001b6 <bread>
8010344b:	83 c4 10             	add    $0x10,%esp
8010344e:	89 45 f0             	mov    %eax,-0x10(%ebp)
    struct buf *dbuf = bread(log.dev, log.lh.block[tail]); // read dst
80103451:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103454:	83 c0 10             	add    $0x10,%eax
80103457:	8b 04 85 8c 32 11 80 	mov    -0x7feecd74(,%eax,4),%eax
8010345e:	89 c2                	mov    %eax,%edx
80103460:	a1 c4 32 11 80       	mov    0x801132c4,%eax
80103465:	83 ec 08             	sub    $0x8,%esp
80103468:	52                   	push   %edx
80103469:	50                   	push   %eax
8010346a:	e8 47 cd ff ff       	call   801001b6 <bread>
8010346f:	83 c4 10             	add    $0x10,%esp
80103472:	89 45 ec             	mov    %eax,-0x14(%ebp)
    memmove(dbuf->data, lbuf->data, BSIZE);  // copy block to dst
80103475:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103478:	8d 50 18             	lea    0x18(%eax),%edx
8010347b:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010347e:	83 c0 18             	add    $0x18,%eax
80103481:	83 ec 04             	sub    $0x4,%esp
80103484:	68 00 02 00 00       	push   $0x200
80103489:	52                   	push   %edx
8010348a:	50                   	push   %eax
8010348b:	e8 da 29 00 00       	call   80105e6a <memmove>
80103490:	83 c4 10             	add    $0x10,%esp
    bwrite(dbuf);  // write dst to disk
80103493:	83 ec 0c             	sub    $0xc,%esp
80103496:	ff 75 ec             	pushl  -0x14(%ebp)
80103499:	e8 51 cd ff ff       	call   801001ef <bwrite>
8010349e:	83 c4 10             	add    $0x10,%esp
    brelse(lbuf); 
801034a1:	83 ec 0c             	sub    $0xc,%esp
801034a4:	ff 75 f0             	pushl  -0x10(%ebp)
801034a7:	e8 82 cd ff ff       	call   8010022e <brelse>
801034ac:	83 c4 10             	add    $0x10,%esp
    brelse(dbuf);
801034af:	83 ec 0c             	sub    $0xc,%esp
801034b2:	ff 75 ec             	pushl  -0x14(%ebp)
801034b5:	e8 74 cd ff ff       	call   8010022e <brelse>
801034ba:	83 c4 10             	add    $0x10,%esp
static void 
install_trans(void)
{
  int tail;

  for (tail = 0; tail < log.lh.n; tail++) {
801034bd:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
801034c1:	a1 c8 32 11 80       	mov    0x801132c8,%eax
801034c6:	3b 45 f4             	cmp    -0xc(%ebp),%eax
801034c9:	0f 8f 5d ff ff ff    	jg     8010342c <install_trans+0x12>
    memmove(dbuf->data, lbuf->data, BSIZE);  // copy block to dst
    bwrite(dbuf);  // write dst to disk
    brelse(lbuf); 
    brelse(dbuf);
  }
}
801034cf:	90                   	nop
801034d0:	c9                   	leave  
801034d1:	c3                   	ret    

801034d2 <read_head>:

// Read the log header from disk into the in-memory log header
static void
read_head(void)
{
801034d2:	55                   	push   %ebp
801034d3:	89 e5                	mov    %esp,%ebp
801034d5:	83 ec 18             	sub    $0x18,%esp
  struct buf *buf = bread(log.dev, log.start);
801034d8:	a1 b4 32 11 80       	mov    0x801132b4,%eax
801034dd:	89 c2                	mov    %eax,%edx
801034df:	a1 c4 32 11 80       	mov    0x801132c4,%eax
801034e4:	83 ec 08             	sub    $0x8,%esp
801034e7:	52                   	push   %edx
801034e8:	50                   	push   %eax
801034e9:	e8 c8 cc ff ff       	call   801001b6 <bread>
801034ee:	83 c4 10             	add    $0x10,%esp
801034f1:	89 45 f0             	mov    %eax,-0x10(%ebp)
  struct logheader *lh = (struct logheader *) (buf->data);
801034f4:	8b 45 f0             	mov    -0x10(%ebp),%eax
801034f7:	83 c0 18             	add    $0x18,%eax
801034fa:	89 45 ec             	mov    %eax,-0x14(%ebp)
  int i;
  log.lh.n = lh->n;
801034fd:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103500:	8b 00                	mov    (%eax),%eax
80103502:	a3 c8 32 11 80       	mov    %eax,0x801132c8
  for (i = 0; i < log.lh.n; i++) {
80103507:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
8010350e:	eb 1b                	jmp    8010352b <read_head+0x59>
    log.lh.block[i] = lh->block[i];
80103510:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103513:	8b 55 f4             	mov    -0xc(%ebp),%edx
80103516:	8b 44 90 04          	mov    0x4(%eax,%edx,4),%eax
8010351a:	8b 55 f4             	mov    -0xc(%ebp),%edx
8010351d:	83 c2 10             	add    $0x10,%edx
80103520:	89 04 95 8c 32 11 80 	mov    %eax,-0x7feecd74(,%edx,4)
{
  struct buf *buf = bread(log.dev, log.start);
  struct logheader *lh = (struct logheader *) (buf->data);
  int i;
  log.lh.n = lh->n;
  for (i = 0; i < log.lh.n; i++) {
80103527:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
8010352b:	a1 c8 32 11 80       	mov    0x801132c8,%eax
80103530:	3b 45 f4             	cmp    -0xc(%ebp),%eax
80103533:	7f db                	jg     80103510 <read_head+0x3e>
    log.lh.block[i] = lh->block[i];
  }
  brelse(buf);
80103535:	83 ec 0c             	sub    $0xc,%esp
80103538:	ff 75 f0             	pushl  -0x10(%ebp)
8010353b:	e8 ee cc ff ff       	call   8010022e <brelse>
80103540:	83 c4 10             	add    $0x10,%esp
}
80103543:	90                   	nop
80103544:	c9                   	leave  
80103545:	c3                   	ret    

80103546 <write_head>:
// Write in-memory log header to disk.
// This is the true point at which the
// current transaction commits.
static void
write_head(void)
{
80103546:	55                   	push   %ebp
80103547:	89 e5                	mov    %esp,%ebp
80103549:	83 ec 18             	sub    $0x18,%esp
  struct buf *buf = bread(log.dev, log.start);
8010354c:	a1 b4 32 11 80       	mov    0x801132b4,%eax
80103551:	89 c2                	mov    %eax,%edx
80103553:	a1 c4 32 11 80       	mov    0x801132c4,%eax
80103558:	83 ec 08             	sub    $0x8,%esp
8010355b:	52                   	push   %edx
8010355c:	50                   	push   %eax
8010355d:	e8 54 cc ff ff       	call   801001b6 <bread>
80103562:	83 c4 10             	add    $0x10,%esp
80103565:	89 45 f0             	mov    %eax,-0x10(%ebp)
  struct logheader *hb = (struct logheader *) (buf->data);
80103568:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010356b:	83 c0 18             	add    $0x18,%eax
8010356e:	89 45 ec             	mov    %eax,-0x14(%ebp)
  int i;
  hb->n = log.lh.n;
80103571:	8b 15 c8 32 11 80    	mov    0x801132c8,%edx
80103577:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010357a:	89 10                	mov    %edx,(%eax)
  for (i = 0; i < log.lh.n; i++) {
8010357c:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80103583:	eb 1b                	jmp    801035a0 <write_head+0x5a>
    hb->block[i] = log.lh.block[i];
80103585:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103588:	83 c0 10             	add    $0x10,%eax
8010358b:	8b 0c 85 8c 32 11 80 	mov    -0x7feecd74(,%eax,4),%ecx
80103592:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103595:	8b 55 f4             	mov    -0xc(%ebp),%edx
80103598:	89 4c 90 04          	mov    %ecx,0x4(%eax,%edx,4)
{
  struct buf *buf = bread(log.dev, log.start);
  struct logheader *hb = (struct logheader *) (buf->data);
  int i;
  hb->n = log.lh.n;
  for (i = 0; i < log.lh.n; i++) {
8010359c:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
801035a0:	a1 c8 32 11 80       	mov    0x801132c8,%eax
801035a5:	3b 45 f4             	cmp    -0xc(%ebp),%eax
801035a8:	7f db                	jg     80103585 <write_head+0x3f>
    hb->block[i] = log.lh.block[i];
  }
  bwrite(buf);
801035aa:	83 ec 0c             	sub    $0xc,%esp
801035ad:	ff 75 f0             	pushl  -0x10(%ebp)
801035b0:	e8 3a cc ff ff       	call   801001ef <bwrite>
801035b5:	83 c4 10             	add    $0x10,%esp
  brelse(buf);
801035b8:	83 ec 0c             	sub    $0xc,%esp
801035bb:	ff 75 f0             	pushl  -0x10(%ebp)
801035be:	e8 6b cc ff ff       	call   8010022e <brelse>
801035c3:	83 c4 10             	add    $0x10,%esp
}
801035c6:	90                   	nop
801035c7:	c9                   	leave  
801035c8:	c3                   	ret    

801035c9 <recover_from_log>:

static void
recover_from_log(void)
{
801035c9:	55                   	push   %ebp
801035ca:	89 e5                	mov    %esp,%ebp
801035cc:	83 ec 08             	sub    $0x8,%esp
  read_head();      
801035cf:	e8 fe fe ff ff       	call   801034d2 <read_head>
  install_trans(); // if committed, copy from log to disk
801035d4:	e8 41 fe ff ff       	call   8010341a <install_trans>
  log.lh.n = 0;
801035d9:	c7 05 c8 32 11 80 00 	movl   $0x0,0x801132c8
801035e0:	00 00 00 
  write_head(); // clear the log
801035e3:	e8 5e ff ff ff       	call   80103546 <write_head>
}
801035e8:	90                   	nop
801035e9:	c9                   	leave  
801035ea:	c3                   	ret    

801035eb <begin_op>:

// called at the start of each FS system call.
void
begin_op(void)
{
801035eb:	55                   	push   %ebp
801035ec:	89 e5                	mov    %esp,%ebp
801035ee:	83 ec 08             	sub    $0x8,%esp
  acquire(&log.lock);
801035f1:	83 ec 0c             	sub    $0xc,%esp
801035f4:	68 80 32 11 80       	push   $0x80113280
801035f9:	e8 4a 25 00 00       	call   80105b48 <acquire>
801035fe:	83 c4 10             	add    $0x10,%esp
  while(1){
    if(log.committing){
80103601:	a1 c0 32 11 80       	mov    0x801132c0,%eax
80103606:	85 c0                	test   %eax,%eax
80103608:	74 17                	je     80103621 <begin_op+0x36>
      sleep(&log, &log.lock);
8010360a:	83 ec 08             	sub    $0x8,%esp
8010360d:	68 80 32 11 80       	push   $0x80113280
80103612:	68 80 32 11 80       	push   $0x80113280
80103617:	e8 48 1d 00 00       	call   80105364 <sleep>
8010361c:	83 c4 10             	add    $0x10,%esp
8010361f:	eb e0                	jmp    80103601 <begin_op+0x16>
    } else if(log.lh.n + (log.outstanding+1)*MAXOPBLOCKS > LOGSIZE){
80103621:	8b 0d c8 32 11 80    	mov    0x801132c8,%ecx
80103627:	a1 bc 32 11 80       	mov    0x801132bc,%eax
8010362c:	8d 50 01             	lea    0x1(%eax),%edx
8010362f:	89 d0                	mov    %edx,%eax
80103631:	c1 e0 02             	shl    $0x2,%eax
80103634:	01 d0                	add    %edx,%eax
80103636:	01 c0                	add    %eax,%eax
80103638:	01 c8                	add    %ecx,%eax
8010363a:	83 f8 1e             	cmp    $0x1e,%eax
8010363d:	7e 17                	jle    80103656 <begin_op+0x6b>
      // this op might exhaust log space; wait for commit.
      sleep(&log, &log.lock);
8010363f:	83 ec 08             	sub    $0x8,%esp
80103642:	68 80 32 11 80       	push   $0x80113280
80103647:	68 80 32 11 80       	push   $0x80113280
8010364c:	e8 13 1d 00 00       	call   80105364 <sleep>
80103651:	83 c4 10             	add    $0x10,%esp
80103654:	eb ab                	jmp    80103601 <begin_op+0x16>
    } else {
      log.outstanding += 1;
80103656:	a1 bc 32 11 80       	mov    0x801132bc,%eax
8010365b:	83 c0 01             	add    $0x1,%eax
8010365e:	a3 bc 32 11 80       	mov    %eax,0x801132bc
      release(&log.lock);
80103663:	83 ec 0c             	sub    $0xc,%esp
80103666:	68 80 32 11 80       	push   $0x80113280
8010366b:	e8 3f 25 00 00       	call   80105baf <release>
80103670:	83 c4 10             	add    $0x10,%esp
      break;
80103673:	90                   	nop
    }
  }
}
80103674:	90                   	nop
80103675:	c9                   	leave  
80103676:	c3                   	ret    

80103677 <end_op>:

// called at the end of each FS system call.
// commits if this was the last outstanding operation.
void
end_op(void)
{
80103677:	55                   	push   %ebp
80103678:	89 e5                	mov    %esp,%ebp
8010367a:	83 ec 18             	sub    $0x18,%esp
  int do_commit = 0;
8010367d:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

  acquire(&log.lock);
80103684:	83 ec 0c             	sub    $0xc,%esp
80103687:	68 80 32 11 80       	push   $0x80113280
8010368c:	e8 b7 24 00 00       	call   80105b48 <acquire>
80103691:	83 c4 10             	add    $0x10,%esp
  log.outstanding -= 1;
80103694:	a1 bc 32 11 80       	mov    0x801132bc,%eax
80103699:	83 e8 01             	sub    $0x1,%eax
8010369c:	a3 bc 32 11 80       	mov    %eax,0x801132bc
  if(log.committing)
801036a1:	a1 c0 32 11 80       	mov    0x801132c0,%eax
801036a6:	85 c0                	test   %eax,%eax
801036a8:	74 0d                	je     801036b7 <end_op+0x40>
    panic("log.committing");
801036aa:	83 ec 0c             	sub    $0xc,%esp
801036ad:	68 84 94 10 80       	push   $0x80109484
801036b2:	e8 af ce ff ff       	call   80100566 <panic>
  if(log.outstanding == 0){
801036b7:	a1 bc 32 11 80       	mov    0x801132bc,%eax
801036bc:	85 c0                	test   %eax,%eax
801036be:	75 13                	jne    801036d3 <end_op+0x5c>
    do_commit = 1;
801036c0:	c7 45 f4 01 00 00 00 	movl   $0x1,-0xc(%ebp)
    log.committing = 1;
801036c7:	c7 05 c0 32 11 80 01 	movl   $0x1,0x801132c0
801036ce:	00 00 00 
801036d1:	eb 10                	jmp    801036e3 <end_op+0x6c>
  } else {
    // begin_op() may be waiting for log space.
    wakeup(&log);
801036d3:	83 ec 0c             	sub    $0xc,%esp
801036d6:	68 80 32 11 80       	push   $0x80113280
801036db:	e8 99 1d 00 00       	call   80105479 <wakeup>
801036e0:	83 c4 10             	add    $0x10,%esp
  }
  release(&log.lock);
801036e3:	83 ec 0c             	sub    $0xc,%esp
801036e6:	68 80 32 11 80       	push   $0x80113280
801036eb:	e8 bf 24 00 00       	call   80105baf <release>
801036f0:	83 c4 10             	add    $0x10,%esp

  if(do_commit){
801036f3:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801036f7:	74 3f                	je     80103738 <end_op+0xc1>
    // call commit w/o holding locks, since not allowed
    // to sleep with locks.
    commit();
801036f9:	e8 f5 00 00 00       	call   801037f3 <commit>
    acquire(&log.lock);
801036fe:	83 ec 0c             	sub    $0xc,%esp
80103701:	68 80 32 11 80       	push   $0x80113280
80103706:	e8 3d 24 00 00       	call   80105b48 <acquire>
8010370b:	83 c4 10             	add    $0x10,%esp
    log.committing = 0;
8010370e:	c7 05 c0 32 11 80 00 	movl   $0x0,0x801132c0
80103715:	00 00 00 
    wakeup(&log);
80103718:	83 ec 0c             	sub    $0xc,%esp
8010371b:	68 80 32 11 80       	push   $0x80113280
80103720:	e8 54 1d 00 00       	call   80105479 <wakeup>
80103725:	83 c4 10             	add    $0x10,%esp
    release(&log.lock);
80103728:	83 ec 0c             	sub    $0xc,%esp
8010372b:	68 80 32 11 80       	push   $0x80113280
80103730:	e8 7a 24 00 00       	call   80105baf <release>
80103735:	83 c4 10             	add    $0x10,%esp
  }
}
80103738:	90                   	nop
80103739:	c9                   	leave  
8010373a:	c3                   	ret    

8010373b <write_log>:

// Copy modified blocks from cache to log.
static void 
write_log(void)
{
8010373b:	55                   	push   %ebp
8010373c:	89 e5                	mov    %esp,%ebp
8010373e:	83 ec 18             	sub    $0x18,%esp
  int tail;

  for (tail = 0; tail < log.lh.n; tail++) {
80103741:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80103748:	e9 95 00 00 00       	jmp    801037e2 <write_log+0xa7>
    struct buf *to = bread(log.dev, log.start+tail+1); // log block
8010374d:	8b 15 b4 32 11 80    	mov    0x801132b4,%edx
80103753:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103756:	01 d0                	add    %edx,%eax
80103758:	83 c0 01             	add    $0x1,%eax
8010375b:	89 c2                	mov    %eax,%edx
8010375d:	a1 c4 32 11 80       	mov    0x801132c4,%eax
80103762:	83 ec 08             	sub    $0x8,%esp
80103765:	52                   	push   %edx
80103766:	50                   	push   %eax
80103767:	e8 4a ca ff ff       	call   801001b6 <bread>
8010376c:	83 c4 10             	add    $0x10,%esp
8010376f:	89 45 f0             	mov    %eax,-0x10(%ebp)
    struct buf *from = bread(log.dev, log.lh.block[tail]); // cache block
80103772:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103775:	83 c0 10             	add    $0x10,%eax
80103778:	8b 04 85 8c 32 11 80 	mov    -0x7feecd74(,%eax,4),%eax
8010377f:	89 c2                	mov    %eax,%edx
80103781:	a1 c4 32 11 80       	mov    0x801132c4,%eax
80103786:	83 ec 08             	sub    $0x8,%esp
80103789:	52                   	push   %edx
8010378a:	50                   	push   %eax
8010378b:	e8 26 ca ff ff       	call   801001b6 <bread>
80103790:	83 c4 10             	add    $0x10,%esp
80103793:	89 45 ec             	mov    %eax,-0x14(%ebp)
    memmove(to->data, from->data, BSIZE);
80103796:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103799:	8d 50 18             	lea    0x18(%eax),%edx
8010379c:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010379f:	83 c0 18             	add    $0x18,%eax
801037a2:	83 ec 04             	sub    $0x4,%esp
801037a5:	68 00 02 00 00       	push   $0x200
801037aa:	52                   	push   %edx
801037ab:	50                   	push   %eax
801037ac:	e8 b9 26 00 00       	call   80105e6a <memmove>
801037b1:	83 c4 10             	add    $0x10,%esp
    bwrite(to);  // write the log
801037b4:	83 ec 0c             	sub    $0xc,%esp
801037b7:	ff 75 f0             	pushl  -0x10(%ebp)
801037ba:	e8 30 ca ff ff       	call   801001ef <bwrite>
801037bf:	83 c4 10             	add    $0x10,%esp
    brelse(from); 
801037c2:	83 ec 0c             	sub    $0xc,%esp
801037c5:	ff 75 ec             	pushl  -0x14(%ebp)
801037c8:	e8 61 ca ff ff       	call   8010022e <brelse>
801037cd:	83 c4 10             	add    $0x10,%esp
    brelse(to);
801037d0:	83 ec 0c             	sub    $0xc,%esp
801037d3:	ff 75 f0             	pushl  -0x10(%ebp)
801037d6:	e8 53 ca ff ff       	call   8010022e <brelse>
801037db:	83 c4 10             	add    $0x10,%esp
static void 
write_log(void)
{
  int tail;

  for (tail = 0; tail < log.lh.n; tail++) {
801037de:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
801037e2:	a1 c8 32 11 80       	mov    0x801132c8,%eax
801037e7:	3b 45 f4             	cmp    -0xc(%ebp),%eax
801037ea:	0f 8f 5d ff ff ff    	jg     8010374d <write_log+0x12>
    memmove(to->data, from->data, BSIZE);
    bwrite(to);  // write the log
    brelse(from); 
    brelse(to);
  }
}
801037f0:	90                   	nop
801037f1:	c9                   	leave  
801037f2:	c3                   	ret    

801037f3 <commit>:

static void
commit()
{
801037f3:	55                   	push   %ebp
801037f4:	89 e5                	mov    %esp,%ebp
801037f6:	83 ec 08             	sub    $0x8,%esp
  if (log.lh.n > 0) {
801037f9:	a1 c8 32 11 80       	mov    0x801132c8,%eax
801037fe:	85 c0                	test   %eax,%eax
80103800:	7e 1e                	jle    80103820 <commit+0x2d>
    write_log();     // Write modified blocks from cache to log
80103802:	e8 34 ff ff ff       	call   8010373b <write_log>
    write_head();    // Write header to disk -- the real commit
80103807:	e8 3a fd ff ff       	call   80103546 <write_head>
    install_trans(); // Now install writes to home locations
8010380c:	e8 09 fc ff ff       	call   8010341a <install_trans>
    log.lh.n = 0; 
80103811:	c7 05 c8 32 11 80 00 	movl   $0x0,0x801132c8
80103818:	00 00 00 
    write_head();    // Erase the transaction from the log
8010381b:	e8 26 fd ff ff       	call   80103546 <write_head>
  }
}
80103820:	90                   	nop
80103821:	c9                   	leave  
80103822:	c3                   	ret    

80103823 <log_write>:
//   modify bp->data[]
//   log_write(bp)
//   brelse(bp)
void
log_write(struct buf *b)
{
80103823:	55                   	push   %ebp
80103824:	89 e5                	mov    %esp,%ebp
80103826:	83 ec 18             	sub    $0x18,%esp
  int i;

  if (log.lh.n >= LOGSIZE || log.lh.n >= log.size - 1)
80103829:	a1 c8 32 11 80       	mov    0x801132c8,%eax
8010382e:	83 f8 1d             	cmp    $0x1d,%eax
80103831:	7f 12                	jg     80103845 <log_write+0x22>
80103833:	a1 c8 32 11 80       	mov    0x801132c8,%eax
80103838:	8b 15 b8 32 11 80    	mov    0x801132b8,%edx
8010383e:	83 ea 01             	sub    $0x1,%edx
80103841:	39 d0                	cmp    %edx,%eax
80103843:	7c 0d                	jl     80103852 <log_write+0x2f>
    panic("too big a transaction");
80103845:	83 ec 0c             	sub    $0xc,%esp
80103848:	68 93 94 10 80       	push   $0x80109493
8010384d:	e8 14 cd ff ff       	call   80100566 <panic>
  if (log.outstanding < 1)
80103852:	a1 bc 32 11 80       	mov    0x801132bc,%eax
80103857:	85 c0                	test   %eax,%eax
80103859:	7f 0d                	jg     80103868 <log_write+0x45>
    panic("log_write outside of trans");
8010385b:	83 ec 0c             	sub    $0xc,%esp
8010385e:	68 a9 94 10 80       	push   $0x801094a9
80103863:	e8 fe cc ff ff       	call   80100566 <panic>

  acquire(&log.lock);
80103868:	83 ec 0c             	sub    $0xc,%esp
8010386b:	68 80 32 11 80       	push   $0x80113280
80103870:	e8 d3 22 00 00       	call   80105b48 <acquire>
80103875:	83 c4 10             	add    $0x10,%esp
  for (i = 0; i < log.lh.n; i++) {
80103878:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
8010387f:	eb 1d                	jmp    8010389e <log_write+0x7b>
    if (log.lh.block[i] == b->blockno)   // log absorbtion
80103881:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103884:	83 c0 10             	add    $0x10,%eax
80103887:	8b 04 85 8c 32 11 80 	mov    -0x7feecd74(,%eax,4),%eax
8010388e:	89 c2                	mov    %eax,%edx
80103890:	8b 45 08             	mov    0x8(%ebp),%eax
80103893:	8b 40 08             	mov    0x8(%eax),%eax
80103896:	39 c2                	cmp    %eax,%edx
80103898:	74 10                	je     801038aa <log_write+0x87>
    panic("too big a transaction");
  if (log.outstanding < 1)
    panic("log_write outside of trans");

  acquire(&log.lock);
  for (i = 0; i < log.lh.n; i++) {
8010389a:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
8010389e:	a1 c8 32 11 80       	mov    0x801132c8,%eax
801038a3:	3b 45 f4             	cmp    -0xc(%ebp),%eax
801038a6:	7f d9                	jg     80103881 <log_write+0x5e>
801038a8:	eb 01                	jmp    801038ab <log_write+0x88>
    if (log.lh.block[i] == b->blockno)   // log absorbtion
      break;
801038aa:	90                   	nop
  }
  log.lh.block[i] = b->blockno;
801038ab:	8b 45 08             	mov    0x8(%ebp),%eax
801038ae:	8b 40 08             	mov    0x8(%eax),%eax
801038b1:	89 c2                	mov    %eax,%edx
801038b3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801038b6:	83 c0 10             	add    $0x10,%eax
801038b9:	89 14 85 8c 32 11 80 	mov    %edx,-0x7feecd74(,%eax,4)
  if (i == log.lh.n)
801038c0:	a1 c8 32 11 80       	mov    0x801132c8,%eax
801038c5:	3b 45 f4             	cmp    -0xc(%ebp),%eax
801038c8:	75 0d                	jne    801038d7 <log_write+0xb4>
    log.lh.n++;
801038ca:	a1 c8 32 11 80       	mov    0x801132c8,%eax
801038cf:	83 c0 01             	add    $0x1,%eax
801038d2:	a3 c8 32 11 80       	mov    %eax,0x801132c8
  b->flags |= B_DIRTY; // prevent eviction
801038d7:	8b 45 08             	mov    0x8(%ebp),%eax
801038da:	8b 00                	mov    (%eax),%eax
801038dc:	83 c8 04             	or     $0x4,%eax
801038df:	89 c2                	mov    %eax,%edx
801038e1:	8b 45 08             	mov    0x8(%ebp),%eax
801038e4:	89 10                	mov    %edx,(%eax)
  release(&log.lock);
801038e6:	83 ec 0c             	sub    $0xc,%esp
801038e9:	68 80 32 11 80       	push   $0x80113280
801038ee:	e8 bc 22 00 00       	call   80105baf <release>
801038f3:	83 c4 10             	add    $0x10,%esp
}
801038f6:	90                   	nop
801038f7:	c9                   	leave  
801038f8:	c3                   	ret    

801038f9 <v2p>:
801038f9:	55                   	push   %ebp
801038fa:	89 e5                	mov    %esp,%ebp
801038fc:	8b 45 08             	mov    0x8(%ebp),%eax
801038ff:	05 00 00 00 80       	add    $0x80000000,%eax
80103904:	5d                   	pop    %ebp
80103905:	c3                   	ret    

80103906 <p2v>:
static inline void *p2v(uint a) { return (void *) ((a) + KERNBASE); }
80103906:	55                   	push   %ebp
80103907:	89 e5                	mov    %esp,%ebp
80103909:	8b 45 08             	mov    0x8(%ebp),%eax
8010390c:	05 00 00 00 80       	add    $0x80000000,%eax
80103911:	5d                   	pop    %ebp
80103912:	c3                   	ret    

80103913 <xchg>:
  asm volatile("sti");
}

static inline uint
xchg(volatile uint *addr, uint newval)
{
80103913:	55                   	push   %ebp
80103914:	89 e5                	mov    %esp,%ebp
80103916:	83 ec 10             	sub    $0x10,%esp
  uint result;
  
  // The + in "+m" denotes a read-modify-write operand.
  asm volatile("lock; xchgl %0, %1" :
80103919:	8b 55 08             	mov    0x8(%ebp),%edx
8010391c:	8b 45 0c             	mov    0xc(%ebp),%eax
8010391f:	8b 4d 08             	mov    0x8(%ebp),%ecx
80103922:	f0 87 02             	lock xchg %eax,(%edx)
80103925:	89 45 fc             	mov    %eax,-0x4(%ebp)
               "+m" (*addr), "=a" (result) :
               "1" (newval) :
               "cc");
  return result;
80103928:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
8010392b:	c9                   	leave  
8010392c:	c3                   	ret    

8010392d <main>:
// Bootstrap processor starts running C code here.
// Allocate a real stack and switch to it, first
// doing some setup required for memory allocator to work.
int
main(void)
{
8010392d:	8d 4c 24 04          	lea    0x4(%esp),%ecx
80103931:	83 e4 f0             	and    $0xfffffff0,%esp
80103934:	ff 71 fc             	pushl  -0x4(%ecx)
80103937:	55                   	push   %ebp
80103938:	89 e5                	mov    %esp,%ebp
8010393a:	51                   	push   %ecx
8010393b:	83 ec 04             	sub    $0x4,%esp
  kinit1(end, P2V(4*1024*1024)); // phys page allocator
8010393e:	83 ec 08             	sub    $0x8,%esp
80103941:	68 00 00 40 80       	push   $0x80400000
80103946:	68 3c 68 11 80       	push   $0x8011683c
8010394b:	e8 7d f2 ff ff       	call   80102bcd <kinit1>
80103950:	83 c4 10             	add    $0x10,%esp
  kvmalloc();      // kernel page table
80103953:	e8 3c 51 00 00       	call   80108a94 <kvmalloc>
  mpinit();        // collect info about this machine
80103958:	e8 43 04 00 00       	call   80103da0 <mpinit>
  lapicinit();
8010395d:	e8 ea f5 ff ff       	call   80102f4c <lapicinit>
  seginit();       // set up segments
80103962:	e8 d6 4a 00 00       	call   8010843d <seginit>
  cprintf("\ncpu%d: starting xv6\n\n", cpu->id);
80103967:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
8010396d:	0f b6 00             	movzbl (%eax),%eax
80103970:	0f b6 c0             	movzbl %al,%eax
80103973:	83 ec 08             	sub    $0x8,%esp
80103976:	50                   	push   %eax
80103977:	68 c4 94 10 80       	push   $0x801094c4
8010397c:	e8 45 ca ff ff       	call   801003c6 <cprintf>
80103981:	83 c4 10             	add    $0x10,%esp
  picinit();       // interrupt controller
80103984:	e8 6d 06 00 00       	call   80103ff6 <picinit>
  ioapicinit();    // another interrupt controller
80103989:	e8 34 f1 ff ff       	call   80102ac2 <ioapicinit>
  consoleinit();   // I/O devices & their interrupts
8010398e:	e8 24 d2 ff ff       	call   80100bb7 <consoleinit>
  uartinit();      // serial port
80103993:	e8 01 3e 00 00       	call   80107799 <uartinit>
  pinit();         // process table
80103998:	e8 86 0e 00 00       	call   80104823 <pinit>
  tvinit();        // trap vectors
8010399d:	e8 f3 39 00 00       	call   80107395 <tvinit>
  binit();         // buffer cache
801039a2:	e8 8d c6 ff ff       	call   80100034 <binit>
  fileinit();      // file table
801039a7:	e8 67 d6 ff ff       	call   80101013 <fileinit>
  ideinit();       // disk
801039ac:	e8 19 ed ff ff       	call   801026ca <ideinit>
  if(!ismp)
801039b1:	a1 64 33 11 80       	mov    0x80113364,%eax
801039b6:	85 c0                	test   %eax,%eax
801039b8:	75 05                	jne    801039bf <main+0x92>
    timerinit();   // uniprocessor timer
801039ba:	e8 27 39 00 00       	call   801072e6 <timerinit>
  startothers();   // start other processors
801039bf:	e8 7f 00 00 00       	call   80103a43 <startothers>
  kinit2(P2V(4*1024*1024), P2V(PHYSTOP)); // must come after startothers()
801039c4:	83 ec 08             	sub    $0x8,%esp
801039c7:	68 00 00 00 8e       	push   $0x8e000000
801039cc:	68 00 00 40 80       	push   $0x80400000
801039d1:	e8 30 f2 ff ff       	call   80102c06 <kinit2>
801039d6:	83 c4 10             	add    $0x10,%esp
  userinit();      // first user process
801039d9:	e8 e4 0f 00 00       	call   801049c2 <userinit>
  // Finish setting up this processor in mpmain.
  mpmain();
801039de:	e8 1a 00 00 00       	call   801039fd <mpmain>

801039e3 <mpenter>:
}

// Other CPUs jump here from entryother.S.
static void
mpenter(void)
{
801039e3:	55                   	push   %ebp
801039e4:	89 e5                	mov    %esp,%ebp
801039e6:	83 ec 08             	sub    $0x8,%esp
  switchkvm(); 
801039e9:	e8 be 50 00 00       	call   80108aac <switchkvm>
  seginit();
801039ee:	e8 4a 4a 00 00       	call   8010843d <seginit>
  lapicinit();
801039f3:	e8 54 f5 ff ff       	call   80102f4c <lapicinit>
  mpmain();
801039f8:	e8 00 00 00 00       	call   801039fd <mpmain>

801039fd <mpmain>:
}

// Common CPU setup code.
static void
mpmain(void)
{
801039fd:	55                   	push   %ebp
801039fe:	89 e5                	mov    %esp,%ebp
80103a00:	83 ec 08             	sub    $0x8,%esp
  cprintf("cpu%d: starting\n", cpu->id);
80103a03:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80103a09:	0f b6 00             	movzbl (%eax),%eax
80103a0c:	0f b6 c0             	movzbl %al,%eax
80103a0f:	83 ec 08             	sub    $0x8,%esp
80103a12:	50                   	push   %eax
80103a13:	68 db 94 10 80       	push   $0x801094db
80103a18:	e8 a9 c9 ff ff       	call   801003c6 <cprintf>
80103a1d:	83 c4 10             	add    $0x10,%esp
  idtinit();       // load idt register
80103a20:	e8 d1 3a 00 00       	call   801074f6 <idtinit>
  xchg(&cpu->started, 1); // tell startothers() we're up
80103a25:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80103a2b:	05 a8 00 00 00       	add    $0xa8,%eax
80103a30:	83 ec 08             	sub    $0x8,%esp
80103a33:	6a 01                	push   $0x1
80103a35:	50                   	push   %eax
80103a36:	e8 d8 fe ff ff       	call   80103913 <xchg>
80103a3b:	83 c4 10             	add    $0x10,%esp
  scheduler();     // start running processes
80103a3e:	e8 c9 16 00 00       	call   8010510c <scheduler>

80103a43 <startothers>:
pde_t entrypgdir[];  // For entry.S

// Start the non-boot (AP) processors.
static void
startothers(void)
{
80103a43:	55                   	push   %ebp
80103a44:	89 e5                	mov    %esp,%ebp
80103a46:	53                   	push   %ebx
80103a47:	83 ec 14             	sub    $0x14,%esp
  char *stack;

  // Write entry code to unused memory at 0x7000.
  // The linker has placed the image of entryother.S in
  // _binary_entryother_start.
  code = p2v(0x7000);
80103a4a:	68 00 70 00 00       	push   $0x7000
80103a4f:	e8 b2 fe ff ff       	call   80103906 <p2v>
80103a54:	83 c4 04             	add    $0x4,%esp
80103a57:	89 45 f0             	mov    %eax,-0x10(%ebp)
  memmove(code, _binary_entryother_start, (uint)_binary_entryother_size);
80103a5a:	b8 8a 00 00 00       	mov    $0x8a,%eax
80103a5f:	83 ec 04             	sub    $0x4,%esp
80103a62:	50                   	push   %eax
80103a63:	68 2c c5 10 80       	push   $0x8010c52c
80103a68:	ff 75 f0             	pushl  -0x10(%ebp)
80103a6b:	e8 fa 23 00 00       	call   80105e6a <memmove>
80103a70:	83 c4 10             	add    $0x10,%esp

  for(c = cpus; c < cpus+ncpu; c++){
80103a73:	c7 45 f4 80 33 11 80 	movl   $0x80113380,-0xc(%ebp)
80103a7a:	e9 90 00 00 00       	jmp    80103b0f <startothers+0xcc>
    if(c == cpus+cpunum())  // We've started already.
80103a7f:	e8 e6 f5 ff ff       	call   8010306a <cpunum>
80103a84:	69 c0 bc 00 00 00    	imul   $0xbc,%eax,%eax
80103a8a:	05 80 33 11 80       	add    $0x80113380,%eax
80103a8f:	3b 45 f4             	cmp    -0xc(%ebp),%eax
80103a92:	74 73                	je     80103b07 <startothers+0xc4>
      continue;

    // Tell entryother.S what stack to use, where to enter, and what 
    // pgdir to use. We cannot use kpgdir yet, because the AP processor
    // is running in low  memory, so we use entrypgdir for the APs too.
    stack = kalloc();
80103a94:	e8 6b f2 ff ff       	call   80102d04 <kalloc>
80103a99:	89 45 ec             	mov    %eax,-0x14(%ebp)
    *(void**)(code-4) = stack + KSTACKSIZE;
80103a9c:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103a9f:	83 e8 04             	sub    $0x4,%eax
80103aa2:	8b 55 ec             	mov    -0x14(%ebp),%edx
80103aa5:	81 c2 00 10 00 00    	add    $0x1000,%edx
80103aab:	89 10                	mov    %edx,(%eax)
    *(void**)(code-8) = mpenter;
80103aad:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103ab0:	83 e8 08             	sub    $0x8,%eax
80103ab3:	c7 00 e3 39 10 80    	movl   $0x801039e3,(%eax)
    *(int**)(code-12) = (void *) v2p(entrypgdir);
80103ab9:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103abc:	8d 58 f4             	lea    -0xc(%eax),%ebx
80103abf:	83 ec 0c             	sub    $0xc,%esp
80103ac2:	68 00 b0 10 80       	push   $0x8010b000
80103ac7:	e8 2d fe ff ff       	call   801038f9 <v2p>
80103acc:	83 c4 10             	add    $0x10,%esp
80103acf:	89 03                	mov    %eax,(%ebx)

    lapicstartap(c->id, v2p(code));
80103ad1:	83 ec 0c             	sub    $0xc,%esp
80103ad4:	ff 75 f0             	pushl  -0x10(%ebp)
80103ad7:	e8 1d fe ff ff       	call   801038f9 <v2p>
80103adc:	83 c4 10             	add    $0x10,%esp
80103adf:	89 c2                	mov    %eax,%edx
80103ae1:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103ae4:	0f b6 00             	movzbl (%eax),%eax
80103ae7:	0f b6 c0             	movzbl %al,%eax
80103aea:	83 ec 08             	sub    $0x8,%esp
80103aed:	52                   	push   %edx
80103aee:	50                   	push   %eax
80103aef:	e8 f0 f5 ff ff       	call   801030e4 <lapicstartap>
80103af4:	83 c4 10             	add    $0x10,%esp

    // wait for cpu to finish mpmain()
    while(c->started == 0)
80103af7:	90                   	nop
80103af8:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103afb:	8b 80 a8 00 00 00    	mov    0xa8(%eax),%eax
80103b01:	85 c0                	test   %eax,%eax
80103b03:	74 f3                	je     80103af8 <startothers+0xb5>
80103b05:	eb 01                	jmp    80103b08 <startothers+0xc5>
  code = p2v(0x7000);
  memmove(code, _binary_entryother_start, (uint)_binary_entryother_size);

  for(c = cpus; c < cpus+ncpu; c++){
    if(c == cpus+cpunum())  // We've started already.
      continue;
80103b07:	90                   	nop
  // The linker has placed the image of entryother.S in
  // _binary_entryother_start.
  code = p2v(0x7000);
  memmove(code, _binary_entryother_start, (uint)_binary_entryother_size);

  for(c = cpus; c < cpus+ncpu; c++){
80103b08:	81 45 f4 bc 00 00 00 	addl   $0xbc,-0xc(%ebp)
80103b0f:	a1 60 39 11 80       	mov    0x80113960,%eax
80103b14:	69 c0 bc 00 00 00    	imul   $0xbc,%eax,%eax
80103b1a:	05 80 33 11 80       	add    $0x80113380,%eax
80103b1f:	3b 45 f4             	cmp    -0xc(%ebp),%eax
80103b22:	0f 87 57 ff ff ff    	ja     80103a7f <startothers+0x3c>

    // wait for cpu to finish mpmain()
    while(c->started == 0)
      ;
  }
}
80103b28:	90                   	nop
80103b29:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80103b2c:	c9                   	leave  
80103b2d:	c3                   	ret    

80103b2e <p2v>:
80103b2e:	55                   	push   %ebp
80103b2f:	89 e5                	mov    %esp,%ebp
80103b31:	8b 45 08             	mov    0x8(%ebp),%eax
80103b34:	05 00 00 00 80       	add    $0x80000000,%eax
80103b39:	5d                   	pop    %ebp
80103b3a:	c3                   	ret    

80103b3b <inb>:

// end of CS333 added routines

static inline uchar
inb(ushort port)
{
80103b3b:	55                   	push   %ebp
80103b3c:	89 e5                	mov    %esp,%ebp
80103b3e:	83 ec 14             	sub    $0x14,%esp
80103b41:	8b 45 08             	mov    0x8(%ebp),%eax
80103b44:	66 89 45 ec          	mov    %ax,-0x14(%ebp)
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80103b48:	0f b7 45 ec          	movzwl -0x14(%ebp),%eax
80103b4c:	89 c2                	mov    %eax,%edx
80103b4e:	ec                   	in     (%dx),%al
80103b4f:	88 45 ff             	mov    %al,-0x1(%ebp)
  return data;
80103b52:	0f b6 45 ff          	movzbl -0x1(%ebp),%eax
}
80103b56:	c9                   	leave  
80103b57:	c3                   	ret    

80103b58 <outb>:
               "memory", "cc");
}

static inline void
outb(ushort port, uchar data)
{
80103b58:	55                   	push   %ebp
80103b59:	89 e5                	mov    %esp,%ebp
80103b5b:	83 ec 08             	sub    $0x8,%esp
80103b5e:	8b 55 08             	mov    0x8(%ebp),%edx
80103b61:	8b 45 0c             	mov    0xc(%ebp),%eax
80103b64:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
80103b68:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80103b6b:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
80103b6f:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
80103b73:	ee                   	out    %al,(%dx)
}
80103b74:	90                   	nop
80103b75:	c9                   	leave  
80103b76:	c3                   	ret    

80103b77 <mpbcpu>:
int ncpu;
uchar ioapicid;

int
mpbcpu(void)
{
80103b77:	55                   	push   %ebp
80103b78:	89 e5                	mov    %esp,%ebp
  return bcpu-cpus;
80103b7a:	a1 64 c6 10 80       	mov    0x8010c664,%eax
80103b7f:	89 c2                	mov    %eax,%edx
80103b81:	b8 80 33 11 80       	mov    $0x80113380,%eax
80103b86:	29 c2                	sub    %eax,%edx
80103b88:	89 d0                	mov    %edx,%eax
80103b8a:	c1 f8 02             	sar    $0x2,%eax
80103b8d:	69 c0 cf 46 7d 67    	imul   $0x677d46cf,%eax,%eax
}
80103b93:	5d                   	pop    %ebp
80103b94:	c3                   	ret    

80103b95 <sum>:

static uchar
sum(uchar *addr, int len)
{
80103b95:	55                   	push   %ebp
80103b96:	89 e5                	mov    %esp,%ebp
80103b98:	83 ec 10             	sub    $0x10,%esp
  int i, sum;
  
  sum = 0;
80103b9b:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%ebp)
  for(i=0; i<len; i++)
80103ba2:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
80103ba9:	eb 15                	jmp    80103bc0 <sum+0x2b>
    sum += addr[i];
80103bab:	8b 55 fc             	mov    -0x4(%ebp),%edx
80103bae:	8b 45 08             	mov    0x8(%ebp),%eax
80103bb1:	01 d0                	add    %edx,%eax
80103bb3:	0f b6 00             	movzbl (%eax),%eax
80103bb6:	0f b6 c0             	movzbl %al,%eax
80103bb9:	01 45 f8             	add    %eax,-0x8(%ebp)
sum(uchar *addr, int len)
{
  int i, sum;
  
  sum = 0;
  for(i=0; i<len; i++)
80103bbc:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
80103bc0:	8b 45 fc             	mov    -0x4(%ebp),%eax
80103bc3:	3b 45 0c             	cmp    0xc(%ebp),%eax
80103bc6:	7c e3                	jl     80103bab <sum+0x16>
    sum += addr[i];
  return sum;
80103bc8:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
80103bcb:	c9                   	leave  
80103bcc:	c3                   	ret    

80103bcd <mpsearch1>:

// Look for an MP structure in the len bytes at addr.
static struct mp*
mpsearch1(uint a, int len)
{
80103bcd:	55                   	push   %ebp
80103bce:	89 e5                	mov    %esp,%ebp
80103bd0:	83 ec 18             	sub    $0x18,%esp
  uchar *e, *p, *addr;

  addr = p2v(a);
80103bd3:	ff 75 08             	pushl  0x8(%ebp)
80103bd6:	e8 53 ff ff ff       	call   80103b2e <p2v>
80103bdb:	83 c4 04             	add    $0x4,%esp
80103bde:	89 45 f0             	mov    %eax,-0x10(%ebp)
  e = addr+len;
80103be1:	8b 55 0c             	mov    0xc(%ebp),%edx
80103be4:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103be7:	01 d0                	add    %edx,%eax
80103be9:	89 45 ec             	mov    %eax,-0x14(%ebp)
  for(p = addr; p < e; p += sizeof(struct mp))
80103bec:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103bef:	89 45 f4             	mov    %eax,-0xc(%ebp)
80103bf2:	eb 36                	jmp    80103c2a <mpsearch1+0x5d>
    if(memcmp(p, "_MP_", 4) == 0 && sum(p, sizeof(struct mp)) == 0)
80103bf4:	83 ec 04             	sub    $0x4,%esp
80103bf7:	6a 04                	push   $0x4
80103bf9:	68 ec 94 10 80       	push   $0x801094ec
80103bfe:	ff 75 f4             	pushl  -0xc(%ebp)
80103c01:	e8 0c 22 00 00       	call   80105e12 <memcmp>
80103c06:	83 c4 10             	add    $0x10,%esp
80103c09:	85 c0                	test   %eax,%eax
80103c0b:	75 19                	jne    80103c26 <mpsearch1+0x59>
80103c0d:	83 ec 08             	sub    $0x8,%esp
80103c10:	6a 10                	push   $0x10
80103c12:	ff 75 f4             	pushl  -0xc(%ebp)
80103c15:	e8 7b ff ff ff       	call   80103b95 <sum>
80103c1a:	83 c4 10             	add    $0x10,%esp
80103c1d:	84 c0                	test   %al,%al
80103c1f:	75 05                	jne    80103c26 <mpsearch1+0x59>
      return (struct mp*)p;
80103c21:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103c24:	eb 11                	jmp    80103c37 <mpsearch1+0x6a>
{
  uchar *e, *p, *addr;

  addr = p2v(a);
  e = addr+len;
  for(p = addr; p < e; p += sizeof(struct mp))
80103c26:	83 45 f4 10          	addl   $0x10,-0xc(%ebp)
80103c2a:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103c2d:	3b 45 ec             	cmp    -0x14(%ebp),%eax
80103c30:	72 c2                	jb     80103bf4 <mpsearch1+0x27>
    if(memcmp(p, "_MP_", 4) == 0 && sum(p, sizeof(struct mp)) == 0)
      return (struct mp*)p;
  return 0;
80103c32:	b8 00 00 00 00       	mov    $0x0,%eax
}
80103c37:	c9                   	leave  
80103c38:	c3                   	ret    

80103c39 <mpsearch>:
// 1) in the first KB of the EBDA;
// 2) in the last KB of system base memory;
// 3) in the BIOS ROM between 0xE0000 and 0xFFFFF.
static struct mp*
mpsearch(void)
{
80103c39:	55                   	push   %ebp
80103c3a:	89 e5                	mov    %esp,%ebp
80103c3c:	83 ec 18             	sub    $0x18,%esp
  uchar *bda;
  uint p;
  struct mp *mp;

  bda = (uchar *) P2V(0x400);
80103c3f:	c7 45 f4 00 04 00 80 	movl   $0x80000400,-0xc(%ebp)
  if((p = ((bda[0x0F]<<8)| bda[0x0E]) << 4)){
80103c46:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103c49:	83 c0 0f             	add    $0xf,%eax
80103c4c:	0f b6 00             	movzbl (%eax),%eax
80103c4f:	0f b6 c0             	movzbl %al,%eax
80103c52:	c1 e0 08             	shl    $0x8,%eax
80103c55:	89 c2                	mov    %eax,%edx
80103c57:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103c5a:	83 c0 0e             	add    $0xe,%eax
80103c5d:	0f b6 00             	movzbl (%eax),%eax
80103c60:	0f b6 c0             	movzbl %al,%eax
80103c63:	09 d0                	or     %edx,%eax
80103c65:	c1 e0 04             	shl    $0x4,%eax
80103c68:	89 45 f0             	mov    %eax,-0x10(%ebp)
80103c6b:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80103c6f:	74 21                	je     80103c92 <mpsearch+0x59>
    if((mp = mpsearch1(p, 1024)))
80103c71:	83 ec 08             	sub    $0x8,%esp
80103c74:	68 00 04 00 00       	push   $0x400
80103c79:	ff 75 f0             	pushl  -0x10(%ebp)
80103c7c:	e8 4c ff ff ff       	call   80103bcd <mpsearch1>
80103c81:	83 c4 10             	add    $0x10,%esp
80103c84:	89 45 ec             	mov    %eax,-0x14(%ebp)
80103c87:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
80103c8b:	74 51                	je     80103cde <mpsearch+0xa5>
      return mp;
80103c8d:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103c90:	eb 61                	jmp    80103cf3 <mpsearch+0xba>
  } else {
    p = ((bda[0x14]<<8)|bda[0x13])*1024;
80103c92:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103c95:	83 c0 14             	add    $0x14,%eax
80103c98:	0f b6 00             	movzbl (%eax),%eax
80103c9b:	0f b6 c0             	movzbl %al,%eax
80103c9e:	c1 e0 08             	shl    $0x8,%eax
80103ca1:	89 c2                	mov    %eax,%edx
80103ca3:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103ca6:	83 c0 13             	add    $0x13,%eax
80103ca9:	0f b6 00             	movzbl (%eax),%eax
80103cac:	0f b6 c0             	movzbl %al,%eax
80103caf:	09 d0                	or     %edx,%eax
80103cb1:	c1 e0 0a             	shl    $0xa,%eax
80103cb4:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if((mp = mpsearch1(p-1024, 1024)))
80103cb7:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103cba:	2d 00 04 00 00       	sub    $0x400,%eax
80103cbf:	83 ec 08             	sub    $0x8,%esp
80103cc2:	68 00 04 00 00       	push   $0x400
80103cc7:	50                   	push   %eax
80103cc8:	e8 00 ff ff ff       	call   80103bcd <mpsearch1>
80103ccd:	83 c4 10             	add    $0x10,%esp
80103cd0:	89 45 ec             	mov    %eax,-0x14(%ebp)
80103cd3:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
80103cd7:	74 05                	je     80103cde <mpsearch+0xa5>
      return mp;
80103cd9:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103cdc:	eb 15                	jmp    80103cf3 <mpsearch+0xba>
  }
  return mpsearch1(0xF0000, 0x10000);
80103cde:	83 ec 08             	sub    $0x8,%esp
80103ce1:	68 00 00 01 00       	push   $0x10000
80103ce6:	68 00 00 0f 00       	push   $0xf0000
80103ceb:	e8 dd fe ff ff       	call   80103bcd <mpsearch1>
80103cf0:	83 c4 10             	add    $0x10,%esp
}
80103cf3:	c9                   	leave  
80103cf4:	c3                   	ret    

80103cf5 <mpconfig>:
// Check for correct signature, calculate the checksum and,
// if correct, check the version.
// To do: check extended table checksum.
static struct mpconf*
mpconfig(struct mp **pmp)
{
80103cf5:	55                   	push   %ebp
80103cf6:	89 e5                	mov    %esp,%ebp
80103cf8:	83 ec 18             	sub    $0x18,%esp
  struct mpconf *conf;
  struct mp *mp;

  if((mp = mpsearch()) == 0 || mp->physaddr == 0)
80103cfb:	e8 39 ff ff ff       	call   80103c39 <mpsearch>
80103d00:	89 45 f4             	mov    %eax,-0xc(%ebp)
80103d03:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80103d07:	74 0a                	je     80103d13 <mpconfig+0x1e>
80103d09:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103d0c:	8b 40 04             	mov    0x4(%eax),%eax
80103d0f:	85 c0                	test   %eax,%eax
80103d11:	75 0a                	jne    80103d1d <mpconfig+0x28>
    return 0;
80103d13:	b8 00 00 00 00       	mov    $0x0,%eax
80103d18:	e9 81 00 00 00       	jmp    80103d9e <mpconfig+0xa9>
  conf = (struct mpconf*) p2v((uint) mp->physaddr);
80103d1d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103d20:	8b 40 04             	mov    0x4(%eax),%eax
80103d23:	83 ec 0c             	sub    $0xc,%esp
80103d26:	50                   	push   %eax
80103d27:	e8 02 fe ff ff       	call   80103b2e <p2v>
80103d2c:	83 c4 10             	add    $0x10,%esp
80103d2f:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(memcmp(conf, "PCMP", 4) != 0)
80103d32:	83 ec 04             	sub    $0x4,%esp
80103d35:	6a 04                	push   $0x4
80103d37:	68 f1 94 10 80       	push   $0x801094f1
80103d3c:	ff 75 f0             	pushl  -0x10(%ebp)
80103d3f:	e8 ce 20 00 00       	call   80105e12 <memcmp>
80103d44:	83 c4 10             	add    $0x10,%esp
80103d47:	85 c0                	test   %eax,%eax
80103d49:	74 07                	je     80103d52 <mpconfig+0x5d>
    return 0;
80103d4b:	b8 00 00 00 00       	mov    $0x0,%eax
80103d50:	eb 4c                	jmp    80103d9e <mpconfig+0xa9>
  if(conf->version != 1 && conf->version != 4)
80103d52:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103d55:	0f b6 40 06          	movzbl 0x6(%eax),%eax
80103d59:	3c 01                	cmp    $0x1,%al
80103d5b:	74 12                	je     80103d6f <mpconfig+0x7a>
80103d5d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103d60:	0f b6 40 06          	movzbl 0x6(%eax),%eax
80103d64:	3c 04                	cmp    $0x4,%al
80103d66:	74 07                	je     80103d6f <mpconfig+0x7a>
    return 0;
80103d68:	b8 00 00 00 00       	mov    $0x0,%eax
80103d6d:	eb 2f                	jmp    80103d9e <mpconfig+0xa9>
  if(sum((uchar*)conf, conf->length) != 0)
80103d6f:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103d72:	0f b7 40 04          	movzwl 0x4(%eax),%eax
80103d76:	0f b7 c0             	movzwl %ax,%eax
80103d79:	83 ec 08             	sub    $0x8,%esp
80103d7c:	50                   	push   %eax
80103d7d:	ff 75 f0             	pushl  -0x10(%ebp)
80103d80:	e8 10 fe ff ff       	call   80103b95 <sum>
80103d85:	83 c4 10             	add    $0x10,%esp
80103d88:	84 c0                	test   %al,%al
80103d8a:	74 07                	je     80103d93 <mpconfig+0x9e>
    return 0;
80103d8c:	b8 00 00 00 00       	mov    $0x0,%eax
80103d91:	eb 0b                	jmp    80103d9e <mpconfig+0xa9>
  *pmp = mp;
80103d93:	8b 45 08             	mov    0x8(%ebp),%eax
80103d96:	8b 55 f4             	mov    -0xc(%ebp),%edx
80103d99:	89 10                	mov    %edx,(%eax)
  return conf;
80103d9b:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
80103d9e:	c9                   	leave  
80103d9f:	c3                   	ret    

80103da0 <mpinit>:

void
mpinit(void)
{
80103da0:	55                   	push   %ebp
80103da1:	89 e5                	mov    %esp,%ebp
80103da3:	83 ec 28             	sub    $0x28,%esp
  struct mp *mp;
  struct mpconf *conf;
  struct mpproc *proc;
  struct mpioapic *ioapic;

  bcpu = &cpus[0];
80103da6:	c7 05 64 c6 10 80 80 	movl   $0x80113380,0x8010c664
80103dad:	33 11 80 
  if((conf = mpconfig(&mp)) == 0)
80103db0:	83 ec 0c             	sub    $0xc,%esp
80103db3:	8d 45 e0             	lea    -0x20(%ebp),%eax
80103db6:	50                   	push   %eax
80103db7:	e8 39 ff ff ff       	call   80103cf5 <mpconfig>
80103dbc:	83 c4 10             	add    $0x10,%esp
80103dbf:	89 45 f0             	mov    %eax,-0x10(%ebp)
80103dc2:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80103dc6:	0f 84 96 01 00 00    	je     80103f62 <mpinit+0x1c2>
    return;
  ismp = 1;
80103dcc:	c7 05 64 33 11 80 01 	movl   $0x1,0x80113364
80103dd3:	00 00 00 
  lapic = (uint*)conf->lapicaddr;
80103dd6:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103dd9:	8b 40 24             	mov    0x24(%eax),%eax
80103ddc:	a3 7c 32 11 80       	mov    %eax,0x8011327c
  for(p=(uchar*)(conf+1), e=(uchar*)conf+conf->length; p<e; ){
80103de1:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103de4:	83 c0 2c             	add    $0x2c,%eax
80103de7:	89 45 f4             	mov    %eax,-0xc(%ebp)
80103dea:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103ded:	0f b7 40 04          	movzwl 0x4(%eax),%eax
80103df1:	0f b7 d0             	movzwl %ax,%edx
80103df4:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103df7:	01 d0                	add    %edx,%eax
80103df9:	89 45 ec             	mov    %eax,-0x14(%ebp)
80103dfc:	e9 f2 00 00 00       	jmp    80103ef3 <mpinit+0x153>
    switch(*p){
80103e01:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103e04:	0f b6 00             	movzbl (%eax),%eax
80103e07:	0f b6 c0             	movzbl %al,%eax
80103e0a:	83 f8 04             	cmp    $0x4,%eax
80103e0d:	0f 87 bc 00 00 00    	ja     80103ecf <mpinit+0x12f>
80103e13:	8b 04 85 34 95 10 80 	mov    -0x7fef6acc(,%eax,4),%eax
80103e1a:	ff e0                	jmp    *%eax
    case MPPROC:
      proc = (struct mpproc*)p;
80103e1c:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103e1f:	89 45 e8             	mov    %eax,-0x18(%ebp)
      if(ncpu != proc->apicid){
80103e22:	8b 45 e8             	mov    -0x18(%ebp),%eax
80103e25:	0f b6 40 01          	movzbl 0x1(%eax),%eax
80103e29:	0f b6 d0             	movzbl %al,%edx
80103e2c:	a1 60 39 11 80       	mov    0x80113960,%eax
80103e31:	39 c2                	cmp    %eax,%edx
80103e33:	74 2b                	je     80103e60 <mpinit+0xc0>
        cprintf("mpinit: ncpu=%d apicid=%d\n", ncpu, proc->apicid);
80103e35:	8b 45 e8             	mov    -0x18(%ebp),%eax
80103e38:	0f b6 40 01          	movzbl 0x1(%eax),%eax
80103e3c:	0f b6 d0             	movzbl %al,%edx
80103e3f:	a1 60 39 11 80       	mov    0x80113960,%eax
80103e44:	83 ec 04             	sub    $0x4,%esp
80103e47:	52                   	push   %edx
80103e48:	50                   	push   %eax
80103e49:	68 f6 94 10 80       	push   $0x801094f6
80103e4e:	e8 73 c5 ff ff       	call   801003c6 <cprintf>
80103e53:	83 c4 10             	add    $0x10,%esp
        ismp = 0;
80103e56:	c7 05 64 33 11 80 00 	movl   $0x0,0x80113364
80103e5d:	00 00 00 
      }
      if(proc->flags & MPBOOT)
80103e60:	8b 45 e8             	mov    -0x18(%ebp),%eax
80103e63:	0f b6 40 03          	movzbl 0x3(%eax),%eax
80103e67:	0f b6 c0             	movzbl %al,%eax
80103e6a:	83 e0 02             	and    $0x2,%eax
80103e6d:	85 c0                	test   %eax,%eax
80103e6f:	74 15                	je     80103e86 <mpinit+0xe6>
        bcpu = &cpus[ncpu];
80103e71:	a1 60 39 11 80       	mov    0x80113960,%eax
80103e76:	69 c0 bc 00 00 00    	imul   $0xbc,%eax,%eax
80103e7c:	05 80 33 11 80       	add    $0x80113380,%eax
80103e81:	a3 64 c6 10 80       	mov    %eax,0x8010c664
      cpus[ncpu].id = ncpu;
80103e86:	a1 60 39 11 80       	mov    0x80113960,%eax
80103e8b:	8b 15 60 39 11 80    	mov    0x80113960,%edx
80103e91:	69 c0 bc 00 00 00    	imul   $0xbc,%eax,%eax
80103e97:	05 80 33 11 80       	add    $0x80113380,%eax
80103e9c:	88 10                	mov    %dl,(%eax)
      ncpu++;
80103e9e:	a1 60 39 11 80       	mov    0x80113960,%eax
80103ea3:	83 c0 01             	add    $0x1,%eax
80103ea6:	a3 60 39 11 80       	mov    %eax,0x80113960
      p += sizeof(struct mpproc);
80103eab:	83 45 f4 14          	addl   $0x14,-0xc(%ebp)
      continue;
80103eaf:	eb 42                	jmp    80103ef3 <mpinit+0x153>
    case MPIOAPIC:
      ioapic = (struct mpioapic*)p;
80103eb1:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103eb4:	89 45 e4             	mov    %eax,-0x1c(%ebp)
      ioapicid = ioapic->apicno;
80103eb7:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80103eba:	0f b6 40 01          	movzbl 0x1(%eax),%eax
80103ebe:	a2 60 33 11 80       	mov    %al,0x80113360
      p += sizeof(struct mpioapic);
80103ec3:	83 45 f4 08          	addl   $0x8,-0xc(%ebp)
      continue;
80103ec7:	eb 2a                	jmp    80103ef3 <mpinit+0x153>
    case MPBUS:
    case MPIOINTR:
    case MPLINTR:
      p += 8;
80103ec9:	83 45 f4 08          	addl   $0x8,-0xc(%ebp)
      continue;
80103ecd:	eb 24                	jmp    80103ef3 <mpinit+0x153>
    default:
      cprintf("mpinit: unknown config type %x\n", *p);
80103ecf:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103ed2:	0f b6 00             	movzbl (%eax),%eax
80103ed5:	0f b6 c0             	movzbl %al,%eax
80103ed8:	83 ec 08             	sub    $0x8,%esp
80103edb:	50                   	push   %eax
80103edc:	68 14 95 10 80       	push   $0x80109514
80103ee1:	e8 e0 c4 ff ff       	call   801003c6 <cprintf>
80103ee6:	83 c4 10             	add    $0x10,%esp
      ismp = 0;
80103ee9:	c7 05 64 33 11 80 00 	movl   $0x0,0x80113364
80103ef0:	00 00 00 
  bcpu = &cpus[0];
  if((conf = mpconfig(&mp)) == 0)
    return;
  ismp = 1;
  lapic = (uint*)conf->lapicaddr;
  for(p=(uchar*)(conf+1), e=(uchar*)conf+conf->length; p<e; ){
80103ef3:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103ef6:	3b 45 ec             	cmp    -0x14(%ebp),%eax
80103ef9:	0f 82 02 ff ff ff    	jb     80103e01 <mpinit+0x61>
    default:
      cprintf("mpinit: unknown config type %x\n", *p);
      ismp = 0;
    }
  }
  if(!ismp){
80103eff:	a1 64 33 11 80       	mov    0x80113364,%eax
80103f04:	85 c0                	test   %eax,%eax
80103f06:	75 1d                	jne    80103f25 <mpinit+0x185>
    // Didn't like what we found; fall back to no MP.
    ncpu = 1;
80103f08:	c7 05 60 39 11 80 01 	movl   $0x1,0x80113960
80103f0f:	00 00 00 
    lapic = 0;
80103f12:	c7 05 7c 32 11 80 00 	movl   $0x0,0x8011327c
80103f19:	00 00 00 
    ioapicid = 0;
80103f1c:	c6 05 60 33 11 80 00 	movb   $0x0,0x80113360
    return;
80103f23:	eb 3e                	jmp    80103f63 <mpinit+0x1c3>
  }

  if(mp->imcrp){
80103f25:	8b 45 e0             	mov    -0x20(%ebp),%eax
80103f28:	0f b6 40 0c          	movzbl 0xc(%eax),%eax
80103f2c:	84 c0                	test   %al,%al
80103f2e:	74 33                	je     80103f63 <mpinit+0x1c3>
    // Bochs doesn't support IMCR, so this doesn't run on Bochs.
    // But it would on real hardware.
    outb(0x22, 0x70);   // Select IMCR
80103f30:	83 ec 08             	sub    $0x8,%esp
80103f33:	6a 70                	push   $0x70
80103f35:	6a 22                	push   $0x22
80103f37:	e8 1c fc ff ff       	call   80103b58 <outb>
80103f3c:	83 c4 10             	add    $0x10,%esp
    outb(0x23, inb(0x23) | 1);  // Mask external interrupts.
80103f3f:	83 ec 0c             	sub    $0xc,%esp
80103f42:	6a 23                	push   $0x23
80103f44:	e8 f2 fb ff ff       	call   80103b3b <inb>
80103f49:	83 c4 10             	add    $0x10,%esp
80103f4c:	83 c8 01             	or     $0x1,%eax
80103f4f:	0f b6 c0             	movzbl %al,%eax
80103f52:	83 ec 08             	sub    $0x8,%esp
80103f55:	50                   	push   %eax
80103f56:	6a 23                	push   $0x23
80103f58:	e8 fb fb ff ff       	call   80103b58 <outb>
80103f5d:	83 c4 10             	add    $0x10,%esp
80103f60:	eb 01                	jmp    80103f63 <mpinit+0x1c3>
  struct mpproc *proc;
  struct mpioapic *ioapic;

  bcpu = &cpus[0];
  if((conf = mpconfig(&mp)) == 0)
    return;
80103f62:	90                   	nop
    // Bochs doesn't support IMCR, so this doesn't run on Bochs.
    // But it would on real hardware.
    outb(0x22, 0x70);   // Select IMCR
    outb(0x23, inb(0x23) | 1);  // Mask external interrupts.
  }
}
80103f63:	c9                   	leave  
80103f64:	c3                   	ret    

80103f65 <outb>:
               "memory", "cc");
}

static inline void
outb(ushort port, uchar data)
{
80103f65:	55                   	push   %ebp
80103f66:	89 e5                	mov    %esp,%ebp
80103f68:	83 ec 08             	sub    $0x8,%esp
80103f6b:	8b 55 08             	mov    0x8(%ebp),%edx
80103f6e:	8b 45 0c             	mov    0xc(%ebp),%eax
80103f71:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
80103f75:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80103f78:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
80103f7c:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
80103f80:	ee                   	out    %al,(%dx)
}
80103f81:	90                   	nop
80103f82:	c9                   	leave  
80103f83:	c3                   	ret    

80103f84 <picsetmask>:
// Initial IRQ mask has interrupt 2 enabled (for slave 8259A).
static ushort irqmask = 0xFFFF & ~(1<<IRQ_SLAVE);

static void
picsetmask(ushort mask)
{
80103f84:	55                   	push   %ebp
80103f85:	89 e5                	mov    %esp,%ebp
80103f87:	83 ec 04             	sub    $0x4,%esp
80103f8a:	8b 45 08             	mov    0x8(%ebp),%eax
80103f8d:	66 89 45 fc          	mov    %ax,-0x4(%ebp)
  irqmask = mask;
80103f91:	0f b7 45 fc          	movzwl -0x4(%ebp),%eax
80103f95:	66 a3 00 c0 10 80    	mov    %ax,0x8010c000
  outb(IO_PIC1+1, mask);
80103f9b:	0f b7 45 fc          	movzwl -0x4(%ebp),%eax
80103f9f:	0f b6 c0             	movzbl %al,%eax
80103fa2:	50                   	push   %eax
80103fa3:	6a 21                	push   $0x21
80103fa5:	e8 bb ff ff ff       	call   80103f65 <outb>
80103faa:	83 c4 08             	add    $0x8,%esp
  outb(IO_PIC2+1, mask >> 8);
80103fad:	0f b7 45 fc          	movzwl -0x4(%ebp),%eax
80103fb1:	66 c1 e8 08          	shr    $0x8,%ax
80103fb5:	0f b6 c0             	movzbl %al,%eax
80103fb8:	50                   	push   %eax
80103fb9:	68 a1 00 00 00       	push   $0xa1
80103fbe:	e8 a2 ff ff ff       	call   80103f65 <outb>
80103fc3:	83 c4 08             	add    $0x8,%esp
}
80103fc6:	90                   	nop
80103fc7:	c9                   	leave  
80103fc8:	c3                   	ret    

80103fc9 <picenable>:

void
picenable(int irq)
{
80103fc9:	55                   	push   %ebp
80103fca:	89 e5                	mov    %esp,%ebp
  picsetmask(irqmask & ~(1<<irq));
80103fcc:	8b 45 08             	mov    0x8(%ebp),%eax
80103fcf:	ba 01 00 00 00       	mov    $0x1,%edx
80103fd4:	89 c1                	mov    %eax,%ecx
80103fd6:	d3 e2                	shl    %cl,%edx
80103fd8:	89 d0                	mov    %edx,%eax
80103fda:	f7 d0                	not    %eax
80103fdc:	89 c2                	mov    %eax,%edx
80103fde:	0f b7 05 00 c0 10 80 	movzwl 0x8010c000,%eax
80103fe5:	21 d0                	and    %edx,%eax
80103fe7:	0f b7 c0             	movzwl %ax,%eax
80103fea:	50                   	push   %eax
80103feb:	e8 94 ff ff ff       	call   80103f84 <picsetmask>
80103ff0:	83 c4 04             	add    $0x4,%esp
}
80103ff3:	90                   	nop
80103ff4:	c9                   	leave  
80103ff5:	c3                   	ret    

80103ff6 <picinit>:

// Initialize the 8259A interrupt controllers.
void
picinit(void)
{
80103ff6:	55                   	push   %ebp
80103ff7:	89 e5                	mov    %esp,%ebp
  // mask all interrupts
  outb(IO_PIC1+1, 0xFF);
80103ff9:	68 ff 00 00 00       	push   $0xff
80103ffe:	6a 21                	push   $0x21
80104000:	e8 60 ff ff ff       	call   80103f65 <outb>
80104005:	83 c4 08             	add    $0x8,%esp
  outb(IO_PIC2+1, 0xFF);
80104008:	68 ff 00 00 00       	push   $0xff
8010400d:	68 a1 00 00 00       	push   $0xa1
80104012:	e8 4e ff ff ff       	call   80103f65 <outb>
80104017:	83 c4 08             	add    $0x8,%esp

  // ICW1:  0001g0hi
  //    g:  0 = edge triggering, 1 = level triggering
  //    h:  0 = cascaded PICs, 1 = master only
  //    i:  0 = no ICW4, 1 = ICW4 required
  outb(IO_PIC1, 0x11);
8010401a:	6a 11                	push   $0x11
8010401c:	6a 20                	push   $0x20
8010401e:	e8 42 ff ff ff       	call   80103f65 <outb>
80104023:	83 c4 08             	add    $0x8,%esp

  // ICW2:  Vector offset
  outb(IO_PIC1+1, T_IRQ0);
80104026:	6a 20                	push   $0x20
80104028:	6a 21                	push   $0x21
8010402a:	e8 36 ff ff ff       	call   80103f65 <outb>
8010402f:	83 c4 08             	add    $0x8,%esp

  // ICW3:  (master PIC) bit mask of IR lines connected to slaves
  //        (slave PIC) 3-bit # of slave's connection to master
  outb(IO_PIC1+1, 1<<IRQ_SLAVE);
80104032:	6a 04                	push   $0x4
80104034:	6a 21                	push   $0x21
80104036:	e8 2a ff ff ff       	call   80103f65 <outb>
8010403b:	83 c4 08             	add    $0x8,%esp
  //    m:  0 = slave PIC, 1 = master PIC
  //      (ignored when b is 0, as the master/slave role
  //      can be hardwired).
  //    a:  1 = Automatic EOI mode
  //    p:  0 = MCS-80/85 mode, 1 = intel x86 mode
  outb(IO_PIC1+1, 0x3);
8010403e:	6a 03                	push   $0x3
80104040:	6a 21                	push   $0x21
80104042:	e8 1e ff ff ff       	call   80103f65 <outb>
80104047:	83 c4 08             	add    $0x8,%esp

  // Set up slave (8259A-2)
  outb(IO_PIC2, 0x11);                  // ICW1
8010404a:	6a 11                	push   $0x11
8010404c:	68 a0 00 00 00       	push   $0xa0
80104051:	e8 0f ff ff ff       	call   80103f65 <outb>
80104056:	83 c4 08             	add    $0x8,%esp
  outb(IO_PIC2+1, T_IRQ0 + 8);      // ICW2
80104059:	6a 28                	push   $0x28
8010405b:	68 a1 00 00 00       	push   $0xa1
80104060:	e8 00 ff ff ff       	call   80103f65 <outb>
80104065:	83 c4 08             	add    $0x8,%esp
  outb(IO_PIC2+1, IRQ_SLAVE);           // ICW3
80104068:	6a 02                	push   $0x2
8010406a:	68 a1 00 00 00       	push   $0xa1
8010406f:	e8 f1 fe ff ff       	call   80103f65 <outb>
80104074:	83 c4 08             	add    $0x8,%esp
  // NB Automatic EOI mode doesn't tend to work on the slave.
  // Linux source code says it's "to be investigated".
  outb(IO_PIC2+1, 0x3);                 // ICW4
80104077:	6a 03                	push   $0x3
80104079:	68 a1 00 00 00       	push   $0xa1
8010407e:	e8 e2 fe ff ff       	call   80103f65 <outb>
80104083:	83 c4 08             	add    $0x8,%esp

  // OCW3:  0ef01prs
  //   ef:  0x = NOP, 10 = clear specific mask, 11 = set specific mask
  //    p:  0 = no polling, 1 = polling mode
  //   rs:  0x = NOP, 10 = read IRR, 11 = read ISR
  outb(IO_PIC1, 0x68);             // clear specific mask
80104086:	6a 68                	push   $0x68
80104088:	6a 20                	push   $0x20
8010408a:	e8 d6 fe ff ff       	call   80103f65 <outb>
8010408f:	83 c4 08             	add    $0x8,%esp
  outb(IO_PIC1, 0x0a);             // read IRR by default
80104092:	6a 0a                	push   $0xa
80104094:	6a 20                	push   $0x20
80104096:	e8 ca fe ff ff       	call   80103f65 <outb>
8010409b:	83 c4 08             	add    $0x8,%esp

  outb(IO_PIC2, 0x68);             // OCW3
8010409e:	6a 68                	push   $0x68
801040a0:	68 a0 00 00 00       	push   $0xa0
801040a5:	e8 bb fe ff ff       	call   80103f65 <outb>
801040aa:	83 c4 08             	add    $0x8,%esp
  outb(IO_PIC2, 0x0a);             // OCW3
801040ad:	6a 0a                	push   $0xa
801040af:	68 a0 00 00 00       	push   $0xa0
801040b4:	e8 ac fe ff ff       	call   80103f65 <outb>
801040b9:	83 c4 08             	add    $0x8,%esp

  if(irqmask != 0xFFFF)
801040bc:	0f b7 05 00 c0 10 80 	movzwl 0x8010c000,%eax
801040c3:	66 83 f8 ff          	cmp    $0xffff,%ax
801040c7:	74 13                	je     801040dc <picinit+0xe6>
    picsetmask(irqmask);
801040c9:	0f b7 05 00 c0 10 80 	movzwl 0x8010c000,%eax
801040d0:	0f b7 c0             	movzwl %ax,%eax
801040d3:	50                   	push   %eax
801040d4:	e8 ab fe ff ff       	call   80103f84 <picsetmask>
801040d9:	83 c4 04             	add    $0x4,%esp
}
801040dc:	90                   	nop
801040dd:	c9                   	leave  
801040de:	c3                   	ret    

801040df <pipealloc>:
  int writeopen;  // write fd is still open
};

int
pipealloc(struct file **f0, struct file **f1)
{
801040df:	55                   	push   %ebp
801040e0:	89 e5                	mov    %esp,%ebp
801040e2:	83 ec 18             	sub    $0x18,%esp
  struct pipe *p;

  p = 0;
801040e5:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  *f0 = *f1 = 0;
801040ec:	8b 45 0c             	mov    0xc(%ebp),%eax
801040ef:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
801040f5:	8b 45 0c             	mov    0xc(%ebp),%eax
801040f8:	8b 10                	mov    (%eax),%edx
801040fa:	8b 45 08             	mov    0x8(%ebp),%eax
801040fd:	89 10                	mov    %edx,(%eax)
  if((*f0 = filealloc()) == 0 || (*f1 = filealloc()) == 0)
801040ff:	e8 2d cf ff ff       	call   80101031 <filealloc>
80104104:	89 c2                	mov    %eax,%edx
80104106:	8b 45 08             	mov    0x8(%ebp),%eax
80104109:	89 10                	mov    %edx,(%eax)
8010410b:	8b 45 08             	mov    0x8(%ebp),%eax
8010410e:	8b 00                	mov    (%eax),%eax
80104110:	85 c0                	test   %eax,%eax
80104112:	0f 84 cb 00 00 00    	je     801041e3 <pipealloc+0x104>
80104118:	e8 14 cf ff ff       	call   80101031 <filealloc>
8010411d:	89 c2                	mov    %eax,%edx
8010411f:	8b 45 0c             	mov    0xc(%ebp),%eax
80104122:	89 10                	mov    %edx,(%eax)
80104124:	8b 45 0c             	mov    0xc(%ebp),%eax
80104127:	8b 00                	mov    (%eax),%eax
80104129:	85 c0                	test   %eax,%eax
8010412b:	0f 84 b2 00 00 00    	je     801041e3 <pipealloc+0x104>
    goto bad;
  if((p = (struct pipe*)kalloc()) == 0)
80104131:	e8 ce eb ff ff       	call   80102d04 <kalloc>
80104136:	89 45 f4             	mov    %eax,-0xc(%ebp)
80104139:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
8010413d:	0f 84 9f 00 00 00    	je     801041e2 <pipealloc+0x103>
    goto bad;
  p->readopen = 1;
80104143:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104146:	c7 80 3c 02 00 00 01 	movl   $0x1,0x23c(%eax)
8010414d:	00 00 00 
  p->writeopen = 1;
80104150:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104153:	c7 80 40 02 00 00 01 	movl   $0x1,0x240(%eax)
8010415a:	00 00 00 
  p->nwrite = 0;
8010415d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104160:	c7 80 38 02 00 00 00 	movl   $0x0,0x238(%eax)
80104167:	00 00 00 
  p->nread = 0;
8010416a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010416d:	c7 80 34 02 00 00 00 	movl   $0x0,0x234(%eax)
80104174:	00 00 00 
  initlock(&p->lock, "pipe");
80104177:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010417a:	83 ec 08             	sub    $0x8,%esp
8010417d:	68 48 95 10 80       	push   $0x80109548
80104182:	50                   	push   %eax
80104183:	e8 9e 19 00 00       	call   80105b26 <initlock>
80104188:	83 c4 10             	add    $0x10,%esp
  (*f0)->type = FD_PIPE;
8010418b:	8b 45 08             	mov    0x8(%ebp),%eax
8010418e:	8b 00                	mov    (%eax),%eax
80104190:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
  (*f0)->readable = 1;
80104196:	8b 45 08             	mov    0x8(%ebp),%eax
80104199:	8b 00                	mov    (%eax),%eax
8010419b:	c6 40 08 01          	movb   $0x1,0x8(%eax)
  (*f0)->writable = 0;
8010419f:	8b 45 08             	mov    0x8(%ebp),%eax
801041a2:	8b 00                	mov    (%eax),%eax
801041a4:	c6 40 09 00          	movb   $0x0,0x9(%eax)
  (*f0)->pipe = p;
801041a8:	8b 45 08             	mov    0x8(%ebp),%eax
801041ab:	8b 00                	mov    (%eax),%eax
801041ad:	8b 55 f4             	mov    -0xc(%ebp),%edx
801041b0:	89 50 0c             	mov    %edx,0xc(%eax)
  (*f1)->type = FD_PIPE;
801041b3:	8b 45 0c             	mov    0xc(%ebp),%eax
801041b6:	8b 00                	mov    (%eax),%eax
801041b8:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
  (*f1)->readable = 0;
801041be:	8b 45 0c             	mov    0xc(%ebp),%eax
801041c1:	8b 00                	mov    (%eax),%eax
801041c3:	c6 40 08 00          	movb   $0x0,0x8(%eax)
  (*f1)->writable = 1;
801041c7:	8b 45 0c             	mov    0xc(%ebp),%eax
801041ca:	8b 00                	mov    (%eax),%eax
801041cc:	c6 40 09 01          	movb   $0x1,0x9(%eax)
  (*f1)->pipe = p;
801041d0:	8b 45 0c             	mov    0xc(%ebp),%eax
801041d3:	8b 00                	mov    (%eax),%eax
801041d5:	8b 55 f4             	mov    -0xc(%ebp),%edx
801041d8:	89 50 0c             	mov    %edx,0xc(%eax)
  return 0;
801041db:	b8 00 00 00 00       	mov    $0x0,%eax
801041e0:	eb 4e                	jmp    80104230 <pipealloc+0x151>
  p = 0;
  *f0 = *f1 = 0;
  if((*f0 = filealloc()) == 0 || (*f1 = filealloc()) == 0)
    goto bad;
  if((p = (struct pipe*)kalloc()) == 0)
    goto bad;
801041e2:	90                   	nop
  (*f1)->writable = 1;
  (*f1)->pipe = p;
  return 0;

 bad:
  if(p)
801041e3:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801041e7:	74 0e                	je     801041f7 <pipealloc+0x118>
    kfree((char*)p);
801041e9:	83 ec 0c             	sub    $0xc,%esp
801041ec:	ff 75 f4             	pushl  -0xc(%ebp)
801041ef:	e8 73 ea ff ff       	call   80102c67 <kfree>
801041f4:	83 c4 10             	add    $0x10,%esp
  if(*f0)
801041f7:	8b 45 08             	mov    0x8(%ebp),%eax
801041fa:	8b 00                	mov    (%eax),%eax
801041fc:	85 c0                	test   %eax,%eax
801041fe:	74 11                	je     80104211 <pipealloc+0x132>
    fileclose(*f0);
80104200:	8b 45 08             	mov    0x8(%ebp),%eax
80104203:	8b 00                	mov    (%eax),%eax
80104205:	83 ec 0c             	sub    $0xc,%esp
80104208:	50                   	push   %eax
80104209:	e8 e1 ce ff ff       	call   801010ef <fileclose>
8010420e:	83 c4 10             	add    $0x10,%esp
  if(*f1)
80104211:	8b 45 0c             	mov    0xc(%ebp),%eax
80104214:	8b 00                	mov    (%eax),%eax
80104216:	85 c0                	test   %eax,%eax
80104218:	74 11                	je     8010422b <pipealloc+0x14c>
    fileclose(*f1);
8010421a:	8b 45 0c             	mov    0xc(%ebp),%eax
8010421d:	8b 00                	mov    (%eax),%eax
8010421f:	83 ec 0c             	sub    $0xc,%esp
80104222:	50                   	push   %eax
80104223:	e8 c7 ce ff ff       	call   801010ef <fileclose>
80104228:	83 c4 10             	add    $0x10,%esp
  return -1;
8010422b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80104230:	c9                   	leave  
80104231:	c3                   	ret    

80104232 <pipeclose>:

void
pipeclose(struct pipe *p, int writable)
{
80104232:	55                   	push   %ebp
80104233:	89 e5                	mov    %esp,%ebp
80104235:	83 ec 08             	sub    $0x8,%esp
  acquire(&p->lock);
80104238:	8b 45 08             	mov    0x8(%ebp),%eax
8010423b:	83 ec 0c             	sub    $0xc,%esp
8010423e:	50                   	push   %eax
8010423f:	e8 04 19 00 00       	call   80105b48 <acquire>
80104244:	83 c4 10             	add    $0x10,%esp
  if(writable){
80104247:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
8010424b:	74 23                	je     80104270 <pipeclose+0x3e>
    p->writeopen = 0;
8010424d:	8b 45 08             	mov    0x8(%ebp),%eax
80104250:	c7 80 40 02 00 00 00 	movl   $0x0,0x240(%eax)
80104257:	00 00 00 
    wakeup(&p->nread);
8010425a:	8b 45 08             	mov    0x8(%ebp),%eax
8010425d:	05 34 02 00 00       	add    $0x234,%eax
80104262:	83 ec 0c             	sub    $0xc,%esp
80104265:	50                   	push   %eax
80104266:	e8 0e 12 00 00       	call   80105479 <wakeup>
8010426b:	83 c4 10             	add    $0x10,%esp
8010426e:	eb 21                	jmp    80104291 <pipeclose+0x5f>
  } else {
    p->readopen = 0;
80104270:	8b 45 08             	mov    0x8(%ebp),%eax
80104273:	c7 80 3c 02 00 00 00 	movl   $0x0,0x23c(%eax)
8010427a:	00 00 00 
    wakeup(&p->nwrite);
8010427d:	8b 45 08             	mov    0x8(%ebp),%eax
80104280:	05 38 02 00 00       	add    $0x238,%eax
80104285:	83 ec 0c             	sub    $0xc,%esp
80104288:	50                   	push   %eax
80104289:	e8 eb 11 00 00       	call   80105479 <wakeup>
8010428e:	83 c4 10             	add    $0x10,%esp
  }
  if(p->readopen == 0 && p->writeopen == 0){
80104291:	8b 45 08             	mov    0x8(%ebp),%eax
80104294:	8b 80 3c 02 00 00    	mov    0x23c(%eax),%eax
8010429a:	85 c0                	test   %eax,%eax
8010429c:	75 2c                	jne    801042ca <pipeclose+0x98>
8010429e:	8b 45 08             	mov    0x8(%ebp),%eax
801042a1:	8b 80 40 02 00 00    	mov    0x240(%eax),%eax
801042a7:	85 c0                	test   %eax,%eax
801042a9:	75 1f                	jne    801042ca <pipeclose+0x98>
    release(&p->lock);
801042ab:	8b 45 08             	mov    0x8(%ebp),%eax
801042ae:	83 ec 0c             	sub    $0xc,%esp
801042b1:	50                   	push   %eax
801042b2:	e8 f8 18 00 00       	call   80105baf <release>
801042b7:	83 c4 10             	add    $0x10,%esp
    kfree((char*)p);
801042ba:	83 ec 0c             	sub    $0xc,%esp
801042bd:	ff 75 08             	pushl  0x8(%ebp)
801042c0:	e8 a2 e9 ff ff       	call   80102c67 <kfree>
801042c5:	83 c4 10             	add    $0x10,%esp
801042c8:	eb 0f                	jmp    801042d9 <pipeclose+0xa7>
  } else
    release(&p->lock);
801042ca:	8b 45 08             	mov    0x8(%ebp),%eax
801042cd:	83 ec 0c             	sub    $0xc,%esp
801042d0:	50                   	push   %eax
801042d1:	e8 d9 18 00 00       	call   80105baf <release>
801042d6:	83 c4 10             	add    $0x10,%esp
}
801042d9:	90                   	nop
801042da:	c9                   	leave  
801042db:	c3                   	ret    

801042dc <pipewrite>:

int
pipewrite(struct pipe *p, char *addr, int n)
{
801042dc:	55                   	push   %ebp
801042dd:	89 e5                	mov    %esp,%ebp
801042df:	83 ec 18             	sub    $0x18,%esp
  int i;

  acquire(&p->lock);
801042e2:	8b 45 08             	mov    0x8(%ebp),%eax
801042e5:	83 ec 0c             	sub    $0xc,%esp
801042e8:	50                   	push   %eax
801042e9:	e8 5a 18 00 00       	call   80105b48 <acquire>
801042ee:	83 c4 10             	add    $0x10,%esp
  for(i = 0; i < n; i++){
801042f1:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
801042f8:	e9 ad 00 00 00       	jmp    801043aa <pipewrite+0xce>
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
      if(p->readopen == 0 || proc->killed){
801042fd:	8b 45 08             	mov    0x8(%ebp),%eax
80104300:	8b 80 3c 02 00 00    	mov    0x23c(%eax),%eax
80104306:	85 c0                	test   %eax,%eax
80104308:	74 0d                	je     80104317 <pipewrite+0x3b>
8010430a:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104310:	8b 40 24             	mov    0x24(%eax),%eax
80104313:	85 c0                	test   %eax,%eax
80104315:	74 19                	je     80104330 <pipewrite+0x54>
        release(&p->lock);
80104317:	8b 45 08             	mov    0x8(%ebp),%eax
8010431a:	83 ec 0c             	sub    $0xc,%esp
8010431d:	50                   	push   %eax
8010431e:	e8 8c 18 00 00       	call   80105baf <release>
80104323:	83 c4 10             	add    $0x10,%esp
        return -1;
80104326:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010432b:	e9 a8 00 00 00       	jmp    801043d8 <pipewrite+0xfc>
      }
      wakeup(&p->nread);
80104330:	8b 45 08             	mov    0x8(%ebp),%eax
80104333:	05 34 02 00 00       	add    $0x234,%eax
80104338:	83 ec 0c             	sub    $0xc,%esp
8010433b:	50                   	push   %eax
8010433c:	e8 38 11 00 00       	call   80105479 <wakeup>
80104341:	83 c4 10             	add    $0x10,%esp
      sleep(&p->nwrite, &p->lock);  //DOC: pipewrite-sleep
80104344:	8b 45 08             	mov    0x8(%ebp),%eax
80104347:	8b 55 08             	mov    0x8(%ebp),%edx
8010434a:	81 c2 38 02 00 00    	add    $0x238,%edx
80104350:	83 ec 08             	sub    $0x8,%esp
80104353:	50                   	push   %eax
80104354:	52                   	push   %edx
80104355:	e8 0a 10 00 00       	call   80105364 <sleep>
8010435a:	83 c4 10             	add    $0x10,%esp
{
  int i;

  acquire(&p->lock);
  for(i = 0; i < n; i++){
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
8010435d:	8b 45 08             	mov    0x8(%ebp),%eax
80104360:	8b 90 38 02 00 00    	mov    0x238(%eax),%edx
80104366:	8b 45 08             	mov    0x8(%ebp),%eax
80104369:	8b 80 34 02 00 00    	mov    0x234(%eax),%eax
8010436f:	05 00 02 00 00       	add    $0x200,%eax
80104374:	39 c2                	cmp    %eax,%edx
80104376:	74 85                	je     801042fd <pipewrite+0x21>
        return -1;
      }
      wakeup(&p->nread);
      sleep(&p->nwrite, &p->lock);  //DOC: pipewrite-sleep
    }
    p->data[p->nwrite++ % PIPESIZE] = addr[i];
80104378:	8b 45 08             	mov    0x8(%ebp),%eax
8010437b:	8b 80 38 02 00 00    	mov    0x238(%eax),%eax
80104381:	8d 48 01             	lea    0x1(%eax),%ecx
80104384:	8b 55 08             	mov    0x8(%ebp),%edx
80104387:	89 8a 38 02 00 00    	mov    %ecx,0x238(%edx)
8010438d:	25 ff 01 00 00       	and    $0x1ff,%eax
80104392:	89 c1                	mov    %eax,%ecx
80104394:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104397:	8b 45 0c             	mov    0xc(%ebp),%eax
8010439a:	01 d0                	add    %edx,%eax
8010439c:	0f b6 10             	movzbl (%eax),%edx
8010439f:	8b 45 08             	mov    0x8(%ebp),%eax
801043a2:	88 54 08 34          	mov    %dl,0x34(%eax,%ecx,1)
pipewrite(struct pipe *p, char *addr, int n)
{
  int i;

  acquire(&p->lock);
  for(i = 0; i < n; i++){
801043a6:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
801043aa:	8b 45 f4             	mov    -0xc(%ebp),%eax
801043ad:	3b 45 10             	cmp    0x10(%ebp),%eax
801043b0:	7c ab                	jl     8010435d <pipewrite+0x81>
      wakeup(&p->nread);
      sleep(&p->nwrite, &p->lock);  //DOC: pipewrite-sleep
    }
    p->data[p->nwrite++ % PIPESIZE] = addr[i];
  }
  wakeup(&p->nread);  //DOC: pipewrite-wakeup1
801043b2:	8b 45 08             	mov    0x8(%ebp),%eax
801043b5:	05 34 02 00 00       	add    $0x234,%eax
801043ba:	83 ec 0c             	sub    $0xc,%esp
801043bd:	50                   	push   %eax
801043be:	e8 b6 10 00 00       	call   80105479 <wakeup>
801043c3:	83 c4 10             	add    $0x10,%esp
  release(&p->lock);
801043c6:	8b 45 08             	mov    0x8(%ebp),%eax
801043c9:	83 ec 0c             	sub    $0xc,%esp
801043cc:	50                   	push   %eax
801043cd:	e8 dd 17 00 00       	call   80105baf <release>
801043d2:	83 c4 10             	add    $0x10,%esp
  return n;
801043d5:	8b 45 10             	mov    0x10(%ebp),%eax
}
801043d8:	c9                   	leave  
801043d9:	c3                   	ret    

801043da <piperead>:

int
piperead(struct pipe *p, char *addr, int n)
{
801043da:	55                   	push   %ebp
801043db:	89 e5                	mov    %esp,%ebp
801043dd:	53                   	push   %ebx
801043de:	83 ec 14             	sub    $0x14,%esp
  int i;

  acquire(&p->lock);
801043e1:	8b 45 08             	mov    0x8(%ebp),%eax
801043e4:	83 ec 0c             	sub    $0xc,%esp
801043e7:	50                   	push   %eax
801043e8:	e8 5b 17 00 00       	call   80105b48 <acquire>
801043ed:	83 c4 10             	add    $0x10,%esp
  while(p->nread == p->nwrite && p->writeopen){  //DOC: pipe-empty
801043f0:	eb 3f                	jmp    80104431 <piperead+0x57>
    if(proc->killed){
801043f2:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801043f8:	8b 40 24             	mov    0x24(%eax),%eax
801043fb:	85 c0                	test   %eax,%eax
801043fd:	74 19                	je     80104418 <piperead+0x3e>
      release(&p->lock);
801043ff:	8b 45 08             	mov    0x8(%ebp),%eax
80104402:	83 ec 0c             	sub    $0xc,%esp
80104405:	50                   	push   %eax
80104406:	e8 a4 17 00 00       	call   80105baf <release>
8010440b:	83 c4 10             	add    $0x10,%esp
      return -1;
8010440e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104413:	e9 bf 00 00 00       	jmp    801044d7 <piperead+0xfd>
    }
    sleep(&p->nread, &p->lock); //DOC: piperead-sleep
80104418:	8b 45 08             	mov    0x8(%ebp),%eax
8010441b:	8b 55 08             	mov    0x8(%ebp),%edx
8010441e:	81 c2 34 02 00 00    	add    $0x234,%edx
80104424:	83 ec 08             	sub    $0x8,%esp
80104427:	50                   	push   %eax
80104428:	52                   	push   %edx
80104429:	e8 36 0f 00 00       	call   80105364 <sleep>
8010442e:	83 c4 10             	add    $0x10,%esp
piperead(struct pipe *p, char *addr, int n)
{
  int i;

  acquire(&p->lock);
  while(p->nread == p->nwrite && p->writeopen){  //DOC: pipe-empty
80104431:	8b 45 08             	mov    0x8(%ebp),%eax
80104434:	8b 90 34 02 00 00    	mov    0x234(%eax),%edx
8010443a:	8b 45 08             	mov    0x8(%ebp),%eax
8010443d:	8b 80 38 02 00 00    	mov    0x238(%eax),%eax
80104443:	39 c2                	cmp    %eax,%edx
80104445:	75 0d                	jne    80104454 <piperead+0x7a>
80104447:	8b 45 08             	mov    0x8(%ebp),%eax
8010444a:	8b 80 40 02 00 00    	mov    0x240(%eax),%eax
80104450:	85 c0                	test   %eax,%eax
80104452:	75 9e                	jne    801043f2 <piperead+0x18>
      release(&p->lock);
      return -1;
    }
    sleep(&p->nread, &p->lock); //DOC: piperead-sleep
  }
  for(i = 0; i < n; i++){  //DOC: piperead-copy
80104454:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
8010445b:	eb 49                	jmp    801044a6 <piperead+0xcc>
    if(p->nread == p->nwrite)
8010445d:	8b 45 08             	mov    0x8(%ebp),%eax
80104460:	8b 90 34 02 00 00    	mov    0x234(%eax),%edx
80104466:	8b 45 08             	mov    0x8(%ebp),%eax
80104469:	8b 80 38 02 00 00    	mov    0x238(%eax),%eax
8010446f:	39 c2                	cmp    %eax,%edx
80104471:	74 3d                	je     801044b0 <piperead+0xd6>
      break;
    addr[i] = p->data[p->nread++ % PIPESIZE];
80104473:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104476:	8b 45 0c             	mov    0xc(%ebp),%eax
80104479:	8d 1c 02             	lea    (%edx,%eax,1),%ebx
8010447c:	8b 45 08             	mov    0x8(%ebp),%eax
8010447f:	8b 80 34 02 00 00    	mov    0x234(%eax),%eax
80104485:	8d 48 01             	lea    0x1(%eax),%ecx
80104488:	8b 55 08             	mov    0x8(%ebp),%edx
8010448b:	89 8a 34 02 00 00    	mov    %ecx,0x234(%edx)
80104491:	25 ff 01 00 00       	and    $0x1ff,%eax
80104496:	89 c2                	mov    %eax,%edx
80104498:	8b 45 08             	mov    0x8(%ebp),%eax
8010449b:	0f b6 44 10 34       	movzbl 0x34(%eax,%edx,1),%eax
801044a0:	88 03                	mov    %al,(%ebx)
      release(&p->lock);
      return -1;
    }
    sleep(&p->nread, &p->lock); //DOC: piperead-sleep
  }
  for(i = 0; i < n; i++){  //DOC: piperead-copy
801044a2:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
801044a6:	8b 45 f4             	mov    -0xc(%ebp),%eax
801044a9:	3b 45 10             	cmp    0x10(%ebp),%eax
801044ac:	7c af                	jl     8010445d <piperead+0x83>
801044ae:	eb 01                	jmp    801044b1 <piperead+0xd7>
    if(p->nread == p->nwrite)
      break;
801044b0:	90                   	nop
    addr[i] = p->data[p->nread++ % PIPESIZE];
  }
  wakeup(&p->nwrite);  //DOC: piperead-wakeup
801044b1:	8b 45 08             	mov    0x8(%ebp),%eax
801044b4:	05 38 02 00 00       	add    $0x238,%eax
801044b9:	83 ec 0c             	sub    $0xc,%esp
801044bc:	50                   	push   %eax
801044bd:	e8 b7 0f 00 00       	call   80105479 <wakeup>
801044c2:	83 c4 10             	add    $0x10,%esp
  release(&p->lock);
801044c5:	8b 45 08             	mov    0x8(%ebp),%eax
801044c8:	83 ec 0c             	sub    $0xc,%esp
801044cb:	50                   	push   %eax
801044cc:	e8 de 16 00 00       	call   80105baf <release>
801044d1:	83 c4 10             	add    $0x10,%esp
  return i;
801044d4:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
801044d7:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801044da:	c9                   	leave  
801044db:	c3                   	ret    

801044dc <hlt>:
}

// hlt() added by Noah Zentzis, Fall 2016.
static inline void
hlt()
{
801044dc:	55                   	push   %ebp
801044dd:	89 e5                	mov    %esp,%ebp
  asm volatile("hlt");
801044df:	f4                   	hlt    
}
801044e0:	90                   	nop
801044e1:	5d                   	pop    %ebp
801044e2:	c3                   	ret    

801044e3 <readeflags>:
  asm volatile("ltr %0" : : "r" (sel));
}

static inline uint
readeflags(void)
{
801044e3:	55                   	push   %ebp
801044e4:	89 e5                	mov    %esp,%ebp
801044e6:	83 ec 10             	sub    $0x10,%esp
  uint eflags;
  asm volatile("pushfl; popl %0" : "=r" (eflags));
801044e9:	9c                   	pushf  
801044ea:	58                   	pop    %eax
801044eb:	89 45 fc             	mov    %eax,-0x4(%ebp)
  return eflags;
801044ee:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
801044f1:	c9                   	leave  
801044f2:	c3                   	ret    

801044f3 <sti>:
  asm volatile("cli");
}

static inline void
sti(void)
{
801044f3:	55                   	push   %ebp
801044f4:	89 e5                	mov    %esp,%ebp
  asm volatile("sti");
801044f6:	fb                   	sti    
}
801044f7:	90                   	nop
801044f8:	5d                   	pop    %ebp
801044f9:	c3                   	ret    

801044fa <addToStateList>:

static void wakeup1(void *chan);
//****************************************************************************************************************************************
#ifdef CS333_P3P4
static int
addToStateList(struct proc **head, struct proc *p){	//add to front of list
801044fa:	55                   	push   %ebp
801044fb:	89 e5                	mov    %esp,%ebp
  p->next = (*head);
801044fd:	8b 45 08             	mov    0x8(%ebp),%eax
80104500:	8b 10                	mov    (%eax),%edx
80104502:	8b 45 0c             	mov    0xc(%ebp),%eax
80104505:	89 90 90 00 00 00    	mov    %edx,0x90(%eax)
  (*head) = p;
8010450b:	8b 45 08             	mov    0x8(%ebp),%eax
8010450e:	8b 55 0c             	mov    0xc(%ebp),%edx
80104511:	89 10                	mov    %edx,(%eax)
  return 0;
80104513:	b8 00 00 00 00       	mov    $0x0,%eax
}
80104518:	5d                   	pop    %ebp
80104519:	c3                   	ret    

8010451a <addToEnd>:
static int
addToEnd(struct proc ** head, struct proc *p){		//add to end of list
8010451a:	55                   	push   %ebp
8010451b:	89 e5                	mov    %esp,%ebp
8010451d:	83 ec 10             	sub    $0x10,%esp
  if(!*head){
80104520:	8b 45 08             	mov    0x8(%ebp),%eax
80104523:	8b 00                	mov    (%eax),%eax
80104525:	85 c0                	test   %eax,%eax
80104527:	75 1e                	jne    80104547 <addToEnd+0x2d>
    *head = p;
80104529:	8b 45 08             	mov    0x8(%ebp),%eax
8010452c:	8b 55 0c             	mov    0xc(%ebp),%edx
8010452f:	89 10                	mov    %edx,(%eax)
    (*head)->next = 0;
80104531:	8b 45 08             	mov    0x8(%ebp),%eax
80104534:	8b 00                	mov    (%eax),%eax
80104536:	c7 80 90 00 00 00 00 	movl   $0x0,0x90(%eax)
8010453d:	00 00 00 
    return 0;
80104540:	b8 00 00 00 00       	mov    $0x0,%eax
80104545:	eb 47                	jmp    8010458e <addToEnd+0x74>
  }
  struct proc * current = *head;
80104547:	8b 45 08             	mov    0x8(%ebp),%eax
8010454a:	8b 00                	mov    (%eax),%eax
8010454c:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while(!current->next)
8010454f:	eb 0c                	jmp    8010455d <addToEnd+0x43>
    current = current->next; 
80104551:	8b 45 fc             	mov    -0x4(%ebp),%eax
80104554:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
8010455a:	89 45 fc             	mov    %eax,-0x4(%ebp)
    *head = p;
    (*head)->next = 0;
    return 0;
  }
  struct proc * current = *head;
  while(!current->next)
8010455d:	8b 45 fc             	mov    -0x4(%ebp),%eax
80104560:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
80104566:	85 c0                	test   %eax,%eax
80104568:	74 e7                	je     80104551 <addToEnd+0x37>
    current = current->next; 
  current->next = p;
8010456a:	8b 45 fc             	mov    -0x4(%ebp),%eax
8010456d:	8b 55 0c             	mov    0xc(%ebp),%edx
80104570:	89 90 90 00 00 00    	mov    %edx,0x90(%eax)
  current->next->next = 0;  
80104576:	8b 45 fc             	mov    -0x4(%ebp),%eax
80104579:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
8010457f:	c7 80 90 00 00 00 00 	movl   $0x0,0x90(%eax)
80104586:	00 00 00 
  return 0;
80104589:	b8 00 00 00 00       	mov    $0x0,%eax
}
8010458e:	c9                   	leave  
8010458f:	c3                   	ret    

80104590 <removeFromStateList>:
static int
removeFromStateList(struct proc **head, struct proc *p){//remove from list
80104590:	55                   	push   %ebp
80104591:	89 e5                	mov    %esp,%ebp
80104593:	83 ec 18             	sub    $0x18,%esp
  if(!*head)
80104596:	8b 45 08             	mov    0x8(%ebp),%eax
80104599:	8b 00                	mov    (%eax),%eax
8010459b:	85 c0                	test   %eax,%eax
8010459d:	75 0d                	jne    801045ac <removeFromStateList+0x1c>
    panic("Error: List is empty.");
8010459f:	83 ec 0c             	sub    $0xc,%esp
801045a2:	68 50 95 10 80       	push   $0x80109550
801045a7:	e8 ba bf ff ff       	call   80100566 <panic>
  if(*head == p){
801045ac:	8b 45 08             	mov    0x8(%ebp),%eax
801045af:	8b 00                	mov    (%eax),%eax
801045b1:	3b 45 0c             	cmp    0xc(%ebp),%eax
801045b4:	75 22                	jne    801045d8 <removeFromStateList+0x48>
    (*head) = p->next;
801045b6:	8b 45 0c             	mov    0xc(%ebp),%eax
801045b9:	8b 90 90 00 00 00    	mov    0x90(%eax),%edx
801045bf:	8b 45 08             	mov    0x8(%ebp),%eax
801045c2:	89 10                	mov    %edx,(%eax)
    p->next = 0;
801045c4:	8b 45 0c             	mov    0xc(%ebp),%eax
801045c7:	c7 80 90 00 00 00 00 	movl   $0x0,0x90(%eax)
801045ce:	00 00 00 
   return 0;
801045d1:	b8 00 00 00 00       	mov    $0x0,%eax
801045d6:	eb 75                	jmp    8010464d <removeFromStateList+0xbd>
  }
  struct proc * tmp = *head;
801045d8:	8b 45 08             	mov    0x8(%ebp),%eax
801045db:	8b 00                	mov    (%eax),%eax
801045dd:	89 45 f4             	mov    %eax,-0xc(%ebp)
  while(tmp->next != 0 && tmp->next != p)
801045e0:	eb 0c                	jmp    801045ee <removeFromStateList+0x5e>
    tmp = tmp->next;
801045e2:	8b 45 f4             	mov    -0xc(%ebp),%eax
801045e5:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
801045eb:	89 45 f4             	mov    %eax,-0xc(%ebp)
    (*head) = p->next;
    p->next = 0;
   return 0;
  }
  struct proc * tmp = *head;
  while(tmp->next != 0 && tmp->next != p)
801045ee:	8b 45 f4             	mov    -0xc(%ebp),%eax
801045f1:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
801045f7:	85 c0                	test   %eax,%eax
801045f9:	74 0e                	je     80104609 <removeFromStateList+0x79>
801045fb:	8b 45 f4             	mov    -0xc(%ebp),%eax
801045fe:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
80104604:	3b 45 0c             	cmp    0xc(%ebp),%eax
80104607:	75 d9                	jne    801045e2 <removeFromStateList+0x52>
    tmp = tmp->next;
  if(tmp->next == 0)
80104609:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010460c:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
80104612:	85 c0                	test   %eax,%eax
80104614:	75 0d                	jne    80104623 <removeFromStateList+0x93>
    panic("Error: p is not in list.");
80104616:	83 ec 0c             	sub    $0xc,%esp
80104619:	68 66 95 10 80       	push   $0x80109566
8010461e:	e8 43 bf ff ff       	call   80100566 <panic>
  tmp->next = tmp->next->next;
80104623:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104626:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
8010462c:	8b 90 90 00 00 00    	mov    0x90(%eax),%edx
80104632:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104635:	89 90 90 00 00 00    	mov    %edx,0x90(%eax)
  p->next = 0;
8010463b:	8b 45 0c             	mov    0xc(%ebp),%eax
8010463e:	c7 80 90 00 00 00 00 	movl   $0x0,0x90(%eax)
80104645:	00 00 00 
  return 0;
80104648:	b8 00 00 00 00       	mov    $0x0,%eax
}
8010464d:	c9                   	leave  
8010464e:	c3                   	ret    

8010464f <assertState>:
static void
assertState(struct proc *p, enum procstate state){	//check for state process
8010464f:	55                   	push   %ebp
80104650:	89 e5                	mov    %esp,%ebp
80104652:	83 ec 08             	sub    $0x8,%esp
  if(p->state == state)
80104655:	8b 45 08             	mov    0x8(%ebp),%eax
80104658:	8b 40 0c             	mov    0xc(%eax),%eax
8010465b:	3b 45 0c             	cmp    0xc(%ebp),%eax
8010465e:	74 0d                	je     8010466d <assertState+0x1e>
    return;
  panic("Error: Proccess state incorrect in assertState()");
80104660:	83 ec 0c             	sub    $0xc,%esp
80104663:	68 80 95 10 80       	push   $0x80109580
80104668:	e8 f9 be ff ff       	call   80100566 <panic>
  return 0;
}
static void
assertState(struct proc *p, enum procstate state){	//check for state process
  if(p->state == state)
    return;
8010466d:	90                   	nop
  panic("Error: Proccess state incorrect in assertState()");
}
8010466e:	c9                   	leave  
8010466f:	c3                   	ret    

80104670 <fromToStateList>:
static void						//general state transition 
fromToStateList(struct proc *p, enum procstate fromState, enum procstate toState, struct proc **from, struct proc **to){
80104670:	55                   	push   %ebp
80104671:	89 e5                	mov    %esp,%ebp
80104673:	83 ec 08             	sub    $0x8,%esp
  if(!holding(&ptable.lock))
80104676:	83 ec 0c             	sub    $0xc,%esp
80104679:	68 80 39 11 80       	push   $0x80113980
8010467e:	e8 f8 15 00 00       	call   80105c7b <holding>
80104683:	83 c4 10             	add    $0x10,%esp
80104686:	85 c0                	test   %eax,%eax
80104688:	75 0d                	jne    80104697 <fromToStateList+0x27>
    panic("In fromToStateList(), not holding lock");
8010468a:	83 ec 0c             	sub    $0xc,%esp
8010468d:	68 b4 95 10 80       	push   $0x801095b4
80104692:	e8 cf be ff ff       	call   80100566 <panic>
  removeFromStateList(from, p);
80104697:	83 ec 08             	sub    $0x8,%esp
8010469a:	ff 75 08             	pushl  0x8(%ebp)
8010469d:	ff 75 14             	pushl  0x14(%ebp)
801046a0:	e8 eb fe ff ff       	call   80104590 <removeFromStateList>
801046a5:	83 c4 10             	add    $0x10,%esp
  assertState(p, fromState);
801046a8:	83 ec 08             	sub    $0x8,%esp
801046ab:	ff 75 0c             	pushl  0xc(%ebp)
801046ae:	ff 75 08             	pushl  0x8(%ebp)
801046b1:	e8 99 ff ff ff       	call   8010464f <assertState>
801046b6:	83 c4 10             	add    $0x10,%esp
  p->state = toState;
801046b9:	8b 45 08             	mov    0x8(%ebp),%eax
801046bc:	8b 55 10             	mov    0x10(%ebp),%edx
801046bf:	89 50 0c             	mov    %edx,0xc(%eax)
  if(toState == RUNNABLE)
801046c2:	83 7d 10 03          	cmpl   $0x3,0x10(%ebp)
801046c6:	75 13                	jne    801046db <fromToStateList+0x6b>
    addToEnd(to, p);
801046c8:	83 ec 08             	sub    $0x8,%esp
801046cb:	ff 75 08             	pushl  0x8(%ebp)
801046ce:	ff 75 18             	pushl  0x18(%ebp)
801046d1:	e8 44 fe ff ff       	call   8010451a <addToEnd>
801046d6:	83 c4 10             	add    $0x10,%esp
  else
    addToStateList(to, p);
}
801046d9:	eb 11                	jmp    801046ec <fromToStateList+0x7c>
  assertState(p, fromState);
  p->state = toState;
  if(toState == RUNNABLE)
    addToEnd(to, p);
  else
    addToStateList(to, p);
801046db:	83 ec 08             	sub    $0x8,%esp
801046de:	ff 75 08             	pushl  0x8(%ebp)
801046e1:	ff 75 18             	pushl  0x18(%ebp)
801046e4:	e8 11 fe ff ff       	call   801044fa <addToStateList>
801046e9:	83 c4 10             	add    $0x10,%esp
}
801046ec:	90                   	nop
801046ed:	c9                   	leave  
801046ee:	c3                   	ret    

801046ef <count>:
#endif
//****************************************************************************************************************************************
int
count(struct proc *head){
801046ef:	55                   	push   %ebp
801046f0:	89 e5                	mov    %esp,%ebp
801046f2:	83 ec 10             	sub    $0x10,%esp
  if(head == 0)
801046f5:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
801046f9:	75 07                	jne    80104702 <count+0x13>
    return 0;
801046fb:	b8 00 00 00 00       	mov    $0x0,%eax
80104700:	eb 28                	jmp    8010472a <count+0x3b>
  int count = 0;
80104702:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  struct proc * current = head;
80104709:	8b 45 08             	mov    0x8(%ebp),%eax
8010470c:	89 45 f8             	mov    %eax,-0x8(%ebp)
  while(current != 0){
8010470f:	eb 10                	jmp    80104721 <count+0x32>
    ++count;
80104711:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
    current = current->next;
80104715:	8b 45 f8             	mov    -0x8(%ebp),%eax
80104718:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
8010471e:	89 45 f8             	mov    %eax,-0x8(%ebp)
count(struct proc *head){
  if(head == 0)
    return 0;
  int count = 0;
  struct proc * current = head;
  while(current != 0){
80104721:	83 7d f8 00          	cmpl   $0x0,-0x8(%ebp)
80104725:	75 ea                	jne    80104711 <count+0x22>
    ++count;
    current = current->next;
  }
  return count;
80104727:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
8010472a:	c9                   	leave  
8010472b:	c3                   	ret    

8010472c <readysleep>:
void
readysleep(struct proc * head){
8010472c:	55                   	push   %ebp
8010472d:	89 e5                	mov    %esp,%ebp
8010472f:	83 ec 08             	sub    $0x8,%esp
  if(head == 0)
80104732:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80104736:	74 5b                	je     80104793 <readysleep+0x67>
    return;
  cprintf("%d", head->pid);
80104738:	8b 45 08             	mov    0x8(%ebp),%eax
8010473b:	8b 40 10             	mov    0x10(%eax),%eax
8010473e:	83 ec 08             	sub    $0x8,%esp
80104741:	50                   	push   %eax
80104742:	68 db 95 10 80       	push   $0x801095db
80104747:	e8 7a bc ff ff       	call   801003c6 <cprintf>
8010474c:	83 c4 10             	add    $0x10,%esp
  if(head->next != 0); 
    cprintf(" -> ");
8010474f:	83 ec 0c             	sub    $0xc,%esp
80104752:	68 de 95 10 80       	push   $0x801095de
80104757:	e8 6a bc ff ff       	call   801003c6 <cprintf>
8010475c:	83 c4 10             	add    $0x10,%esp
  if(head->next == 0)
8010475f:	8b 45 08             	mov    0x8(%ebp),%eax
80104762:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
80104768:	85 c0                	test   %eax,%eax
8010476a:	75 10                	jne    8010477c <readysleep+0x50>
    cprintf("\n");
8010476c:	83 ec 0c             	sub    $0xc,%esp
8010476f:	68 e3 95 10 80       	push   $0x801095e3
80104774:	e8 4d bc ff ff       	call   801003c6 <cprintf>
80104779:	83 c4 10             	add    $0x10,%esp
  readysleep(head->next);
8010477c:	8b 45 08             	mov    0x8(%ebp),%eax
8010477f:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
80104785:	83 ec 0c             	sub    $0xc,%esp
80104788:	50                   	push   %eax
80104789:	e8 9e ff ff ff       	call   8010472c <readysleep>
8010478e:	83 c4 10             	add    $0x10,%esp
80104791:	eb 01                	jmp    80104794 <readysleep+0x68>
  return count;
}
void
readysleep(struct proc * head){
  if(head == 0)
    return;
80104793:	90                   	nop
  if(head->next != 0); 
    cprintf(" -> ");
  if(head->next == 0)
    cprintf("\n");
  readysleep(head->next);
}
80104794:	c9                   	leave  
80104795:	c3                   	ret    

80104796 <zombie>:
void
zombie(struct proc * head){
80104796:	55                   	push   %ebp
80104797:	89 e5                	mov    %esp,%ebp
80104799:	83 ec 18             	sub    $0x18,%esp
  if(head == 0)
8010479c:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
801047a0:	74 7e                	je     80104820 <zombie+0x8a>
    return;
  int ppid;
  if(head->pid == 1)
801047a2:	8b 45 08             	mov    0x8(%ebp),%eax
801047a5:	8b 40 10             	mov    0x10(%eax),%eax
801047a8:	83 f8 01             	cmp    $0x1,%eax
801047ab:	75 09                	jne    801047b6 <zombie+0x20>
    ppid = 1;
801047ad:	c7 45 f4 01 00 00 00 	movl   $0x1,-0xc(%ebp)
801047b4:	eb 0c                	jmp    801047c2 <zombie+0x2c>
  else
    ppid = head->parent->pid;
801047b6:	8b 45 08             	mov    0x8(%ebp),%eax
801047b9:	8b 40 14             	mov    0x14(%eax),%eax
801047bc:	8b 40 10             	mov    0x10(%eax),%eax
801047bf:	89 45 f4             	mov    %eax,-0xc(%ebp)
  cprintf("(%d, PPID%d)", head->pid, ppid);
801047c2:	8b 45 08             	mov    0x8(%ebp),%eax
801047c5:	8b 40 10             	mov    0x10(%eax),%eax
801047c8:	83 ec 04             	sub    $0x4,%esp
801047cb:	ff 75 f4             	pushl  -0xc(%ebp)
801047ce:	50                   	push   %eax
801047cf:	68 e5 95 10 80       	push   $0x801095e5
801047d4:	e8 ed bb ff ff       	call   801003c6 <cprintf>
801047d9:	83 c4 10             	add    $0x10,%esp
  if(head->next != 0); 
    cprintf(" -> ");
801047dc:	83 ec 0c             	sub    $0xc,%esp
801047df:	68 de 95 10 80       	push   $0x801095de
801047e4:	e8 dd bb ff ff       	call   801003c6 <cprintf>
801047e9:	83 c4 10             	add    $0x10,%esp
  if(head->next == 0)
801047ec:	8b 45 08             	mov    0x8(%ebp),%eax
801047ef:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
801047f5:	85 c0                	test   %eax,%eax
801047f7:	75 10                	jne    80104809 <zombie+0x73>
    cprintf("\n");
801047f9:	83 ec 0c             	sub    $0xc,%esp
801047fc:	68 e3 95 10 80       	push   $0x801095e3
80104801:	e8 c0 bb ff ff       	call   801003c6 <cprintf>
80104806:	83 c4 10             	add    $0x10,%esp
  zombie(head->next);
80104809:	8b 45 08             	mov    0x8(%ebp),%eax
8010480c:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
80104812:	83 ec 0c             	sub    $0xc,%esp
80104815:	50                   	push   %eax
80104816:	e8 7b ff ff ff       	call   80104796 <zombie>
8010481b:	83 c4 10             	add    $0x10,%esp
8010481e:	eb 01                	jmp    80104821 <zombie+0x8b>
  readysleep(head->next);
}
void
zombie(struct proc * head){
  if(head == 0)
    return;
80104820:	90                   	nop
  if(head->next != 0); 
    cprintf(" -> ");
  if(head->next == 0)
    cprintf("\n");
  zombie(head->next);
}
80104821:	c9                   	leave  
80104822:	c3                   	ret    

80104823 <pinit>:
//****************************************************************************************************************************************
void
pinit(void)
{
80104823:	55                   	push   %ebp
80104824:	89 e5                	mov    %esp,%ebp
80104826:	83 ec 08             	sub    $0x8,%esp
  initlock(&ptable.lock, "ptable");
80104829:	83 ec 08             	sub    $0x8,%esp
8010482c:	68 f2 95 10 80       	push   $0x801095f2
80104831:	68 80 39 11 80       	push   $0x80113980
80104836:	e8 eb 12 00 00       	call   80105b26 <initlock>
8010483b:	83 c4 10             	add    $0x10,%esp
}
8010483e:	90                   	nop
8010483f:	c9                   	leave  
80104840:	c3                   	ret    

80104841 <allocproc>:
// If found, change state to EMBRYO and initialize
// state required to run in the kernel.
// Otherwise return 0.
static struct proc*
allocproc(void)										//***************************************  ALLOCPROC  *******
{
80104841:	55                   	push   %ebp
80104842:	89 e5                	mov    %esp,%ebp
80104844:	83 ec 18             	sub    $0x18,%esp
  struct proc *p;
  char *sp;

  acquire(&ptable.lock);
80104847:	83 ec 0c             	sub    $0xc,%esp
8010484a:	68 80 39 11 80       	push   $0x80113980
8010484f:	e8 f4 12 00 00       	call   80105b48 <acquire>
80104854:	83 c4 10             	add    $0x10,%esp
    if(p->state == UNUSED){
      goto found;
    }
  }
#else
  p = ptable.pLists.free;
80104857:	a1 b8 5f 11 80       	mov    0x80115fb8,%eax
8010485c:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(p){
8010485f:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80104863:	74 78                	je     801048dd <allocproc+0x9c>
    ptable.pLists.free = p->next;
80104865:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104868:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
8010486e:	a3 b8 5f 11 80       	mov    %eax,0x80115fb8
    p->next = 0;
80104873:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104876:	c7 80 90 00 00 00 00 	movl   $0x0,0x90(%eax)
8010487d:	00 00 00 
    goto found;
80104880:	90                   	nop
  return 0;

found:
#ifdef CS333_P3P4
  //fromToStateList(p, UNUSED, EMBRYO, &ptable.pLists.free, &ptable.pLists.embryo);  
  p->state = EMBRYO;
80104881:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104884:	c7 40 0c 01 00 00 00 	movl   $0x1,0xc(%eax)
  addToStateList(&ptable.pLists.embryo, p);
8010488b:	83 ec 08             	sub    $0x8,%esp
8010488e:	ff 75 f4             	pushl  -0xc(%ebp)
80104891:	68 c8 5f 11 80       	push   $0x80115fc8
80104896:	e8 5f fc ff ff       	call   801044fa <addToStateList>
8010489b:	83 c4 10             	add    $0x10,%esp
#else
  p->state = EMBRYO;
#endif
  p->pid = nextpid++;
8010489e:	a1 04 c0 10 80       	mov    0x8010c004,%eax
801048a3:	8d 50 01             	lea    0x1(%eax),%edx
801048a6:	89 15 04 c0 10 80    	mov    %edx,0x8010c004
801048ac:	89 c2                	mov    %eax,%edx
801048ae:	8b 45 f4             	mov    -0xc(%ebp),%eax
801048b1:	89 50 10             	mov    %edx,0x10(%eax)
  release(&ptable.lock);
801048b4:	83 ec 0c             	sub    $0xc,%esp
801048b7:	68 80 39 11 80       	push   $0x80113980
801048bc:	e8 ee 12 00 00       	call   80105baf <release>
801048c1:	83 c4 10             	add    $0x10,%esp

  // Allocate kernel stack.
  if((p->kstack = kalloc()) == 0){
801048c4:	e8 3b e4 ff ff       	call   80102d04 <kalloc>
801048c9:	89 c2                	mov    %eax,%edx
801048cb:	8b 45 f4             	mov    -0xc(%ebp),%eax
801048ce:	89 50 08             	mov    %edx,0x8(%eax)
801048d1:	8b 45 f4             	mov    -0xc(%ebp),%eax
801048d4:	8b 40 08             	mov    0x8(%eax),%eax
801048d7:	85 c0                	test   %eax,%eax
801048d9:	75 62                	jne    8010493d <allocproc+0xfc>
801048db:	eb 1a                	jmp    801048f7 <allocproc+0xb6>
    ptable.pLists.free = p->next;
    p->next = 0;
    goto found;
  }
#endif
  release(&ptable.lock);
801048dd:	83 ec 0c             	sub    $0xc,%esp
801048e0:	68 80 39 11 80       	push   $0x80113980
801048e5:	e8 c5 12 00 00       	call   80105baf <release>
801048ea:	83 c4 10             	add    $0x10,%esp
  return 0;
801048ed:	b8 00 00 00 00       	mov    $0x0,%eax
801048f2:	e9 c9 00 00 00       	jmp    801049c0 <allocproc+0x17f>
  release(&ptable.lock);

  // Allocate kernel stack.
  if((p->kstack = kalloc()) == 0){
#ifdef CS333_P3P4
    acquire(&ptable.lock);
801048f7:	83 ec 0c             	sub    $0xc,%esp
801048fa:	68 80 39 11 80       	push   $0x80113980
801048ff:	e8 44 12 00 00       	call   80105b48 <acquire>
80104904:	83 c4 10             	add    $0x10,%esp
    fromToStateList(p, EMBRYO, UNUSED, &ptable.pLists.embryo, &ptable.pLists.free);
80104907:	83 ec 0c             	sub    $0xc,%esp
8010490a:	68 b8 5f 11 80       	push   $0x80115fb8
8010490f:	68 c8 5f 11 80       	push   $0x80115fc8
80104914:	6a 00                	push   $0x0
80104916:	6a 01                	push   $0x1
80104918:	ff 75 f4             	pushl  -0xc(%ebp)
8010491b:	e8 50 fd ff ff       	call   80104670 <fromToStateList>
80104920:	83 c4 20             	add    $0x20,%esp
    release(&ptable.lock);
80104923:	83 ec 0c             	sub    $0xc,%esp
80104926:	68 80 39 11 80       	push   $0x80113980
8010492b:	e8 7f 12 00 00       	call   80105baf <release>
80104930:	83 c4 10             	add    $0x10,%esp
    //p->state = UNUSED;
#else
    p->state = UNUSED;
#endif
    return 0;
80104933:	b8 00 00 00 00       	mov    $0x0,%eax
80104938:	e9 83 00 00 00       	jmp    801049c0 <allocproc+0x17f>
  }
  sp = p->kstack + KSTACKSIZE;
8010493d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104940:	8b 40 08             	mov    0x8(%eax),%eax
80104943:	05 00 10 00 00       	add    $0x1000,%eax
80104948:	89 45 f0             	mov    %eax,-0x10(%ebp)
  
  // Leave room for trap frame.
  sp -= sizeof *p->tf;
8010494b:	83 6d f0 4c          	subl   $0x4c,-0x10(%ebp)
  p->tf = (struct trapframe*)sp;
8010494f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104952:	8b 55 f0             	mov    -0x10(%ebp),%edx
80104955:	89 50 18             	mov    %edx,0x18(%eax)
  
  // Set up new context to start executing at forkret,
  // which returns to trapret.
  sp -= 4;
80104958:	83 6d f0 04          	subl   $0x4,-0x10(%ebp)
  *(uint*)sp = (uint)trapret;
8010495c:	ba 43 73 10 80       	mov    $0x80107343,%edx
80104961:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104964:	89 10                	mov    %edx,(%eax)

  sp -= sizeof *p->context;
80104966:	83 6d f0 14          	subl   $0x14,-0x10(%ebp)
  p->context = (struct context*)sp;
8010496a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010496d:	8b 55 f0             	mov    -0x10(%ebp),%edx
80104970:	89 50 1c             	mov    %edx,0x1c(%eax)
  memset(p->context, 0, sizeof *p->context);
80104973:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104976:	8b 40 1c             	mov    0x1c(%eax),%eax
80104979:	83 ec 04             	sub    $0x4,%esp
8010497c:	6a 14                	push   $0x14
8010497e:	6a 00                	push   $0x0
80104980:	50                   	push   %eax
80104981:	e8 25 14 00 00       	call   80105dab <memset>
80104986:	83 c4 10             	add    $0x10,%esp
  p->context->eip = (uint)forkret;
80104989:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010498c:	8b 40 1c             	mov    0x1c(%eax),%eax
8010498f:	ba 1e 53 10 80       	mov    $0x8010531e,%edx
80104994:	89 50 10             	mov    %edx,0x10(%eax)

  p->start_ticks = ticks;	//initialization of start_ticks to global kernel variable
80104997:	8b 15 e0 67 11 80    	mov    0x801167e0,%edx
8010499d:	8b 45 f4             	mov    -0xc(%ebp),%eax
801049a0:	89 50 7c             	mov    %edx,0x7c(%eax)
  p->cpu_ticks_total = 0;
801049a3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801049a6:	c7 80 88 00 00 00 00 	movl   $0x0,0x88(%eax)
801049ad:	00 00 00 
  p->cpu_ticks_in = 0;
801049b0:	8b 45 f4             	mov    -0xc(%ebp),%eax
801049b3:	c7 80 8c 00 00 00 00 	movl   $0x0,0x8c(%eax)
801049ba:	00 00 00 

  return p;
801049bd:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
801049c0:	c9                   	leave  
801049c1:	c3                   	ret    

801049c2 <userinit>:

// Set up first user process.
void											//***************************************  USERINIT  *******
userinit(void)
{
801049c2:	55                   	push   %ebp
801049c3:	89 e5                	mov    %esp,%ebp
801049c5:	83 ec 18             	sub    $0x18,%esp
  struct proc *p;
  extern char _binary_initcode_start[], _binary_initcode_size[];

#ifdef CS333_P3P4
struct proc *tmp;
acquire(&ptable.lock);
801049c8:	83 ec 0c             	sub    $0xc,%esp
801049cb:	68 80 39 11 80       	push   $0x80113980
801049d0:	e8 73 11 00 00       	call   80105b48 <acquire>
801049d5:	83 c4 10             	add    $0x10,%esp
ptable.pLists.free = 0;
801049d8:	c7 05 b8 5f 11 80 00 	movl   $0x0,0x80115fb8
801049df:	00 00 00 
ptable.pLists.ready = 0;
801049e2:	c7 05 b4 5f 11 80 00 	movl   $0x0,0x80115fb4
801049e9:	00 00 00 
ptable.pLists.sleep = 0;
801049ec:	c7 05 bc 5f 11 80 00 	movl   $0x0,0x80115fbc
801049f3:	00 00 00 
ptable.pLists.embryo = 0;
801049f6:	c7 05 c8 5f 11 80 00 	movl   $0x0,0x80115fc8
801049fd:	00 00 00 
ptable.pLists.running = 0;
80104a00:	c7 05 c4 5f 11 80 00 	movl   $0x0,0x80115fc4
80104a07:	00 00 00 
ptable.pLists.zombie = 0;
80104a0a:	c7 05 c0 5f 11 80 00 	movl   $0x0,0x80115fc0
80104a11:	00 00 00 
for(tmp = ptable.proc; tmp < &ptable.proc[NPROC]; ++tmp)
80104a14:	c7 45 f4 b4 39 11 80 	movl   $0x801139b4,-0xc(%ebp)
80104a1b:	eb 1a                	jmp    80104a37 <userinit+0x75>
  addToStateList(&ptable.pLists.free, tmp);
80104a1d:	83 ec 08             	sub    $0x8,%esp
80104a20:	ff 75 f4             	pushl  -0xc(%ebp)
80104a23:	68 b8 5f 11 80       	push   $0x80115fb8
80104a28:	e8 cd fa ff ff       	call   801044fa <addToStateList>
80104a2d:	83 c4 10             	add    $0x10,%esp
ptable.pLists.ready = 0;
ptable.pLists.sleep = 0;
ptable.pLists.embryo = 0;
ptable.pLists.running = 0;
ptable.pLists.zombie = 0;
for(tmp = ptable.proc; tmp < &ptable.proc[NPROC]; ++tmp)
80104a30:	81 45 f4 98 00 00 00 	addl   $0x98,-0xc(%ebp)
80104a37:	81 7d f4 b4 5f 11 80 	cmpl   $0x80115fb4,-0xc(%ebp)
80104a3e:	72 dd                	jb     80104a1d <userinit+0x5b>
  addToStateList(&ptable.pLists.free, tmp);
release(&ptable.lock);
80104a40:	83 ec 0c             	sub    $0xc,%esp
80104a43:	68 80 39 11 80       	push   $0x80113980
80104a48:	e8 62 11 00 00       	call   80105baf <release>
80104a4d:	83 c4 10             	add    $0x10,%esp
#endif 
  p = allocproc();
80104a50:	e8 ec fd ff ff       	call   80104841 <allocproc>
80104a55:	89 45 f0             	mov    %eax,-0x10(%ebp)
  initproc = p;
80104a58:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104a5b:	a3 68 c6 10 80       	mov    %eax,0x8010c668
  if((p->pgdir = setupkvm()) == 0)
80104a60:	e8 7d 3f 00 00       	call   801089e2 <setupkvm>
80104a65:	89 c2                	mov    %eax,%edx
80104a67:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104a6a:	89 50 04             	mov    %edx,0x4(%eax)
80104a6d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104a70:	8b 40 04             	mov    0x4(%eax),%eax
80104a73:	85 c0                	test   %eax,%eax
80104a75:	75 0d                	jne    80104a84 <userinit+0xc2>
    panic("userinit: out of memory?");
80104a77:	83 ec 0c             	sub    $0xc,%esp
80104a7a:	68 f9 95 10 80       	push   $0x801095f9
80104a7f:	e8 e2 ba ff ff       	call   80100566 <panic>
  inituvm(p->pgdir, _binary_initcode_start, (int)_binary_initcode_size);
80104a84:	ba 2c 00 00 00       	mov    $0x2c,%edx
80104a89:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104a8c:	8b 40 04             	mov    0x4(%eax),%eax
80104a8f:	83 ec 04             	sub    $0x4,%esp
80104a92:	52                   	push   %edx
80104a93:	68 00 c5 10 80       	push   $0x8010c500
80104a98:	50                   	push   %eax
80104a99:	e8 9e 41 00 00       	call   80108c3c <inituvm>
80104a9e:	83 c4 10             	add    $0x10,%esp
  p->sz = PGSIZE;
80104aa1:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104aa4:	c7 00 00 10 00 00    	movl   $0x1000,(%eax)
  memset(p->tf, 0, sizeof(*p->tf));
80104aaa:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104aad:	8b 40 18             	mov    0x18(%eax),%eax
80104ab0:	83 ec 04             	sub    $0x4,%esp
80104ab3:	6a 4c                	push   $0x4c
80104ab5:	6a 00                	push   $0x0
80104ab7:	50                   	push   %eax
80104ab8:	e8 ee 12 00 00       	call   80105dab <memset>
80104abd:	83 c4 10             	add    $0x10,%esp
  p->tf->cs = (SEG_UCODE << 3) | DPL_USER;
80104ac0:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104ac3:	8b 40 18             	mov    0x18(%eax),%eax
80104ac6:	66 c7 40 3c 23 00    	movw   $0x23,0x3c(%eax)
  p->tf->ds = (SEG_UDATA << 3) | DPL_USER;
80104acc:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104acf:	8b 40 18             	mov    0x18(%eax),%eax
80104ad2:	66 c7 40 2c 2b 00    	movw   $0x2b,0x2c(%eax)
  p->tf->es = p->tf->ds;
80104ad8:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104adb:	8b 40 18             	mov    0x18(%eax),%eax
80104ade:	8b 55 f0             	mov    -0x10(%ebp),%edx
80104ae1:	8b 52 18             	mov    0x18(%edx),%edx
80104ae4:	0f b7 52 2c          	movzwl 0x2c(%edx),%edx
80104ae8:	66 89 50 28          	mov    %dx,0x28(%eax)
  p->tf->ss = p->tf->ds;
80104aec:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104aef:	8b 40 18             	mov    0x18(%eax),%eax
80104af2:	8b 55 f0             	mov    -0x10(%ebp),%edx
80104af5:	8b 52 18             	mov    0x18(%edx),%edx
80104af8:	0f b7 52 2c          	movzwl 0x2c(%edx),%edx
80104afc:	66 89 50 48          	mov    %dx,0x48(%eax)
  p->tf->eflags = FL_IF;
80104b00:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104b03:	8b 40 18             	mov    0x18(%eax),%eax
80104b06:	c7 40 40 00 02 00 00 	movl   $0x200,0x40(%eax)
  p->tf->esp = PGSIZE;
80104b0d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104b10:	8b 40 18             	mov    0x18(%eax),%eax
80104b13:	c7 40 44 00 10 00 00 	movl   $0x1000,0x44(%eax)
  p->tf->eip = 0;  // beginning of initcode.S
80104b1a:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104b1d:	8b 40 18             	mov    0x18(%eax),%eax
80104b20:	c7 40 38 00 00 00 00 	movl   $0x0,0x38(%eax)

  safestrcpy(p->name, "initcode", sizeof(p->name));
80104b27:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104b2a:	83 c0 6c             	add    $0x6c,%eax
80104b2d:	83 ec 04             	sub    $0x4,%esp
80104b30:	6a 10                	push   $0x10
80104b32:	68 12 96 10 80       	push   $0x80109612
80104b37:	50                   	push   %eax
80104b38:	e8 71 14 00 00       	call   80105fae <safestrcpy>
80104b3d:	83 c4 10             	add    $0x10,%esp
  p->cwd = namei("/");
80104b40:	83 ec 0c             	sub    $0xc,%esp
80104b43:	68 1b 96 10 80       	push   $0x8010961b
80104b48:	e8 79 da ff ff       	call   801025c6 <namei>
80104b4d:	83 c4 10             	add    $0x10,%esp
80104b50:	89 c2                	mov    %eax,%edx
80104b52:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104b55:	89 50 68             	mov    %edx,0x68(%eax)
#ifdef CS333_P3P4
  acquire(&ptable.lock);
80104b58:	83 ec 0c             	sub    $0xc,%esp
80104b5b:	68 80 39 11 80       	push   $0x80113980
80104b60:	e8 e3 0f 00 00       	call   80105b48 <acquire>
80104b65:	83 c4 10             	add    $0x10,%esp
  fromToStateList(p, EMBRYO, RUNNABLE, &ptable.pLists.embryo, &ptable.pLists.ready);
80104b68:	83 ec 0c             	sub    $0xc,%esp
80104b6b:	68 b4 5f 11 80       	push   $0x80115fb4
80104b70:	68 c8 5f 11 80       	push   $0x80115fc8
80104b75:	6a 03                	push   $0x3
80104b77:	6a 01                	push   $0x1
80104b79:	ff 75 f0             	pushl  -0x10(%ebp)
80104b7c:	e8 ef fa ff ff       	call   80104670 <fromToStateList>
80104b81:	83 c4 20             	add    $0x20,%esp
  release(&ptable.lock);
80104b84:	83 ec 0c             	sub    $0xc,%esp
80104b87:	68 80 39 11 80       	push   $0x80113980
80104b8c:	e8 1e 10 00 00       	call   80105baf <release>
80104b91:	83 c4 10             	add    $0x10,%esp
  //p->state = RUNNABLE;
  ptable.pLists.embryo = 0;
80104b94:	c7 05 c8 5f 11 80 00 	movl   $0x0,0x80115fc8
80104b9b:	00 00 00 
#else
  p->state = RUNNABLE;
#endif
  p->next = 0;
80104b9e:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104ba1:	c7 80 90 00 00 00 00 	movl   $0x0,0x90(%eax)
80104ba8:	00 00 00 
  p->gid = NGID;
80104bab:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104bae:	c7 80 80 00 00 00 15 	movl   $0x15,0x80(%eax)
80104bb5:	00 00 00 
  p->uid = NUID;
80104bb8:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104bbb:	c7 80 84 00 00 00 15 	movl   $0x15,0x84(%eax)
80104bc2:	00 00 00 
}
80104bc5:	90                   	nop
80104bc6:	c9                   	leave  
80104bc7:	c3                   	ret    

80104bc8 <growproc>:

// Grow current process's memory by n bytes.
// Return 0 on success, -1 on failure.
int
growproc(int n)
{
80104bc8:	55                   	push   %ebp
80104bc9:	89 e5                	mov    %esp,%ebp
80104bcb:	83 ec 18             	sub    $0x18,%esp
  uint sz;
  
  sz = proc->sz;
80104bce:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104bd4:	8b 00                	mov    (%eax),%eax
80104bd6:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(n > 0){
80104bd9:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80104bdd:	7e 31                	jle    80104c10 <growproc+0x48>
    if((sz = allocuvm(proc->pgdir, sz, sz + n)) == 0)
80104bdf:	8b 55 08             	mov    0x8(%ebp),%edx
80104be2:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104be5:	01 c2                	add    %eax,%edx
80104be7:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104bed:	8b 40 04             	mov    0x4(%eax),%eax
80104bf0:	83 ec 04             	sub    $0x4,%esp
80104bf3:	52                   	push   %edx
80104bf4:	ff 75 f4             	pushl  -0xc(%ebp)
80104bf7:	50                   	push   %eax
80104bf8:	e8 8c 41 00 00       	call   80108d89 <allocuvm>
80104bfd:	83 c4 10             	add    $0x10,%esp
80104c00:	89 45 f4             	mov    %eax,-0xc(%ebp)
80104c03:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80104c07:	75 3e                	jne    80104c47 <growproc+0x7f>
      return -1;
80104c09:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104c0e:	eb 59                	jmp    80104c69 <growproc+0xa1>
  } else if(n < 0){
80104c10:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80104c14:	79 31                	jns    80104c47 <growproc+0x7f>
    if((sz = deallocuvm(proc->pgdir, sz, sz + n)) == 0)
80104c16:	8b 55 08             	mov    0x8(%ebp),%edx
80104c19:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104c1c:	01 c2                	add    %eax,%edx
80104c1e:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104c24:	8b 40 04             	mov    0x4(%eax),%eax
80104c27:	83 ec 04             	sub    $0x4,%esp
80104c2a:	52                   	push   %edx
80104c2b:	ff 75 f4             	pushl  -0xc(%ebp)
80104c2e:	50                   	push   %eax
80104c2f:	e8 1e 42 00 00       	call   80108e52 <deallocuvm>
80104c34:	83 c4 10             	add    $0x10,%esp
80104c37:	89 45 f4             	mov    %eax,-0xc(%ebp)
80104c3a:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80104c3e:	75 07                	jne    80104c47 <growproc+0x7f>
      return -1;
80104c40:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104c45:	eb 22                	jmp    80104c69 <growproc+0xa1>
  }
  proc->sz = sz;
80104c47:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104c4d:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104c50:	89 10                	mov    %edx,(%eax)
  switchuvm(proc);
80104c52:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104c58:	83 ec 0c             	sub    $0xc,%esp
80104c5b:	50                   	push   %eax
80104c5c:	e8 68 3e 00 00       	call   80108ac9 <switchuvm>
80104c61:	83 c4 10             	add    $0x10,%esp
  return 0;
80104c64:	b8 00 00 00 00       	mov    $0x0,%eax
}
80104c69:	c9                   	leave  
80104c6a:	c3                   	ret    

80104c6b <fork>:
// Create a new process copying p as the parent.
// Sets up stack to return as if from system call.
// Caller must set state of returned proc to RUNNABLE.
int										//***************************************  FORK  *******
fork(void)
{
80104c6b:	55                   	push   %ebp
80104c6c:	89 e5                	mov    %esp,%ebp
80104c6e:	57                   	push   %edi
80104c6f:	56                   	push   %esi
80104c70:	53                   	push   %ebx
80104c71:	83 ec 1c             	sub    $0x1c,%esp
  int i, pid;
  struct proc *np;

  // Allocate process.
  if((np = allocproc()) == 0)
80104c74:	e8 c8 fb ff ff       	call   80104841 <allocproc>
80104c79:	89 45 e0             	mov    %eax,-0x20(%ebp)
80104c7c:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
80104c80:	75 0a                	jne    80104c8c <fork+0x21>
    return -1;
80104c82:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104c87:	e9 d6 01 00 00       	jmp    80104e62 <fork+0x1f7>

  // Copy process state from p.
  if((np->pgdir = copyuvm(proc->pgdir, proc->sz)) == 0){
80104c8c:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104c92:	8b 10                	mov    (%eax),%edx
80104c94:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104c9a:	8b 40 04             	mov    0x4(%eax),%eax
80104c9d:	83 ec 08             	sub    $0x8,%esp
80104ca0:	52                   	push   %edx
80104ca1:	50                   	push   %eax
80104ca2:	e8 49 43 00 00       	call   80108ff0 <copyuvm>
80104ca7:	83 c4 10             	add    $0x10,%esp
80104caa:	89 c2                	mov    %eax,%edx
80104cac:	8b 45 e0             	mov    -0x20(%ebp),%eax
80104caf:	89 50 04             	mov    %edx,0x4(%eax)
80104cb2:	8b 45 e0             	mov    -0x20(%ebp),%eax
80104cb5:	8b 40 04             	mov    0x4(%eax),%eax
80104cb8:	85 c0                	test   %eax,%eax
80104cba:	75 62                	jne    80104d1e <fork+0xb3>
    kfree(np->kstack);
80104cbc:	8b 45 e0             	mov    -0x20(%ebp),%eax
80104cbf:	8b 40 08             	mov    0x8(%eax),%eax
80104cc2:	83 ec 0c             	sub    $0xc,%esp
80104cc5:	50                   	push   %eax
80104cc6:	e8 9c df ff ff       	call   80102c67 <kfree>
80104ccb:	83 c4 10             	add    $0x10,%esp
    np->kstack = 0;
80104cce:	8b 45 e0             	mov    -0x20(%ebp),%eax
80104cd1:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
#ifdef CS333_P3P4
    acquire(&ptable.lock);
80104cd8:	83 ec 0c             	sub    $0xc,%esp
80104cdb:	68 80 39 11 80       	push   $0x80113980
80104ce0:	e8 63 0e 00 00       	call   80105b48 <acquire>
80104ce5:	83 c4 10             	add    $0x10,%esp
    fromToStateList(np, EMBRYO, UNUSED, &ptable.pLists.embryo, &ptable.pLists.free);
80104ce8:	83 ec 0c             	sub    $0xc,%esp
80104ceb:	68 b8 5f 11 80       	push   $0x80115fb8
80104cf0:	68 c8 5f 11 80       	push   $0x80115fc8
80104cf5:	6a 00                	push   $0x0
80104cf7:	6a 01                	push   $0x1
80104cf9:	ff 75 e0             	pushl  -0x20(%ebp)
80104cfc:	e8 6f f9 ff ff       	call   80104670 <fromToStateList>
80104d01:	83 c4 20             	add    $0x20,%esp
    release(&ptable.lock);
80104d04:	83 ec 0c             	sub    $0xc,%esp
80104d07:	68 80 39 11 80       	push   $0x80113980
80104d0c:	e8 9e 0e 00 00       	call   80105baf <release>
80104d11:	83 c4 10             	add    $0x10,%esp
    //np->state = UNUSED;
#else
    np->state = UNUSED;
#endif
    return -1;
80104d14:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104d19:	e9 44 01 00 00       	jmp    80104e62 <fork+0x1f7>
  }
  np->sz = proc->sz;
80104d1e:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104d24:	8b 10                	mov    (%eax),%edx
80104d26:	8b 45 e0             	mov    -0x20(%ebp),%eax
80104d29:	89 10                	mov    %edx,(%eax)
  np->parent = proc;
80104d2b:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
80104d32:	8b 45 e0             	mov    -0x20(%ebp),%eax
80104d35:	89 50 14             	mov    %edx,0x14(%eax)
  *np->tf = *proc->tf;
80104d38:	8b 45 e0             	mov    -0x20(%ebp),%eax
80104d3b:	8b 50 18             	mov    0x18(%eax),%edx
80104d3e:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104d44:	8b 40 18             	mov    0x18(%eax),%eax
80104d47:	89 c3                	mov    %eax,%ebx
80104d49:	b8 13 00 00 00       	mov    $0x13,%eax
80104d4e:	89 d7                	mov    %edx,%edi
80104d50:	89 de                	mov    %ebx,%esi
80104d52:	89 c1                	mov    %eax,%ecx
80104d54:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  np->uid = proc->uid;		//copy uid & gid to child
80104d56:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104d5c:	8b 90 84 00 00 00    	mov    0x84(%eax),%edx
80104d62:	8b 45 e0             	mov    -0x20(%ebp),%eax
80104d65:	89 90 84 00 00 00    	mov    %edx,0x84(%eax)
  np->gid = proc->gid;
80104d6b:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104d71:	8b 90 80 00 00 00    	mov    0x80(%eax),%edx
80104d77:	8b 45 e0             	mov    -0x20(%ebp),%eax
80104d7a:	89 90 80 00 00 00    	mov    %edx,0x80(%eax)

  // Clear %eax so that fork returns 0 in the child.
  np->tf->eax = 0;
80104d80:	8b 45 e0             	mov    -0x20(%ebp),%eax
80104d83:	8b 40 18             	mov    0x18(%eax),%eax
80104d86:	c7 40 1c 00 00 00 00 	movl   $0x0,0x1c(%eax)

  for(i = 0; i < NOFILE; i++)
80104d8d:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
80104d94:	eb 43                	jmp    80104dd9 <fork+0x16e>
    if(proc->ofile[i])
80104d96:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104d9c:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80104d9f:	83 c2 08             	add    $0x8,%edx
80104da2:	8b 44 90 08          	mov    0x8(%eax,%edx,4),%eax
80104da6:	85 c0                	test   %eax,%eax
80104da8:	74 2b                	je     80104dd5 <fork+0x16a>
      np->ofile[i] = filedup(proc->ofile[i]);
80104daa:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104db0:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80104db3:	83 c2 08             	add    $0x8,%edx
80104db6:	8b 44 90 08          	mov    0x8(%eax,%edx,4),%eax
80104dba:	83 ec 0c             	sub    $0xc,%esp
80104dbd:	50                   	push   %eax
80104dbe:	e8 db c2 ff ff       	call   8010109e <filedup>
80104dc3:	83 c4 10             	add    $0x10,%esp
80104dc6:	89 c1                	mov    %eax,%ecx
80104dc8:	8b 45 e0             	mov    -0x20(%ebp),%eax
80104dcb:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80104dce:	83 c2 08             	add    $0x8,%edx
80104dd1:	89 4c 90 08          	mov    %ecx,0x8(%eax,%edx,4)
  np->gid = proc->gid;

  // Clear %eax so that fork returns 0 in the child.
  np->tf->eax = 0;

  for(i = 0; i < NOFILE; i++)
80104dd5:	83 45 e4 01          	addl   $0x1,-0x1c(%ebp)
80104dd9:	83 7d e4 0f          	cmpl   $0xf,-0x1c(%ebp)
80104ddd:	7e b7                	jle    80104d96 <fork+0x12b>
    if(proc->ofile[i])
      np->ofile[i] = filedup(proc->ofile[i]);
  np->cwd = idup(proc->cwd);
80104ddf:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104de5:	8b 40 68             	mov    0x68(%eax),%eax
80104de8:	83 ec 0c             	sub    $0xc,%esp
80104deb:	50                   	push   %eax
80104dec:	e8 dd cb ff ff       	call   801019ce <idup>
80104df1:	83 c4 10             	add    $0x10,%esp
80104df4:	89 c2                	mov    %eax,%edx
80104df6:	8b 45 e0             	mov    -0x20(%ebp),%eax
80104df9:	89 50 68             	mov    %edx,0x68(%eax)

  safestrcpy(np->name, proc->name, sizeof(proc->name));
80104dfc:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104e02:	8d 50 6c             	lea    0x6c(%eax),%edx
80104e05:	8b 45 e0             	mov    -0x20(%ebp),%eax
80104e08:	83 c0 6c             	add    $0x6c,%eax
80104e0b:	83 ec 04             	sub    $0x4,%esp
80104e0e:	6a 10                	push   $0x10
80104e10:	52                   	push   %edx
80104e11:	50                   	push   %eax
80104e12:	e8 97 11 00 00       	call   80105fae <safestrcpy>
80104e17:	83 c4 10             	add    $0x10,%esp
 
  pid = np->pid;
80104e1a:	8b 45 e0             	mov    -0x20(%ebp),%eax
80104e1d:	8b 40 10             	mov    0x10(%eax),%eax
80104e20:	89 45 dc             	mov    %eax,-0x24(%ebp)

  // lock to force the compiler to emit the np->state write last.
  acquire(&ptable.lock);
80104e23:	83 ec 0c             	sub    $0xc,%esp
80104e26:	68 80 39 11 80       	push   $0x80113980
80104e2b:	e8 18 0d 00 00       	call   80105b48 <acquire>
80104e30:	83 c4 10             	add    $0x10,%esp
#ifdef CS333_P3P4
  fromToStateList(np, EMBRYO, RUNNABLE, &ptable.pLists.embryo, &ptable.pLists.ready);
80104e33:	83 ec 0c             	sub    $0xc,%esp
80104e36:	68 b4 5f 11 80       	push   $0x80115fb4
80104e3b:	68 c8 5f 11 80       	push   $0x80115fc8
80104e40:	6a 03                	push   $0x3
80104e42:	6a 01                	push   $0x1
80104e44:	ff 75 e0             	pushl  -0x20(%ebp)
80104e47:	e8 24 f8 ff ff       	call   80104670 <fromToStateList>
80104e4c:	83 c4 20             	add    $0x20,%esp
#else
  np->state = RUNNABLE;
#endif
  release(&ptable.lock);
80104e4f:	83 ec 0c             	sub    $0xc,%esp
80104e52:	68 80 39 11 80       	push   $0x80113980
80104e57:	e8 53 0d 00 00       	call   80105baf <release>
80104e5c:	83 c4 10             	add    $0x10,%esp
  
  return pid;
80104e5f:	8b 45 dc             	mov    -0x24(%ebp),%eax
}
80104e62:	8d 65 f4             	lea    -0xc(%ebp),%esp
80104e65:	5b                   	pop    %ebx
80104e66:	5e                   	pop    %esi
80104e67:	5f                   	pop    %edi
80104e68:	5d                   	pop    %ebp
80104e69:	c3                   	ret    

80104e6a <exit>:
// An exited process remains in the zombie state
// until its parent calls wait() to find out it exited.
#ifndef CS333_P3P4_NO
void
exit(void)
{
80104e6a:	55                   	push   %ebp
80104e6b:	89 e5                	mov    %esp,%ebp
80104e6d:	83 ec 18             	sub    $0x18,%esp
  struct proc *p;
  int fd;

  if(proc == initproc)
80104e70:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
80104e77:	a1 68 c6 10 80       	mov    0x8010c668,%eax
80104e7c:	39 c2                	cmp    %eax,%edx
80104e7e:	75 0d                	jne    80104e8d <exit+0x23>
    panic("init exiting");
80104e80:	83 ec 0c             	sub    $0xc,%esp
80104e83:	68 1d 96 10 80       	push   $0x8010961d
80104e88:	e8 d9 b6 ff ff       	call   80100566 <panic>
  // Close all open files.
  for(fd = 0; fd < NOFILE; fd++){
80104e8d:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
80104e94:	eb 48                	jmp    80104ede <exit+0x74>
    if(proc->ofile[fd]){
80104e96:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104e9c:	8b 55 f0             	mov    -0x10(%ebp),%edx
80104e9f:	83 c2 08             	add    $0x8,%edx
80104ea2:	8b 44 90 08          	mov    0x8(%eax,%edx,4),%eax
80104ea6:	85 c0                	test   %eax,%eax
80104ea8:	74 30                	je     80104eda <exit+0x70>
      fileclose(proc->ofile[fd]);
80104eaa:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104eb0:	8b 55 f0             	mov    -0x10(%ebp),%edx
80104eb3:	83 c2 08             	add    $0x8,%edx
80104eb6:	8b 44 90 08          	mov    0x8(%eax,%edx,4),%eax
80104eba:	83 ec 0c             	sub    $0xc,%esp
80104ebd:	50                   	push   %eax
80104ebe:	e8 2c c2 ff ff       	call   801010ef <fileclose>
80104ec3:	83 c4 10             	add    $0x10,%esp
      proc->ofile[fd] = 0;
80104ec6:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104ecc:	8b 55 f0             	mov    -0x10(%ebp),%edx
80104ecf:	83 c2 08             	add    $0x8,%edx
80104ed2:	c7 44 90 08 00 00 00 	movl   $0x0,0x8(%eax,%edx,4)
80104ed9:	00 
  int fd;

  if(proc == initproc)
    panic("init exiting");
  // Close all open files.
  for(fd = 0; fd < NOFILE; fd++){
80104eda:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
80104ede:	83 7d f0 0f          	cmpl   $0xf,-0x10(%ebp)
80104ee2:	7e b2                	jle    80104e96 <exit+0x2c>
    if(proc->ofile[fd]){
      fileclose(proc->ofile[fd]);
      proc->ofile[fd] = 0;
    }
  }
  begin_op();
80104ee4:	e8 02 e7 ff ff       	call   801035eb <begin_op>
  iput(proc->cwd);
80104ee9:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104eef:	8b 40 68             	mov    0x68(%eax),%eax
80104ef2:	83 ec 0c             	sub    $0xc,%esp
80104ef5:	50                   	push   %eax
80104ef6:	e8 dd cc ff ff       	call   80101bd8 <iput>
80104efb:	83 c4 10             	add    $0x10,%esp
  end_op();
80104efe:	e8 74 e7 ff ff       	call   80103677 <end_op>
  proc->cwd = 0;
80104f03:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104f09:	c7 40 68 00 00 00 00 	movl   $0x0,0x68(%eax)
  acquire(&ptable.lock);
80104f10:	83 ec 0c             	sub    $0xc,%esp
80104f13:	68 80 39 11 80       	push   $0x80113980
80104f18:	e8 2b 0c 00 00       	call   80105b48 <acquire>
80104f1d:	83 c4 10             	add    $0x10,%esp
  // Parent might be sleeping in wait().
  wakeup1(proc->parent);
80104f20:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104f26:	8b 40 14             	mov    0x14(%eax),%eax
80104f29:	83 ec 0c             	sub    $0xc,%esp
80104f2c:	50                   	push   %eax
80104f2d:	e8 ea 04 00 00       	call   8010541c <wakeup1>
80104f32:	83 c4 10             	add    $0x10,%esp
  // Pass abandoned children to init.
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80104f35:	c7 45 f4 b4 39 11 80 	movl   $0x801139b4,-0xc(%ebp)
80104f3c:	eb 3f                	jmp    80104f7d <exit+0x113>
    if(p->parent == proc){
80104f3e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104f41:	8b 50 14             	mov    0x14(%eax),%edx
80104f44:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104f4a:	39 c2                	cmp    %eax,%edx
80104f4c:	75 28                	jne    80104f76 <exit+0x10c>
      p->parent = initproc;
80104f4e:	8b 15 68 c6 10 80    	mov    0x8010c668,%edx
80104f54:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104f57:	89 50 14             	mov    %edx,0x14(%eax)
      if(p->state == ZOMBIE)
80104f5a:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104f5d:	8b 40 0c             	mov    0xc(%eax),%eax
80104f60:	83 f8 05             	cmp    $0x5,%eax
80104f63:	75 11                	jne    80104f76 <exit+0x10c>
        wakeup1(initproc);
80104f65:	a1 68 c6 10 80       	mov    0x8010c668,%eax
80104f6a:	83 ec 0c             	sub    $0xc,%esp
80104f6d:	50                   	push   %eax
80104f6e:	e8 a9 04 00 00       	call   8010541c <wakeup1>
80104f73:	83 c4 10             	add    $0x10,%esp
  proc->cwd = 0;
  acquire(&ptable.lock);
  // Parent might be sleeping in wait().
  wakeup1(proc->parent);
  // Pass abandoned children to init.
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80104f76:	81 45 f4 98 00 00 00 	addl   $0x98,-0xc(%ebp)
80104f7d:	81 7d f4 b4 5f 11 80 	cmpl   $0x80115fb4,-0xc(%ebp)
80104f84:	72 b8                	jb     80104f3e <exit+0xd4>
        wakeup1(initproc);
    }
  }
  // Jump into the scheduler, never to return.
#ifdef CS333_P3P4
  fromToStateList(proc, RUNNING, ZOMBIE, &ptable.pLists.running, &ptable.pLists.zombie);
80104f86:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104f8c:	83 ec 0c             	sub    $0xc,%esp
80104f8f:	68 c0 5f 11 80       	push   $0x80115fc0
80104f94:	68 c4 5f 11 80       	push   $0x80115fc4
80104f99:	6a 05                	push   $0x5
80104f9b:	6a 04                	push   $0x4
80104f9d:	50                   	push   %eax
80104f9e:	e8 cd f6 ff ff       	call   80104670 <fromToStateList>
80104fa3:	83 c4 20             	add    $0x20,%esp
#else
  proc->state = ZOMBIE;
#endif
  sched();
80104fa6:	e8 33 02 00 00       	call   801051de <sched>
  panic("zombie exit");
80104fab:	83 ec 0c             	sub    $0xc,%esp
80104fae:	68 2a 96 10 80       	push   $0x8010962a
80104fb3:	e8 ae b5 ff ff       	call   80100566 <panic>

80104fb8 <wait>:
// Wait for a child process to exit and return its pid.
// Return -1 if this process has no children.
#ifndef CS333_P3P4_NO
int
wait(void)
{
80104fb8:	55                   	push   %ebp
80104fb9:	89 e5                	mov    %esp,%ebp
80104fbb:	83 ec 18             	sub    $0x18,%esp
  struct proc *p;
  int havekids, pid;

  acquire(&ptable.lock);
80104fbe:	83 ec 0c             	sub    $0xc,%esp
80104fc1:	68 80 39 11 80       	push   $0x80113980
80104fc6:	e8 7d 0b 00 00       	call   80105b48 <acquire>
80104fcb:	83 c4 10             	add    $0x10,%esp
  for(;;){
    // Scan through table looking for zombie children.
    havekids = 0;
80104fce:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80104fd5:	c7 45 f4 b4 39 11 80 	movl   $0x801139b4,-0xc(%ebp)
80104fdc:	e9 d6 00 00 00       	jmp    801050b7 <wait+0xff>
      if(p->parent != proc)
80104fe1:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104fe4:	8b 50 14             	mov    0x14(%eax),%edx
80104fe7:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104fed:	39 c2                	cmp    %eax,%edx
80104fef:	0f 85 ba 00 00 00    	jne    801050af <wait+0xf7>
        continue;
      havekids = 1;
80104ff5:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
      if(p->state == ZOMBIE){
80104ffc:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104fff:	8b 40 0c             	mov    0xc(%eax),%eax
80105002:	83 f8 05             	cmp    $0x5,%eax
80105005:	0f 85 a5 00 00 00    	jne    801050b0 <wait+0xf8>
        // Found one.
        pid = p->pid;
8010500b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010500e:	8b 40 10             	mov    0x10(%eax),%eax
80105011:	89 45 ec             	mov    %eax,-0x14(%ebp)
        kfree(p->kstack);
80105014:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105017:	8b 40 08             	mov    0x8(%eax),%eax
8010501a:	83 ec 0c             	sub    $0xc,%esp
8010501d:	50                   	push   %eax
8010501e:	e8 44 dc ff ff       	call   80102c67 <kfree>
80105023:	83 c4 10             	add    $0x10,%esp
        p->kstack = 0;
80105026:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105029:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
        freevm(p->pgdir);
80105030:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105033:	8b 40 04             	mov    0x4(%eax),%eax
80105036:	83 ec 0c             	sub    $0xc,%esp
80105039:	50                   	push   %eax
8010503a:	e8 d0 3e 00 00       	call   80108f0f <freevm>
8010503f:	83 c4 10             	add    $0x10,%esp
	#ifdef CS333_P3P4 
        cprintf("found a zombie! name is: %s\n", p->name);
80105042:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105045:	83 c0 6c             	add    $0x6c,%eax
80105048:	83 ec 08             	sub    $0x8,%esp
8010504b:	50                   	push   %eax
8010504c:	68 36 96 10 80       	push   $0x80109636
80105051:	e8 70 b3 ff ff       	call   801003c6 <cprintf>
80105056:	83 c4 10             	add    $0x10,%esp
        fromToStateList(p, ZOMBIE, UNUSED, &ptable.pLists.zombie, &ptable.pLists.free);
80105059:	83 ec 0c             	sub    $0xc,%esp
8010505c:	68 b8 5f 11 80       	push   $0x80115fb8
80105061:	68 c0 5f 11 80       	push   $0x80115fc0
80105066:	6a 00                	push   $0x0
80105068:	6a 05                	push   $0x5
8010506a:	ff 75 f4             	pushl  -0xc(%ebp)
8010506d:	e8 fe f5 ff ff       	call   80104670 <fromToStateList>
80105072:	83 c4 20             	add    $0x20,%esp
        #else
        p->state = UNUSED;
        #endif
        p->pid = 0;
80105075:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105078:	c7 40 10 00 00 00 00 	movl   $0x0,0x10(%eax)
        p->parent = 0;
8010507f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105082:	c7 40 14 00 00 00 00 	movl   $0x0,0x14(%eax)
        p->name[0] = 0;
80105089:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010508c:	c6 40 6c 00          	movb   $0x0,0x6c(%eax)
        p->killed = 0;
80105090:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105093:	c7 40 24 00 00 00 00 	movl   $0x0,0x24(%eax)
        release(&ptable.lock);
8010509a:	83 ec 0c             	sub    $0xc,%esp
8010509d:	68 80 39 11 80       	push   $0x80113980
801050a2:	e8 08 0b 00 00       	call   80105baf <release>
801050a7:	83 c4 10             	add    $0x10,%esp
        return pid;
801050aa:	8b 45 ec             	mov    -0x14(%ebp),%eax
801050ad:	eb 5b                	jmp    8010510a <wait+0x152>
  for(;;){
    // Scan through table looking for zombie children.
    havekids = 0;
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
      if(p->parent != proc)
        continue;
801050af:	90                   	nop

  acquire(&ptable.lock);
  for(;;){
    // Scan through table looking for zombie children.
    havekids = 0;
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
801050b0:	81 45 f4 98 00 00 00 	addl   $0x98,-0xc(%ebp)
801050b7:	81 7d f4 b4 5f 11 80 	cmpl   $0x80115fb4,-0xc(%ebp)
801050be:	0f 82 1d ff ff ff    	jb     80104fe1 <wait+0x29>
        return pid;
      }
    }

    // No point waiting if we don't have any children.
    if(!havekids || proc->killed){
801050c4:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
801050c8:	74 0d                	je     801050d7 <wait+0x11f>
801050ca:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801050d0:	8b 40 24             	mov    0x24(%eax),%eax
801050d3:	85 c0                	test   %eax,%eax
801050d5:	74 17                	je     801050ee <wait+0x136>
      release(&ptable.lock);
801050d7:	83 ec 0c             	sub    $0xc,%esp
801050da:	68 80 39 11 80       	push   $0x80113980
801050df:	e8 cb 0a 00 00       	call   80105baf <release>
801050e4:	83 c4 10             	add    $0x10,%esp
      return -1;
801050e7:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801050ec:	eb 1c                	jmp    8010510a <wait+0x152>
    }

    // Wait for children to exit.  (See wakeup1 call in proc_exit.)
    sleep(proc, &ptable.lock);  //DOC: wait-sleep
801050ee:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801050f4:	83 ec 08             	sub    $0x8,%esp
801050f7:	68 80 39 11 80       	push   $0x80113980
801050fc:	50                   	push   %eax
801050fd:	e8 62 02 00 00       	call   80105364 <sleep>
80105102:	83 c4 10             	add    $0x10,%esp
  }
80105105:	e9 c4 fe ff ff       	jmp    80104fce <wait+0x16>
}
8010510a:	c9                   	leave  
8010510b:	c3                   	ret    

8010510c <scheduler>:
}

#else
void													//***************************************  SCHEDULER  *******
scheduler(void)
{
8010510c:	55                   	push   %ebp
8010510d:	89 e5                	mov    %esp,%ebp
8010510f:	83 ec 18             	sub    $0x18,%esp
  struct proc *p;
  int idle;  // for checking if processor is idle

  for(;;){
    // Enable interrupts on this processor.
    sti();
80105112:	e8 dc f3 ff ff       	call   801044f3 <sti>
    idle = 1;  // assume idle unless we schedule a process
80105117:	c7 45 f4 01 00 00 00 	movl   $0x1,-0xc(%ebp)
    // Loop over process table looking for process to run.
    acquire(&ptable.lock);
8010511e:	83 ec 0c             	sub    $0xc,%esp
80105121:	68 80 39 11 80       	push   $0x80113980
80105126:	e8 1d 0a 00 00       	call   80105b48 <acquire>
8010512b:	83 c4 10             	add    $0x10,%esp
    p = ptable.pLists.ready;
8010512e:	a1 b4 5f 11 80       	mov    0x80115fb4,%eax
80105133:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if(p){
80105136:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
8010513a:	74 79                	je     801051b5 <scheduler+0xa9>
      // Switch to chosen process.  It is the process's job
      // to release ptable.lock and then reacquire it
      // before jumping back to us.
      idle = 0;  // not idle this timeslice
8010513c:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
      proc = p;
80105143:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105146:	65 a3 04 00 00 00    	mov    %eax,%gs:0x4
      switchuvm(p);
8010514c:	83 ec 0c             	sub    $0xc,%esp
8010514f:	ff 75 f0             	pushl  -0x10(%ebp)
80105152:	e8 72 39 00 00       	call   80108ac9 <switchuvm>
80105157:	83 c4 10             	add    $0x10,%esp
      fromToStateList(p, RUNNABLE, RUNNING, &ptable.pLists.ready, &ptable.pLists.running);
8010515a:	83 ec 0c             	sub    $0xc,%esp
8010515d:	68 c4 5f 11 80       	push   $0x80115fc4
80105162:	68 b4 5f 11 80       	push   $0x80115fb4
80105167:	6a 04                	push   $0x4
80105169:	6a 03                	push   $0x3
8010516b:	ff 75 f0             	pushl  -0x10(%ebp)
8010516e:	e8 fd f4 ff ff       	call   80104670 <fromToStateList>
80105173:	83 c4 20             	add    $0x20,%esp
      p->cpu_ticks_in = ticks;			//ticks in
80105176:	8b 15 e0 67 11 80    	mov    0x801167e0,%edx
8010517c:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010517f:	89 90 8c 00 00 00    	mov    %edx,0x8c(%eax)
      swtch(&cpu->scheduler, proc->context);
80105185:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010518b:	8b 40 1c             	mov    0x1c(%eax),%eax
8010518e:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
80105195:	83 c2 04             	add    $0x4,%edx
80105198:	83 ec 08             	sub    $0x8,%esp
8010519b:	50                   	push   %eax
8010519c:	52                   	push   %edx
8010519d:	e8 7d 0e 00 00       	call   8010601f <swtch>
801051a2:	83 c4 10             	add    $0x10,%esp
      switchkvm();
801051a5:	e8 02 39 00 00       	call   80108aac <switchkvm>
      // Process is done running for now.
      // It should have changed its p->state before coming back.
      proc = 0;
801051aa:	65 c7 05 04 00 00 00 	movl   $0x0,%gs:0x4
801051b1:	00 00 00 00 
    }
    release(&ptable.lock);
801051b5:	83 ec 0c             	sub    $0xc,%esp
801051b8:	68 80 39 11 80       	push   $0x80113980
801051bd:	e8 ed 09 00 00       	call   80105baf <release>
801051c2:	83 c4 10             	add    $0x10,%esp
    // if idle, wait for next interrupt
    if (idle) {
801051c5:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801051c9:	0f 84 43 ff ff ff    	je     80105112 <scheduler+0x6>
      sti();
801051cf:	e8 1f f3 ff ff       	call   801044f3 <sti>
      hlt();
801051d4:	e8 03 f3 ff ff       	call   801044dc <hlt>
    }
  }
801051d9:	e9 34 ff ff ff       	jmp    80105112 <scheduler+0x6>

801051de <sched>:
  cpu->intena = intena; 
}
#else
void
sched(void)
{
801051de:	55                   	push   %ebp
801051df:	89 e5                	mov    %esp,%ebp
801051e1:	53                   	push   %ebx
801051e2:	83 ec 14             	sub    $0x14,%esp
  int intena;

  if(!holding(&ptable.lock))
801051e5:	83 ec 0c             	sub    $0xc,%esp
801051e8:	68 80 39 11 80       	push   $0x80113980
801051ed:	e8 89 0a 00 00       	call   80105c7b <holding>
801051f2:	83 c4 10             	add    $0x10,%esp
801051f5:	85 c0                	test   %eax,%eax
801051f7:	75 0d                	jne    80105206 <sched+0x28>
    panic("sched ptable.lock");
801051f9:	83 ec 0c             	sub    $0xc,%esp
801051fc:	68 53 96 10 80       	push   $0x80109653
80105201:	e8 60 b3 ff ff       	call   80100566 <panic>
  if(cpu->ncli != 1)
80105206:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
8010520c:	8b 80 ac 00 00 00    	mov    0xac(%eax),%eax
80105212:	83 f8 01             	cmp    $0x1,%eax
80105215:	74 0d                	je     80105224 <sched+0x46>
    panic("sched locks");
80105217:	83 ec 0c             	sub    $0xc,%esp
8010521a:	68 65 96 10 80       	push   $0x80109665
8010521f:	e8 42 b3 ff ff       	call   80100566 <panic>
  if(proc->state == RUNNING)
80105224:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010522a:	8b 40 0c             	mov    0xc(%eax),%eax
8010522d:	83 f8 04             	cmp    $0x4,%eax
80105230:	75 0d                	jne    8010523f <sched+0x61>
    panic("sched running");
80105232:	83 ec 0c             	sub    $0xc,%esp
80105235:	68 71 96 10 80       	push   $0x80109671
8010523a:	e8 27 b3 ff ff       	call   80100566 <panic>
  if(readeflags()&FL_IF)
8010523f:	e8 9f f2 ff ff       	call   801044e3 <readeflags>
80105244:	25 00 02 00 00       	and    $0x200,%eax
80105249:	85 c0                	test   %eax,%eax
8010524b:	74 0d                	je     8010525a <sched+0x7c>
    panic("sched interruptible");
8010524d:	83 ec 0c             	sub    $0xc,%esp
80105250:	68 7f 96 10 80       	push   $0x8010967f
80105255:	e8 0c b3 ff ff       	call   80100566 <panic>
  intena = cpu->intena;
8010525a:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80105260:	8b 80 b0 00 00 00    	mov    0xb0(%eax),%eax
80105266:	89 45 f4             	mov    %eax,-0xc(%ebp)
  proc->cpu_ticks_total += (ticks - proc->cpu_ticks_in);
80105269:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010526f:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
80105276:	8b 8a 88 00 00 00    	mov    0x88(%edx),%ecx
8010527c:	8b 1d e0 67 11 80    	mov    0x801167e0,%ebx
80105282:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
80105289:	8b 92 8c 00 00 00    	mov    0x8c(%edx),%edx
8010528f:	29 d3                	sub    %edx,%ebx
80105291:	89 da                	mov    %ebx,%edx
80105293:	01 ca                	add    %ecx,%edx
80105295:	89 90 88 00 00 00    	mov    %edx,0x88(%eax)
  swtch(&proc->context, cpu->scheduler);
8010529b:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
801052a1:	8b 40 04             	mov    0x4(%eax),%eax
801052a4:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
801052ab:	83 c2 1c             	add    $0x1c,%edx
801052ae:	83 ec 08             	sub    $0x8,%esp
801052b1:	50                   	push   %eax
801052b2:	52                   	push   %edx
801052b3:	e8 67 0d 00 00       	call   8010601f <swtch>
801052b8:	83 c4 10             	add    $0x10,%esp
  cpu->intena = intena;  
801052bb:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
801052c1:	8b 55 f4             	mov    -0xc(%ebp),%edx
801052c4:	89 90 b0 00 00 00    	mov    %edx,0xb0(%eax)
}
801052ca:	90                   	nop
801052cb:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801052ce:	c9                   	leave  
801052cf:	c3                   	ret    

801052d0 <yield>:
#endif

// Give up the CPU for one scheduling round.
void
yield(void)
{
801052d0:	55                   	push   %ebp
801052d1:	89 e5                	mov    %esp,%ebp
801052d3:	83 ec 08             	sub    $0x8,%esp
  acquire(&ptable.lock);  //DOC: yieldlock
801052d6:	83 ec 0c             	sub    $0xc,%esp
801052d9:	68 80 39 11 80       	push   $0x80113980
801052de:	e8 65 08 00 00       	call   80105b48 <acquire>
801052e3:	83 c4 10             	add    $0x10,%esp
#ifndef CS333_P3P4
  proc->state = RUNNABLE;
#else
  fromToStateList(proc, RUNNING, RUNNABLE, &ptable.pLists.running, &ptable.pLists.ready);
801052e6:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801052ec:	83 ec 0c             	sub    $0xc,%esp
801052ef:	68 b4 5f 11 80       	push   $0x80115fb4
801052f4:	68 c4 5f 11 80       	push   $0x80115fc4
801052f9:	6a 03                	push   $0x3
801052fb:	6a 04                	push   $0x4
801052fd:	50                   	push   %eax
801052fe:	e8 6d f3 ff ff       	call   80104670 <fromToStateList>
80105303:	83 c4 20             	add    $0x20,%esp
#endif
  sched();
80105306:	e8 d3 fe ff ff       	call   801051de <sched>
  release(&ptable.lock);
8010530b:	83 ec 0c             	sub    $0xc,%esp
8010530e:	68 80 39 11 80       	push   $0x80113980
80105313:	e8 97 08 00 00       	call   80105baf <release>
80105318:	83 c4 10             	add    $0x10,%esp
}
8010531b:	90                   	nop
8010531c:	c9                   	leave  
8010531d:	c3                   	ret    

8010531e <forkret>:

// A fork child's very first scheduling by scheduler()
// will swtch here.  "Return" to user space.
void
forkret(void)
{
8010531e:	55                   	push   %ebp
8010531f:	89 e5                	mov    %esp,%ebp
80105321:	83 ec 08             	sub    $0x8,%esp
  static int first = 1;
  // Still holding ptable.lock from scheduler.
  release(&ptable.lock);
80105324:	83 ec 0c             	sub    $0xc,%esp
80105327:	68 80 39 11 80       	push   $0x80113980
8010532c:	e8 7e 08 00 00       	call   80105baf <release>
80105331:	83 c4 10             	add    $0x10,%esp

  if (first) {
80105334:	a1 20 c0 10 80       	mov    0x8010c020,%eax
80105339:	85 c0                	test   %eax,%eax
8010533b:	74 24                	je     80105361 <forkret+0x43>
    // Some initialization functions must be run in the context
    // of a regular process (e.g., they call sleep), and thus cannot 
    // be run from main().
    first = 0;
8010533d:	c7 05 20 c0 10 80 00 	movl   $0x0,0x8010c020
80105344:	00 00 00 
    iinit(ROOTDEV);
80105347:	83 ec 0c             	sub    $0xc,%esp
8010534a:	6a 01                	push   $0x1
8010534c:	e8 8b c3 ff ff       	call   801016dc <iinit>
80105351:	83 c4 10             	add    $0x10,%esp
    initlog(ROOTDEV);
80105354:	83 ec 0c             	sub    $0xc,%esp
80105357:	6a 01                	push   $0x1
80105359:	e8 6f e0 ff ff       	call   801033cd <initlog>
8010535e:	83 c4 10             	add    $0x10,%esp
  }
  
  // Return to "caller", actually trapret (see allocproc).
}
80105361:	90                   	nop
80105362:	c9                   	leave  
80105363:	c3                   	ret    

80105364 <sleep>:
// Reacquires lock when awakened.
// 2016/12/28: ticklock removed from xv6. sleep() changed to
// accept a NULL lock to accommodate.
void
sleep(void *chan, struct spinlock *lk)
{
80105364:	55                   	push   %ebp
80105365:	89 e5                	mov    %esp,%ebp
80105367:	83 ec 08             	sub    $0x8,%esp
  if(proc == 0)
8010536a:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105370:	85 c0                	test   %eax,%eax
80105372:	75 0d                	jne    80105381 <sleep+0x1d>
    panic("sleep");
80105374:	83 ec 0c             	sub    $0xc,%esp
80105377:	68 93 96 10 80       	push   $0x80109693
8010537c:	e8 e5 b1 ff ff       	call   80100566 <panic>
  // change p->state and then call sched.
  // Once we hold ptable.lock, we can be
  // guaranteed that we won't miss any wakeup
  // (wakeup runs with ptable.lock locked),
  // so it's okay to release lk.
  if(lk != &ptable.lock){
80105381:	81 7d 0c 80 39 11 80 	cmpl   $0x80113980,0xc(%ebp)
80105388:	74 24                	je     801053ae <sleep+0x4a>
    acquire(&ptable.lock);
8010538a:	83 ec 0c             	sub    $0xc,%esp
8010538d:	68 80 39 11 80       	push   $0x80113980
80105392:	e8 b1 07 00 00       	call   80105b48 <acquire>
80105397:	83 c4 10             	add    $0x10,%esp
    if (lk) release(lk);
8010539a:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
8010539e:	74 0e                	je     801053ae <sleep+0x4a>
801053a0:	83 ec 0c             	sub    $0xc,%esp
801053a3:	ff 75 0c             	pushl  0xc(%ebp)
801053a6:	e8 04 08 00 00       	call   80105baf <release>
801053ab:	83 c4 10             	add    $0x10,%esp
  }

  // Go to sleep.
  proc->chan = chan;
801053ae:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801053b4:	8b 55 08             	mov    0x8(%ebp),%edx
801053b7:	89 50 20             	mov    %edx,0x20(%eax)
#ifndef CS333_P3P4
  proc->state = SLEEPING;
#else
  fromToStateList(proc, RUNNING, SLEEPING, &ptable.pLists.running, &ptable.pLists.sleep);
801053ba:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801053c0:	83 ec 0c             	sub    $0xc,%esp
801053c3:	68 bc 5f 11 80       	push   $0x80115fbc
801053c8:	68 c4 5f 11 80       	push   $0x80115fc4
801053cd:	6a 02                	push   $0x2
801053cf:	6a 04                	push   $0x4
801053d1:	50                   	push   %eax
801053d2:	e8 99 f2 ff ff       	call   80104670 <fromToStateList>
801053d7:	83 c4 20             	add    $0x20,%esp
#endif
  sched();
801053da:	e8 ff fd ff ff       	call   801051de <sched>

  // Tidy up.
  proc->chan = 0;
801053df:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801053e5:	c7 40 20 00 00 00 00 	movl   $0x0,0x20(%eax)

  // Reacquire original lock.
  if(lk != &ptable.lock){ 
801053ec:	81 7d 0c 80 39 11 80 	cmpl   $0x80113980,0xc(%ebp)
801053f3:	74 24                	je     80105419 <sleep+0xb5>
    release(&ptable.lock);
801053f5:	83 ec 0c             	sub    $0xc,%esp
801053f8:	68 80 39 11 80       	push   $0x80113980
801053fd:	e8 ad 07 00 00       	call   80105baf <release>
80105402:	83 c4 10             	add    $0x10,%esp
    if (lk) acquire(lk);
80105405:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
80105409:	74 0e                	je     80105419 <sleep+0xb5>
8010540b:	83 ec 0c             	sub    $0xc,%esp
8010540e:	ff 75 0c             	pushl  0xc(%ebp)
80105411:	e8 32 07 00 00       	call   80105b48 <acquire>
80105416:	83 c4 10             	add    $0x10,%esp
  }
}
80105419:	90                   	nop
8010541a:	c9                   	leave  
8010541b:	c3                   	ret    

8010541c <wakeup1>:
      p->state = RUNNABLE;
}
#else
static void												//*********************** WAKEUP1 ****************************
wakeup1(void *chan)
{
8010541c:	55                   	push   %ebp
8010541d:	89 e5                	mov    %esp,%ebp
8010541f:	83 ec 18             	sub    $0x18,%esp
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
    if(p->state == SLEEPING && p->chan == chan)
        fromToStateList(p, SLEEPING, RUNNABLE, &ptable.pLists.sleep, &ptable.pLists.ready);
*/
  struct proc *p, *r;
  p = ptable.pLists.sleep;
80105422:	a1 bc 5f 11 80       	mov    0x80115fbc,%eax
80105427:	89 45 f4             	mov    %eax,-0xc(%ebp)
  while(p != 0){
8010542a:	eb 44                	jmp    80105470 <wakeup1+0x54>
    r = p->next;
8010542c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010542f:	8b 80 90 00 00 00    	mov    0x90(%eax),%eax
80105435:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if(p->state == SLEEPING && p->chan == chan)
80105438:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010543b:	8b 40 0c             	mov    0xc(%eax),%eax
8010543e:	83 f8 02             	cmp    $0x2,%eax
80105441:	75 27                	jne    8010546a <wakeup1+0x4e>
80105443:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105446:	8b 40 20             	mov    0x20(%eax),%eax
80105449:	3b 45 08             	cmp    0x8(%ebp),%eax
8010544c:	75 1c                	jne    8010546a <wakeup1+0x4e>
      fromToStateList(p, SLEEPING, RUNNABLE, &ptable.pLists.sleep, &ptable.pLists.ready);
8010544e:	83 ec 0c             	sub    $0xc,%esp
80105451:	68 b4 5f 11 80       	push   $0x80115fb4
80105456:	68 bc 5f 11 80       	push   $0x80115fbc
8010545b:	6a 03                	push   $0x3
8010545d:	6a 02                	push   $0x2
8010545f:	ff 75 f4             	pushl  -0xc(%ebp)
80105462:	e8 09 f2 ff ff       	call   80104670 <fromToStateList>
80105467:	83 c4 20             	add    $0x20,%esp
    p = r;
8010546a:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010546d:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if(p->state == SLEEPING && p->chan == chan)
        fromToStateList(p, SLEEPING, RUNNABLE, &ptable.pLists.sleep, &ptable.pLists.ready);
*/
  struct proc *p, *r;
  p = ptable.pLists.sleep;
  while(p != 0){
80105470:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80105474:	75 b6                	jne    8010542c <wakeup1+0x10>
    if(p->state == SLEEPING && p->chan == chan)
      fromToStateList(p, SLEEPING, RUNNABLE, &ptable.pLists.sleep, &ptable.pLists.ready);
    p = r;
  }

}
80105476:	90                   	nop
80105477:	c9                   	leave  
80105478:	c3                   	ret    

80105479 <wakeup>:
#endif

// Wake up all processes sleeping on chan.
void
wakeup(void *chan)
{
80105479:	55                   	push   %ebp
8010547a:	89 e5                	mov    %esp,%ebp
8010547c:	83 ec 08             	sub    $0x8,%esp
  acquire(&ptable.lock);
8010547f:	83 ec 0c             	sub    $0xc,%esp
80105482:	68 80 39 11 80       	push   $0x80113980
80105487:	e8 bc 06 00 00       	call   80105b48 <acquire>
8010548c:	83 c4 10             	add    $0x10,%esp
  wakeup1(chan);
8010548f:	83 ec 0c             	sub    $0xc,%esp
80105492:	ff 75 08             	pushl  0x8(%ebp)
80105495:	e8 82 ff ff ff       	call   8010541c <wakeup1>
8010549a:	83 c4 10             	add    $0x10,%esp
  release(&ptable.lock);
8010549d:	83 ec 0c             	sub    $0xc,%esp
801054a0:	68 80 39 11 80       	push   $0x80113980
801054a5:	e8 05 07 00 00       	call   80105baf <release>
801054aa:	83 c4 10             	add    $0x10,%esp
}
801054ad:	90                   	nop
801054ae:	c9                   	leave  
801054af:	c3                   	ret    

801054b0 <kill>:
// Process won't exit until it returns
// to user space (see trap in trap.c).
#ifndef CS333_P3P4_NO
int
kill(int pid)
{
801054b0:	55                   	push   %ebp
801054b1:	89 e5                	mov    %esp,%ebp
801054b3:	83 ec 18             	sub    $0x18,%esp
  struct proc *p;

  acquire(&ptable.lock);
801054b6:	83 ec 0c             	sub    $0xc,%esp
801054b9:	68 80 39 11 80       	push   $0x80113980
801054be:	e8 85 06 00 00       	call   80105b48 <acquire>
801054c3:	83 c4 10             	add    $0x10,%esp
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
801054c6:	c7 45 f4 b4 39 11 80 	movl   $0x801139b4,-0xc(%ebp)
801054cd:	eb 5c                	jmp    8010552b <kill+0x7b>
    if(p->pid == pid){
801054cf:	8b 45 f4             	mov    -0xc(%ebp),%eax
801054d2:	8b 50 10             	mov    0x10(%eax),%edx
801054d5:	8b 45 08             	mov    0x8(%ebp),%eax
801054d8:	39 c2                	cmp    %eax,%edx
801054da:	75 48                	jne    80105524 <kill+0x74>
      p->killed = 1;
801054dc:	8b 45 f4             	mov    -0xc(%ebp),%eax
801054df:	c7 40 24 01 00 00 00 	movl   $0x1,0x24(%eax)
      // Wake process from sleep if necessary.
      if(p->state == SLEEPING) {
801054e6:	8b 45 f4             	mov    -0xc(%ebp),%eax
801054e9:	8b 40 0c             	mov    0xc(%eax),%eax
801054ec:	83 f8 02             	cmp    $0x2,%eax
801054ef:	75 1c                	jne    8010550d <kill+0x5d>
        #ifdef CS333_P3P4
        fromToStateList(p, SLEEPING, RUNNABLE, &ptable.pLists.sleep, &ptable.pLists.ready);
801054f1:	83 ec 0c             	sub    $0xc,%esp
801054f4:	68 b4 5f 11 80       	push   $0x80115fb4
801054f9:	68 bc 5f 11 80       	push   $0x80115fbc
801054fe:	6a 03                	push   $0x3
80105500:	6a 02                	push   $0x2
80105502:	ff 75 f4             	pushl  -0xc(%ebp)
80105505:	e8 66 f1 ff ff       	call   80104670 <fromToStateList>
8010550a:	83 c4 20             	add    $0x20,%esp
        #else
        p->state = RUNNABLE;
        #endif
      }
      release(&ptable.lock);
8010550d:	83 ec 0c             	sub    $0xc,%esp
80105510:	68 80 39 11 80       	push   $0x80113980
80105515:	e8 95 06 00 00       	call   80105baf <release>
8010551a:	83 c4 10             	add    $0x10,%esp
      return 0;
8010551d:	b8 00 00 00 00       	mov    $0x0,%eax
80105522:	eb 25                	jmp    80105549 <kill+0x99>
kill(int pid)
{
  struct proc *p;

  acquire(&ptable.lock);
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80105524:	81 45 f4 98 00 00 00 	addl   $0x98,-0xc(%ebp)
8010552b:	81 7d f4 b4 5f 11 80 	cmpl   $0x80115fb4,-0xc(%ebp)
80105532:	72 9b                	jb     801054cf <kill+0x1f>
      }
      release(&ptable.lock);
      return 0;
    }
  }
  release(&ptable.lock);
80105534:	83 ec 0c             	sub    $0xc,%esp
80105537:	68 80 39 11 80       	push   $0x80113980
8010553c:	e8 6e 06 00 00       	call   80105baf <release>
80105541:	83 c4 10             	add    $0x10,%esp
  return -1;
80105544:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80105549:	c9                   	leave  
8010554a:	c3                   	ret    

8010554b <procdump>:
// Print a process listing to console.  For debugging.
// Runs when user types ^P on console.
// No lock to avoid wedging a stuck machine further.
void
procdump(void)
{
8010554b:	55                   	push   %ebp
8010554c:	89 e5                	mov    %esp,%ebp
8010554e:	57                   	push   %edi
8010554f:	56                   	push   %esi
80105550:	53                   	push   %ebx
80105551:	83 ec 5c             	sub    $0x5c,%esp
  uint sec;
  uint part;
  uint pc[10];
  int ppid;
 
  cprintf("\nPID\tName\tUID\tGID\tPPID\tElapsed\tCPU\tState\tSize\tPCs\n");
80105554:	83 ec 0c             	sub    $0xc,%esp
80105557:	68 c4 96 10 80       	push   $0x801096c4
8010555c:	e8 65 ae ff ff       	call   801003c6 <cprintf>
80105561:	83 c4 10             	add    $0x10,%esp
 
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80105564:	c7 45 e0 b4 39 11 80 	movl   $0x801139b4,-0x20(%ebp)
8010556b:	e9 a2 01 00 00       	jmp    80105712 <procdump+0x1c7>
    if(p->state == UNUSED)
80105570:	8b 45 e0             	mov    -0x20(%ebp),%eax
80105573:	8b 40 0c             	mov    0xc(%eax),%eax
80105576:	85 c0                	test   %eax,%eax
80105578:	0f 84 8c 01 00 00    	je     8010570a <procdump+0x1bf>
      continue;
    if(p->state >= 0 && p->state < NELEM(states) && states[p->state]){
8010557e:	8b 45 e0             	mov    -0x20(%ebp),%eax
80105581:	8b 40 0c             	mov    0xc(%eax),%eax
80105584:	83 f8 05             	cmp    $0x5,%eax
80105587:	77 23                	ja     801055ac <procdump+0x61>
80105589:	8b 45 e0             	mov    -0x20(%ebp),%eax
8010558c:	8b 40 0c             	mov    0xc(%eax),%eax
8010558f:	8b 04 85 08 c0 10 80 	mov    -0x7fef3ff8(,%eax,4),%eax
80105596:	85 c0                	test   %eax,%eax
80105598:	74 12                	je     801055ac <procdump+0x61>
      state = states[p->state];
8010559a:	8b 45 e0             	mov    -0x20(%ebp),%eax
8010559d:	8b 40 0c             	mov    0xc(%eax),%eax
801055a0:	8b 04 85 08 c0 10 80 	mov    -0x7fef3ff8(,%eax,4),%eax
801055a7:	89 45 dc             	mov    %eax,-0x24(%ebp)
801055aa:	eb 07                	jmp    801055b3 <procdump+0x68>

    }
    else
      state = "???";
801055ac:	c7 45 dc f7 96 10 80 	movl   $0x801096f7,-0x24(%ebp)
    sec = ((ticks)-(p->start_ticks)) / 100;
801055b3:	8b 15 e0 67 11 80    	mov    0x801167e0,%edx
801055b9:	8b 45 e0             	mov    -0x20(%ebp),%eax
801055bc:	8b 40 7c             	mov    0x7c(%eax),%eax
801055bf:	29 c2                	sub    %eax,%edx
801055c1:	89 d0                	mov    %edx,%eax
801055c3:	ba 1f 85 eb 51       	mov    $0x51eb851f,%edx
801055c8:	f7 e2                	mul    %edx
801055ca:	89 d0                	mov    %edx,%eax
801055cc:	c1 e8 05             	shr    $0x5,%eax
801055cf:	89 45 d4             	mov    %eax,-0x2c(%ebp)
    part = ((ticks)-(p->start_ticks)) % 100;
801055d2:	8b 15 e0 67 11 80    	mov    0x801167e0,%edx
801055d8:	8b 45 e0             	mov    -0x20(%ebp),%eax
801055db:	8b 40 7c             	mov    0x7c(%eax),%eax
801055de:	89 d1                	mov    %edx,%ecx
801055e0:	29 c1                	sub    %eax,%ecx
801055e2:	ba 1f 85 eb 51       	mov    $0x51eb851f,%edx
801055e7:	89 c8                	mov    %ecx,%eax
801055e9:	f7 e2                	mul    %edx
801055eb:	89 d0                	mov    %edx,%eax
801055ed:	c1 e8 05             	shr    $0x5,%eax
801055f0:	89 45 d0             	mov    %eax,-0x30(%ebp)
801055f3:	8b 45 d0             	mov    -0x30(%ebp),%eax
801055f6:	6b c0 64             	imul   $0x64,%eax,%eax
801055f9:	29 c1                	sub    %eax,%ecx
801055fb:	89 c8                	mov    %ecx,%eax
801055fd:	89 45 d0             	mov    %eax,-0x30(%ebp)
    if(p->pid == 1) ppid = 1;
80105600:	8b 45 e0             	mov    -0x20(%ebp),%eax
80105603:	8b 40 10             	mov    0x10(%eax),%eax
80105606:	83 f8 01             	cmp    $0x1,%eax
80105609:	75 09                	jne    80105614 <procdump+0xc9>
8010560b:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
80105612:	eb 0c                	jmp    80105620 <procdump+0xd5>
    else ppid = p->parent->pid;  
80105614:	8b 45 e0             	mov    -0x20(%ebp),%eax
80105617:	8b 40 14             	mov    0x14(%eax),%eax
8010561a:	8b 40 10             	mov    0x10(%eax),%eax
8010561d:	89 45 d8             	mov    %eax,-0x28(%ebp)
    cprintf("%d\t%s\t%d\t%d\t%d\t%d.%d\t%d.%d\t%s\t%d\t", p->pid, p->name, p->uid, p->gid, ppid, sec, part, p->cpu_ticks_total / 100, p->cpu_ticks_total % 100, state, p->sz);
80105620:	8b 45 e0             	mov    -0x20(%ebp),%eax
80105623:	8b 30                	mov    (%eax),%esi
80105625:	8b 45 e0             	mov    -0x20(%ebp),%eax
80105628:	8b 88 88 00 00 00    	mov    0x88(%eax),%ecx
8010562e:	ba 1f 85 eb 51       	mov    $0x51eb851f,%edx
80105633:	89 c8                	mov    %ecx,%eax
80105635:	f7 e2                	mul    %edx
80105637:	89 d3                	mov    %edx,%ebx
80105639:	c1 eb 05             	shr    $0x5,%ebx
8010563c:	6b c3 64             	imul   $0x64,%ebx,%eax
8010563f:	89 cb                	mov    %ecx,%ebx
80105641:	29 c3                	sub    %eax,%ebx
80105643:	8b 45 e0             	mov    -0x20(%ebp),%eax
80105646:	8b 80 88 00 00 00    	mov    0x88(%eax),%eax
8010564c:	ba 1f 85 eb 51       	mov    $0x51eb851f,%edx
80105651:	f7 e2                	mul    %edx
80105653:	c1 ea 05             	shr    $0x5,%edx
80105656:	89 55 a4             	mov    %edx,-0x5c(%ebp)
80105659:	8b 45 e0             	mov    -0x20(%ebp),%eax
8010565c:	8b 88 80 00 00 00    	mov    0x80(%eax),%ecx
80105662:	8b 45 e0             	mov    -0x20(%ebp),%eax
80105665:	8b 90 84 00 00 00    	mov    0x84(%eax),%edx
8010566b:	8b 45 e0             	mov    -0x20(%ebp),%eax
8010566e:	8d 78 6c             	lea    0x6c(%eax),%edi
80105671:	8b 45 e0             	mov    -0x20(%ebp),%eax
80105674:	8b 40 10             	mov    0x10(%eax),%eax
80105677:	56                   	push   %esi
80105678:	ff 75 dc             	pushl  -0x24(%ebp)
8010567b:	53                   	push   %ebx
8010567c:	ff 75 a4             	pushl  -0x5c(%ebp)
8010567f:	ff 75 d0             	pushl  -0x30(%ebp)
80105682:	ff 75 d4             	pushl  -0x2c(%ebp)
80105685:	ff 75 d8             	pushl  -0x28(%ebp)
80105688:	51                   	push   %ecx
80105689:	52                   	push   %edx
8010568a:	57                   	push   %edi
8010568b:	50                   	push   %eax
8010568c:	68 fc 96 10 80       	push   $0x801096fc
80105691:	e8 30 ad ff ff       	call   801003c6 <cprintf>
80105696:	83 c4 30             	add    $0x30,%esp
    if(p->state == SLEEPING){
80105699:	8b 45 e0             	mov    -0x20(%ebp),%eax
8010569c:	8b 40 0c             	mov    0xc(%eax),%eax
8010569f:	83 f8 02             	cmp    $0x2,%eax
801056a2:	75 54                	jne    801056f8 <procdump+0x1ad>
      getcallerpcs((uint*)p->context->ebp+2, pc);
801056a4:	8b 45 e0             	mov    -0x20(%ebp),%eax
801056a7:	8b 40 1c             	mov    0x1c(%eax),%eax
801056aa:	8b 40 0c             	mov    0xc(%eax),%eax
801056ad:	83 c0 08             	add    $0x8,%eax
801056b0:	89 c2                	mov    %eax,%edx
801056b2:	83 ec 08             	sub    $0x8,%esp
801056b5:	8d 45 a8             	lea    -0x58(%ebp),%eax
801056b8:	50                   	push   %eax
801056b9:	52                   	push   %edx
801056ba:	e8 42 05 00 00       	call   80105c01 <getcallerpcs>
801056bf:	83 c4 10             	add    $0x10,%esp
      for(i=0; i<10 && pc[i] != 0; i++){
801056c2:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
801056c9:	eb 1c                	jmp    801056e7 <procdump+0x19c>

        cprintf("%p  ", pc[i]);
801056cb:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801056ce:	8b 44 85 a8          	mov    -0x58(%ebp,%eax,4),%eax
801056d2:	83 ec 08             	sub    $0x8,%esp
801056d5:	50                   	push   %eax
801056d6:	68 1e 97 10 80       	push   $0x8010971e
801056db:	e8 e6 ac ff ff       	call   801003c6 <cprintf>
801056e0:	83 c4 10             	add    $0x10,%esp
    if(p->pid == 1) ppid = 1;
    else ppid = p->parent->pid;  
    cprintf("%d\t%s\t%d\t%d\t%d\t%d.%d\t%d.%d\t%s\t%d\t", p->pid, p->name, p->uid, p->gid, ppid, sec, part, p->cpu_ticks_total / 100, p->cpu_ticks_total % 100, state, p->sz);
    if(p->state == SLEEPING){
      getcallerpcs((uint*)p->context->ebp+2, pc);
      for(i=0; i<10 && pc[i] != 0; i++){
801056e3:	83 45 e4 01          	addl   $0x1,-0x1c(%ebp)
801056e7:	83 7d e4 09          	cmpl   $0x9,-0x1c(%ebp)
801056eb:	7f 0b                	jg     801056f8 <procdump+0x1ad>
801056ed:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801056f0:	8b 44 85 a8          	mov    -0x58(%ebp,%eax,4),%eax
801056f4:	85 c0                	test   %eax,%eax
801056f6:	75 d3                	jne    801056cb <procdump+0x180>

        cprintf("%p  ", pc[i]);
      }
    }
    cprintf("\n");
801056f8:	83 ec 0c             	sub    $0xc,%esp
801056fb:	68 e3 95 10 80       	push   $0x801095e3
80105700:	e8 c1 ac ff ff       	call   801003c6 <cprintf>
80105705:	83 c4 10             	add    $0x10,%esp
80105708:	eb 01                	jmp    8010570b <procdump+0x1c0>
 
  cprintf("\nPID\tName\tUID\tGID\tPPID\tElapsed\tCPU\tState\tSize\tPCs\n");
 
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->state == UNUSED)
      continue;
8010570a:	90                   	nop
  uint pc[10];
  int ppid;
 
  cprintf("\nPID\tName\tUID\tGID\tPPID\tElapsed\tCPU\tState\tSize\tPCs\n");
 
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
8010570b:	81 45 e0 98 00 00 00 	addl   $0x98,-0x20(%ebp)
80105712:	81 7d e0 b4 5f 11 80 	cmpl   $0x80115fb4,-0x20(%ebp)
80105719:	0f 82 51 fe ff ff    	jb     80105570 <procdump+0x25>
        cprintf("%p  ", pc[i]);
      }
    }
    cprintf("\n");
  }
}
8010571f:	90                   	nop
80105720:	8d 65 f4             	lea    -0xc(%ebp),%esp
80105723:	5b                   	pop    %ebx
80105724:	5e                   	pop    %esi
80105725:	5f                   	pop    %edi
80105726:	5d                   	pop    %ebp
80105727:	c3                   	ret    

80105728 <printready>:
		//************************************************ control helpers ********************************
void
printready(void)
{
80105728:	55                   	push   %ebp
80105729:	89 e5                	mov    %esp,%ebp
8010572b:	83 ec 18             	sub    $0x18,%esp
  struct proc *p;
  p = ptable.pLists.ready;
8010572e:	a1 b4 5f 11 80       	mov    0x80115fb4,%eax
80105733:	89 45 f4             	mov    %eax,-0xc(%ebp)
  cprintf("Ready List Processes:\n");
80105736:	83 ec 0c             	sub    $0xc,%esp
80105739:	68 23 97 10 80       	push   $0x80109723
8010573e:	e8 83 ac ff ff       	call   801003c6 <cprintf>
80105743:	83 c4 10             	add    $0x10,%esp
  readysleep(p);
80105746:	83 ec 0c             	sub    $0xc,%esp
80105749:	ff 75 f4             	pushl  -0xc(%ebp)
8010574c:	e8 db ef ff ff       	call   8010472c <readysleep>
80105751:	83 c4 10             	add    $0x10,%esp
}
80105754:	90                   	nop
80105755:	c9                   	leave  
80105756:	c3                   	ret    

80105757 <printfree>:
void
printfree(void)
{
80105757:	55                   	push   %ebp
80105758:	89 e5                	mov    %esp,%ebp
8010575a:	83 ec 18             	sub    $0x18,%esp
  struct proc *p;
  p = ptable.pLists.free;
8010575d:	a1 b8 5f 11 80       	mov    0x80115fb8,%eax
80105762:	89 45 f4             	mov    %eax,-0xc(%ebp)
  cprintf("Free List Size: %d  processes.\n", count(p));
80105765:	ff 75 f4             	pushl  -0xc(%ebp)
80105768:	e8 82 ef ff ff       	call   801046ef <count>
8010576d:	83 c4 04             	add    $0x4,%esp
80105770:	83 ec 08             	sub    $0x8,%esp
80105773:	50                   	push   %eax
80105774:	68 3c 97 10 80       	push   $0x8010973c
80105779:	e8 48 ac ff ff       	call   801003c6 <cprintf>
8010577e:	83 c4 10             	add    $0x10,%esp
}
80105781:	90                   	nop
80105782:	c9                   	leave  
80105783:	c3                   	ret    

80105784 <printsleep>:
void
printsleep(void)
{
80105784:	55                   	push   %ebp
80105785:	89 e5                	mov    %esp,%ebp
80105787:	83 ec 18             	sub    $0x18,%esp
  struct proc *p;
  p = ptable.pLists.sleep;
8010578a:	a1 bc 5f 11 80       	mov    0x80115fbc,%eax
8010578f:	89 45 f4             	mov    %eax,-0xc(%ebp)
  cprintf("Sleep List Processes:\n");
80105792:	83 ec 0c             	sub    $0xc,%esp
80105795:	68 5c 97 10 80       	push   $0x8010975c
8010579a:	e8 27 ac ff ff       	call   801003c6 <cprintf>
8010579f:	83 c4 10             	add    $0x10,%esp
  readysleep(p);
801057a2:	83 ec 0c             	sub    $0xc,%esp
801057a5:	ff 75 f4             	pushl  -0xc(%ebp)
801057a8:	e8 7f ef ff ff       	call   8010472c <readysleep>
801057ad:	83 c4 10             	add    $0x10,%esp
}
801057b0:	90                   	nop
801057b1:	c9                   	leave  
801057b2:	c3                   	ret    

801057b3 <printzombie>:
void
printzombie(void)
{
801057b3:	55                   	push   %ebp
801057b4:	89 e5                	mov    %esp,%ebp
801057b6:	83 ec 18             	sub    $0x18,%esp
  struct proc *p;
  p = ptable.pLists.zombie;
801057b9:	a1 c0 5f 11 80       	mov    0x80115fc0,%eax
801057be:	89 45 f4             	mov    %eax,-0xc(%ebp)
  cprintf("Zombie List Processes:\n");
801057c1:	83 ec 0c             	sub    $0xc,%esp
801057c4:	68 73 97 10 80       	push   $0x80109773
801057c9:	e8 f8 ab ff ff       	call   801003c6 <cprintf>
801057ce:	83 c4 10             	add    $0x10,%esp
  zombie(p);
801057d1:	83 ec 0c             	sub    $0xc,%esp
801057d4:	ff 75 f4             	pushl  -0xc(%ebp)
801057d7:	e8 ba ef ff ff       	call   80104796 <zombie>
801057dc:	83 c4 10             	add    $0x10,%esp
}
801057df:	90                   	nop
801057e0:	c9                   	leave  
801057e1:	c3                   	ret    

801057e2 <setuid>:
					//********************************************************************************
int
setuid(int num)
{
801057e2:	55                   	push   %ebp
801057e3:	89 e5                	mov    %esp,%ebp
801057e5:	83 ec 08             	sub    $0x8,%esp
  acquire(&ptable.lock);
801057e8:	83 ec 0c             	sub    $0xc,%esp
801057eb:	68 80 39 11 80       	push   $0x80113980
801057f0:	e8 53 03 00 00       	call   80105b48 <acquire>
801057f5:	83 c4 10             	add    $0x10,%esp
  proc->uid = num;
801057f8:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801057fe:	8b 55 08             	mov    0x8(%ebp),%edx
80105801:	89 90 84 00 00 00    	mov    %edx,0x84(%eax)
  release(&ptable.lock);
80105807:	83 ec 0c             	sub    $0xc,%esp
8010580a:	68 80 39 11 80       	push   $0x80113980
8010580f:	e8 9b 03 00 00       	call   80105baf <release>
80105814:	83 c4 10             	add    $0x10,%esp
  return 0;
80105817:	b8 00 00 00 00       	mov    $0x0,%eax
}
8010581c:	c9                   	leave  
8010581d:	c3                   	ret    

8010581e <setgid>:

int
setgid(int num)
{
8010581e:	55                   	push   %ebp
8010581f:	89 e5                	mov    %esp,%ebp
80105821:	83 ec 08             	sub    $0x8,%esp
  acquire(&ptable.lock);
80105824:	83 ec 0c             	sub    $0xc,%esp
80105827:	68 80 39 11 80       	push   $0x80113980
8010582c:	e8 17 03 00 00       	call   80105b48 <acquire>
80105831:	83 c4 10             	add    $0x10,%esp
  proc->gid = num;
80105834:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010583a:	8b 55 08             	mov    0x8(%ebp),%edx
8010583d:	89 90 80 00 00 00    	mov    %edx,0x80(%eax)
  release(&ptable.lock);
80105843:	83 ec 0c             	sub    $0xc,%esp
80105846:	68 80 39 11 80       	push   $0x80113980
8010584b:	e8 5f 03 00 00       	call   80105baf <release>
80105850:	83 c4 10             	add    $0x10,%esp
  return 0;
80105853:	b8 00 00 00 00       	mov    $0x0,%eax
}
80105858:	c9                   	leave  
80105859:	c3                   	ret    

8010585a <getprocs>:

int
getprocs(int max, struct uproc * table)
{
8010585a:	55                   	push   %ebp
8010585b:	89 e5                	mov    %esp,%ebp
8010585d:	53                   	push   %ebx
8010585e:	83 ec 14             	sub    $0x14,%esp
  int i;				//counter for # of processes in array
  struct proc *p;			//ptable will be here

  acquire(&ptable.lock);		//lock for mutual exclusion
80105861:	83 ec 0c             	sub    $0xc,%esp
80105864:	68 80 39 11 80       	push   $0x80113980
80105869:	e8 da 02 00 00       	call   80105b48 <acquire>
8010586e:	83 c4 10             	add    $0x10,%esp
  for(i = 0, p = ptable.proc; p < &ptable.proc[NPROC] && i < max; ++p)
80105871:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80105878:	c7 45 f0 b4 39 11 80 	movl   $0x801139b4,-0x10(%ebp)
8010587f:	e9 33 02 00 00       	jmp    80105ab7 <getprocs+0x25d>
  {
    if(p->state != UNUSED){		//look for active process to be copied until max amount of processes
80105884:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105887:	8b 40 0c             	mov    0xc(%eax),%eax
8010588a:	85 c0                	test   %eax,%eax
8010588c:	0f 84 1e 02 00 00    	je     80105ab0 <getprocs+0x256>
      table[i].pid = p->pid;
80105892:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105895:	6b d0 64             	imul   $0x64,%eax,%edx
80105898:	8b 45 0c             	mov    0xc(%ebp),%eax
8010589b:	01 c2                	add    %eax,%edx
8010589d:	8b 45 f0             	mov    -0x10(%ebp),%eax
801058a0:	8b 40 10             	mov    0x10(%eax),%eax
801058a3:	89 02                	mov    %eax,(%edx)
      table[i].uid = p->uid;
801058a5:	8b 45 f4             	mov    -0xc(%ebp),%eax
801058a8:	6b d0 64             	imul   $0x64,%eax,%edx
801058ab:	8b 45 0c             	mov    0xc(%ebp),%eax
801058ae:	01 c2                	add    %eax,%edx
801058b0:	8b 45 f0             	mov    -0x10(%ebp),%eax
801058b3:	8b 80 84 00 00 00    	mov    0x84(%eax),%eax
801058b9:	89 42 04             	mov    %eax,0x4(%edx)
      table[i].gid = p->gid;
801058bc:	8b 45 f4             	mov    -0xc(%ebp),%eax
801058bf:	6b d0 64             	imul   $0x64,%eax,%edx
801058c2:	8b 45 0c             	mov    0xc(%ebp),%eax
801058c5:	01 c2                	add    %eax,%edx
801058c7:	8b 45 f0             	mov    -0x10(%ebp),%eax
801058ca:	8b 80 80 00 00 00    	mov    0x80(%eax),%eax
801058d0:	89 42 08             	mov    %eax,0x8(%edx)
      table[i].ppid = p->parent != 0 ? p->parent->pid : 1;
801058d3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801058d6:	6b d0 64             	imul   $0x64,%eax,%edx
801058d9:	8b 45 0c             	mov    0xc(%ebp),%eax
801058dc:	01 c2                	add    %eax,%edx
801058de:	8b 45 f0             	mov    -0x10(%ebp),%eax
801058e1:	8b 40 14             	mov    0x14(%eax),%eax
801058e4:	85 c0                	test   %eax,%eax
801058e6:	74 0b                	je     801058f3 <getprocs+0x99>
801058e8:	8b 45 f0             	mov    -0x10(%ebp),%eax
801058eb:	8b 40 14             	mov    0x14(%eax),%eax
801058ee:	8b 40 10             	mov    0x10(%eax),%eax
801058f1:	eb 05                	jmp    801058f8 <getprocs+0x9e>
801058f3:	b8 01 00 00 00       	mov    $0x1,%eax
801058f8:	89 42 0c             	mov    %eax,0xc(%edx)
      table[i].size = p->sz;
801058fb:	8b 45 f4             	mov    -0xc(%ebp),%eax
801058fe:	6b d0 64             	imul   $0x64,%eax,%edx
80105901:	8b 45 0c             	mov    0xc(%ebp),%eax
80105904:	01 c2                	add    %eax,%edx
80105906:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105909:	8b 00                	mov    (%eax),%eax
8010590b:	89 42 40             	mov    %eax,0x40(%edx)
      safestrcpy(table[i].name, p->name, sizeof(p->name));
8010590e:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105911:	8d 50 6c             	lea    0x6c(%eax),%edx
80105914:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105917:	6b c8 64             	imul   $0x64,%eax,%ecx
8010591a:	8b 45 0c             	mov    0xc(%ebp),%eax
8010591d:	01 c8                	add    %ecx,%eax
8010591f:	83 c0 44             	add    $0x44,%eax
80105922:	83 ec 04             	sub    $0x4,%esp
80105925:	6a 10                	push   $0x10
80105927:	52                   	push   %edx
80105928:	50                   	push   %eax
80105929:	e8 80 06 00 00       	call   80105fae <safestrcpy>
8010592e:	83 c4 10             	add    $0x10,%esp
      table[i].elapsed_ticks = ((ticks)-(p->start_ticks)) / 100;
80105931:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105934:	6b d0 64             	imul   $0x64,%eax,%edx
80105937:	8b 45 0c             	mov    0xc(%ebp),%eax
8010593a:	8d 0c 02             	lea    (%edx,%eax,1),%ecx
8010593d:	8b 15 e0 67 11 80    	mov    0x801167e0,%edx
80105943:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105946:	8b 40 7c             	mov    0x7c(%eax),%eax
80105949:	29 c2                	sub    %eax,%edx
8010594b:	89 d0                	mov    %edx,%eax
8010594d:	ba 1f 85 eb 51       	mov    $0x51eb851f,%edx
80105952:	f7 e2                	mul    %edx
80105954:	89 d0                	mov    %edx,%eax
80105956:	c1 e8 05             	shr    $0x5,%eax
80105959:	89 41 10             	mov    %eax,0x10(%ecx)
      table[i].elapsed_ticks1 = ((ticks)-(p->start_ticks)) % 100;
8010595c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010595f:	6b d0 64             	imul   $0x64,%eax,%edx
80105962:	8b 45 0c             	mov    0xc(%ebp),%eax
80105965:	8d 1c 02             	lea    (%edx,%eax,1),%ebx
80105968:	8b 15 e0 67 11 80    	mov    0x801167e0,%edx
8010596e:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105971:	8b 40 7c             	mov    0x7c(%eax),%eax
80105974:	89 d1                	mov    %edx,%ecx
80105976:	29 c1                	sub    %eax,%ecx
80105978:	ba 1f 85 eb 51       	mov    $0x51eb851f,%edx
8010597d:	89 c8                	mov    %ecx,%eax
8010597f:	f7 e2                	mul    %edx
80105981:	89 d0                	mov    %edx,%eax
80105983:	c1 e8 05             	shr    $0x5,%eax
80105986:	6b c0 64             	imul   $0x64,%eax,%eax
80105989:	29 c1                	sub    %eax,%ecx
8010598b:	89 c8                	mov    %ecx,%eax
8010598d:	89 43 14             	mov    %eax,0x14(%ebx)
      table[i].CPU_total_ticks = p->cpu_ticks_total /100;
80105990:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105993:	6b d0 64             	imul   $0x64,%eax,%edx
80105996:	8b 45 0c             	mov    0xc(%ebp),%eax
80105999:	8d 0c 02             	lea    (%edx,%eax,1),%ecx
8010599c:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010599f:	8b 80 88 00 00 00    	mov    0x88(%eax),%eax
801059a5:	ba 1f 85 eb 51       	mov    $0x51eb851f,%edx
801059aa:	f7 e2                	mul    %edx
801059ac:	89 d0                	mov    %edx,%eax
801059ae:	c1 e8 05             	shr    $0x5,%eax
801059b1:	89 41 18             	mov    %eax,0x18(%ecx)
      table[i].CPU_total_ticks1 = p->cpu_ticks_total %100;
801059b4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801059b7:	6b d0 64             	imul   $0x64,%eax,%edx
801059ba:	8b 45 0c             	mov    0xc(%ebp),%eax
801059bd:	8d 1c 02             	lea    (%edx,%eax,1),%ebx
801059c0:	8b 45 f0             	mov    -0x10(%ebp),%eax
801059c3:	8b 88 88 00 00 00    	mov    0x88(%eax),%ecx
801059c9:	ba 1f 85 eb 51       	mov    $0x51eb851f,%edx
801059ce:	89 c8                	mov    %ecx,%eax
801059d0:	f7 e2                	mul    %edx
801059d2:	89 d0                	mov    %edx,%eax
801059d4:	c1 e8 05             	shr    $0x5,%eax
801059d7:	6b c0 64             	imul   $0x64,%eax,%eax
801059da:	29 c1                	sub    %eax,%ecx
801059dc:	89 c8                	mov    %ecx,%eax
801059de:	89 43 1c             	mov    %eax,0x1c(%ebx)

      switch(p->state){			//switch statement for process state
801059e1:	8b 45 f0             	mov    -0x10(%ebp),%eax
801059e4:	8b 40 0c             	mov    0xc(%eax),%eax
801059e7:	83 f8 05             	cmp    $0x5,%eax
801059ea:	0f 87 bc 00 00 00    	ja     80105aac <getprocs+0x252>
801059f0:	8b 04 85 98 97 10 80 	mov    -0x7fef6868(,%eax,4),%eax
801059f7:	ff e0                	jmp    *%eax
        case UNUSED:
          break;
 	case EMBRYO:
	  safestrcpy(table[i].state, "embryo", sizeof("embryo")); break;
801059f9:	8b 45 f4             	mov    -0xc(%ebp),%eax
801059fc:	6b d0 64             	imul   $0x64,%eax,%edx
801059ff:	8b 45 0c             	mov    0xc(%ebp),%eax
80105a02:	01 d0                	add    %edx,%eax
80105a04:	83 c0 20             	add    $0x20,%eax
80105a07:	83 ec 04             	sub    $0x4,%esp
80105a0a:	6a 07                	push   $0x7
80105a0c:	68 a0 96 10 80       	push   $0x801096a0
80105a11:	50                   	push   %eax
80105a12:	e8 97 05 00 00       	call   80105fae <safestrcpy>
80105a17:	83 c4 10             	add    $0x10,%esp
80105a1a:	e9 8d 00 00 00       	jmp    80105aac <getprocs+0x252>
 	case SLEEPING:
	  safestrcpy(table[i].state, "sleep", sizeof("sleep")); break;
80105a1f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105a22:	6b d0 64             	imul   $0x64,%eax,%edx
80105a25:	8b 45 0c             	mov    0xc(%ebp),%eax
80105a28:	01 d0                	add    %edx,%eax
80105a2a:	83 c0 20             	add    $0x20,%eax
80105a2d:	83 ec 04             	sub    $0x4,%esp
80105a30:	6a 06                	push   $0x6
80105a32:	68 93 96 10 80       	push   $0x80109693
80105a37:	50                   	push   %eax
80105a38:	e8 71 05 00 00       	call   80105fae <safestrcpy>
80105a3d:	83 c4 10             	add    $0x10,%esp
80105a40:	eb 6a                	jmp    80105aac <getprocs+0x252>
 	case RUNNABLE:
	  safestrcpy(table[i].state, "ready", sizeof("ready")); break;
80105a42:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105a45:	6b d0 64             	imul   $0x64,%eax,%edx
80105a48:	8b 45 0c             	mov    0xc(%ebp),%eax
80105a4b:	01 d0                	add    %edx,%eax
80105a4d:	83 c0 20             	add    $0x20,%eax
80105a50:	83 ec 04             	sub    $0x4,%esp
80105a53:	6a 06                	push   $0x6
80105a55:	68 8b 97 10 80       	push   $0x8010978b
80105a5a:	50                   	push   %eax
80105a5b:	e8 4e 05 00 00       	call   80105fae <safestrcpy>
80105a60:	83 c4 10             	add    $0x10,%esp
80105a63:	eb 47                	jmp    80105aac <getprocs+0x252>
 	case RUNNING:
	  safestrcpy(table[i].state, "run", sizeof("run")); break;
80105a65:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105a68:	6b d0 64             	imul   $0x64,%eax,%edx
80105a6b:	8b 45 0c             	mov    0xc(%ebp),%eax
80105a6e:	01 d0                	add    %edx,%eax
80105a70:	83 c0 20             	add    $0x20,%eax
80105a73:	83 ec 04             	sub    $0x4,%esp
80105a76:	6a 04                	push   $0x4
80105a78:	68 91 97 10 80       	push   $0x80109791
80105a7d:	50                   	push   %eax
80105a7e:	e8 2b 05 00 00       	call   80105fae <safestrcpy>
80105a83:	83 c4 10             	add    $0x10,%esp
80105a86:	eb 24                	jmp    80105aac <getprocs+0x252>
 	case ZOMBIE:
	  safestrcpy(table[i].state, "zombie", sizeof("zombie")); break;
80105a88:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105a8b:	6b d0 64             	imul   $0x64,%eax,%edx
80105a8e:	8b 45 0c             	mov    0xc(%ebp),%eax
80105a91:	01 d0                	add    %edx,%eax
80105a93:	83 c0 20             	add    $0x20,%eax
80105a96:	83 ec 04             	sub    $0x4,%esp
80105a99:	6a 07                	push   $0x7
80105a9b:	68 bc 96 10 80       	push   $0x801096bc
80105aa0:	50                   	push   %eax
80105aa1:	e8 08 05 00 00       	call   80105fae <safestrcpy>
80105aa6:	83 c4 10             	add    $0x10,%esp
80105aa9:	eb 01                	jmp    80105aac <getprocs+0x252>
      table[i].CPU_total_ticks = p->cpu_ticks_total /100;
      table[i].CPU_total_ticks1 = p->cpu_ticks_total %100;

      switch(p->state){			//switch statement for process state
        case UNUSED:
          break;
80105aab:	90                   	nop
 	case RUNNING:
	  safestrcpy(table[i].state, "run", sizeof("run")); break;
 	case ZOMBIE:
	  safestrcpy(table[i].state, "zombie", sizeof("zombie")); break;
      }
      ++i;
80105aac:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
{
  int i;				//counter for # of processes in array
  struct proc *p;			//ptable will be here

  acquire(&ptable.lock);		//lock for mutual exclusion
  for(i = 0, p = ptable.proc; p < &ptable.proc[NPROC] && i < max; ++p)
80105ab0:	81 45 f0 98 00 00 00 	addl   $0x98,-0x10(%ebp)
80105ab7:	81 7d f0 b4 5f 11 80 	cmpl   $0x80115fb4,-0x10(%ebp)
80105abe:	73 0c                	jae    80105acc <getprocs+0x272>
80105ac0:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105ac3:	3b 45 08             	cmp    0x8(%ebp),%eax
80105ac6:	0f 8c b8 fd ff ff    	jl     80105884 <getprocs+0x2a>
	  safestrcpy(table[i].state, "zombie", sizeof("zombie")); break;
      }
      ++i;
    }    
  }
  release(&ptable.lock);		//release lock
80105acc:	83 ec 0c             	sub    $0xc,%esp
80105acf:	68 80 39 11 80       	push   $0x80113980
80105ad4:	e8 d6 00 00 00       	call   80105baf <release>
80105ad9:	83 c4 10             	add    $0x10,%esp
  return i;				//return the number of proccesses in the uproc struct
80105adc:	8b 45 f4             	mov    -0xc(%ebp),%eax
} 
80105adf:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80105ae2:	c9                   	leave  
80105ae3:	c3                   	ret    

80105ae4 <setpriority>:
int
setpriority(int pid, int priority)
{
80105ae4:	55                   	push   %ebp
80105ae5:	89 e5                	mov    %esp,%ebp
  return 1;
80105ae7:	b8 01 00 00 00       	mov    $0x1,%eax
}
80105aec:	5d                   	pop    %ebp
80105aed:	c3                   	ret    

80105aee <readeflags>:
  asm volatile("ltr %0" : : "r" (sel));
}

static inline uint
readeflags(void)
{
80105aee:	55                   	push   %ebp
80105aef:	89 e5                	mov    %esp,%ebp
80105af1:	83 ec 10             	sub    $0x10,%esp
  uint eflags;
  asm volatile("pushfl; popl %0" : "=r" (eflags));
80105af4:	9c                   	pushf  
80105af5:	58                   	pop    %eax
80105af6:	89 45 fc             	mov    %eax,-0x4(%ebp)
  return eflags;
80105af9:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80105afc:	c9                   	leave  
80105afd:	c3                   	ret    

80105afe <cli>:
  asm volatile("movw %0, %%gs" : : "r" (v));
}

static inline void
cli(void)
{
80105afe:	55                   	push   %ebp
80105aff:	89 e5                	mov    %esp,%ebp
  asm volatile("cli");
80105b01:	fa                   	cli    
}
80105b02:	90                   	nop
80105b03:	5d                   	pop    %ebp
80105b04:	c3                   	ret    

80105b05 <sti>:

static inline void
sti(void)
{
80105b05:	55                   	push   %ebp
80105b06:	89 e5                	mov    %esp,%ebp
  asm volatile("sti");
80105b08:	fb                   	sti    
}
80105b09:	90                   	nop
80105b0a:	5d                   	pop    %ebp
80105b0b:	c3                   	ret    

80105b0c <xchg>:

static inline uint
xchg(volatile uint *addr, uint newval)
{
80105b0c:	55                   	push   %ebp
80105b0d:	89 e5                	mov    %esp,%ebp
80105b0f:	83 ec 10             	sub    $0x10,%esp
  uint result;
  
  // The + in "+m" denotes a read-modify-write operand.
  asm volatile("lock; xchgl %0, %1" :
80105b12:	8b 55 08             	mov    0x8(%ebp),%edx
80105b15:	8b 45 0c             	mov    0xc(%ebp),%eax
80105b18:	8b 4d 08             	mov    0x8(%ebp),%ecx
80105b1b:	f0 87 02             	lock xchg %eax,(%edx)
80105b1e:	89 45 fc             	mov    %eax,-0x4(%ebp)
               "+m" (*addr), "=a" (result) :
               "1" (newval) :
               "cc");
  return result;
80105b21:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80105b24:	c9                   	leave  
80105b25:	c3                   	ret    

80105b26 <initlock>:
#include "proc.h"
#include "spinlock.h"

void
initlock(struct spinlock *lk, char *name)
{
80105b26:	55                   	push   %ebp
80105b27:	89 e5                	mov    %esp,%ebp
  lk->name = name;
80105b29:	8b 45 08             	mov    0x8(%ebp),%eax
80105b2c:	8b 55 0c             	mov    0xc(%ebp),%edx
80105b2f:	89 50 04             	mov    %edx,0x4(%eax)
  lk->locked = 0;
80105b32:	8b 45 08             	mov    0x8(%ebp),%eax
80105b35:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
  lk->cpu = 0;
80105b3b:	8b 45 08             	mov    0x8(%ebp),%eax
80105b3e:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
}
80105b45:	90                   	nop
80105b46:	5d                   	pop    %ebp
80105b47:	c3                   	ret    

80105b48 <acquire>:
// Loops (spins) until the lock is acquired.
// Holding a lock for a long time may cause
// other CPUs to waste time spinning to acquire it.
void
acquire(struct spinlock *lk)
{
80105b48:	55                   	push   %ebp
80105b49:	89 e5                	mov    %esp,%ebp
80105b4b:	83 ec 08             	sub    $0x8,%esp
  pushcli(); // disable interrupts to avoid deadlock.
80105b4e:	e8 52 01 00 00       	call   80105ca5 <pushcli>
  if(holding(lk))
80105b53:	8b 45 08             	mov    0x8(%ebp),%eax
80105b56:	83 ec 0c             	sub    $0xc,%esp
80105b59:	50                   	push   %eax
80105b5a:	e8 1c 01 00 00       	call   80105c7b <holding>
80105b5f:	83 c4 10             	add    $0x10,%esp
80105b62:	85 c0                	test   %eax,%eax
80105b64:	74 0d                	je     80105b73 <acquire+0x2b>
    panic("acquire");
80105b66:	83 ec 0c             	sub    $0xc,%esp
80105b69:	68 b0 97 10 80       	push   $0x801097b0
80105b6e:	e8 f3 a9 ff ff       	call   80100566 <panic>

  // The xchg is atomic.
  // It also serializes, so that reads after acquire are not
  // reordered before it. 
  while(xchg(&lk->locked, 1) != 0)
80105b73:	90                   	nop
80105b74:	8b 45 08             	mov    0x8(%ebp),%eax
80105b77:	83 ec 08             	sub    $0x8,%esp
80105b7a:	6a 01                	push   $0x1
80105b7c:	50                   	push   %eax
80105b7d:	e8 8a ff ff ff       	call   80105b0c <xchg>
80105b82:	83 c4 10             	add    $0x10,%esp
80105b85:	85 c0                	test   %eax,%eax
80105b87:	75 eb                	jne    80105b74 <acquire+0x2c>
    ;

  // Record info about lock acquisition for debugging.
  lk->cpu = cpu;
80105b89:	8b 45 08             	mov    0x8(%ebp),%eax
80105b8c:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
80105b93:	89 50 08             	mov    %edx,0x8(%eax)
  getcallerpcs(&lk, lk->pcs);
80105b96:	8b 45 08             	mov    0x8(%ebp),%eax
80105b99:	83 c0 0c             	add    $0xc,%eax
80105b9c:	83 ec 08             	sub    $0x8,%esp
80105b9f:	50                   	push   %eax
80105ba0:	8d 45 08             	lea    0x8(%ebp),%eax
80105ba3:	50                   	push   %eax
80105ba4:	e8 58 00 00 00       	call   80105c01 <getcallerpcs>
80105ba9:	83 c4 10             	add    $0x10,%esp
}
80105bac:	90                   	nop
80105bad:	c9                   	leave  
80105bae:	c3                   	ret    

80105baf <release>:

// Release the lock.
void
release(struct spinlock *lk)
{
80105baf:	55                   	push   %ebp
80105bb0:	89 e5                	mov    %esp,%ebp
80105bb2:	83 ec 08             	sub    $0x8,%esp
  if(!holding(lk))
80105bb5:	83 ec 0c             	sub    $0xc,%esp
80105bb8:	ff 75 08             	pushl  0x8(%ebp)
80105bbb:	e8 bb 00 00 00       	call   80105c7b <holding>
80105bc0:	83 c4 10             	add    $0x10,%esp
80105bc3:	85 c0                	test   %eax,%eax
80105bc5:	75 0d                	jne    80105bd4 <release+0x25>
    panic("release");
80105bc7:	83 ec 0c             	sub    $0xc,%esp
80105bca:	68 b8 97 10 80       	push   $0x801097b8
80105bcf:	e8 92 a9 ff ff       	call   80100566 <panic>

  lk->pcs[0] = 0;
80105bd4:	8b 45 08             	mov    0x8(%ebp),%eax
80105bd7:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
  lk->cpu = 0;
80105bde:	8b 45 08             	mov    0x8(%ebp),%eax
80105be1:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
  // But the 2007 Intel 64 Architecture Memory Ordering White
  // Paper says that Intel 64 and IA-32 will not move a load
  // after a store. So lock->locked = 0 would work here.
  // The xchg being asm volatile ensures gcc emits it after
  // the above assignments (and after the critical section).
  xchg(&lk->locked, 0);
80105be8:	8b 45 08             	mov    0x8(%ebp),%eax
80105beb:	83 ec 08             	sub    $0x8,%esp
80105bee:	6a 00                	push   $0x0
80105bf0:	50                   	push   %eax
80105bf1:	e8 16 ff ff ff       	call   80105b0c <xchg>
80105bf6:	83 c4 10             	add    $0x10,%esp

  popcli();
80105bf9:	e8 ec 00 00 00       	call   80105cea <popcli>
}
80105bfe:	90                   	nop
80105bff:	c9                   	leave  
80105c00:	c3                   	ret    

80105c01 <getcallerpcs>:

// Record the current call stack in pcs[] by following the %ebp chain.
void
getcallerpcs(void *v, uint pcs[])
{
80105c01:	55                   	push   %ebp
80105c02:	89 e5                	mov    %esp,%ebp
80105c04:	83 ec 10             	sub    $0x10,%esp
  uint *ebp;
  int i;
  
  ebp = (uint*)v - 2;
80105c07:	8b 45 08             	mov    0x8(%ebp),%eax
80105c0a:	83 e8 08             	sub    $0x8,%eax
80105c0d:	89 45 fc             	mov    %eax,-0x4(%ebp)
  for(i = 0; i < 10; i++){
80105c10:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%ebp)
80105c17:	eb 38                	jmp    80105c51 <getcallerpcs+0x50>
    if(ebp == 0 || ebp < (uint*)KERNBASE || ebp == (uint*)0xffffffff)
80105c19:	83 7d fc 00          	cmpl   $0x0,-0x4(%ebp)
80105c1d:	74 53                	je     80105c72 <getcallerpcs+0x71>
80105c1f:	81 7d fc ff ff ff 7f 	cmpl   $0x7fffffff,-0x4(%ebp)
80105c26:	76 4a                	jbe    80105c72 <getcallerpcs+0x71>
80105c28:	83 7d fc ff          	cmpl   $0xffffffff,-0x4(%ebp)
80105c2c:	74 44                	je     80105c72 <getcallerpcs+0x71>
      break;
    pcs[i] = ebp[1];     // saved %eip
80105c2e:	8b 45 f8             	mov    -0x8(%ebp),%eax
80105c31:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
80105c38:	8b 45 0c             	mov    0xc(%ebp),%eax
80105c3b:	01 c2                	add    %eax,%edx
80105c3d:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105c40:	8b 40 04             	mov    0x4(%eax),%eax
80105c43:	89 02                	mov    %eax,(%edx)
    ebp = (uint*)ebp[0]; // saved %ebp
80105c45:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105c48:	8b 00                	mov    (%eax),%eax
80105c4a:	89 45 fc             	mov    %eax,-0x4(%ebp)
{
  uint *ebp;
  int i;
  
  ebp = (uint*)v - 2;
  for(i = 0; i < 10; i++){
80105c4d:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
80105c51:	83 7d f8 09          	cmpl   $0x9,-0x8(%ebp)
80105c55:	7e c2                	jle    80105c19 <getcallerpcs+0x18>
    if(ebp == 0 || ebp < (uint*)KERNBASE || ebp == (uint*)0xffffffff)
      break;
    pcs[i] = ebp[1];     // saved %eip
    ebp = (uint*)ebp[0]; // saved %ebp
  }
  for(; i < 10; i++)
80105c57:	eb 19                	jmp    80105c72 <getcallerpcs+0x71>
    pcs[i] = 0;
80105c59:	8b 45 f8             	mov    -0x8(%ebp),%eax
80105c5c:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
80105c63:	8b 45 0c             	mov    0xc(%ebp),%eax
80105c66:	01 d0                	add    %edx,%eax
80105c68:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
    if(ebp == 0 || ebp < (uint*)KERNBASE || ebp == (uint*)0xffffffff)
      break;
    pcs[i] = ebp[1];     // saved %eip
    ebp = (uint*)ebp[0]; // saved %ebp
  }
  for(; i < 10; i++)
80105c6e:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
80105c72:	83 7d f8 09          	cmpl   $0x9,-0x8(%ebp)
80105c76:	7e e1                	jle    80105c59 <getcallerpcs+0x58>
    pcs[i] = 0;
}
80105c78:	90                   	nop
80105c79:	c9                   	leave  
80105c7a:	c3                   	ret    

80105c7b <holding>:

// Check whether this cpu is holding the lock.
int
holding(struct spinlock *lock)
{
80105c7b:	55                   	push   %ebp
80105c7c:	89 e5                	mov    %esp,%ebp
  return lock->locked && lock->cpu == cpu;
80105c7e:	8b 45 08             	mov    0x8(%ebp),%eax
80105c81:	8b 00                	mov    (%eax),%eax
80105c83:	85 c0                	test   %eax,%eax
80105c85:	74 17                	je     80105c9e <holding+0x23>
80105c87:	8b 45 08             	mov    0x8(%ebp),%eax
80105c8a:	8b 50 08             	mov    0x8(%eax),%edx
80105c8d:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80105c93:	39 c2                	cmp    %eax,%edx
80105c95:	75 07                	jne    80105c9e <holding+0x23>
80105c97:	b8 01 00 00 00       	mov    $0x1,%eax
80105c9c:	eb 05                	jmp    80105ca3 <holding+0x28>
80105c9e:	b8 00 00 00 00       	mov    $0x0,%eax
}
80105ca3:	5d                   	pop    %ebp
80105ca4:	c3                   	ret    

80105ca5 <pushcli>:
// it takes two popcli to undo two pushcli.  Also, if interrupts
// are off, then pushcli, popcli leaves them off.

void
pushcli(void)
{
80105ca5:	55                   	push   %ebp
80105ca6:	89 e5                	mov    %esp,%ebp
80105ca8:	83 ec 10             	sub    $0x10,%esp
  int eflags;
  
  eflags = readeflags();
80105cab:	e8 3e fe ff ff       	call   80105aee <readeflags>
80105cb0:	89 45 fc             	mov    %eax,-0x4(%ebp)
  cli();
80105cb3:	e8 46 fe ff ff       	call   80105afe <cli>
  if(cpu->ncli++ == 0)
80105cb8:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
80105cbf:	8b 82 ac 00 00 00    	mov    0xac(%edx),%eax
80105cc5:	8d 48 01             	lea    0x1(%eax),%ecx
80105cc8:	89 8a ac 00 00 00    	mov    %ecx,0xac(%edx)
80105cce:	85 c0                	test   %eax,%eax
80105cd0:	75 15                	jne    80105ce7 <pushcli+0x42>
    cpu->intena = eflags & FL_IF;
80105cd2:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80105cd8:	8b 55 fc             	mov    -0x4(%ebp),%edx
80105cdb:	81 e2 00 02 00 00    	and    $0x200,%edx
80105ce1:	89 90 b0 00 00 00    	mov    %edx,0xb0(%eax)
}
80105ce7:	90                   	nop
80105ce8:	c9                   	leave  
80105ce9:	c3                   	ret    

80105cea <popcli>:

void
popcli(void)
{
80105cea:	55                   	push   %ebp
80105ceb:	89 e5                	mov    %esp,%ebp
80105ced:	83 ec 08             	sub    $0x8,%esp
  if(readeflags()&FL_IF)
80105cf0:	e8 f9 fd ff ff       	call   80105aee <readeflags>
80105cf5:	25 00 02 00 00       	and    $0x200,%eax
80105cfa:	85 c0                	test   %eax,%eax
80105cfc:	74 0d                	je     80105d0b <popcli+0x21>
    panic("popcli - interruptible");
80105cfe:	83 ec 0c             	sub    $0xc,%esp
80105d01:	68 c0 97 10 80       	push   $0x801097c0
80105d06:	e8 5b a8 ff ff       	call   80100566 <panic>
  if(--cpu->ncli < 0)
80105d0b:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80105d11:	8b 90 ac 00 00 00    	mov    0xac(%eax),%edx
80105d17:	83 ea 01             	sub    $0x1,%edx
80105d1a:	89 90 ac 00 00 00    	mov    %edx,0xac(%eax)
80105d20:	8b 80 ac 00 00 00    	mov    0xac(%eax),%eax
80105d26:	85 c0                	test   %eax,%eax
80105d28:	79 0d                	jns    80105d37 <popcli+0x4d>
    panic("popcli");
80105d2a:	83 ec 0c             	sub    $0xc,%esp
80105d2d:	68 d7 97 10 80       	push   $0x801097d7
80105d32:	e8 2f a8 ff ff       	call   80100566 <panic>
  if(cpu->ncli == 0 && cpu->intena)
80105d37:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80105d3d:	8b 80 ac 00 00 00    	mov    0xac(%eax),%eax
80105d43:	85 c0                	test   %eax,%eax
80105d45:	75 15                	jne    80105d5c <popcli+0x72>
80105d47:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80105d4d:	8b 80 b0 00 00 00    	mov    0xb0(%eax),%eax
80105d53:	85 c0                	test   %eax,%eax
80105d55:	74 05                	je     80105d5c <popcli+0x72>
    sti();
80105d57:	e8 a9 fd ff ff       	call   80105b05 <sti>
}
80105d5c:	90                   	nop
80105d5d:	c9                   	leave  
80105d5e:	c3                   	ret    

80105d5f <stosb>:
               "cc");
}

static inline void
stosb(void *addr, int data, int cnt)
{
80105d5f:	55                   	push   %ebp
80105d60:	89 e5                	mov    %esp,%ebp
80105d62:	57                   	push   %edi
80105d63:	53                   	push   %ebx
  asm volatile("cld; rep stosb" :
80105d64:	8b 4d 08             	mov    0x8(%ebp),%ecx
80105d67:	8b 55 10             	mov    0x10(%ebp),%edx
80105d6a:	8b 45 0c             	mov    0xc(%ebp),%eax
80105d6d:	89 cb                	mov    %ecx,%ebx
80105d6f:	89 df                	mov    %ebx,%edi
80105d71:	89 d1                	mov    %edx,%ecx
80105d73:	fc                   	cld    
80105d74:	f3 aa                	rep stos %al,%es:(%edi)
80105d76:	89 ca                	mov    %ecx,%edx
80105d78:	89 fb                	mov    %edi,%ebx
80105d7a:	89 5d 08             	mov    %ebx,0x8(%ebp)
80105d7d:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
80105d80:	90                   	nop
80105d81:	5b                   	pop    %ebx
80105d82:	5f                   	pop    %edi
80105d83:	5d                   	pop    %ebp
80105d84:	c3                   	ret    

80105d85 <stosl>:

static inline void
stosl(void *addr, int data, int cnt)
{
80105d85:	55                   	push   %ebp
80105d86:	89 e5                	mov    %esp,%ebp
80105d88:	57                   	push   %edi
80105d89:	53                   	push   %ebx
  asm volatile("cld; rep stosl" :
80105d8a:	8b 4d 08             	mov    0x8(%ebp),%ecx
80105d8d:	8b 55 10             	mov    0x10(%ebp),%edx
80105d90:	8b 45 0c             	mov    0xc(%ebp),%eax
80105d93:	89 cb                	mov    %ecx,%ebx
80105d95:	89 df                	mov    %ebx,%edi
80105d97:	89 d1                	mov    %edx,%ecx
80105d99:	fc                   	cld    
80105d9a:	f3 ab                	rep stos %eax,%es:(%edi)
80105d9c:	89 ca                	mov    %ecx,%edx
80105d9e:	89 fb                	mov    %edi,%ebx
80105da0:	89 5d 08             	mov    %ebx,0x8(%ebp)
80105da3:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
80105da6:	90                   	nop
80105da7:	5b                   	pop    %ebx
80105da8:	5f                   	pop    %edi
80105da9:	5d                   	pop    %ebp
80105daa:	c3                   	ret    

80105dab <memset>:
#include "types.h"
#include "x86.h"

void*
memset(void *dst, int c, uint n)
{
80105dab:	55                   	push   %ebp
80105dac:	89 e5                	mov    %esp,%ebp
  if ((int)dst%4 == 0 && n%4 == 0){
80105dae:	8b 45 08             	mov    0x8(%ebp),%eax
80105db1:	83 e0 03             	and    $0x3,%eax
80105db4:	85 c0                	test   %eax,%eax
80105db6:	75 43                	jne    80105dfb <memset+0x50>
80105db8:	8b 45 10             	mov    0x10(%ebp),%eax
80105dbb:	83 e0 03             	and    $0x3,%eax
80105dbe:	85 c0                	test   %eax,%eax
80105dc0:	75 39                	jne    80105dfb <memset+0x50>
    c &= 0xFF;
80105dc2:	81 65 0c ff 00 00 00 	andl   $0xff,0xc(%ebp)
    stosl(dst, (c<<24)|(c<<16)|(c<<8)|c, n/4);
80105dc9:	8b 45 10             	mov    0x10(%ebp),%eax
80105dcc:	c1 e8 02             	shr    $0x2,%eax
80105dcf:	89 c1                	mov    %eax,%ecx
80105dd1:	8b 45 0c             	mov    0xc(%ebp),%eax
80105dd4:	c1 e0 18             	shl    $0x18,%eax
80105dd7:	89 c2                	mov    %eax,%edx
80105dd9:	8b 45 0c             	mov    0xc(%ebp),%eax
80105ddc:	c1 e0 10             	shl    $0x10,%eax
80105ddf:	09 c2                	or     %eax,%edx
80105de1:	8b 45 0c             	mov    0xc(%ebp),%eax
80105de4:	c1 e0 08             	shl    $0x8,%eax
80105de7:	09 d0                	or     %edx,%eax
80105de9:	0b 45 0c             	or     0xc(%ebp),%eax
80105dec:	51                   	push   %ecx
80105ded:	50                   	push   %eax
80105dee:	ff 75 08             	pushl  0x8(%ebp)
80105df1:	e8 8f ff ff ff       	call   80105d85 <stosl>
80105df6:	83 c4 0c             	add    $0xc,%esp
80105df9:	eb 12                	jmp    80105e0d <memset+0x62>
  } else
    stosb(dst, c, n);
80105dfb:	8b 45 10             	mov    0x10(%ebp),%eax
80105dfe:	50                   	push   %eax
80105dff:	ff 75 0c             	pushl  0xc(%ebp)
80105e02:	ff 75 08             	pushl  0x8(%ebp)
80105e05:	e8 55 ff ff ff       	call   80105d5f <stosb>
80105e0a:	83 c4 0c             	add    $0xc,%esp
  return dst;
80105e0d:	8b 45 08             	mov    0x8(%ebp),%eax
}
80105e10:	c9                   	leave  
80105e11:	c3                   	ret    

80105e12 <memcmp>:

int
memcmp(const void *v1, const void *v2, uint n)
{
80105e12:	55                   	push   %ebp
80105e13:	89 e5                	mov    %esp,%ebp
80105e15:	83 ec 10             	sub    $0x10,%esp
  const uchar *s1, *s2;
  
  s1 = v1;
80105e18:	8b 45 08             	mov    0x8(%ebp),%eax
80105e1b:	89 45 fc             	mov    %eax,-0x4(%ebp)
  s2 = v2;
80105e1e:	8b 45 0c             	mov    0xc(%ebp),%eax
80105e21:	89 45 f8             	mov    %eax,-0x8(%ebp)
  while(n-- > 0){
80105e24:	eb 30                	jmp    80105e56 <memcmp+0x44>
    if(*s1 != *s2)
80105e26:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105e29:	0f b6 10             	movzbl (%eax),%edx
80105e2c:	8b 45 f8             	mov    -0x8(%ebp),%eax
80105e2f:	0f b6 00             	movzbl (%eax),%eax
80105e32:	38 c2                	cmp    %al,%dl
80105e34:	74 18                	je     80105e4e <memcmp+0x3c>
      return *s1 - *s2;
80105e36:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105e39:	0f b6 00             	movzbl (%eax),%eax
80105e3c:	0f b6 d0             	movzbl %al,%edx
80105e3f:	8b 45 f8             	mov    -0x8(%ebp),%eax
80105e42:	0f b6 00             	movzbl (%eax),%eax
80105e45:	0f b6 c0             	movzbl %al,%eax
80105e48:	29 c2                	sub    %eax,%edx
80105e4a:	89 d0                	mov    %edx,%eax
80105e4c:	eb 1a                	jmp    80105e68 <memcmp+0x56>
    s1++, s2++;
80105e4e:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
80105e52:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
{
  const uchar *s1, *s2;
  
  s1 = v1;
  s2 = v2;
  while(n-- > 0){
80105e56:	8b 45 10             	mov    0x10(%ebp),%eax
80105e59:	8d 50 ff             	lea    -0x1(%eax),%edx
80105e5c:	89 55 10             	mov    %edx,0x10(%ebp)
80105e5f:	85 c0                	test   %eax,%eax
80105e61:	75 c3                	jne    80105e26 <memcmp+0x14>
    if(*s1 != *s2)
      return *s1 - *s2;
    s1++, s2++;
  }

  return 0;
80105e63:	b8 00 00 00 00       	mov    $0x0,%eax
}
80105e68:	c9                   	leave  
80105e69:	c3                   	ret    

80105e6a <memmove>:

void*
memmove(void *dst, const void *src, uint n)
{
80105e6a:	55                   	push   %ebp
80105e6b:	89 e5                	mov    %esp,%ebp
80105e6d:	83 ec 10             	sub    $0x10,%esp
  const char *s;
  char *d;

  s = src;
80105e70:	8b 45 0c             	mov    0xc(%ebp),%eax
80105e73:	89 45 fc             	mov    %eax,-0x4(%ebp)
  d = dst;
80105e76:	8b 45 08             	mov    0x8(%ebp),%eax
80105e79:	89 45 f8             	mov    %eax,-0x8(%ebp)
  if(s < d && s + n > d){
80105e7c:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105e7f:	3b 45 f8             	cmp    -0x8(%ebp),%eax
80105e82:	73 54                	jae    80105ed8 <memmove+0x6e>
80105e84:	8b 55 fc             	mov    -0x4(%ebp),%edx
80105e87:	8b 45 10             	mov    0x10(%ebp),%eax
80105e8a:	01 d0                	add    %edx,%eax
80105e8c:	3b 45 f8             	cmp    -0x8(%ebp),%eax
80105e8f:	76 47                	jbe    80105ed8 <memmove+0x6e>
    s += n;
80105e91:	8b 45 10             	mov    0x10(%ebp),%eax
80105e94:	01 45 fc             	add    %eax,-0x4(%ebp)
    d += n;
80105e97:	8b 45 10             	mov    0x10(%ebp),%eax
80105e9a:	01 45 f8             	add    %eax,-0x8(%ebp)
    while(n-- > 0)
80105e9d:	eb 13                	jmp    80105eb2 <memmove+0x48>
      *--d = *--s;
80105e9f:	83 6d f8 01          	subl   $0x1,-0x8(%ebp)
80105ea3:	83 6d fc 01          	subl   $0x1,-0x4(%ebp)
80105ea7:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105eaa:	0f b6 10             	movzbl (%eax),%edx
80105ead:	8b 45 f8             	mov    -0x8(%ebp),%eax
80105eb0:	88 10                	mov    %dl,(%eax)
  s = src;
  d = dst;
  if(s < d && s + n > d){
    s += n;
    d += n;
    while(n-- > 0)
80105eb2:	8b 45 10             	mov    0x10(%ebp),%eax
80105eb5:	8d 50 ff             	lea    -0x1(%eax),%edx
80105eb8:	89 55 10             	mov    %edx,0x10(%ebp)
80105ebb:	85 c0                	test   %eax,%eax
80105ebd:	75 e0                	jne    80105e9f <memmove+0x35>
  const char *s;
  char *d;

  s = src;
  d = dst;
  if(s < d && s + n > d){
80105ebf:	eb 24                	jmp    80105ee5 <memmove+0x7b>
    d += n;
    while(n-- > 0)
      *--d = *--s;
  } else
    while(n-- > 0)
      *d++ = *s++;
80105ec1:	8b 45 f8             	mov    -0x8(%ebp),%eax
80105ec4:	8d 50 01             	lea    0x1(%eax),%edx
80105ec7:	89 55 f8             	mov    %edx,-0x8(%ebp)
80105eca:	8b 55 fc             	mov    -0x4(%ebp),%edx
80105ecd:	8d 4a 01             	lea    0x1(%edx),%ecx
80105ed0:	89 4d fc             	mov    %ecx,-0x4(%ebp)
80105ed3:	0f b6 12             	movzbl (%edx),%edx
80105ed6:	88 10                	mov    %dl,(%eax)
    s += n;
    d += n;
    while(n-- > 0)
      *--d = *--s;
  } else
    while(n-- > 0)
80105ed8:	8b 45 10             	mov    0x10(%ebp),%eax
80105edb:	8d 50 ff             	lea    -0x1(%eax),%edx
80105ede:	89 55 10             	mov    %edx,0x10(%ebp)
80105ee1:	85 c0                	test   %eax,%eax
80105ee3:	75 dc                	jne    80105ec1 <memmove+0x57>
      *d++ = *s++;

  return dst;
80105ee5:	8b 45 08             	mov    0x8(%ebp),%eax
}
80105ee8:	c9                   	leave  
80105ee9:	c3                   	ret    

80105eea <memcpy>:

// memcpy exists to placate GCC.  Use memmove.
void*
memcpy(void *dst, const void *src, uint n)
{
80105eea:	55                   	push   %ebp
80105eeb:	89 e5                	mov    %esp,%ebp
  return memmove(dst, src, n);
80105eed:	ff 75 10             	pushl  0x10(%ebp)
80105ef0:	ff 75 0c             	pushl  0xc(%ebp)
80105ef3:	ff 75 08             	pushl  0x8(%ebp)
80105ef6:	e8 6f ff ff ff       	call   80105e6a <memmove>
80105efb:	83 c4 0c             	add    $0xc,%esp
}
80105efe:	c9                   	leave  
80105eff:	c3                   	ret    

80105f00 <strncmp>:

int
strncmp(const char *p, const char *q, uint n)
{
80105f00:	55                   	push   %ebp
80105f01:	89 e5                	mov    %esp,%ebp
  while(n > 0 && *p && *p == *q)
80105f03:	eb 0c                	jmp    80105f11 <strncmp+0x11>
    n--, p++, q++;
80105f05:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
80105f09:	83 45 08 01          	addl   $0x1,0x8(%ebp)
80105f0d:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strncmp(const char *p, const char *q, uint n)
{
  while(n > 0 && *p && *p == *q)
80105f11:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80105f15:	74 1a                	je     80105f31 <strncmp+0x31>
80105f17:	8b 45 08             	mov    0x8(%ebp),%eax
80105f1a:	0f b6 00             	movzbl (%eax),%eax
80105f1d:	84 c0                	test   %al,%al
80105f1f:	74 10                	je     80105f31 <strncmp+0x31>
80105f21:	8b 45 08             	mov    0x8(%ebp),%eax
80105f24:	0f b6 10             	movzbl (%eax),%edx
80105f27:	8b 45 0c             	mov    0xc(%ebp),%eax
80105f2a:	0f b6 00             	movzbl (%eax),%eax
80105f2d:	38 c2                	cmp    %al,%dl
80105f2f:	74 d4                	je     80105f05 <strncmp+0x5>
    n--, p++, q++;
  if(n == 0)
80105f31:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80105f35:	75 07                	jne    80105f3e <strncmp+0x3e>
    return 0;
80105f37:	b8 00 00 00 00       	mov    $0x0,%eax
80105f3c:	eb 16                	jmp    80105f54 <strncmp+0x54>
  return (uchar)*p - (uchar)*q;
80105f3e:	8b 45 08             	mov    0x8(%ebp),%eax
80105f41:	0f b6 00             	movzbl (%eax),%eax
80105f44:	0f b6 d0             	movzbl %al,%edx
80105f47:	8b 45 0c             	mov    0xc(%ebp),%eax
80105f4a:	0f b6 00             	movzbl (%eax),%eax
80105f4d:	0f b6 c0             	movzbl %al,%eax
80105f50:	29 c2                	sub    %eax,%edx
80105f52:	89 d0                	mov    %edx,%eax
}
80105f54:	5d                   	pop    %ebp
80105f55:	c3                   	ret    

80105f56 <strncpy>:

char*
strncpy(char *s, const char *t, int n)
{
80105f56:	55                   	push   %ebp
80105f57:	89 e5                	mov    %esp,%ebp
80105f59:	83 ec 10             	sub    $0x10,%esp
  char *os;
  
  os = s;
80105f5c:	8b 45 08             	mov    0x8(%ebp),%eax
80105f5f:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while(n-- > 0 && (*s++ = *t++) != 0)
80105f62:	90                   	nop
80105f63:	8b 45 10             	mov    0x10(%ebp),%eax
80105f66:	8d 50 ff             	lea    -0x1(%eax),%edx
80105f69:	89 55 10             	mov    %edx,0x10(%ebp)
80105f6c:	85 c0                	test   %eax,%eax
80105f6e:	7e 2c                	jle    80105f9c <strncpy+0x46>
80105f70:	8b 45 08             	mov    0x8(%ebp),%eax
80105f73:	8d 50 01             	lea    0x1(%eax),%edx
80105f76:	89 55 08             	mov    %edx,0x8(%ebp)
80105f79:	8b 55 0c             	mov    0xc(%ebp),%edx
80105f7c:	8d 4a 01             	lea    0x1(%edx),%ecx
80105f7f:	89 4d 0c             	mov    %ecx,0xc(%ebp)
80105f82:	0f b6 12             	movzbl (%edx),%edx
80105f85:	88 10                	mov    %dl,(%eax)
80105f87:	0f b6 00             	movzbl (%eax),%eax
80105f8a:	84 c0                	test   %al,%al
80105f8c:	75 d5                	jne    80105f63 <strncpy+0xd>
    ;
  while(n-- > 0)
80105f8e:	eb 0c                	jmp    80105f9c <strncpy+0x46>
    *s++ = 0;
80105f90:	8b 45 08             	mov    0x8(%ebp),%eax
80105f93:	8d 50 01             	lea    0x1(%eax),%edx
80105f96:	89 55 08             	mov    %edx,0x8(%ebp)
80105f99:	c6 00 00             	movb   $0x0,(%eax)
  char *os;
  
  os = s;
  while(n-- > 0 && (*s++ = *t++) != 0)
    ;
  while(n-- > 0)
80105f9c:	8b 45 10             	mov    0x10(%ebp),%eax
80105f9f:	8d 50 ff             	lea    -0x1(%eax),%edx
80105fa2:	89 55 10             	mov    %edx,0x10(%ebp)
80105fa5:	85 c0                	test   %eax,%eax
80105fa7:	7f e7                	jg     80105f90 <strncpy+0x3a>
    *s++ = 0;
  return os;
80105fa9:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80105fac:	c9                   	leave  
80105fad:	c3                   	ret    

80105fae <safestrcpy>:

// Like strncpy but guaranteed to NUL-terminate.
char*
safestrcpy(char *s, const char *t, int n)
{
80105fae:	55                   	push   %ebp
80105faf:	89 e5                	mov    %esp,%ebp
80105fb1:	83 ec 10             	sub    $0x10,%esp
  char *os;
  
  os = s;
80105fb4:	8b 45 08             	mov    0x8(%ebp),%eax
80105fb7:	89 45 fc             	mov    %eax,-0x4(%ebp)
  if(n <= 0)
80105fba:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80105fbe:	7f 05                	jg     80105fc5 <safestrcpy+0x17>
    return os;
80105fc0:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105fc3:	eb 31                	jmp    80105ff6 <safestrcpy+0x48>
  while(--n > 0 && (*s++ = *t++) != 0)
80105fc5:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
80105fc9:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80105fcd:	7e 1e                	jle    80105fed <safestrcpy+0x3f>
80105fcf:	8b 45 08             	mov    0x8(%ebp),%eax
80105fd2:	8d 50 01             	lea    0x1(%eax),%edx
80105fd5:	89 55 08             	mov    %edx,0x8(%ebp)
80105fd8:	8b 55 0c             	mov    0xc(%ebp),%edx
80105fdb:	8d 4a 01             	lea    0x1(%edx),%ecx
80105fde:	89 4d 0c             	mov    %ecx,0xc(%ebp)
80105fe1:	0f b6 12             	movzbl (%edx),%edx
80105fe4:	88 10                	mov    %dl,(%eax)
80105fe6:	0f b6 00             	movzbl (%eax),%eax
80105fe9:	84 c0                	test   %al,%al
80105feb:	75 d8                	jne    80105fc5 <safestrcpy+0x17>
    ;
  *s = 0;
80105fed:	8b 45 08             	mov    0x8(%ebp),%eax
80105ff0:	c6 00 00             	movb   $0x0,(%eax)
  return os;
80105ff3:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80105ff6:	c9                   	leave  
80105ff7:	c3                   	ret    

80105ff8 <strlen>:

int
strlen(const char *s)
{
80105ff8:	55                   	push   %ebp
80105ff9:	89 e5                	mov    %esp,%ebp
80105ffb:	83 ec 10             	sub    $0x10,%esp
  int n;

  for(n = 0; s[n]; n++)
80105ffe:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
80106005:	eb 04                	jmp    8010600b <strlen+0x13>
80106007:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
8010600b:	8b 55 fc             	mov    -0x4(%ebp),%edx
8010600e:	8b 45 08             	mov    0x8(%ebp),%eax
80106011:	01 d0                	add    %edx,%eax
80106013:	0f b6 00             	movzbl (%eax),%eax
80106016:	84 c0                	test   %al,%al
80106018:	75 ed                	jne    80106007 <strlen+0xf>
    ;
  return n;
8010601a:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
8010601d:	c9                   	leave  
8010601e:	c3                   	ret    

8010601f <swtch>:
# Save current register context in old
# and then load register context from new.

.globl swtch
swtch:
  movl 4(%esp), %eax
8010601f:	8b 44 24 04          	mov    0x4(%esp),%eax
  movl 8(%esp), %edx
80106023:	8b 54 24 08          	mov    0x8(%esp),%edx

  # Save old callee-save registers
  pushl %ebp
80106027:	55                   	push   %ebp
  pushl %ebx
80106028:	53                   	push   %ebx
  pushl %esi
80106029:	56                   	push   %esi
  pushl %edi
8010602a:	57                   	push   %edi

  # Switch stacks
  movl %esp, (%eax)
8010602b:	89 20                	mov    %esp,(%eax)
  movl %edx, %esp
8010602d:	89 d4                	mov    %edx,%esp

  # Load new callee-save registers
  popl %edi
8010602f:	5f                   	pop    %edi
  popl %esi
80106030:	5e                   	pop    %esi
  popl %ebx
80106031:	5b                   	pop    %ebx
  popl %ebp
80106032:	5d                   	pop    %ebp
  ret
80106033:	c3                   	ret    

80106034 <fetchint>:
// to a saved program counter, and then the first argument.

// Fetch the int at addr from the current process.
int
fetchint(uint addr, int *ip)
{
80106034:	55                   	push   %ebp
80106035:	89 e5                	mov    %esp,%ebp
  if(addr >= proc->sz || addr+4 > proc->sz)
80106037:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010603d:	8b 00                	mov    (%eax),%eax
8010603f:	3b 45 08             	cmp    0x8(%ebp),%eax
80106042:	76 12                	jbe    80106056 <fetchint+0x22>
80106044:	8b 45 08             	mov    0x8(%ebp),%eax
80106047:	8d 50 04             	lea    0x4(%eax),%edx
8010604a:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106050:	8b 00                	mov    (%eax),%eax
80106052:	39 c2                	cmp    %eax,%edx
80106054:	76 07                	jbe    8010605d <fetchint+0x29>
    return -1;
80106056:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010605b:	eb 0f                	jmp    8010606c <fetchint+0x38>
  *ip = *(int*)(addr);
8010605d:	8b 45 08             	mov    0x8(%ebp),%eax
80106060:	8b 10                	mov    (%eax),%edx
80106062:	8b 45 0c             	mov    0xc(%ebp),%eax
80106065:	89 10                	mov    %edx,(%eax)
  return 0;
80106067:	b8 00 00 00 00       	mov    $0x0,%eax
}
8010606c:	5d                   	pop    %ebp
8010606d:	c3                   	ret    

8010606e <fetchstr>:
// Fetch the nul-terminated string at addr from the current process.
// Doesn't actually copy the string - just sets *pp to point at it.
// Returns length of string, not including nul.
int
fetchstr(uint addr, char **pp)
{
8010606e:	55                   	push   %ebp
8010606f:	89 e5                	mov    %esp,%ebp
80106071:	83 ec 10             	sub    $0x10,%esp
  char *s, *ep;

  if(addr >= proc->sz)
80106074:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010607a:	8b 00                	mov    (%eax),%eax
8010607c:	3b 45 08             	cmp    0x8(%ebp),%eax
8010607f:	77 07                	ja     80106088 <fetchstr+0x1a>
    return -1;
80106081:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106086:	eb 46                	jmp    801060ce <fetchstr+0x60>
  *pp = (char*)addr;
80106088:	8b 55 08             	mov    0x8(%ebp),%edx
8010608b:	8b 45 0c             	mov    0xc(%ebp),%eax
8010608e:	89 10                	mov    %edx,(%eax)
  ep = (char*)proc->sz;
80106090:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106096:	8b 00                	mov    (%eax),%eax
80106098:	89 45 f8             	mov    %eax,-0x8(%ebp)
  for(s = *pp; s < ep; s++)
8010609b:	8b 45 0c             	mov    0xc(%ebp),%eax
8010609e:	8b 00                	mov    (%eax),%eax
801060a0:	89 45 fc             	mov    %eax,-0x4(%ebp)
801060a3:	eb 1c                	jmp    801060c1 <fetchstr+0x53>
    if(*s == 0)
801060a5:	8b 45 fc             	mov    -0x4(%ebp),%eax
801060a8:	0f b6 00             	movzbl (%eax),%eax
801060ab:	84 c0                	test   %al,%al
801060ad:	75 0e                	jne    801060bd <fetchstr+0x4f>
      return s - *pp;
801060af:	8b 55 fc             	mov    -0x4(%ebp),%edx
801060b2:	8b 45 0c             	mov    0xc(%ebp),%eax
801060b5:	8b 00                	mov    (%eax),%eax
801060b7:	29 c2                	sub    %eax,%edx
801060b9:	89 d0                	mov    %edx,%eax
801060bb:	eb 11                	jmp    801060ce <fetchstr+0x60>

  if(addr >= proc->sz)
    return -1;
  *pp = (char*)addr;
  ep = (char*)proc->sz;
  for(s = *pp; s < ep; s++)
801060bd:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
801060c1:	8b 45 fc             	mov    -0x4(%ebp),%eax
801060c4:	3b 45 f8             	cmp    -0x8(%ebp),%eax
801060c7:	72 dc                	jb     801060a5 <fetchstr+0x37>
    if(*s == 0)
      return s - *pp;
  return -1;
801060c9:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
801060ce:	c9                   	leave  
801060cf:	c3                   	ret    

801060d0 <argint>:

// Fetch the nth 32-bit system call argument.
int
argint(int n, int *ip)
{
801060d0:	55                   	push   %ebp
801060d1:	89 e5                	mov    %esp,%ebp
  return fetchint(proc->tf->esp + 4 + 4*n, ip);
801060d3:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801060d9:	8b 40 18             	mov    0x18(%eax),%eax
801060dc:	8b 40 44             	mov    0x44(%eax),%eax
801060df:	8b 55 08             	mov    0x8(%ebp),%edx
801060e2:	c1 e2 02             	shl    $0x2,%edx
801060e5:	01 d0                	add    %edx,%eax
801060e7:	83 c0 04             	add    $0x4,%eax
801060ea:	ff 75 0c             	pushl  0xc(%ebp)
801060ed:	50                   	push   %eax
801060ee:	e8 41 ff ff ff       	call   80106034 <fetchint>
801060f3:	83 c4 08             	add    $0x8,%esp
}
801060f6:	c9                   	leave  
801060f7:	c3                   	ret    

801060f8 <argptr>:
// Fetch the nth word-sized system call argument as a pointer
// to a block of memory of size n bytes.  Check that the pointer
// lies within the process address space.
int
argptr(int n, char **pp, int size)
{
801060f8:	55                   	push   %ebp
801060f9:	89 e5                	mov    %esp,%ebp
801060fb:	83 ec 10             	sub    $0x10,%esp
  int i;
  
  if(argint(n, &i) < 0)
801060fe:	8d 45 fc             	lea    -0x4(%ebp),%eax
80106101:	50                   	push   %eax
80106102:	ff 75 08             	pushl  0x8(%ebp)
80106105:	e8 c6 ff ff ff       	call   801060d0 <argint>
8010610a:	83 c4 08             	add    $0x8,%esp
8010610d:	85 c0                	test   %eax,%eax
8010610f:	79 07                	jns    80106118 <argptr+0x20>
    return -1;
80106111:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106116:	eb 3b                	jmp    80106153 <argptr+0x5b>
  if((uint)i >= proc->sz || (uint)i+size > proc->sz)
80106118:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010611e:	8b 00                	mov    (%eax),%eax
80106120:	8b 55 fc             	mov    -0x4(%ebp),%edx
80106123:	39 d0                	cmp    %edx,%eax
80106125:	76 16                	jbe    8010613d <argptr+0x45>
80106127:	8b 45 fc             	mov    -0x4(%ebp),%eax
8010612a:	89 c2                	mov    %eax,%edx
8010612c:	8b 45 10             	mov    0x10(%ebp),%eax
8010612f:	01 c2                	add    %eax,%edx
80106131:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106137:	8b 00                	mov    (%eax),%eax
80106139:	39 c2                	cmp    %eax,%edx
8010613b:	76 07                	jbe    80106144 <argptr+0x4c>
    return -1;
8010613d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106142:	eb 0f                	jmp    80106153 <argptr+0x5b>
  *pp = (char*)i;
80106144:	8b 45 fc             	mov    -0x4(%ebp),%eax
80106147:	89 c2                	mov    %eax,%edx
80106149:	8b 45 0c             	mov    0xc(%ebp),%eax
8010614c:	89 10                	mov    %edx,(%eax)
  return 0;
8010614e:	b8 00 00 00 00       	mov    $0x0,%eax
}
80106153:	c9                   	leave  
80106154:	c3                   	ret    

80106155 <argstr>:
// Check that the pointer is valid and the string is nul-terminated.
// (There is no shared writable memory, so the string can't change
// between this check and being used by the kernel.)
int
argstr(int n, char **pp)
{
80106155:	55                   	push   %ebp
80106156:	89 e5                	mov    %esp,%ebp
80106158:	83 ec 10             	sub    $0x10,%esp
  int addr;
  if(argint(n, &addr) < 0)
8010615b:	8d 45 fc             	lea    -0x4(%ebp),%eax
8010615e:	50                   	push   %eax
8010615f:	ff 75 08             	pushl  0x8(%ebp)
80106162:	e8 69 ff ff ff       	call   801060d0 <argint>
80106167:	83 c4 08             	add    $0x8,%esp
8010616a:	85 c0                	test   %eax,%eax
8010616c:	79 07                	jns    80106175 <argstr+0x20>
    return -1;
8010616e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106173:	eb 0f                	jmp    80106184 <argstr+0x2f>
  return fetchstr(addr, pp);
80106175:	8b 45 fc             	mov    -0x4(%ebp),%eax
80106178:	ff 75 0c             	pushl  0xc(%ebp)
8010617b:	50                   	push   %eax
8010617c:	e8 ed fe ff ff       	call   8010606e <fetchstr>
80106181:	83 c4 08             	add    $0x8,%esp
}
80106184:	c9                   	leave  
80106185:	c3                   	ret    

80106186 <syscall>:
"getprocs", "setpriority",
};
#endif
void
syscall(void)
{
80106186:	55                   	push   %ebp
80106187:	89 e5                	mov    %esp,%ebp
80106189:	53                   	push   %ebx
8010618a:	83 ec 14             	sub    $0x14,%esp
  int num;

  num = proc->tf->eax;
8010618d:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106193:	8b 40 18             	mov    0x18(%eax),%eax
80106196:	8b 40 1c             	mov    0x1c(%eax),%eax
80106199:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(num > 0 && num < NELEM(syscalls) && syscalls[num]) {
8010619c:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801061a0:	7e 30                	jle    801061d2 <syscall+0x4c>
801061a2:	8b 45 f4             	mov    -0xc(%ebp),%eax
801061a5:	83 f8 1e             	cmp    $0x1e,%eax
801061a8:	77 28                	ja     801061d2 <syscall+0x4c>
801061aa:	8b 45 f4             	mov    -0xc(%ebp),%eax
801061ad:	8b 04 85 40 c0 10 80 	mov    -0x7fef3fc0(,%eax,4),%eax
801061b4:	85 c0                	test   %eax,%eax
801061b6:	74 1a                	je     801061d2 <syscall+0x4c>
    proc->tf->eax = syscalls[num]();
801061b8:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801061be:	8b 58 18             	mov    0x18(%eax),%ebx
801061c1:	8b 45 f4             	mov    -0xc(%ebp),%eax
801061c4:	8b 04 85 40 c0 10 80 	mov    -0x7fef3fc0(,%eax,4),%eax
801061cb:	ff d0                	call   *%eax
801061cd:	89 43 1c             	mov    %eax,0x1c(%ebx)
801061d0:	eb 34                	jmp    80106206 <syscall+0x80>
    #ifdef PRINT_SYSCALLS
    cprintf("%s -> %d \n", syscallnames[num-1], proc->tf->eax);
    #endif
  } else {
    cprintf("%d %s: unknown sys call %d\n",
            proc->pid, proc->name, num);
801061d2:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801061d8:	8d 50 6c             	lea    0x6c(%eax),%edx
801061db:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
// some code goes here
    #ifdef PRINT_SYSCALLS
    cprintf("%s -> %d \n", syscallnames[num-1], proc->tf->eax);
    #endif
  } else {
    cprintf("%d %s: unknown sys call %d\n",
801061e1:	8b 40 10             	mov    0x10(%eax),%eax
801061e4:	ff 75 f4             	pushl  -0xc(%ebp)
801061e7:	52                   	push   %edx
801061e8:	50                   	push   %eax
801061e9:	68 de 97 10 80       	push   $0x801097de
801061ee:	e8 d3 a1 ff ff       	call   801003c6 <cprintf>
801061f3:	83 c4 10             	add    $0x10,%esp
            proc->pid, proc->name, num);
    proc->tf->eax = -1;
801061f6:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801061fc:	8b 40 18             	mov    0x18(%eax),%eax
801061ff:	c7 40 1c ff ff ff ff 	movl   $0xffffffff,0x1c(%eax)
  }
}
80106206:	90                   	nop
80106207:	8b 5d fc             	mov    -0x4(%ebp),%ebx
8010620a:	c9                   	leave  
8010620b:	c3                   	ret    

8010620c <argfd>:

// Fetch the nth word-sized system call argument as a file descriptor
// and return both the descriptor and the corresponding struct file.
static int
argfd(int n, int *pfd, struct file **pf)
{
8010620c:	55                   	push   %ebp
8010620d:	89 e5                	mov    %esp,%ebp
8010620f:	83 ec 18             	sub    $0x18,%esp
  int fd;
  struct file *f;

  if(argint(n, &fd) < 0)
80106212:	83 ec 08             	sub    $0x8,%esp
80106215:	8d 45 f0             	lea    -0x10(%ebp),%eax
80106218:	50                   	push   %eax
80106219:	ff 75 08             	pushl  0x8(%ebp)
8010621c:	e8 af fe ff ff       	call   801060d0 <argint>
80106221:	83 c4 10             	add    $0x10,%esp
80106224:	85 c0                	test   %eax,%eax
80106226:	79 07                	jns    8010622f <argfd+0x23>
    return -1;
80106228:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010622d:	eb 50                	jmp    8010627f <argfd+0x73>
  if(fd < 0 || fd >= NOFILE || (f=proc->ofile[fd]) == 0)
8010622f:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106232:	85 c0                	test   %eax,%eax
80106234:	78 21                	js     80106257 <argfd+0x4b>
80106236:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106239:	83 f8 0f             	cmp    $0xf,%eax
8010623c:	7f 19                	jg     80106257 <argfd+0x4b>
8010623e:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106244:	8b 55 f0             	mov    -0x10(%ebp),%edx
80106247:	83 c2 08             	add    $0x8,%edx
8010624a:	8b 44 90 08          	mov    0x8(%eax,%edx,4),%eax
8010624e:	89 45 f4             	mov    %eax,-0xc(%ebp)
80106251:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80106255:	75 07                	jne    8010625e <argfd+0x52>
    return -1;
80106257:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010625c:	eb 21                	jmp    8010627f <argfd+0x73>
  if(pfd)
8010625e:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
80106262:	74 08                	je     8010626c <argfd+0x60>
    *pfd = fd;
80106264:	8b 55 f0             	mov    -0x10(%ebp),%edx
80106267:	8b 45 0c             	mov    0xc(%ebp),%eax
8010626a:	89 10                	mov    %edx,(%eax)
  if(pf)
8010626c:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80106270:	74 08                	je     8010627a <argfd+0x6e>
    *pf = f;
80106272:	8b 45 10             	mov    0x10(%ebp),%eax
80106275:	8b 55 f4             	mov    -0xc(%ebp),%edx
80106278:	89 10                	mov    %edx,(%eax)
  return 0;
8010627a:	b8 00 00 00 00       	mov    $0x0,%eax
}
8010627f:	c9                   	leave  
80106280:	c3                   	ret    

80106281 <fdalloc>:

// Allocate a file descriptor for the given file.
// Takes over file reference from caller on success.
static int
fdalloc(struct file *f)
{
80106281:	55                   	push   %ebp
80106282:	89 e5                	mov    %esp,%ebp
80106284:	83 ec 10             	sub    $0x10,%esp
  int fd;

  for(fd = 0; fd < NOFILE; fd++){
80106287:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
8010628e:	eb 30                	jmp    801062c0 <fdalloc+0x3f>
    if(proc->ofile[fd] == 0){
80106290:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106296:	8b 55 fc             	mov    -0x4(%ebp),%edx
80106299:	83 c2 08             	add    $0x8,%edx
8010629c:	8b 44 90 08          	mov    0x8(%eax,%edx,4),%eax
801062a0:	85 c0                	test   %eax,%eax
801062a2:	75 18                	jne    801062bc <fdalloc+0x3b>
      proc->ofile[fd] = f;
801062a4:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801062aa:	8b 55 fc             	mov    -0x4(%ebp),%edx
801062ad:	8d 4a 08             	lea    0x8(%edx),%ecx
801062b0:	8b 55 08             	mov    0x8(%ebp),%edx
801062b3:	89 54 88 08          	mov    %edx,0x8(%eax,%ecx,4)
      return fd;
801062b7:	8b 45 fc             	mov    -0x4(%ebp),%eax
801062ba:	eb 0f                	jmp    801062cb <fdalloc+0x4a>
static int
fdalloc(struct file *f)
{
  int fd;

  for(fd = 0; fd < NOFILE; fd++){
801062bc:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
801062c0:	83 7d fc 0f          	cmpl   $0xf,-0x4(%ebp)
801062c4:	7e ca                	jle    80106290 <fdalloc+0xf>
    if(proc->ofile[fd] == 0){
      proc->ofile[fd] = f;
      return fd;
    }
  }
  return -1;
801062c6:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
801062cb:	c9                   	leave  
801062cc:	c3                   	ret    

801062cd <sys_dup>:

int
sys_dup(void)
{
801062cd:	55                   	push   %ebp
801062ce:	89 e5                	mov    %esp,%ebp
801062d0:	83 ec 18             	sub    $0x18,%esp
  struct file *f;
  int fd;
  
  if(argfd(0, 0, &f) < 0)
801062d3:	83 ec 04             	sub    $0x4,%esp
801062d6:	8d 45 f0             	lea    -0x10(%ebp),%eax
801062d9:	50                   	push   %eax
801062da:	6a 00                	push   $0x0
801062dc:	6a 00                	push   $0x0
801062de:	e8 29 ff ff ff       	call   8010620c <argfd>
801062e3:	83 c4 10             	add    $0x10,%esp
801062e6:	85 c0                	test   %eax,%eax
801062e8:	79 07                	jns    801062f1 <sys_dup+0x24>
    return -1;
801062ea:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801062ef:	eb 31                	jmp    80106322 <sys_dup+0x55>
  if((fd=fdalloc(f)) < 0)
801062f1:	8b 45 f0             	mov    -0x10(%ebp),%eax
801062f4:	83 ec 0c             	sub    $0xc,%esp
801062f7:	50                   	push   %eax
801062f8:	e8 84 ff ff ff       	call   80106281 <fdalloc>
801062fd:	83 c4 10             	add    $0x10,%esp
80106300:	89 45 f4             	mov    %eax,-0xc(%ebp)
80106303:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80106307:	79 07                	jns    80106310 <sys_dup+0x43>
    return -1;
80106309:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010630e:	eb 12                	jmp    80106322 <sys_dup+0x55>
  filedup(f);
80106310:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106313:	83 ec 0c             	sub    $0xc,%esp
80106316:	50                   	push   %eax
80106317:	e8 82 ad ff ff       	call   8010109e <filedup>
8010631c:	83 c4 10             	add    $0x10,%esp
  return fd;
8010631f:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
80106322:	c9                   	leave  
80106323:	c3                   	ret    

80106324 <sys_read>:

int
sys_read(void)
{
80106324:	55                   	push   %ebp
80106325:	89 e5                	mov    %esp,%ebp
80106327:	83 ec 18             	sub    $0x18,%esp
  struct file *f;
  int n;
  char *p;

  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
8010632a:	83 ec 04             	sub    $0x4,%esp
8010632d:	8d 45 f4             	lea    -0xc(%ebp),%eax
80106330:	50                   	push   %eax
80106331:	6a 00                	push   $0x0
80106333:	6a 00                	push   $0x0
80106335:	e8 d2 fe ff ff       	call   8010620c <argfd>
8010633a:	83 c4 10             	add    $0x10,%esp
8010633d:	85 c0                	test   %eax,%eax
8010633f:	78 2e                	js     8010636f <sys_read+0x4b>
80106341:	83 ec 08             	sub    $0x8,%esp
80106344:	8d 45 f0             	lea    -0x10(%ebp),%eax
80106347:	50                   	push   %eax
80106348:	6a 02                	push   $0x2
8010634a:	e8 81 fd ff ff       	call   801060d0 <argint>
8010634f:	83 c4 10             	add    $0x10,%esp
80106352:	85 c0                	test   %eax,%eax
80106354:	78 19                	js     8010636f <sys_read+0x4b>
80106356:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106359:	83 ec 04             	sub    $0x4,%esp
8010635c:	50                   	push   %eax
8010635d:	8d 45 ec             	lea    -0x14(%ebp),%eax
80106360:	50                   	push   %eax
80106361:	6a 01                	push   $0x1
80106363:	e8 90 fd ff ff       	call   801060f8 <argptr>
80106368:	83 c4 10             	add    $0x10,%esp
8010636b:	85 c0                	test   %eax,%eax
8010636d:	79 07                	jns    80106376 <sys_read+0x52>
    return -1;
8010636f:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106374:	eb 17                	jmp    8010638d <sys_read+0x69>
  return fileread(f, p, n);
80106376:	8b 4d f0             	mov    -0x10(%ebp),%ecx
80106379:	8b 55 ec             	mov    -0x14(%ebp),%edx
8010637c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010637f:	83 ec 04             	sub    $0x4,%esp
80106382:	51                   	push   %ecx
80106383:	52                   	push   %edx
80106384:	50                   	push   %eax
80106385:	e8 a4 ae ff ff       	call   8010122e <fileread>
8010638a:	83 c4 10             	add    $0x10,%esp
}
8010638d:	c9                   	leave  
8010638e:	c3                   	ret    

8010638f <sys_write>:

int
sys_write(void)
{
8010638f:	55                   	push   %ebp
80106390:	89 e5                	mov    %esp,%ebp
80106392:	83 ec 18             	sub    $0x18,%esp
  struct file *f;
  int n;
  char *p;

  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
80106395:	83 ec 04             	sub    $0x4,%esp
80106398:	8d 45 f4             	lea    -0xc(%ebp),%eax
8010639b:	50                   	push   %eax
8010639c:	6a 00                	push   $0x0
8010639e:	6a 00                	push   $0x0
801063a0:	e8 67 fe ff ff       	call   8010620c <argfd>
801063a5:	83 c4 10             	add    $0x10,%esp
801063a8:	85 c0                	test   %eax,%eax
801063aa:	78 2e                	js     801063da <sys_write+0x4b>
801063ac:	83 ec 08             	sub    $0x8,%esp
801063af:	8d 45 f0             	lea    -0x10(%ebp),%eax
801063b2:	50                   	push   %eax
801063b3:	6a 02                	push   $0x2
801063b5:	e8 16 fd ff ff       	call   801060d0 <argint>
801063ba:	83 c4 10             	add    $0x10,%esp
801063bd:	85 c0                	test   %eax,%eax
801063bf:	78 19                	js     801063da <sys_write+0x4b>
801063c1:	8b 45 f0             	mov    -0x10(%ebp),%eax
801063c4:	83 ec 04             	sub    $0x4,%esp
801063c7:	50                   	push   %eax
801063c8:	8d 45 ec             	lea    -0x14(%ebp),%eax
801063cb:	50                   	push   %eax
801063cc:	6a 01                	push   $0x1
801063ce:	e8 25 fd ff ff       	call   801060f8 <argptr>
801063d3:	83 c4 10             	add    $0x10,%esp
801063d6:	85 c0                	test   %eax,%eax
801063d8:	79 07                	jns    801063e1 <sys_write+0x52>
    return -1;
801063da:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801063df:	eb 17                	jmp    801063f8 <sys_write+0x69>
  return filewrite(f, p, n);
801063e1:	8b 4d f0             	mov    -0x10(%ebp),%ecx
801063e4:	8b 55 ec             	mov    -0x14(%ebp),%edx
801063e7:	8b 45 f4             	mov    -0xc(%ebp),%eax
801063ea:	83 ec 04             	sub    $0x4,%esp
801063ed:	51                   	push   %ecx
801063ee:	52                   	push   %edx
801063ef:	50                   	push   %eax
801063f0:	e8 f1 ae ff ff       	call   801012e6 <filewrite>
801063f5:	83 c4 10             	add    $0x10,%esp
}
801063f8:	c9                   	leave  
801063f9:	c3                   	ret    

801063fa <sys_close>:

int
sys_close(void)
{
801063fa:	55                   	push   %ebp
801063fb:	89 e5                	mov    %esp,%ebp
801063fd:	83 ec 18             	sub    $0x18,%esp
  int fd;
  struct file *f;
  
  if(argfd(0, &fd, &f) < 0)
80106400:	83 ec 04             	sub    $0x4,%esp
80106403:	8d 45 f0             	lea    -0x10(%ebp),%eax
80106406:	50                   	push   %eax
80106407:	8d 45 f4             	lea    -0xc(%ebp),%eax
8010640a:	50                   	push   %eax
8010640b:	6a 00                	push   $0x0
8010640d:	e8 fa fd ff ff       	call   8010620c <argfd>
80106412:	83 c4 10             	add    $0x10,%esp
80106415:	85 c0                	test   %eax,%eax
80106417:	79 07                	jns    80106420 <sys_close+0x26>
    return -1;
80106419:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010641e:	eb 28                	jmp    80106448 <sys_close+0x4e>
  proc->ofile[fd] = 0;
80106420:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106426:	8b 55 f4             	mov    -0xc(%ebp),%edx
80106429:	83 c2 08             	add    $0x8,%edx
8010642c:	c7 44 90 08 00 00 00 	movl   $0x0,0x8(%eax,%edx,4)
80106433:	00 
  fileclose(f);
80106434:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106437:	83 ec 0c             	sub    $0xc,%esp
8010643a:	50                   	push   %eax
8010643b:	e8 af ac ff ff       	call   801010ef <fileclose>
80106440:	83 c4 10             	add    $0x10,%esp
  return 0;
80106443:	b8 00 00 00 00       	mov    $0x0,%eax
}
80106448:	c9                   	leave  
80106449:	c3                   	ret    

8010644a <sys_fstat>:

int
sys_fstat(void)
{
8010644a:	55                   	push   %ebp
8010644b:	89 e5                	mov    %esp,%ebp
8010644d:	83 ec 18             	sub    $0x18,%esp
  struct file *f;
  struct stat *st;
  
  if(argfd(0, 0, &f) < 0 || argptr(1, (void*)&st, sizeof(*st)) < 0)
80106450:	83 ec 04             	sub    $0x4,%esp
80106453:	8d 45 f4             	lea    -0xc(%ebp),%eax
80106456:	50                   	push   %eax
80106457:	6a 00                	push   $0x0
80106459:	6a 00                	push   $0x0
8010645b:	e8 ac fd ff ff       	call   8010620c <argfd>
80106460:	83 c4 10             	add    $0x10,%esp
80106463:	85 c0                	test   %eax,%eax
80106465:	78 17                	js     8010647e <sys_fstat+0x34>
80106467:	83 ec 04             	sub    $0x4,%esp
8010646a:	6a 14                	push   $0x14
8010646c:	8d 45 f0             	lea    -0x10(%ebp),%eax
8010646f:	50                   	push   %eax
80106470:	6a 01                	push   $0x1
80106472:	e8 81 fc ff ff       	call   801060f8 <argptr>
80106477:	83 c4 10             	add    $0x10,%esp
8010647a:	85 c0                	test   %eax,%eax
8010647c:	79 07                	jns    80106485 <sys_fstat+0x3b>
    return -1;
8010647e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106483:	eb 13                	jmp    80106498 <sys_fstat+0x4e>
  return filestat(f, st);
80106485:	8b 55 f0             	mov    -0x10(%ebp),%edx
80106488:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010648b:	83 ec 08             	sub    $0x8,%esp
8010648e:	52                   	push   %edx
8010648f:	50                   	push   %eax
80106490:	e8 42 ad ff ff       	call   801011d7 <filestat>
80106495:	83 c4 10             	add    $0x10,%esp
}
80106498:	c9                   	leave  
80106499:	c3                   	ret    

8010649a <sys_link>:

// Create the path new as a link to the same inode as old.
int
sys_link(void)
{
8010649a:	55                   	push   %ebp
8010649b:	89 e5                	mov    %esp,%ebp
8010649d:	83 ec 28             	sub    $0x28,%esp
  char name[DIRSIZ], *new, *old;
  struct inode *dp, *ip;

  if(argstr(0, &old) < 0 || argstr(1, &new) < 0)
801064a0:	83 ec 08             	sub    $0x8,%esp
801064a3:	8d 45 d8             	lea    -0x28(%ebp),%eax
801064a6:	50                   	push   %eax
801064a7:	6a 00                	push   $0x0
801064a9:	e8 a7 fc ff ff       	call   80106155 <argstr>
801064ae:	83 c4 10             	add    $0x10,%esp
801064b1:	85 c0                	test   %eax,%eax
801064b3:	78 15                	js     801064ca <sys_link+0x30>
801064b5:	83 ec 08             	sub    $0x8,%esp
801064b8:	8d 45 dc             	lea    -0x24(%ebp),%eax
801064bb:	50                   	push   %eax
801064bc:	6a 01                	push   $0x1
801064be:	e8 92 fc ff ff       	call   80106155 <argstr>
801064c3:	83 c4 10             	add    $0x10,%esp
801064c6:	85 c0                	test   %eax,%eax
801064c8:	79 0a                	jns    801064d4 <sys_link+0x3a>
    return -1;
801064ca:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801064cf:	e9 68 01 00 00       	jmp    8010663c <sys_link+0x1a2>

  begin_op();
801064d4:	e8 12 d1 ff ff       	call   801035eb <begin_op>
  if((ip = namei(old)) == 0){
801064d9:	8b 45 d8             	mov    -0x28(%ebp),%eax
801064dc:	83 ec 0c             	sub    $0xc,%esp
801064df:	50                   	push   %eax
801064e0:	e8 e1 c0 ff ff       	call   801025c6 <namei>
801064e5:	83 c4 10             	add    $0x10,%esp
801064e8:	89 45 f4             	mov    %eax,-0xc(%ebp)
801064eb:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801064ef:	75 0f                	jne    80106500 <sys_link+0x66>
    end_op();
801064f1:	e8 81 d1 ff ff       	call   80103677 <end_op>
    return -1;
801064f6:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801064fb:	e9 3c 01 00 00       	jmp    8010663c <sys_link+0x1a2>
  }

  ilock(ip);
80106500:	83 ec 0c             	sub    $0xc,%esp
80106503:	ff 75 f4             	pushl  -0xc(%ebp)
80106506:	e8 fd b4 ff ff       	call   80101a08 <ilock>
8010650b:	83 c4 10             	add    $0x10,%esp
  if(ip->type == T_DIR){
8010650e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106511:	0f b7 40 10          	movzwl 0x10(%eax),%eax
80106515:	66 83 f8 01          	cmp    $0x1,%ax
80106519:	75 1d                	jne    80106538 <sys_link+0x9e>
    iunlockput(ip);
8010651b:	83 ec 0c             	sub    $0xc,%esp
8010651e:	ff 75 f4             	pushl  -0xc(%ebp)
80106521:	e8 a2 b7 ff ff       	call   80101cc8 <iunlockput>
80106526:	83 c4 10             	add    $0x10,%esp
    end_op();
80106529:	e8 49 d1 ff ff       	call   80103677 <end_op>
    return -1;
8010652e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106533:	e9 04 01 00 00       	jmp    8010663c <sys_link+0x1a2>
  }

  ip->nlink++;
80106538:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010653b:	0f b7 40 16          	movzwl 0x16(%eax),%eax
8010653f:	83 c0 01             	add    $0x1,%eax
80106542:	89 c2                	mov    %eax,%edx
80106544:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106547:	66 89 50 16          	mov    %dx,0x16(%eax)
  iupdate(ip);
8010654b:	83 ec 0c             	sub    $0xc,%esp
8010654e:	ff 75 f4             	pushl  -0xc(%ebp)
80106551:	e8 d8 b2 ff ff       	call   8010182e <iupdate>
80106556:	83 c4 10             	add    $0x10,%esp
  iunlock(ip);
80106559:	83 ec 0c             	sub    $0xc,%esp
8010655c:	ff 75 f4             	pushl  -0xc(%ebp)
8010655f:	e8 02 b6 ff ff       	call   80101b66 <iunlock>
80106564:	83 c4 10             	add    $0x10,%esp

  if((dp = nameiparent(new, name)) == 0)
80106567:	8b 45 dc             	mov    -0x24(%ebp),%eax
8010656a:	83 ec 08             	sub    $0x8,%esp
8010656d:	8d 55 e2             	lea    -0x1e(%ebp),%edx
80106570:	52                   	push   %edx
80106571:	50                   	push   %eax
80106572:	e8 6b c0 ff ff       	call   801025e2 <nameiparent>
80106577:	83 c4 10             	add    $0x10,%esp
8010657a:	89 45 f0             	mov    %eax,-0x10(%ebp)
8010657d:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80106581:	74 71                	je     801065f4 <sys_link+0x15a>
    goto bad;
  ilock(dp);
80106583:	83 ec 0c             	sub    $0xc,%esp
80106586:	ff 75 f0             	pushl  -0x10(%ebp)
80106589:	e8 7a b4 ff ff       	call   80101a08 <ilock>
8010658e:	83 c4 10             	add    $0x10,%esp
  if(dp->dev != ip->dev || dirlink(dp, name, ip->inum) < 0){
80106591:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106594:	8b 10                	mov    (%eax),%edx
80106596:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106599:	8b 00                	mov    (%eax),%eax
8010659b:	39 c2                	cmp    %eax,%edx
8010659d:	75 1d                	jne    801065bc <sys_link+0x122>
8010659f:	8b 45 f4             	mov    -0xc(%ebp),%eax
801065a2:	8b 40 04             	mov    0x4(%eax),%eax
801065a5:	83 ec 04             	sub    $0x4,%esp
801065a8:	50                   	push   %eax
801065a9:	8d 45 e2             	lea    -0x1e(%ebp),%eax
801065ac:	50                   	push   %eax
801065ad:	ff 75 f0             	pushl  -0x10(%ebp)
801065b0:	e8 75 bd ff ff       	call   8010232a <dirlink>
801065b5:	83 c4 10             	add    $0x10,%esp
801065b8:	85 c0                	test   %eax,%eax
801065ba:	79 10                	jns    801065cc <sys_link+0x132>
    iunlockput(dp);
801065bc:	83 ec 0c             	sub    $0xc,%esp
801065bf:	ff 75 f0             	pushl  -0x10(%ebp)
801065c2:	e8 01 b7 ff ff       	call   80101cc8 <iunlockput>
801065c7:	83 c4 10             	add    $0x10,%esp
    goto bad;
801065ca:	eb 29                	jmp    801065f5 <sys_link+0x15b>
  }
  iunlockput(dp);
801065cc:	83 ec 0c             	sub    $0xc,%esp
801065cf:	ff 75 f0             	pushl  -0x10(%ebp)
801065d2:	e8 f1 b6 ff ff       	call   80101cc8 <iunlockput>
801065d7:	83 c4 10             	add    $0x10,%esp
  iput(ip);
801065da:	83 ec 0c             	sub    $0xc,%esp
801065dd:	ff 75 f4             	pushl  -0xc(%ebp)
801065e0:	e8 f3 b5 ff ff       	call   80101bd8 <iput>
801065e5:	83 c4 10             	add    $0x10,%esp

  end_op();
801065e8:	e8 8a d0 ff ff       	call   80103677 <end_op>

  return 0;
801065ed:	b8 00 00 00 00       	mov    $0x0,%eax
801065f2:	eb 48                	jmp    8010663c <sys_link+0x1a2>
  ip->nlink++;
  iupdate(ip);
  iunlock(ip);

  if((dp = nameiparent(new, name)) == 0)
    goto bad;
801065f4:	90                   	nop
  end_op();

  return 0;

bad:
  ilock(ip);
801065f5:	83 ec 0c             	sub    $0xc,%esp
801065f8:	ff 75 f4             	pushl  -0xc(%ebp)
801065fb:	e8 08 b4 ff ff       	call   80101a08 <ilock>
80106600:	83 c4 10             	add    $0x10,%esp
  ip->nlink--;
80106603:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106606:	0f b7 40 16          	movzwl 0x16(%eax),%eax
8010660a:	83 e8 01             	sub    $0x1,%eax
8010660d:	89 c2                	mov    %eax,%edx
8010660f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106612:	66 89 50 16          	mov    %dx,0x16(%eax)
  iupdate(ip);
80106616:	83 ec 0c             	sub    $0xc,%esp
80106619:	ff 75 f4             	pushl  -0xc(%ebp)
8010661c:	e8 0d b2 ff ff       	call   8010182e <iupdate>
80106621:	83 c4 10             	add    $0x10,%esp
  iunlockput(ip);
80106624:	83 ec 0c             	sub    $0xc,%esp
80106627:	ff 75 f4             	pushl  -0xc(%ebp)
8010662a:	e8 99 b6 ff ff       	call   80101cc8 <iunlockput>
8010662f:	83 c4 10             	add    $0x10,%esp
  end_op();
80106632:	e8 40 d0 ff ff       	call   80103677 <end_op>
  return -1;
80106637:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
8010663c:	c9                   	leave  
8010663d:	c3                   	ret    

8010663e <isdirempty>:

// Is the directory dp empty except for "." and ".." ?
static int
isdirempty(struct inode *dp)
{
8010663e:	55                   	push   %ebp
8010663f:	89 e5                	mov    %esp,%ebp
80106641:	83 ec 28             	sub    $0x28,%esp
  int off;
  struct dirent de;

  for(off=2*sizeof(de); off<dp->size; off+=sizeof(de)){
80106644:	c7 45 f4 20 00 00 00 	movl   $0x20,-0xc(%ebp)
8010664b:	eb 40                	jmp    8010668d <isdirempty+0x4f>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
8010664d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106650:	6a 10                	push   $0x10
80106652:	50                   	push   %eax
80106653:	8d 45 e4             	lea    -0x1c(%ebp),%eax
80106656:	50                   	push   %eax
80106657:	ff 75 08             	pushl  0x8(%ebp)
8010665a:	e8 17 b9 ff ff       	call   80101f76 <readi>
8010665f:	83 c4 10             	add    $0x10,%esp
80106662:	83 f8 10             	cmp    $0x10,%eax
80106665:	74 0d                	je     80106674 <isdirempty+0x36>
      panic("isdirempty: readi");
80106667:	83 ec 0c             	sub    $0xc,%esp
8010666a:	68 fa 97 10 80       	push   $0x801097fa
8010666f:	e8 f2 9e ff ff       	call   80100566 <panic>
    if(de.inum != 0)
80106674:	0f b7 45 e4          	movzwl -0x1c(%ebp),%eax
80106678:	66 85 c0             	test   %ax,%ax
8010667b:	74 07                	je     80106684 <isdirempty+0x46>
      return 0;
8010667d:	b8 00 00 00 00       	mov    $0x0,%eax
80106682:	eb 1b                	jmp    8010669f <isdirempty+0x61>
isdirempty(struct inode *dp)
{
  int off;
  struct dirent de;

  for(off=2*sizeof(de); off<dp->size; off+=sizeof(de)){
80106684:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106687:	83 c0 10             	add    $0x10,%eax
8010668a:	89 45 f4             	mov    %eax,-0xc(%ebp)
8010668d:	8b 45 08             	mov    0x8(%ebp),%eax
80106690:	8b 50 18             	mov    0x18(%eax),%edx
80106693:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106696:	39 c2                	cmp    %eax,%edx
80106698:	77 b3                	ja     8010664d <isdirempty+0xf>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
      panic("isdirempty: readi");
    if(de.inum != 0)
      return 0;
  }
  return 1;
8010669a:	b8 01 00 00 00       	mov    $0x1,%eax
}
8010669f:	c9                   	leave  
801066a0:	c3                   	ret    

801066a1 <sys_unlink>:

int
sys_unlink(void)
{
801066a1:	55                   	push   %ebp
801066a2:	89 e5                	mov    %esp,%ebp
801066a4:	83 ec 38             	sub    $0x38,%esp
  struct inode *ip, *dp;
  struct dirent de;
  char name[DIRSIZ], *path;
  uint off;

  if(argstr(0, &path) < 0)
801066a7:	83 ec 08             	sub    $0x8,%esp
801066aa:	8d 45 cc             	lea    -0x34(%ebp),%eax
801066ad:	50                   	push   %eax
801066ae:	6a 00                	push   $0x0
801066b0:	e8 a0 fa ff ff       	call   80106155 <argstr>
801066b5:	83 c4 10             	add    $0x10,%esp
801066b8:	85 c0                	test   %eax,%eax
801066ba:	79 0a                	jns    801066c6 <sys_unlink+0x25>
    return -1;
801066bc:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801066c1:	e9 bc 01 00 00       	jmp    80106882 <sys_unlink+0x1e1>

  begin_op();
801066c6:	e8 20 cf ff ff       	call   801035eb <begin_op>
  if((dp = nameiparent(path, name)) == 0){
801066cb:	8b 45 cc             	mov    -0x34(%ebp),%eax
801066ce:	83 ec 08             	sub    $0x8,%esp
801066d1:	8d 55 d2             	lea    -0x2e(%ebp),%edx
801066d4:	52                   	push   %edx
801066d5:	50                   	push   %eax
801066d6:	e8 07 bf ff ff       	call   801025e2 <nameiparent>
801066db:	83 c4 10             	add    $0x10,%esp
801066de:	89 45 f4             	mov    %eax,-0xc(%ebp)
801066e1:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801066e5:	75 0f                	jne    801066f6 <sys_unlink+0x55>
    end_op();
801066e7:	e8 8b cf ff ff       	call   80103677 <end_op>
    return -1;
801066ec:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801066f1:	e9 8c 01 00 00       	jmp    80106882 <sys_unlink+0x1e1>
  }

  ilock(dp);
801066f6:	83 ec 0c             	sub    $0xc,%esp
801066f9:	ff 75 f4             	pushl  -0xc(%ebp)
801066fc:	e8 07 b3 ff ff       	call   80101a08 <ilock>
80106701:	83 c4 10             	add    $0x10,%esp

  // Cannot unlink "." or "..".
  if(namecmp(name, ".") == 0 || namecmp(name, "..") == 0)
80106704:	83 ec 08             	sub    $0x8,%esp
80106707:	68 0c 98 10 80       	push   $0x8010980c
8010670c:	8d 45 d2             	lea    -0x2e(%ebp),%eax
8010670f:	50                   	push   %eax
80106710:	e8 40 bb ff ff       	call   80102255 <namecmp>
80106715:	83 c4 10             	add    $0x10,%esp
80106718:	85 c0                	test   %eax,%eax
8010671a:	0f 84 4a 01 00 00    	je     8010686a <sys_unlink+0x1c9>
80106720:	83 ec 08             	sub    $0x8,%esp
80106723:	68 0e 98 10 80       	push   $0x8010980e
80106728:	8d 45 d2             	lea    -0x2e(%ebp),%eax
8010672b:	50                   	push   %eax
8010672c:	e8 24 bb ff ff       	call   80102255 <namecmp>
80106731:	83 c4 10             	add    $0x10,%esp
80106734:	85 c0                	test   %eax,%eax
80106736:	0f 84 2e 01 00 00    	je     8010686a <sys_unlink+0x1c9>
    goto bad;

  if((ip = dirlookup(dp, name, &off)) == 0)
8010673c:	83 ec 04             	sub    $0x4,%esp
8010673f:	8d 45 c8             	lea    -0x38(%ebp),%eax
80106742:	50                   	push   %eax
80106743:	8d 45 d2             	lea    -0x2e(%ebp),%eax
80106746:	50                   	push   %eax
80106747:	ff 75 f4             	pushl  -0xc(%ebp)
8010674a:	e8 21 bb ff ff       	call   80102270 <dirlookup>
8010674f:	83 c4 10             	add    $0x10,%esp
80106752:	89 45 f0             	mov    %eax,-0x10(%ebp)
80106755:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80106759:	0f 84 0a 01 00 00    	je     80106869 <sys_unlink+0x1c8>
    goto bad;
  ilock(ip);
8010675f:	83 ec 0c             	sub    $0xc,%esp
80106762:	ff 75 f0             	pushl  -0x10(%ebp)
80106765:	e8 9e b2 ff ff       	call   80101a08 <ilock>
8010676a:	83 c4 10             	add    $0x10,%esp

  if(ip->nlink < 1)
8010676d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106770:	0f b7 40 16          	movzwl 0x16(%eax),%eax
80106774:	66 85 c0             	test   %ax,%ax
80106777:	7f 0d                	jg     80106786 <sys_unlink+0xe5>
    panic("unlink: nlink < 1");
80106779:	83 ec 0c             	sub    $0xc,%esp
8010677c:	68 11 98 10 80       	push   $0x80109811
80106781:	e8 e0 9d ff ff       	call   80100566 <panic>
  if(ip->type == T_DIR && !isdirempty(ip)){
80106786:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106789:	0f b7 40 10          	movzwl 0x10(%eax),%eax
8010678d:	66 83 f8 01          	cmp    $0x1,%ax
80106791:	75 25                	jne    801067b8 <sys_unlink+0x117>
80106793:	83 ec 0c             	sub    $0xc,%esp
80106796:	ff 75 f0             	pushl  -0x10(%ebp)
80106799:	e8 a0 fe ff ff       	call   8010663e <isdirempty>
8010679e:	83 c4 10             	add    $0x10,%esp
801067a1:	85 c0                	test   %eax,%eax
801067a3:	75 13                	jne    801067b8 <sys_unlink+0x117>
    iunlockput(ip);
801067a5:	83 ec 0c             	sub    $0xc,%esp
801067a8:	ff 75 f0             	pushl  -0x10(%ebp)
801067ab:	e8 18 b5 ff ff       	call   80101cc8 <iunlockput>
801067b0:	83 c4 10             	add    $0x10,%esp
    goto bad;
801067b3:	e9 b2 00 00 00       	jmp    8010686a <sys_unlink+0x1c9>
  }

  memset(&de, 0, sizeof(de));
801067b8:	83 ec 04             	sub    $0x4,%esp
801067bb:	6a 10                	push   $0x10
801067bd:	6a 00                	push   $0x0
801067bf:	8d 45 e0             	lea    -0x20(%ebp),%eax
801067c2:	50                   	push   %eax
801067c3:	e8 e3 f5 ff ff       	call   80105dab <memset>
801067c8:	83 c4 10             	add    $0x10,%esp
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
801067cb:	8b 45 c8             	mov    -0x38(%ebp),%eax
801067ce:	6a 10                	push   $0x10
801067d0:	50                   	push   %eax
801067d1:	8d 45 e0             	lea    -0x20(%ebp),%eax
801067d4:	50                   	push   %eax
801067d5:	ff 75 f4             	pushl  -0xc(%ebp)
801067d8:	e8 f0 b8 ff ff       	call   801020cd <writei>
801067dd:	83 c4 10             	add    $0x10,%esp
801067e0:	83 f8 10             	cmp    $0x10,%eax
801067e3:	74 0d                	je     801067f2 <sys_unlink+0x151>
    panic("unlink: writei");
801067e5:	83 ec 0c             	sub    $0xc,%esp
801067e8:	68 23 98 10 80       	push   $0x80109823
801067ed:	e8 74 9d ff ff       	call   80100566 <panic>
  if(ip->type == T_DIR){
801067f2:	8b 45 f0             	mov    -0x10(%ebp),%eax
801067f5:	0f b7 40 10          	movzwl 0x10(%eax),%eax
801067f9:	66 83 f8 01          	cmp    $0x1,%ax
801067fd:	75 21                	jne    80106820 <sys_unlink+0x17f>
    dp->nlink--;
801067ff:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106802:	0f b7 40 16          	movzwl 0x16(%eax),%eax
80106806:	83 e8 01             	sub    $0x1,%eax
80106809:	89 c2                	mov    %eax,%edx
8010680b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010680e:	66 89 50 16          	mov    %dx,0x16(%eax)
    iupdate(dp);
80106812:	83 ec 0c             	sub    $0xc,%esp
80106815:	ff 75 f4             	pushl  -0xc(%ebp)
80106818:	e8 11 b0 ff ff       	call   8010182e <iupdate>
8010681d:	83 c4 10             	add    $0x10,%esp
  }
  iunlockput(dp);
80106820:	83 ec 0c             	sub    $0xc,%esp
80106823:	ff 75 f4             	pushl  -0xc(%ebp)
80106826:	e8 9d b4 ff ff       	call   80101cc8 <iunlockput>
8010682b:	83 c4 10             	add    $0x10,%esp

  ip->nlink--;
8010682e:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106831:	0f b7 40 16          	movzwl 0x16(%eax),%eax
80106835:	83 e8 01             	sub    $0x1,%eax
80106838:	89 c2                	mov    %eax,%edx
8010683a:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010683d:	66 89 50 16          	mov    %dx,0x16(%eax)
  iupdate(ip);
80106841:	83 ec 0c             	sub    $0xc,%esp
80106844:	ff 75 f0             	pushl  -0x10(%ebp)
80106847:	e8 e2 af ff ff       	call   8010182e <iupdate>
8010684c:	83 c4 10             	add    $0x10,%esp
  iunlockput(ip);
8010684f:	83 ec 0c             	sub    $0xc,%esp
80106852:	ff 75 f0             	pushl  -0x10(%ebp)
80106855:	e8 6e b4 ff ff       	call   80101cc8 <iunlockput>
8010685a:	83 c4 10             	add    $0x10,%esp

  end_op();
8010685d:	e8 15 ce ff ff       	call   80103677 <end_op>

  return 0;
80106862:	b8 00 00 00 00       	mov    $0x0,%eax
80106867:	eb 19                	jmp    80106882 <sys_unlink+0x1e1>
  // Cannot unlink "." or "..".
  if(namecmp(name, ".") == 0 || namecmp(name, "..") == 0)
    goto bad;

  if((ip = dirlookup(dp, name, &off)) == 0)
    goto bad;
80106869:	90                   	nop
  end_op();

  return 0;

bad:
  iunlockput(dp);
8010686a:	83 ec 0c             	sub    $0xc,%esp
8010686d:	ff 75 f4             	pushl  -0xc(%ebp)
80106870:	e8 53 b4 ff ff       	call   80101cc8 <iunlockput>
80106875:	83 c4 10             	add    $0x10,%esp
  end_op();
80106878:	e8 fa cd ff ff       	call   80103677 <end_op>
  return -1;
8010687d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80106882:	c9                   	leave  
80106883:	c3                   	ret    

80106884 <create>:

static struct inode*
create(char *path, short type, short major, short minor)
{
80106884:	55                   	push   %ebp
80106885:	89 e5                	mov    %esp,%ebp
80106887:	83 ec 38             	sub    $0x38,%esp
8010688a:	8b 4d 0c             	mov    0xc(%ebp),%ecx
8010688d:	8b 55 10             	mov    0x10(%ebp),%edx
80106890:	8b 45 14             	mov    0x14(%ebp),%eax
80106893:	66 89 4d d4          	mov    %cx,-0x2c(%ebp)
80106897:	66 89 55 d0          	mov    %dx,-0x30(%ebp)
8010689b:	66 89 45 cc          	mov    %ax,-0x34(%ebp)
  uint off;
  struct inode *ip, *dp;
  char name[DIRSIZ];

  if((dp = nameiparent(path, name)) == 0)
8010689f:	83 ec 08             	sub    $0x8,%esp
801068a2:	8d 45 de             	lea    -0x22(%ebp),%eax
801068a5:	50                   	push   %eax
801068a6:	ff 75 08             	pushl  0x8(%ebp)
801068a9:	e8 34 bd ff ff       	call   801025e2 <nameiparent>
801068ae:	83 c4 10             	add    $0x10,%esp
801068b1:	89 45 f4             	mov    %eax,-0xc(%ebp)
801068b4:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801068b8:	75 0a                	jne    801068c4 <create+0x40>
    return 0;
801068ba:	b8 00 00 00 00       	mov    $0x0,%eax
801068bf:	e9 90 01 00 00       	jmp    80106a54 <create+0x1d0>
  ilock(dp);
801068c4:	83 ec 0c             	sub    $0xc,%esp
801068c7:	ff 75 f4             	pushl  -0xc(%ebp)
801068ca:	e8 39 b1 ff ff       	call   80101a08 <ilock>
801068cf:	83 c4 10             	add    $0x10,%esp

  if((ip = dirlookup(dp, name, &off)) != 0){
801068d2:	83 ec 04             	sub    $0x4,%esp
801068d5:	8d 45 ec             	lea    -0x14(%ebp),%eax
801068d8:	50                   	push   %eax
801068d9:	8d 45 de             	lea    -0x22(%ebp),%eax
801068dc:	50                   	push   %eax
801068dd:	ff 75 f4             	pushl  -0xc(%ebp)
801068e0:	e8 8b b9 ff ff       	call   80102270 <dirlookup>
801068e5:	83 c4 10             	add    $0x10,%esp
801068e8:	89 45 f0             	mov    %eax,-0x10(%ebp)
801068eb:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
801068ef:	74 50                	je     80106941 <create+0xbd>
    iunlockput(dp);
801068f1:	83 ec 0c             	sub    $0xc,%esp
801068f4:	ff 75 f4             	pushl  -0xc(%ebp)
801068f7:	e8 cc b3 ff ff       	call   80101cc8 <iunlockput>
801068fc:	83 c4 10             	add    $0x10,%esp
    ilock(ip);
801068ff:	83 ec 0c             	sub    $0xc,%esp
80106902:	ff 75 f0             	pushl  -0x10(%ebp)
80106905:	e8 fe b0 ff ff       	call   80101a08 <ilock>
8010690a:	83 c4 10             	add    $0x10,%esp
    if(type == T_FILE && ip->type == T_FILE)
8010690d:	66 83 7d d4 02       	cmpw   $0x2,-0x2c(%ebp)
80106912:	75 15                	jne    80106929 <create+0xa5>
80106914:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106917:	0f b7 40 10          	movzwl 0x10(%eax),%eax
8010691b:	66 83 f8 02          	cmp    $0x2,%ax
8010691f:	75 08                	jne    80106929 <create+0xa5>
      return ip;
80106921:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106924:	e9 2b 01 00 00       	jmp    80106a54 <create+0x1d0>
    iunlockput(ip);
80106929:	83 ec 0c             	sub    $0xc,%esp
8010692c:	ff 75 f0             	pushl  -0x10(%ebp)
8010692f:	e8 94 b3 ff ff       	call   80101cc8 <iunlockput>
80106934:	83 c4 10             	add    $0x10,%esp
    return 0;
80106937:	b8 00 00 00 00       	mov    $0x0,%eax
8010693c:	e9 13 01 00 00       	jmp    80106a54 <create+0x1d0>
  }

  if((ip = ialloc(dp->dev, type)) == 0)
80106941:	0f bf 55 d4          	movswl -0x2c(%ebp),%edx
80106945:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106948:	8b 00                	mov    (%eax),%eax
8010694a:	83 ec 08             	sub    $0x8,%esp
8010694d:	52                   	push   %edx
8010694e:	50                   	push   %eax
8010694f:	e8 03 ae ff ff       	call   80101757 <ialloc>
80106954:	83 c4 10             	add    $0x10,%esp
80106957:	89 45 f0             	mov    %eax,-0x10(%ebp)
8010695a:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
8010695e:	75 0d                	jne    8010696d <create+0xe9>
    panic("create: ialloc");
80106960:	83 ec 0c             	sub    $0xc,%esp
80106963:	68 32 98 10 80       	push   $0x80109832
80106968:	e8 f9 9b ff ff       	call   80100566 <panic>

  ilock(ip);
8010696d:	83 ec 0c             	sub    $0xc,%esp
80106970:	ff 75 f0             	pushl  -0x10(%ebp)
80106973:	e8 90 b0 ff ff       	call   80101a08 <ilock>
80106978:	83 c4 10             	add    $0x10,%esp
  ip->major = major;
8010697b:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010697e:	0f b7 55 d0          	movzwl -0x30(%ebp),%edx
80106982:	66 89 50 12          	mov    %dx,0x12(%eax)
  ip->minor = minor;
80106986:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106989:	0f b7 55 cc          	movzwl -0x34(%ebp),%edx
8010698d:	66 89 50 14          	mov    %dx,0x14(%eax)
  ip->nlink = 1;
80106991:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106994:	66 c7 40 16 01 00    	movw   $0x1,0x16(%eax)
  iupdate(ip);
8010699a:	83 ec 0c             	sub    $0xc,%esp
8010699d:	ff 75 f0             	pushl  -0x10(%ebp)
801069a0:	e8 89 ae ff ff       	call   8010182e <iupdate>
801069a5:	83 c4 10             	add    $0x10,%esp

  if(type == T_DIR){  // Create . and .. entries.
801069a8:	66 83 7d d4 01       	cmpw   $0x1,-0x2c(%ebp)
801069ad:	75 6a                	jne    80106a19 <create+0x195>
    dp->nlink++;  // for ".."
801069af:	8b 45 f4             	mov    -0xc(%ebp),%eax
801069b2:	0f b7 40 16          	movzwl 0x16(%eax),%eax
801069b6:	83 c0 01             	add    $0x1,%eax
801069b9:	89 c2                	mov    %eax,%edx
801069bb:	8b 45 f4             	mov    -0xc(%ebp),%eax
801069be:	66 89 50 16          	mov    %dx,0x16(%eax)
    iupdate(dp);
801069c2:	83 ec 0c             	sub    $0xc,%esp
801069c5:	ff 75 f4             	pushl  -0xc(%ebp)
801069c8:	e8 61 ae ff ff       	call   8010182e <iupdate>
801069cd:	83 c4 10             	add    $0x10,%esp
    // No ip->nlink++ for ".": avoid cyclic ref count.
    if(dirlink(ip, ".", ip->inum) < 0 || dirlink(ip, "..", dp->inum) < 0)
801069d0:	8b 45 f0             	mov    -0x10(%ebp),%eax
801069d3:	8b 40 04             	mov    0x4(%eax),%eax
801069d6:	83 ec 04             	sub    $0x4,%esp
801069d9:	50                   	push   %eax
801069da:	68 0c 98 10 80       	push   $0x8010980c
801069df:	ff 75 f0             	pushl  -0x10(%ebp)
801069e2:	e8 43 b9 ff ff       	call   8010232a <dirlink>
801069e7:	83 c4 10             	add    $0x10,%esp
801069ea:	85 c0                	test   %eax,%eax
801069ec:	78 1e                	js     80106a0c <create+0x188>
801069ee:	8b 45 f4             	mov    -0xc(%ebp),%eax
801069f1:	8b 40 04             	mov    0x4(%eax),%eax
801069f4:	83 ec 04             	sub    $0x4,%esp
801069f7:	50                   	push   %eax
801069f8:	68 0e 98 10 80       	push   $0x8010980e
801069fd:	ff 75 f0             	pushl  -0x10(%ebp)
80106a00:	e8 25 b9 ff ff       	call   8010232a <dirlink>
80106a05:	83 c4 10             	add    $0x10,%esp
80106a08:	85 c0                	test   %eax,%eax
80106a0a:	79 0d                	jns    80106a19 <create+0x195>
      panic("create dots");
80106a0c:	83 ec 0c             	sub    $0xc,%esp
80106a0f:	68 41 98 10 80       	push   $0x80109841
80106a14:	e8 4d 9b ff ff       	call   80100566 <panic>
  }

  if(dirlink(dp, name, ip->inum) < 0)
80106a19:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106a1c:	8b 40 04             	mov    0x4(%eax),%eax
80106a1f:	83 ec 04             	sub    $0x4,%esp
80106a22:	50                   	push   %eax
80106a23:	8d 45 de             	lea    -0x22(%ebp),%eax
80106a26:	50                   	push   %eax
80106a27:	ff 75 f4             	pushl  -0xc(%ebp)
80106a2a:	e8 fb b8 ff ff       	call   8010232a <dirlink>
80106a2f:	83 c4 10             	add    $0x10,%esp
80106a32:	85 c0                	test   %eax,%eax
80106a34:	79 0d                	jns    80106a43 <create+0x1bf>
    panic("create: dirlink");
80106a36:	83 ec 0c             	sub    $0xc,%esp
80106a39:	68 4d 98 10 80       	push   $0x8010984d
80106a3e:	e8 23 9b ff ff       	call   80100566 <panic>

  iunlockput(dp);
80106a43:	83 ec 0c             	sub    $0xc,%esp
80106a46:	ff 75 f4             	pushl  -0xc(%ebp)
80106a49:	e8 7a b2 ff ff       	call   80101cc8 <iunlockput>
80106a4e:	83 c4 10             	add    $0x10,%esp

  return ip;
80106a51:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
80106a54:	c9                   	leave  
80106a55:	c3                   	ret    

80106a56 <sys_open>:

int
sys_open(void)
{
80106a56:	55                   	push   %ebp
80106a57:	89 e5                	mov    %esp,%ebp
80106a59:	83 ec 28             	sub    $0x28,%esp
  char *path;
  int fd, omode;
  struct file *f;
  struct inode *ip;

  if(argstr(0, &path) < 0 || argint(1, &omode) < 0)
80106a5c:	83 ec 08             	sub    $0x8,%esp
80106a5f:	8d 45 e8             	lea    -0x18(%ebp),%eax
80106a62:	50                   	push   %eax
80106a63:	6a 00                	push   $0x0
80106a65:	e8 eb f6 ff ff       	call   80106155 <argstr>
80106a6a:	83 c4 10             	add    $0x10,%esp
80106a6d:	85 c0                	test   %eax,%eax
80106a6f:	78 15                	js     80106a86 <sys_open+0x30>
80106a71:	83 ec 08             	sub    $0x8,%esp
80106a74:	8d 45 e4             	lea    -0x1c(%ebp),%eax
80106a77:	50                   	push   %eax
80106a78:	6a 01                	push   $0x1
80106a7a:	e8 51 f6 ff ff       	call   801060d0 <argint>
80106a7f:	83 c4 10             	add    $0x10,%esp
80106a82:	85 c0                	test   %eax,%eax
80106a84:	79 0a                	jns    80106a90 <sys_open+0x3a>
    return -1;
80106a86:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106a8b:	e9 61 01 00 00       	jmp    80106bf1 <sys_open+0x19b>

  begin_op();
80106a90:	e8 56 cb ff ff       	call   801035eb <begin_op>

  if(omode & O_CREATE){
80106a95:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80106a98:	25 00 02 00 00       	and    $0x200,%eax
80106a9d:	85 c0                	test   %eax,%eax
80106a9f:	74 2a                	je     80106acb <sys_open+0x75>
    ip = create(path, T_FILE, 0, 0);
80106aa1:	8b 45 e8             	mov    -0x18(%ebp),%eax
80106aa4:	6a 00                	push   $0x0
80106aa6:	6a 00                	push   $0x0
80106aa8:	6a 02                	push   $0x2
80106aaa:	50                   	push   %eax
80106aab:	e8 d4 fd ff ff       	call   80106884 <create>
80106ab0:	83 c4 10             	add    $0x10,%esp
80106ab3:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if(ip == 0){
80106ab6:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80106aba:	75 75                	jne    80106b31 <sys_open+0xdb>
      end_op();
80106abc:	e8 b6 cb ff ff       	call   80103677 <end_op>
      return -1;
80106ac1:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106ac6:	e9 26 01 00 00       	jmp    80106bf1 <sys_open+0x19b>
    }
  } else {
    if((ip = namei(path)) == 0){
80106acb:	8b 45 e8             	mov    -0x18(%ebp),%eax
80106ace:	83 ec 0c             	sub    $0xc,%esp
80106ad1:	50                   	push   %eax
80106ad2:	e8 ef ba ff ff       	call   801025c6 <namei>
80106ad7:	83 c4 10             	add    $0x10,%esp
80106ada:	89 45 f4             	mov    %eax,-0xc(%ebp)
80106add:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80106ae1:	75 0f                	jne    80106af2 <sys_open+0x9c>
      end_op();
80106ae3:	e8 8f cb ff ff       	call   80103677 <end_op>
      return -1;
80106ae8:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106aed:	e9 ff 00 00 00       	jmp    80106bf1 <sys_open+0x19b>
    }
    ilock(ip);
80106af2:	83 ec 0c             	sub    $0xc,%esp
80106af5:	ff 75 f4             	pushl  -0xc(%ebp)
80106af8:	e8 0b af ff ff       	call   80101a08 <ilock>
80106afd:	83 c4 10             	add    $0x10,%esp
    if(ip->type == T_DIR && omode != O_RDONLY){
80106b00:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106b03:	0f b7 40 10          	movzwl 0x10(%eax),%eax
80106b07:	66 83 f8 01          	cmp    $0x1,%ax
80106b0b:	75 24                	jne    80106b31 <sys_open+0xdb>
80106b0d:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80106b10:	85 c0                	test   %eax,%eax
80106b12:	74 1d                	je     80106b31 <sys_open+0xdb>
      iunlockput(ip);
80106b14:	83 ec 0c             	sub    $0xc,%esp
80106b17:	ff 75 f4             	pushl  -0xc(%ebp)
80106b1a:	e8 a9 b1 ff ff       	call   80101cc8 <iunlockput>
80106b1f:	83 c4 10             	add    $0x10,%esp
      end_op();
80106b22:	e8 50 cb ff ff       	call   80103677 <end_op>
      return -1;
80106b27:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106b2c:	e9 c0 00 00 00       	jmp    80106bf1 <sys_open+0x19b>
    }
  }

  if((f = filealloc()) == 0 || (fd = fdalloc(f)) < 0){
80106b31:	e8 fb a4 ff ff       	call   80101031 <filealloc>
80106b36:	89 45 f0             	mov    %eax,-0x10(%ebp)
80106b39:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80106b3d:	74 17                	je     80106b56 <sys_open+0x100>
80106b3f:	83 ec 0c             	sub    $0xc,%esp
80106b42:	ff 75 f0             	pushl  -0x10(%ebp)
80106b45:	e8 37 f7 ff ff       	call   80106281 <fdalloc>
80106b4a:	83 c4 10             	add    $0x10,%esp
80106b4d:	89 45 ec             	mov    %eax,-0x14(%ebp)
80106b50:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
80106b54:	79 2e                	jns    80106b84 <sys_open+0x12e>
    if(f)
80106b56:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80106b5a:	74 0e                	je     80106b6a <sys_open+0x114>
      fileclose(f);
80106b5c:	83 ec 0c             	sub    $0xc,%esp
80106b5f:	ff 75 f0             	pushl  -0x10(%ebp)
80106b62:	e8 88 a5 ff ff       	call   801010ef <fileclose>
80106b67:	83 c4 10             	add    $0x10,%esp
    iunlockput(ip);
80106b6a:	83 ec 0c             	sub    $0xc,%esp
80106b6d:	ff 75 f4             	pushl  -0xc(%ebp)
80106b70:	e8 53 b1 ff ff       	call   80101cc8 <iunlockput>
80106b75:	83 c4 10             	add    $0x10,%esp
    end_op();
80106b78:	e8 fa ca ff ff       	call   80103677 <end_op>
    return -1;
80106b7d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106b82:	eb 6d                	jmp    80106bf1 <sys_open+0x19b>
  }
  iunlock(ip);
80106b84:	83 ec 0c             	sub    $0xc,%esp
80106b87:	ff 75 f4             	pushl  -0xc(%ebp)
80106b8a:	e8 d7 af ff ff       	call   80101b66 <iunlock>
80106b8f:	83 c4 10             	add    $0x10,%esp
  end_op();
80106b92:	e8 e0 ca ff ff       	call   80103677 <end_op>

  f->type = FD_INODE;
80106b97:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106b9a:	c7 00 02 00 00 00    	movl   $0x2,(%eax)
  f->ip = ip;
80106ba0:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106ba3:	8b 55 f4             	mov    -0xc(%ebp),%edx
80106ba6:	89 50 10             	mov    %edx,0x10(%eax)
  f->off = 0;
80106ba9:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106bac:	c7 40 14 00 00 00 00 	movl   $0x0,0x14(%eax)
  f->readable = !(omode & O_WRONLY);
80106bb3:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80106bb6:	83 e0 01             	and    $0x1,%eax
80106bb9:	85 c0                	test   %eax,%eax
80106bbb:	0f 94 c0             	sete   %al
80106bbe:	89 c2                	mov    %eax,%edx
80106bc0:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106bc3:	88 50 08             	mov    %dl,0x8(%eax)
  f->writable = (omode & O_WRONLY) || (omode & O_RDWR);
80106bc6:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80106bc9:	83 e0 01             	and    $0x1,%eax
80106bcc:	85 c0                	test   %eax,%eax
80106bce:	75 0a                	jne    80106bda <sys_open+0x184>
80106bd0:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80106bd3:	83 e0 02             	and    $0x2,%eax
80106bd6:	85 c0                	test   %eax,%eax
80106bd8:	74 07                	je     80106be1 <sys_open+0x18b>
80106bda:	b8 01 00 00 00       	mov    $0x1,%eax
80106bdf:	eb 05                	jmp    80106be6 <sys_open+0x190>
80106be1:	b8 00 00 00 00       	mov    $0x0,%eax
80106be6:	89 c2                	mov    %eax,%edx
80106be8:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106beb:	88 50 09             	mov    %dl,0x9(%eax)
  return fd;
80106bee:	8b 45 ec             	mov    -0x14(%ebp),%eax
}
80106bf1:	c9                   	leave  
80106bf2:	c3                   	ret    

80106bf3 <sys_mkdir>:

int
sys_mkdir(void)
{
80106bf3:	55                   	push   %ebp
80106bf4:	89 e5                	mov    %esp,%ebp
80106bf6:	83 ec 18             	sub    $0x18,%esp
  char *path;
  struct inode *ip;

  begin_op();
80106bf9:	e8 ed c9 ff ff       	call   801035eb <begin_op>
  if(argstr(0, &path) < 0 || (ip = create(path, T_DIR, 0, 0)) == 0){
80106bfe:	83 ec 08             	sub    $0x8,%esp
80106c01:	8d 45 f0             	lea    -0x10(%ebp),%eax
80106c04:	50                   	push   %eax
80106c05:	6a 00                	push   $0x0
80106c07:	e8 49 f5 ff ff       	call   80106155 <argstr>
80106c0c:	83 c4 10             	add    $0x10,%esp
80106c0f:	85 c0                	test   %eax,%eax
80106c11:	78 1b                	js     80106c2e <sys_mkdir+0x3b>
80106c13:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106c16:	6a 00                	push   $0x0
80106c18:	6a 00                	push   $0x0
80106c1a:	6a 01                	push   $0x1
80106c1c:	50                   	push   %eax
80106c1d:	e8 62 fc ff ff       	call   80106884 <create>
80106c22:	83 c4 10             	add    $0x10,%esp
80106c25:	89 45 f4             	mov    %eax,-0xc(%ebp)
80106c28:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80106c2c:	75 0c                	jne    80106c3a <sys_mkdir+0x47>
    end_op();
80106c2e:	e8 44 ca ff ff       	call   80103677 <end_op>
    return -1;
80106c33:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106c38:	eb 18                	jmp    80106c52 <sys_mkdir+0x5f>
  }
  iunlockput(ip);
80106c3a:	83 ec 0c             	sub    $0xc,%esp
80106c3d:	ff 75 f4             	pushl  -0xc(%ebp)
80106c40:	e8 83 b0 ff ff       	call   80101cc8 <iunlockput>
80106c45:	83 c4 10             	add    $0x10,%esp
  end_op();
80106c48:	e8 2a ca ff ff       	call   80103677 <end_op>
  return 0;
80106c4d:	b8 00 00 00 00       	mov    $0x0,%eax
}
80106c52:	c9                   	leave  
80106c53:	c3                   	ret    

80106c54 <sys_mknod>:

int
sys_mknod(void)
{
80106c54:	55                   	push   %ebp
80106c55:	89 e5                	mov    %esp,%ebp
80106c57:	83 ec 28             	sub    $0x28,%esp
  struct inode *ip;
  char *path;
  int len;
  int major, minor;
  
  begin_op();
80106c5a:	e8 8c c9 ff ff       	call   801035eb <begin_op>
  if((len=argstr(0, &path)) < 0 ||
80106c5f:	83 ec 08             	sub    $0x8,%esp
80106c62:	8d 45 ec             	lea    -0x14(%ebp),%eax
80106c65:	50                   	push   %eax
80106c66:	6a 00                	push   $0x0
80106c68:	e8 e8 f4 ff ff       	call   80106155 <argstr>
80106c6d:	83 c4 10             	add    $0x10,%esp
80106c70:	89 45 f4             	mov    %eax,-0xc(%ebp)
80106c73:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80106c77:	78 4f                	js     80106cc8 <sys_mknod+0x74>
     argint(1, &major) < 0 ||
80106c79:	83 ec 08             	sub    $0x8,%esp
80106c7c:	8d 45 e8             	lea    -0x18(%ebp),%eax
80106c7f:	50                   	push   %eax
80106c80:	6a 01                	push   $0x1
80106c82:	e8 49 f4 ff ff       	call   801060d0 <argint>
80106c87:	83 c4 10             	add    $0x10,%esp
  char *path;
  int len;
  int major, minor;
  
  begin_op();
  if((len=argstr(0, &path)) < 0 ||
80106c8a:	85 c0                	test   %eax,%eax
80106c8c:	78 3a                	js     80106cc8 <sys_mknod+0x74>
     argint(1, &major) < 0 ||
     argint(2, &minor) < 0 ||
80106c8e:	83 ec 08             	sub    $0x8,%esp
80106c91:	8d 45 e4             	lea    -0x1c(%ebp),%eax
80106c94:	50                   	push   %eax
80106c95:	6a 02                	push   $0x2
80106c97:	e8 34 f4 ff ff       	call   801060d0 <argint>
80106c9c:	83 c4 10             	add    $0x10,%esp
  int len;
  int major, minor;
  
  begin_op();
  if((len=argstr(0, &path)) < 0 ||
     argint(1, &major) < 0 ||
80106c9f:	85 c0                	test   %eax,%eax
80106ca1:	78 25                	js     80106cc8 <sys_mknod+0x74>
     argint(2, &minor) < 0 ||
     (ip = create(path, T_DEV, major, minor)) == 0){
80106ca3:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80106ca6:	0f bf c8             	movswl %ax,%ecx
80106ca9:	8b 45 e8             	mov    -0x18(%ebp),%eax
80106cac:	0f bf d0             	movswl %ax,%edx
80106caf:	8b 45 ec             	mov    -0x14(%ebp),%eax
  int major, minor;
  
  begin_op();
  if((len=argstr(0, &path)) < 0 ||
     argint(1, &major) < 0 ||
     argint(2, &minor) < 0 ||
80106cb2:	51                   	push   %ecx
80106cb3:	52                   	push   %edx
80106cb4:	6a 03                	push   $0x3
80106cb6:	50                   	push   %eax
80106cb7:	e8 c8 fb ff ff       	call   80106884 <create>
80106cbc:	83 c4 10             	add    $0x10,%esp
80106cbf:	89 45 f0             	mov    %eax,-0x10(%ebp)
80106cc2:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80106cc6:	75 0c                	jne    80106cd4 <sys_mknod+0x80>
     (ip = create(path, T_DEV, major, minor)) == 0){
    end_op();
80106cc8:	e8 aa c9 ff ff       	call   80103677 <end_op>
    return -1;
80106ccd:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106cd2:	eb 18                	jmp    80106cec <sys_mknod+0x98>
  }
  iunlockput(ip);
80106cd4:	83 ec 0c             	sub    $0xc,%esp
80106cd7:	ff 75 f0             	pushl  -0x10(%ebp)
80106cda:	e8 e9 af ff ff       	call   80101cc8 <iunlockput>
80106cdf:	83 c4 10             	add    $0x10,%esp
  end_op();
80106ce2:	e8 90 c9 ff ff       	call   80103677 <end_op>
  return 0;
80106ce7:	b8 00 00 00 00       	mov    $0x0,%eax
}
80106cec:	c9                   	leave  
80106ced:	c3                   	ret    

80106cee <sys_chdir>:

int
sys_chdir(void)
{
80106cee:	55                   	push   %ebp
80106cef:	89 e5                	mov    %esp,%ebp
80106cf1:	83 ec 18             	sub    $0x18,%esp
  char *path;
  struct inode *ip;

  begin_op();
80106cf4:	e8 f2 c8 ff ff       	call   801035eb <begin_op>
  if(argstr(0, &path) < 0 || (ip = namei(path)) == 0){
80106cf9:	83 ec 08             	sub    $0x8,%esp
80106cfc:	8d 45 f0             	lea    -0x10(%ebp),%eax
80106cff:	50                   	push   %eax
80106d00:	6a 00                	push   $0x0
80106d02:	e8 4e f4 ff ff       	call   80106155 <argstr>
80106d07:	83 c4 10             	add    $0x10,%esp
80106d0a:	85 c0                	test   %eax,%eax
80106d0c:	78 18                	js     80106d26 <sys_chdir+0x38>
80106d0e:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106d11:	83 ec 0c             	sub    $0xc,%esp
80106d14:	50                   	push   %eax
80106d15:	e8 ac b8 ff ff       	call   801025c6 <namei>
80106d1a:	83 c4 10             	add    $0x10,%esp
80106d1d:	89 45 f4             	mov    %eax,-0xc(%ebp)
80106d20:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80106d24:	75 0c                	jne    80106d32 <sys_chdir+0x44>
    end_op();
80106d26:	e8 4c c9 ff ff       	call   80103677 <end_op>
    return -1;
80106d2b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106d30:	eb 6e                	jmp    80106da0 <sys_chdir+0xb2>
  }
  ilock(ip);
80106d32:	83 ec 0c             	sub    $0xc,%esp
80106d35:	ff 75 f4             	pushl  -0xc(%ebp)
80106d38:	e8 cb ac ff ff       	call   80101a08 <ilock>
80106d3d:	83 c4 10             	add    $0x10,%esp
  if(ip->type != T_DIR){
80106d40:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106d43:	0f b7 40 10          	movzwl 0x10(%eax),%eax
80106d47:	66 83 f8 01          	cmp    $0x1,%ax
80106d4b:	74 1a                	je     80106d67 <sys_chdir+0x79>
    iunlockput(ip);
80106d4d:	83 ec 0c             	sub    $0xc,%esp
80106d50:	ff 75 f4             	pushl  -0xc(%ebp)
80106d53:	e8 70 af ff ff       	call   80101cc8 <iunlockput>
80106d58:	83 c4 10             	add    $0x10,%esp
    end_op();
80106d5b:	e8 17 c9 ff ff       	call   80103677 <end_op>
    return -1;
80106d60:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106d65:	eb 39                	jmp    80106da0 <sys_chdir+0xb2>
  }
  iunlock(ip);
80106d67:	83 ec 0c             	sub    $0xc,%esp
80106d6a:	ff 75 f4             	pushl  -0xc(%ebp)
80106d6d:	e8 f4 ad ff ff       	call   80101b66 <iunlock>
80106d72:	83 c4 10             	add    $0x10,%esp
  iput(proc->cwd);
80106d75:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106d7b:	8b 40 68             	mov    0x68(%eax),%eax
80106d7e:	83 ec 0c             	sub    $0xc,%esp
80106d81:	50                   	push   %eax
80106d82:	e8 51 ae ff ff       	call   80101bd8 <iput>
80106d87:	83 c4 10             	add    $0x10,%esp
  end_op();
80106d8a:	e8 e8 c8 ff ff       	call   80103677 <end_op>
  proc->cwd = ip;
80106d8f:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106d95:	8b 55 f4             	mov    -0xc(%ebp),%edx
80106d98:	89 50 68             	mov    %edx,0x68(%eax)
  return 0;
80106d9b:	b8 00 00 00 00       	mov    $0x0,%eax
}
80106da0:	c9                   	leave  
80106da1:	c3                   	ret    

80106da2 <sys_exec>:

int
sys_exec(void)
{
80106da2:	55                   	push   %ebp
80106da3:	89 e5                	mov    %esp,%ebp
80106da5:	81 ec 98 00 00 00    	sub    $0x98,%esp
  char *path, *argv[MAXARG];
  int i;
  uint uargv, uarg;

  if(argstr(0, &path) < 0 || argint(1, (int*)&uargv) < 0){
80106dab:	83 ec 08             	sub    $0x8,%esp
80106dae:	8d 45 f0             	lea    -0x10(%ebp),%eax
80106db1:	50                   	push   %eax
80106db2:	6a 00                	push   $0x0
80106db4:	e8 9c f3 ff ff       	call   80106155 <argstr>
80106db9:	83 c4 10             	add    $0x10,%esp
80106dbc:	85 c0                	test   %eax,%eax
80106dbe:	78 18                	js     80106dd8 <sys_exec+0x36>
80106dc0:	83 ec 08             	sub    $0x8,%esp
80106dc3:	8d 85 6c ff ff ff    	lea    -0x94(%ebp),%eax
80106dc9:	50                   	push   %eax
80106dca:	6a 01                	push   $0x1
80106dcc:	e8 ff f2 ff ff       	call   801060d0 <argint>
80106dd1:	83 c4 10             	add    $0x10,%esp
80106dd4:	85 c0                	test   %eax,%eax
80106dd6:	79 0a                	jns    80106de2 <sys_exec+0x40>
    return -1;
80106dd8:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106ddd:	e9 c6 00 00 00       	jmp    80106ea8 <sys_exec+0x106>
  }
  memset(argv, 0, sizeof(argv));
80106de2:	83 ec 04             	sub    $0x4,%esp
80106de5:	68 80 00 00 00       	push   $0x80
80106dea:	6a 00                	push   $0x0
80106dec:	8d 85 70 ff ff ff    	lea    -0x90(%ebp),%eax
80106df2:	50                   	push   %eax
80106df3:	e8 b3 ef ff ff       	call   80105dab <memset>
80106df8:	83 c4 10             	add    $0x10,%esp
  for(i=0;; i++){
80106dfb:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
    if(i >= NELEM(argv))
80106e02:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106e05:	83 f8 1f             	cmp    $0x1f,%eax
80106e08:	76 0a                	jbe    80106e14 <sys_exec+0x72>
      return -1;
80106e0a:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106e0f:	e9 94 00 00 00       	jmp    80106ea8 <sys_exec+0x106>
    if(fetchint(uargv+4*i, (int*)&uarg) < 0)
80106e14:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106e17:	c1 e0 02             	shl    $0x2,%eax
80106e1a:	89 c2                	mov    %eax,%edx
80106e1c:	8b 85 6c ff ff ff    	mov    -0x94(%ebp),%eax
80106e22:	01 c2                	add    %eax,%edx
80106e24:	83 ec 08             	sub    $0x8,%esp
80106e27:	8d 85 68 ff ff ff    	lea    -0x98(%ebp),%eax
80106e2d:	50                   	push   %eax
80106e2e:	52                   	push   %edx
80106e2f:	e8 00 f2 ff ff       	call   80106034 <fetchint>
80106e34:	83 c4 10             	add    $0x10,%esp
80106e37:	85 c0                	test   %eax,%eax
80106e39:	79 07                	jns    80106e42 <sys_exec+0xa0>
      return -1;
80106e3b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106e40:	eb 66                	jmp    80106ea8 <sys_exec+0x106>
    if(uarg == 0){
80106e42:	8b 85 68 ff ff ff    	mov    -0x98(%ebp),%eax
80106e48:	85 c0                	test   %eax,%eax
80106e4a:	75 27                	jne    80106e73 <sys_exec+0xd1>
      argv[i] = 0;
80106e4c:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106e4f:	c7 84 85 70 ff ff ff 	movl   $0x0,-0x90(%ebp,%eax,4)
80106e56:	00 00 00 00 
      break;
80106e5a:	90                   	nop
    }
    if(fetchstr(uarg, &argv[i]) < 0)
      return -1;
  }
  return exec(path, argv);
80106e5b:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106e5e:	83 ec 08             	sub    $0x8,%esp
80106e61:	8d 95 70 ff ff ff    	lea    -0x90(%ebp),%edx
80106e67:	52                   	push   %edx
80106e68:	50                   	push   %eax
80106e69:	e8 a1 9d ff ff       	call   80100c0f <exec>
80106e6e:	83 c4 10             	add    $0x10,%esp
80106e71:	eb 35                	jmp    80106ea8 <sys_exec+0x106>
      return -1;
    if(uarg == 0){
      argv[i] = 0;
      break;
    }
    if(fetchstr(uarg, &argv[i]) < 0)
80106e73:	8d 85 70 ff ff ff    	lea    -0x90(%ebp),%eax
80106e79:	8b 55 f4             	mov    -0xc(%ebp),%edx
80106e7c:	c1 e2 02             	shl    $0x2,%edx
80106e7f:	01 c2                	add    %eax,%edx
80106e81:	8b 85 68 ff ff ff    	mov    -0x98(%ebp),%eax
80106e87:	83 ec 08             	sub    $0x8,%esp
80106e8a:	52                   	push   %edx
80106e8b:	50                   	push   %eax
80106e8c:	e8 dd f1 ff ff       	call   8010606e <fetchstr>
80106e91:	83 c4 10             	add    $0x10,%esp
80106e94:	85 c0                	test   %eax,%eax
80106e96:	79 07                	jns    80106e9f <sys_exec+0xfd>
      return -1;
80106e98:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106e9d:	eb 09                	jmp    80106ea8 <sys_exec+0x106>

  if(argstr(0, &path) < 0 || argint(1, (int*)&uargv) < 0){
    return -1;
  }
  memset(argv, 0, sizeof(argv));
  for(i=0;; i++){
80106e9f:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
      argv[i] = 0;
      break;
    }
    if(fetchstr(uarg, &argv[i]) < 0)
      return -1;
  }
80106ea3:	e9 5a ff ff ff       	jmp    80106e02 <sys_exec+0x60>
  return exec(path, argv);
}
80106ea8:	c9                   	leave  
80106ea9:	c3                   	ret    

80106eaa <sys_pipe>:

int
sys_pipe(void)
{
80106eaa:	55                   	push   %ebp
80106eab:	89 e5                	mov    %esp,%ebp
80106ead:	83 ec 28             	sub    $0x28,%esp
  int *fd;
  struct file *rf, *wf;
  int fd0, fd1;

  if(argptr(0, (void*)&fd, 2*sizeof(fd[0])) < 0)
80106eb0:	83 ec 04             	sub    $0x4,%esp
80106eb3:	6a 08                	push   $0x8
80106eb5:	8d 45 ec             	lea    -0x14(%ebp),%eax
80106eb8:	50                   	push   %eax
80106eb9:	6a 00                	push   $0x0
80106ebb:	e8 38 f2 ff ff       	call   801060f8 <argptr>
80106ec0:	83 c4 10             	add    $0x10,%esp
80106ec3:	85 c0                	test   %eax,%eax
80106ec5:	79 0a                	jns    80106ed1 <sys_pipe+0x27>
    return -1;
80106ec7:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106ecc:	e9 af 00 00 00       	jmp    80106f80 <sys_pipe+0xd6>
  if(pipealloc(&rf, &wf) < 0)
80106ed1:	83 ec 08             	sub    $0x8,%esp
80106ed4:	8d 45 e4             	lea    -0x1c(%ebp),%eax
80106ed7:	50                   	push   %eax
80106ed8:	8d 45 e8             	lea    -0x18(%ebp),%eax
80106edb:	50                   	push   %eax
80106edc:	e8 fe d1 ff ff       	call   801040df <pipealloc>
80106ee1:	83 c4 10             	add    $0x10,%esp
80106ee4:	85 c0                	test   %eax,%eax
80106ee6:	79 0a                	jns    80106ef2 <sys_pipe+0x48>
    return -1;
80106ee8:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106eed:	e9 8e 00 00 00       	jmp    80106f80 <sys_pipe+0xd6>
  fd0 = -1;
80106ef2:	c7 45 f4 ff ff ff ff 	movl   $0xffffffff,-0xc(%ebp)
  if((fd0 = fdalloc(rf)) < 0 || (fd1 = fdalloc(wf)) < 0){
80106ef9:	8b 45 e8             	mov    -0x18(%ebp),%eax
80106efc:	83 ec 0c             	sub    $0xc,%esp
80106eff:	50                   	push   %eax
80106f00:	e8 7c f3 ff ff       	call   80106281 <fdalloc>
80106f05:	83 c4 10             	add    $0x10,%esp
80106f08:	89 45 f4             	mov    %eax,-0xc(%ebp)
80106f0b:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80106f0f:	78 18                	js     80106f29 <sys_pipe+0x7f>
80106f11:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80106f14:	83 ec 0c             	sub    $0xc,%esp
80106f17:	50                   	push   %eax
80106f18:	e8 64 f3 ff ff       	call   80106281 <fdalloc>
80106f1d:	83 c4 10             	add    $0x10,%esp
80106f20:	89 45 f0             	mov    %eax,-0x10(%ebp)
80106f23:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80106f27:	79 3f                	jns    80106f68 <sys_pipe+0xbe>
    if(fd0 >= 0)
80106f29:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80106f2d:	78 14                	js     80106f43 <sys_pipe+0x99>
      proc->ofile[fd0] = 0;
80106f2f:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106f35:	8b 55 f4             	mov    -0xc(%ebp),%edx
80106f38:	83 c2 08             	add    $0x8,%edx
80106f3b:	c7 44 90 08 00 00 00 	movl   $0x0,0x8(%eax,%edx,4)
80106f42:	00 
    fileclose(rf);
80106f43:	8b 45 e8             	mov    -0x18(%ebp),%eax
80106f46:	83 ec 0c             	sub    $0xc,%esp
80106f49:	50                   	push   %eax
80106f4a:	e8 a0 a1 ff ff       	call   801010ef <fileclose>
80106f4f:	83 c4 10             	add    $0x10,%esp
    fileclose(wf);
80106f52:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80106f55:	83 ec 0c             	sub    $0xc,%esp
80106f58:	50                   	push   %eax
80106f59:	e8 91 a1 ff ff       	call   801010ef <fileclose>
80106f5e:	83 c4 10             	add    $0x10,%esp
    return -1;
80106f61:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106f66:	eb 18                	jmp    80106f80 <sys_pipe+0xd6>
  }
  fd[0] = fd0;
80106f68:	8b 45 ec             	mov    -0x14(%ebp),%eax
80106f6b:	8b 55 f4             	mov    -0xc(%ebp),%edx
80106f6e:	89 10                	mov    %edx,(%eax)
  fd[1] = fd1;
80106f70:	8b 45 ec             	mov    -0x14(%ebp),%eax
80106f73:	8d 50 04             	lea    0x4(%eax),%edx
80106f76:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106f79:	89 02                	mov    %eax,(%edx)
  return 0;
80106f7b:	b8 00 00 00 00       	mov    $0x0,%eax
}
80106f80:	c9                   	leave  
80106f81:	c3                   	ret    

80106f82 <outw>:
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
}

static inline void
outw(ushort port, ushort data)
{
80106f82:	55                   	push   %ebp
80106f83:	89 e5                	mov    %esp,%ebp
80106f85:	83 ec 08             	sub    $0x8,%esp
80106f88:	8b 55 08             	mov    0x8(%ebp),%edx
80106f8b:	8b 45 0c             	mov    0xc(%ebp),%eax
80106f8e:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
80106f92:	66 89 45 f8          	mov    %ax,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80106f96:	0f b7 45 f8          	movzwl -0x8(%ebp),%eax
80106f9a:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
80106f9e:	66 ef                	out    %ax,(%dx)
}
80106fa0:	90                   	nop
80106fa1:	c9                   	leave  
80106fa2:	c3                   	ret    

80106fa3 <sys_fork>:
#include "proc.h"
#include "uproc.h"

int
sys_fork(void)
{
80106fa3:	55                   	push   %ebp
80106fa4:	89 e5                	mov    %esp,%ebp
80106fa6:	83 ec 08             	sub    $0x8,%esp
  return fork();
80106fa9:	e8 bd dc ff ff       	call   80104c6b <fork>
}
80106fae:	c9                   	leave  
80106faf:	c3                   	ret    

80106fb0 <sys_exit>:

int
sys_exit(void)
{
80106fb0:	55                   	push   %ebp
80106fb1:	89 e5                	mov    %esp,%ebp
80106fb3:	83 ec 08             	sub    $0x8,%esp
  exit();
80106fb6:	e8 af de ff ff       	call   80104e6a <exit>
  return 0;  // not reached
80106fbb:	b8 00 00 00 00       	mov    $0x0,%eax
}
80106fc0:	c9                   	leave  
80106fc1:	c3                   	ret    

80106fc2 <sys_wait>:

int
sys_wait(void)
{
80106fc2:	55                   	push   %ebp
80106fc3:	89 e5                	mov    %esp,%ebp
80106fc5:	83 ec 08             	sub    $0x8,%esp
  return wait();
80106fc8:	e8 eb df ff ff       	call   80104fb8 <wait>
}
80106fcd:	c9                   	leave  
80106fce:	c3                   	ret    

80106fcf <sys_kill>:

int
sys_kill(void)
{
80106fcf:	55                   	push   %ebp
80106fd0:	89 e5                	mov    %esp,%ebp
80106fd2:	83 ec 18             	sub    $0x18,%esp
  int pid;

  if(argint(0, &pid) < 0)
80106fd5:	83 ec 08             	sub    $0x8,%esp
80106fd8:	8d 45 f4             	lea    -0xc(%ebp),%eax
80106fdb:	50                   	push   %eax
80106fdc:	6a 00                	push   $0x0
80106fde:	e8 ed f0 ff ff       	call   801060d0 <argint>
80106fe3:	83 c4 10             	add    $0x10,%esp
80106fe6:	85 c0                	test   %eax,%eax
80106fe8:	79 07                	jns    80106ff1 <sys_kill+0x22>
    return -1;
80106fea:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106fef:	eb 0f                	jmp    80107000 <sys_kill+0x31>
  return kill(pid);
80106ff1:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106ff4:	83 ec 0c             	sub    $0xc,%esp
80106ff7:	50                   	push   %eax
80106ff8:	e8 b3 e4 ff ff       	call   801054b0 <kill>
80106ffd:	83 c4 10             	add    $0x10,%esp
}
80107000:	c9                   	leave  
80107001:	c3                   	ret    

80107002 <sys_getpid>:

int
sys_getpid(void)
{
80107002:	55                   	push   %ebp
80107003:	89 e5                	mov    %esp,%ebp
  return proc->pid;
80107005:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010700b:	8b 40 10             	mov    0x10(%eax),%eax
}
8010700e:	5d                   	pop    %ebp
8010700f:	c3                   	ret    

80107010 <sys_sbrk>:

int
sys_sbrk(void)
{
80107010:	55                   	push   %ebp
80107011:	89 e5                	mov    %esp,%ebp
80107013:	83 ec 18             	sub    $0x18,%esp
  int addr;
  int n;

  if(argint(0, &n) < 0)
80107016:	83 ec 08             	sub    $0x8,%esp
80107019:	8d 45 f0             	lea    -0x10(%ebp),%eax
8010701c:	50                   	push   %eax
8010701d:	6a 00                	push   $0x0
8010701f:	e8 ac f0 ff ff       	call   801060d0 <argint>
80107024:	83 c4 10             	add    $0x10,%esp
80107027:	85 c0                	test   %eax,%eax
80107029:	79 07                	jns    80107032 <sys_sbrk+0x22>
    return -1;
8010702b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80107030:	eb 28                	jmp    8010705a <sys_sbrk+0x4a>
  addr = proc->sz;
80107032:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80107038:	8b 00                	mov    (%eax),%eax
8010703a:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(growproc(n) < 0)
8010703d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80107040:	83 ec 0c             	sub    $0xc,%esp
80107043:	50                   	push   %eax
80107044:	e8 7f db ff ff       	call   80104bc8 <growproc>
80107049:	83 c4 10             	add    $0x10,%esp
8010704c:	85 c0                	test   %eax,%eax
8010704e:	79 07                	jns    80107057 <sys_sbrk+0x47>
    return -1;
80107050:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80107055:	eb 03                	jmp    8010705a <sys_sbrk+0x4a>
  return addr;
80107057:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
8010705a:	c9                   	leave  
8010705b:	c3                   	ret    

8010705c <sys_sleep>:

int
sys_sleep(void)
{
8010705c:	55                   	push   %ebp
8010705d:	89 e5                	mov    %esp,%ebp
8010705f:	83 ec 18             	sub    $0x18,%esp
  int n;
  uint ticks0;
  
  if(argint(0, &n) < 0)
80107062:	83 ec 08             	sub    $0x8,%esp
80107065:	8d 45 f0             	lea    -0x10(%ebp),%eax
80107068:	50                   	push   %eax
80107069:	6a 00                	push   $0x0
8010706b:	e8 60 f0 ff ff       	call   801060d0 <argint>
80107070:	83 c4 10             	add    $0x10,%esp
80107073:	85 c0                	test   %eax,%eax
80107075:	79 07                	jns    8010707e <sys_sleep+0x22>
    return -1;
80107077:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010707c:	eb 44                	jmp    801070c2 <sys_sleep+0x66>
  ticks0 = ticks;
8010707e:	a1 e0 67 11 80       	mov    0x801167e0,%eax
80107083:	89 45 f4             	mov    %eax,-0xc(%ebp)
  while(ticks - ticks0 < n){
80107086:	eb 26                	jmp    801070ae <sys_sleep+0x52>
    if(proc->killed){
80107088:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010708e:	8b 40 24             	mov    0x24(%eax),%eax
80107091:	85 c0                	test   %eax,%eax
80107093:	74 07                	je     8010709c <sys_sleep+0x40>
      return -1;
80107095:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010709a:	eb 26                	jmp    801070c2 <sys_sleep+0x66>
    }
    sleep(&ticks, (struct spinlock *)0);
8010709c:	83 ec 08             	sub    $0x8,%esp
8010709f:	6a 00                	push   $0x0
801070a1:	68 e0 67 11 80       	push   $0x801167e0
801070a6:	e8 b9 e2 ff ff       	call   80105364 <sleep>
801070ab:	83 c4 10             	add    $0x10,%esp
  uint ticks0;
  
  if(argint(0, &n) < 0)
    return -1;
  ticks0 = ticks;
  while(ticks - ticks0 < n){
801070ae:	a1 e0 67 11 80       	mov    0x801167e0,%eax
801070b3:	2b 45 f4             	sub    -0xc(%ebp),%eax
801070b6:	8b 55 f0             	mov    -0x10(%ebp),%edx
801070b9:	39 d0                	cmp    %edx,%eax
801070bb:	72 cb                	jb     80107088 <sys_sleep+0x2c>
    if(proc->killed){
      return -1;
    }
    sleep(&ticks, (struct spinlock *)0);
  }
  return 0;
801070bd:	b8 00 00 00 00       	mov    $0x0,%eax
}
801070c2:	c9                   	leave  
801070c3:	c3                   	ret    

801070c4 <sys_uptime>:

// return how many clock tick interrupts have occurred
// since start. 
int
sys_uptime(void)
{
801070c4:	55                   	push   %ebp
801070c5:	89 e5                	mov    %esp,%ebp
801070c7:	83 ec 10             	sub    $0x10,%esp
  uint xticks;
  
  xticks = ticks;
801070ca:	a1 e0 67 11 80       	mov    0x801167e0,%eax
801070cf:	89 45 fc             	mov    %eax,-0x4(%ebp)
  return xticks;
801070d2:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
801070d5:	c9                   	leave  
801070d6:	c3                   	ret    

801070d7 <sys_halt>:

//Turn of the computer
int sys_halt(void){
801070d7:	55                   	push   %ebp
801070d8:	89 e5                	mov    %esp,%ebp
801070da:	83 ec 08             	sub    $0x8,%esp
  cprintf("Shutting down ...\n");
801070dd:	83 ec 0c             	sub    $0xc,%esp
801070e0:	68 5d 98 10 80       	push   $0x8010985d
801070e5:	e8 dc 92 ff ff       	call   801003c6 <cprintf>
801070ea:	83 c4 10             	add    $0x10,%esp
// outw (0xB004, 0x0 | 0x2000);  // changed in newest version of QEMU
  outw( 0x604, 0x0 | 0x2000);
801070ed:	83 ec 08             	sub    $0x8,%esp
801070f0:	68 00 20 00 00       	push   $0x2000
801070f5:	68 04 06 00 00       	push   $0x604
801070fa:	e8 83 fe ff ff       	call   80106f82 <outw>
801070ff:	83 c4 10             	add    $0x10,%esp
  return 0;
80107102:	b8 00 00 00 00       	mov    $0x0,%eax
}
80107107:	c9                   	leave  
80107108:	c3                   	ret    

80107109 <sys_date>:
#ifdef CS333_P1
//Print date, copied from project ! (code provided)
int
sys_date(void)
{
80107109:	55                   	push   %ebp
8010710a:	89 e5                	mov    %esp,%ebp
8010710c:	83 ec 18             	sub    $0x18,%esp
  struct rtcdate *d;

  if(argptr(0, (void*)&d, sizeof(*d)) < 0)
8010710f:	83 ec 04             	sub    $0x4,%esp
80107112:	6a 18                	push   $0x18
80107114:	8d 45 f4             	lea    -0xc(%ebp),%eax
80107117:	50                   	push   %eax
80107118:	6a 00                	push   $0x0
8010711a:	e8 d9 ef ff ff       	call   801060f8 <argptr>
8010711f:	83 c4 10             	add    $0x10,%esp
80107122:	85 c0                	test   %eax,%eax
80107124:	79 07                	jns    8010712d <sys_date+0x24>
    return -1;
80107126:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010712b:	eb 14                	jmp    80107141 <sys_date+0x38>
  cmostime(d);    
8010712d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107130:	83 ec 0c             	sub    $0xc,%esp
80107133:	50                   	push   %eax
80107134:	e8 2d c1 ff ff       	call   80103266 <cmostime>
80107139:	83 c4 10             	add    $0x10,%esp
  return 0;
8010713c:	b8 00 00 00 00       	mov    $0x0,%eax
}
80107141:	c9                   	leave  
80107142:	c3                   	ret    

80107143 <sys_getuid>:
#endif

int
sys_getuid(void)
{
80107143:	55                   	push   %ebp
80107144:	89 e5                	mov    %esp,%ebp
  return proc->uid;
80107146:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010714c:	8b 80 84 00 00 00    	mov    0x84(%eax),%eax
}
80107152:	5d                   	pop    %ebp
80107153:	c3                   	ret    

80107154 <sys_getgid>:

int
sys_getgid(void)
{
80107154:	55                   	push   %ebp
80107155:	89 e5                	mov    %esp,%ebp
  return proc->gid;
80107157:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010715d:	8b 80 80 00 00 00    	mov    0x80(%eax),%eax
}
80107163:	5d                   	pop    %ebp
80107164:	c3                   	ret    

80107165 <sys_getppid>:

int
sys_getppid(void)
{
80107165:	55                   	push   %ebp
80107166:	89 e5                	mov    %esp,%ebp
  if(!proc->parent->pid)
80107168:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010716e:	8b 40 14             	mov    0x14(%eax),%eax
80107171:	8b 40 10             	mov    0x10(%eax),%eax
80107174:	85 c0                	test   %eax,%eax
80107176:	75 07                	jne    8010717f <sys_getppid+0x1a>
    return 1;
80107178:	b8 01 00 00 00       	mov    $0x1,%eax
8010717d:	eb 0c                	jmp    8010718b <sys_getppid+0x26>
  return proc->parent->pid;
8010717f:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80107185:	8b 40 14             	mov    0x14(%eax),%eax
80107188:	8b 40 10             	mov    0x10(%eax),%eax
}
8010718b:	5d                   	pop    %ebp
8010718c:	c3                   	ret    

8010718d <sys_setuid>:

int
sys_setuid(void)
{
8010718d:	55                   	push   %ebp
8010718e:	89 e5                	mov    %esp,%ebp
80107190:	83 ec 18             	sub    $0x18,%esp
  int num;
  if(argint(0, &num) < 0)
80107193:	83 ec 08             	sub    $0x8,%esp
80107196:	8d 45 f4             	lea    -0xc(%ebp),%eax
80107199:	50                   	push   %eax
8010719a:	6a 00                	push   $0x0
8010719c:	e8 2f ef ff ff       	call   801060d0 <argint>
801071a1:	83 c4 10             	add    $0x10,%esp
801071a4:	85 c0                	test   %eax,%eax
801071a6:	79 07                	jns    801071af <sys_setuid+0x22>
    return -1;
801071a8:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801071ad:	eb 2c                	jmp    801071db <sys_setuid+0x4e>
  if(num < 0 || num > 32767)
801071af:	8b 45 f4             	mov    -0xc(%ebp),%eax
801071b2:	85 c0                	test   %eax,%eax
801071b4:	78 0a                	js     801071c0 <sys_setuid+0x33>
801071b6:	8b 45 f4             	mov    -0xc(%ebp),%eax
801071b9:	3d ff 7f 00 00       	cmp    $0x7fff,%eax
801071be:	7e 07                	jle    801071c7 <sys_setuid+0x3a>
    return -1;
801071c0:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801071c5:	eb 14                	jmp    801071db <sys_setuid+0x4e>
  setuid(num);
801071c7:	8b 45 f4             	mov    -0xc(%ebp),%eax
801071ca:	83 ec 0c             	sub    $0xc,%esp
801071cd:	50                   	push   %eax
801071ce:	e8 0f e6 ff ff       	call   801057e2 <setuid>
801071d3:	83 c4 10             	add    $0x10,%esp
  return 0;
801071d6:	b8 00 00 00 00       	mov    $0x0,%eax
}
801071db:	c9                   	leave  
801071dc:	c3                   	ret    

801071dd <sys_setgid>:

int
sys_setgid(void)
{
801071dd:	55                   	push   %ebp
801071de:	89 e5                	mov    %esp,%ebp
801071e0:	83 ec 18             	sub    $0x18,%esp
  int num;
  if(argint(0, &num) < 0)
801071e3:	83 ec 08             	sub    $0x8,%esp
801071e6:	8d 45 f4             	lea    -0xc(%ebp),%eax
801071e9:	50                   	push   %eax
801071ea:	6a 00                	push   $0x0
801071ec:	e8 df ee ff ff       	call   801060d0 <argint>
801071f1:	83 c4 10             	add    $0x10,%esp
801071f4:	85 c0                	test   %eax,%eax
801071f6:	79 07                	jns    801071ff <sys_setgid+0x22>
    return -1;
801071f8:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801071fd:	eb 2c                	jmp    8010722b <sys_setgid+0x4e>
  if(num < 0 || num > 32767)
801071ff:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107202:	85 c0                	test   %eax,%eax
80107204:	78 0a                	js     80107210 <sys_setgid+0x33>
80107206:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107209:	3d ff 7f 00 00       	cmp    $0x7fff,%eax
8010720e:	7e 07                	jle    80107217 <sys_setgid+0x3a>
    return -1;
80107210:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80107215:	eb 14                	jmp    8010722b <sys_setgid+0x4e>
  setgid(num);
80107217:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010721a:	83 ec 0c             	sub    $0xc,%esp
8010721d:	50                   	push   %eax
8010721e:	e8 fb e5 ff ff       	call   8010581e <setgid>
80107223:	83 c4 10             	add    $0x10,%esp
  return 0;
80107226:	b8 00 00 00 00       	mov    $0x0,%eax
}
8010722b:	c9                   	leave  
8010722c:	c3                   	ret    

8010722d <sys_getprocs>:

int 
sys_getprocs(void)
{
8010722d:	55                   	push   %ebp
8010722e:	89 e5                	mov    %esp,%ebp
80107230:	83 ec 18             	sub    $0x18,%esp
  int MAX;
  struct uproc * temp;
  
  if(argint(0, &MAX) < 0 || argptr(1, (void*) &temp, sizeof(*temp)) < 0)
80107233:	83 ec 08             	sub    $0x8,%esp
80107236:	8d 45 f4             	lea    -0xc(%ebp),%eax
80107239:	50                   	push   %eax
8010723a:	6a 00                	push   $0x0
8010723c:	e8 8f ee ff ff       	call   801060d0 <argint>
80107241:	83 c4 10             	add    $0x10,%esp
80107244:	85 c0                	test   %eax,%eax
80107246:	78 17                	js     8010725f <sys_getprocs+0x32>
80107248:	83 ec 04             	sub    $0x4,%esp
8010724b:	6a 64                	push   $0x64
8010724d:	8d 45 f0             	lea    -0x10(%ebp),%eax
80107250:	50                   	push   %eax
80107251:	6a 01                	push   $0x1
80107253:	e8 a0 ee ff ff       	call   801060f8 <argptr>
80107258:	83 c4 10             	add    $0x10,%esp
8010725b:	85 c0                	test   %eax,%eax
8010725d:	79 07                	jns    80107266 <sys_getprocs+0x39>
    return -1;
8010725f:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80107264:	eb 13                	jmp    80107279 <sys_getprocs+0x4c>

  return getprocs(MAX, temp);
80107266:	8b 55 f0             	mov    -0x10(%ebp),%edx
80107269:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010726c:	83 ec 08             	sub    $0x8,%esp
8010726f:	52                   	push   %edx
80107270:	50                   	push   %eax
80107271:	e8 e4 e5 ff ff       	call   8010585a <getprocs>
80107276:	83 c4 10             	add    $0x10,%esp
}
80107279:	c9                   	leave  
8010727a:	c3                   	ret    

8010727b <sys_setpriority>:

int 
sys_setpriority(void)
{
8010727b:	55                   	push   %ebp
8010727c:	89 e5                	mov    %esp,%ebp
8010727e:	83 ec 18             	sub    $0x18,%esp
  int pid;
  int priority;

  if(argint(0, &pid) < 0 || argint(1, &priority) < 0)
80107281:	83 ec 08             	sub    $0x8,%esp
80107284:	8d 45 f4             	lea    -0xc(%ebp),%eax
80107287:	50                   	push   %eax
80107288:	6a 00                	push   $0x0
8010728a:	e8 41 ee ff ff       	call   801060d0 <argint>
8010728f:	83 c4 10             	add    $0x10,%esp
80107292:	85 c0                	test   %eax,%eax
80107294:	78 15                	js     801072ab <sys_setpriority+0x30>
80107296:	83 ec 08             	sub    $0x8,%esp
80107299:	8d 45 f0             	lea    -0x10(%ebp),%eax
8010729c:	50                   	push   %eax
8010729d:	6a 01                	push   $0x1
8010729f:	e8 2c ee ff ff       	call   801060d0 <argint>
801072a4:	83 c4 10             	add    $0x10,%esp
801072a7:	85 c0                	test   %eax,%eax
801072a9:	79 07                	jns    801072b2 <sys_setpriority+0x37>
    return -1;
801072ab:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801072b0:	eb 13                	jmp    801072c5 <sys_setpriority+0x4a>

  return setpriority(pid, priority);  
801072b2:	8b 55 f0             	mov    -0x10(%ebp),%edx
801072b5:	8b 45 f4             	mov    -0xc(%ebp),%eax
801072b8:	83 ec 08             	sub    $0x8,%esp
801072bb:	52                   	push   %edx
801072bc:	50                   	push   %eax
801072bd:	e8 22 e8 ff ff       	call   80105ae4 <setpriority>
801072c2:	83 c4 10             	add    $0x10,%esp
}
801072c5:	c9                   	leave  
801072c6:	c3                   	ret    

801072c7 <outb>:
               "memory", "cc");
}

static inline void
outb(ushort port, uchar data)
{
801072c7:	55                   	push   %ebp
801072c8:	89 e5                	mov    %esp,%ebp
801072ca:	83 ec 08             	sub    $0x8,%esp
801072cd:	8b 55 08             	mov    0x8(%ebp),%edx
801072d0:	8b 45 0c             	mov    0xc(%ebp),%eax
801072d3:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
801072d7:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801072da:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
801072de:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
801072e2:	ee                   	out    %al,(%dx)
}
801072e3:	90                   	nop
801072e4:	c9                   	leave  
801072e5:	c3                   	ret    

801072e6 <timerinit>:
#define TIMER_RATEGEN   0x04    // mode 2, rate generator
#define TIMER_16BIT     0x30    // r/w counter 16 bits, LSB first

void
timerinit(void)
{
801072e6:	55                   	push   %ebp
801072e7:	89 e5                	mov    %esp,%ebp
801072e9:	83 ec 08             	sub    $0x8,%esp
  // Interrupt 100 times/sec.
  outb(TIMER_MODE, TIMER_SEL0 | TIMER_RATEGEN | TIMER_16BIT);
801072ec:	6a 34                	push   $0x34
801072ee:	6a 43                	push   $0x43
801072f0:	e8 d2 ff ff ff       	call   801072c7 <outb>
801072f5:	83 c4 08             	add    $0x8,%esp
  outb(IO_TIMER1, TIMER_DIV(100) % 256);
801072f8:	68 9c 00 00 00       	push   $0x9c
801072fd:	6a 40                	push   $0x40
801072ff:	e8 c3 ff ff ff       	call   801072c7 <outb>
80107304:	83 c4 08             	add    $0x8,%esp
  outb(IO_TIMER1, TIMER_DIV(100) / 256);
80107307:	6a 2e                	push   $0x2e
80107309:	6a 40                	push   $0x40
8010730b:	e8 b7 ff ff ff       	call   801072c7 <outb>
80107310:	83 c4 08             	add    $0x8,%esp
  picenable(IRQ_TIMER);
80107313:	83 ec 0c             	sub    $0xc,%esp
80107316:	6a 00                	push   $0x0
80107318:	e8 ac cc ff ff       	call   80103fc9 <picenable>
8010731d:	83 c4 10             	add    $0x10,%esp
}
80107320:	90                   	nop
80107321:	c9                   	leave  
80107322:	c3                   	ret    

80107323 <alltraps>:

  # vectors.S sends all traps here.
.globl alltraps
alltraps:
  # Build trap frame.
  pushl %ds
80107323:	1e                   	push   %ds
  pushl %es
80107324:	06                   	push   %es
  pushl %fs
80107325:	0f a0                	push   %fs
  pushl %gs
80107327:	0f a8                	push   %gs
  pushal
80107329:	60                   	pusha  
  
  # Set up data and per-cpu segments.
  movw $(SEG_KDATA<<3), %ax
8010732a:	66 b8 10 00          	mov    $0x10,%ax
  movw %ax, %ds
8010732e:	8e d8                	mov    %eax,%ds
  movw %ax, %es
80107330:	8e c0                	mov    %eax,%es
  movw $(SEG_KCPU<<3), %ax
80107332:	66 b8 18 00          	mov    $0x18,%ax
  movw %ax, %fs
80107336:	8e e0                	mov    %eax,%fs
  movw %ax, %gs
80107338:	8e e8                	mov    %eax,%gs

  # Call trap(tf), where tf=%esp
  pushl %esp
8010733a:	54                   	push   %esp
  call trap
8010733b:	e8 ce 01 00 00       	call   8010750e <trap>
  addl $4, %esp
80107340:	83 c4 04             	add    $0x4,%esp

80107343 <trapret>:

  # Return falls through to trapret...
.globl trapret
trapret:
  popal
80107343:	61                   	popa   
  popl %gs
80107344:	0f a9                	pop    %gs
  popl %fs
80107346:	0f a1                	pop    %fs
  popl %es
80107348:	07                   	pop    %es
  popl %ds
80107349:	1f                   	pop    %ds
  addl $0x8, %esp  # trapno and errcode
8010734a:	83 c4 08             	add    $0x8,%esp
  iret
8010734d:	cf                   	iret   

8010734e <atom_inc>:

// Routines added for CS333
// atom_inc() added to simplify handling of ticks global
static inline void
atom_inc(volatile int *num)
{
8010734e:	55                   	push   %ebp
8010734f:	89 e5                	mov    %esp,%ebp
  asm volatile ( "lock incl %0" : "=m" (*num));
80107351:	8b 45 08             	mov    0x8(%ebp),%eax
80107354:	f0 ff 00             	lock incl (%eax)
}
80107357:	90                   	nop
80107358:	5d                   	pop    %ebp
80107359:	c3                   	ret    

8010735a <lidt>:

struct gatedesc;

static inline void
lidt(struct gatedesc *p, int size)
{
8010735a:	55                   	push   %ebp
8010735b:	89 e5                	mov    %esp,%ebp
8010735d:	83 ec 10             	sub    $0x10,%esp
  volatile ushort pd[3];

  pd[0] = size-1;
80107360:	8b 45 0c             	mov    0xc(%ebp),%eax
80107363:	83 e8 01             	sub    $0x1,%eax
80107366:	66 89 45 fa          	mov    %ax,-0x6(%ebp)
  pd[1] = (uint)p;
8010736a:	8b 45 08             	mov    0x8(%ebp),%eax
8010736d:	66 89 45 fc          	mov    %ax,-0x4(%ebp)
  pd[2] = (uint)p >> 16;
80107371:	8b 45 08             	mov    0x8(%ebp),%eax
80107374:	c1 e8 10             	shr    $0x10,%eax
80107377:	66 89 45 fe          	mov    %ax,-0x2(%ebp)

  asm volatile("lidt (%0)" : : "r" (pd));
8010737b:	8d 45 fa             	lea    -0x6(%ebp),%eax
8010737e:	0f 01 18             	lidtl  (%eax)
}
80107381:	90                   	nop
80107382:	c9                   	leave  
80107383:	c3                   	ret    

80107384 <rcr2>:
  return result;
}

static inline uint
rcr2(void)
{
80107384:	55                   	push   %ebp
80107385:	89 e5                	mov    %esp,%ebp
80107387:	83 ec 10             	sub    $0x10,%esp
  uint val;
  asm volatile("movl %%cr2,%0" : "=r" (val));
8010738a:	0f 20 d0             	mov    %cr2,%eax
8010738d:	89 45 fc             	mov    %eax,-0x4(%ebp)
  return val;
80107390:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80107393:	c9                   	leave  
80107394:	c3                   	ret    

80107395 <tvinit>:
// Software Developer’s Manual, Vol 3A, 8.1.1 Guaranteed Atomic Operations.
uint ticks __attribute__ ((aligned (4)));

void
tvinit(void)
{
80107395:	55                   	push   %ebp
80107396:	89 e5                	mov    %esp,%ebp
80107398:	83 ec 10             	sub    $0x10,%esp
  int i;

  for(i = 0; i < 256; i++)
8010739b:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
801073a2:	e9 c3 00 00 00       	jmp    8010746a <tvinit+0xd5>
    SETGATE(idt[i], 0, SEG_KCODE<<3, vectors[i], 0);
801073a7:	8b 45 fc             	mov    -0x4(%ebp),%eax
801073aa:	8b 04 85 bc c0 10 80 	mov    -0x7fef3f44(,%eax,4),%eax
801073b1:	89 c2                	mov    %eax,%edx
801073b3:	8b 45 fc             	mov    -0x4(%ebp),%eax
801073b6:	66 89 14 c5 e0 5f 11 	mov    %dx,-0x7feea020(,%eax,8)
801073bd:	80 
801073be:	8b 45 fc             	mov    -0x4(%ebp),%eax
801073c1:	66 c7 04 c5 e2 5f 11 	movw   $0x8,-0x7feea01e(,%eax,8)
801073c8:	80 08 00 
801073cb:	8b 45 fc             	mov    -0x4(%ebp),%eax
801073ce:	0f b6 14 c5 e4 5f 11 	movzbl -0x7feea01c(,%eax,8),%edx
801073d5:	80 
801073d6:	83 e2 e0             	and    $0xffffffe0,%edx
801073d9:	88 14 c5 e4 5f 11 80 	mov    %dl,-0x7feea01c(,%eax,8)
801073e0:	8b 45 fc             	mov    -0x4(%ebp),%eax
801073e3:	0f b6 14 c5 e4 5f 11 	movzbl -0x7feea01c(,%eax,8),%edx
801073ea:	80 
801073eb:	83 e2 1f             	and    $0x1f,%edx
801073ee:	88 14 c5 e4 5f 11 80 	mov    %dl,-0x7feea01c(,%eax,8)
801073f5:	8b 45 fc             	mov    -0x4(%ebp),%eax
801073f8:	0f b6 14 c5 e5 5f 11 	movzbl -0x7feea01b(,%eax,8),%edx
801073ff:	80 
80107400:	83 e2 f0             	and    $0xfffffff0,%edx
80107403:	83 ca 0e             	or     $0xe,%edx
80107406:	88 14 c5 e5 5f 11 80 	mov    %dl,-0x7feea01b(,%eax,8)
8010740d:	8b 45 fc             	mov    -0x4(%ebp),%eax
80107410:	0f b6 14 c5 e5 5f 11 	movzbl -0x7feea01b(,%eax,8),%edx
80107417:	80 
80107418:	83 e2 ef             	and    $0xffffffef,%edx
8010741b:	88 14 c5 e5 5f 11 80 	mov    %dl,-0x7feea01b(,%eax,8)
80107422:	8b 45 fc             	mov    -0x4(%ebp),%eax
80107425:	0f b6 14 c5 e5 5f 11 	movzbl -0x7feea01b(,%eax,8),%edx
8010742c:	80 
8010742d:	83 e2 9f             	and    $0xffffff9f,%edx
80107430:	88 14 c5 e5 5f 11 80 	mov    %dl,-0x7feea01b(,%eax,8)
80107437:	8b 45 fc             	mov    -0x4(%ebp),%eax
8010743a:	0f b6 14 c5 e5 5f 11 	movzbl -0x7feea01b(,%eax,8),%edx
80107441:	80 
80107442:	83 ca 80             	or     $0xffffff80,%edx
80107445:	88 14 c5 e5 5f 11 80 	mov    %dl,-0x7feea01b(,%eax,8)
8010744c:	8b 45 fc             	mov    -0x4(%ebp),%eax
8010744f:	8b 04 85 bc c0 10 80 	mov    -0x7fef3f44(,%eax,4),%eax
80107456:	c1 e8 10             	shr    $0x10,%eax
80107459:	89 c2                	mov    %eax,%edx
8010745b:	8b 45 fc             	mov    -0x4(%ebp),%eax
8010745e:	66 89 14 c5 e6 5f 11 	mov    %dx,-0x7feea01a(,%eax,8)
80107465:	80 
void
tvinit(void)
{
  int i;

  for(i = 0; i < 256; i++)
80107466:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
8010746a:	81 7d fc ff 00 00 00 	cmpl   $0xff,-0x4(%ebp)
80107471:	0f 8e 30 ff ff ff    	jle    801073a7 <tvinit+0x12>
    SETGATE(idt[i], 0, SEG_KCODE<<3, vectors[i], 0);
  SETGATE(idt[T_SYSCALL], 1, SEG_KCODE<<3, vectors[T_SYSCALL], DPL_USER);
80107477:	a1 bc c1 10 80       	mov    0x8010c1bc,%eax
8010747c:	66 a3 e0 61 11 80    	mov    %ax,0x801161e0
80107482:	66 c7 05 e2 61 11 80 	movw   $0x8,0x801161e2
80107489:	08 00 
8010748b:	0f b6 05 e4 61 11 80 	movzbl 0x801161e4,%eax
80107492:	83 e0 e0             	and    $0xffffffe0,%eax
80107495:	a2 e4 61 11 80       	mov    %al,0x801161e4
8010749a:	0f b6 05 e4 61 11 80 	movzbl 0x801161e4,%eax
801074a1:	83 e0 1f             	and    $0x1f,%eax
801074a4:	a2 e4 61 11 80       	mov    %al,0x801161e4
801074a9:	0f b6 05 e5 61 11 80 	movzbl 0x801161e5,%eax
801074b0:	83 c8 0f             	or     $0xf,%eax
801074b3:	a2 e5 61 11 80       	mov    %al,0x801161e5
801074b8:	0f b6 05 e5 61 11 80 	movzbl 0x801161e5,%eax
801074bf:	83 e0 ef             	and    $0xffffffef,%eax
801074c2:	a2 e5 61 11 80       	mov    %al,0x801161e5
801074c7:	0f b6 05 e5 61 11 80 	movzbl 0x801161e5,%eax
801074ce:	83 c8 60             	or     $0x60,%eax
801074d1:	a2 e5 61 11 80       	mov    %al,0x801161e5
801074d6:	0f b6 05 e5 61 11 80 	movzbl 0x801161e5,%eax
801074dd:	83 c8 80             	or     $0xffffff80,%eax
801074e0:	a2 e5 61 11 80       	mov    %al,0x801161e5
801074e5:	a1 bc c1 10 80       	mov    0x8010c1bc,%eax
801074ea:	c1 e8 10             	shr    $0x10,%eax
801074ed:	66 a3 e6 61 11 80    	mov    %ax,0x801161e6
  
}
801074f3:	90                   	nop
801074f4:	c9                   	leave  
801074f5:	c3                   	ret    

801074f6 <idtinit>:

void
idtinit(void)
{
801074f6:	55                   	push   %ebp
801074f7:	89 e5                	mov    %esp,%ebp
  lidt(idt, sizeof(idt));
801074f9:	68 00 08 00 00       	push   $0x800
801074fe:	68 e0 5f 11 80       	push   $0x80115fe0
80107503:	e8 52 fe ff ff       	call   8010735a <lidt>
80107508:	83 c4 08             	add    $0x8,%esp
}
8010750b:	90                   	nop
8010750c:	c9                   	leave  
8010750d:	c3                   	ret    

8010750e <trap>:

void
trap(struct trapframe *tf)
{
8010750e:	55                   	push   %ebp
8010750f:	89 e5                	mov    %esp,%ebp
80107511:	57                   	push   %edi
80107512:	56                   	push   %esi
80107513:	53                   	push   %ebx
80107514:	83 ec 1c             	sub    $0x1c,%esp
  if(tf->trapno == T_SYSCALL){
80107517:	8b 45 08             	mov    0x8(%ebp),%eax
8010751a:	8b 40 30             	mov    0x30(%eax),%eax
8010751d:	83 f8 40             	cmp    $0x40,%eax
80107520:	75 3e                	jne    80107560 <trap+0x52>
    if(proc->killed)
80107522:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80107528:	8b 40 24             	mov    0x24(%eax),%eax
8010752b:	85 c0                	test   %eax,%eax
8010752d:	74 05                	je     80107534 <trap+0x26>
      exit();
8010752f:	e8 36 d9 ff ff       	call   80104e6a <exit>
    proc->tf = tf;
80107534:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010753a:	8b 55 08             	mov    0x8(%ebp),%edx
8010753d:	89 50 18             	mov    %edx,0x18(%eax)
    syscall();
80107540:	e8 41 ec ff ff       	call   80106186 <syscall>
    if(proc->killed)
80107545:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010754b:	8b 40 24             	mov    0x24(%eax),%eax
8010754e:	85 c0                	test   %eax,%eax
80107550:	0f 84 fe 01 00 00    	je     80107754 <trap+0x246>
      exit();
80107556:	e8 0f d9 ff ff       	call   80104e6a <exit>
    return;
8010755b:	e9 f4 01 00 00       	jmp    80107754 <trap+0x246>
  }

  switch(tf->trapno){
80107560:	8b 45 08             	mov    0x8(%ebp),%eax
80107563:	8b 40 30             	mov    0x30(%eax),%eax
80107566:	83 e8 20             	sub    $0x20,%eax
80107569:	83 f8 1f             	cmp    $0x1f,%eax
8010756c:	0f 87 a3 00 00 00    	ja     80107615 <trap+0x107>
80107572:	8b 04 85 10 99 10 80 	mov    -0x7fef66f0(,%eax,4),%eax
80107579:	ff e0                	jmp    *%eax
  case T_IRQ0 + IRQ_TIMER:
   if(cpu->id == 0){
8010757b:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80107581:	0f b6 00             	movzbl (%eax),%eax
80107584:	84 c0                	test   %al,%al
80107586:	75 20                	jne    801075a8 <trap+0x9a>
      atom_inc((int *)&ticks);   // guaranteed atomic so no lock necessary
80107588:	83 ec 0c             	sub    $0xc,%esp
8010758b:	68 e0 67 11 80       	push   $0x801167e0
80107590:	e8 b9 fd ff ff       	call   8010734e <atom_inc>
80107595:	83 c4 10             	add    $0x10,%esp
      wakeup(&ticks);
80107598:	83 ec 0c             	sub    $0xc,%esp
8010759b:	68 e0 67 11 80       	push   $0x801167e0
801075a0:	e8 d4 de ff ff       	call   80105479 <wakeup>
801075a5:	83 c4 10             	add    $0x10,%esp
    }
    lapiceoi();
801075a8:	e8 16 bb ff ff       	call   801030c3 <lapiceoi>
    break;
801075ad:	e9 1c 01 00 00       	jmp    801076ce <trap+0x1c0>
  case T_IRQ0 + IRQ_IDE:
    ideintr();
801075b2:	e8 1f b3 ff ff       	call   801028d6 <ideintr>
    lapiceoi();
801075b7:	e8 07 bb ff ff       	call   801030c3 <lapiceoi>
    break;
801075bc:	e9 0d 01 00 00       	jmp    801076ce <trap+0x1c0>
  case T_IRQ0 + IRQ_IDE+1:
    // Bochs generates spurious IDE1 interrupts.
    break;
  case T_IRQ0 + IRQ_KBD:
    kbdintr();
801075c1:	e8 ff b8 ff ff       	call   80102ec5 <kbdintr>
    lapiceoi();
801075c6:	e8 f8 ba ff ff       	call   801030c3 <lapiceoi>
    break;
801075cb:	e9 fe 00 00 00       	jmp    801076ce <trap+0x1c0>
  case T_IRQ0 + IRQ_COM1:
    uartintr();
801075d0:	e8 60 03 00 00       	call   80107935 <uartintr>
    lapiceoi();
801075d5:	e8 e9 ba ff ff       	call   801030c3 <lapiceoi>
    break;
801075da:	e9 ef 00 00 00       	jmp    801076ce <trap+0x1c0>
  case T_IRQ0 + 7:
  case T_IRQ0 + IRQ_SPURIOUS:
    cprintf("cpu%d: spurious interrupt at %x:%x\n",
801075df:	8b 45 08             	mov    0x8(%ebp),%eax
801075e2:	8b 48 38             	mov    0x38(%eax),%ecx
            cpu->id, tf->cs, tf->eip);
801075e5:	8b 45 08             	mov    0x8(%ebp),%eax
801075e8:	0f b7 40 3c          	movzwl 0x3c(%eax),%eax
    uartintr();
    lapiceoi();
    break;
  case T_IRQ0 + 7:
  case T_IRQ0 + IRQ_SPURIOUS:
    cprintf("cpu%d: spurious interrupt at %x:%x\n",
801075ec:	0f b7 d0             	movzwl %ax,%edx
            cpu->id, tf->cs, tf->eip);
801075ef:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
801075f5:	0f b6 00             	movzbl (%eax),%eax
    uartintr();
    lapiceoi();
    break;
  case T_IRQ0 + 7:
  case T_IRQ0 + IRQ_SPURIOUS:
    cprintf("cpu%d: spurious interrupt at %x:%x\n",
801075f8:	0f b6 c0             	movzbl %al,%eax
801075fb:	51                   	push   %ecx
801075fc:	52                   	push   %edx
801075fd:	50                   	push   %eax
801075fe:	68 70 98 10 80       	push   $0x80109870
80107603:	e8 be 8d ff ff       	call   801003c6 <cprintf>
80107608:	83 c4 10             	add    $0x10,%esp
            cpu->id, tf->cs, tf->eip);
    lapiceoi();
8010760b:	e8 b3 ba ff ff       	call   801030c3 <lapiceoi>
    break;
80107610:	e9 b9 00 00 00       	jmp    801076ce <trap+0x1c0>
   
  default:
    if(proc == 0 || (tf->cs&3) == 0){
80107615:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010761b:	85 c0                	test   %eax,%eax
8010761d:	74 11                	je     80107630 <trap+0x122>
8010761f:	8b 45 08             	mov    0x8(%ebp),%eax
80107622:	0f b7 40 3c          	movzwl 0x3c(%eax),%eax
80107626:	0f b7 c0             	movzwl %ax,%eax
80107629:	83 e0 03             	and    $0x3,%eax
8010762c:	85 c0                	test   %eax,%eax
8010762e:	75 40                	jne    80107670 <trap+0x162>
      // In kernel, it must be our mistake.
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
80107630:	e8 4f fd ff ff       	call   80107384 <rcr2>
80107635:	89 c3                	mov    %eax,%ebx
80107637:	8b 45 08             	mov    0x8(%ebp),%eax
8010763a:	8b 48 38             	mov    0x38(%eax),%ecx
              tf->trapno, cpu->id, tf->eip, rcr2());
8010763d:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80107643:	0f b6 00             	movzbl (%eax),%eax
    break;
   
  default:
    if(proc == 0 || (tf->cs&3) == 0){
      // In kernel, it must be our mistake.
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
80107646:	0f b6 d0             	movzbl %al,%edx
80107649:	8b 45 08             	mov    0x8(%ebp),%eax
8010764c:	8b 40 30             	mov    0x30(%eax),%eax
8010764f:	83 ec 0c             	sub    $0xc,%esp
80107652:	53                   	push   %ebx
80107653:	51                   	push   %ecx
80107654:	52                   	push   %edx
80107655:	50                   	push   %eax
80107656:	68 94 98 10 80       	push   $0x80109894
8010765b:	e8 66 8d ff ff       	call   801003c6 <cprintf>
80107660:	83 c4 20             	add    $0x20,%esp
              tf->trapno, cpu->id, tf->eip, rcr2());
      panic("trap");
80107663:	83 ec 0c             	sub    $0xc,%esp
80107666:	68 c6 98 10 80       	push   $0x801098c6
8010766b:	e8 f6 8e ff ff       	call   80100566 <panic>
    }
    // In user space, assume process misbehaved.
    cprintf("pid %d %s: trap %d err %d on cpu %d "
80107670:	e8 0f fd ff ff       	call   80107384 <rcr2>
80107675:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80107678:	8b 45 08             	mov    0x8(%ebp),%eax
8010767b:	8b 70 38             	mov    0x38(%eax),%esi
            "eip 0x%x addr 0x%x--kill proc\n",
            proc->pid, proc->name, tf->trapno, tf->err, cpu->id, tf->eip, 
8010767e:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80107684:	0f b6 00             	movzbl (%eax),%eax
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
              tf->trapno, cpu->id, tf->eip, rcr2());
      panic("trap");
    }
    // In user space, assume process misbehaved.
    cprintf("pid %d %s: trap %d err %d on cpu %d "
80107687:	0f b6 d8             	movzbl %al,%ebx
8010768a:	8b 45 08             	mov    0x8(%ebp),%eax
8010768d:	8b 48 34             	mov    0x34(%eax),%ecx
80107690:	8b 45 08             	mov    0x8(%ebp),%eax
80107693:	8b 50 30             	mov    0x30(%eax),%edx
            "eip 0x%x addr 0x%x--kill proc\n",
            proc->pid, proc->name, tf->trapno, tf->err, cpu->id, tf->eip, 
80107696:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010769c:	8d 78 6c             	lea    0x6c(%eax),%edi
8010769f:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
              tf->trapno, cpu->id, tf->eip, rcr2());
      panic("trap");
    }
    // In user space, assume process misbehaved.
    cprintf("pid %d %s: trap %d err %d on cpu %d "
801076a5:	8b 40 10             	mov    0x10(%eax),%eax
801076a8:	ff 75 e4             	pushl  -0x1c(%ebp)
801076ab:	56                   	push   %esi
801076ac:	53                   	push   %ebx
801076ad:	51                   	push   %ecx
801076ae:	52                   	push   %edx
801076af:	57                   	push   %edi
801076b0:	50                   	push   %eax
801076b1:	68 cc 98 10 80       	push   $0x801098cc
801076b6:	e8 0b 8d ff ff       	call   801003c6 <cprintf>
801076bb:	83 c4 20             	add    $0x20,%esp
            "eip 0x%x addr 0x%x--kill proc\n",
            proc->pid, proc->name, tf->trapno, tf->err, cpu->id, tf->eip, 
            rcr2());
    proc->killed = 1;
801076be:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801076c4:	c7 40 24 01 00 00 00 	movl   $0x1,0x24(%eax)
801076cb:	eb 01                	jmp    801076ce <trap+0x1c0>
    ideintr();
    lapiceoi();
    break;
  case T_IRQ0 + IRQ_IDE+1:
    // Bochs generates spurious IDE1 interrupts.
    break;
801076cd:	90                   	nop
  }

  // Force process exit if it has been killed and is in user space.
  // (If it is still executing in the kernel, let it keep running 
  // until it gets to the regular system call return.)
  if(proc && proc->killed && (tf->cs&3) == DPL_USER)
801076ce:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801076d4:	85 c0                	test   %eax,%eax
801076d6:	74 24                	je     801076fc <trap+0x1ee>
801076d8:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801076de:	8b 40 24             	mov    0x24(%eax),%eax
801076e1:	85 c0                	test   %eax,%eax
801076e3:	74 17                	je     801076fc <trap+0x1ee>
801076e5:	8b 45 08             	mov    0x8(%ebp),%eax
801076e8:	0f b7 40 3c          	movzwl 0x3c(%eax),%eax
801076ec:	0f b7 c0             	movzwl %ax,%eax
801076ef:	83 e0 03             	and    $0x3,%eax
801076f2:	83 f8 03             	cmp    $0x3,%eax
801076f5:	75 05                	jne    801076fc <trap+0x1ee>
    exit();
801076f7:	e8 6e d7 ff ff       	call   80104e6a <exit>

  // Force process to give up CPU on clock tick.
  // If interrupts were on while locks held, would need to check nlock.
  if(proc && proc->state == RUNNING && tf->trapno == T_IRQ0+IRQ_TIMER)
801076fc:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80107702:	85 c0                	test   %eax,%eax
80107704:	74 1e                	je     80107724 <trap+0x216>
80107706:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010770c:	8b 40 0c             	mov    0xc(%eax),%eax
8010770f:	83 f8 04             	cmp    $0x4,%eax
80107712:	75 10                	jne    80107724 <trap+0x216>
80107714:	8b 45 08             	mov    0x8(%ebp),%eax
80107717:	8b 40 30             	mov    0x30(%eax),%eax
8010771a:	83 f8 20             	cmp    $0x20,%eax
8010771d:	75 05                	jne    80107724 <trap+0x216>
    yield();
8010771f:	e8 ac db ff ff       	call   801052d0 <yield>

  // Check if the process has been killed since we yielded
  if(proc && proc->killed && (tf->cs&3) == DPL_USER)
80107724:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010772a:	85 c0                	test   %eax,%eax
8010772c:	74 27                	je     80107755 <trap+0x247>
8010772e:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80107734:	8b 40 24             	mov    0x24(%eax),%eax
80107737:	85 c0                	test   %eax,%eax
80107739:	74 1a                	je     80107755 <trap+0x247>
8010773b:	8b 45 08             	mov    0x8(%ebp),%eax
8010773e:	0f b7 40 3c          	movzwl 0x3c(%eax),%eax
80107742:	0f b7 c0             	movzwl %ax,%eax
80107745:	83 e0 03             	and    $0x3,%eax
80107748:	83 f8 03             	cmp    $0x3,%eax
8010774b:	75 08                	jne    80107755 <trap+0x247>
    exit();
8010774d:	e8 18 d7 ff ff       	call   80104e6a <exit>
80107752:	eb 01                	jmp    80107755 <trap+0x247>
      exit();
    proc->tf = tf;
    syscall();
    if(proc->killed)
      exit();
    return;
80107754:	90                   	nop
    yield();

  // Check if the process has been killed since we yielded
  if(proc && proc->killed && (tf->cs&3) == DPL_USER)
    exit();
}
80107755:	8d 65 f4             	lea    -0xc(%ebp),%esp
80107758:	5b                   	pop    %ebx
80107759:	5e                   	pop    %esi
8010775a:	5f                   	pop    %edi
8010775b:	5d                   	pop    %ebp
8010775c:	c3                   	ret    

8010775d <inb>:

// end of CS333 added routines

static inline uchar
inb(ushort port)
{
8010775d:	55                   	push   %ebp
8010775e:	89 e5                	mov    %esp,%ebp
80107760:	83 ec 14             	sub    $0x14,%esp
80107763:	8b 45 08             	mov    0x8(%ebp),%eax
80107766:	66 89 45 ec          	mov    %ax,-0x14(%ebp)
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
8010776a:	0f b7 45 ec          	movzwl -0x14(%ebp),%eax
8010776e:	89 c2                	mov    %eax,%edx
80107770:	ec                   	in     (%dx),%al
80107771:	88 45 ff             	mov    %al,-0x1(%ebp)
  return data;
80107774:	0f b6 45 ff          	movzbl -0x1(%ebp),%eax
}
80107778:	c9                   	leave  
80107779:	c3                   	ret    

8010777a <outb>:
               "memory", "cc");
}

static inline void
outb(ushort port, uchar data)
{
8010777a:	55                   	push   %ebp
8010777b:	89 e5                	mov    %esp,%ebp
8010777d:	83 ec 08             	sub    $0x8,%esp
80107780:	8b 55 08             	mov    0x8(%ebp),%edx
80107783:	8b 45 0c             	mov    0xc(%ebp),%eax
80107786:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
8010778a:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
8010778d:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
80107791:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
80107795:	ee                   	out    %al,(%dx)
}
80107796:	90                   	nop
80107797:	c9                   	leave  
80107798:	c3                   	ret    

80107799 <uartinit>:

static int uart;    // is there a uart?

void
uartinit(void)
{
80107799:	55                   	push   %ebp
8010779a:	89 e5                	mov    %esp,%ebp
8010779c:	83 ec 18             	sub    $0x18,%esp
  char *p;

  // Turn off the FIFO
  outb(COM1+2, 0);
8010779f:	6a 00                	push   $0x0
801077a1:	68 fa 03 00 00       	push   $0x3fa
801077a6:	e8 cf ff ff ff       	call   8010777a <outb>
801077ab:	83 c4 08             	add    $0x8,%esp
  
  // 9600 baud, 8 data bits, 1 stop bit, parity off.
  outb(COM1+3, 0x80);    // Unlock divisor
801077ae:	68 80 00 00 00       	push   $0x80
801077b3:	68 fb 03 00 00       	push   $0x3fb
801077b8:	e8 bd ff ff ff       	call   8010777a <outb>
801077bd:	83 c4 08             	add    $0x8,%esp
  outb(COM1+0, 115200/9600);
801077c0:	6a 0c                	push   $0xc
801077c2:	68 f8 03 00 00       	push   $0x3f8
801077c7:	e8 ae ff ff ff       	call   8010777a <outb>
801077cc:	83 c4 08             	add    $0x8,%esp
  outb(COM1+1, 0);
801077cf:	6a 00                	push   $0x0
801077d1:	68 f9 03 00 00       	push   $0x3f9
801077d6:	e8 9f ff ff ff       	call   8010777a <outb>
801077db:	83 c4 08             	add    $0x8,%esp
  outb(COM1+3, 0x03);    // Lock divisor, 8 data bits.
801077de:	6a 03                	push   $0x3
801077e0:	68 fb 03 00 00       	push   $0x3fb
801077e5:	e8 90 ff ff ff       	call   8010777a <outb>
801077ea:	83 c4 08             	add    $0x8,%esp
  outb(COM1+4, 0);
801077ed:	6a 00                	push   $0x0
801077ef:	68 fc 03 00 00       	push   $0x3fc
801077f4:	e8 81 ff ff ff       	call   8010777a <outb>
801077f9:	83 c4 08             	add    $0x8,%esp
  outb(COM1+1, 0x01);    // Enable receive interrupts.
801077fc:	6a 01                	push   $0x1
801077fe:	68 f9 03 00 00       	push   $0x3f9
80107803:	e8 72 ff ff ff       	call   8010777a <outb>
80107808:	83 c4 08             	add    $0x8,%esp

  // If status is 0xFF, no serial port.
  if(inb(COM1+5) == 0xFF)
8010780b:	68 fd 03 00 00       	push   $0x3fd
80107810:	e8 48 ff ff ff       	call   8010775d <inb>
80107815:	83 c4 04             	add    $0x4,%esp
80107818:	3c ff                	cmp    $0xff,%al
8010781a:	74 6e                	je     8010788a <uartinit+0xf1>
    return;
  uart = 1;
8010781c:	c7 05 6c c6 10 80 01 	movl   $0x1,0x8010c66c
80107823:	00 00 00 

  // Acknowledge pre-existing interrupt conditions;
  // enable interrupts.
  inb(COM1+2);
80107826:	68 fa 03 00 00       	push   $0x3fa
8010782b:	e8 2d ff ff ff       	call   8010775d <inb>
80107830:	83 c4 04             	add    $0x4,%esp
  inb(COM1+0);
80107833:	68 f8 03 00 00       	push   $0x3f8
80107838:	e8 20 ff ff ff       	call   8010775d <inb>
8010783d:	83 c4 04             	add    $0x4,%esp
  picenable(IRQ_COM1);
80107840:	83 ec 0c             	sub    $0xc,%esp
80107843:	6a 04                	push   $0x4
80107845:	e8 7f c7 ff ff       	call   80103fc9 <picenable>
8010784a:	83 c4 10             	add    $0x10,%esp
  ioapicenable(IRQ_COM1, 0);
8010784d:	83 ec 08             	sub    $0x8,%esp
80107850:	6a 00                	push   $0x0
80107852:	6a 04                	push   $0x4
80107854:	e8 1f b3 ff ff       	call   80102b78 <ioapicenable>
80107859:	83 c4 10             	add    $0x10,%esp
  
  // Announce that we're here.
  for(p="xv6...\n"; *p; p++)
8010785c:	c7 45 f4 90 99 10 80 	movl   $0x80109990,-0xc(%ebp)
80107863:	eb 19                	jmp    8010787e <uartinit+0xe5>
    uartputc(*p);
80107865:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107868:	0f b6 00             	movzbl (%eax),%eax
8010786b:	0f be c0             	movsbl %al,%eax
8010786e:	83 ec 0c             	sub    $0xc,%esp
80107871:	50                   	push   %eax
80107872:	e8 16 00 00 00       	call   8010788d <uartputc>
80107877:	83 c4 10             	add    $0x10,%esp
  inb(COM1+0);
  picenable(IRQ_COM1);
  ioapicenable(IRQ_COM1, 0);
  
  // Announce that we're here.
  for(p="xv6...\n"; *p; p++)
8010787a:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
8010787e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107881:	0f b6 00             	movzbl (%eax),%eax
80107884:	84 c0                	test   %al,%al
80107886:	75 dd                	jne    80107865 <uartinit+0xcc>
80107888:	eb 01                	jmp    8010788b <uartinit+0xf2>
  outb(COM1+4, 0);
  outb(COM1+1, 0x01);    // Enable receive interrupts.

  // If status is 0xFF, no serial port.
  if(inb(COM1+5) == 0xFF)
    return;
8010788a:	90                   	nop
  ioapicenable(IRQ_COM1, 0);
  
  // Announce that we're here.
  for(p="xv6...\n"; *p; p++)
    uartputc(*p);
}
8010788b:	c9                   	leave  
8010788c:	c3                   	ret    

8010788d <uartputc>:

void
uartputc(int c)
{
8010788d:	55                   	push   %ebp
8010788e:	89 e5                	mov    %esp,%ebp
80107890:	83 ec 18             	sub    $0x18,%esp
  int i;

  if(!uart)
80107893:	a1 6c c6 10 80       	mov    0x8010c66c,%eax
80107898:	85 c0                	test   %eax,%eax
8010789a:	74 53                	je     801078ef <uartputc+0x62>
    return;
  for(i = 0; i < 128 && !(inb(COM1+5) & 0x20); i++)
8010789c:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
801078a3:	eb 11                	jmp    801078b6 <uartputc+0x29>
    microdelay(10);
801078a5:	83 ec 0c             	sub    $0xc,%esp
801078a8:	6a 0a                	push   $0xa
801078aa:	e8 2f b8 ff ff       	call   801030de <microdelay>
801078af:	83 c4 10             	add    $0x10,%esp
{
  int i;

  if(!uart)
    return;
  for(i = 0; i < 128 && !(inb(COM1+5) & 0x20); i++)
801078b2:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
801078b6:	83 7d f4 7f          	cmpl   $0x7f,-0xc(%ebp)
801078ba:	7f 1a                	jg     801078d6 <uartputc+0x49>
801078bc:	83 ec 0c             	sub    $0xc,%esp
801078bf:	68 fd 03 00 00       	push   $0x3fd
801078c4:	e8 94 fe ff ff       	call   8010775d <inb>
801078c9:	83 c4 10             	add    $0x10,%esp
801078cc:	0f b6 c0             	movzbl %al,%eax
801078cf:	83 e0 20             	and    $0x20,%eax
801078d2:	85 c0                	test   %eax,%eax
801078d4:	74 cf                	je     801078a5 <uartputc+0x18>
    microdelay(10);
  outb(COM1+0, c);
801078d6:	8b 45 08             	mov    0x8(%ebp),%eax
801078d9:	0f b6 c0             	movzbl %al,%eax
801078dc:	83 ec 08             	sub    $0x8,%esp
801078df:	50                   	push   %eax
801078e0:	68 f8 03 00 00       	push   $0x3f8
801078e5:	e8 90 fe ff ff       	call   8010777a <outb>
801078ea:	83 c4 10             	add    $0x10,%esp
801078ed:	eb 01                	jmp    801078f0 <uartputc+0x63>
uartputc(int c)
{
  int i;

  if(!uart)
    return;
801078ef:	90                   	nop
  for(i = 0; i < 128 && !(inb(COM1+5) & 0x20); i++)
    microdelay(10);
  outb(COM1+0, c);
}
801078f0:	c9                   	leave  
801078f1:	c3                   	ret    

801078f2 <uartgetc>:

static int
uartgetc(void)
{
801078f2:	55                   	push   %ebp
801078f3:	89 e5                	mov    %esp,%ebp
  if(!uart)
801078f5:	a1 6c c6 10 80       	mov    0x8010c66c,%eax
801078fa:	85 c0                	test   %eax,%eax
801078fc:	75 07                	jne    80107905 <uartgetc+0x13>
    return -1;
801078fe:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80107903:	eb 2e                	jmp    80107933 <uartgetc+0x41>
  if(!(inb(COM1+5) & 0x01))
80107905:	68 fd 03 00 00       	push   $0x3fd
8010790a:	e8 4e fe ff ff       	call   8010775d <inb>
8010790f:	83 c4 04             	add    $0x4,%esp
80107912:	0f b6 c0             	movzbl %al,%eax
80107915:	83 e0 01             	and    $0x1,%eax
80107918:	85 c0                	test   %eax,%eax
8010791a:	75 07                	jne    80107923 <uartgetc+0x31>
    return -1;
8010791c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80107921:	eb 10                	jmp    80107933 <uartgetc+0x41>
  return inb(COM1+0);
80107923:	68 f8 03 00 00       	push   $0x3f8
80107928:	e8 30 fe ff ff       	call   8010775d <inb>
8010792d:	83 c4 04             	add    $0x4,%esp
80107930:	0f b6 c0             	movzbl %al,%eax
}
80107933:	c9                   	leave  
80107934:	c3                   	ret    

80107935 <uartintr>:

void
uartintr(void)
{
80107935:	55                   	push   %ebp
80107936:	89 e5                	mov    %esp,%ebp
80107938:	83 ec 08             	sub    $0x8,%esp
  consoleintr(uartgetc);
8010793b:	83 ec 0c             	sub    $0xc,%esp
8010793e:	68 f2 78 10 80       	push   $0x801078f2
80107943:	e8 b1 8e ff ff       	call   801007f9 <consoleintr>
80107948:	83 c4 10             	add    $0x10,%esp
}
8010794b:	90                   	nop
8010794c:	c9                   	leave  
8010794d:	c3                   	ret    

8010794e <vector0>:
# generated by vectors.pl - do not edit
# handlers
.globl alltraps
.globl vector0
vector0:
  pushl $0
8010794e:	6a 00                	push   $0x0
  pushl $0
80107950:	6a 00                	push   $0x0
  jmp alltraps
80107952:	e9 cc f9 ff ff       	jmp    80107323 <alltraps>

80107957 <vector1>:
.globl vector1
vector1:
  pushl $0
80107957:	6a 00                	push   $0x0
  pushl $1
80107959:	6a 01                	push   $0x1
  jmp alltraps
8010795b:	e9 c3 f9 ff ff       	jmp    80107323 <alltraps>

80107960 <vector2>:
.globl vector2
vector2:
  pushl $0
80107960:	6a 00                	push   $0x0
  pushl $2
80107962:	6a 02                	push   $0x2
  jmp alltraps
80107964:	e9 ba f9 ff ff       	jmp    80107323 <alltraps>

80107969 <vector3>:
.globl vector3
vector3:
  pushl $0
80107969:	6a 00                	push   $0x0
  pushl $3
8010796b:	6a 03                	push   $0x3
  jmp alltraps
8010796d:	e9 b1 f9 ff ff       	jmp    80107323 <alltraps>

80107972 <vector4>:
.globl vector4
vector4:
  pushl $0
80107972:	6a 00                	push   $0x0
  pushl $4
80107974:	6a 04                	push   $0x4
  jmp alltraps
80107976:	e9 a8 f9 ff ff       	jmp    80107323 <alltraps>

8010797b <vector5>:
.globl vector5
vector5:
  pushl $0
8010797b:	6a 00                	push   $0x0
  pushl $5
8010797d:	6a 05                	push   $0x5
  jmp alltraps
8010797f:	e9 9f f9 ff ff       	jmp    80107323 <alltraps>

80107984 <vector6>:
.globl vector6
vector6:
  pushl $0
80107984:	6a 00                	push   $0x0
  pushl $6
80107986:	6a 06                	push   $0x6
  jmp alltraps
80107988:	e9 96 f9 ff ff       	jmp    80107323 <alltraps>

8010798d <vector7>:
.globl vector7
vector7:
  pushl $0
8010798d:	6a 00                	push   $0x0
  pushl $7
8010798f:	6a 07                	push   $0x7
  jmp alltraps
80107991:	e9 8d f9 ff ff       	jmp    80107323 <alltraps>

80107996 <vector8>:
.globl vector8
vector8:
  pushl $8
80107996:	6a 08                	push   $0x8
  jmp alltraps
80107998:	e9 86 f9 ff ff       	jmp    80107323 <alltraps>

8010799d <vector9>:
.globl vector9
vector9:
  pushl $0
8010799d:	6a 00                	push   $0x0
  pushl $9
8010799f:	6a 09                	push   $0x9
  jmp alltraps
801079a1:	e9 7d f9 ff ff       	jmp    80107323 <alltraps>

801079a6 <vector10>:
.globl vector10
vector10:
  pushl $10
801079a6:	6a 0a                	push   $0xa
  jmp alltraps
801079a8:	e9 76 f9 ff ff       	jmp    80107323 <alltraps>

801079ad <vector11>:
.globl vector11
vector11:
  pushl $11
801079ad:	6a 0b                	push   $0xb
  jmp alltraps
801079af:	e9 6f f9 ff ff       	jmp    80107323 <alltraps>

801079b4 <vector12>:
.globl vector12
vector12:
  pushl $12
801079b4:	6a 0c                	push   $0xc
  jmp alltraps
801079b6:	e9 68 f9 ff ff       	jmp    80107323 <alltraps>

801079bb <vector13>:
.globl vector13
vector13:
  pushl $13
801079bb:	6a 0d                	push   $0xd
  jmp alltraps
801079bd:	e9 61 f9 ff ff       	jmp    80107323 <alltraps>

801079c2 <vector14>:
.globl vector14
vector14:
  pushl $14
801079c2:	6a 0e                	push   $0xe
  jmp alltraps
801079c4:	e9 5a f9 ff ff       	jmp    80107323 <alltraps>

801079c9 <vector15>:
.globl vector15
vector15:
  pushl $0
801079c9:	6a 00                	push   $0x0
  pushl $15
801079cb:	6a 0f                	push   $0xf
  jmp alltraps
801079cd:	e9 51 f9 ff ff       	jmp    80107323 <alltraps>

801079d2 <vector16>:
.globl vector16
vector16:
  pushl $0
801079d2:	6a 00                	push   $0x0
  pushl $16
801079d4:	6a 10                	push   $0x10
  jmp alltraps
801079d6:	e9 48 f9 ff ff       	jmp    80107323 <alltraps>

801079db <vector17>:
.globl vector17
vector17:
  pushl $17
801079db:	6a 11                	push   $0x11
  jmp alltraps
801079dd:	e9 41 f9 ff ff       	jmp    80107323 <alltraps>

801079e2 <vector18>:
.globl vector18
vector18:
  pushl $0
801079e2:	6a 00                	push   $0x0
  pushl $18
801079e4:	6a 12                	push   $0x12
  jmp alltraps
801079e6:	e9 38 f9 ff ff       	jmp    80107323 <alltraps>

801079eb <vector19>:
.globl vector19
vector19:
  pushl $0
801079eb:	6a 00                	push   $0x0
  pushl $19
801079ed:	6a 13                	push   $0x13
  jmp alltraps
801079ef:	e9 2f f9 ff ff       	jmp    80107323 <alltraps>

801079f4 <vector20>:
.globl vector20
vector20:
  pushl $0
801079f4:	6a 00                	push   $0x0
  pushl $20
801079f6:	6a 14                	push   $0x14
  jmp alltraps
801079f8:	e9 26 f9 ff ff       	jmp    80107323 <alltraps>

801079fd <vector21>:
.globl vector21
vector21:
  pushl $0
801079fd:	6a 00                	push   $0x0
  pushl $21
801079ff:	6a 15                	push   $0x15
  jmp alltraps
80107a01:	e9 1d f9 ff ff       	jmp    80107323 <alltraps>

80107a06 <vector22>:
.globl vector22
vector22:
  pushl $0
80107a06:	6a 00                	push   $0x0
  pushl $22
80107a08:	6a 16                	push   $0x16
  jmp alltraps
80107a0a:	e9 14 f9 ff ff       	jmp    80107323 <alltraps>

80107a0f <vector23>:
.globl vector23
vector23:
  pushl $0
80107a0f:	6a 00                	push   $0x0
  pushl $23
80107a11:	6a 17                	push   $0x17
  jmp alltraps
80107a13:	e9 0b f9 ff ff       	jmp    80107323 <alltraps>

80107a18 <vector24>:
.globl vector24
vector24:
  pushl $0
80107a18:	6a 00                	push   $0x0
  pushl $24
80107a1a:	6a 18                	push   $0x18
  jmp alltraps
80107a1c:	e9 02 f9 ff ff       	jmp    80107323 <alltraps>

80107a21 <vector25>:
.globl vector25
vector25:
  pushl $0
80107a21:	6a 00                	push   $0x0
  pushl $25
80107a23:	6a 19                	push   $0x19
  jmp alltraps
80107a25:	e9 f9 f8 ff ff       	jmp    80107323 <alltraps>

80107a2a <vector26>:
.globl vector26
vector26:
  pushl $0
80107a2a:	6a 00                	push   $0x0
  pushl $26
80107a2c:	6a 1a                	push   $0x1a
  jmp alltraps
80107a2e:	e9 f0 f8 ff ff       	jmp    80107323 <alltraps>

80107a33 <vector27>:
.globl vector27
vector27:
  pushl $0
80107a33:	6a 00                	push   $0x0
  pushl $27
80107a35:	6a 1b                	push   $0x1b
  jmp alltraps
80107a37:	e9 e7 f8 ff ff       	jmp    80107323 <alltraps>

80107a3c <vector28>:
.globl vector28
vector28:
  pushl $0
80107a3c:	6a 00                	push   $0x0
  pushl $28
80107a3e:	6a 1c                	push   $0x1c
  jmp alltraps
80107a40:	e9 de f8 ff ff       	jmp    80107323 <alltraps>

80107a45 <vector29>:
.globl vector29
vector29:
  pushl $0
80107a45:	6a 00                	push   $0x0
  pushl $29
80107a47:	6a 1d                	push   $0x1d
  jmp alltraps
80107a49:	e9 d5 f8 ff ff       	jmp    80107323 <alltraps>

80107a4e <vector30>:
.globl vector30
vector30:
  pushl $0
80107a4e:	6a 00                	push   $0x0
  pushl $30
80107a50:	6a 1e                	push   $0x1e
  jmp alltraps
80107a52:	e9 cc f8 ff ff       	jmp    80107323 <alltraps>

80107a57 <vector31>:
.globl vector31
vector31:
  pushl $0
80107a57:	6a 00                	push   $0x0
  pushl $31
80107a59:	6a 1f                	push   $0x1f
  jmp alltraps
80107a5b:	e9 c3 f8 ff ff       	jmp    80107323 <alltraps>

80107a60 <vector32>:
.globl vector32
vector32:
  pushl $0
80107a60:	6a 00                	push   $0x0
  pushl $32
80107a62:	6a 20                	push   $0x20
  jmp alltraps
80107a64:	e9 ba f8 ff ff       	jmp    80107323 <alltraps>

80107a69 <vector33>:
.globl vector33
vector33:
  pushl $0
80107a69:	6a 00                	push   $0x0
  pushl $33
80107a6b:	6a 21                	push   $0x21
  jmp alltraps
80107a6d:	e9 b1 f8 ff ff       	jmp    80107323 <alltraps>

80107a72 <vector34>:
.globl vector34
vector34:
  pushl $0
80107a72:	6a 00                	push   $0x0
  pushl $34
80107a74:	6a 22                	push   $0x22
  jmp alltraps
80107a76:	e9 a8 f8 ff ff       	jmp    80107323 <alltraps>

80107a7b <vector35>:
.globl vector35
vector35:
  pushl $0
80107a7b:	6a 00                	push   $0x0
  pushl $35
80107a7d:	6a 23                	push   $0x23
  jmp alltraps
80107a7f:	e9 9f f8 ff ff       	jmp    80107323 <alltraps>

80107a84 <vector36>:
.globl vector36
vector36:
  pushl $0
80107a84:	6a 00                	push   $0x0
  pushl $36
80107a86:	6a 24                	push   $0x24
  jmp alltraps
80107a88:	e9 96 f8 ff ff       	jmp    80107323 <alltraps>

80107a8d <vector37>:
.globl vector37
vector37:
  pushl $0
80107a8d:	6a 00                	push   $0x0
  pushl $37
80107a8f:	6a 25                	push   $0x25
  jmp alltraps
80107a91:	e9 8d f8 ff ff       	jmp    80107323 <alltraps>

80107a96 <vector38>:
.globl vector38
vector38:
  pushl $0
80107a96:	6a 00                	push   $0x0
  pushl $38
80107a98:	6a 26                	push   $0x26
  jmp alltraps
80107a9a:	e9 84 f8 ff ff       	jmp    80107323 <alltraps>

80107a9f <vector39>:
.globl vector39
vector39:
  pushl $0
80107a9f:	6a 00                	push   $0x0
  pushl $39
80107aa1:	6a 27                	push   $0x27
  jmp alltraps
80107aa3:	e9 7b f8 ff ff       	jmp    80107323 <alltraps>

80107aa8 <vector40>:
.globl vector40
vector40:
  pushl $0
80107aa8:	6a 00                	push   $0x0
  pushl $40
80107aaa:	6a 28                	push   $0x28
  jmp alltraps
80107aac:	e9 72 f8 ff ff       	jmp    80107323 <alltraps>

80107ab1 <vector41>:
.globl vector41
vector41:
  pushl $0
80107ab1:	6a 00                	push   $0x0
  pushl $41
80107ab3:	6a 29                	push   $0x29
  jmp alltraps
80107ab5:	e9 69 f8 ff ff       	jmp    80107323 <alltraps>

80107aba <vector42>:
.globl vector42
vector42:
  pushl $0
80107aba:	6a 00                	push   $0x0
  pushl $42
80107abc:	6a 2a                	push   $0x2a
  jmp alltraps
80107abe:	e9 60 f8 ff ff       	jmp    80107323 <alltraps>

80107ac3 <vector43>:
.globl vector43
vector43:
  pushl $0
80107ac3:	6a 00                	push   $0x0
  pushl $43
80107ac5:	6a 2b                	push   $0x2b
  jmp alltraps
80107ac7:	e9 57 f8 ff ff       	jmp    80107323 <alltraps>

80107acc <vector44>:
.globl vector44
vector44:
  pushl $0
80107acc:	6a 00                	push   $0x0
  pushl $44
80107ace:	6a 2c                	push   $0x2c
  jmp alltraps
80107ad0:	e9 4e f8 ff ff       	jmp    80107323 <alltraps>

80107ad5 <vector45>:
.globl vector45
vector45:
  pushl $0
80107ad5:	6a 00                	push   $0x0
  pushl $45
80107ad7:	6a 2d                	push   $0x2d
  jmp alltraps
80107ad9:	e9 45 f8 ff ff       	jmp    80107323 <alltraps>

80107ade <vector46>:
.globl vector46
vector46:
  pushl $0
80107ade:	6a 00                	push   $0x0
  pushl $46
80107ae0:	6a 2e                	push   $0x2e
  jmp alltraps
80107ae2:	e9 3c f8 ff ff       	jmp    80107323 <alltraps>

80107ae7 <vector47>:
.globl vector47
vector47:
  pushl $0
80107ae7:	6a 00                	push   $0x0
  pushl $47
80107ae9:	6a 2f                	push   $0x2f
  jmp alltraps
80107aeb:	e9 33 f8 ff ff       	jmp    80107323 <alltraps>

80107af0 <vector48>:
.globl vector48
vector48:
  pushl $0
80107af0:	6a 00                	push   $0x0
  pushl $48
80107af2:	6a 30                	push   $0x30
  jmp alltraps
80107af4:	e9 2a f8 ff ff       	jmp    80107323 <alltraps>

80107af9 <vector49>:
.globl vector49
vector49:
  pushl $0
80107af9:	6a 00                	push   $0x0
  pushl $49
80107afb:	6a 31                	push   $0x31
  jmp alltraps
80107afd:	e9 21 f8 ff ff       	jmp    80107323 <alltraps>

80107b02 <vector50>:
.globl vector50
vector50:
  pushl $0
80107b02:	6a 00                	push   $0x0
  pushl $50
80107b04:	6a 32                	push   $0x32
  jmp alltraps
80107b06:	e9 18 f8 ff ff       	jmp    80107323 <alltraps>

80107b0b <vector51>:
.globl vector51
vector51:
  pushl $0
80107b0b:	6a 00                	push   $0x0
  pushl $51
80107b0d:	6a 33                	push   $0x33
  jmp alltraps
80107b0f:	e9 0f f8 ff ff       	jmp    80107323 <alltraps>

80107b14 <vector52>:
.globl vector52
vector52:
  pushl $0
80107b14:	6a 00                	push   $0x0
  pushl $52
80107b16:	6a 34                	push   $0x34
  jmp alltraps
80107b18:	e9 06 f8 ff ff       	jmp    80107323 <alltraps>

80107b1d <vector53>:
.globl vector53
vector53:
  pushl $0
80107b1d:	6a 00                	push   $0x0
  pushl $53
80107b1f:	6a 35                	push   $0x35
  jmp alltraps
80107b21:	e9 fd f7 ff ff       	jmp    80107323 <alltraps>

80107b26 <vector54>:
.globl vector54
vector54:
  pushl $0
80107b26:	6a 00                	push   $0x0
  pushl $54
80107b28:	6a 36                	push   $0x36
  jmp alltraps
80107b2a:	e9 f4 f7 ff ff       	jmp    80107323 <alltraps>

80107b2f <vector55>:
.globl vector55
vector55:
  pushl $0
80107b2f:	6a 00                	push   $0x0
  pushl $55
80107b31:	6a 37                	push   $0x37
  jmp alltraps
80107b33:	e9 eb f7 ff ff       	jmp    80107323 <alltraps>

80107b38 <vector56>:
.globl vector56
vector56:
  pushl $0
80107b38:	6a 00                	push   $0x0
  pushl $56
80107b3a:	6a 38                	push   $0x38
  jmp alltraps
80107b3c:	e9 e2 f7 ff ff       	jmp    80107323 <alltraps>

80107b41 <vector57>:
.globl vector57
vector57:
  pushl $0
80107b41:	6a 00                	push   $0x0
  pushl $57
80107b43:	6a 39                	push   $0x39
  jmp alltraps
80107b45:	e9 d9 f7 ff ff       	jmp    80107323 <alltraps>

80107b4a <vector58>:
.globl vector58
vector58:
  pushl $0
80107b4a:	6a 00                	push   $0x0
  pushl $58
80107b4c:	6a 3a                	push   $0x3a
  jmp alltraps
80107b4e:	e9 d0 f7 ff ff       	jmp    80107323 <alltraps>

80107b53 <vector59>:
.globl vector59
vector59:
  pushl $0
80107b53:	6a 00                	push   $0x0
  pushl $59
80107b55:	6a 3b                	push   $0x3b
  jmp alltraps
80107b57:	e9 c7 f7 ff ff       	jmp    80107323 <alltraps>

80107b5c <vector60>:
.globl vector60
vector60:
  pushl $0
80107b5c:	6a 00                	push   $0x0
  pushl $60
80107b5e:	6a 3c                	push   $0x3c
  jmp alltraps
80107b60:	e9 be f7 ff ff       	jmp    80107323 <alltraps>

80107b65 <vector61>:
.globl vector61
vector61:
  pushl $0
80107b65:	6a 00                	push   $0x0
  pushl $61
80107b67:	6a 3d                	push   $0x3d
  jmp alltraps
80107b69:	e9 b5 f7 ff ff       	jmp    80107323 <alltraps>

80107b6e <vector62>:
.globl vector62
vector62:
  pushl $0
80107b6e:	6a 00                	push   $0x0
  pushl $62
80107b70:	6a 3e                	push   $0x3e
  jmp alltraps
80107b72:	e9 ac f7 ff ff       	jmp    80107323 <alltraps>

80107b77 <vector63>:
.globl vector63
vector63:
  pushl $0
80107b77:	6a 00                	push   $0x0
  pushl $63
80107b79:	6a 3f                	push   $0x3f
  jmp alltraps
80107b7b:	e9 a3 f7 ff ff       	jmp    80107323 <alltraps>

80107b80 <vector64>:
.globl vector64
vector64:
  pushl $0
80107b80:	6a 00                	push   $0x0
  pushl $64
80107b82:	6a 40                	push   $0x40
  jmp alltraps
80107b84:	e9 9a f7 ff ff       	jmp    80107323 <alltraps>

80107b89 <vector65>:
.globl vector65
vector65:
  pushl $0
80107b89:	6a 00                	push   $0x0
  pushl $65
80107b8b:	6a 41                	push   $0x41
  jmp alltraps
80107b8d:	e9 91 f7 ff ff       	jmp    80107323 <alltraps>

80107b92 <vector66>:
.globl vector66
vector66:
  pushl $0
80107b92:	6a 00                	push   $0x0
  pushl $66
80107b94:	6a 42                	push   $0x42
  jmp alltraps
80107b96:	e9 88 f7 ff ff       	jmp    80107323 <alltraps>

80107b9b <vector67>:
.globl vector67
vector67:
  pushl $0
80107b9b:	6a 00                	push   $0x0
  pushl $67
80107b9d:	6a 43                	push   $0x43
  jmp alltraps
80107b9f:	e9 7f f7 ff ff       	jmp    80107323 <alltraps>

80107ba4 <vector68>:
.globl vector68
vector68:
  pushl $0
80107ba4:	6a 00                	push   $0x0
  pushl $68
80107ba6:	6a 44                	push   $0x44
  jmp alltraps
80107ba8:	e9 76 f7 ff ff       	jmp    80107323 <alltraps>

80107bad <vector69>:
.globl vector69
vector69:
  pushl $0
80107bad:	6a 00                	push   $0x0
  pushl $69
80107baf:	6a 45                	push   $0x45
  jmp alltraps
80107bb1:	e9 6d f7 ff ff       	jmp    80107323 <alltraps>

80107bb6 <vector70>:
.globl vector70
vector70:
  pushl $0
80107bb6:	6a 00                	push   $0x0
  pushl $70
80107bb8:	6a 46                	push   $0x46
  jmp alltraps
80107bba:	e9 64 f7 ff ff       	jmp    80107323 <alltraps>

80107bbf <vector71>:
.globl vector71
vector71:
  pushl $0
80107bbf:	6a 00                	push   $0x0
  pushl $71
80107bc1:	6a 47                	push   $0x47
  jmp alltraps
80107bc3:	e9 5b f7 ff ff       	jmp    80107323 <alltraps>

80107bc8 <vector72>:
.globl vector72
vector72:
  pushl $0
80107bc8:	6a 00                	push   $0x0
  pushl $72
80107bca:	6a 48                	push   $0x48
  jmp alltraps
80107bcc:	e9 52 f7 ff ff       	jmp    80107323 <alltraps>

80107bd1 <vector73>:
.globl vector73
vector73:
  pushl $0
80107bd1:	6a 00                	push   $0x0
  pushl $73
80107bd3:	6a 49                	push   $0x49
  jmp alltraps
80107bd5:	e9 49 f7 ff ff       	jmp    80107323 <alltraps>

80107bda <vector74>:
.globl vector74
vector74:
  pushl $0
80107bda:	6a 00                	push   $0x0
  pushl $74
80107bdc:	6a 4a                	push   $0x4a
  jmp alltraps
80107bde:	e9 40 f7 ff ff       	jmp    80107323 <alltraps>

80107be3 <vector75>:
.globl vector75
vector75:
  pushl $0
80107be3:	6a 00                	push   $0x0
  pushl $75
80107be5:	6a 4b                	push   $0x4b
  jmp alltraps
80107be7:	e9 37 f7 ff ff       	jmp    80107323 <alltraps>

80107bec <vector76>:
.globl vector76
vector76:
  pushl $0
80107bec:	6a 00                	push   $0x0
  pushl $76
80107bee:	6a 4c                	push   $0x4c
  jmp alltraps
80107bf0:	e9 2e f7 ff ff       	jmp    80107323 <alltraps>

80107bf5 <vector77>:
.globl vector77
vector77:
  pushl $0
80107bf5:	6a 00                	push   $0x0
  pushl $77
80107bf7:	6a 4d                	push   $0x4d
  jmp alltraps
80107bf9:	e9 25 f7 ff ff       	jmp    80107323 <alltraps>

80107bfe <vector78>:
.globl vector78
vector78:
  pushl $0
80107bfe:	6a 00                	push   $0x0
  pushl $78
80107c00:	6a 4e                	push   $0x4e
  jmp alltraps
80107c02:	e9 1c f7 ff ff       	jmp    80107323 <alltraps>

80107c07 <vector79>:
.globl vector79
vector79:
  pushl $0
80107c07:	6a 00                	push   $0x0
  pushl $79
80107c09:	6a 4f                	push   $0x4f
  jmp alltraps
80107c0b:	e9 13 f7 ff ff       	jmp    80107323 <alltraps>

80107c10 <vector80>:
.globl vector80
vector80:
  pushl $0
80107c10:	6a 00                	push   $0x0
  pushl $80
80107c12:	6a 50                	push   $0x50
  jmp alltraps
80107c14:	e9 0a f7 ff ff       	jmp    80107323 <alltraps>

80107c19 <vector81>:
.globl vector81
vector81:
  pushl $0
80107c19:	6a 00                	push   $0x0
  pushl $81
80107c1b:	6a 51                	push   $0x51
  jmp alltraps
80107c1d:	e9 01 f7 ff ff       	jmp    80107323 <alltraps>

80107c22 <vector82>:
.globl vector82
vector82:
  pushl $0
80107c22:	6a 00                	push   $0x0
  pushl $82
80107c24:	6a 52                	push   $0x52
  jmp alltraps
80107c26:	e9 f8 f6 ff ff       	jmp    80107323 <alltraps>

80107c2b <vector83>:
.globl vector83
vector83:
  pushl $0
80107c2b:	6a 00                	push   $0x0
  pushl $83
80107c2d:	6a 53                	push   $0x53
  jmp alltraps
80107c2f:	e9 ef f6 ff ff       	jmp    80107323 <alltraps>

80107c34 <vector84>:
.globl vector84
vector84:
  pushl $0
80107c34:	6a 00                	push   $0x0
  pushl $84
80107c36:	6a 54                	push   $0x54
  jmp alltraps
80107c38:	e9 e6 f6 ff ff       	jmp    80107323 <alltraps>

80107c3d <vector85>:
.globl vector85
vector85:
  pushl $0
80107c3d:	6a 00                	push   $0x0
  pushl $85
80107c3f:	6a 55                	push   $0x55
  jmp alltraps
80107c41:	e9 dd f6 ff ff       	jmp    80107323 <alltraps>

80107c46 <vector86>:
.globl vector86
vector86:
  pushl $0
80107c46:	6a 00                	push   $0x0
  pushl $86
80107c48:	6a 56                	push   $0x56
  jmp alltraps
80107c4a:	e9 d4 f6 ff ff       	jmp    80107323 <alltraps>

80107c4f <vector87>:
.globl vector87
vector87:
  pushl $0
80107c4f:	6a 00                	push   $0x0
  pushl $87
80107c51:	6a 57                	push   $0x57
  jmp alltraps
80107c53:	e9 cb f6 ff ff       	jmp    80107323 <alltraps>

80107c58 <vector88>:
.globl vector88
vector88:
  pushl $0
80107c58:	6a 00                	push   $0x0
  pushl $88
80107c5a:	6a 58                	push   $0x58
  jmp alltraps
80107c5c:	e9 c2 f6 ff ff       	jmp    80107323 <alltraps>

80107c61 <vector89>:
.globl vector89
vector89:
  pushl $0
80107c61:	6a 00                	push   $0x0
  pushl $89
80107c63:	6a 59                	push   $0x59
  jmp alltraps
80107c65:	e9 b9 f6 ff ff       	jmp    80107323 <alltraps>

80107c6a <vector90>:
.globl vector90
vector90:
  pushl $0
80107c6a:	6a 00                	push   $0x0
  pushl $90
80107c6c:	6a 5a                	push   $0x5a
  jmp alltraps
80107c6e:	e9 b0 f6 ff ff       	jmp    80107323 <alltraps>

80107c73 <vector91>:
.globl vector91
vector91:
  pushl $0
80107c73:	6a 00                	push   $0x0
  pushl $91
80107c75:	6a 5b                	push   $0x5b
  jmp alltraps
80107c77:	e9 a7 f6 ff ff       	jmp    80107323 <alltraps>

80107c7c <vector92>:
.globl vector92
vector92:
  pushl $0
80107c7c:	6a 00                	push   $0x0
  pushl $92
80107c7e:	6a 5c                	push   $0x5c
  jmp alltraps
80107c80:	e9 9e f6 ff ff       	jmp    80107323 <alltraps>

80107c85 <vector93>:
.globl vector93
vector93:
  pushl $0
80107c85:	6a 00                	push   $0x0
  pushl $93
80107c87:	6a 5d                	push   $0x5d
  jmp alltraps
80107c89:	e9 95 f6 ff ff       	jmp    80107323 <alltraps>

80107c8e <vector94>:
.globl vector94
vector94:
  pushl $0
80107c8e:	6a 00                	push   $0x0
  pushl $94
80107c90:	6a 5e                	push   $0x5e
  jmp alltraps
80107c92:	e9 8c f6 ff ff       	jmp    80107323 <alltraps>

80107c97 <vector95>:
.globl vector95
vector95:
  pushl $0
80107c97:	6a 00                	push   $0x0
  pushl $95
80107c99:	6a 5f                	push   $0x5f
  jmp alltraps
80107c9b:	e9 83 f6 ff ff       	jmp    80107323 <alltraps>

80107ca0 <vector96>:
.globl vector96
vector96:
  pushl $0
80107ca0:	6a 00                	push   $0x0
  pushl $96
80107ca2:	6a 60                	push   $0x60
  jmp alltraps
80107ca4:	e9 7a f6 ff ff       	jmp    80107323 <alltraps>

80107ca9 <vector97>:
.globl vector97
vector97:
  pushl $0
80107ca9:	6a 00                	push   $0x0
  pushl $97
80107cab:	6a 61                	push   $0x61
  jmp alltraps
80107cad:	e9 71 f6 ff ff       	jmp    80107323 <alltraps>

80107cb2 <vector98>:
.globl vector98
vector98:
  pushl $0
80107cb2:	6a 00                	push   $0x0
  pushl $98
80107cb4:	6a 62                	push   $0x62
  jmp alltraps
80107cb6:	e9 68 f6 ff ff       	jmp    80107323 <alltraps>

80107cbb <vector99>:
.globl vector99
vector99:
  pushl $0
80107cbb:	6a 00                	push   $0x0
  pushl $99
80107cbd:	6a 63                	push   $0x63
  jmp alltraps
80107cbf:	e9 5f f6 ff ff       	jmp    80107323 <alltraps>

80107cc4 <vector100>:
.globl vector100
vector100:
  pushl $0
80107cc4:	6a 00                	push   $0x0
  pushl $100
80107cc6:	6a 64                	push   $0x64
  jmp alltraps
80107cc8:	e9 56 f6 ff ff       	jmp    80107323 <alltraps>

80107ccd <vector101>:
.globl vector101
vector101:
  pushl $0
80107ccd:	6a 00                	push   $0x0
  pushl $101
80107ccf:	6a 65                	push   $0x65
  jmp alltraps
80107cd1:	e9 4d f6 ff ff       	jmp    80107323 <alltraps>

80107cd6 <vector102>:
.globl vector102
vector102:
  pushl $0
80107cd6:	6a 00                	push   $0x0
  pushl $102
80107cd8:	6a 66                	push   $0x66
  jmp alltraps
80107cda:	e9 44 f6 ff ff       	jmp    80107323 <alltraps>

80107cdf <vector103>:
.globl vector103
vector103:
  pushl $0
80107cdf:	6a 00                	push   $0x0
  pushl $103
80107ce1:	6a 67                	push   $0x67
  jmp alltraps
80107ce3:	e9 3b f6 ff ff       	jmp    80107323 <alltraps>

80107ce8 <vector104>:
.globl vector104
vector104:
  pushl $0
80107ce8:	6a 00                	push   $0x0
  pushl $104
80107cea:	6a 68                	push   $0x68
  jmp alltraps
80107cec:	e9 32 f6 ff ff       	jmp    80107323 <alltraps>

80107cf1 <vector105>:
.globl vector105
vector105:
  pushl $0
80107cf1:	6a 00                	push   $0x0
  pushl $105
80107cf3:	6a 69                	push   $0x69
  jmp alltraps
80107cf5:	e9 29 f6 ff ff       	jmp    80107323 <alltraps>

80107cfa <vector106>:
.globl vector106
vector106:
  pushl $0
80107cfa:	6a 00                	push   $0x0
  pushl $106
80107cfc:	6a 6a                	push   $0x6a
  jmp alltraps
80107cfe:	e9 20 f6 ff ff       	jmp    80107323 <alltraps>

80107d03 <vector107>:
.globl vector107
vector107:
  pushl $0
80107d03:	6a 00                	push   $0x0
  pushl $107
80107d05:	6a 6b                	push   $0x6b
  jmp alltraps
80107d07:	e9 17 f6 ff ff       	jmp    80107323 <alltraps>

80107d0c <vector108>:
.globl vector108
vector108:
  pushl $0
80107d0c:	6a 00                	push   $0x0
  pushl $108
80107d0e:	6a 6c                	push   $0x6c
  jmp alltraps
80107d10:	e9 0e f6 ff ff       	jmp    80107323 <alltraps>

80107d15 <vector109>:
.globl vector109
vector109:
  pushl $0
80107d15:	6a 00                	push   $0x0
  pushl $109
80107d17:	6a 6d                	push   $0x6d
  jmp alltraps
80107d19:	e9 05 f6 ff ff       	jmp    80107323 <alltraps>

80107d1e <vector110>:
.globl vector110
vector110:
  pushl $0
80107d1e:	6a 00                	push   $0x0
  pushl $110
80107d20:	6a 6e                	push   $0x6e
  jmp alltraps
80107d22:	e9 fc f5 ff ff       	jmp    80107323 <alltraps>

80107d27 <vector111>:
.globl vector111
vector111:
  pushl $0
80107d27:	6a 00                	push   $0x0
  pushl $111
80107d29:	6a 6f                	push   $0x6f
  jmp alltraps
80107d2b:	e9 f3 f5 ff ff       	jmp    80107323 <alltraps>

80107d30 <vector112>:
.globl vector112
vector112:
  pushl $0
80107d30:	6a 00                	push   $0x0
  pushl $112
80107d32:	6a 70                	push   $0x70
  jmp alltraps
80107d34:	e9 ea f5 ff ff       	jmp    80107323 <alltraps>

80107d39 <vector113>:
.globl vector113
vector113:
  pushl $0
80107d39:	6a 00                	push   $0x0
  pushl $113
80107d3b:	6a 71                	push   $0x71
  jmp alltraps
80107d3d:	e9 e1 f5 ff ff       	jmp    80107323 <alltraps>

80107d42 <vector114>:
.globl vector114
vector114:
  pushl $0
80107d42:	6a 00                	push   $0x0
  pushl $114
80107d44:	6a 72                	push   $0x72
  jmp alltraps
80107d46:	e9 d8 f5 ff ff       	jmp    80107323 <alltraps>

80107d4b <vector115>:
.globl vector115
vector115:
  pushl $0
80107d4b:	6a 00                	push   $0x0
  pushl $115
80107d4d:	6a 73                	push   $0x73
  jmp alltraps
80107d4f:	e9 cf f5 ff ff       	jmp    80107323 <alltraps>

80107d54 <vector116>:
.globl vector116
vector116:
  pushl $0
80107d54:	6a 00                	push   $0x0
  pushl $116
80107d56:	6a 74                	push   $0x74
  jmp alltraps
80107d58:	e9 c6 f5 ff ff       	jmp    80107323 <alltraps>

80107d5d <vector117>:
.globl vector117
vector117:
  pushl $0
80107d5d:	6a 00                	push   $0x0
  pushl $117
80107d5f:	6a 75                	push   $0x75
  jmp alltraps
80107d61:	e9 bd f5 ff ff       	jmp    80107323 <alltraps>

80107d66 <vector118>:
.globl vector118
vector118:
  pushl $0
80107d66:	6a 00                	push   $0x0
  pushl $118
80107d68:	6a 76                	push   $0x76
  jmp alltraps
80107d6a:	e9 b4 f5 ff ff       	jmp    80107323 <alltraps>

80107d6f <vector119>:
.globl vector119
vector119:
  pushl $0
80107d6f:	6a 00                	push   $0x0
  pushl $119
80107d71:	6a 77                	push   $0x77
  jmp alltraps
80107d73:	e9 ab f5 ff ff       	jmp    80107323 <alltraps>

80107d78 <vector120>:
.globl vector120
vector120:
  pushl $0
80107d78:	6a 00                	push   $0x0
  pushl $120
80107d7a:	6a 78                	push   $0x78
  jmp alltraps
80107d7c:	e9 a2 f5 ff ff       	jmp    80107323 <alltraps>

80107d81 <vector121>:
.globl vector121
vector121:
  pushl $0
80107d81:	6a 00                	push   $0x0
  pushl $121
80107d83:	6a 79                	push   $0x79
  jmp alltraps
80107d85:	e9 99 f5 ff ff       	jmp    80107323 <alltraps>

80107d8a <vector122>:
.globl vector122
vector122:
  pushl $0
80107d8a:	6a 00                	push   $0x0
  pushl $122
80107d8c:	6a 7a                	push   $0x7a
  jmp alltraps
80107d8e:	e9 90 f5 ff ff       	jmp    80107323 <alltraps>

80107d93 <vector123>:
.globl vector123
vector123:
  pushl $0
80107d93:	6a 00                	push   $0x0
  pushl $123
80107d95:	6a 7b                	push   $0x7b
  jmp alltraps
80107d97:	e9 87 f5 ff ff       	jmp    80107323 <alltraps>

80107d9c <vector124>:
.globl vector124
vector124:
  pushl $0
80107d9c:	6a 00                	push   $0x0
  pushl $124
80107d9e:	6a 7c                	push   $0x7c
  jmp alltraps
80107da0:	e9 7e f5 ff ff       	jmp    80107323 <alltraps>

80107da5 <vector125>:
.globl vector125
vector125:
  pushl $0
80107da5:	6a 00                	push   $0x0
  pushl $125
80107da7:	6a 7d                	push   $0x7d
  jmp alltraps
80107da9:	e9 75 f5 ff ff       	jmp    80107323 <alltraps>

80107dae <vector126>:
.globl vector126
vector126:
  pushl $0
80107dae:	6a 00                	push   $0x0
  pushl $126
80107db0:	6a 7e                	push   $0x7e
  jmp alltraps
80107db2:	e9 6c f5 ff ff       	jmp    80107323 <alltraps>

80107db7 <vector127>:
.globl vector127
vector127:
  pushl $0
80107db7:	6a 00                	push   $0x0
  pushl $127
80107db9:	6a 7f                	push   $0x7f
  jmp alltraps
80107dbb:	e9 63 f5 ff ff       	jmp    80107323 <alltraps>

80107dc0 <vector128>:
.globl vector128
vector128:
  pushl $0
80107dc0:	6a 00                	push   $0x0
  pushl $128
80107dc2:	68 80 00 00 00       	push   $0x80
  jmp alltraps
80107dc7:	e9 57 f5 ff ff       	jmp    80107323 <alltraps>

80107dcc <vector129>:
.globl vector129
vector129:
  pushl $0
80107dcc:	6a 00                	push   $0x0
  pushl $129
80107dce:	68 81 00 00 00       	push   $0x81
  jmp alltraps
80107dd3:	e9 4b f5 ff ff       	jmp    80107323 <alltraps>

80107dd8 <vector130>:
.globl vector130
vector130:
  pushl $0
80107dd8:	6a 00                	push   $0x0
  pushl $130
80107dda:	68 82 00 00 00       	push   $0x82
  jmp alltraps
80107ddf:	e9 3f f5 ff ff       	jmp    80107323 <alltraps>

80107de4 <vector131>:
.globl vector131
vector131:
  pushl $0
80107de4:	6a 00                	push   $0x0
  pushl $131
80107de6:	68 83 00 00 00       	push   $0x83
  jmp alltraps
80107deb:	e9 33 f5 ff ff       	jmp    80107323 <alltraps>

80107df0 <vector132>:
.globl vector132
vector132:
  pushl $0
80107df0:	6a 00                	push   $0x0
  pushl $132
80107df2:	68 84 00 00 00       	push   $0x84
  jmp alltraps
80107df7:	e9 27 f5 ff ff       	jmp    80107323 <alltraps>

80107dfc <vector133>:
.globl vector133
vector133:
  pushl $0
80107dfc:	6a 00                	push   $0x0
  pushl $133
80107dfe:	68 85 00 00 00       	push   $0x85
  jmp alltraps
80107e03:	e9 1b f5 ff ff       	jmp    80107323 <alltraps>

80107e08 <vector134>:
.globl vector134
vector134:
  pushl $0
80107e08:	6a 00                	push   $0x0
  pushl $134
80107e0a:	68 86 00 00 00       	push   $0x86
  jmp alltraps
80107e0f:	e9 0f f5 ff ff       	jmp    80107323 <alltraps>

80107e14 <vector135>:
.globl vector135
vector135:
  pushl $0
80107e14:	6a 00                	push   $0x0
  pushl $135
80107e16:	68 87 00 00 00       	push   $0x87
  jmp alltraps
80107e1b:	e9 03 f5 ff ff       	jmp    80107323 <alltraps>

80107e20 <vector136>:
.globl vector136
vector136:
  pushl $0
80107e20:	6a 00                	push   $0x0
  pushl $136
80107e22:	68 88 00 00 00       	push   $0x88
  jmp alltraps
80107e27:	e9 f7 f4 ff ff       	jmp    80107323 <alltraps>

80107e2c <vector137>:
.globl vector137
vector137:
  pushl $0
80107e2c:	6a 00                	push   $0x0
  pushl $137
80107e2e:	68 89 00 00 00       	push   $0x89
  jmp alltraps
80107e33:	e9 eb f4 ff ff       	jmp    80107323 <alltraps>

80107e38 <vector138>:
.globl vector138
vector138:
  pushl $0
80107e38:	6a 00                	push   $0x0
  pushl $138
80107e3a:	68 8a 00 00 00       	push   $0x8a
  jmp alltraps
80107e3f:	e9 df f4 ff ff       	jmp    80107323 <alltraps>

80107e44 <vector139>:
.globl vector139
vector139:
  pushl $0
80107e44:	6a 00                	push   $0x0
  pushl $139
80107e46:	68 8b 00 00 00       	push   $0x8b
  jmp alltraps
80107e4b:	e9 d3 f4 ff ff       	jmp    80107323 <alltraps>

80107e50 <vector140>:
.globl vector140
vector140:
  pushl $0
80107e50:	6a 00                	push   $0x0
  pushl $140
80107e52:	68 8c 00 00 00       	push   $0x8c
  jmp alltraps
80107e57:	e9 c7 f4 ff ff       	jmp    80107323 <alltraps>

80107e5c <vector141>:
.globl vector141
vector141:
  pushl $0
80107e5c:	6a 00                	push   $0x0
  pushl $141
80107e5e:	68 8d 00 00 00       	push   $0x8d
  jmp alltraps
80107e63:	e9 bb f4 ff ff       	jmp    80107323 <alltraps>

80107e68 <vector142>:
.globl vector142
vector142:
  pushl $0
80107e68:	6a 00                	push   $0x0
  pushl $142
80107e6a:	68 8e 00 00 00       	push   $0x8e
  jmp alltraps
80107e6f:	e9 af f4 ff ff       	jmp    80107323 <alltraps>

80107e74 <vector143>:
.globl vector143
vector143:
  pushl $0
80107e74:	6a 00                	push   $0x0
  pushl $143
80107e76:	68 8f 00 00 00       	push   $0x8f
  jmp alltraps
80107e7b:	e9 a3 f4 ff ff       	jmp    80107323 <alltraps>

80107e80 <vector144>:
.globl vector144
vector144:
  pushl $0
80107e80:	6a 00                	push   $0x0
  pushl $144
80107e82:	68 90 00 00 00       	push   $0x90
  jmp alltraps
80107e87:	e9 97 f4 ff ff       	jmp    80107323 <alltraps>

80107e8c <vector145>:
.globl vector145
vector145:
  pushl $0
80107e8c:	6a 00                	push   $0x0
  pushl $145
80107e8e:	68 91 00 00 00       	push   $0x91
  jmp alltraps
80107e93:	e9 8b f4 ff ff       	jmp    80107323 <alltraps>

80107e98 <vector146>:
.globl vector146
vector146:
  pushl $0
80107e98:	6a 00                	push   $0x0
  pushl $146
80107e9a:	68 92 00 00 00       	push   $0x92
  jmp alltraps
80107e9f:	e9 7f f4 ff ff       	jmp    80107323 <alltraps>

80107ea4 <vector147>:
.globl vector147
vector147:
  pushl $0
80107ea4:	6a 00                	push   $0x0
  pushl $147
80107ea6:	68 93 00 00 00       	push   $0x93
  jmp alltraps
80107eab:	e9 73 f4 ff ff       	jmp    80107323 <alltraps>

80107eb0 <vector148>:
.globl vector148
vector148:
  pushl $0
80107eb0:	6a 00                	push   $0x0
  pushl $148
80107eb2:	68 94 00 00 00       	push   $0x94
  jmp alltraps
80107eb7:	e9 67 f4 ff ff       	jmp    80107323 <alltraps>

80107ebc <vector149>:
.globl vector149
vector149:
  pushl $0
80107ebc:	6a 00                	push   $0x0
  pushl $149
80107ebe:	68 95 00 00 00       	push   $0x95
  jmp alltraps
80107ec3:	e9 5b f4 ff ff       	jmp    80107323 <alltraps>

80107ec8 <vector150>:
.globl vector150
vector150:
  pushl $0
80107ec8:	6a 00                	push   $0x0
  pushl $150
80107eca:	68 96 00 00 00       	push   $0x96
  jmp alltraps
80107ecf:	e9 4f f4 ff ff       	jmp    80107323 <alltraps>

80107ed4 <vector151>:
.globl vector151
vector151:
  pushl $0
80107ed4:	6a 00                	push   $0x0
  pushl $151
80107ed6:	68 97 00 00 00       	push   $0x97
  jmp alltraps
80107edb:	e9 43 f4 ff ff       	jmp    80107323 <alltraps>

80107ee0 <vector152>:
.globl vector152
vector152:
  pushl $0
80107ee0:	6a 00                	push   $0x0
  pushl $152
80107ee2:	68 98 00 00 00       	push   $0x98
  jmp alltraps
80107ee7:	e9 37 f4 ff ff       	jmp    80107323 <alltraps>

80107eec <vector153>:
.globl vector153
vector153:
  pushl $0
80107eec:	6a 00                	push   $0x0
  pushl $153
80107eee:	68 99 00 00 00       	push   $0x99
  jmp alltraps
80107ef3:	e9 2b f4 ff ff       	jmp    80107323 <alltraps>

80107ef8 <vector154>:
.globl vector154
vector154:
  pushl $0
80107ef8:	6a 00                	push   $0x0
  pushl $154
80107efa:	68 9a 00 00 00       	push   $0x9a
  jmp alltraps
80107eff:	e9 1f f4 ff ff       	jmp    80107323 <alltraps>

80107f04 <vector155>:
.globl vector155
vector155:
  pushl $0
80107f04:	6a 00                	push   $0x0
  pushl $155
80107f06:	68 9b 00 00 00       	push   $0x9b
  jmp alltraps
80107f0b:	e9 13 f4 ff ff       	jmp    80107323 <alltraps>

80107f10 <vector156>:
.globl vector156
vector156:
  pushl $0
80107f10:	6a 00                	push   $0x0
  pushl $156
80107f12:	68 9c 00 00 00       	push   $0x9c
  jmp alltraps
80107f17:	e9 07 f4 ff ff       	jmp    80107323 <alltraps>

80107f1c <vector157>:
.globl vector157
vector157:
  pushl $0
80107f1c:	6a 00                	push   $0x0
  pushl $157
80107f1e:	68 9d 00 00 00       	push   $0x9d
  jmp alltraps
80107f23:	e9 fb f3 ff ff       	jmp    80107323 <alltraps>

80107f28 <vector158>:
.globl vector158
vector158:
  pushl $0
80107f28:	6a 00                	push   $0x0
  pushl $158
80107f2a:	68 9e 00 00 00       	push   $0x9e
  jmp alltraps
80107f2f:	e9 ef f3 ff ff       	jmp    80107323 <alltraps>

80107f34 <vector159>:
.globl vector159
vector159:
  pushl $0
80107f34:	6a 00                	push   $0x0
  pushl $159
80107f36:	68 9f 00 00 00       	push   $0x9f
  jmp alltraps
80107f3b:	e9 e3 f3 ff ff       	jmp    80107323 <alltraps>

80107f40 <vector160>:
.globl vector160
vector160:
  pushl $0
80107f40:	6a 00                	push   $0x0
  pushl $160
80107f42:	68 a0 00 00 00       	push   $0xa0
  jmp alltraps
80107f47:	e9 d7 f3 ff ff       	jmp    80107323 <alltraps>

80107f4c <vector161>:
.globl vector161
vector161:
  pushl $0
80107f4c:	6a 00                	push   $0x0
  pushl $161
80107f4e:	68 a1 00 00 00       	push   $0xa1
  jmp alltraps
80107f53:	e9 cb f3 ff ff       	jmp    80107323 <alltraps>

80107f58 <vector162>:
.globl vector162
vector162:
  pushl $0
80107f58:	6a 00                	push   $0x0
  pushl $162
80107f5a:	68 a2 00 00 00       	push   $0xa2
  jmp alltraps
80107f5f:	e9 bf f3 ff ff       	jmp    80107323 <alltraps>

80107f64 <vector163>:
.globl vector163
vector163:
  pushl $0
80107f64:	6a 00                	push   $0x0
  pushl $163
80107f66:	68 a3 00 00 00       	push   $0xa3
  jmp alltraps
80107f6b:	e9 b3 f3 ff ff       	jmp    80107323 <alltraps>

80107f70 <vector164>:
.globl vector164
vector164:
  pushl $0
80107f70:	6a 00                	push   $0x0
  pushl $164
80107f72:	68 a4 00 00 00       	push   $0xa4
  jmp alltraps
80107f77:	e9 a7 f3 ff ff       	jmp    80107323 <alltraps>

80107f7c <vector165>:
.globl vector165
vector165:
  pushl $0
80107f7c:	6a 00                	push   $0x0
  pushl $165
80107f7e:	68 a5 00 00 00       	push   $0xa5
  jmp alltraps
80107f83:	e9 9b f3 ff ff       	jmp    80107323 <alltraps>

80107f88 <vector166>:
.globl vector166
vector166:
  pushl $0
80107f88:	6a 00                	push   $0x0
  pushl $166
80107f8a:	68 a6 00 00 00       	push   $0xa6
  jmp alltraps
80107f8f:	e9 8f f3 ff ff       	jmp    80107323 <alltraps>

80107f94 <vector167>:
.globl vector167
vector167:
  pushl $0
80107f94:	6a 00                	push   $0x0
  pushl $167
80107f96:	68 a7 00 00 00       	push   $0xa7
  jmp alltraps
80107f9b:	e9 83 f3 ff ff       	jmp    80107323 <alltraps>

80107fa0 <vector168>:
.globl vector168
vector168:
  pushl $0
80107fa0:	6a 00                	push   $0x0
  pushl $168
80107fa2:	68 a8 00 00 00       	push   $0xa8
  jmp alltraps
80107fa7:	e9 77 f3 ff ff       	jmp    80107323 <alltraps>

80107fac <vector169>:
.globl vector169
vector169:
  pushl $0
80107fac:	6a 00                	push   $0x0
  pushl $169
80107fae:	68 a9 00 00 00       	push   $0xa9
  jmp alltraps
80107fb3:	e9 6b f3 ff ff       	jmp    80107323 <alltraps>

80107fb8 <vector170>:
.globl vector170
vector170:
  pushl $0
80107fb8:	6a 00                	push   $0x0
  pushl $170
80107fba:	68 aa 00 00 00       	push   $0xaa
  jmp alltraps
80107fbf:	e9 5f f3 ff ff       	jmp    80107323 <alltraps>

80107fc4 <vector171>:
.globl vector171
vector171:
  pushl $0
80107fc4:	6a 00                	push   $0x0
  pushl $171
80107fc6:	68 ab 00 00 00       	push   $0xab
  jmp alltraps
80107fcb:	e9 53 f3 ff ff       	jmp    80107323 <alltraps>

80107fd0 <vector172>:
.globl vector172
vector172:
  pushl $0
80107fd0:	6a 00                	push   $0x0
  pushl $172
80107fd2:	68 ac 00 00 00       	push   $0xac
  jmp alltraps
80107fd7:	e9 47 f3 ff ff       	jmp    80107323 <alltraps>

80107fdc <vector173>:
.globl vector173
vector173:
  pushl $0
80107fdc:	6a 00                	push   $0x0
  pushl $173
80107fde:	68 ad 00 00 00       	push   $0xad
  jmp alltraps
80107fe3:	e9 3b f3 ff ff       	jmp    80107323 <alltraps>

80107fe8 <vector174>:
.globl vector174
vector174:
  pushl $0
80107fe8:	6a 00                	push   $0x0
  pushl $174
80107fea:	68 ae 00 00 00       	push   $0xae
  jmp alltraps
80107fef:	e9 2f f3 ff ff       	jmp    80107323 <alltraps>

80107ff4 <vector175>:
.globl vector175
vector175:
  pushl $0
80107ff4:	6a 00                	push   $0x0
  pushl $175
80107ff6:	68 af 00 00 00       	push   $0xaf
  jmp alltraps
80107ffb:	e9 23 f3 ff ff       	jmp    80107323 <alltraps>

80108000 <vector176>:
.globl vector176
vector176:
  pushl $0
80108000:	6a 00                	push   $0x0
  pushl $176
80108002:	68 b0 00 00 00       	push   $0xb0
  jmp alltraps
80108007:	e9 17 f3 ff ff       	jmp    80107323 <alltraps>

8010800c <vector177>:
.globl vector177
vector177:
  pushl $0
8010800c:	6a 00                	push   $0x0
  pushl $177
8010800e:	68 b1 00 00 00       	push   $0xb1
  jmp alltraps
80108013:	e9 0b f3 ff ff       	jmp    80107323 <alltraps>

80108018 <vector178>:
.globl vector178
vector178:
  pushl $0
80108018:	6a 00                	push   $0x0
  pushl $178
8010801a:	68 b2 00 00 00       	push   $0xb2
  jmp alltraps
8010801f:	e9 ff f2 ff ff       	jmp    80107323 <alltraps>

80108024 <vector179>:
.globl vector179
vector179:
  pushl $0
80108024:	6a 00                	push   $0x0
  pushl $179
80108026:	68 b3 00 00 00       	push   $0xb3
  jmp alltraps
8010802b:	e9 f3 f2 ff ff       	jmp    80107323 <alltraps>

80108030 <vector180>:
.globl vector180
vector180:
  pushl $0
80108030:	6a 00                	push   $0x0
  pushl $180
80108032:	68 b4 00 00 00       	push   $0xb4
  jmp alltraps
80108037:	e9 e7 f2 ff ff       	jmp    80107323 <alltraps>

8010803c <vector181>:
.globl vector181
vector181:
  pushl $0
8010803c:	6a 00                	push   $0x0
  pushl $181
8010803e:	68 b5 00 00 00       	push   $0xb5
  jmp alltraps
80108043:	e9 db f2 ff ff       	jmp    80107323 <alltraps>

80108048 <vector182>:
.globl vector182
vector182:
  pushl $0
80108048:	6a 00                	push   $0x0
  pushl $182
8010804a:	68 b6 00 00 00       	push   $0xb6
  jmp alltraps
8010804f:	e9 cf f2 ff ff       	jmp    80107323 <alltraps>

80108054 <vector183>:
.globl vector183
vector183:
  pushl $0
80108054:	6a 00                	push   $0x0
  pushl $183
80108056:	68 b7 00 00 00       	push   $0xb7
  jmp alltraps
8010805b:	e9 c3 f2 ff ff       	jmp    80107323 <alltraps>

80108060 <vector184>:
.globl vector184
vector184:
  pushl $0
80108060:	6a 00                	push   $0x0
  pushl $184
80108062:	68 b8 00 00 00       	push   $0xb8
  jmp alltraps
80108067:	e9 b7 f2 ff ff       	jmp    80107323 <alltraps>

8010806c <vector185>:
.globl vector185
vector185:
  pushl $0
8010806c:	6a 00                	push   $0x0
  pushl $185
8010806e:	68 b9 00 00 00       	push   $0xb9
  jmp alltraps
80108073:	e9 ab f2 ff ff       	jmp    80107323 <alltraps>

80108078 <vector186>:
.globl vector186
vector186:
  pushl $0
80108078:	6a 00                	push   $0x0
  pushl $186
8010807a:	68 ba 00 00 00       	push   $0xba
  jmp alltraps
8010807f:	e9 9f f2 ff ff       	jmp    80107323 <alltraps>

80108084 <vector187>:
.globl vector187
vector187:
  pushl $0
80108084:	6a 00                	push   $0x0
  pushl $187
80108086:	68 bb 00 00 00       	push   $0xbb
  jmp alltraps
8010808b:	e9 93 f2 ff ff       	jmp    80107323 <alltraps>

80108090 <vector188>:
.globl vector188
vector188:
  pushl $0
80108090:	6a 00                	push   $0x0
  pushl $188
80108092:	68 bc 00 00 00       	push   $0xbc
  jmp alltraps
80108097:	e9 87 f2 ff ff       	jmp    80107323 <alltraps>

8010809c <vector189>:
.globl vector189
vector189:
  pushl $0
8010809c:	6a 00                	push   $0x0
  pushl $189
8010809e:	68 bd 00 00 00       	push   $0xbd
  jmp alltraps
801080a3:	e9 7b f2 ff ff       	jmp    80107323 <alltraps>

801080a8 <vector190>:
.globl vector190
vector190:
  pushl $0
801080a8:	6a 00                	push   $0x0
  pushl $190
801080aa:	68 be 00 00 00       	push   $0xbe
  jmp alltraps
801080af:	e9 6f f2 ff ff       	jmp    80107323 <alltraps>

801080b4 <vector191>:
.globl vector191
vector191:
  pushl $0
801080b4:	6a 00                	push   $0x0
  pushl $191
801080b6:	68 bf 00 00 00       	push   $0xbf
  jmp alltraps
801080bb:	e9 63 f2 ff ff       	jmp    80107323 <alltraps>

801080c0 <vector192>:
.globl vector192
vector192:
  pushl $0
801080c0:	6a 00                	push   $0x0
  pushl $192
801080c2:	68 c0 00 00 00       	push   $0xc0
  jmp alltraps
801080c7:	e9 57 f2 ff ff       	jmp    80107323 <alltraps>

801080cc <vector193>:
.globl vector193
vector193:
  pushl $0
801080cc:	6a 00                	push   $0x0
  pushl $193
801080ce:	68 c1 00 00 00       	push   $0xc1
  jmp alltraps
801080d3:	e9 4b f2 ff ff       	jmp    80107323 <alltraps>

801080d8 <vector194>:
.globl vector194
vector194:
  pushl $0
801080d8:	6a 00                	push   $0x0
  pushl $194
801080da:	68 c2 00 00 00       	push   $0xc2
  jmp alltraps
801080df:	e9 3f f2 ff ff       	jmp    80107323 <alltraps>

801080e4 <vector195>:
.globl vector195
vector195:
  pushl $0
801080e4:	6a 00                	push   $0x0
  pushl $195
801080e6:	68 c3 00 00 00       	push   $0xc3
  jmp alltraps
801080eb:	e9 33 f2 ff ff       	jmp    80107323 <alltraps>

801080f0 <vector196>:
.globl vector196
vector196:
  pushl $0
801080f0:	6a 00                	push   $0x0
  pushl $196
801080f2:	68 c4 00 00 00       	push   $0xc4
  jmp alltraps
801080f7:	e9 27 f2 ff ff       	jmp    80107323 <alltraps>

801080fc <vector197>:
.globl vector197
vector197:
  pushl $0
801080fc:	6a 00                	push   $0x0
  pushl $197
801080fe:	68 c5 00 00 00       	push   $0xc5
  jmp alltraps
80108103:	e9 1b f2 ff ff       	jmp    80107323 <alltraps>

80108108 <vector198>:
.globl vector198
vector198:
  pushl $0
80108108:	6a 00                	push   $0x0
  pushl $198
8010810a:	68 c6 00 00 00       	push   $0xc6
  jmp alltraps
8010810f:	e9 0f f2 ff ff       	jmp    80107323 <alltraps>

80108114 <vector199>:
.globl vector199
vector199:
  pushl $0
80108114:	6a 00                	push   $0x0
  pushl $199
80108116:	68 c7 00 00 00       	push   $0xc7
  jmp alltraps
8010811b:	e9 03 f2 ff ff       	jmp    80107323 <alltraps>

80108120 <vector200>:
.globl vector200
vector200:
  pushl $0
80108120:	6a 00                	push   $0x0
  pushl $200
80108122:	68 c8 00 00 00       	push   $0xc8
  jmp alltraps
80108127:	e9 f7 f1 ff ff       	jmp    80107323 <alltraps>

8010812c <vector201>:
.globl vector201
vector201:
  pushl $0
8010812c:	6a 00                	push   $0x0
  pushl $201
8010812e:	68 c9 00 00 00       	push   $0xc9
  jmp alltraps
80108133:	e9 eb f1 ff ff       	jmp    80107323 <alltraps>

80108138 <vector202>:
.globl vector202
vector202:
  pushl $0
80108138:	6a 00                	push   $0x0
  pushl $202
8010813a:	68 ca 00 00 00       	push   $0xca
  jmp alltraps
8010813f:	e9 df f1 ff ff       	jmp    80107323 <alltraps>

80108144 <vector203>:
.globl vector203
vector203:
  pushl $0
80108144:	6a 00                	push   $0x0
  pushl $203
80108146:	68 cb 00 00 00       	push   $0xcb
  jmp alltraps
8010814b:	e9 d3 f1 ff ff       	jmp    80107323 <alltraps>

80108150 <vector204>:
.globl vector204
vector204:
  pushl $0
80108150:	6a 00                	push   $0x0
  pushl $204
80108152:	68 cc 00 00 00       	push   $0xcc
  jmp alltraps
80108157:	e9 c7 f1 ff ff       	jmp    80107323 <alltraps>

8010815c <vector205>:
.globl vector205
vector205:
  pushl $0
8010815c:	6a 00                	push   $0x0
  pushl $205
8010815e:	68 cd 00 00 00       	push   $0xcd
  jmp alltraps
80108163:	e9 bb f1 ff ff       	jmp    80107323 <alltraps>

80108168 <vector206>:
.globl vector206
vector206:
  pushl $0
80108168:	6a 00                	push   $0x0
  pushl $206
8010816a:	68 ce 00 00 00       	push   $0xce
  jmp alltraps
8010816f:	e9 af f1 ff ff       	jmp    80107323 <alltraps>

80108174 <vector207>:
.globl vector207
vector207:
  pushl $0
80108174:	6a 00                	push   $0x0
  pushl $207
80108176:	68 cf 00 00 00       	push   $0xcf
  jmp alltraps
8010817b:	e9 a3 f1 ff ff       	jmp    80107323 <alltraps>

80108180 <vector208>:
.globl vector208
vector208:
  pushl $0
80108180:	6a 00                	push   $0x0
  pushl $208
80108182:	68 d0 00 00 00       	push   $0xd0
  jmp alltraps
80108187:	e9 97 f1 ff ff       	jmp    80107323 <alltraps>

8010818c <vector209>:
.globl vector209
vector209:
  pushl $0
8010818c:	6a 00                	push   $0x0
  pushl $209
8010818e:	68 d1 00 00 00       	push   $0xd1
  jmp alltraps
80108193:	e9 8b f1 ff ff       	jmp    80107323 <alltraps>

80108198 <vector210>:
.globl vector210
vector210:
  pushl $0
80108198:	6a 00                	push   $0x0
  pushl $210
8010819a:	68 d2 00 00 00       	push   $0xd2
  jmp alltraps
8010819f:	e9 7f f1 ff ff       	jmp    80107323 <alltraps>

801081a4 <vector211>:
.globl vector211
vector211:
  pushl $0
801081a4:	6a 00                	push   $0x0
  pushl $211
801081a6:	68 d3 00 00 00       	push   $0xd3
  jmp alltraps
801081ab:	e9 73 f1 ff ff       	jmp    80107323 <alltraps>

801081b0 <vector212>:
.globl vector212
vector212:
  pushl $0
801081b0:	6a 00                	push   $0x0
  pushl $212
801081b2:	68 d4 00 00 00       	push   $0xd4
  jmp alltraps
801081b7:	e9 67 f1 ff ff       	jmp    80107323 <alltraps>

801081bc <vector213>:
.globl vector213
vector213:
  pushl $0
801081bc:	6a 00                	push   $0x0
  pushl $213
801081be:	68 d5 00 00 00       	push   $0xd5
  jmp alltraps
801081c3:	e9 5b f1 ff ff       	jmp    80107323 <alltraps>

801081c8 <vector214>:
.globl vector214
vector214:
  pushl $0
801081c8:	6a 00                	push   $0x0
  pushl $214
801081ca:	68 d6 00 00 00       	push   $0xd6
  jmp alltraps
801081cf:	e9 4f f1 ff ff       	jmp    80107323 <alltraps>

801081d4 <vector215>:
.globl vector215
vector215:
  pushl $0
801081d4:	6a 00                	push   $0x0
  pushl $215
801081d6:	68 d7 00 00 00       	push   $0xd7
  jmp alltraps
801081db:	e9 43 f1 ff ff       	jmp    80107323 <alltraps>

801081e0 <vector216>:
.globl vector216
vector216:
  pushl $0
801081e0:	6a 00                	push   $0x0
  pushl $216
801081e2:	68 d8 00 00 00       	push   $0xd8
  jmp alltraps
801081e7:	e9 37 f1 ff ff       	jmp    80107323 <alltraps>

801081ec <vector217>:
.globl vector217
vector217:
  pushl $0
801081ec:	6a 00                	push   $0x0
  pushl $217
801081ee:	68 d9 00 00 00       	push   $0xd9
  jmp alltraps
801081f3:	e9 2b f1 ff ff       	jmp    80107323 <alltraps>

801081f8 <vector218>:
.globl vector218
vector218:
  pushl $0
801081f8:	6a 00                	push   $0x0
  pushl $218
801081fa:	68 da 00 00 00       	push   $0xda
  jmp alltraps
801081ff:	e9 1f f1 ff ff       	jmp    80107323 <alltraps>

80108204 <vector219>:
.globl vector219
vector219:
  pushl $0
80108204:	6a 00                	push   $0x0
  pushl $219
80108206:	68 db 00 00 00       	push   $0xdb
  jmp alltraps
8010820b:	e9 13 f1 ff ff       	jmp    80107323 <alltraps>

80108210 <vector220>:
.globl vector220
vector220:
  pushl $0
80108210:	6a 00                	push   $0x0
  pushl $220
80108212:	68 dc 00 00 00       	push   $0xdc
  jmp alltraps
80108217:	e9 07 f1 ff ff       	jmp    80107323 <alltraps>

8010821c <vector221>:
.globl vector221
vector221:
  pushl $0
8010821c:	6a 00                	push   $0x0
  pushl $221
8010821e:	68 dd 00 00 00       	push   $0xdd
  jmp alltraps
80108223:	e9 fb f0 ff ff       	jmp    80107323 <alltraps>

80108228 <vector222>:
.globl vector222
vector222:
  pushl $0
80108228:	6a 00                	push   $0x0
  pushl $222
8010822a:	68 de 00 00 00       	push   $0xde
  jmp alltraps
8010822f:	e9 ef f0 ff ff       	jmp    80107323 <alltraps>

80108234 <vector223>:
.globl vector223
vector223:
  pushl $0
80108234:	6a 00                	push   $0x0
  pushl $223
80108236:	68 df 00 00 00       	push   $0xdf
  jmp alltraps
8010823b:	e9 e3 f0 ff ff       	jmp    80107323 <alltraps>

80108240 <vector224>:
.globl vector224
vector224:
  pushl $0
80108240:	6a 00                	push   $0x0
  pushl $224
80108242:	68 e0 00 00 00       	push   $0xe0
  jmp alltraps
80108247:	e9 d7 f0 ff ff       	jmp    80107323 <alltraps>

8010824c <vector225>:
.globl vector225
vector225:
  pushl $0
8010824c:	6a 00                	push   $0x0
  pushl $225
8010824e:	68 e1 00 00 00       	push   $0xe1
  jmp alltraps
80108253:	e9 cb f0 ff ff       	jmp    80107323 <alltraps>

80108258 <vector226>:
.globl vector226
vector226:
  pushl $0
80108258:	6a 00                	push   $0x0
  pushl $226
8010825a:	68 e2 00 00 00       	push   $0xe2
  jmp alltraps
8010825f:	e9 bf f0 ff ff       	jmp    80107323 <alltraps>

80108264 <vector227>:
.globl vector227
vector227:
  pushl $0
80108264:	6a 00                	push   $0x0
  pushl $227
80108266:	68 e3 00 00 00       	push   $0xe3
  jmp alltraps
8010826b:	e9 b3 f0 ff ff       	jmp    80107323 <alltraps>

80108270 <vector228>:
.globl vector228
vector228:
  pushl $0
80108270:	6a 00                	push   $0x0
  pushl $228
80108272:	68 e4 00 00 00       	push   $0xe4
  jmp alltraps
80108277:	e9 a7 f0 ff ff       	jmp    80107323 <alltraps>

8010827c <vector229>:
.globl vector229
vector229:
  pushl $0
8010827c:	6a 00                	push   $0x0
  pushl $229
8010827e:	68 e5 00 00 00       	push   $0xe5
  jmp alltraps
80108283:	e9 9b f0 ff ff       	jmp    80107323 <alltraps>

80108288 <vector230>:
.globl vector230
vector230:
  pushl $0
80108288:	6a 00                	push   $0x0
  pushl $230
8010828a:	68 e6 00 00 00       	push   $0xe6
  jmp alltraps
8010828f:	e9 8f f0 ff ff       	jmp    80107323 <alltraps>

80108294 <vector231>:
.globl vector231
vector231:
  pushl $0
80108294:	6a 00                	push   $0x0
  pushl $231
80108296:	68 e7 00 00 00       	push   $0xe7
  jmp alltraps
8010829b:	e9 83 f0 ff ff       	jmp    80107323 <alltraps>

801082a0 <vector232>:
.globl vector232
vector232:
  pushl $0
801082a0:	6a 00                	push   $0x0
  pushl $232
801082a2:	68 e8 00 00 00       	push   $0xe8
  jmp alltraps
801082a7:	e9 77 f0 ff ff       	jmp    80107323 <alltraps>

801082ac <vector233>:
.globl vector233
vector233:
  pushl $0
801082ac:	6a 00                	push   $0x0
  pushl $233
801082ae:	68 e9 00 00 00       	push   $0xe9
  jmp alltraps
801082b3:	e9 6b f0 ff ff       	jmp    80107323 <alltraps>

801082b8 <vector234>:
.globl vector234
vector234:
  pushl $0
801082b8:	6a 00                	push   $0x0
  pushl $234
801082ba:	68 ea 00 00 00       	push   $0xea
  jmp alltraps
801082bf:	e9 5f f0 ff ff       	jmp    80107323 <alltraps>

801082c4 <vector235>:
.globl vector235
vector235:
  pushl $0
801082c4:	6a 00                	push   $0x0
  pushl $235
801082c6:	68 eb 00 00 00       	push   $0xeb
  jmp alltraps
801082cb:	e9 53 f0 ff ff       	jmp    80107323 <alltraps>

801082d0 <vector236>:
.globl vector236
vector236:
  pushl $0
801082d0:	6a 00                	push   $0x0
  pushl $236
801082d2:	68 ec 00 00 00       	push   $0xec
  jmp alltraps
801082d7:	e9 47 f0 ff ff       	jmp    80107323 <alltraps>

801082dc <vector237>:
.globl vector237
vector237:
  pushl $0
801082dc:	6a 00                	push   $0x0
  pushl $237
801082de:	68 ed 00 00 00       	push   $0xed
  jmp alltraps
801082e3:	e9 3b f0 ff ff       	jmp    80107323 <alltraps>

801082e8 <vector238>:
.globl vector238
vector238:
  pushl $0
801082e8:	6a 00                	push   $0x0
  pushl $238
801082ea:	68 ee 00 00 00       	push   $0xee
  jmp alltraps
801082ef:	e9 2f f0 ff ff       	jmp    80107323 <alltraps>

801082f4 <vector239>:
.globl vector239
vector239:
  pushl $0
801082f4:	6a 00                	push   $0x0
  pushl $239
801082f6:	68 ef 00 00 00       	push   $0xef
  jmp alltraps
801082fb:	e9 23 f0 ff ff       	jmp    80107323 <alltraps>

80108300 <vector240>:
.globl vector240
vector240:
  pushl $0
80108300:	6a 00                	push   $0x0
  pushl $240
80108302:	68 f0 00 00 00       	push   $0xf0
  jmp alltraps
80108307:	e9 17 f0 ff ff       	jmp    80107323 <alltraps>

8010830c <vector241>:
.globl vector241
vector241:
  pushl $0
8010830c:	6a 00                	push   $0x0
  pushl $241
8010830e:	68 f1 00 00 00       	push   $0xf1
  jmp alltraps
80108313:	e9 0b f0 ff ff       	jmp    80107323 <alltraps>

80108318 <vector242>:
.globl vector242
vector242:
  pushl $0
80108318:	6a 00                	push   $0x0
  pushl $242
8010831a:	68 f2 00 00 00       	push   $0xf2
  jmp alltraps
8010831f:	e9 ff ef ff ff       	jmp    80107323 <alltraps>

80108324 <vector243>:
.globl vector243
vector243:
  pushl $0
80108324:	6a 00                	push   $0x0
  pushl $243
80108326:	68 f3 00 00 00       	push   $0xf3
  jmp alltraps
8010832b:	e9 f3 ef ff ff       	jmp    80107323 <alltraps>

80108330 <vector244>:
.globl vector244
vector244:
  pushl $0
80108330:	6a 00                	push   $0x0
  pushl $244
80108332:	68 f4 00 00 00       	push   $0xf4
  jmp alltraps
80108337:	e9 e7 ef ff ff       	jmp    80107323 <alltraps>

8010833c <vector245>:
.globl vector245
vector245:
  pushl $0
8010833c:	6a 00                	push   $0x0
  pushl $245
8010833e:	68 f5 00 00 00       	push   $0xf5
  jmp alltraps
80108343:	e9 db ef ff ff       	jmp    80107323 <alltraps>

80108348 <vector246>:
.globl vector246
vector246:
  pushl $0
80108348:	6a 00                	push   $0x0
  pushl $246
8010834a:	68 f6 00 00 00       	push   $0xf6
  jmp alltraps
8010834f:	e9 cf ef ff ff       	jmp    80107323 <alltraps>

80108354 <vector247>:
.globl vector247
vector247:
  pushl $0
80108354:	6a 00                	push   $0x0
  pushl $247
80108356:	68 f7 00 00 00       	push   $0xf7
  jmp alltraps
8010835b:	e9 c3 ef ff ff       	jmp    80107323 <alltraps>

80108360 <vector248>:
.globl vector248
vector248:
  pushl $0
80108360:	6a 00                	push   $0x0
  pushl $248
80108362:	68 f8 00 00 00       	push   $0xf8
  jmp alltraps
80108367:	e9 b7 ef ff ff       	jmp    80107323 <alltraps>

8010836c <vector249>:
.globl vector249
vector249:
  pushl $0
8010836c:	6a 00                	push   $0x0
  pushl $249
8010836e:	68 f9 00 00 00       	push   $0xf9
  jmp alltraps
80108373:	e9 ab ef ff ff       	jmp    80107323 <alltraps>

80108378 <vector250>:
.globl vector250
vector250:
  pushl $0
80108378:	6a 00                	push   $0x0
  pushl $250
8010837a:	68 fa 00 00 00       	push   $0xfa
  jmp alltraps
8010837f:	e9 9f ef ff ff       	jmp    80107323 <alltraps>

80108384 <vector251>:
.globl vector251
vector251:
  pushl $0
80108384:	6a 00                	push   $0x0
  pushl $251
80108386:	68 fb 00 00 00       	push   $0xfb
  jmp alltraps
8010838b:	e9 93 ef ff ff       	jmp    80107323 <alltraps>

80108390 <vector252>:
.globl vector252
vector252:
  pushl $0
80108390:	6a 00                	push   $0x0
  pushl $252
80108392:	68 fc 00 00 00       	push   $0xfc
  jmp alltraps
80108397:	e9 87 ef ff ff       	jmp    80107323 <alltraps>

8010839c <vector253>:
.globl vector253
vector253:
  pushl $0
8010839c:	6a 00                	push   $0x0
  pushl $253
8010839e:	68 fd 00 00 00       	push   $0xfd
  jmp alltraps
801083a3:	e9 7b ef ff ff       	jmp    80107323 <alltraps>

801083a8 <vector254>:
.globl vector254
vector254:
  pushl $0
801083a8:	6a 00                	push   $0x0
  pushl $254
801083aa:	68 fe 00 00 00       	push   $0xfe
  jmp alltraps
801083af:	e9 6f ef ff ff       	jmp    80107323 <alltraps>

801083b4 <vector255>:
.globl vector255
vector255:
  pushl $0
801083b4:	6a 00                	push   $0x0
  pushl $255
801083b6:	68 ff 00 00 00       	push   $0xff
  jmp alltraps
801083bb:	e9 63 ef ff ff       	jmp    80107323 <alltraps>

801083c0 <lgdt>:

struct segdesc;

static inline void
lgdt(struct segdesc *p, int size)
{
801083c0:	55                   	push   %ebp
801083c1:	89 e5                	mov    %esp,%ebp
801083c3:	83 ec 10             	sub    $0x10,%esp
  volatile ushort pd[3];

  pd[0] = size-1;
801083c6:	8b 45 0c             	mov    0xc(%ebp),%eax
801083c9:	83 e8 01             	sub    $0x1,%eax
801083cc:	66 89 45 fa          	mov    %ax,-0x6(%ebp)
  pd[1] = (uint)p;
801083d0:	8b 45 08             	mov    0x8(%ebp),%eax
801083d3:	66 89 45 fc          	mov    %ax,-0x4(%ebp)
  pd[2] = (uint)p >> 16;
801083d7:	8b 45 08             	mov    0x8(%ebp),%eax
801083da:	c1 e8 10             	shr    $0x10,%eax
801083dd:	66 89 45 fe          	mov    %ax,-0x2(%ebp)

  asm volatile("lgdt (%0)" : : "r" (pd));
801083e1:	8d 45 fa             	lea    -0x6(%ebp),%eax
801083e4:	0f 01 10             	lgdtl  (%eax)
}
801083e7:	90                   	nop
801083e8:	c9                   	leave  
801083e9:	c3                   	ret    

801083ea <ltr>:
  asm volatile("lidt (%0)" : : "r" (pd));
}

static inline void
ltr(ushort sel)
{
801083ea:	55                   	push   %ebp
801083eb:	89 e5                	mov    %esp,%ebp
801083ed:	83 ec 04             	sub    $0x4,%esp
801083f0:	8b 45 08             	mov    0x8(%ebp),%eax
801083f3:	66 89 45 fc          	mov    %ax,-0x4(%ebp)
  asm volatile("ltr %0" : : "r" (sel));
801083f7:	0f b7 45 fc          	movzwl -0x4(%ebp),%eax
801083fb:	0f 00 d8             	ltr    %ax
}
801083fe:	90                   	nop
801083ff:	c9                   	leave  
80108400:	c3                   	ret    

80108401 <loadgs>:
  return eflags;
}

static inline void
loadgs(ushort v)
{
80108401:	55                   	push   %ebp
80108402:	89 e5                	mov    %esp,%ebp
80108404:	83 ec 04             	sub    $0x4,%esp
80108407:	8b 45 08             	mov    0x8(%ebp),%eax
8010840a:	66 89 45 fc          	mov    %ax,-0x4(%ebp)
  asm volatile("movw %0, %%gs" : : "r" (v));
8010840e:	0f b7 45 fc          	movzwl -0x4(%ebp),%eax
80108412:	8e e8                	mov    %eax,%gs
}
80108414:	90                   	nop
80108415:	c9                   	leave  
80108416:	c3                   	ret    

80108417 <lcr3>:
  return val;
}

static inline void
lcr3(uint val) 
{
80108417:	55                   	push   %ebp
80108418:	89 e5                	mov    %esp,%ebp
  asm volatile("movl %0,%%cr3" : : "r" (val));
8010841a:	8b 45 08             	mov    0x8(%ebp),%eax
8010841d:	0f 22 d8             	mov    %eax,%cr3
}
80108420:	90                   	nop
80108421:	5d                   	pop    %ebp
80108422:	c3                   	ret    

80108423 <v2p>:
#define KERNBASE 0x80000000         // First kernel virtual address
#define KERNLINK (KERNBASE+EXTMEM)  // Address where kernel is linked

#ifndef __ASSEMBLER__

static inline uint v2p(void *a) { return ((uint) (a))  - KERNBASE; }
80108423:	55                   	push   %ebp
80108424:	89 e5                	mov    %esp,%ebp
80108426:	8b 45 08             	mov    0x8(%ebp),%eax
80108429:	05 00 00 00 80       	add    $0x80000000,%eax
8010842e:	5d                   	pop    %ebp
8010842f:	c3                   	ret    

80108430 <p2v>:
static inline void *p2v(uint a) { return (void *) ((a) + KERNBASE); }
80108430:	55                   	push   %ebp
80108431:	89 e5                	mov    %esp,%ebp
80108433:	8b 45 08             	mov    0x8(%ebp),%eax
80108436:	05 00 00 00 80       	add    $0x80000000,%eax
8010843b:	5d                   	pop    %ebp
8010843c:	c3                   	ret    

8010843d <seginit>:

// Set up CPU's kernel segment descriptors.
// Run once on entry on each CPU.
void
seginit(void)
{
8010843d:	55                   	push   %ebp
8010843e:	89 e5                	mov    %esp,%ebp
80108440:	53                   	push   %ebx
80108441:	83 ec 14             	sub    $0x14,%esp

  // Map "logical" addresses to virtual addresses using identity map.
  // Cannot share a CODE descriptor for both kernel and user
  // because it would have to have DPL_USR, but the CPU forbids
  // an interrupt from CPL=0 to DPL=3.
  c = &cpus[cpunum()];
80108444:	e8 21 ac ff ff       	call   8010306a <cpunum>
80108449:	69 c0 bc 00 00 00    	imul   $0xbc,%eax,%eax
8010844f:	05 80 33 11 80       	add    $0x80113380,%eax
80108454:	89 45 f4             	mov    %eax,-0xc(%ebp)
  c->gdt[SEG_KCODE] = SEG(STA_X|STA_R, 0, 0xffffffff, 0);
80108457:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010845a:	66 c7 40 78 ff ff    	movw   $0xffff,0x78(%eax)
80108460:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108463:	66 c7 40 7a 00 00    	movw   $0x0,0x7a(%eax)
80108469:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010846c:	c6 40 7c 00          	movb   $0x0,0x7c(%eax)
80108470:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108473:	0f b6 50 7d          	movzbl 0x7d(%eax),%edx
80108477:	83 e2 f0             	and    $0xfffffff0,%edx
8010847a:	83 ca 0a             	or     $0xa,%edx
8010847d:	88 50 7d             	mov    %dl,0x7d(%eax)
80108480:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108483:	0f b6 50 7d          	movzbl 0x7d(%eax),%edx
80108487:	83 ca 10             	or     $0x10,%edx
8010848a:	88 50 7d             	mov    %dl,0x7d(%eax)
8010848d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108490:	0f b6 50 7d          	movzbl 0x7d(%eax),%edx
80108494:	83 e2 9f             	and    $0xffffff9f,%edx
80108497:	88 50 7d             	mov    %dl,0x7d(%eax)
8010849a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010849d:	0f b6 50 7d          	movzbl 0x7d(%eax),%edx
801084a1:	83 ca 80             	or     $0xffffff80,%edx
801084a4:	88 50 7d             	mov    %dl,0x7d(%eax)
801084a7:	8b 45 f4             	mov    -0xc(%ebp),%eax
801084aa:	0f b6 50 7e          	movzbl 0x7e(%eax),%edx
801084ae:	83 ca 0f             	or     $0xf,%edx
801084b1:	88 50 7e             	mov    %dl,0x7e(%eax)
801084b4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801084b7:	0f b6 50 7e          	movzbl 0x7e(%eax),%edx
801084bb:	83 e2 ef             	and    $0xffffffef,%edx
801084be:	88 50 7e             	mov    %dl,0x7e(%eax)
801084c1:	8b 45 f4             	mov    -0xc(%ebp),%eax
801084c4:	0f b6 50 7e          	movzbl 0x7e(%eax),%edx
801084c8:	83 e2 df             	and    $0xffffffdf,%edx
801084cb:	88 50 7e             	mov    %dl,0x7e(%eax)
801084ce:	8b 45 f4             	mov    -0xc(%ebp),%eax
801084d1:	0f b6 50 7e          	movzbl 0x7e(%eax),%edx
801084d5:	83 ca 40             	or     $0x40,%edx
801084d8:	88 50 7e             	mov    %dl,0x7e(%eax)
801084db:	8b 45 f4             	mov    -0xc(%ebp),%eax
801084de:	0f b6 50 7e          	movzbl 0x7e(%eax),%edx
801084e2:	83 ca 80             	or     $0xffffff80,%edx
801084e5:	88 50 7e             	mov    %dl,0x7e(%eax)
801084e8:	8b 45 f4             	mov    -0xc(%ebp),%eax
801084eb:	c6 40 7f 00          	movb   $0x0,0x7f(%eax)
  c->gdt[SEG_KDATA] = SEG(STA_W, 0, 0xffffffff, 0);
801084ef:	8b 45 f4             	mov    -0xc(%ebp),%eax
801084f2:	66 c7 80 80 00 00 00 	movw   $0xffff,0x80(%eax)
801084f9:	ff ff 
801084fb:	8b 45 f4             	mov    -0xc(%ebp),%eax
801084fe:	66 c7 80 82 00 00 00 	movw   $0x0,0x82(%eax)
80108505:	00 00 
80108507:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010850a:	c6 80 84 00 00 00 00 	movb   $0x0,0x84(%eax)
80108511:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108514:	0f b6 90 85 00 00 00 	movzbl 0x85(%eax),%edx
8010851b:	83 e2 f0             	and    $0xfffffff0,%edx
8010851e:	83 ca 02             	or     $0x2,%edx
80108521:	88 90 85 00 00 00    	mov    %dl,0x85(%eax)
80108527:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010852a:	0f b6 90 85 00 00 00 	movzbl 0x85(%eax),%edx
80108531:	83 ca 10             	or     $0x10,%edx
80108534:	88 90 85 00 00 00    	mov    %dl,0x85(%eax)
8010853a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010853d:	0f b6 90 85 00 00 00 	movzbl 0x85(%eax),%edx
80108544:	83 e2 9f             	and    $0xffffff9f,%edx
80108547:	88 90 85 00 00 00    	mov    %dl,0x85(%eax)
8010854d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108550:	0f b6 90 85 00 00 00 	movzbl 0x85(%eax),%edx
80108557:	83 ca 80             	or     $0xffffff80,%edx
8010855a:	88 90 85 00 00 00    	mov    %dl,0x85(%eax)
80108560:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108563:	0f b6 90 86 00 00 00 	movzbl 0x86(%eax),%edx
8010856a:	83 ca 0f             	or     $0xf,%edx
8010856d:	88 90 86 00 00 00    	mov    %dl,0x86(%eax)
80108573:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108576:	0f b6 90 86 00 00 00 	movzbl 0x86(%eax),%edx
8010857d:	83 e2 ef             	and    $0xffffffef,%edx
80108580:	88 90 86 00 00 00    	mov    %dl,0x86(%eax)
80108586:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108589:	0f b6 90 86 00 00 00 	movzbl 0x86(%eax),%edx
80108590:	83 e2 df             	and    $0xffffffdf,%edx
80108593:	88 90 86 00 00 00    	mov    %dl,0x86(%eax)
80108599:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010859c:	0f b6 90 86 00 00 00 	movzbl 0x86(%eax),%edx
801085a3:	83 ca 40             	or     $0x40,%edx
801085a6:	88 90 86 00 00 00    	mov    %dl,0x86(%eax)
801085ac:	8b 45 f4             	mov    -0xc(%ebp),%eax
801085af:	0f b6 90 86 00 00 00 	movzbl 0x86(%eax),%edx
801085b6:	83 ca 80             	or     $0xffffff80,%edx
801085b9:	88 90 86 00 00 00    	mov    %dl,0x86(%eax)
801085bf:	8b 45 f4             	mov    -0xc(%ebp),%eax
801085c2:	c6 80 87 00 00 00 00 	movb   $0x0,0x87(%eax)
  c->gdt[SEG_UCODE] = SEG(STA_X|STA_R, 0, 0xffffffff, DPL_USER);
801085c9:	8b 45 f4             	mov    -0xc(%ebp),%eax
801085cc:	66 c7 80 90 00 00 00 	movw   $0xffff,0x90(%eax)
801085d3:	ff ff 
801085d5:	8b 45 f4             	mov    -0xc(%ebp),%eax
801085d8:	66 c7 80 92 00 00 00 	movw   $0x0,0x92(%eax)
801085df:	00 00 
801085e1:	8b 45 f4             	mov    -0xc(%ebp),%eax
801085e4:	c6 80 94 00 00 00 00 	movb   $0x0,0x94(%eax)
801085eb:	8b 45 f4             	mov    -0xc(%ebp),%eax
801085ee:	0f b6 90 95 00 00 00 	movzbl 0x95(%eax),%edx
801085f5:	83 e2 f0             	and    $0xfffffff0,%edx
801085f8:	83 ca 0a             	or     $0xa,%edx
801085fb:	88 90 95 00 00 00    	mov    %dl,0x95(%eax)
80108601:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108604:	0f b6 90 95 00 00 00 	movzbl 0x95(%eax),%edx
8010860b:	83 ca 10             	or     $0x10,%edx
8010860e:	88 90 95 00 00 00    	mov    %dl,0x95(%eax)
80108614:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108617:	0f b6 90 95 00 00 00 	movzbl 0x95(%eax),%edx
8010861e:	83 ca 60             	or     $0x60,%edx
80108621:	88 90 95 00 00 00    	mov    %dl,0x95(%eax)
80108627:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010862a:	0f b6 90 95 00 00 00 	movzbl 0x95(%eax),%edx
80108631:	83 ca 80             	or     $0xffffff80,%edx
80108634:	88 90 95 00 00 00    	mov    %dl,0x95(%eax)
8010863a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010863d:	0f b6 90 96 00 00 00 	movzbl 0x96(%eax),%edx
80108644:	83 ca 0f             	or     $0xf,%edx
80108647:	88 90 96 00 00 00    	mov    %dl,0x96(%eax)
8010864d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108650:	0f b6 90 96 00 00 00 	movzbl 0x96(%eax),%edx
80108657:	83 e2 ef             	and    $0xffffffef,%edx
8010865a:	88 90 96 00 00 00    	mov    %dl,0x96(%eax)
80108660:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108663:	0f b6 90 96 00 00 00 	movzbl 0x96(%eax),%edx
8010866a:	83 e2 df             	and    $0xffffffdf,%edx
8010866d:	88 90 96 00 00 00    	mov    %dl,0x96(%eax)
80108673:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108676:	0f b6 90 96 00 00 00 	movzbl 0x96(%eax),%edx
8010867d:	83 ca 40             	or     $0x40,%edx
80108680:	88 90 96 00 00 00    	mov    %dl,0x96(%eax)
80108686:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108689:	0f b6 90 96 00 00 00 	movzbl 0x96(%eax),%edx
80108690:	83 ca 80             	or     $0xffffff80,%edx
80108693:	88 90 96 00 00 00    	mov    %dl,0x96(%eax)
80108699:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010869c:	c6 80 97 00 00 00 00 	movb   $0x0,0x97(%eax)
  c->gdt[SEG_UDATA] = SEG(STA_W, 0, 0xffffffff, DPL_USER);
801086a3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801086a6:	66 c7 80 98 00 00 00 	movw   $0xffff,0x98(%eax)
801086ad:	ff ff 
801086af:	8b 45 f4             	mov    -0xc(%ebp),%eax
801086b2:	66 c7 80 9a 00 00 00 	movw   $0x0,0x9a(%eax)
801086b9:	00 00 
801086bb:	8b 45 f4             	mov    -0xc(%ebp),%eax
801086be:	c6 80 9c 00 00 00 00 	movb   $0x0,0x9c(%eax)
801086c5:	8b 45 f4             	mov    -0xc(%ebp),%eax
801086c8:	0f b6 90 9d 00 00 00 	movzbl 0x9d(%eax),%edx
801086cf:	83 e2 f0             	and    $0xfffffff0,%edx
801086d2:	83 ca 02             	or     $0x2,%edx
801086d5:	88 90 9d 00 00 00    	mov    %dl,0x9d(%eax)
801086db:	8b 45 f4             	mov    -0xc(%ebp),%eax
801086de:	0f b6 90 9d 00 00 00 	movzbl 0x9d(%eax),%edx
801086e5:	83 ca 10             	or     $0x10,%edx
801086e8:	88 90 9d 00 00 00    	mov    %dl,0x9d(%eax)
801086ee:	8b 45 f4             	mov    -0xc(%ebp),%eax
801086f1:	0f b6 90 9d 00 00 00 	movzbl 0x9d(%eax),%edx
801086f8:	83 ca 60             	or     $0x60,%edx
801086fb:	88 90 9d 00 00 00    	mov    %dl,0x9d(%eax)
80108701:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108704:	0f b6 90 9d 00 00 00 	movzbl 0x9d(%eax),%edx
8010870b:	83 ca 80             	or     $0xffffff80,%edx
8010870e:	88 90 9d 00 00 00    	mov    %dl,0x9d(%eax)
80108714:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108717:	0f b6 90 9e 00 00 00 	movzbl 0x9e(%eax),%edx
8010871e:	83 ca 0f             	or     $0xf,%edx
80108721:	88 90 9e 00 00 00    	mov    %dl,0x9e(%eax)
80108727:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010872a:	0f b6 90 9e 00 00 00 	movzbl 0x9e(%eax),%edx
80108731:	83 e2 ef             	and    $0xffffffef,%edx
80108734:	88 90 9e 00 00 00    	mov    %dl,0x9e(%eax)
8010873a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010873d:	0f b6 90 9e 00 00 00 	movzbl 0x9e(%eax),%edx
80108744:	83 e2 df             	and    $0xffffffdf,%edx
80108747:	88 90 9e 00 00 00    	mov    %dl,0x9e(%eax)
8010874d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108750:	0f b6 90 9e 00 00 00 	movzbl 0x9e(%eax),%edx
80108757:	83 ca 40             	or     $0x40,%edx
8010875a:	88 90 9e 00 00 00    	mov    %dl,0x9e(%eax)
80108760:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108763:	0f b6 90 9e 00 00 00 	movzbl 0x9e(%eax),%edx
8010876a:	83 ca 80             	or     $0xffffff80,%edx
8010876d:	88 90 9e 00 00 00    	mov    %dl,0x9e(%eax)
80108773:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108776:	c6 80 9f 00 00 00 00 	movb   $0x0,0x9f(%eax)

  // Map cpu, and curproc
  c->gdt[SEG_KCPU] = SEG(STA_W, &c->cpu, 8, 0);
8010877d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108780:	05 b4 00 00 00       	add    $0xb4,%eax
80108785:	89 c3                	mov    %eax,%ebx
80108787:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010878a:	05 b4 00 00 00       	add    $0xb4,%eax
8010878f:	c1 e8 10             	shr    $0x10,%eax
80108792:	89 c2                	mov    %eax,%edx
80108794:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108797:	05 b4 00 00 00       	add    $0xb4,%eax
8010879c:	c1 e8 18             	shr    $0x18,%eax
8010879f:	89 c1                	mov    %eax,%ecx
801087a1:	8b 45 f4             	mov    -0xc(%ebp),%eax
801087a4:	66 c7 80 88 00 00 00 	movw   $0x0,0x88(%eax)
801087ab:	00 00 
801087ad:	8b 45 f4             	mov    -0xc(%ebp),%eax
801087b0:	66 89 98 8a 00 00 00 	mov    %bx,0x8a(%eax)
801087b7:	8b 45 f4             	mov    -0xc(%ebp),%eax
801087ba:	88 90 8c 00 00 00    	mov    %dl,0x8c(%eax)
801087c0:	8b 45 f4             	mov    -0xc(%ebp),%eax
801087c3:	0f b6 90 8d 00 00 00 	movzbl 0x8d(%eax),%edx
801087ca:	83 e2 f0             	and    $0xfffffff0,%edx
801087cd:	83 ca 02             	or     $0x2,%edx
801087d0:	88 90 8d 00 00 00    	mov    %dl,0x8d(%eax)
801087d6:	8b 45 f4             	mov    -0xc(%ebp),%eax
801087d9:	0f b6 90 8d 00 00 00 	movzbl 0x8d(%eax),%edx
801087e0:	83 ca 10             	or     $0x10,%edx
801087e3:	88 90 8d 00 00 00    	mov    %dl,0x8d(%eax)
801087e9:	8b 45 f4             	mov    -0xc(%ebp),%eax
801087ec:	0f b6 90 8d 00 00 00 	movzbl 0x8d(%eax),%edx
801087f3:	83 e2 9f             	and    $0xffffff9f,%edx
801087f6:	88 90 8d 00 00 00    	mov    %dl,0x8d(%eax)
801087fc:	8b 45 f4             	mov    -0xc(%ebp),%eax
801087ff:	0f b6 90 8d 00 00 00 	movzbl 0x8d(%eax),%edx
80108806:	83 ca 80             	or     $0xffffff80,%edx
80108809:	88 90 8d 00 00 00    	mov    %dl,0x8d(%eax)
8010880f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108812:	0f b6 90 8e 00 00 00 	movzbl 0x8e(%eax),%edx
80108819:	83 e2 f0             	and    $0xfffffff0,%edx
8010881c:	88 90 8e 00 00 00    	mov    %dl,0x8e(%eax)
80108822:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108825:	0f b6 90 8e 00 00 00 	movzbl 0x8e(%eax),%edx
8010882c:	83 e2 ef             	and    $0xffffffef,%edx
8010882f:	88 90 8e 00 00 00    	mov    %dl,0x8e(%eax)
80108835:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108838:	0f b6 90 8e 00 00 00 	movzbl 0x8e(%eax),%edx
8010883f:	83 e2 df             	and    $0xffffffdf,%edx
80108842:	88 90 8e 00 00 00    	mov    %dl,0x8e(%eax)
80108848:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010884b:	0f b6 90 8e 00 00 00 	movzbl 0x8e(%eax),%edx
80108852:	83 ca 40             	or     $0x40,%edx
80108855:	88 90 8e 00 00 00    	mov    %dl,0x8e(%eax)
8010885b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010885e:	0f b6 90 8e 00 00 00 	movzbl 0x8e(%eax),%edx
80108865:	83 ca 80             	or     $0xffffff80,%edx
80108868:	88 90 8e 00 00 00    	mov    %dl,0x8e(%eax)
8010886e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108871:	88 88 8f 00 00 00    	mov    %cl,0x8f(%eax)

  lgdt(c->gdt, sizeof(c->gdt));
80108877:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010887a:	83 c0 70             	add    $0x70,%eax
8010887d:	83 ec 08             	sub    $0x8,%esp
80108880:	6a 38                	push   $0x38
80108882:	50                   	push   %eax
80108883:	e8 38 fb ff ff       	call   801083c0 <lgdt>
80108888:	83 c4 10             	add    $0x10,%esp
  loadgs(SEG_KCPU << 3);
8010888b:	83 ec 0c             	sub    $0xc,%esp
8010888e:	6a 18                	push   $0x18
80108890:	e8 6c fb ff ff       	call   80108401 <loadgs>
80108895:	83 c4 10             	add    $0x10,%esp
  
  // Initialize cpu-local storage.
  cpu = c;
80108898:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010889b:	65 a3 00 00 00 00    	mov    %eax,%gs:0x0
  proc = 0;
801088a1:	65 c7 05 04 00 00 00 	movl   $0x0,%gs:0x4
801088a8:	00 00 00 00 
}
801088ac:	90                   	nop
801088ad:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801088b0:	c9                   	leave  
801088b1:	c3                   	ret    

801088b2 <walkpgdir>:
// Return the address of the PTE in page table pgdir
// that corresponds to virtual address va.  If alloc!=0,
// create any required page table pages.
static pte_t *
walkpgdir(pde_t *pgdir, const void *va, int alloc)
{
801088b2:	55                   	push   %ebp
801088b3:	89 e5                	mov    %esp,%ebp
801088b5:	83 ec 18             	sub    $0x18,%esp
  pde_t *pde;
  pte_t *pgtab;

  pde = &pgdir[PDX(va)];
801088b8:	8b 45 0c             	mov    0xc(%ebp),%eax
801088bb:	c1 e8 16             	shr    $0x16,%eax
801088be:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
801088c5:	8b 45 08             	mov    0x8(%ebp),%eax
801088c8:	01 d0                	add    %edx,%eax
801088ca:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(*pde & PTE_P){
801088cd:	8b 45 f0             	mov    -0x10(%ebp),%eax
801088d0:	8b 00                	mov    (%eax),%eax
801088d2:	83 e0 01             	and    $0x1,%eax
801088d5:	85 c0                	test   %eax,%eax
801088d7:	74 18                	je     801088f1 <walkpgdir+0x3f>
    pgtab = (pte_t*)p2v(PTE_ADDR(*pde));
801088d9:	8b 45 f0             	mov    -0x10(%ebp),%eax
801088dc:	8b 00                	mov    (%eax),%eax
801088de:	25 00 f0 ff ff       	and    $0xfffff000,%eax
801088e3:	50                   	push   %eax
801088e4:	e8 47 fb ff ff       	call   80108430 <p2v>
801088e9:	83 c4 04             	add    $0x4,%esp
801088ec:	89 45 f4             	mov    %eax,-0xc(%ebp)
801088ef:	eb 48                	jmp    80108939 <walkpgdir+0x87>
  } else {
    if(!alloc || (pgtab = (pte_t*)kalloc()) == 0)
801088f1:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
801088f5:	74 0e                	je     80108905 <walkpgdir+0x53>
801088f7:	e8 08 a4 ff ff       	call   80102d04 <kalloc>
801088fc:	89 45 f4             	mov    %eax,-0xc(%ebp)
801088ff:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80108903:	75 07                	jne    8010890c <walkpgdir+0x5a>
      return 0;
80108905:	b8 00 00 00 00       	mov    $0x0,%eax
8010890a:	eb 44                	jmp    80108950 <walkpgdir+0x9e>
    // Make sure all those PTE_P bits are zero.
    memset(pgtab, 0, PGSIZE);
8010890c:	83 ec 04             	sub    $0x4,%esp
8010890f:	68 00 10 00 00       	push   $0x1000
80108914:	6a 00                	push   $0x0
80108916:	ff 75 f4             	pushl  -0xc(%ebp)
80108919:	e8 8d d4 ff ff       	call   80105dab <memset>
8010891e:	83 c4 10             	add    $0x10,%esp
    // The permissions here are overly generous, but they can
    // be further restricted by the permissions in the page table 
    // entries, if necessary.
    *pde = v2p(pgtab) | PTE_P | PTE_W | PTE_U;
80108921:	83 ec 0c             	sub    $0xc,%esp
80108924:	ff 75 f4             	pushl  -0xc(%ebp)
80108927:	e8 f7 fa ff ff       	call   80108423 <v2p>
8010892c:	83 c4 10             	add    $0x10,%esp
8010892f:	83 c8 07             	or     $0x7,%eax
80108932:	89 c2                	mov    %eax,%edx
80108934:	8b 45 f0             	mov    -0x10(%ebp),%eax
80108937:	89 10                	mov    %edx,(%eax)
  }
  return &pgtab[PTX(va)];
80108939:	8b 45 0c             	mov    0xc(%ebp),%eax
8010893c:	c1 e8 0c             	shr    $0xc,%eax
8010893f:	25 ff 03 00 00       	and    $0x3ff,%eax
80108944:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
8010894b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010894e:	01 d0                	add    %edx,%eax
}
80108950:	c9                   	leave  
80108951:	c3                   	ret    

80108952 <mappages>:
// Create PTEs for virtual addresses starting at va that refer to
// physical addresses starting at pa. va and size might not
// be page-aligned.
static int
mappages(pde_t *pgdir, void *va, uint size, uint pa, int perm)
{
80108952:	55                   	push   %ebp
80108953:	89 e5                	mov    %esp,%ebp
80108955:	83 ec 18             	sub    $0x18,%esp
  char *a, *last;
  pte_t *pte;
  
  a = (char*)PGROUNDDOWN((uint)va);
80108958:	8b 45 0c             	mov    0xc(%ebp),%eax
8010895b:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80108960:	89 45 f4             	mov    %eax,-0xc(%ebp)
  last = (char*)PGROUNDDOWN(((uint)va) + size - 1);
80108963:	8b 55 0c             	mov    0xc(%ebp),%edx
80108966:	8b 45 10             	mov    0x10(%ebp),%eax
80108969:	01 d0                	add    %edx,%eax
8010896b:	83 e8 01             	sub    $0x1,%eax
8010896e:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80108973:	89 45 f0             	mov    %eax,-0x10(%ebp)
  for(;;){
    if((pte = walkpgdir(pgdir, a, 1)) == 0)
80108976:	83 ec 04             	sub    $0x4,%esp
80108979:	6a 01                	push   $0x1
8010897b:	ff 75 f4             	pushl  -0xc(%ebp)
8010897e:	ff 75 08             	pushl  0x8(%ebp)
80108981:	e8 2c ff ff ff       	call   801088b2 <walkpgdir>
80108986:	83 c4 10             	add    $0x10,%esp
80108989:	89 45 ec             	mov    %eax,-0x14(%ebp)
8010898c:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
80108990:	75 07                	jne    80108999 <mappages+0x47>
      return -1;
80108992:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80108997:	eb 47                	jmp    801089e0 <mappages+0x8e>
    if(*pte & PTE_P)
80108999:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010899c:	8b 00                	mov    (%eax),%eax
8010899e:	83 e0 01             	and    $0x1,%eax
801089a1:	85 c0                	test   %eax,%eax
801089a3:	74 0d                	je     801089b2 <mappages+0x60>
      panic("remap");
801089a5:	83 ec 0c             	sub    $0xc,%esp
801089a8:	68 98 99 10 80       	push   $0x80109998
801089ad:	e8 b4 7b ff ff       	call   80100566 <panic>
    *pte = pa | perm | PTE_P;
801089b2:	8b 45 18             	mov    0x18(%ebp),%eax
801089b5:	0b 45 14             	or     0x14(%ebp),%eax
801089b8:	83 c8 01             	or     $0x1,%eax
801089bb:	89 c2                	mov    %eax,%edx
801089bd:	8b 45 ec             	mov    -0x14(%ebp),%eax
801089c0:	89 10                	mov    %edx,(%eax)
    if(a == last)
801089c2:	8b 45 f4             	mov    -0xc(%ebp),%eax
801089c5:	3b 45 f0             	cmp    -0x10(%ebp),%eax
801089c8:	74 10                	je     801089da <mappages+0x88>
      break;
    a += PGSIZE;
801089ca:	81 45 f4 00 10 00 00 	addl   $0x1000,-0xc(%ebp)
    pa += PGSIZE;
801089d1:	81 45 14 00 10 00 00 	addl   $0x1000,0x14(%ebp)
  }
801089d8:	eb 9c                	jmp    80108976 <mappages+0x24>
      return -1;
    if(*pte & PTE_P)
      panic("remap");
    *pte = pa | perm | PTE_P;
    if(a == last)
      break;
801089da:	90                   	nop
    a += PGSIZE;
    pa += PGSIZE;
  }
  return 0;
801089db:	b8 00 00 00 00       	mov    $0x0,%eax
}
801089e0:	c9                   	leave  
801089e1:	c3                   	ret    

801089e2 <setupkvm>:
};

// Set up kernel part of a page table.
pde_t*
setupkvm(void)
{
801089e2:	55                   	push   %ebp
801089e3:	89 e5                	mov    %esp,%ebp
801089e5:	53                   	push   %ebx
801089e6:	83 ec 14             	sub    $0x14,%esp
  pde_t *pgdir;
  struct kmap *k;

  if((pgdir = (pde_t*)kalloc()) == 0)
801089e9:	e8 16 a3 ff ff       	call   80102d04 <kalloc>
801089ee:	89 45 f0             	mov    %eax,-0x10(%ebp)
801089f1:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
801089f5:	75 0a                	jne    80108a01 <setupkvm+0x1f>
    return 0;
801089f7:	b8 00 00 00 00       	mov    $0x0,%eax
801089fc:	e9 8e 00 00 00       	jmp    80108a8f <setupkvm+0xad>
  memset(pgdir, 0, PGSIZE);
80108a01:	83 ec 04             	sub    $0x4,%esp
80108a04:	68 00 10 00 00       	push   $0x1000
80108a09:	6a 00                	push   $0x0
80108a0b:	ff 75 f0             	pushl  -0x10(%ebp)
80108a0e:	e8 98 d3 ff ff       	call   80105dab <memset>
80108a13:	83 c4 10             	add    $0x10,%esp
  if (p2v(PHYSTOP) > (void*)DEVSPACE)
80108a16:	83 ec 0c             	sub    $0xc,%esp
80108a19:	68 00 00 00 0e       	push   $0xe000000
80108a1e:	e8 0d fa ff ff       	call   80108430 <p2v>
80108a23:	83 c4 10             	add    $0x10,%esp
80108a26:	3d 00 00 00 fe       	cmp    $0xfe000000,%eax
80108a2b:	76 0d                	jbe    80108a3a <setupkvm+0x58>
    panic("PHYSTOP too high");
80108a2d:	83 ec 0c             	sub    $0xc,%esp
80108a30:	68 9e 99 10 80       	push   $0x8010999e
80108a35:	e8 2c 7b ff ff       	call   80100566 <panic>
  for(k = kmap; k < &kmap[NELEM(kmap)]; k++)
80108a3a:	c7 45 f4 c0 c4 10 80 	movl   $0x8010c4c0,-0xc(%ebp)
80108a41:	eb 40                	jmp    80108a83 <setupkvm+0xa1>
    if(mappages(pgdir, k->virt, k->phys_end - k->phys_start, 
80108a43:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108a46:	8b 48 0c             	mov    0xc(%eax),%ecx
                (uint)k->phys_start, k->perm) < 0)
80108a49:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108a4c:	8b 50 04             	mov    0x4(%eax),%edx
    return 0;
  memset(pgdir, 0, PGSIZE);
  if (p2v(PHYSTOP) > (void*)DEVSPACE)
    panic("PHYSTOP too high");
  for(k = kmap; k < &kmap[NELEM(kmap)]; k++)
    if(mappages(pgdir, k->virt, k->phys_end - k->phys_start, 
80108a4f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108a52:	8b 58 08             	mov    0x8(%eax),%ebx
80108a55:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108a58:	8b 40 04             	mov    0x4(%eax),%eax
80108a5b:	29 c3                	sub    %eax,%ebx
80108a5d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108a60:	8b 00                	mov    (%eax),%eax
80108a62:	83 ec 0c             	sub    $0xc,%esp
80108a65:	51                   	push   %ecx
80108a66:	52                   	push   %edx
80108a67:	53                   	push   %ebx
80108a68:	50                   	push   %eax
80108a69:	ff 75 f0             	pushl  -0x10(%ebp)
80108a6c:	e8 e1 fe ff ff       	call   80108952 <mappages>
80108a71:	83 c4 20             	add    $0x20,%esp
80108a74:	85 c0                	test   %eax,%eax
80108a76:	79 07                	jns    80108a7f <setupkvm+0x9d>
                (uint)k->phys_start, k->perm) < 0)
      return 0;
80108a78:	b8 00 00 00 00       	mov    $0x0,%eax
80108a7d:	eb 10                	jmp    80108a8f <setupkvm+0xad>
  if((pgdir = (pde_t*)kalloc()) == 0)
    return 0;
  memset(pgdir, 0, PGSIZE);
  if (p2v(PHYSTOP) > (void*)DEVSPACE)
    panic("PHYSTOP too high");
  for(k = kmap; k < &kmap[NELEM(kmap)]; k++)
80108a7f:	83 45 f4 10          	addl   $0x10,-0xc(%ebp)
80108a83:	81 7d f4 00 c5 10 80 	cmpl   $0x8010c500,-0xc(%ebp)
80108a8a:	72 b7                	jb     80108a43 <setupkvm+0x61>
    if(mappages(pgdir, k->virt, k->phys_end - k->phys_start, 
                (uint)k->phys_start, k->perm) < 0)
      return 0;
  return pgdir;
80108a8c:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
80108a8f:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80108a92:	c9                   	leave  
80108a93:	c3                   	ret    

80108a94 <kvmalloc>:

// Allocate one page table for the machine for the kernel address
// space for scheduler processes.
void
kvmalloc(void)
{
80108a94:	55                   	push   %ebp
80108a95:	89 e5                	mov    %esp,%ebp
80108a97:	83 ec 08             	sub    $0x8,%esp
  kpgdir = setupkvm();
80108a9a:	e8 43 ff ff ff       	call   801089e2 <setupkvm>
80108a9f:	a3 38 68 11 80       	mov    %eax,0x80116838
  switchkvm();
80108aa4:	e8 03 00 00 00       	call   80108aac <switchkvm>
}
80108aa9:	90                   	nop
80108aaa:	c9                   	leave  
80108aab:	c3                   	ret    

80108aac <switchkvm>:

// Switch h/w page table register to the kernel-only page table,
// for when no process is running.
void
switchkvm(void)
{
80108aac:	55                   	push   %ebp
80108aad:	89 e5                	mov    %esp,%ebp
  lcr3(v2p(kpgdir));   // switch to the kernel page table
80108aaf:	a1 38 68 11 80       	mov    0x80116838,%eax
80108ab4:	50                   	push   %eax
80108ab5:	e8 69 f9 ff ff       	call   80108423 <v2p>
80108aba:	83 c4 04             	add    $0x4,%esp
80108abd:	50                   	push   %eax
80108abe:	e8 54 f9 ff ff       	call   80108417 <lcr3>
80108ac3:	83 c4 04             	add    $0x4,%esp
}
80108ac6:	90                   	nop
80108ac7:	c9                   	leave  
80108ac8:	c3                   	ret    

80108ac9 <switchuvm>:

// Switch TSS and h/w page table to correspond to process p.
void
switchuvm(struct proc *p)
{
80108ac9:	55                   	push   %ebp
80108aca:	89 e5                	mov    %esp,%ebp
80108acc:	56                   	push   %esi
80108acd:	53                   	push   %ebx
  pushcli();
80108ace:	e8 d2 d1 ff ff       	call   80105ca5 <pushcli>
  cpu->gdt[SEG_TSS] = SEG16(STS_T32A, &cpu->ts, sizeof(cpu->ts)-1, 0);
80108ad3:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80108ad9:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
80108ae0:	83 c2 08             	add    $0x8,%edx
80108ae3:	89 d6                	mov    %edx,%esi
80108ae5:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
80108aec:	83 c2 08             	add    $0x8,%edx
80108aef:	c1 ea 10             	shr    $0x10,%edx
80108af2:	89 d3                	mov    %edx,%ebx
80108af4:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
80108afb:	83 c2 08             	add    $0x8,%edx
80108afe:	c1 ea 18             	shr    $0x18,%edx
80108b01:	89 d1                	mov    %edx,%ecx
80108b03:	66 c7 80 a0 00 00 00 	movw   $0x67,0xa0(%eax)
80108b0a:	67 00 
80108b0c:	66 89 b0 a2 00 00 00 	mov    %si,0xa2(%eax)
80108b13:	88 98 a4 00 00 00    	mov    %bl,0xa4(%eax)
80108b19:	0f b6 90 a5 00 00 00 	movzbl 0xa5(%eax),%edx
80108b20:	83 e2 f0             	and    $0xfffffff0,%edx
80108b23:	83 ca 09             	or     $0x9,%edx
80108b26:	88 90 a5 00 00 00    	mov    %dl,0xa5(%eax)
80108b2c:	0f b6 90 a5 00 00 00 	movzbl 0xa5(%eax),%edx
80108b33:	83 ca 10             	or     $0x10,%edx
80108b36:	88 90 a5 00 00 00    	mov    %dl,0xa5(%eax)
80108b3c:	0f b6 90 a5 00 00 00 	movzbl 0xa5(%eax),%edx
80108b43:	83 e2 9f             	and    $0xffffff9f,%edx
80108b46:	88 90 a5 00 00 00    	mov    %dl,0xa5(%eax)
80108b4c:	0f b6 90 a5 00 00 00 	movzbl 0xa5(%eax),%edx
80108b53:	83 ca 80             	or     $0xffffff80,%edx
80108b56:	88 90 a5 00 00 00    	mov    %dl,0xa5(%eax)
80108b5c:	0f b6 90 a6 00 00 00 	movzbl 0xa6(%eax),%edx
80108b63:	83 e2 f0             	and    $0xfffffff0,%edx
80108b66:	88 90 a6 00 00 00    	mov    %dl,0xa6(%eax)
80108b6c:	0f b6 90 a6 00 00 00 	movzbl 0xa6(%eax),%edx
80108b73:	83 e2 ef             	and    $0xffffffef,%edx
80108b76:	88 90 a6 00 00 00    	mov    %dl,0xa6(%eax)
80108b7c:	0f b6 90 a6 00 00 00 	movzbl 0xa6(%eax),%edx
80108b83:	83 e2 df             	and    $0xffffffdf,%edx
80108b86:	88 90 a6 00 00 00    	mov    %dl,0xa6(%eax)
80108b8c:	0f b6 90 a6 00 00 00 	movzbl 0xa6(%eax),%edx
80108b93:	83 ca 40             	or     $0x40,%edx
80108b96:	88 90 a6 00 00 00    	mov    %dl,0xa6(%eax)
80108b9c:	0f b6 90 a6 00 00 00 	movzbl 0xa6(%eax),%edx
80108ba3:	83 e2 7f             	and    $0x7f,%edx
80108ba6:	88 90 a6 00 00 00    	mov    %dl,0xa6(%eax)
80108bac:	88 88 a7 00 00 00    	mov    %cl,0xa7(%eax)
  cpu->gdt[SEG_TSS].s = 0;
80108bb2:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80108bb8:	0f b6 90 a5 00 00 00 	movzbl 0xa5(%eax),%edx
80108bbf:	83 e2 ef             	and    $0xffffffef,%edx
80108bc2:	88 90 a5 00 00 00    	mov    %dl,0xa5(%eax)
  cpu->ts.ss0 = SEG_KDATA << 3;
80108bc8:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80108bce:	66 c7 40 10 10 00    	movw   $0x10,0x10(%eax)
  cpu->ts.esp0 = (uint)proc->kstack + KSTACKSIZE;
80108bd4:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80108bda:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
80108be1:	8b 52 08             	mov    0x8(%edx),%edx
80108be4:	81 c2 00 10 00 00    	add    $0x1000,%edx
80108bea:	89 50 0c             	mov    %edx,0xc(%eax)
  ltr(SEG_TSS << 3);
80108bed:	83 ec 0c             	sub    $0xc,%esp
80108bf0:	6a 30                	push   $0x30
80108bf2:	e8 f3 f7 ff ff       	call   801083ea <ltr>
80108bf7:	83 c4 10             	add    $0x10,%esp
  if(p->pgdir == 0)
80108bfa:	8b 45 08             	mov    0x8(%ebp),%eax
80108bfd:	8b 40 04             	mov    0x4(%eax),%eax
80108c00:	85 c0                	test   %eax,%eax
80108c02:	75 0d                	jne    80108c11 <switchuvm+0x148>
    panic("switchuvm: no pgdir");
80108c04:	83 ec 0c             	sub    $0xc,%esp
80108c07:	68 af 99 10 80       	push   $0x801099af
80108c0c:	e8 55 79 ff ff       	call   80100566 <panic>
  lcr3(v2p(p->pgdir));  // switch to new address space
80108c11:	8b 45 08             	mov    0x8(%ebp),%eax
80108c14:	8b 40 04             	mov    0x4(%eax),%eax
80108c17:	83 ec 0c             	sub    $0xc,%esp
80108c1a:	50                   	push   %eax
80108c1b:	e8 03 f8 ff ff       	call   80108423 <v2p>
80108c20:	83 c4 10             	add    $0x10,%esp
80108c23:	83 ec 0c             	sub    $0xc,%esp
80108c26:	50                   	push   %eax
80108c27:	e8 eb f7 ff ff       	call   80108417 <lcr3>
80108c2c:	83 c4 10             	add    $0x10,%esp
  popcli();
80108c2f:	e8 b6 d0 ff ff       	call   80105cea <popcli>
}
80108c34:	90                   	nop
80108c35:	8d 65 f8             	lea    -0x8(%ebp),%esp
80108c38:	5b                   	pop    %ebx
80108c39:	5e                   	pop    %esi
80108c3a:	5d                   	pop    %ebp
80108c3b:	c3                   	ret    

80108c3c <inituvm>:

// Load the initcode into address 0 of pgdir.
// sz must be less than a page.
void
inituvm(pde_t *pgdir, char *init, uint sz)
{
80108c3c:	55                   	push   %ebp
80108c3d:	89 e5                	mov    %esp,%ebp
80108c3f:	83 ec 18             	sub    $0x18,%esp
  char *mem;
  
  if(sz >= PGSIZE)
80108c42:	81 7d 10 ff 0f 00 00 	cmpl   $0xfff,0x10(%ebp)
80108c49:	76 0d                	jbe    80108c58 <inituvm+0x1c>
    panic("inituvm: more than a page");
80108c4b:	83 ec 0c             	sub    $0xc,%esp
80108c4e:	68 c3 99 10 80       	push   $0x801099c3
80108c53:	e8 0e 79 ff ff       	call   80100566 <panic>
  mem = kalloc();
80108c58:	e8 a7 a0 ff ff       	call   80102d04 <kalloc>
80108c5d:	89 45 f4             	mov    %eax,-0xc(%ebp)
  memset(mem, 0, PGSIZE);
80108c60:	83 ec 04             	sub    $0x4,%esp
80108c63:	68 00 10 00 00       	push   $0x1000
80108c68:	6a 00                	push   $0x0
80108c6a:	ff 75 f4             	pushl  -0xc(%ebp)
80108c6d:	e8 39 d1 ff ff       	call   80105dab <memset>
80108c72:	83 c4 10             	add    $0x10,%esp
  mappages(pgdir, 0, PGSIZE, v2p(mem), PTE_W|PTE_U);
80108c75:	83 ec 0c             	sub    $0xc,%esp
80108c78:	ff 75 f4             	pushl  -0xc(%ebp)
80108c7b:	e8 a3 f7 ff ff       	call   80108423 <v2p>
80108c80:	83 c4 10             	add    $0x10,%esp
80108c83:	83 ec 0c             	sub    $0xc,%esp
80108c86:	6a 06                	push   $0x6
80108c88:	50                   	push   %eax
80108c89:	68 00 10 00 00       	push   $0x1000
80108c8e:	6a 00                	push   $0x0
80108c90:	ff 75 08             	pushl  0x8(%ebp)
80108c93:	e8 ba fc ff ff       	call   80108952 <mappages>
80108c98:	83 c4 20             	add    $0x20,%esp
  memmove(mem, init, sz);
80108c9b:	83 ec 04             	sub    $0x4,%esp
80108c9e:	ff 75 10             	pushl  0x10(%ebp)
80108ca1:	ff 75 0c             	pushl  0xc(%ebp)
80108ca4:	ff 75 f4             	pushl  -0xc(%ebp)
80108ca7:	e8 be d1 ff ff       	call   80105e6a <memmove>
80108cac:	83 c4 10             	add    $0x10,%esp
}
80108caf:	90                   	nop
80108cb0:	c9                   	leave  
80108cb1:	c3                   	ret    

80108cb2 <loaduvm>:

// Load a program segment into pgdir.  addr must be page-aligned
// and the pages from addr to addr+sz must already be mapped.
int
loaduvm(pde_t *pgdir, char *addr, struct inode *ip, uint offset, uint sz)
{
80108cb2:	55                   	push   %ebp
80108cb3:	89 e5                	mov    %esp,%ebp
80108cb5:	53                   	push   %ebx
80108cb6:	83 ec 14             	sub    $0x14,%esp
  uint i, pa, n;
  pte_t *pte;

  if((uint) addr % PGSIZE != 0)
80108cb9:	8b 45 0c             	mov    0xc(%ebp),%eax
80108cbc:	25 ff 0f 00 00       	and    $0xfff,%eax
80108cc1:	85 c0                	test   %eax,%eax
80108cc3:	74 0d                	je     80108cd2 <loaduvm+0x20>
    panic("loaduvm: addr must be page aligned");
80108cc5:	83 ec 0c             	sub    $0xc,%esp
80108cc8:	68 e0 99 10 80       	push   $0x801099e0
80108ccd:	e8 94 78 ff ff       	call   80100566 <panic>
  for(i = 0; i < sz; i += PGSIZE){
80108cd2:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80108cd9:	e9 95 00 00 00       	jmp    80108d73 <loaduvm+0xc1>
    if((pte = walkpgdir(pgdir, addr+i, 0)) == 0)
80108cde:	8b 55 0c             	mov    0xc(%ebp),%edx
80108ce1:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108ce4:	01 d0                	add    %edx,%eax
80108ce6:	83 ec 04             	sub    $0x4,%esp
80108ce9:	6a 00                	push   $0x0
80108ceb:	50                   	push   %eax
80108cec:	ff 75 08             	pushl  0x8(%ebp)
80108cef:	e8 be fb ff ff       	call   801088b2 <walkpgdir>
80108cf4:	83 c4 10             	add    $0x10,%esp
80108cf7:	89 45 ec             	mov    %eax,-0x14(%ebp)
80108cfa:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
80108cfe:	75 0d                	jne    80108d0d <loaduvm+0x5b>
      panic("loaduvm: address should exist");
80108d00:	83 ec 0c             	sub    $0xc,%esp
80108d03:	68 03 9a 10 80       	push   $0x80109a03
80108d08:	e8 59 78 ff ff       	call   80100566 <panic>
    pa = PTE_ADDR(*pte);
80108d0d:	8b 45 ec             	mov    -0x14(%ebp),%eax
80108d10:	8b 00                	mov    (%eax),%eax
80108d12:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80108d17:	89 45 e8             	mov    %eax,-0x18(%ebp)
    if(sz - i < PGSIZE)
80108d1a:	8b 45 18             	mov    0x18(%ebp),%eax
80108d1d:	2b 45 f4             	sub    -0xc(%ebp),%eax
80108d20:	3d ff 0f 00 00       	cmp    $0xfff,%eax
80108d25:	77 0b                	ja     80108d32 <loaduvm+0x80>
      n = sz - i;
80108d27:	8b 45 18             	mov    0x18(%ebp),%eax
80108d2a:	2b 45 f4             	sub    -0xc(%ebp),%eax
80108d2d:	89 45 f0             	mov    %eax,-0x10(%ebp)
80108d30:	eb 07                	jmp    80108d39 <loaduvm+0x87>
    else
      n = PGSIZE;
80108d32:	c7 45 f0 00 10 00 00 	movl   $0x1000,-0x10(%ebp)
    if(readi(ip, p2v(pa), offset+i, n) != n)
80108d39:	8b 55 14             	mov    0x14(%ebp),%edx
80108d3c:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108d3f:	8d 1c 02             	lea    (%edx,%eax,1),%ebx
80108d42:	83 ec 0c             	sub    $0xc,%esp
80108d45:	ff 75 e8             	pushl  -0x18(%ebp)
80108d48:	e8 e3 f6 ff ff       	call   80108430 <p2v>
80108d4d:	83 c4 10             	add    $0x10,%esp
80108d50:	ff 75 f0             	pushl  -0x10(%ebp)
80108d53:	53                   	push   %ebx
80108d54:	50                   	push   %eax
80108d55:	ff 75 10             	pushl  0x10(%ebp)
80108d58:	e8 19 92 ff ff       	call   80101f76 <readi>
80108d5d:	83 c4 10             	add    $0x10,%esp
80108d60:	3b 45 f0             	cmp    -0x10(%ebp),%eax
80108d63:	74 07                	je     80108d6c <loaduvm+0xba>
      return -1;
80108d65:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80108d6a:	eb 18                	jmp    80108d84 <loaduvm+0xd2>
  uint i, pa, n;
  pte_t *pte;

  if((uint) addr % PGSIZE != 0)
    panic("loaduvm: addr must be page aligned");
  for(i = 0; i < sz; i += PGSIZE){
80108d6c:	81 45 f4 00 10 00 00 	addl   $0x1000,-0xc(%ebp)
80108d73:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108d76:	3b 45 18             	cmp    0x18(%ebp),%eax
80108d79:	0f 82 5f ff ff ff    	jb     80108cde <loaduvm+0x2c>
    else
      n = PGSIZE;
    if(readi(ip, p2v(pa), offset+i, n) != n)
      return -1;
  }
  return 0;
80108d7f:	b8 00 00 00 00       	mov    $0x0,%eax
}
80108d84:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80108d87:	c9                   	leave  
80108d88:	c3                   	ret    

80108d89 <allocuvm>:

// Allocate page tables and physical memory to grow process from oldsz to
// newsz, which need not be page aligned.  Returns new size or 0 on error.
int
allocuvm(pde_t *pgdir, uint oldsz, uint newsz)
{
80108d89:	55                   	push   %ebp
80108d8a:	89 e5                	mov    %esp,%ebp
80108d8c:	83 ec 18             	sub    $0x18,%esp
  char *mem;
  uint a;

  if(newsz >= KERNBASE)
80108d8f:	8b 45 10             	mov    0x10(%ebp),%eax
80108d92:	85 c0                	test   %eax,%eax
80108d94:	79 0a                	jns    80108da0 <allocuvm+0x17>
    return 0;
80108d96:	b8 00 00 00 00       	mov    $0x0,%eax
80108d9b:	e9 b0 00 00 00       	jmp    80108e50 <allocuvm+0xc7>
  if(newsz < oldsz)
80108da0:	8b 45 10             	mov    0x10(%ebp),%eax
80108da3:	3b 45 0c             	cmp    0xc(%ebp),%eax
80108da6:	73 08                	jae    80108db0 <allocuvm+0x27>
    return oldsz;
80108da8:	8b 45 0c             	mov    0xc(%ebp),%eax
80108dab:	e9 a0 00 00 00       	jmp    80108e50 <allocuvm+0xc7>

  a = PGROUNDUP(oldsz);
80108db0:	8b 45 0c             	mov    0xc(%ebp),%eax
80108db3:	05 ff 0f 00 00       	add    $0xfff,%eax
80108db8:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80108dbd:	89 45 f4             	mov    %eax,-0xc(%ebp)
  for(; a < newsz; a += PGSIZE){
80108dc0:	eb 7f                	jmp    80108e41 <allocuvm+0xb8>
    mem = kalloc();
80108dc2:	e8 3d 9f ff ff       	call   80102d04 <kalloc>
80108dc7:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if(mem == 0){
80108dca:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80108dce:	75 2b                	jne    80108dfb <allocuvm+0x72>
      cprintf("allocuvm out of memory\n");
80108dd0:	83 ec 0c             	sub    $0xc,%esp
80108dd3:	68 21 9a 10 80       	push   $0x80109a21
80108dd8:	e8 e9 75 ff ff       	call   801003c6 <cprintf>
80108ddd:	83 c4 10             	add    $0x10,%esp
      deallocuvm(pgdir, newsz, oldsz);
80108de0:	83 ec 04             	sub    $0x4,%esp
80108de3:	ff 75 0c             	pushl  0xc(%ebp)
80108de6:	ff 75 10             	pushl  0x10(%ebp)
80108de9:	ff 75 08             	pushl  0x8(%ebp)
80108dec:	e8 61 00 00 00       	call   80108e52 <deallocuvm>
80108df1:	83 c4 10             	add    $0x10,%esp
      return 0;
80108df4:	b8 00 00 00 00       	mov    $0x0,%eax
80108df9:	eb 55                	jmp    80108e50 <allocuvm+0xc7>
    }
    memset(mem, 0, PGSIZE);
80108dfb:	83 ec 04             	sub    $0x4,%esp
80108dfe:	68 00 10 00 00       	push   $0x1000
80108e03:	6a 00                	push   $0x0
80108e05:	ff 75 f0             	pushl  -0x10(%ebp)
80108e08:	e8 9e cf ff ff       	call   80105dab <memset>
80108e0d:	83 c4 10             	add    $0x10,%esp
    mappages(pgdir, (char*)a, PGSIZE, v2p(mem), PTE_W|PTE_U);
80108e10:	83 ec 0c             	sub    $0xc,%esp
80108e13:	ff 75 f0             	pushl  -0x10(%ebp)
80108e16:	e8 08 f6 ff ff       	call   80108423 <v2p>
80108e1b:	83 c4 10             	add    $0x10,%esp
80108e1e:	89 c2                	mov    %eax,%edx
80108e20:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108e23:	83 ec 0c             	sub    $0xc,%esp
80108e26:	6a 06                	push   $0x6
80108e28:	52                   	push   %edx
80108e29:	68 00 10 00 00       	push   $0x1000
80108e2e:	50                   	push   %eax
80108e2f:	ff 75 08             	pushl  0x8(%ebp)
80108e32:	e8 1b fb ff ff       	call   80108952 <mappages>
80108e37:	83 c4 20             	add    $0x20,%esp
    return 0;
  if(newsz < oldsz)
    return oldsz;

  a = PGROUNDUP(oldsz);
  for(; a < newsz; a += PGSIZE){
80108e3a:	81 45 f4 00 10 00 00 	addl   $0x1000,-0xc(%ebp)
80108e41:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108e44:	3b 45 10             	cmp    0x10(%ebp),%eax
80108e47:	0f 82 75 ff ff ff    	jb     80108dc2 <allocuvm+0x39>
      return 0;
    }
    memset(mem, 0, PGSIZE);
    mappages(pgdir, (char*)a, PGSIZE, v2p(mem), PTE_W|PTE_U);
  }
  return newsz;
80108e4d:	8b 45 10             	mov    0x10(%ebp),%eax
}
80108e50:	c9                   	leave  
80108e51:	c3                   	ret    

80108e52 <deallocuvm>:
// newsz.  oldsz and newsz need not be page-aligned, nor does newsz
// need to be less than oldsz.  oldsz can be larger than the actual
// process size.  Returns the new process size.
int
deallocuvm(pde_t *pgdir, uint oldsz, uint newsz)
{
80108e52:	55                   	push   %ebp
80108e53:	89 e5                	mov    %esp,%ebp
80108e55:	83 ec 18             	sub    $0x18,%esp
  pte_t *pte;
  uint a, pa;

  if(newsz >= oldsz)
80108e58:	8b 45 10             	mov    0x10(%ebp),%eax
80108e5b:	3b 45 0c             	cmp    0xc(%ebp),%eax
80108e5e:	72 08                	jb     80108e68 <deallocuvm+0x16>
    return oldsz;
80108e60:	8b 45 0c             	mov    0xc(%ebp),%eax
80108e63:	e9 a5 00 00 00       	jmp    80108f0d <deallocuvm+0xbb>

  a = PGROUNDUP(newsz);
80108e68:	8b 45 10             	mov    0x10(%ebp),%eax
80108e6b:	05 ff 0f 00 00       	add    $0xfff,%eax
80108e70:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80108e75:	89 45 f4             	mov    %eax,-0xc(%ebp)
  for(; a  < oldsz; a += PGSIZE){
80108e78:	e9 81 00 00 00       	jmp    80108efe <deallocuvm+0xac>
    pte = walkpgdir(pgdir, (char*)a, 0);
80108e7d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108e80:	83 ec 04             	sub    $0x4,%esp
80108e83:	6a 00                	push   $0x0
80108e85:	50                   	push   %eax
80108e86:	ff 75 08             	pushl  0x8(%ebp)
80108e89:	e8 24 fa ff ff       	call   801088b2 <walkpgdir>
80108e8e:	83 c4 10             	add    $0x10,%esp
80108e91:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if(!pte)
80108e94:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80108e98:	75 09                	jne    80108ea3 <deallocuvm+0x51>
      a += (NPTENTRIES - 1) * PGSIZE;
80108e9a:	81 45 f4 00 f0 3f 00 	addl   $0x3ff000,-0xc(%ebp)
80108ea1:	eb 54                	jmp    80108ef7 <deallocuvm+0xa5>
    else if((*pte & PTE_P) != 0){
80108ea3:	8b 45 f0             	mov    -0x10(%ebp),%eax
80108ea6:	8b 00                	mov    (%eax),%eax
80108ea8:	83 e0 01             	and    $0x1,%eax
80108eab:	85 c0                	test   %eax,%eax
80108ead:	74 48                	je     80108ef7 <deallocuvm+0xa5>
      pa = PTE_ADDR(*pte);
80108eaf:	8b 45 f0             	mov    -0x10(%ebp),%eax
80108eb2:	8b 00                	mov    (%eax),%eax
80108eb4:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80108eb9:	89 45 ec             	mov    %eax,-0x14(%ebp)
      if(pa == 0)
80108ebc:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
80108ec0:	75 0d                	jne    80108ecf <deallocuvm+0x7d>
        panic("kfree");
80108ec2:	83 ec 0c             	sub    $0xc,%esp
80108ec5:	68 39 9a 10 80       	push   $0x80109a39
80108eca:	e8 97 76 ff ff       	call   80100566 <panic>
      char *v = p2v(pa);
80108ecf:	83 ec 0c             	sub    $0xc,%esp
80108ed2:	ff 75 ec             	pushl  -0x14(%ebp)
80108ed5:	e8 56 f5 ff ff       	call   80108430 <p2v>
80108eda:	83 c4 10             	add    $0x10,%esp
80108edd:	89 45 e8             	mov    %eax,-0x18(%ebp)
      kfree(v);
80108ee0:	83 ec 0c             	sub    $0xc,%esp
80108ee3:	ff 75 e8             	pushl  -0x18(%ebp)
80108ee6:	e8 7c 9d ff ff       	call   80102c67 <kfree>
80108eeb:	83 c4 10             	add    $0x10,%esp
      *pte = 0;
80108eee:	8b 45 f0             	mov    -0x10(%ebp),%eax
80108ef1:	c7 00 00 00 00 00    	movl   $0x0,(%eax)

  if(newsz >= oldsz)
    return oldsz;

  a = PGROUNDUP(newsz);
  for(; a  < oldsz; a += PGSIZE){
80108ef7:	81 45 f4 00 10 00 00 	addl   $0x1000,-0xc(%ebp)
80108efe:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108f01:	3b 45 0c             	cmp    0xc(%ebp),%eax
80108f04:	0f 82 73 ff ff ff    	jb     80108e7d <deallocuvm+0x2b>
      char *v = p2v(pa);
      kfree(v);
      *pte = 0;
    }
  }
  return newsz;
80108f0a:	8b 45 10             	mov    0x10(%ebp),%eax
}
80108f0d:	c9                   	leave  
80108f0e:	c3                   	ret    

80108f0f <freevm>:

// Free a page table and all the physical memory pages
// in the user part.
void
freevm(pde_t *pgdir)
{
80108f0f:	55                   	push   %ebp
80108f10:	89 e5                	mov    %esp,%ebp
80108f12:	83 ec 18             	sub    $0x18,%esp
  uint i;

  if(pgdir == 0)
80108f15:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80108f19:	75 0d                	jne    80108f28 <freevm+0x19>
    panic("freevm: no pgdir");
80108f1b:	83 ec 0c             	sub    $0xc,%esp
80108f1e:	68 3f 9a 10 80       	push   $0x80109a3f
80108f23:	e8 3e 76 ff ff       	call   80100566 <panic>
  deallocuvm(pgdir, KERNBASE, 0);
80108f28:	83 ec 04             	sub    $0x4,%esp
80108f2b:	6a 00                	push   $0x0
80108f2d:	68 00 00 00 80       	push   $0x80000000
80108f32:	ff 75 08             	pushl  0x8(%ebp)
80108f35:	e8 18 ff ff ff       	call   80108e52 <deallocuvm>
80108f3a:	83 c4 10             	add    $0x10,%esp
  for(i = 0; i < NPDENTRIES; i++){
80108f3d:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80108f44:	eb 4f                	jmp    80108f95 <freevm+0x86>
    if(pgdir[i] & PTE_P){
80108f46:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108f49:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
80108f50:	8b 45 08             	mov    0x8(%ebp),%eax
80108f53:	01 d0                	add    %edx,%eax
80108f55:	8b 00                	mov    (%eax),%eax
80108f57:	83 e0 01             	and    $0x1,%eax
80108f5a:	85 c0                	test   %eax,%eax
80108f5c:	74 33                	je     80108f91 <freevm+0x82>
      char * v = p2v(PTE_ADDR(pgdir[i]));
80108f5e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108f61:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
80108f68:	8b 45 08             	mov    0x8(%ebp),%eax
80108f6b:	01 d0                	add    %edx,%eax
80108f6d:	8b 00                	mov    (%eax),%eax
80108f6f:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80108f74:	83 ec 0c             	sub    $0xc,%esp
80108f77:	50                   	push   %eax
80108f78:	e8 b3 f4 ff ff       	call   80108430 <p2v>
80108f7d:	83 c4 10             	add    $0x10,%esp
80108f80:	89 45 f0             	mov    %eax,-0x10(%ebp)
      kfree(v);
80108f83:	83 ec 0c             	sub    $0xc,%esp
80108f86:	ff 75 f0             	pushl  -0x10(%ebp)
80108f89:	e8 d9 9c ff ff       	call   80102c67 <kfree>
80108f8e:	83 c4 10             	add    $0x10,%esp
  uint i;

  if(pgdir == 0)
    panic("freevm: no pgdir");
  deallocuvm(pgdir, KERNBASE, 0);
  for(i = 0; i < NPDENTRIES; i++){
80108f91:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80108f95:	81 7d f4 ff 03 00 00 	cmpl   $0x3ff,-0xc(%ebp)
80108f9c:	76 a8                	jbe    80108f46 <freevm+0x37>
    if(pgdir[i] & PTE_P){
      char * v = p2v(PTE_ADDR(pgdir[i]));
      kfree(v);
    }
  }
  kfree((char*)pgdir);
80108f9e:	83 ec 0c             	sub    $0xc,%esp
80108fa1:	ff 75 08             	pushl  0x8(%ebp)
80108fa4:	e8 be 9c ff ff       	call   80102c67 <kfree>
80108fa9:	83 c4 10             	add    $0x10,%esp
}
80108fac:	90                   	nop
80108fad:	c9                   	leave  
80108fae:	c3                   	ret    

80108faf <clearpteu>:

// Clear PTE_U on a page. Used to create an inaccessible
// page beneath the user stack.
void
clearpteu(pde_t *pgdir, char *uva)
{
80108faf:	55                   	push   %ebp
80108fb0:	89 e5                	mov    %esp,%ebp
80108fb2:	83 ec 18             	sub    $0x18,%esp
  pte_t *pte;

  pte = walkpgdir(pgdir, uva, 0);
80108fb5:	83 ec 04             	sub    $0x4,%esp
80108fb8:	6a 00                	push   $0x0
80108fba:	ff 75 0c             	pushl  0xc(%ebp)
80108fbd:	ff 75 08             	pushl  0x8(%ebp)
80108fc0:	e8 ed f8 ff ff       	call   801088b2 <walkpgdir>
80108fc5:	83 c4 10             	add    $0x10,%esp
80108fc8:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(pte == 0)
80108fcb:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80108fcf:	75 0d                	jne    80108fde <clearpteu+0x2f>
    panic("clearpteu");
80108fd1:	83 ec 0c             	sub    $0xc,%esp
80108fd4:	68 50 9a 10 80       	push   $0x80109a50
80108fd9:	e8 88 75 ff ff       	call   80100566 <panic>
  *pte &= ~PTE_U;
80108fde:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108fe1:	8b 00                	mov    (%eax),%eax
80108fe3:	83 e0 fb             	and    $0xfffffffb,%eax
80108fe6:	89 c2                	mov    %eax,%edx
80108fe8:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108feb:	89 10                	mov    %edx,(%eax)
}
80108fed:	90                   	nop
80108fee:	c9                   	leave  
80108fef:	c3                   	ret    

80108ff0 <copyuvm>:

// Given a parent process's page table, create a copy
// of it for a child.
pde_t*
copyuvm(pde_t *pgdir, uint sz)
{
80108ff0:	55                   	push   %ebp
80108ff1:	89 e5                	mov    %esp,%ebp
80108ff3:	53                   	push   %ebx
80108ff4:	83 ec 24             	sub    $0x24,%esp
  pde_t *d;
  pte_t *pte;
  uint pa, i, flags;
  char *mem;

  if((d = setupkvm()) == 0)
80108ff7:	e8 e6 f9 ff ff       	call   801089e2 <setupkvm>
80108ffc:	89 45 f0             	mov    %eax,-0x10(%ebp)
80108fff:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80109003:	75 0a                	jne    8010900f <copyuvm+0x1f>
    return 0;
80109005:	b8 00 00 00 00       	mov    $0x0,%eax
8010900a:	e9 f8 00 00 00       	jmp    80109107 <copyuvm+0x117>
  for(i = 0; i < sz; i += PGSIZE){
8010900f:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80109016:	e9 c4 00 00 00       	jmp    801090df <copyuvm+0xef>
    if((pte = walkpgdir(pgdir, (void *) i, 0)) == 0)
8010901b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010901e:	83 ec 04             	sub    $0x4,%esp
80109021:	6a 00                	push   $0x0
80109023:	50                   	push   %eax
80109024:	ff 75 08             	pushl  0x8(%ebp)
80109027:	e8 86 f8 ff ff       	call   801088b2 <walkpgdir>
8010902c:	83 c4 10             	add    $0x10,%esp
8010902f:	89 45 ec             	mov    %eax,-0x14(%ebp)
80109032:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
80109036:	75 0d                	jne    80109045 <copyuvm+0x55>
      panic("copyuvm: pte should exist");
80109038:	83 ec 0c             	sub    $0xc,%esp
8010903b:	68 5a 9a 10 80       	push   $0x80109a5a
80109040:	e8 21 75 ff ff       	call   80100566 <panic>
    if(!(*pte & PTE_P))
80109045:	8b 45 ec             	mov    -0x14(%ebp),%eax
80109048:	8b 00                	mov    (%eax),%eax
8010904a:	83 e0 01             	and    $0x1,%eax
8010904d:	85 c0                	test   %eax,%eax
8010904f:	75 0d                	jne    8010905e <copyuvm+0x6e>
      panic("copyuvm: page not present");
80109051:	83 ec 0c             	sub    $0xc,%esp
80109054:	68 74 9a 10 80       	push   $0x80109a74
80109059:	e8 08 75 ff ff       	call   80100566 <panic>
    pa = PTE_ADDR(*pte);
8010905e:	8b 45 ec             	mov    -0x14(%ebp),%eax
80109061:	8b 00                	mov    (%eax),%eax
80109063:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80109068:	89 45 e8             	mov    %eax,-0x18(%ebp)
    flags = PTE_FLAGS(*pte);
8010906b:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010906e:	8b 00                	mov    (%eax),%eax
80109070:	25 ff 0f 00 00       	and    $0xfff,%eax
80109075:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    if((mem = kalloc()) == 0)
80109078:	e8 87 9c ff ff       	call   80102d04 <kalloc>
8010907d:	89 45 e0             	mov    %eax,-0x20(%ebp)
80109080:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
80109084:	74 6a                	je     801090f0 <copyuvm+0x100>
      goto bad;
    memmove(mem, (char*)p2v(pa), PGSIZE);
80109086:	83 ec 0c             	sub    $0xc,%esp
80109089:	ff 75 e8             	pushl  -0x18(%ebp)
8010908c:	e8 9f f3 ff ff       	call   80108430 <p2v>
80109091:	83 c4 10             	add    $0x10,%esp
80109094:	83 ec 04             	sub    $0x4,%esp
80109097:	68 00 10 00 00       	push   $0x1000
8010909c:	50                   	push   %eax
8010909d:	ff 75 e0             	pushl  -0x20(%ebp)
801090a0:	e8 c5 cd ff ff       	call   80105e6a <memmove>
801090a5:	83 c4 10             	add    $0x10,%esp
    if(mappages(d, (void*)i, PGSIZE, v2p(mem), flags) < 0)
801090a8:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
801090ab:	83 ec 0c             	sub    $0xc,%esp
801090ae:	ff 75 e0             	pushl  -0x20(%ebp)
801090b1:	e8 6d f3 ff ff       	call   80108423 <v2p>
801090b6:	83 c4 10             	add    $0x10,%esp
801090b9:	89 c2                	mov    %eax,%edx
801090bb:	8b 45 f4             	mov    -0xc(%ebp),%eax
801090be:	83 ec 0c             	sub    $0xc,%esp
801090c1:	53                   	push   %ebx
801090c2:	52                   	push   %edx
801090c3:	68 00 10 00 00       	push   $0x1000
801090c8:	50                   	push   %eax
801090c9:	ff 75 f0             	pushl  -0x10(%ebp)
801090cc:	e8 81 f8 ff ff       	call   80108952 <mappages>
801090d1:	83 c4 20             	add    $0x20,%esp
801090d4:	85 c0                	test   %eax,%eax
801090d6:	78 1b                	js     801090f3 <copyuvm+0x103>
  uint pa, i, flags;
  char *mem;

  if((d = setupkvm()) == 0)
    return 0;
  for(i = 0; i < sz; i += PGSIZE){
801090d8:	81 45 f4 00 10 00 00 	addl   $0x1000,-0xc(%ebp)
801090df:	8b 45 f4             	mov    -0xc(%ebp),%eax
801090e2:	3b 45 0c             	cmp    0xc(%ebp),%eax
801090e5:	0f 82 30 ff ff ff    	jb     8010901b <copyuvm+0x2b>
      goto bad;
    memmove(mem, (char*)p2v(pa), PGSIZE);
    if(mappages(d, (void*)i, PGSIZE, v2p(mem), flags) < 0)
      goto bad;
  }
  return d;
801090eb:	8b 45 f0             	mov    -0x10(%ebp),%eax
801090ee:	eb 17                	jmp    80109107 <copyuvm+0x117>
    if(!(*pte & PTE_P))
      panic("copyuvm: page not present");
    pa = PTE_ADDR(*pte);
    flags = PTE_FLAGS(*pte);
    if((mem = kalloc()) == 0)
      goto bad;
801090f0:	90                   	nop
801090f1:	eb 01                	jmp    801090f4 <copyuvm+0x104>
    memmove(mem, (char*)p2v(pa), PGSIZE);
    if(mappages(d, (void*)i, PGSIZE, v2p(mem), flags) < 0)
      goto bad;
801090f3:	90                   	nop
  }
  return d;

bad:
  freevm(d);
801090f4:	83 ec 0c             	sub    $0xc,%esp
801090f7:	ff 75 f0             	pushl  -0x10(%ebp)
801090fa:	e8 10 fe ff ff       	call   80108f0f <freevm>
801090ff:	83 c4 10             	add    $0x10,%esp
  return 0;
80109102:	b8 00 00 00 00       	mov    $0x0,%eax
}
80109107:	8b 5d fc             	mov    -0x4(%ebp),%ebx
8010910a:	c9                   	leave  
8010910b:	c3                   	ret    

8010910c <uva2ka>:

// Map user virtual address to kernel address.
char*
uva2ka(pde_t *pgdir, char *uva)
{
8010910c:	55                   	push   %ebp
8010910d:	89 e5                	mov    %esp,%ebp
8010910f:	83 ec 18             	sub    $0x18,%esp
  pte_t *pte;

  pte = walkpgdir(pgdir, uva, 0);
80109112:	83 ec 04             	sub    $0x4,%esp
80109115:	6a 00                	push   $0x0
80109117:	ff 75 0c             	pushl  0xc(%ebp)
8010911a:	ff 75 08             	pushl  0x8(%ebp)
8010911d:	e8 90 f7 ff ff       	call   801088b2 <walkpgdir>
80109122:	83 c4 10             	add    $0x10,%esp
80109125:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if((*pte & PTE_P) == 0)
80109128:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010912b:	8b 00                	mov    (%eax),%eax
8010912d:	83 e0 01             	and    $0x1,%eax
80109130:	85 c0                	test   %eax,%eax
80109132:	75 07                	jne    8010913b <uva2ka+0x2f>
    return 0;
80109134:	b8 00 00 00 00       	mov    $0x0,%eax
80109139:	eb 29                	jmp    80109164 <uva2ka+0x58>
  if((*pte & PTE_U) == 0)
8010913b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010913e:	8b 00                	mov    (%eax),%eax
80109140:	83 e0 04             	and    $0x4,%eax
80109143:	85 c0                	test   %eax,%eax
80109145:	75 07                	jne    8010914e <uva2ka+0x42>
    return 0;
80109147:	b8 00 00 00 00       	mov    $0x0,%eax
8010914c:	eb 16                	jmp    80109164 <uva2ka+0x58>
  return (char*)p2v(PTE_ADDR(*pte));
8010914e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80109151:	8b 00                	mov    (%eax),%eax
80109153:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80109158:	83 ec 0c             	sub    $0xc,%esp
8010915b:	50                   	push   %eax
8010915c:	e8 cf f2 ff ff       	call   80108430 <p2v>
80109161:	83 c4 10             	add    $0x10,%esp
}
80109164:	c9                   	leave  
80109165:	c3                   	ret    

80109166 <copyout>:
// Copy len bytes from p to user address va in page table pgdir.
// Most useful when pgdir is not the current page table.
// uva2ka ensures this only works for PTE_U pages.
int
copyout(pde_t *pgdir, uint va, void *p, uint len)
{
80109166:	55                   	push   %ebp
80109167:	89 e5                	mov    %esp,%ebp
80109169:	83 ec 18             	sub    $0x18,%esp
  char *buf, *pa0;
  uint n, va0;

  buf = (char*)p;
8010916c:	8b 45 10             	mov    0x10(%ebp),%eax
8010916f:	89 45 f4             	mov    %eax,-0xc(%ebp)
  while(len > 0){
80109172:	eb 7f                	jmp    801091f3 <copyout+0x8d>
    va0 = (uint)PGROUNDDOWN(va);
80109174:	8b 45 0c             	mov    0xc(%ebp),%eax
80109177:	25 00 f0 ff ff       	and    $0xfffff000,%eax
8010917c:	89 45 ec             	mov    %eax,-0x14(%ebp)
    pa0 = uva2ka(pgdir, (char*)va0);
8010917f:	8b 45 ec             	mov    -0x14(%ebp),%eax
80109182:	83 ec 08             	sub    $0x8,%esp
80109185:	50                   	push   %eax
80109186:	ff 75 08             	pushl  0x8(%ebp)
80109189:	e8 7e ff ff ff       	call   8010910c <uva2ka>
8010918e:	83 c4 10             	add    $0x10,%esp
80109191:	89 45 e8             	mov    %eax,-0x18(%ebp)
    if(pa0 == 0)
80109194:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
80109198:	75 07                	jne    801091a1 <copyout+0x3b>
      return -1;
8010919a:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010919f:	eb 61                	jmp    80109202 <copyout+0x9c>
    n = PGSIZE - (va - va0);
801091a1:	8b 45 ec             	mov    -0x14(%ebp),%eax
801091a4:	2b 45 0c             	sub    0xc(%ebp),%eax
801091a7:	05 00 10 00 00       	add    $0x1000,%eax
801091ac:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if(n > len)
801091af:	8b 45 f0             	mov    -0x10(%ebp),%eax
801091b2:	3b 45 14             	cmp    0x14(%ebp),%eax
801091b5:	76 06                	jbe    801091bd <copyout+0x57>
      n = len;
801091b7:	8b 45 14             	mov    0x14(%ebp),%eax
801091ba:	89 45 f0             	mov    %eax,-0x10(%ebp)
    memmove(pa0 + (va - va0), buf, n);
801091bd:	8b 45 0c             	mov    0xc(%ebp),%eax
801091c0:	2b 45 ec             	sub    -0x14(%ebp),%eax
801091c3:	89 c2                	mov    %eax,%edx
801091c5:	8b 45 e8             	mov    -0x18(%ebp),%eax
801091c8:	01 d0                	add    %edx,%eax
801091ca:	83 ec 04             	sub    $0x4,%esp
801091cd:	ff 75 f0             	pushl  -0x10(%ebp)
801091d0:	ff 75 f4             	pushl  -0xc(%ebp)
801091d3:	50                   	push   %eax
801091d4:	e8 91 cc ff ff       	call   80105e6a <memmove>
801091d9:	83 c4 10             	add    $0x10,%esp
    len -= n;
801091dc:	8b 45 f0             	mov    -0x10(%ebp),%eax
801091df:	29 45 14             	sub    %eax,0x14(%ebp)
    buf += n;
801091e2:	8b 45 f0             	mov    -0x10(%ebp),%eax
801091e5:	01 45 f4             	add    %eax,-0xc(%ebp)
    va = va0 + PGSIZE;
801091e8:	8b 45 ec             	mov    -0x14(%ebp),%eax
801091eb:	05 00 10 00 00       	add    $0x1000,%eax
801091f0:	89 45 0c             	mov    %eax,0xc(%ebp)
{
  char *buf, *pa0;
  uint n, va0;

  buf = (char*)p;
  while(len > 0){
801091f3:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
801091f7:	0f 85 77 ff ff ff    	jne    80109174 <copyout+0xe>
    memmove(pa0 + (va - va0), buf, n);
    len -= n;
    buf += n;
    va = va0 + PGSIZE;
  }
  return 0;
801091fd:	b8 00 00 00 00       	mov    $0x0,%eax
}
80109202:	c9                   	leave  
80109203:	c3                   	ret    
