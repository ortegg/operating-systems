
_time:     file format elf32-i386


Disassembly of section .text:

00000000 <main>:
#include "types.h"
#include "user.h"

int
main(int argc, char * argv[])
{  
   0:	8d 4c 24 04          	lea    0x4(%esp),%ecx
   4:	83 e4 f0             	and    $0xfffffff0,%esp
   7:	ff 71 fc             	pushl  -0x4(%ecx)
   a:	55                   	push   %ebp
   b:	89 e5                	mov    %esp,%ebp
   d:	56                   	push   %esi
   e:	53                   	push   %ebx
   f:	51                   	push   %ecx
  10:	83 ec 1c             	sub    $0x1c,%esp
  13:	89 cb                	mov    %ecx,%ebx
  uint start;
  int pid, time;
  
  pid = fork();
  15:	e8 91 03 00 00       	call   3ab <fork>
  1a:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  start = uptime();
  1d:	e8 29 04 00 00       	call   44b <uptime>
  22:	89 45 e0             	mov    %eax,-0x20(%ebp)

  if(pid < 0){
  25:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  29:	79 17                	jns    42 <main+0x42>
    printf(2, "time_fail: Invalid PID.\n");
  2b:	83 ec 08             	sub    $0x8,%esp
  2e:	68 28 09 00 00       	push   $0x928
  33:	6a 02                	push   $0x2
  35:	e8 38 05 00 00       	call   572 <printf>
  3a:	83 c4 10             	add    $0x10,%esp
    exit();
  3d:	e8 71 03 00 00       	call   3b3 <exit>
  }
  if(pid > 0)	//parent process wait for child to finish and then tell us time to run
  42:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  46:	7e 05                	jle    4d <main+0x4d>
    wait();
  48:	e8 6e 03 00 00       	call   3bb <wait>
  if(pid == 0){ //child process
  4d:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  51:	75 24                	jne    77 <main+0x77>
    if(exec(argv[1], argv + 1)){   
  53:	8b 43 04             	mov    0x4(%ebx),%eax
  56:	8d 50 04             	lea    0x4(%eax),%edx
  59:	8b 43 04             	mov    0x4(%ebx),%eax
  5c:	83 c0 04             	add    $0x4,%eax
  5f:	8b 00                	mov    (%eax),%eax
  61:	83 ec 08             	sub    $0x8,%esp
  64:	52                   	push   %edx
  65:	50                   	push   %eax
  66:	e8 80 03 00 00       	call   3eb <exec>
  6b:	83 c4 10             	add    $0x10,%esp
  6e:	85 c0                	test   %eax,%eax
  70:	74 05                	je     77 <main+0x77>
      exit();
  72:	e8 3c 03 00 00       	call   3b3 <exit>
    }
  }
  time = uptime() - start;
  77:	e8 cf 03 00 00       	call   44b <uptime>
  7c:	2b 45 e0             	sub    -0x20(%ebp),%eax
  7f:	89 45 dc             	mov    %eax,-0x24(%ebp)
  if(argc == 1)
  82:	83 3b 01             	cmpl   $0x1,(%ebx)
  85:	75 51                	jne    d8 <main+0xd8>
    printf(1, "%s ran in %d.%d seconds.\n", argv[0], time / 100, time % 100);
  87:	8b 75 dc             	mov    -0x24(%ebp),%esi
  8a:	ba 1f 85 eb 51       	mov    $0x51eb851f,%edx
  8f:	89 f0                	mov    %esi,%eax
  91:	f7 ea                	imul   %edx
  93:	c1 fa 05             	sar    $0x5,%edx
  96:	89 f0                	mov    %esi,%eax
  98:	c1 f8 1f             	sar    $0x1f,%eax
  9b:	89 d1                	mov    %edx,%ecx
  9d:	29 c1                	sub    %eax,%ecx
  9f:	6b c1 64             	imul   $0x64,%ecx,%eax
  a2:	89 f1                	mov    %esi,%ecx
  a4:	29 c1                	sub    %eax,%ecx
  a6:	8b 75 dc             	mov    -0x24(%ebp),%esi
  a9:	ba 1f 85 eb 51       	mov    $0x51eb851f,%edx
  ae:	89 f0                	mov    %esi,%eax
  b0:	f7 ea                	imul   %edx
  b2:	c1 fa 05             	sar    $0x5,%edx
  b5:	89 f0                	mov    %esi,%eax
  b7:	c1 f8 1f             	sar    $0x1f,%eax
  ba:	29 c2                	sub    %eax,%edx
  bc:	8b 43 04             	mov    0x4(%ebx),%eax
  bf:	8b 00                	mov    (%eax),%eax
  c1:	83 ec 0c             	sub    $0xc,%esp
  c4:	51                   	push   %ecx
  c5:	52                   	push   %edx
  c6:	50                   	push   %eax
  c7:	68 41 09 00 00       	push   $0x941
  cc:	6a 01                	push   $0x1
  ce:	e8 9f 04 00 00       	call   572 <printf>
  d3:	83 c4 20             	add    $0x20,%esp
  d6:	eb 52                	jmp    12a <main+0x12a>
  else
    printf(1, "%s ran in %d.%d seconds.\n", argv[1], time / 100, time % 100);
  d8:	8b 75 dc             	mov    -0x24(%ebp),%esi
  db:	ba 1f 85 eb 51       	mov    $0x51eb851f,%edx
  e0:	89 f0                	mov    %esi,%eax
  e2:	f7 ea                	imul   %edx
  e4:	c1 fa 05             	sar    $0x5,%edx
  e7:	89 f0                	mov    %esi,%eax
  e9:	c1 f8 1f             	sar    $0x1f,%eax
  ec:	89 d1                	mov    %edx,%ecx
  ee:	29 c1                	sub    %eax,%ecx
  f0:	6b c1 64             	imul   $0x64,%ecx,%eax
  f3:	29 c6                	sub    %eax,%esi
  f5:	89 f1                	mov    %esi,%ecx
  f7:	8b 75 dc             	mov    -0x24(%ebp),%esi
  fa:	ba 1f 85 eb 51       	mov    $0x51eb851f,%edx
  ff:	89 f0                	mov    %esi,%eax
 101:	f7 ea                	imul   %edx
 103:	c1 fa 05             	sar    $0x5,%edx
 106:	89 f0                	mov    %esi,%eax
 108:	c1 f8 1f             	sar    $0x1f,%eax
 10b:	29 c2                	sub    %eax,%edx
 10d:	8b 43 04             	mov    0x4(%ebx),%eax
 110:	83 c0 04             	add    $0x4,%eax
 113:	8b 00                	mov    (%eax),%eax
 115:	83 ec 0c             	sub    $0xc,%esp
 118:	51                   	push   %ecx
 119:	52                   	push   %edx
 11a:	50                   	push   %eax
 11b:	68 41 09 00 00       	push   $0x941
 120:	6a 01                	push   $0x1
 122:	e8 4b 04 00 00       	call   572 <printf>
 127:	83 c4 20             	add    $0x20,%esp
  exit();
 12a:	e8 84 02 00 00       	call   3b3 <exit>

0000012f <stosb>:
               "cc");
}

static inline void
stosb(void *addr, int data, int cnt)
{
 12f:	55                   	push   %ebp
 130:	89 e5                	mov    %esp,%ebp
 132:	57                   	push   %edi
 133:	53                   	push   %ebx
  asm volatile("cld; rep stosb" :
 134:	8b 4d 08             	mov    0x8(%ebp),%ecx
 137:	8b 55 10             	mov    0x10(%ebp),%edx
 13a:	8b 45 0c             	mov    0xc(%ebp),%eax
 13d:	89 cb                	mov    %ecx,%ebx
 13f:	89 df                	mov    %ebx,%edi
 141:	89 d1                	mov    %edx,%ecx
 143:	fc                   	cld    
 144:	f3 aa                	rep stos %al,%es:(%edi)
 146:	89 ca                	mov    %ecx,%edx
 148:	89 fb                	mov    %edi,%ebx
 14a:	89 5d 08             	mov    %ebx,0x8(%ebp)
 14d:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
 150:	90                   	nop
 151:	5b                   	pop    %ebx
 152:	5f                   	pop    %edi
 153:	5d                   	pop    %ebp
 154:	c3                   	ret    

00000155 <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, char *t)
{
 155:	55                   	push   %ebp
 156:	89 e5                	mov    %esp,%ebp
 158:	83 ec 10             	sub    $0x10,%esp
  char *os;

  os = s;
 15b:	8b 45 08             	mov    0x8(%ebp),%eax
 15e:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while((*s++ = *t++) != 0)
 161:	90                   	nop
 162:	8b 45 08             	mov    0x8(%ebp),%eax
 165:	8d 50 01             	lea    0x1(%eax),%edx
 168:	89 55 08             	mov    %edx,0x8(%ebp)
 16b:	8b 55 0c             	mov    0xc(%ebp),%edx
 16e:	8d 4a 01             	lea    0x1(%edx),%ecx
 171:	89 4d 0c             	mov    %ecx,0xc(%ebp)
 174:	0f b6 12             	movzbl (%edx),%edx
 177:	88 10                	mov    %dl,(%eax)
 179:	0f b6 00             	movzbl (%eax),%eax
 17c:	84 c0                	test   %al,%al
 17e:	75 e2                	jne    162 <strcpy+0xd>
    ;
  return os;
 180:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 183:	c9                   	leave  
 184:	c3                   	ret    

00000185 <strcmp>:

int
strcmp(const char *p, const char *q)
{
 185:	55                   	push   %ebp
 186:	89 e5                	mov    %esp,%ebp
  while(*p && *p == *q)
 188:	eb 08                	jmp    192 <strcmp+0xd>
    p++, q++;
 18a:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 18e:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
  while(*p && *p == *q)
 192:	8b 45 08             	mov    0x8(%ebp),%eax
 195:	0f b6 00             	movzbl (%eax),%eax
 198:	84 c0                	test   %al,%al
 19a:	74 10                	je     1ac <strcmp+0x27>
 19c:	8b 45 08             	mov    0x8(%ebp),%eax
 19f:	0f b6 10             	movzbl (%eax),%edx
 1a2:	8b 45 0c             	mov    0xc(%ebp),%eax
 1a5:	0f b6 00             	movzbl (%eax),%eax
 1a8:	38 c2                	cmp    %al,%dl
 1aa:	74 de                	je     18a <strcmp+0x5>
    p++, q++;
  return (uchar)*p - (uchar)*q;
 1ac:	8b 45 08             	mov    0x8(%ebp),%eax
 1af:	0f b6 00             	movzbl (%eax),%eax
 1b2:	0f b6 d0             	movzbl %al,%edx
 1b5:	8b 45 0c             	mov    0xc(%ebp),%eax
 1b8:	0f b6 00             	movzbl (%eax),%eax
 1bb:	0f b6 c0             	movzbl %al,%eax
 1be:	29 c2                	sub    %eax,%edx
 1c0:	89 d0                	mov    %edx,%eax
}
 1c2:	5d                   	pop    %ebp
 1c3:	c3                   	ret    

000001c4 <strlen>:

uint
strlen(char *s)
{
 1c4:	55                   	push   %ebp
 1c5:	89 e5                	mov    %esp,%ebp
 1c7:	83 ec 10             	sub    $0x10,%esp
  int n;

  for(n = 0; s[n]; n++)
 1ca:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
 1d1:	eb 04                	jmp    1d7 <strlen+0x13>
 1d3:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
 1d7:	8b 55 fc             	mov    -0x4(%ebp),%edx
 1da:	8b 45 08             	mov    0x8(%ebp),%eax
 1dd:	01 d0                	add    %edx,%eax
 1df:	0f b6 00             	movzbl (%eax),%eax
 1e2:	84 c0                	test   %al,%al
 1e4:	75 ed                	jne    1d3 <strlen+0xf>
    ;
  return n;
 1e6:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 1e9:	c9                   	leave  
 1ea:	c3                   	ret    

000001eb <memset>:

void*
memset(void *dst, int c, uint n)
{
 1eb:	55                   	push   %ebp
 1ec:	89 e5                	mov    %esp,%ebp
  stosb(dst, c, n);
 1ee:	8b 45 10             	mov    0x10(%ebp),%eax
 1f1:	50                   	push   %eax
 1f2:	ff 75 0c             	pushl  0xc(%ebp)
 1f5:	ff 75 08             	pushl  0x8(%ebp)
 1f8:	e8 32 ff ff ff       	call   12f <stosb>
 1fd:	83 c4 0c             	add    $0xc,%esp
  return dst;
 200:	8b 45 08             	mov    0x8(%ebp),%eax
}
 203:	c9                   	leave  
 204:	c3                   	ret    

00000205 <strchr>:

char*
strchr(const char *s, char c)
{
 205:	55                   	push   %ebp
 206:	89 e5                	mov    %esp,%ebp
 208:	83 ec 04             	sub    $0x4,%esp
 20b:	8b 45 0c             	mov    0xc(%ebp),%eax
 20e:	88 45 fc             	mov    %al,-0x4(%ebp)
  for(; *s; s++)
 211:	eb 14                	jmp    227 <strchr+0x22>
    if(*s == c)
 213:	8b 45 08             	mov    0x8(%ebp),%eax
 216:	0f b6 00             	movzbl (%eax),%eax
 219:	3a 45 fc             	cmp    -0x4(%ebp),%al
 21c:	75 05                	jne    223 <strchr+0x1e>
      return (char*)s;
 21e:	8b 45 08             	mov    0x8(%ebp),%eax
 221:	eb 13                	jmp    236 <strchr+0x31>
}

char*
strchr(const char *s, char c)
{
  for(; *s; s++)
 223:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 227:	8b 45 08             	mov    0x8(%ebp),%eax
 22a:	0f b6 00             	movzbl (%eax),%eax
 22d:	84 c0                	test   %al,%al
 22f:	75 e2                	jne    213 <strchr+0xe>
    if(*s == c)
      return (char*)s;
  return 0;
 231:	b8 00 00 00 00       	mov    $0x0,%eax
}
 236:	c9                   	leave  
 237:	c3                   	ret    

00000238 <gets>:

char*
gets(char *buf, int max)
{
 238:	55                   	push   %ebp
 239:	89 e5                	mov    %esp,%ebp
 23b:	83 ec 18             	sub    $0x18,%esp
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 23e:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 245:	eb 42                	jmp    289 <gets+0x51>
    cc = read(0, &c, 1);
 247:	83 ec 04             	sub    $0x4,%esp
 24a:	6a 01                	push   $0x1
 24c:	8d 45 ef             	lea    -0x11(%ebp),%eax
 24f:	50                   	push   %eax
 250:	6a 00                	push   $0x0
 252:	e8 74 01 00 00       	call   3cb <read>
 257:	83 c4 10             	add    $0x10,%esp
 25a:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if(cc < 1)
 25d:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 261:	7e 33                	jle    296 <gets+0x5e>
      break;
    buf[i++] = c;
 263:	8b 45 f4             	mov    -0xc(%ebp),%eax
 266:	8d 50 01             	lea    0x1(%eax),%edx
 269:	89 55 f4             	mov    %edx,-0xc(%ebp)
 26c:	89 c2                	mov    %eax,%edx
 26e:	8b 45 08             	mov    0x8(%ebp),%eax
 271:	01 c2                	add    %eax,%edx
 273:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 277:	88 02                	mov    %al,(%edx)
    if(c == '\n' || c == '\r')
 279:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 27d:	3c 0a                	cmp    $0xa,%al
 27f:	74 16                	je     297 <gets+0x5f>
 281:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 285:	3c 0d                	cmp    $0xd,%al
 287:	74 0e                	je     297 <gets+0x5f>
gets(char *buf, int max)
{
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 289:	8b 45 f4             	mov    -0xc(%ebp),%eax
 28c:	83 c0 01             	add    $0x1,%eax
 28f:	3b 45 0c             	cmp    0xc(%ebp),%eax
 292:	7c b3                	jl     247 <gets+0xf>
 294:	eb 01                	jmp    297 <gets+0x5f>
    cc = read(0, &c, 1);
    if(cc < 1)
      break;
 296:	90                   	nop
    buf[i++] = c;
    if(c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 297:	8b 55 f4             	mov    -0xc(%ebp),%edx
 29a:	8b 45 08             	mov    0x8(%ebp),%eax
 29d:	01 d0                	add    %edx,%eax
 29f:	c6 00 00             	movb   $0x0,(%eax)
  return buf;
 2a2:	8b 45 08             	mov    0x8(%ebp),%eax
}
 2a5:	c9                   	leave  
 2a6:	c3                   	ret    

000002a7 <stat>:

int
stat(char *n, struct stat *st)
{
 2a7:	55                   	push   %ebp
 2a8:	89 e5                	mov    %esp,%ebp
 2aa:	83 ec 18             	sub    $0x18,%esp
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 2ad:	83 ec 08             	sub    $0x8,%esp
 2b0:	6a 00                	push   $0x0
 2b2:	ff 75 08             	pushl  0x8(%ebp)
 2b5:	e8 39 01 00 00       	call   3f3 <open>
 2ba:	83 c4 10             	add    $0x10,%esp
 2bd:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(fd < 0)
 2c0:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 2c4:	79 07                	jns    2cd <stat+0x26>
    return -1;
 2c6:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 2cb:	eb 25                	jmp    2f2 <stat+0x4b>
  r = fstat(fd, st);
 2cd:	83 ec 08             	sub    $0x8,%esp
 2d0:	ff 75 0c             	pushl  0xc(%ebp)
 2d3:	ff 75 f4             	pushl  -0xc(%ebp)
 2d6:	e8 30 01 00 00       	call   40b <fstat>
 2db:	83 c4 10             	add    $0x10,%esp
 2de:	89 45 f0             	mov    %eax,-0x10(%ebp)
  close(fd);
 2e1:	83 ec 0c             	sub    $0xc,%esp
 2e4:	ff 75 f4             	pushl  -0xc(%ebp)
 2e7:	e8 ef 00 00 00       	call   3db <close>
 2ec:	83 c4 10             	add    $0x10,%esp
  return r;
 2ef:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
 2f2:	c9                   	leave  
 2f3:	c3                   	ret    

000002f4 <atoi>:

int
atoi(const char *s)
{
 2f4:	55                   	push   %ebp
 2f5:	89 e5                	mov    %esp,%ebp
 2f7:	83 ec 10             	sub    $0x10,%esp
  int n, sign;

  n = 0;
 2fa:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  while(*s == ' ') s++;
 301:	eb 04                	jmp    307 <atoi+0x13>
 303:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 307:	8b 45 08             	mov    0x8(%ebp),%eax
 30a:	0f b6 00             	movzbl (%eax),%eax
 30d:	3c 20                	cmp    $0x20,%al
 30f:	74 f2                	je     303 <atoi+0xf>
  sign = (*s == '-') ? -1 : 1;
 311:	8b 45 08             	mov    0x8(%ebp),%eax
 314:	0f b6 00             	movzbl (%eax),%eax
 317:	3c 2d                	cmp    $0x2d,%al
 319:	75 07                	jne    322 <atoi+0x2e>
 31b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 320:	eb 05                	jmp    327 <atoi+0x33>
 322:	b8 01 00 00 00       	mov    $0x1,%eax
 327:	89 45 f8             	mov    %eax,-0x8(%ebp)
  while('0' <= *s && *s <= '9')
 32a:	eb 25                	jmp    351 <atoi+0x5d>
    n = n*10 + *s++ - '0';
 32c:	8b 55 fc             	mov    -0x4(%ebp),%edx
 32f:	89 d0                	mov    %edx,%eax
 331:	c1 e0 02             	shl    $0x2,%eax
 334:	01 d0                	add    %edx,%eax
 336:	01 c0                	add    %eax,%eax
 338:	89 c1                	mov    %eax,%ecx
 33a:	8b 45 08             	mov    0x8(%ebp),%eax
 33d:	8d 50 01             	lea    0x1(%eax),%edx
 340:	89 55 08             	mov    %edx,0x8(%ebp)
 343:	0f b6 00             	movzbl (%eax),%eax
 346:	0f be c0             	movsbl %al,%eax
 349:	01 c8                	add    %ecx,%eax
 34b:	83 e8 30             	sub    $0x30,%eax
 34e:	89 45 fc             	mov    %eax,-0x4(%ebp)
  int n, sign;

  n = 0;
  while(*s == ' ') s++;
  sign = (*s == '-') ? -1 : 1;
  while('0' <= *s && *s <= '9')
 351:	8b 45 08             	mov    0x8(%ebp),%eax
 354:	0f b6 00             	movzbl (%eax),%eax
 357:	3c 2f                	cmp    $0x2f,%al
 359:	7e 0a                	jle    365 <atoi+0x71>
 35b:	8b 45 08             	mov    0x8(%ebp),%eax
 35e:	0f b6 00             	movzbl (%eax),%eax
 361:	3c 39                	cmp    $0x39,%al
 363:	7e c7                	jle    32c <atoi+0x38>
    n = n*10 + *s++ - '0';
  return sign*n;
 365:	8b 45 f8             	mov    -0x8(%ebp),%eax
 368:	0f af 45 fc          	imul   -0x4(%ebp),%eax
}
 36c:	c9                   	leave  
 36d:	c3                   	ret    

0000036e <memmove>:

void*
memmove(void *vdst, void *vsrc, int n)
{
 36e:	55                   	push   %ebp
 36f:	89 e5                	mov    %esp,%ebp
 371:	83 ec 10             	sub    $0x10,%esp
  char *dst, *src;
  
  dst = vdst;
 374:	8b 45 08             	mov    0x8(%ebp),%eax
 377:	89 45 fc             	mov    %eax,-0x4(%ebp)
  src = vsrc;
 37a:	8b 45 0c             	mov    0xc(%ebp),%eax
 37d:	89 45 f8             	mov    %eax,-0x8(%ebp)
  while(n-- > 0)
 380:	eb 17                	jmp    399 <memmove+0x2b>
    *dst++ = *src++;
 382:	8b 45 fc             	mov    -0x4(%ebp),%eax
 385:	8d 50 01             	lea    0x1(%eax),%edx
 388:	89 55 fc             	mov    %edx,-0x4(%ebp)
 38b:	8b 55 f8             	mov    -0x8(%ebp),%edx
 38e:	8d 4a 01             	lea    0x1(%edx),%ecx
 391:	89 4d f8             	mov    %ecx,-0x8(%ebp)
 394:	0f b6 12             	movzbl (%edx),%edx
 397:	88 10                	mov    %dl,(%eax)
{
  char *dst, *src;
  
  dst = vdst;
  src = vsrc;
  while(n-- > 0)
 399:	8b 45 10             	mov    0x10(%ebp),%eax
 39c:	8d 50 ff             	lea    -0x1(%eax),%edx
 39f:	89 55 10             	mov    %edx,0x10(%ebp)
 3a2:	85 c0                	test   %eax,%eax
 3a4:	7f dc                	jg     382 <memmove+0x14>
    *dst++ = *src++;
  return vdst;
 3a6:	8b 45 08             	mov    0x8(%ebp),%eax
}
 3a9:	c9                   	leave  
 3aa:	c3                   	ret    

000003ab <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
 3ab:	b8 01 00 00 00       	mov    $0x1,%eax
 3b0:	cd 40                	int    $0x40
 3b2:	c3                   	ret    

000003b3 <exit>:
SYSCALL(exit)
 3b3:	b8 02 00 00 00       	mov    $0x2,%eax
 3b8:	cd 40                	int    $0x40
 3ba:	c3                   	ret    

000003bb <wait>:
SYSCALL(wait)
 3bb:	b8 03 00 00 00       	mov    $0x3,%eax
 3c0:	cd 40                	int    $0x40
 3c2:	c3                   	ret    

000003c3 <pipe>:
SYSCALL(pipe)
 3c3:	b8 04 00 00 00       	mov    $0x4,%eax
 3c8:	cd 40                	int    $0x40
 3ca:	c3                   	ret    

000003cb <read>:
SYSCALL(read)
 3cb:	b8 05 00 00 00       	mov    $0x5,%eax
 3d0:	cd 40                	int    $0x40
 3d2:	c3                   	ret    

000003d3 <write>:
SYSCALL(write)
 3d3:	b8 10 00 00 00       	mov    $0x10,%eax
 3d8:	cd 40                	int    $0x40
 3da:	c3                   	ret    

000003db <close>:
SYSCALL(close)
 3db:	b8 15 00 00 00       	mov    $0x15,%eax
 3e0:	cd 40                	int    $0x40
 3e2:	c3                   	ret    

000003e3 <kill>:
SYSCALL(kill)
 3e3:	b8 06 00 00 00       	mov    $0x6,%eax
 3e8:	cd 40                	int    $0x40
 3ea:	c3                   	ret    

000003eb <exec>:
SYSCALL(exec)
 3eb:	b8 07 00 00 00       	mov    $0x7,%eax
 3f0:	cd 40                	int    $0x40
 3f2:	c3                   	ret    

000003f3 <open>:
SYSCALL(open)
 3f3:	b8 0f 00 00 00       	mov    $0xf,%eax
 3f8:	cd 40                	int    $0x40
 3fa:	c3                   	ret    

000003fb <mknod>:
SYSCALL(mknod)
 3fb:	b8 11 00 00 00       	mov    $0x11,%eax
 400:	cd 40                	int    $0x40
 402:	c3                   	ret    

00000403 <unlink>:
SYSCALL(unlink)
 403:	b8 12 00 00 00       	mov    $0x12,%eax
 408:	cd 40                	int    $0x40
 40a:	c3                   	ret    

0000040b <fstat>:
SYSCALL(fstat)
 40b:	b8 08 00 00 00       	mov    $0x8,%eax
 410:	cd 40                	int    $0x40
 412:	c3                   	ret    

00000413 <link>:
SYSCALL(link)
 413:	b8 13 00 00 00       	mov    $0x13,%eax
 418:	cd 40                	int    $0x40
 41a:	c3                   	ret    

0000041b <mkdir>:
SYSCALL(mkdir)
 41b:	b8 14 00 00 00       	mov    $0x14,%eax
 420:	cd 40                	int    $0x40
 422:	c3                   	ret    

00000423 <chdir>:
SYSCALL(chdir)
 423:	b8 09 00 00 00       	mov    $0x9,%eax
 428:	cd 40                	int    $0x40
 42a:	c3                   	ret    

0000042b <dup>:
SYSCALL(dup)
 42b:	b8 0a 00 00 00       	mov    $0xa,%eax
 430:	cd 40                	int    $0x40
 432:	c3                   	ret    

00000433 <getpid>:
SYSCALL(getpid)
 433:	b8 0b 00 00 00       	mov    $0xb,%eax
 438:	cd 40                	int    $0x40
 43a:	c3                   	ret    

0000043b <sbrk>:
SYSCALL(sbrk)
 43b:	b8 0c 00 00 00       	mov    $0xc,%eax
 440:	cd 40                	int    $0x40
 442:	c3                   	ret    

00000443 <sleep>:
SYSCALL(sleep)
 443:	b8 0d 00 00 00       	mov    $0xd,%eax
 448:	cd 40                	int    $0x40
 44a:	c3                   	ret    

0000044b <uptime>:
SYSCALL(uptime)
 44b:	b8 0e 00 00 00       	mov    $0xe,%eax
 450:	cd 40                	int    $0x40
 452:	c3                   	ret    

00000453 <halt>:
SYSCALL(halt)
 453:	b8 16 00 00 00       	mov    $0x16,%eax
 458:	cd 40                	int    $0x40
 45a:	c3                   	ret    

0000045b <date>:
SYSCALL(date)    #added the date system call
 45b:	b8 17 00 00 00       	mov    $0x17,%eax
 460:	cd 40                	int    $0x40
 462:	c3                   	ret    

00000463 <getuid>:
SYSCALL(getuid)
 463:	b8 18 00 00 00       	mov    $0x18,%eax
 468:	cd 40                	int    $0x40
 46a:	c3                   	ret    

0000046b <getgid>:
SYSCALL(getgid)
 46b:	b8 19 00 00 00       	mov    $0x19,%eax
 470:	cd 40                	int    $0x40
 472:	c3                   	ret    

00000473 <getppid>:
SYSCALL(getppid)
 473:	b8 1a 00 00 00       	mov    $0x1a,%eax
 478:	cd 40                	int    $0x40
 47a:	c3                   	ret    

0000047b <setuid>:
SYSCALL(setuid)
 47b:	b8 1b 00 00 00       	mov    $0x1b,%eax
 480:	cd 40                	int    $0x40
 482:	c3                   	ret    

00000483 <setgid>:
SYSCALL(setgid)
 483:	b8 1c 00 00 00       	mov    $0x1c,%eax
 488:	cd 40                	int    $0x40
 48a:	c3                   	ret    

0000048b <getprocs>:
SYSCALL(getprocs)
 48b:	b8 1d 00 00 00       	mov    $0x1d,%eax
 490:	cd 40                	int    $0x40
 492:	c3                   	ret    

00000493 <setpriority>:
SYSCALL(setpriority)
 493:	b8 1e 00 00 00       	mov    $0x1e,%eax
 498:	cd 40                	int    $0x40
 49a:	c3                   	ret    

0000049b <putc>:
#include "stat.h"
#include "user.h"

static void
putc(int fd, char c)
{
 49b:	55                   	push   %ebp
 49c:	89 e5                	mov    %esp,%ebp
 49e:	83 ec 18             	sub    $0x18,%esp
 4a1:	8b 45 0c             	mov    0xc(%ebp),%eax
 4a4:	88 45 f4             	mov    %al,-0xc(%ebp)
  write(fd, &c, 1);
 4a7:	83 ec 04             	sub    $0x4,%esp
 4aa:	6a 01                	push   $0x1
 4ac:	8d 45 f4             	lea    -0xc(%ebp),%eax
 4af:	50                   	push   %eax
 4b0:	ff 75 08             	pushl  0x8(%ebp)
 4b3:	e8 1b ff ff ff       	call   3d3 <write>
 4b8:	83 c4 10             	add    $0x10,%esp
}
 4bb:	90                   	nop
 4bc:	c9                   	leave  
 4bd:	c3                   	ret    

000004be <printint>:

static void
printint(int fd, int xx, int base, int sgn)
{
 4be:	55                   	push   %ebp
 4bf:	89 e5                	mov    %esp,%ebp
 4c1:	53                   	push   %ebx
 4c2:	83 ec 24             	sub    $0x24,%esp
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 4c5:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  if(sgn && xx < 0){
 4cc:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
 4d0:	74 17                	je     4e9 <printint+0x2b>
 4d2:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
 4d6:	79 11                	jns    4e9 <printint+0x2b>
    neg = 1;
 4d8:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
    x = -xx;
 4df:	8b 45 0c             	mov    0xc(%ebp),%eax
 4e2:	f7 d8                	neg    %eax
 4e4:	89 45 ec             	mov    %eax,-0x14(%ebp)
 4e7:	eb 06                	jmp    4ef <printint+0x31>
  } else {
    x = xx;
 4e9:	8b 45 0c             	mov    0xc(%ebp),%eax
 4ec:	89 45 ec             	mov    %eax,-0x14(%ebp)
  }

  i = 0;
 4ef:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  do{
    buf[i++] = digits[x % base];
 4f6:	8b 4d f4             	mov    -0xc(%ebp),%ecx
 4f9:	8d 41 01             	lea    0x1(%ecx),%eax
 4fc:	89 45 f4             	mov    %eax,-0xc(%ebp)
 4ff:	8b 5d 10             	mov    0x10(%ebp),%ebx
 502:	8b 45 ec             	mov    -0x14(%ebp),%eax
 505:	ba 00 00 00 00       	mov    $0x0,%edx
 50a:	f7 f3                	div    %ebx
 50c:	89 d0                	mov    %edx,%eax
 50e:	0f b6 80 b4 0b 00 00 	movzbl 0xbb4(%eax),%eax
 515:	88 44 0d dc          	mov    %al,-0x24(%ebp,%ecx,1)
  }while((x /= base) != 0);
 519:	8b 5d 10             	mov    0x10(%ebp),%ebx
 51c:	8b 45 ec             	mov    -0x14(%ebp),%eax
 51f:	ba 00 00 00 00       	mov    $0x0,%edx
 524:	f7 f3                	div    %ebx
 526:	89 45 ec             	mov    %eax,-0x14(%ebp)
 529:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 52d:	75 c7                	jne    4f6 <printint+0x38>
  if(neg)
 52f:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 533:	74 2d                	je     562 <printint+0xa4>
    buf[i++] = '-';
 535:	8b 45 f4             	mov    -0xc(%ebp),%eax
 538:	8d 50 01             	lea    0x1(%eax),%edx
 53b:	89 55 f4             	mov    %edx,-0xc(%ebp)
 53e:	c6 44 05 dc 2d       	movb   $0x2d,-0x24(%ebp,%eax,1)

  while(--i >= 0)
 543:	eb 1d                	jmp    562 <printint+0xa4>
    putc(fd, buf[i]);
 545:	8d 55 dc             	lea    -0x24(%ebp),%edx
 548:	8b 45 f4             	mov    -0xc(%ebp),%eax
 54b:	01 d0                	add    %edx,%eax
 54d:	0f b6 00             	movzbl (%eax),%eax
 550:	0f be c0             	movsbl %al,%eax
 553:	83 ec 08             	sub    $0x8,%esp
 556:	50                   	push   %eax
 557:	ff 75 08             	pushl  0x8(%ebp)
 55a:	e8 3c ff ff ff       	call   49b <putc>
 55f:	83 c4 10             	add    $0x10,%esp
    buf[i++] = digits[x % base];
  }while((x /= base) != 0);
  if(neg)
    buf[i++] = '-';

  while(--i >= 0)
 562:	83 6d f4 01          	subl   $0x1,-0xc(%ebp)
 566:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 56a:	79 d9                	jns    545 <printint+0x87>
    putc(fd, buf[i]);
}
 56c:	90                   	nop
 56d:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 570:	c9                   	leave  
 571:	c3                   	ret    

00000572 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, char *fmt, ...)
{
 572:	55                   	push   %ebp
 573:	89 e5                	mov    %esp,%ebp
 575:	83 ec 28             	sub    $0x28,%esp
  char *s;
  int c, i, state;
  uint *ap;

  state = 0;
 578:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  ap = (uint*)(void*)&fmt + 1;
 57f:	8d 45 0c             	lea    0xc(%ebp),%eax
 582:	83 c0 04             	add    $0x4,%eax
 585:	89 45 e8             	mov    %eax,-0x18(%ebp)
  for(i = 0; fmt[i]; i++){
 588:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 58f:	e9 59 01 00 00       	jmp    6ed <printf+0x17b>
    c = fmt[i] & 0xff;
 594:	8b 55 0c             	mov    0xc(%ebp),%edx
 597:	8b 45 f0             	mov    -0x10(%ebp),%eax
 59a:	01 d0                	add    %edx,%eax
 59c:	0f b6 00             	movzbl (%eax),%eax
 59f:	0f be c0             	movsbl %al,%eax
 5a2:	25 ff 00 00 00       	and    $0xff,%eax
 5a7:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    if(state == 0){
 5aa:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 5ae:	75 2c                	jne    5dc <printf+0x6a>
      if(c == '%'){
 5b0:	83 7d e4 25          	cmpl   $0x25,-0x1c(%ebp)
 5b4:	75 0c                	jne    5c2 <printf+0x50>
        state = '%';
 5b6:	c7 45 ec 25 00 00 00 	movl   $0x25,-0x14(%ebp)
 5bd:	e9 27 01 00 00       	jmp    6e9 <printf+0x177>
      } else {
        putc(fd, c);
 5c2:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 5c5:	0f be c0             	movsbl %al,%eax
 5c8:	83 ec 08             	sub    $0x8,%esp
 5cb:	50                   	push   %eax
 5cc:	ff 75 08             	pushl  0x8(%ebp)
 5cf:	e8 c7 fe ff ff       	call   49b <putc>
 5d4:	83 c4 10             	add    $0x10,%esp
 5d7:	e9 0d 01 00 00       	jmp    6e9 <printf+0x177>
      }
    } else if(state == '%'){
 5dc:	83 7d ec 25          	cmpl   $0x25,-0x14(%ebp)
 5e0:	0f 85 03 01 00 00    	jne    6e9 <printf+0x177>
      if(c == 'd'){
 5e6:	83 7d e4 64          	cmpl   $0x64,-0x1c(%ebp)
 5ea:	75 1e                	jne    60a <printf+0x98>
        printint(fd, *ap, 10, 1);
 5ec:	8b 45 e8             	mov    -0x18(%ebp),%eax
 5ef:	8b 00                	mov    (%eax),%eax
 5f1:	6a 01                	push   $0x1
 5f3:	6a 0a                	push   $0xa
 5f5:	50                   	push   %eax
 5f6:	ff 75 08             	pushl  0x8(%ebp)
 5f9:	e8 c0 fe ff ff       	call   4be <printint>
 5fe:	83 c4 10             	add    $0x10,%esp
        ap++;
 601:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 605:	e9 d8 00 00 00       	jmp    6e2 <printf+0x170>
      } else if(c == 'x' || c == 'p'){
 60a:	83 7d e4 78          	cmpl   $0x78,-0x1c(%ebp)
 60e:	74 06                	je     616 <printf+0xa4>
 610:	83 7d e4 70          	cmpl   $0x70,-0x1c(%ebp)
 614:	75 1e                	jne    634 <printf+0xc2>
        printint(fd, *ap, 16, 0);
 616:	8b 45 e8             	mov    -0x18(%ebp),%eax
 619:	8b 00                	mov    (%eax),%eax
 61b:	6a 00                	push   $0x0
 61d:	6a 10                	push   $0x10
 61f:	50                   	push   %eax
 620:	ff 75 08             	pushl  0x8(%ebp)
 623:	e8 96 fe ff ff       	call   4be <printint>
 628:	83 c4 10             	add    $0x10,%esp
        ap++;
 62b:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 62f:	e9 ae 00 00 00       	jmp    6e2 <printf+0x170>
      } else if(c == 's'){
 634:	83 7d e4 73          	cmpl   $0x73,-0x1c(%ebp)
 638:	75 43                	jne    67d <printf+0x10b>
        s = (char*)*ap;
 63a:	8b 45 e8             	mov    -0x18(%ebp),%eax
 63d:	8b 00                	mov    (%eax),%eax
 63f:	89 45 f4             	mov    %eax,-0xc(%ebp)
        ap++;
 642:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
        if(s == 0)
 646:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 64a:	75 25                	jne    671 <printf+0xff>
          s = "(null)";
 64c:	c7 45 f4 5b 09 00 00 	movl   $0x95b,-0xc(%ebp)
        while(*s != 0){
 653:	eb 1c                	jmp    671 <printf+0xff>
          putc(fd, *s);
 655:	8b 45 f4             	mov    -0xc(%ebp),%eax
 658:	0f b6 00             	movzbl (%eax),%eax
 65b:	0f be c0             	movsbl %al,%eax
 65e:	83 ec 08             	sub    $0x8,%esp
 661:	50                   	push   %eax
 662:	ff 75 08             	pushl  0x8(%ebp)
 665:	e8 31 fe ff ff       	call   49b <putc>
 66a:	83 c4 10             	add    $0x10,%esp
          s++;
 66d:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
      } else if(c == 's'){
        s = (char*)*ap;
        ap++;
        if(s == 0)
          s = "(null)";
        while(*s != 0){
 671:	8b 45 f4             	mov    -0xc(%ebp),%eax
 674:	0f b6 00             	movzbl (%eax),%eax
 677:	84 c0                	test   %al,%al
 679:	75 da                	jne    655 <printf+0xe3>
 67b:	eb 65                	jmp    6e2 <printf+0x170>
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 67d:	83 7d e4 63          	cmpl   $0x63,-0x1c(%ebp)
 681:	75 1d                	jne    6a0 <printf+0x12e>
        putc(fd, *ap);
 683:	8b 45 e8             	mov    -0x18(%ebp),%eax
 686:	8b 00                	mov    (%eax),%eax
 688:	0f be c0             	movsbl %al,%eax
 68b:	83 ec 08             	sub    $0x8,%esp
 68e:	50                   	push   %eax
 68f:	ff 75 08             	pushl  0x8(%ebp)
 692:	e8 04 fe ff ff       	call   49b <putc>
 697:	83 c4 10             	add    $0x10,%esp
        ap++;
 69a:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 69e:	eb 42                	jmp    6e2 <printf+0x170>
      } else if(c == '%'){
 6a0:	83 7d e4 25          	cmpl   $0x25,-0x1c(%ebp)
 6a4:	75 17                	jne    6bd <printf+0x14b>
        putc(fd, c);
 6a6:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 6a9:	0f be c0             	movsbl %al,%eax
 6ac:	83 ec 08             	sub    $0x8,%esp
 6af:	50                   	push   %eax
 6b0:	ff 75 08             	pushl  0x8(%ebp)
 6b3:	e8 e3 fd ff ff       	call   49b <putc>
 6b8:	83 c4 10             	add    $0x10,%esp
 6bb:	eb 25                	jmp    6e2 <printf+0x170>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 6bd:	83 ec 08             	sub    $0x8,%esp
 6c0:	6a 25                	push   $0x25
 6c2:	ff 75 08             	pushl  0x8(%ebp)
 6c5:	e8 d1 fd ff ff       	call   49b <putc>
 6ca:	83 c4 10             	add    $0x10,%esp
        putc(fd, c);
 6cd:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 6d0:	0f be c0             	movsbl %al,%eax
 6d3:	83 ec 08             	sub    $0x8,%esp
 6d6:	50                   	push   %eax
 6d7:	ff 75 08             	pushl  0x8(%ebp)
 6da:	e8 bc fd ff ff       	call   49b <putc>
 6df:	83 c4 10             	add    $0x10,%esp
      }
      state = 0;
 6e2:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
 6e9:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
 6ed:	8b 55 0c             	mov    0xc(%ebp),%edx
 6f0:	8b 45 f0             	mov    -0x10(%ebp),%eax
 6f3:	01 d0                	add    %edx,%eax
 6f5:	0f b6 00             	movzbl (%eax),%eax
 6f8:	84 c0                	test   %al,%al
 6fa:	0f 85 94 fe ff ff    	jne    594 <printf+0x22>
        putc(fd, c);
      }
      state = 0;
    }
  }
}
 700:	90                   	nop
 701:	c9                   	leave  
 702:	c3                   	ret    

00000703 <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 703:	55                   	push   %ebp
 704:	89 e5                	mov    %esp,%ebp
 706:	83 ec 10             	sub    $0x10,%esp
  Header *bp, *p;

  bp = (Header*)ap - 1;
 709:	8b 45 08             	mov    0x8(%ebp),%eax
 70c:	83 e8 08             	sub    $0x8,%eax
 70f:	89 45 f8             	mov    %eax,-0x8(%ebp)
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 712:	a1 d0 0b 00 00       	mov    0xbd0,%eax
 717:	89 45 fc             	mov    %eax,-0x4(%ebp)
 71a:	eb 24                	jmp    740 <free+0x3d>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 71c:	8b 45 fc             	mov    -0x4(%ebp),%eax
 71f:	8b 00                	mov    (%eax),%eax
 721:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 724:	77 12                	ja     738 <free+0x35>
 726:	8b 45 f8             	mov    -0x8(%ebp),%eax
 729:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 72c:	77 24                	ja     752 <free+0x4f>
 72e:	8b 45 fc             	mov    -0x4(%ebp),%eax
 731:	8b 00                	mov    (%eax),%eax
 733:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 736:	77 1a                	ja     752 <free+0x4f>
free(void *ap)
{
  Header *bp, *p;

  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 738:	8b 45 fc             	mov    -0x4(%ebp),%eax
 73b:	8b 00                	mov    (%eax),%eax
 73d:	89 45 fc             	mov    %eax,-0x4(%ebp)
 740:	8b 45 f8             	mov    -0x8(%ebp),%eax
 743:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 746:	76 d4                	jbe    71c <free+0x19>
 748:	8b 45 fc             	mov    -0x4(%ebp),%eax
 74b:	8b 00                	mov    (%eax),%eax
 74d:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 750:	76 ca                	jbe    71c <free+0x19>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if(bp + bp->s.size == p->s.ptr){
 752:	8b 45 f8             	mov    -0x8(%ebp),%eax
 755:	8b 40 04             	mov    0x4(%eax),%eax
 758:	8d 14 c5 00 00 00 00 	lea    0x0(,%eax,8),%edx
 75f:	8b 45 f8             	mov    -0x8(%ebp),%eax
 762:	01 c2                	add    %eax,%edx
 764:	8b 45 fc             	mov    -0x4(%ebp),%eax
 767:	8b 00                	mov    (%eax),%eax
 769:	39 c2                	cmp    %eax,%edx
 76b:	75 24                	jne    791 <free+0x8e>
    bp->s.size += p->s.ptr->s.size;
 76d:	8b 45 f8             	mov    -0x8(%ebp),%eax
 770:	8b 50 04             	mov    0x4(%eax),%edx
 773:	8b 45 fc             	mov    -0x4(%ebp),%eax
 776:	8b 00                	mov    (%eax),%eax
 778:	8b 40 04             	mov    0x4(%eax),%eax
 77b:	01 c2                	add    %eax,%edx
 77d:	8b 45 f8             	mov    -0x8(%ebp),%eax
 780:	89 50 04             	mov    %edx,0x4(%eax)
    bp->s.ptr = p->s.ptr->s.ptr;
 783:	8b 45 fc             	mov    -0x4(%ebp),%eax
 786:	8b 00                	mov    (%eax),%eax
 788:	8b 10                	mov    (%eax),%edx
 78a:	8b 45 f8             	mov    -0x8(%ebp),%eax
 78d:	89 10                	mov    %edx,(%eax)
 78f:	eb 0a                	jmp    79b <free+0x98>
  } else
    bp->s.ptr = p->s.ptr;
 791:	8b 45 fc             	mov    -0x4(%ebp),%eax
 794:	8b 10                	mov    (%eax),%edx
 796:	8b 45 f8             	mov    -0x8(%ebp),%eax
 799:	89 10                	mov    %edx,(%eax)
  if(p + p->s.size == bp){
 79b:	8b 45 fc             	mov    -0x4(%ebp),%eax
 79e:	8b 40 04             	mov    0x4(%eax),%eax
 7a1:	8d 14 c5 00 00 00 00 	lea    0x0(,%eax,8),%edx
 7a8:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7ab:	01 d0                	add    %edx,%eax
 7ad:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 7b0:	75 20                	jne    7d2 <free+0xcf>
    p->s.size += bp->s.size;
 7b2:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7b5:	8b 50 04             	mov    0x4(%eax),%edx
 7b8:	8b 45 f8             	mov    -0x8(%ebp),%eax
 7bb:	8b 40 04             	mov    0x4(%eax),%eax
 7be:	01 c2                	add    %eax,%edx
 7c0:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7c3:	89 50 04             	mov    %edx,0x4(%eax)
    p->s.ptr = bp->s.ptr;
 7c6:	8b 45 f8             	mov    -0x8(%ebp),%eax
 7c9:	8b 10                	mov    (%eax),%edx
 7cb:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7ce:	89 10                	mov    %edx,(%eax)
 7d0:	eb 08                	jmp    7da <free+0xd7>
  } else
    p->s.ptr = bp;
 7d2:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7d5:	8b 55 f8             	mov    -0x8(%ebp),%edx
 7d8:	89 10                	mov    %edx,(%eax)
  freep = p;
 7da:	8b 45 fc             	mov    -0x4(%ebp),%eax
 7dd:	a3 d0 0b 00 00       	mov    %eax,0xbd0
}
 7e2:	90                   	nop
 7e3:	c9                   	leave  
 7e4:	c3                   	ret    

000007e5 <morecore>:

static Header*
morecore(uint nu)
{
 7e5:	55                   	push   %ebp
 7e6:	89 e5                	mov    %esp,%ebp
 7e8:	83 ec 18             	sub    $0x18,%esp
  char *p;
  Header *hp;

  if(nu < 4096)
 7eb:	81 7d 08 ff 0f 00 00 	cmpl   $0xfff,0x8(%ebp)
 7f2:	77 07                	ja     7fb <morecore+0x16>
    nu = 4096;
 7f4:	c7 45 08 00 10 00 00 	movl   $0x1000,0x8(%ebp)
  p = sbrk(nu * sizeof(Header));
 7fb:	8b 45 08             	mov    0x8(%ebp),%eax
 7fe:	c1 e0 03             	shl    $0x3,%eax
 801:	83 ec 0c             	sub    $0xc,%esp
 804:	50                   	push   %eax
 805:	e8 31 fc ff ff       	call   43b <sbrk>
 80a:	83 c4 10             	add    $0x10,%esp
 80d:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(p == (char*)-1)
 810:	83 7d f4 ff          	cmpl   $0xffffffff,-0xc(%ebp)
 814:	75 07                	jne    81d <morecore+0x38>
    return 0;
 816:	b8 00 00 00 00       	mov    $0x0,%eax
 81b:	eb 26                	jmp    843 <morecore+0x5e>
  hp = (Header*)p;
 81d:	8b 45 f4             	mov    -0xc(%ebp),%eax
 820:	89 45 f0             	mov    %eax,-0x10(%ebp)
  hp->s.size = nu;
 823:	8b 45 f0             	mov    -0x10(%ebp),%eax
 826:	8b 55 08             	mov    0x8(%ebp),%edx
 829:	89 50 04             	mov    %edx,0x4(%eax)
  free((void*)(hp + 1));
 82c:	8b 45 f0             	mov    -0x10(%ebp),%eax
 82f:	83 c0 08             	add    $0x8,%eax
 832:	83 ec 0c             	sub    $0xc,%esp
 835:	50                   	push   %eax
 836:	e8 c8 fe ff ff       	call   703 <free>
 83b:	83 c4 10             	add    $0x10,%esp
  return freep;
 83e:	a1 d0 0b 00 00       	mov    0xbd0,%eax
}
 843:	c9                   	leave  
 844:	c3                   	ret    

00000845 <malloc>:

void*
malloc(uint nbytes)
{
 845:	55                   	push   %ebp
 846:	89 e5                	mov    %esp,%ebp
 848:	83 ec 18             	sub    $0x18,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 84b:	8b 45 08             	mov    0x8(%ebp),%eax
 84e:	83 c0 07             	add    $0x7,%eax
 851:	c1 e8 03             	shr    $0x3,%eax
 854:	83 c0 01             	add    $0x1,%eax
 857:	89 45 ec             	mov    %eax,-0x14(%ebp)
  if((prevp = freep) == 0){
 85a:	a1 d0 0b 00 00       	mov    0xbd0,%eax
 85f:	89 45 f0             	mov    %eax,-0x10(%ebp)
 862:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 866:	75 23                	jne    88b <malloc+0x46>
    base.s.ptr = freep = prevp = &base;
 868:	c7 45 f0 c8 0b 00 00 	movl   $0xbc8,-0x10(%ebp)
 86f:	8b 45 f0             	mov    -0x10(%ebp),%eax
 872:	a3 d0 0b 00 00       	mov    %eax,0xbd0
 877:	a1 d0 0b 00 00       	mov    0xbd0,%eax
 87c:	a3 c8 0b 00 00       	mov    %eax,0xbc8
    base.s.size = 0;
 881:	c7 05 cc 0b 00 00 00 	movl   $0x0,0xbcc
 888:	00 00 00 
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 88b:	8b 45 f0             	mov    -0x10(%ebp),%eax
 88e:	8b 00                	mov    (%eax),%eax
 890:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if(p->s.size >= nunits){
 893:	8b 45 f4             	mov    -0xc(%ebp),%eax
 896:	8b 40 04             	mov    0x4(%eax),%eax
 899:	3b 45 ec             	cmp    -0x14(%ebp),%eax
 89c:	72 4d                	jb     8eb <malloc+0xa6>
      if(p->s.size == nunits)
 89e:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8a1:	8b 40 04             	mov    0x4(%eax),%eax
 8a4:	3b 45 ec             	cmp    -0x14(%ebp),%eax
 8a7:	75 0c                	jne    8b5 <malloc+0x70>
        prevp->s.ptr = p->s.ptr;
 8a9:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8ac:	8b 10                	mov    (%eax),%edx
 8ae:	8b 45 f0             	mov    -0x10(%ebp),%eax
 8b1:	89 10                	mov    %edx,(%eax)
 8b3:	eb 26                	jmp    8db <malloc+0x96>
      else {
        p->s.size -= nunits;
 8b5:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8b8:	8b 40 04             	mov    0x4(%eax),%eax
 8bb:	2b 45 ec             	sub    -0x14(%ebp),%eax
 8be:	89 c2                	mov    %eax,%edx
 8c0:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8c3:	89 50 04             	mov    %edx,0x4(%eax)
        p += p->s.size;
 8c6:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8c9:	8b 40 04             	mov    0x4(%eax),%eax
 8cc:	c1 e0 03             	shl    $0x3,%eax
 8cf:	01 45 f4             	add    %eax,-0xc(%ebp)
        p->s.size = nunits;
 8d2:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8d5:	8b 55 ec             	mov    -0x14(%ebp),%edx
 8d8:	89 50 04             	mov    %edx,0x4(%eax)
      }
      freep = prevp;
 8db:	8b 45 f0             	mov    -0x10(%ebp),%eax
 8de:	a3 d0 0b 00 00       	mov    %eax,0xbd0
      return (void*)(p + 1);
 8e3:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8e6:	83 c0 08             	add    $0x8,%eax
 8e9:	eb 3b                	jmp    926 <malloc+0xe1>
    }
    if(p == freep)
 8eb:	a1 d0 0b 00 00       	mov    0xbd0,%eax
 8f0:	39 45 f4             	cmp    %eax,-0xc(%ebp)
 8f3:	75 1e                	jne    913 <malloc+0xce>
      if((p = morecore(nunits)) == 0)
 8f5:	83 ec 0c             	sub    $0xc,%esp
 8f8:	ff 75 ec             	pushl  -0x14(%ebp)
 8fb:	e8 e5 fe ff ff       	call   7e5 <morecore>
 900:	83 c4 10             	add    $0x10,%esp
 903:	89 45 f4             	mov    %eax,-0xc(%ebp)
 906:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 90a:	75 07                	jne    913 <malloc+0xce>
        return 0;
 90c:	b8 00 00 00 00       	mov    $0x0,%eax
 911:	eb 13                	jmp    926 <malloc+0xe1>
  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
  if((prevp = freep) == 0){
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 913:	8b 45 f4             	mov    -0xc(%ebp),%eax
 916:	89 45 f0             	mov    %eax,-0x10(%ebp)
 919:	8b 45 f4             	mov    -0xc(%ebp),%eax
 91c:	8b 00                	mov    (%eax),%eax
 91e:	89 45 f4             	mov    %eax,-0xc(%ebp)
      return (void*)(p + 1);
    }
    if(p == freep)
      if((p = morecore(nunits)) == 0)
        return 0;
  }
 921:	e9 6d ff ff ff       	jmp    893 <malloc+0x4e>
}
 926:	c9                   	leave  
 927:	c3                   	ret    
