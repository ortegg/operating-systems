
_kill:     file format elf32-i386


Disassembly of section .text:

00000000 <main>:
#include "stat.h"
#include "user.h"

int
main(int argc, char **argv)
{
   0:	8d 4c 24 04          	lea    0x4(%esp),%ecx
   4:	83 e4 f0             	and    $0xfffffff0,%esp
   7:	ff 71 fc             	pushl  -0x4(%ecx)
   a:	55                   	push   %ebp
   b:	89 e5                	mov    %esp,%ebp
   d:	53                   	push   %ebx
   e:	51                   	push   %ecx
   f:	83 ec 10             	sub    $0x10,%esp
  12:	89 cb                	mov    %ecx,%ebx
  int i;

  if(argc < 2){
  14:	83 3b 01             	cmpl   $0x1,(%ebx)
  17:	7f 17                	jg     30 <main+0x30>
    printf(2, "usage: kill pid...\n");
  19:	83 ec 08             	sub    $0x8,%esp
  1c:	68 6b 08 00 00       	push   $0x86b
  21:	6a 02                	push   $0x2
  23:	e8 8d 04 00 00       	call   4b5 <printf>
  28:	83 c4 10             	add    $0x10,%esp
    exit();
  2b:	e8 c6 02 00 00       	call   2f6 <exit>
  }
  for(i=1; i<argc; i++)
  30:	c7 45 f4 01 00 00 00 	movl   $0x1,-0xc(%ebp)
  37:	eb 2d                	jmp    66 <main+0x66>
    kill(atoi(argv[i]));
  39:	8b 45 f4             	mov    -0xc(%ebp),%eax
  3c:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  43:	8b 43 04             	mov    0x4(%ebx),%eax
  46:	01 d0                	add    %edx,%eax
  48:	8b 00                	mov    (%eax),%eax
  4a:	83 ec 0c             	sub    $0xc,%esp
  4d:	50                   	push   %eax
  4e:	e8 e4 01 00 00       	call   237 <atoi>
  53:	83 c4 10             	add    $0x10,%esp
  56:	83 ec 0c             	sub    $0xc,%esp
  59:	50                   	push   %eax
  5a:	e8 c7 02 00 00       	call   326 <kill>
  5f:	83 c4 10             	add    $0x10,%esp

  if(argc < 2){
    printf(2, "usage: kill pid...\n");
    exit();
  }
  for(i=1; i<argc; i++)
  62:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
  66:	8b 45 f4             	mov    -0xc(%ebp),%eax
  69:	3b 03                	cmp    (%ebx),%eax
  6b:	7c cc                	jl     39 <main+0x39>
    kill(atoi(argv[i]));
  exit();
  6d:	e8 84 02 00 00       	call   2f6 <exit>

00000072 <stosb>:
               "cc");
}

static inline void
stosb(void *addr, int data, int cnt)
{
  72:	55                   	push   %ebp
  73:	89 e5                	mov    %esp,%ebp
  75:	57                   	push   %edi
  76:	53                   	push   %ebx
  asm volatile("cld; rep stosb" :
  77:	8b 4d 08             	mov    0x8(%ebp),%ecx
  7a:	8b 55 10             	mov    0x10(%ebp),%edx
  7d:	8b 45 0c             	mov    0xc(%ebp),%eax
  80:	89 cb                	mov    %ecx,%ebx
  82:	89 df                	mov    %ebx,%edi
  84:	89 d1                	mov    %edx,%ecx
  86:	fc                   	cld    
  87:	f3 aa                	rep stos %al,%es:(%edi)
  89:	89 ca                	mov    %ecx,%edx
  8b:	89 fb                	mov    %edi,%ebx
  8d:	89 5d 08             	mov    %ebx,0x8(%ebp)
  90:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
  93:	90                   	nop
  94:	5b                   	pop    %ebx
  95:	5f                   	pop    %edi
  96:	5d                   	pop    %ebp
  97:	c3                   	ret    

00000098 <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, char *t)
{
  98:	55                   	push   %ebp
  99:	89 e5                	mov    %esp,%ebp
  9b:	83 ec 10             	sub    $0x10,%esp
  char *os;

  os = s;
  9e:	8b 45 08             	mov    0x8(%ebp),%eax
  a1:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while((*s++ = *t++) != 0)
  a4:	90                   	nop
  a5:	8b 45 08             	mov    0x8(%ebp),%eax
  a8:	8d 50 01             	lea    0x1(%eax),%edx
  ab:	89 55 08             	mov    %edx,0x8(%ebp)
  ae:	8b 55 0c             	mov    0xc(%ebp),%edx
  b1:	8d 4a 01             	lea    0x1(%edx),%ecx
  b4:	89 4d 0c             	mov    %ecx,0xc(%ebp)
  b7:	0f b6 12             	movzbl (%edx),%edx
  ba:	88 10                	mov    %dl,(%eax)
  bc:	0f b6 00             	movzbl (%eax),%eax
  bf:	84 c0                	test   %al,%al
  c1:	75 e2                	jne    a5 <strcpy+0xd>
    ;
  return os;
  c3:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  c6:	c9                   	leave  
  c7:	c3                   	ret    

000000c8 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  c8:	55                   	push   %ebp
  c9:	89 e5                	mov    %esp,%ebp
  while(*p && *p == *q)
  cb:	eb 08                	jmp    d5 <strcmp+0xd>
    p++, q++;
  cd:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  d1:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
  while(*p && *p == *q)
  d5:	8b 45 08             	mov    0x8(%ebp),%eax
  d8:	0f b6 00             	movzbl (%eax),%eax
  db:	84 c0                	test   %al,%al
  dd:	74 10                	je     ef <strcmp+0x27>
  df:	8b 45 08             	mov    0x8(%ebp),%eax
  e2:	0f b6 10             	movzbl (%eax),%edx
  e5:	8b 45 0c             	mov    0xc(%ebp),%eax
  e8:	0f b6 00             	movzbl (%eax),%eax
  eb:	38 c2                	cmp    %al,%dl
  ed:	74 de                	je     cd <strcmp+0x5>
    p++, q++;
  return (uchar)*p - (uchar)*q;
  ef:	8b 45 08             	mov    0x8(%ebp),%eax
  f2:	0f b6 00             	movzbl (%eax),%eax
  f5:	0f b6 d0             	movzbl %al,%edx
  f8:	8b 45 0c             	mov    0xc(%ebp),%eax
  fb:	0f b6 00             	movzbl (%eax),%eax
  fe:	0f b6 c0             	movzbl %al,%eax
 101:	29 c2                	sub    %eax,%edx
 103:	89 d0                	mov    %edx,%eax
}
 105:	5d                   	pop    %ebp
 106:	c3                   	ret    

00000107 <strlen>:

uint
strlen(char *s)
{
 107:	55                   	push   %ebp
 108:	89 e5                	mov    %esp,%ebp
 10a:	83 ec 10             	sub    $0x10,%esp
  int n;

  for(n = 0; s[n]; n++)
 10d:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
 114:	eb 04                	jmp    11a <strlen+0x13>
 116:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
 11a:	8b 55 fc             	mov    -0x4(%ebp),%edx
 11d:	8b 45 08             	mov    0x8(%ebp),%eax
 120:	01 d0                	add    %edx,%eax
 122:	0f b6 00             	movzbl (%eax),%eax
 125:	84 c0                	test   %al,%al
 127:	75 ed                	jne    116 <strlen+0xf>
    ;
  return n;
 129:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 12c:	c9                   	leave  
 12d:	c3                   	ret    

0000012e <memset>:

void*
memset(void *dst, int c, uint n)
{
 12e:	55                   	push   %ebp
 12f:	89 e5                	mov    %esp,%ebp
  stosb(dst, c, n);
 131:	8b 45 10             	mov    0x10(%ebp),%eax
 134:	50                   	push   %eax
 135:	ff 75 0c             	pushl  0xc(%ebp)
 138:	ff 75 08             	pushl  0x8(%ebp)
 13b:	e8 32 ff ff ff       	call   72 <stosb>
 140:	83 c4 0c             	add    $0xc,%esp
  return dst;
 143:	8b 45 08             	mov    0x8(%ebp),%eax
}
 146:	c9                   	leave  
 147:	c3                   	ret    

00000148 <strchr>:

char*
strchr(const char *s, char c)
{
 148:	55                   	push   %ebp
 149:	89 e5                	mov    %esp,%ebp
 14b:	83 ec 04             	sub    $0x4,%esp
 14e:	8b 45 0c             	mov    0xc(%ebp),%eax
 151:	88 45 fc             	mov    %al,-0x4(%ebp)
  for(; *s; s++)
 154:	eb 14                	jmp    16a <strchr+0x22>
    if(*s == c)
 156:	8b 45 08             	mov    0x8(%ebp),%eax
 159:	0f b6 00             	movzbl (%eax),%eax
 15c:	3a 45 fc             	cmp    -0x4(%ebp),%al
 15f:	75 05                	jne    166 <strchr+0x1e>
      return (char*)s;
 161:	8b 45 08             	mov    0x8(%ebp),%eax
 164:	eb 13                	jmp    179 <strchr+0x31>
}

char*
strchr(const char *s, char c)
{
  for(; *s; s++)
 166:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 16a:	8b 45 08             	mov    0x8(%ebp),%eax
 16d:	0f b6 00             	movzbl (%eax),%eax
 170:	84 c0                	test   %al,%al
 172:	75 e2                	jne    156 <strchr+0xe>
    if(*s == c)
      return (char*)s;
  return 0;
 174:	b8 00 00 00 00       	mov    $0x0,%eax
}
 179:	c9                   	leave  
 17a:	c3                   	ret    

0000017b <gets>:

char*
gets(char *buf, int max)
{
 17b:	55                   	push   %ebp
 17c:	89 e5                	mov    %esp,%ebp
 17e:	83 ec 18             	sub    $0x18,%esp
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 181:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 188:	eb 42                	jmp    1cc <gets+0x51>
    cc = read(0, &c, 1);
 18a:	83 ec 04             	sub    $0x4,%esp
 18d:	6a 01                	push   $0x1
 18f:	8d 45 ef             	lea    -0x11(%ebp),%eax
 192:	50                   	push   %eax
 193:	6a 00                	push   $0x0
 195:	e8 74 01 00 00       	call   30e <read>
 19a:	83 c4 10             	add    $0x10,%esp
 19d:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if(cc < 1)
 1a0:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 1a4:	7e 33                	jle    1d9 <gets+0x5e>
      break;
    buf[i++] = c;
 1a6:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1a9:	8d 50 01             	lea    0x1(%eax),%edx
 1ac:	89 55 f4             	mov    %edx,-0xc(%ebp)
 1af:	89 c2                	mov    %eax,%edx
 1b1:	8b 45 08             	mov    0x8(%ebp),%eax
 1b4:	01 c2                	add    %eax,%edx
 1b6:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 1ba:	88 02                	mov    %al,(%edx)
    if(c == '\n' || c == '\r')
 1bc:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 1c0:	3c 0a                	cmp    $0xa,%al
 1c2:	74 16                	je     1da <gets+0x5f>
 1c4:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 1c8:	3c 0d                	cmp    $0xd,%al
 1ca:	74 0e                	je     1da <gets+0x5f>
gets(char *buf, int max)
{
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 1cc:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1cf:	83 c0 01             	add    $0x1,%eax
 1d2:	3b 45 0c             	cmp    0xc(%ebp),%eax
 1d5:	7c b3                	jl     18a <gets+0xf>
 1d7:	eb 01                	jmp    1da <gets+0x5f>
    cc = read(0, &c, 1);
    if(cc < 1)
      break;
 1d9:	90                   	nop
    buf[i++] = c;
    if(c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 1da:	8b 55 f4             	mov    -0xc(%ebp),%edx
 1dd:	8b 45 08             	mov    0x8(%ebp),%eax
 1e0:	01 d0                	add    %edx,%eax
 1e2:	c6 00 00             	movb   $0x0,(%eax)
  return buf;
 1e5:	8b 45 08             	mov    0x8(%ebp),%eax
}
 1e8:	c9                   	leave  
 1e9:	c3                   	ret    

000001ea <stat>:

int
stat(char *n, struct stat *st)
{
 1ea:	55                   	push   %ebp
 1eb:	89 e5                	mov    %esp,%ebp
 1ed:	83 ec 18             	sub    $0x18,%esp
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 1f0:	83 ec 08             	sub    $0x8,%esp
 1f3:	6a 00                	push   $0x0
 1f5:	ff 75 08             	pushl  0x8(%ebp)
 1f8:	e8 39 01 00 00       	call   336 <open>
 1fd:	83 c4 10             	add    $0x10,%esp
 200:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(fd < 0)
 203:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 207:	79 07                	jns    210 <stat+0x26>
    return -1;
 209:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 20e:	eb 25                	jmp    235 <stat+0x4b>
  r = fstat(fd, st);
 210:	83 ec 08             	sub    $0x8,%esp
 213:	ff 75 0c             	pushl  0xc(%ebp)
 216:	ff 75 f4             	pushl  -0xc(%ebp)
 219:	e8 30 01 00 00       	call   34e <fstat>
 21e:	83 c4 10             	add    $0x10,%esp
 221:	89 45 f0             	mov    %eax,-0x10(%ebp)
  close(fd);
 224:	83 ec 0c             	sub    $0xc,%esp
 227:	ff 75 f4             	pushl  -0xc(%ebp)
 22a:	e8 ef 00 00 00       	call   31e <close>
 22f:	83 c4 10             	add    $0x10,%esp
  return r;
 232:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
 235:	c9                   	leave  
 236:	c3                   	ret    

00000237 <atoi>:

int
atoi(const char *s)
{
 237:	55                   	push   %ebp
 238:	89 e5                	mov    %esp,%ebp
 23a:	83 ec 10             	sub    $0x10,%esp
  int n, sign;

  n = 0;
 23d:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  while(*s == ' ') s++;
 244:	eb 04                	jmp    24a <atoi+0x13>
 246:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 24a:	8b 45 08             	mov    0x8(%ebp),%eax
 24d:	0f b6 00             	movzbl (%eax),%eax
 250:	3c 20                	cmp    $0x20,%al
 252:	74 f2                	je     246 <atoi+0xf>
  sign = (*s == '-') ? -1 : 1;
 254:	8b 45 08             	mov    0x8(%ebp),%eax
 257:	0f b6 00             	movzbl (%eax),%eax
 25a:	3c 2d                	cmp    $0x2d,%al
 25c:	75 07                	jne    265 <atoi+0x2e>
 25e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 263:	eb 05                	jmp    26a <atoi+0x33>
 265:	b8 01 00 00 00       	mov    $0x1,%eax
 26a:	89 45 f8             	mov    %eax,-0x8(%ebp)
  while('0' <= *s && *s <= '9')
 26d:	eb 25                	jmp    294 <atoi+0x5d>
    n = n*10 + *s++ - '0';
 26f:	8b 55 fc             	mov    -0x4(%ebp),%edx
 272:	89 d0                	mov    %edx,%eax
 274:	c1 e0 02             	shl    $0x2,%eax
 277:	01 d0                	add    %edx,%eax
 279:	01 c0                	add    %eax,%eax
 27b:	89 c1                	mov    %eax,%ecx
 27d:	8b 45 08             	mov    0x8(%ebp),%eax
 280:	8d 50 01             	lea    0x1(%eax),%edx
 283:	89 55 08             	mov    %edx,0x8(%ebp)
 286:	0f b6 00             	movzbl (%eax),%eax
 289:	0f be c0             	movsbl %al,%eax
 28c:	01 c8                	add    %ecx,%eax
 28e:	83 e8 30             	sub    $0x30,%eax
 291:	89 45 fc             	mov    %eax,-0x4(%ebp)
  int n, sign;

  n = 0;
  while(*s == ' ') s++;
  sign = (*s == '-') ? -1 : 1;
  while('0' <= *s && *s <= '9')
 294:	8b 45 08             	mov    0x8(%ebp),%eax
 297:	0f b6 00             	movzbl (%eax),%eax
 29a:	3c 2f                	cmp    $0x2f,%al
 29c:	7e 0a                	jle    2a8 <atoi+0x71>
 29e:	8b 45 08             	mov    0x8(%ebp),%eax
 2a1:	0f b6 00             	movzbl (%eax),%eax
 2a4:	3c 39                	cmp    $0x39,%al
 2a6:	7e c7                	jle    26f <atoi+0x38>
    n = n*10 + *s++ - '0';
  return sign*n;
 2a8:	8b 45 f8             	mov    -0x8(%ebp),%eax
 2ab:	0f af 45 fc          	imul   -0x4(%ebp),%eax
}
 2af:	c9                   	leave  
 2b0:	c3                   	ret    

000002b1 <memmove>:

void*
memmove(void *vdst, void *vsrc, int n)
{
 2b1:	55                   	push   %ebp
 2b2:	89 e5                	mov    %esp,%ebp
 2b4:	83 ec 10             	sub    $0x10,%esp
  char *dst, *src;
  
  dst = vdst;
 2b7:	8b 45 08             	mov    0x8(%ebp),%eax
 2ba:	89 45 fc             	mov    %eax,-0x4(%ebp)
  src = vsrc;
 2bd:	8b 45 0c             	mov    0xc(%ebp),%eax
 2c0:	89 45 f8             	mov    %eax,-0x8(%ebp)
  while(n-- > 0)
 2c3:	eb 17                	jmp    2dc <memmove+0x2b>
    *dst++ = *src++;
 2c5:	8b 45 fc             	mov    -0x4(%ebp),%eax
 2c8:	8d 50 01             	lea    0x1(%eax),%edx
 2cb:	89 55 fc             	mov    %edx,-0x4(%ebp)
 2ce:	8b 55 f8             	mov    -0x8(%ebp),%edx
 2d1:	8d 4a 01             	lea    0x1(%edx),%ecx
 2d4:	89 4d f8             	mov    %ecx,-0x8(%ebp)
 2d7:	0f b6 12             	movzbl (%edx),%edx
 2da:	88 10                	mov    %dl,(%eax)
{
  char *dst, *src;
  
  dst = vdst;
  src = vsrc;
  while(n-- > 0)
 2dc:	8b 45 10             	mov    0x10(%ebp),%eax
 2df:	8d 50 ff             	lea    -0x1(%eax),%edx
 2e2:	89 55 10             	mov    %edx,0x10(%ebp)
 2e5:	85 c0                	test   %eax,%eax
 2e7:	7f dc                	jg     2c5 <memmove+0x14>
    *dst++ = *src++;
  return vdst;
 2e9:	8b 45 08             	mov    0x8(%ebp),%eax
}
 2ec:	c9                   	leave  
 2ed:	c3                   	ret    

000002ee <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
 2ee:	b8 01 00 00 00       	mov    $0x1,%eax
 2f3:	cd 40                	int    $0x40
 2f5:	c3                   	ret    

000002f6 <exit>:
SYSCALL(exit)
 2f6:	b8 02 00 00 00       	mov    $0x2,%eax
 2fb:	cd 40                	int    $0x40
 2fd:	c3                   	ret    

000002fe <wait>:
SYSCALL(wait)
 2fe:	b8 03 00 00 00       	mov    $0x3,%eax
 303:	cd 40                	int    $0x40
 305:	c3                   	ret    

00000306 <pipe>:
SYSCALL(pipe)
 306:	b8 04 00 00 00       	mov    $0x4,%eax
 30b:	cd 40                	int    $0x40
 30d:	c3                   	ret    

0000030e <read>:
SYSCALL(read)
 30e:	b8 05 00 00 00       	mov    $0x5,%eax
 313:	cd 40                	int    $0x40
 315:	c3                   	ret    

00000316 <write>:
SYSCALL(write)
 316:	b8 10 00 00 00       	mov    $0x10,%eax
 31b:	cd 40                	int    $0x40
 31d:	c3                   	ret    

0000031e <close>:
SYSCALL(close)
 31e:	b8 15 00 00 00       	mov    $0x15,%eax
 323:	cd 40                	int    $0x40
 325:	c3                   	ret    

00000326 <kill>:
SYSCALL(kill)
 326:	b8 06 00 00 00       	mov    $0x6,%eax
 32b:	cd 40                	int    $0x40
 32d:	c3                   	ret    

0000032e <exec>:
SYSCALL(exec)
 32e:	b8 07 00 00 00       	mov    $0x7,%eax
 333:	cd 40                	int    $0x40
 335:	c3                   	ret    

00000336 <open>:
SYSCALL(open)
 336:	b8 0f 00 00 00       	mov    $0xf,%eax
 33b:	cd 40                	int    $0x40
 33d:	c3                   	ret    

0000033e <mknod>:
SYSCALL(mknod)
 33e:	b8 11 00 00 00       	mov    $0x11,%eax
 343:	cd 40                	int    $0x40
 345:	c3                   	ret    

00000346 <unlink>:
SYSCALL(unlink)
 346:	b8 12 00 00 00       	mov    $0x12,%eax
 34b:	cd 40                	int    $0x40
 34d:	c3                   	ret    

0000034e <fstat>:
SYSCALL(fstat)
 34e:	b8 08 00 00 00       	mov    $0x8,%eax
 353:	cd 40                	int    $0x40
 355:	c3                   	ret    

00000356 <link>:
SYSCALL(link)
 356:	b8 13 00 00 00       	mov    $0x13,%eax
 35b:	cd 40                	int    $0x40
 35d:	c3                   	ret    

0000035e <mkdir>:
SYSCALL(mkdir)
 35e:	b8 14 00 00 00       	mov    $0x14,%eax
 363:	cd 40                	int    $0x40
 365:	c3                   	ret    

00000366 <chdir>:
SYSCALL(chdir)
 366:	b8 09 00 00 00       	mov    $0x9,%eax
 36b:	cd 40                	int    $0x40
 36d:	c3                   	ret    

0000036e <dup>:
SYSCALL(dup)
 36e:	b8 0a 00 00 00       	mov    $0xa,%eax
 373:	cd 40                	int    $0x40
 375:	c3                   	ret    

00000376 <getpid>:
SYSCALL(getpid)
 376:	b8 0b 00 00 00       	mov    $0xb,%eax
 37b:	cd 40                	int    $0x40
 37d:	c3                   	ret    

0000037e <sbrk>:
SYSCALL(sbrk)
 37e:	b8 0c 00 00 00       	mov    $0xc,%eax
 383:	cd 40                	int    $0x40
 385:	c3                   	ret    

00000386 <sleep>:
SYSCALL(sleep)
 386:	b8 0d 00 00 00       	mov    $0xd,%eax
 38b:	cd 40                	int    $0x40
 38d:	c3                   	ret    

0000038e <uptime>:
SYSCALL(uptime)
 38e:	b8 0e 00 00 00       	mov    $0xe,%eax
 393:	cd 40                	int    $0x40
 395:	c3                   	ret    

00000396 <halt>:
SYSCALL(halt)
 396:	b8 16 00 00 00       	mov    $0x16,%eax
 39b:	cd 40                	int    $0x40
 39d:	c3                   	ret    

0000039e <date>:
SYSCALL(date)    #added the date system call
 39e:	b8 17 00 00 00       	mov    $0x17,%eax
 3a3:	cd 40                	int    $0x40
 3a5:	c3                   	ret    

000003a6 <getuid>:
SYSCALL(getuid)
 3a6:	b8 18 00 00 00       	mov    $0x18,%eax
 3ab:	cd 40                	int    $0x40
 3ad:	c3                   	ret    

000003ae <getgid>:
SYSCALL(getgid)
 3ae:	b8 19 00 00 00       	mov    $0x19,%eax
 3b3:	cd 40                	int    $0x40
 3b5:	c3                   	ret    

000003b6 <getppid>:
SYSCALL(getppid)
 3b6:	b8 1a 00 00 00       	mov    $0x1a,%eax
 3bb:	cd 40                	int    $0x40
 3bd:	c3                   	ret    

000003be <setuid>:
SYSCALL(setuid)
 3be:	b8 1b 00 00 00       	mov    $0x1b,%eax
 3c3:	cd 40                	int    $0x40
 3c5:	c3                   	ret    

000003c6 <setgid>:
SYSCALL(setgid)
 3c6:	b8 1c 00 00 00       	mov    $0x1c,%eax
 3cb:	cd 40                	int    $0x40
 3cd:	c3                   	ret    

000003ce <getprocs>:
SYSCALL(getprocs)
 3ce:	b8 1d 00 00 00       	mov    $0x1d,%eax
 3d3:	cd 40                	int    $0x40
 3d5:	c3                   	ret    

000003d6 <setpriority>:
SYSCALL(setpriority)
 3d6:	b8 1e 00 00 00       	mov    $0x1e,%eax
 3db:	cd 40                	int    $0x40
 3dd:	c3                   	ret    

000003de <putc>:
#include "stat.h"
#include "user.h"

static void
putc(int fd, char c)
{
 3de:	55                   	push   %ebp
 3df:	89 e5                	mov    %esp,%ebp
 3e1:	83 ec 18             	sub    $0x18,%esp
 3e4:	8b 45 0c             	mov    0xc(%ebp),%eax
 3e7:	88 45 f4             	mov    %al,-0xc(%ebp)
  write(fd, &c, 1);
 3ea:	83 ec 04             	sub    $0x4,%esp
 3ed:	6a 01                	push   $0x1
 3ef:	8d 45 f4             	lea    -0xc(%ebp),%eax
 3f2:	50                   	push   %eax
 3f3:	ff 75 08             	pushl  0x8(%ebp)
 3f6:	e8 1b ff ff ff       	call   316 <write>
 3fb:	83 c4 10             	add    $0x10,%esp
}
 3fe:	90                   	nop
 3ff:	c9                   	leave  
 400:	c3                   	ret    

00000401 <printint>:

static void
printint(int fd, int xx, int base, int sgn)
{
 401:	55                   	push   %ebp
 402:	89 e5                	mov    %esp,%ebp
 404:	53                   	push   %ebx
 405:	83 ec 24             	sub    $0x24,%esp
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 408:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  if(sgn && xx < 0){
 40f:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
 413:	74 17                	je     42c <printint+0x2b>
 415:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
 419:	79 11                	jns    42c <printint+0x2b>
    neg = 1;
 41b:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
    x = -xx;
 422:	8b 45 0c             	mov    0xc(%ebp),%eax
 425:	f7 d8                	neg    %eax
 427:	89 45 ec             	mov    %eax,-0x14(%ebp)
 42a:	eb 06                	jmp    432 <printint+0x31>
  } else {
    x = xx;
 42c:	8b 45 0c             	mov    0xc(%ebp),%eax
 42f:	89 45 ec             	mov    %eax,-0x14(%ebp)
  }

  i = 0;
 432:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  do{
    buf[i++] = digits[x % base];
 439:	8b 4d f4             	mov    -0xc(%ebp),%ecx
 43c:	8d 41 01             	lea    0x1(%ecx),%eax
 43f:	89 45 f4             	mov    %eax,-0xc(%ebp)
 442:	8b 5d 10             	mov    0x10(%ebp),%ebx
 445:	8b 45 ec             	mov    -0x14(%ebp),%eax
 448:	ba 00 00 00 00       	mov    $0x0,%edx
 44d:	f7 f3                	div    %ebx
 44f:	89 d0                	mov    %edx,%eax
 451:	0f b6 80 d4 0a 00 00 	movzbl 0xad4(%eax),%eax
 458:	88 44 0d dc          	mov    %al,-0x24(%ebp,%ecx,1)
  }while((x /= base) != 0);
 45c:	8b 5d 10             	mov    0x10(%ebp),%ebx
 45f:	8b 45 ec             	mov    -0x14(%ebp),%eax
 462:	ba 00 00 00 00       	mov    $0x0,%edx
 467:	f7 f3                	div    %ebx
 469:	89 45 ec             	mov    %eax,-0x14(%ebp)
 46c:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 470:	75 c7                	jne    439 <printint+0x38>
  if(neg)
 472:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 476:	74 2d                	je     4a5 <printint+0xa4>
    buf[i++] = '-';
 478:	8b 45 f4             	mov    -0xc(%ebp),%eax
 47b:	8d 50 01             	lea    0x1(%eax),%edx
 47e:	89 55 f4             	mov    %edx,-0xc(%ebp)
 481:	c6 44 05 dc 2d       	movb   $0x2d,-0x24(%ebp,%eax,1)

  while(--i >= 0)
 486:	eb 1d                	jmp    4a5 <printint+0xa4>
    putc(fd, buf[i]);
 488:	8d 55 dc             	lea    -0x24(%ebp),%edx
 48b:	8b 45 f4             	mov    -0xc(%ebp),%eax
 48e:	01 d0                	add    %edx,%eax
 490:	0f b6 00             	movzbl (%eax),%eax
 493:	0f be c0             	movsbl %al,%eax
 496:	83 ec 08             	sub    $0x8,%esp
 499:	50                   	push   %eax
 49a:	ff 75 08             	pushl  0x8(%ebp)
 49d:	e8 3c ff ff ff       	call   3de <putc>
 4a2:	83 c4 10             	add    $0x10,%esp
    buf[i++] = digits[x % base];
  }while((x /= base) != 0);
  if(neg)
    buf[i++] = '-';

  while(--i >= 0)
 4a5:	83 6d f4 01          	subl   $0x1,-0xc(%ebp)
 4a9:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 4ad:	79 d9                	jns    488 <printint+0x87>
    putc(fd, buf[i]);
}
 4af:	90                   	nop
 4b0:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 4b3:	c9                   	leave  
 4b4:	c3                   	ret    

000004b5 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, char *fmt, ...)
{
 4b5:	55                   	push   %ebp
 4b6:	89 e5                	mov    %esp,%ebp
 4b8:	83 ec 28             	sub    $0x28,%esp
  char *s;
  int c, i, state;
  uint *ap;

  state = 0;
 4bb:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  ap = (uint*)(void*)&fmt + 1;
 4c2:	8d 45 0c             	lea    0xc(%ebp),%eax
 4c5:	83 c0 04             	add    $0x4,%eax
 4c8:	89 45 e8             	mov    %eax,-0x18(%ebp)
  for(i = 0; fmt[i]; i++){
 4cb:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 4d2:	e9 59 01 00 00       	jmp    630 <printf+0x17b>
    c = fmt[i] & 0xff;
 4d7:	8b 55 0c             	mov    0xc(%ebp),%edx
 4da:	8b 45 f0             	mov    -0x10(%ebp),%eax
 4dd:	01 d0                	add    %edx,%eax
 4df:	0f b6 00             	movzbl (%eax),%eax
 4e2:	0f be c0             	movsbl %al,%eax
 4e5:	25 ff 00 00 00       	and    $0xff,%eax
 4ea:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    if(state == 0){
 4ed:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 4f1:	75 2c                	jne    51f <printf+0x6a>
      if(c == '%'){
 4f3:	83 7d e4 25          	cmpl   $0x25,-0x1c(%ebp)
 4f7:	75 0c                	jne    505 <printf+0x50>
        state = '%';
 4f9:	c7 45 ec 25 00 00 00 	movl   $0x25,-0x14(%ebp)
 500:	e9 27 01 00 00       	jmp    62c <printf+0x177>
      } else {
        putc(fd, c);
 505:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 508:	0f be c0             	movsbl %al,%eax
 50b:	83 ec 08             	sub    $0x8,%esp
 50e:	50                   	push   %eax
 50f:	ff 75 08             	pushl  0x8(%ebp)
 512:	e8 c7 fe ff ff       	call   3de <putc>
 517:	83 c4 10             	add    $0x10,%esp
 51a:	e9 0d 01 00 00       	jmp    62c <printf+0x177>
      }
    } else if(state == '%'){
 51f:	83 7d ec 25          	cmpl   $0x25,-0x14(%ebp)
 523:	0f 85 03 01 00 00    	jne    62c <printf+0x177>
      if(c == 'd'){
 529:	83 7d e4 64          	cmpl   $0x64,-0x1c(%ebp)
 52d:	75 1e                	jne    54d <printf+0x98>
        printint(fd, *ap, 10, 1);
 52f:	8b 45 e8             	mov    -0x18(%ebp),%eax
 532:	8b 00                	mov    (%eax),%eax
 534:	6a 01                	push   $0x1
 536:	6a 0a                	push   $0xa
 538:	50                   	push   %eax
 539:	ff 75 08             	pushl  0x8(%ebp)
 53c:	e8 c0 fe ff ff       	call   401 <printint>
 541:	83 c4 10             	add    $0x10,%esp
        ap++;
 544:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 548:	e9 d8 00 00 00       	jmp    625 <printf+0x170>
      } else if(c == 'x' || c == 'p'){
 54d:	83 7d e4 78          	cmpl   $0x78,-0x1c(%ebp)
 551:	74 06                	je     559 <printf+0xa4>
 553:	83 7d e4 70          	cmpl   $0x70,-0x1c(%ebp)
 557:	75 1e                	jne    577 <printf+0xc2>
        printint(fd, *ap, 16, 0);
 559:	8b 45 e8             	mov    -0x18(%ebp),%eax
 55c:	8b 00                	mov    (%eax),%eax
 55e:	6a 00                	push   $0x0
 560:	6a 10                	push   $0x10
 562:	50                   	push   %eax
 563:	ff 75 08             	pushl  0x8(%ebp)
 566:	e8 96 fe ff ff       	call   401 <printint>
 56b:	83 c4 10             	add    $0x10,%esp
        ap++;
 56e:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 572:	e9 ae 00 00 00       	jmp    625 <printf+0x170>
      } else if(c == 's'){
 577:	83 7d e4 73          	cmpl   $0x73,-0x1c(%ebp)
 57b:	75 43                	jne    5c0 <printf+0x10b>
        s = (char*)*ap;
 57d:	8b 45 e8             	mov    -0x18(%ebp),%eax
 580:	8b 00                	mov    (%eax),%eax
 582:	89 45 f4             	mov    %eax,-0xc(%ebp)
        ap++;
 585:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
        if(s == 0)
 589:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 58d:	75 25                	jne    5b4 <printf+0xff>
          s = "(null)";
 58f:	c7 45 f4 7f 08 00 00 	movl   $0x87f,-0xc(%ebp)
        while(*s != 0){
 596:	eb 1c                	jmp    5b4 <printf+0xff>
          putc(fd, *s);
 598:	8b 45 f4             	mov    -0xc(%ebp),%eax
 59b:	0f b6 00             	movzbl (%eax),%eax
 59e:	0f be c0             	movsbl %al,%eax
 5a1:	83 ec 08             	sub    $0x8,%esp
 5a4:	50                   	push   %eax
 5a5:	ff 75 08             	pushl  0x8(%ebp)
 5a8:	e8 31 fe ff ff       	call   3de <putc>
 5ad:	83 c4 10             	add    $0x10,%esp
          s++;
 5b0:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
      } else if(c == 's'){
        s = (char*)*ap;
        ap++;
        if(s == 0)
          s = "(null)";
        while(*s != 0){
 5b4:	8b 45 f4             	mov    -0xc(%ebp),%eax
 5b7:	0f b6 00             	movzbl (%eax),%eax
 5ba:	84 c0                	test   %al,%al
 5bc:	75 da                	jne    598 <printf+0xe3>
 5be:	eb 65                	jmp    625 <printf+0x170>
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 5c0:	83 7d e4 63          	cmpl   $0x63,-0x1c(%ebp)
 5c4:	75 1d                	jne    5e3 <printf+0x12e>
        putc(fd, *ap);
 5c6:	8b 45 e8             	mov    -0x18(%ebp),%eax
 5c9:	8b 00                	mov    (%eax),%eax
 5cb:	0f be c0             	movsbl %al,%eax
 5ce:	83 ec 08             	sub    $0x8,%esp
 5d1:	50                   	push   %eax
 5d2:	ff 75 08             	pushl  0x8(%ebp)
 5d5:	e8 04 fe ff ff       	call   3de <putc>
 5da:	83 c4 10             	add    $0x10,%esp
        ap++;
 5dd:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 5e1:	eb 42                	jmp    625 <printf+0x170>
      } else if(c == '%'){
 5e3:	83 7d e4 25          	cmpl   $0x25,-0x1c(%ebp)
 5e7:	75 17                	jne    600 <printf+0x14b>
        putc(fd, c);
 5e9:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 5ec:	0f be c0             	movsbl %al,%eax
 5ef:	83 ec 08             	sub    $0x8,%esp
 5f2:	50                   	push   %eax
 5f3:	ff 75 08             	pushl  0x8(%ebp)
 5f6:	e8 e3 fd ff ff       	call   3de <putc>
 5fb:	83 c4 10             	add    $0x10,%esp
 5fe:	eb 25                	jmp    625 <printf+0x170>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 600:	83 ec 08             	sub    $0x8,%esp
 603:	6a 25                	push   $0x25
 605:	ff 75 08             	pushl  0x8(%ebp)
 608:	e8 d1 fd ff ff       	call   3de <putc>
 60d:	83 c4 10             	add    $0x10,%esp
        putc(fd, c);
 610:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 613:	0f be c0             	movsbl %al,%eax
 616:	83 ec 08             	sub    $0x8,%esp
 619:	50                   	push   %eax
 61a:	ff 75 08             	pushl  0x8(%ebp)
 61d:	e8 bc fd ff ff       	call   3de <putc>
 622:	83 c4 10             	add    $0x10,%esp
      }
      state = 0;
 625:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
 62c:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
 630:	8b 55 0c             	mov    0xc(%ebp),%edx
 633:	8b 45 f0             	mov    -0x10(%ebp),%eax
 636:	01 d0                	add    %edx,%eax
 638:	0f b6 00             	movzbl (%eax),%eax
 63b:	84 c0                	test   %al,%al
 63d:	0f 85 94 fe ff ff    	jne    4d7 <printf+0x22>
        putc(fd, c);
      }
      state = 0;
    }
  }
}
 643:	90                   	nop
 644:	c9                   	leave  
 645:	c3                   	ret    

00000646 <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 646:	55                   	push   %ebp
 647:	89 e5                	mov    %esp,%ebp
 649:	83 ec 10             	sub    $0x10,%esp
  Header *bp, *p;

  bp = (Header*)ap - 1;
 64c:	8b 45 08             	mov    0x8(%ebp),%eax
 64f:	83 e8 08             	sub    $0x8,%eax
 652:	89 45 f8             	mov    %eax,-0x8(%ebp)
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 655:	a1 f0 0a 00 00       	mov    0xaf0,%eax
 65a:	89 45 fc             	mov    %eax,-0x4(%ebp)
 65d:	eb 24                	jmp    683 <free+0x3d>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 65f:	8b 45 fc             	mov    -0x4(%ebp),%eax
 662:	8b 00                	mov    (%eax),%eax
 664:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 667:	77 12                	ja     67b <free+0x35>
 669:	8b 45 f8             	mov    -0x8(%ebp),%eax
 66c:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 66f:	77 24                	ja     695 <free+0x4f>
 671:	8b 45 fc             	mov    -0x4(%ebp),%eax
 674:	8b 00                	mov    (%eax),%eax
 676:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 679:	77 1a                	ja     695 <free+0x4f>
free(void *ap)
{
  Header *bp, *p;

  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 67b:	8b 45 fc             	mov    -0x4(%ebp),%eax
 67e:	8b 00                	mov    (%eax),%eax
 680:	89 45 fc             	mov    %eax,-0x4(%ebp)
 683:	8b 45 f8             	mov    -0x8(%ebp),%eax
 686:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 689:	76 d4                	jbe    65f <free+0x19>
 68b:	8b 45 fc             	mov    -0x4(%ebp),%eax
 68e:	8b 00                	mov    (%eax),%eax
 690:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 693:	76 ca                	jbe    65f <free+0x19>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if(bp + bp->s.size == p->s.ptr){
 695:	8b 45 f8             	mov    -0x8(%ebp),%eax
 698:	8b 40 04             	mov    0x4(%eax),%eax
 69b:	8d 14 c5 00 00 00 00 	lea    0x0(,%eax,8),%edx
 6a2:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6a5:	01 c2                	add    %eax,%edx
 6a7:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6aa:	8b 00                	mov    (%eax),%eax
 6ac:	39 c2                	cmp    %eax,%edx
 6ae:	75 24                	jne    6d4 <free+0x8e>
    bp->s.size += p->s.ptr->s.size;
 6b0:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6b3:	8b 50 04             	mov    0x4(%eax),%edx
 6b6:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6b9:	8b 00                	mov    (%eax),%eax
 6bb:	8b 40 04             	mov    0x4(%eax),%eax
 6be:	01 c2                	add    %eax,%edx
 6c0:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6c3:	89 50 04             	mov    %edx,0x4(%eax)
    bp->s.ptr = p->s.ptr->s.ptr;
 6c6:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6c9:	8b 00                	mov    (%eax),%eax
 6cb:	8b 10                	mov    (%eax),%edx
 6cd:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6d0:	89 10                	mov    %edx,(%eax)
 6d2:	eb 0a                	jmp    6de <free+0x98>
  } else
    bp->s.ptr = p->s.ptr;
 6d4:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6d7:	8b 10                	mov    (%eax),%edx
 6d9:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6dc:	89 10                	mov    %edx,(%eax)
  if(p + p->s.size == bp){
 6de:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6e1:	8b 40 04             	mov    0x4(%eax),%eax
 6e4:	8d 14 c5 00 00 00 00 	lea    0x0(,%eax,8),%edx
 6eb:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6ee:	01 d0                	add    %edx,%eax
 6f0:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 6f3:	75 20                	jne    715 <free+0xcf>
    p->s.size += bp->s.size;
 6f5:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6f8:	8b 50 04             	mov    0x4(%eax),%edx
 6fb:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6fe:	8b 40 04             	mov    0x4(%eax),%eax
 701:	01 c2                	add    %eax,%edx
 703:	8b 45 fc             	mov    -0x4(%ebp),%eax
 706:	89 50 04             	mov    %edx,0x4(%eax)
    p->s.ptr = bp->s.ptr;
 709:	8b 45 f8             	mov    -0x8(%ebp),%eax
 70c:	8b 10                	mov    (%eax),%edx
 70e:	8b 45 fc             	mov    -0x4(%ebp),%eax
 711:	89 10                	mov    %edx,(%eax)
 713:	eb 08                	jmp    71d <free+0xd7>
  } else
    p->s.ptr = bp;
 715:	8b 45 fc             	mov    -0x4(%ebp),%eax
 718:	8b 55 f8             	mov    -0x8(%ebp),%edx
 71b:	89 10                	mov    %edx,(%eax)
  freep = p;
 71d:	8b 45 fc             	mov    -0x4(%ebp),%eax
 720:	a3 f0 0a 00 00       	mov    %eax,0xaf0
}
 725:	90                   	nop
 726:	c9                   	leave  
 727:	c3                   	ret    

00000728 <morecore>:

static Header*
morecore(uint nu)
{
 728:	55                   	push   %ebp
 729:	89 e5                	mov    %esp,%ebp
 72b:	83 ec 18             	sub    $0x18,%esp
  char *p;
  Header *hp;

  if(nu < 4096)
 72e:	81 7d 08 ff 0f 00 00 	cmpl   $0xfff,0x8(%ebp)
 735:	77 07                	ja     73e <morecore+0x16>
    nu = 4096;
 737:	c7 45 08 00 10 00 00 	movl   $0x1000,0x8(%ebp)
  p = sbrk(nu * sizeof(Header));
 73e:	8b 45 08             	mov    0x8(%ebp),%eax
 741:	c1 e0 03             	shl    $0x3,%eax
 744:	83 ec 0c             	sub    $0xc,%esp
 747:	50                   	push   %eax
 748:	e8 31 fc ff ff       	call   37e <sbrk>
 74d:	83 c4 10             	add    $0x10,%esp
 750:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(p == (char*)-1)
 753:	83 7d f4 ff          	cmpl   $0xffffffff,-0xc(%ebp)
 757:	75 07                	jne    760 <morecore+0x38>
    return 0;
 759:	b8 00 00 00 00       	mov    $0x0,%eax
 75e:	eb 26                	jmp    786 <morecore+0x5e>
  hp = (Header*)p;
 760:	8b 45 f4             	mov    -0xc(%ebp),%eax
 763:	89 45 f0             	mov    %eax,-0x10(%ebp)
  hp->s.size = nu;
 766:	8b 45 f0             	mov    -0x10(%ebp),%eax
 769:	8b 55 08             	mov    0x8(%ebp),%edx
 76c:	89 50 04             	mov    %edx,0x4(%eax)
  free((void*)(hp + 1));
 76f:	8b 45 f0             	mov    -0x10(%ebp),%eax
 772:	83 c0 08             	add    $0x8,%eax
 775:	83 ec 0c             	sub    $0xc,%esp
 778:	50                   	push   %eax
 779:	e8 c8 fe ff ff       	call   646 <free>
 77e:	83 c4 10             	add    $0x10,%esp
  return freep;
 781:	a1 f0 0a 00 00       	mov    0xaf0,%eax
}
 786:	c9                   	leave  
 787:	c3                   	ret    

00000788 <malloc>:

void*
malloc(uint nbytes)
{
 788:	55                   	push   %ebp
 789:	89 e5                	mov    %esp,%ebp
 78b:	83 ec 18             	sub    $0x18,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 78e:	8b 45 08             	mov    0x8(%ebp),%eax
 791:	83 c0 07             	add    $0x7,%eax
 794:	c1 e8 03             	shr    $0x3,%eax
 797:	83 c0 01             	add    $0x1,%eax
 79a:	89 45 ec             	mov    %eax,-0x14(%ebp)
  if((prevp = freep) == 0){
 79d:	a1 f0 0a 00 00       	mov    0xaf0,%eax
 7a2:	89 45 f0             	mov    %eax,-0x10(%ebp)
 7a5:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 7a9:	75 23                	jne    7ce <malloc+0x46>
    base.s.ptr = freep = prevp = &base;
 7ab:	c7 45 f0 e8 0a 00 00 	movl   $0xae8,-0x10(%ebp)
 7b2:	8b 45 f0             	mov    -0x10(%ebp),%eax
 7b5:	a3 f0 0a 00 00       	mov    %eax,0xaf0
 7ba:	a1 f0 0a 00 00       	mov    0xaf0,%eax
 7bf:	a3 e8 0a 00 00       	mov    %eax,0xae8
    base.s.size = 0;
 7c4:	c7 05 ec 0a 00 00 00 	movl   $0x0,0xaec
 7cb:	00 00 00 
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 7ce:	8b 45 f0             	mov    -0x10(%ebp),%eax
 7d1:	8b 00                	mov    (%eax),%eax
 7d3:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if(p->s.size >= nunits){
 7d6:	8b 45 f4             	mov    -0xc(%ebp),%eax
 7d9:	8b 40 04             	mov    0x4(%eax),%eax
 7dc:	3b 45 ec             	cmp    -0x14(%ebp),%eax
 7df:	72 4d                	jb     82e <malloc+0xa6>
      if(p->s.size == nunits)
 7e1:	8b 45 f4             	mov    -0xc(%ebp),%eax
 7e4:	8b 40 04             	mov    0x4(%eax),%eax
 7e7:	3b 45 ec             	cmp    -0x14(%ebp),%eax
 7ea:	75 0c                	jne    7f8 <malloc+0x70>
        prevp->s.ptr = p->s.ptr;
 7ec:	8b 45 f4             	mov    -0xc(%ebp),%eax
 7ef:	8b 10                	mov    (%eax),%edx
 7f1:	8b 45 f0             	mov    -0x10(%ebp),%eax
 7f4:	89 10                	mov    %edx,(%eax)
 7f6:	eb 26                	jmp    81e <malloc+0x96>
      else {
        p->s.size -= nunits;
 7f8:	8b 45 f4             	mov    -0xc(%ebp),%eax
 7fb:	8b 40 04             	mov    0x4(%eax),%eax
 7fe:	2b 45 ec             	sub    -0x14(%ebp),%eax
 801:	89 c2                	mov    %eax,%edx
 803:	8b 45 f4             	mov    -0xc(%ebp),%eax
 806:	89 50 04             	mov    %edx,0x4(%eax)
        p += p->s.size;
 809:	8b 45 f4             	mov    -0xc(%ebp),%eax
 80c:	8b 40 04             	mov    0x4(%eax),%eax
 80f:	c1 e0 03             	shl    $0x3,%eax
 812:	01 45 f4             	add    %eax,-0xc(%ebp)
        p->s.size = nunits;
 815:	8b 45 f4             	mov    -0xc(%ebp),%eax
 818:	8b 55 ec             	mov    -0x14(%ebp),%edx
 81b:	89 50 04             	mov    %edx,0x4(%eax)
      }
      freep = prevp;
 81e:	8b 45 f0             	mov    -0x10(%ebp),%eax
 821:	a3 f0 0a 00 00       	mov    %eax,0xaf0
      return (void*)(p + 1);
 826:	8b 45 f4             	mov    -0xc(%ebp),%eax
 829:	83 c0 08             	add    $0x8,%eax
 82c:	eb 3b                	jmp    869 <malloc+0xe1>
    }
    if(p == freep)
 82e:	a1 f0 0a 00 00       	mov    0xaf0,%eax
 833:	39 45 f4             	cmp    %eax,-0xc(%ebp)
 836:	75 1e                	jne    856 <malloc+0xce>
      if((p = morecore(nunits)) == 0)
 838:	83 ec 0c             	sub    $0xc,%esp
 83b:	ff 75 ec             	pushl  -0x14(%ebp)
 83e:	e8 e5 fe ff ff       	call   728 <morecore>
 843:	83 c4 10             	add    $0x10,%esp
 846:	89 45 f4             	mov    %eax,-0xc(%ebp)
 849:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 84d:	75 07                	jne    856 <malloc+0xce>
        return 0;
 84f:	b8 00 00 00 00       	mov    $0x0,%eax
 854:	eb 13                	jmp    869 <malloc+0xe1>
  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
  if((prevp = freep) == 0){
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 856:	8b 45 f4             	mov    -0xc(%ebp),%eax
 859:	89 45 f0             	mov    %eax,-0x10(%ebp)
 85c:	8b 45 f4             	mov    -0xc(%ebp),%eax
 85f:	8b 00                	mov    (%eax),%eax
 861:	89 45 f4             	mov    %eax,-0xc(%ebp)
      return (void*)(p + 1);
    }
    if(p == freep)
      if((p = morecore(nunits)) == 0)
        return 0;
  }
 864:	e9 6d ff ff ff       	jmp    7d6 <malloc+0x4e>
}
 869:	c9                   	leave  
 86a:	c3                   	ret    
