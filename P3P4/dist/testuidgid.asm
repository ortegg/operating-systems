
_testuidgid:     file format elf32-i386


Disassembly of section .text:

00000000 <uidTest>:

#define TPS 100 	//TPS : Ticks Per Second

static void
uidTest(uint nval)
{
   0:	55                   	push   %ebp
   1:	89 e5                	mov    %esp,%ebp
   3:	83 ec 18             	sub    $0x18,%esp
  uint uid = getuid();
   6:	e8 46 07 00 00       	call   751 <getuid>
   b:	89 45 f4             	mov    %eax,-0xc(%ebp)
  printf(1, "Current UID is: %d\n", uid);
   e:	83 ec 04             	sub    $0x4,%esp
  11:	ff 75 f4             	pushl  -0xc(%ebp)
  14:	68 18 0c 00 00       	push   $0xc18
  19:	6a 01                	push   $0x1
  1b:	e8 40 08 00 00       	call   860 <printf>
  20:	83 c4 10             	add    $0x10,%esp
  printf(1, "Setting UID to: %d\n", nval);
  23:	83 ec 04             	sub    $0x4,%esp
  26:	ff 75 08             	pushl  0x8(%ebp)
  29:	68 2c 0c 00 00       	push   $0xc2c
  2e:	6a 01                	push   $0x1
  30:	e8 2b 08 00 00       	call   860 <printf>
  35:	83 c4 10             	add    $0x10,%esp
  if(setuid(nval) < 0)  
  38:	8b 45 08             	mov    0x8(%ebp),%eax
  3b:	83 ec 0c             	sub    $0xc,%esp
  3e:	50                   	push   %eax
  3f:	e8 25 07 00 00       	call   769 <setuid>
  44:	83 c4 10             	add    $0x10,%esp
  47:	85 c0                	test   %eax,%eax
  49:	79 15                	jns    60 <uidTest+0x60>
    printf(2, "Error. Invalid UID: %d\n", nval);
  4b:	83 ec 04             	sub    $0x4,%esp
  4e:	ff 75 08             	pushl  0x8(%ebp)
  51:	68 40 0c 00 00       	push   $0xc40
  56:	6a 02                	push   $0x2
  58:	e8 03 08 00 00       	call   860 <printf>
  5d:	83 c4 10             	add    $0x10,%esp
  setuid(nval);
  60:	8b 45 08             	mov    0x8(%ebp),%eax
  63:	83 ec 0c             	sub    $0xc,%esp
  66:	50                   	push   %eax
  67:	e8 fd 06 00 00       	call   769 <setuid>
  6c:	83 c4 10             	add    $0x10,%esp
  uid = getuid();
  6f:	e8 dd 06 00 00       	call   751 <getuid>
  74:	89 45 f4             	mov    %eax,-0xc(%ebp)
  printf(1, "Current UID is: %d\n", uid);
  77:	83 ec 04             	sub    $0x4,%esp
  7a:	ff 75 f4             	pushl  -0xc(%ebp)
  7d:	68 18 0c 00 00       	push   $0xc18
  82:	6a 01                	push   $0x1
  84:	e8 d7 07 00 00       	call   860 <printf>
  89:	83 c4 10             	add    $0x10,%esp
  sleep(5 * TPS); //now type control p
  8c:	83 ec 0c             	sub    $0xc,%esp
  8f:	68 f4 01 00 00       	push   $0x1f4
  94:	e8 98 06 00 00       	call   731 <sleep>
  99:	83 c4 10             	add    $0x10,%esp
} 
  9c:	90                   	nop
  9d:	c9                   	leave  
  9e:	c3                   	ret    

0000009f <gidTest>:

static void
gidTest(uint nval)
{
  9f:	55                   	push   %ebp
  a0:	89 e5                	mov    %esp,%ebp
  a2:	83 ec 18             	sub    $0x18,%esp
  uint gid = getgid();
  a5:	e8 af 06 00 00       	call   759 <getgid>
  aa:	89 45 f4             	mov    %eax,-0xc(%ebp)
  printf(1, "Current GID is: %d\n", gid);
  ad:	83 ec 04             	sub    $0x4,%esp
  b0:	ff 75 f4             	pushl  -0xc(%ebp)
  b3:	68 58 0c 00 00       	push   $0xc58
  b8:	6a 01                	push   $0x1
  ba:	e8 a1 07 00 00       	call   860 <printf>
  bf:	83 c4 10             	add    $0x10,%esp
  printf(1, "Setting GID to: %d\n", nval);
  c2:	83 ec 04             	sub    $0x4,%esp
  c5:	ff 75 08             	pushl  0x8(%ebp)
  c8:	68 6c 0c 00 00       	push   $0xc6c
  cd:	6a 01                	push   $0x1
  cf:	e8 8c 07 00 00       	call   860 <printf>
  d4:	83 c4 10             	add    $0x10,%esp
  if(setgid(nval) < 0)  
  d7:	8b 45 08             	mov    0x8(%ebp),%eax
  da:	83 ec 0c             	sub    $0xc,%esp
  dd:	50                   	push   %eax
  de:	e8 8e 06 00 00       	call   771 <setgid>
  e3:	83 c4 10             	add    $0x10,%esp
  e6:	85 c0                	test   %eax,%eax
  e8:	79 15                	jns    ff <gidTest+0x60>
    printf(2, "Error. Invalid GID: %d\n", nval);
  ea:	83 ec 04             	sub    $0x4,%esp
  ed:	ff 75 08             	pushl  0x8(%ebp)
  f0:	68 80 0c 00 00       	push   $0xc80
  f5:	6a 02                	push   $0x2
  f7:	e8 64 07 00 00       	call   860 <printf>
  fc:	83 c4 10             	add    $0x10,%esp
  setgid(nval);
  ff:	8b 45 08             	mov    0x8(%ebp),%eax
 102:	83 ec 0c             	sub    $0xc,%esp
 105:	50                   	push   %eax
 106:	e8 66 06 00 00       	call   771 <setgid>
 10b:	83 c4 10             	add    $0x10,%esp
  gid = getgid();
 10e:	e8 46 06 00 00       	call   759 <getgid>
 113:	89 45 f4             	mov    %eax,-0xc(%ebp)
  printf(1, "Current GID is: %d\n", gid);
 116:	83 ec 04             	sub    $0x4,%esp
 119:	ff 75 f4             	pushl  -0xc(%ebp)
 11c:	68 58 0c 00 00       	push   $0xc58
 121:	6a 01                	push   $0x1
 123:	e8 38 07 00 00       	call   860 <printf>
 128:	83 c4 10             	add    $0x10,%esp
  sleep(5 * TPS); //now type control p
 12b:	83 ec 0c             	sub    $0xc,%esp
 12e:	68 f4 01 00 00       	push   $0x1f4
 133:	e8 f9 05 00 00       	call   731 <sleep>
 138:	83 c4 10             	add    $0x10,%esp
}
 13b:	90                   	nop
 13c:	c9                   	leave  
 13d:	c3                   	ret    

0000013e <forkTest>:

static void
forkTest(uint nval)
{
 13e:	55                   	push   %ebp
 13f:	89 e5                	mov    %esp,%ebp
 141:	53                   	push   %ebx
 142:	83 ec 14             	sub    $0x14,%esp
  uint uid, gid;
  int pid;

  printf(1, "Setting UID to %d and GID to %d before fork(). Value should be inherited\n", nval, nval);
 145:	ff 75 08             	pushl  0x8(%ebp)
 148:	ff 75 08             	pushl  0x8(%ebp)
 14b:	68 98 0c 00 00       	push   $0xc98
 150:	6a 01                	push   $0x1
 152:	e8 09 07 00 00       	call   860 <printf>
 157:	83 c4 10             	add    $0x10,%esp

  if(setuid(nval) < 0)  
 15a:	8b 45 08             	mov    0x8(%ebp),%eax
 15d:	83 ec 0c             	sub    $0xc,%esp
 160:	50                   	push   %eax
 161:	e8 03 06 00 00       	call   769 <setuid>
 166:	83 c4 10             	add    $0x10,%esp
 169:	85 c0                	test   %eax,%eax
 16b:	79 15                	jns    182 <forkTest+0x44>
    printf(2, "Error. Invalid UID: %d\n", nval); 
 16d:	83 ec 04             	sub    $0x4,%esp
 170:	ff 75 08             	pushl  0x8(%ebp)
 173:	68 40 0c 00 00       	push   $0xc40
 178:	6a 02                	push   $0x2
 17a:	e8 e1 06 00 00       	call   860 <printf>
 17f:	83 c4 10             	add    $0x10,%esp
  if(setgid(nval) < 0)  
 182:	8b 45 08             	mov    0x8(%ebp),%eax
 185:	83 ec 0c             	sub    $0xc,%esp
 188:	50                   	push   %eax
 189:	e8 e3 05 00 00       	call   771 <setgid>
 18e:	83 c4 10             	add    $0x10,%esp
 191:	85 c0                	test   %eax,%eax
 193:	79 15                	jns    1aa <forkTest+0x6c>
    printf(2, "Error. Invalid GID: %d\n", nval);
 195:	83 ec 04             	sub    $0x4,%esp
 198:	ff 75 08             	pushl  0x8(%ebp)
 19b:	68 80 0c 00 00       	push   $0xc80
 1a0:	6a 02                	push   $0x2
 1a2:	e8 b9 06 00 00       	call   860 <printf>
 1a7:	83 c4 10             	add    $0x10,%esp

  printf(1, "Before fork(), UID = %d, GID = %d\n", getuid(), getgid());
 1aa:	e8 aa 05 00 00       	call   759 <getgid>
 1af:	89 c3                	mov    %eax,%ebx
 1b1:	e8 9b 05 00 00       	call   751 <getuid>
 1b6:	53                   	push   %ebx
 1b7:	50                   	push   %eax
 1b8:	68 e4 0c 00 00       	push   $0xce4
 1bd:	6a 01                	push   $0x1
 1bf:	e8 9c 06 00 00       	call   860 <printf>
 1c4:	83 c4 10             	add    $0x10,%esp
  pid = fork();
 1c7:	e8 cd 04 00 00       	call   699 <fork>
 1cc:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(pid == 0) {
 1cf:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 1d3:	75 3a                	jne    20f <forkTest+0xd1>
    uid = getuid();
 1d5:	e8 77 05 00 00       	call   751 <getuid>
 1da:	89 45 f0             	mov    %eax,-0x10(%ebp)
    gid = getgid();
 1dd:	e8 77 05 00 00       	call   759 <getgid>
 1e2:	89 45 ec             	mov    %eax,-0x14(%ebp)
    printf(1, "Child: UID is: %d, GID = %d\n", uid, gid);
 1e5:	ff 75 ec             	pushl  -0x14(%ebp)
 1e8:	ff 75 f0             	pushl  -0x10(%ebp)
 1eb:	68 07 0d 00 00       	push   $0xd07
 1f0:	6a 01                	push   $0x1
 1f2:	e8 69 06 00 00       	call   860 <printf>
 1f7:	83 c4 10             	add    $0x10,%esp
    sleep(5 * TPS); //now type control p
 1fa:	83 ec 0c             	sub    $0xc,%esp
 1fd:	68 f4 01 00 00       	push   $0x1f4
 202:	e8 2a 05 00 00       	call   731 <sleep>
 207:	83 c4 10             	add    $0x10,%esp
    exit();
 20a:	e8 92 04 00 00       	call   6a1 <exit>
  }
  else 
    sleep(10 * TPS); //wait for child to exit before proceeding
 20f:	83 ec 0c             	sub    $0xc,%esp
 212:	68 e8 03 00 00       	push   $0x3e8
 217:	e8 15 05 00 00       	call   731 <sleep>
 21c:	83 c4 10             	add    $0x10,%esp
}
 21f:	90                   	nop
 220:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 223:	c9                   	leave  
 224:	c3                   	ret    

00000225 <invalidTest>:

static void
invalidTest(uint nval)
{
 225:	55                   	push   %ebp
 226:	89 e5                	mov    %esp,%ebp
 228:	83 ec 08             	sub    $0x8,%esp
  printf(1, "Setting UID to %d. This test should FAIL\n", nval);
 22b:	83 ec 04             	sub    $0x4,%esp
 22e:	ff 75 08             	pushl  0x8(%ebp)
 231:	68 24 0d 00 00       	push   $0xd24
 236:	6a 01                	push   $0x1
 238:	e8 23 06 00 00       	call   860 <printf>
 23d:	83 c4 10             	add    $0x10,%esp
  if(setuid(nval) < 0)
 240:	8b 45 08             	mov    0x8(%ebp),%eax
 243:	83 ec 0c             	sub    $0xc,%esp
 246:	50                   	push   %eax
 247:	e8 1d 05 00 00       	call   769 <setuid>
 24c:	83 c4 10             	add    $0x10,%esp
 24f:	85 c0                	test   %eax,%eax
 251:	79 14                	jns    267 <invalidTest+0x42>
    printf(1, "SUCCESS! The setuid system call indicated failure\n");
 253:	83 ec 08             	sub    $0x8,%esp
 256:	68 50 0d 00 00       	push   $0xd50
 25b:	6a 01                	push   $0x1
 25d:	e8 fe 05 00 00       	call   860 <printf>
 262:	83 c4 10             	add    $0x10,%esp
 265:	eb 12                	jmp    279 <invalidTest+0x54>
  else
    printf(2, "FAILURE! The setuid system call indicated success\n");
 267:	83 ec 08             	sub    $0x8,%esp
 26a:	68 84 0d 00 00       	push   $0xd84
 26f:	6a 02                	push   $0x2
 271:	e8 ea 05 00 00       	call   860 <printf>
 276:	83 c4 10             	add    $0x10,%esp

  printf(1, "Setting GID to %d. This test should FAIL\n", nval);
 279:	83 ec 04             	sub    $0x4,%esp
 27c:	ff 75 08             	pushl  0x8(%ebp)
 27f:	68 b8 0d 00 00       	push   $0xdb8
 284:	6a 01                	push   $0x1
 286:	e8 d5 05 00 00       	call   860 <printf>
 28b:	83 c4 10             	add    $0x10,%esp
   if(setgid(nval) < 0)
 28e:	8b 45 08             	mov    0x8(%ebp),%eax
 291:	83 ec 0c             	sub    $0xc,%esp
 294:	50                   	push   %eax
 295:	e8 d7 04 00 00       	call   771 <setgid>
 29a:	83 c4 10             	add    $0x10,%esp
 29d:	85 c0                	test   %eax,%eax
 29f:	79 14                	jns    2b5 <invalidTest+0x90>
    printf(1, "SUCCESS! The setgid system call indicated failure\n");
 2a1:	83 ec 08             	sub    $0x8,%esp
 2a4:	68 e4 0d 00 00       	push   $0xde4
 2a9:	6a 01                	push   $0x1
 2ab:	e8 b0 05 00 00       	call   860 <printf>
 2b0:	83 c4 10             	add    $0x10,%esp
 2b3:	eb 12                	jmp    2c7 <invalidTest+0xa2>
  else
    printf(2, "FAILURE! The setgid system call indicated success\n");
 2b5:	83 ec 08             	sub    $0x8,%esp
 2b8:	68 18 0e 00 00       	push   $0xe18
 2bd:	6a 02                	push   $0x2
 2bf:	e8 9c 05 00 00       	call   860 <printf>
 2c4:	83 c4 10             	add    $0x10,%esp

  printf(1, "Setting UID to %d. This test should FAIL\n", -1);
 2c7:	83 ec 04             	sub    $0x4,%esp
 2ca:	6a ff                	push   $0xffffffff
 2cc:	68 24 0d 00 00       	push   $0xd24
 2d1:	6a 01                	push   $0x1
 2d3:	e8 88 05 00 00       	call   860 <printf>
 2d8:	83 c4 10             	add    $0x10,%esp
  if(setuid(-1) < 0)
 2db:	83 ec 0c             	sub    $0xc,%esp
 2de:	6a ff                	push   $0xffffffff
 2e0:	e8 84 04 00 00       	call   769 <setuid>
 2e5:	83 c4 10             	add    $0x10,%esp
 2e8:	85 c0                	test   %eax,%eax
 2ea:	79 14                	jns    300 <invalidTest+0xdb>
    printf(1, "SUCCESS! The setuid system call indicated failure\n");
 2ec:	83 ec 08             	sub    $0x8,%esp
 2ef:	68 50 0d 00 00       	push   $0xd50
 2f4:	6a 01                	push   $0x1
 2f6:	e8 65 05 00 00       	call   860 <printf>
 2fb:	83 c4 10             	add    $0x10,%esp
 2fe:	eb 12                	jmp    312 <invalidTest+0xed>
  else
    printf(2, "FAILURE! The setuid system call indicated success\n");
 300:	83 ec 08             	sub    $0x8,%esp
 303:	68 84 0d 00 00       	push   $0xd84
 308:	6a 02                	push   $0x2
 30a:	e8 51 05 00 00       	call   860 <printf>
 30f:	83 c4 10             	add    $0x10,%esp

  printf(1, "Setting GID to %d. This test should FAIL\n", -1);
 312:	83 ec 04             	sub    $0x4,%esp
 315:	6a ff                	push   $0xffffffff
 317:	68 b8 0d 00 00       	push   $0xdb8
 31c:	6a 01                	push   $0x1
 31e:	e8 3d 05 00 00       	call   860 <printf>
 323:	83 c4 10             	add    $0x10,%esp
  if(setgid(-1) < 0)
 326:	83 ec 0c             	sub    $0xc,%esp
 329:	6a ff                	push   $0xffffffff
 32b:	e8 41 04 00 00       	call   771 <setgid>
 330:	83 c4 10             	add    $0x10,%esp
 333:	85 c0                	test   %eax,%eax
 335:	79 14                	jns    34b <invalidTest+0x126>
    printf(1, "SUCCESS! The setgid system call indicated failure\n");
 337:	83 ec 08             	sub    $0x8,%esp
 33a:	68 e4 0d 00 00       	push   $0xde4
 33f:	6a 01                	push   $0x1
 341:	e8 1a 05 00 00       	call   860 <printf>
 346:	83 c4 10             	add    $0x10,%esp
  else
    printf(2, "FAILURE! The setgid system call indicated success\n");
}
 349:	eb 12                	jmp    35d <invalidTest+0x138>

  printf(1, "Setting GID to %d. This test should FAIL\n", -1);
  if(setgid(-1) < 0)
    printf(1, "SUCCESS! The setgid system call indicated failure\n");
  else
    printf(2, "FAILURE! The setgid system call indicated success\n");
 34b:	83 ec 08             	sub    $0x8,%esp
 34e:	68 18 0e 00 00       	push   $0xe18
 353:	6a 02                	push   $0x2
 355:	e8 06 05 00 00       	call   860 <printf>
 35a:	83 c4 10             	add    $0x10,%esp
}
 35d:	90                   	nop
 35e:	c9                   	leave  
 35f:	c3                   	ret    

00000360 <testuidgid>:

static int
testuidgid(void)
{
 360:	55                   	push   %ebp
 361:	89 e5                	mov    %esp,%ebp
 363:	83 ec 18             	sub    $0x18,%esp
  uint nval, ppid;
  
  //get/set uid test
  nval = 100;
 366:	c7 45 f4 64 00 00 00 	movl   $0x64,-0xc(%ebp)
  uidTest(nval);
 36d:	83 ec 0c             	sub    $0xc,%esp
 370:	ff 75 f4             	pushl  -0xc(%ebp)
 373:	e8 88 fc ff ff       	call   0 <uidTest>
 378:	83 c4 10             	add    $0x10,%esp
  
  //get/set gid test
  nval = 200;
 37b:	c7 45 f4 c8 00 00 00 	movl   $0xc8,-0xc(%ebp)
  gidTest(nval);
 382:	83 ec 0c             	sub    $0xc,%esp
 385:	ff 75 f4             	pushl  -0xc(%ebp)
 388:	e8 12 fd ff ff       	call   9f <gidTest>
 38d:	83 c4 10             	add    $0x10,%esp

  //test ppid
  ppid = getppid();
 390:	e8 cc 03 00 00       	call   761 <getppid>
 395:	89 45 f0             	mov    %eax,-0x10(%ebp)
  printf(2, "My parent process is: %d\n", ppid);
 398:	83 ec 04             	sub    $0x4,%esp
 39b:	ff 75 f0             	pushl  -0x10(%ebp)
 39e:	68 4b 0e 00 00       	push   $0xe4b
 3a3:	6a 02                	push   $0x2
 3a5:	e8 b6 04 00 00       	call   860 <printf>
 3aa:	83 c4 10             	add    $0x10,%esp

  //fork tests to demonstrate UID/GID inheritance
  nval = 111;
 3ad:	c7 45 f4 6f 00 00 00 	movl   $0x6f,-0xc(%ebp)
  forkTest(nval);
 3b4:	83 ec 0c             	sub    $0xc,%esp
 3b7:	ff 75 f4             	pushl  -0xc(%ebp)
 3ba:	e8 7f fd ff ff       	call   13e <forkTest>
 3bf:	83 c4 10             	add    $0x10,%esp

  //test for invalid values for uid & gid
  nval = 32800;
 3c2:	c7 45 f4 20 80 00 00 	movl   $0x8020,-0xc(%ebp)
  invalidTest(nval);
 3c9:	83 ec 0c             	sub    $0xc,%esp
 3cc:	ff 75 f4             	pushl  -0xc(%ebp)
 3cf:	e8 51 fe ff ff       	call   225 <invalidTest>
 3d4:	83 c4 10             	add    $0x10,%esp

  printf(1, "Done!\n");
 3d7:	83 ec 08             	sub    $0x8,%esp
 3da:	68 65 0e 00 00       	push   $0xe65
 3df:	6a 01                	push   $0x1
 3e1:	e8 7a 04 00 00       	call   860 <printf>
 3e6:	83 c4 10             	add    $0x10,%esp
  return 0;
 3e9:	b8 00 00 00 00       	mov    $0x0,%eax
}
 3ee:	c9                   	leave  
 3ef:	c3                   	ret    

000003f0 <main>:

int main() {
 3f0:	8d 4c 24 04          	lea    0x4(%esp),%ecx
 3f4:	83 e4 f0             	and    $0xfffffff0,%esp
 3f7:	ff 71 fc             	pushl  -0x4(%ecx)
 3fa:	55                   	push   %ebp
 3fb:	89 e5                	mov    %esp,%ebp
 3fd:	51                   	push   %ecx
 3fe:	83 ec 04             	sub    $0x4,%esp
  testuidgid();
 401:	e8 5a ff ff ff       	call   360 <testuidgid>
  exit();
 406:	e8 96 02 00 00       	call   6a1 <exit>

0000040b <sys_testuidgid>:
}

int 
sys_testuidgid(void)
{
 40b:	55                   	push   %ebp
 40c:	89 e5                	mov    %esp,%ebp
 40e:	83 ec 08             	sub    $0x8,%esp
  main();
 411:	e8 da ff ff ff       	call   3f0 <main>
  return 0;
 416:	b8 00 00 00 00       	mov    $0x0,%eax
}
 41b:	c9                   	leave  
 41c:	c3                   	ret    

0000041d <stosb>:
               "cc");
}

static inline void
stosb(void *addr, int data, int cnt)
{
 41d:	55                   	push   %ebp
 41e:	89 e5                	mov    %esp,%ebp
 420:	57                   	push   %edi
 421:	53                   	push   %ebx
  asm volatile("cld; rep stosb" :
 422:	8b 4d 08             	mov    0x8(%ebp),%ecx
 425:	8b 55 10             	mov    0x10(%ebp),%edx
 428:	8b 45 0c             	mov    0xc(%ebp),%eax
 42b:	89 cb                	mov    %ecx,%ebx
 42d:	89 df                	mov    %ebx,%edi
 42f:	89 d1                	mov    %edx,%ecx
 431:	fc                   	cld    
 432:	f3 aa                	rep stos %al,%es:(%edi)
 434:	89 ca                	mov    %ecx,%edx
 436:	89 fb                	mov    %edi,%ebx
 438:	89 5d 08             	mov    %ebx,0x8(%ebp)
 43b:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
 43e:	90                   	nop
 43f:	5b                   	pop    %ebx
 440:	5f                   	pop    %edi
 441:	5d                   	pop    %ebp
 442:	c3                   	ret    

00000443 <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, char *t)
{
 443:	55                   	push   %ebp
 444:	89 e5                	mov    %esp,%ebp
 446:	83 ec 10             	sub    $0x10,%esp
  char *os;

  os = s;
 449:	8b 45 08             	mov    0x8(%ebp),%eax
 44c:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while((*s++ = *t++) != 0)
 44f:	90                   	nop
 450:	8b 45 08             	mov    0x8(%ebp),%eax
 453:	8d 50 01             	lea    0x1(%eax),%edx
 456:	89 55 08             	mov    %edx,0x8(%ebp)
 459:	8b 55 0c             	mov    0xc(%ebp),%edx
 45c:	8d 4a 01             	lea    0x1(%edx),%ecx
 45f:	89 4d 0c             	mov    %ecx,0xc(%ebp)
 462:	0f b6 12             	movzbl (%edx),%edx
 465:	88 10                	mov    %dl,(%eax)
 467:	0f b6 00             	movzbl (%eax),%eax
 46a:	84 c0                	test   %al,%al
 46c:	75 e2                	jne    450 <strcpy+0xd>
    ;
  return os;
 46e:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 471:	c9                   	leave  
 472:	c3                   	ret    

00000473 <strcmp>:

int
strcmp(const char *p, const char *q)
{
 473:	55                   	push   %ebp
 474:	89 e5                	mov    %esp,%ebp
  while(*p && *p == *q)
 476:	eb 08                	jmp    480 <strcmp+0xd>
    p++, q++;
 478:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 47c:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
  while(*p && *p == *q)
 480:	8b 45 08             	mov    0x8(%ebp),%eax
 483:	0f b6 00             	movzbl (%eax),%eax
 486:	84 c0                	test   %al,%al
 488:	74 10                	je     49a <strcmp+0x27>
 48a:	8b 45 08             	mov    0x8(%ebp),%eax
 48d:	0f b6 10             	movzbl (%eax),%edx
 490:	8b 45 0c             	mov    0xc(%ebp),%eax
 493:	0f b6 00             	movzbl (%eax),%eax
 496:	38 c2                	cmp    %al,%dl
 498:	74 de                	je     478 <strcmp+0x5>
    p++, q++;
  return (uchar)*p - (uchar)*q;
 49a:	8b 45 08             	mov    0x8(%ebp),%eax
 49d:	0f b6 00             	movzbl (%eax),%eax
 4a0:	0f b6 d0             	movzbl %al,%edx
 4a3:	8b 45 0c             	mov    0xc(%ebp),%eax
 4a6:	0f b6 00             	movzbl (%eax),%eax
 4a9:	0f b6 c0             	movzbl %al,%eax
 4ac:	29 c2                	sub    %eax,%edx
 4ae:	89 d0                	mov    %edx,%eax
}
 4b0:	5d                   	pop    %ebp
 4b1:	c3                   	ret    

000004b2 <strlen>:

uint
strlen(char *s)
{
 4b2:	55                   	push   %ebp
 4b3:	89 e5                	mov    %esp,%ebp
 4b5:	83 ec 10             	sub    $0x10,%esp
  int n;

  for(n = 0; s[n]; n++)
 4b8:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
 4bf:	eb 04                	jmp    4c5 <strlen+0x13>
 4c1:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
 4c5:	8b 55 fc             	mov    -0x4(%ebp),%edx
 4c8:	8b 45 08             	mov    0x8(%ebp),%eax
 4cb:	01 d0                	add    %edx,%eax
 4cd:	0f b6 00             	movzbl (%eax),%eax
 4d0:	84 c0                	test   %al,%al
 4d2:	75 ed                	jne    4c1 <strlen+0xf>
    ;
  return n;
 4d4:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 4d7:	c9                   	leave  
 4d8:	c3                   	ret    

000004d9 <memset>:

void*
memset(void *dst, int c, uint n)
{
 4d9:	55                   	push   %ebp
 4da:	89 e5                	mov    %esp,%ebp
  stosb(dst, c, n);
 4dc:	8b 45 10             	mov    0x10(%ebp),%eax
 4df:	50                   	push   %eax
 4e0:	ff 75 0c             	pushl  0xc(%ebp)
 4e3:	ff 75 08             	pushl  0x8(%ebp)
 4e6:	e8 32 ff ff ff       	call   41d <stosb>
 4eb:	83 c4 0c             	add    $0xc,%esp
  return dst;
 4ee:	8b 45 08             	mov    0x8(%ebp),%eax
}
 4f1:	c9                   	leave  
 4f2:	c3                   	ret    

000004f3 <strchr>:

char*
strchr(const char *s, char c)
{
 4f3:	55                   	push   %ebp
 4f4:	89 e5                	mov    %esp,%ebp
 4f6:	83 ec 04             	sub    $0x4,%esp
 4f9:	8b 45 0c             	mov    0xc(%ebp),%eax
 4fc:	88 45 fc             	mov    %al,-0x4(%ebp)
  for(; *s; s++)
 4ff:	eb 14                	jmp    515 <strchr+0x22>
    if(*s == c)
 501:	8b 45 08             	mov    0x8(%ebp),%eax
 504:	0f b6 00             	movzbl (%eax),%eax
 507:	3a 45 fc             	cmp    -0x4(%ebp),%al
 50a:	75 05                	jne    511 <strchr+0x1e>
      return (char*)s;
 50c:	8b 45 08             	mov    0x8(%ebp),%eax
 50f:	eb 13                	jmp    524 <strchr+0x31>
}

char*
strchr(const char *s, char c)
{
  for(; *s; s++)
 511:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 515:	8b 45 08             	mov    0x8(%ebp),%eax
 518:	0f b6 00             	movzbl (%eax),%eax
 51b:	84 c0                	test   %al,%al
 51d:	75 e2                	jne    501 <strchr+0xe>
    if(*s == c)
      return (char*)s;
  return 0;
 51f:	b8 00 00 00 00       	mov    $0x0,%eax
}
 524:	c9                   	leave  
 525:	c3                   	ret    

00000526 <gets>:

char*
gets(char *buf, int max)
{
 526:	55                   	push   %ebp
 527:	89 e5                	mov    %esp,%ebp
 529:	83 ec 18             	sub    $0x18,%esp
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 52c:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 533:	eb 42                	jmp    577 <gets+0x51>
    cc = read(0, &c, 1);
 535:	83 ec 04             	sub    $0x4,%esp
 538:	6a 01                	push   $0x1
 53a:	8d 45 ef             	lea    -0x11(%ebp),%eax
 53d:	50                   	push   %eax
 53e:	6a 00                	push   $0x0
 540:	e8 74 01 00 00       	call   6b9 <read>
 545:	83 c4 10             	add    $0x10,%esp
 548:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if(cc < 1)
 54b:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 54f:	7e 33                	jle    584 <gets+0x5e>
      break;
    buf[i++] = c;
 551:	8b 45 f4             	mov    -0xc(%ebp),%eax
 554:	8d 50 01             	lea    0x1(%eax),%edx
 557:	89 55 f4             	mov    %edx,-0xc(%ebp)
 55a:	89 c2                	mov    %eax,%edx
 55c:	8b 45 08             	mov    0x8(%ebp),%eax
 55f:	01 c2                	add    %eax,%edx
 561:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 565:	88 02                	mov    %al,(%edx)
    if(c == '\n' || c == '\r')
 567:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 56b:	3c 0a                	cmp    $0xa,%al
 56d:	74 16                	je     585 <gets+0x5f>
 56f:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 573:	3c 0d                	cmp    $0xd,%al
 575:	74 0e                	je     585 <gets+0x5f>
gets(char *buf, int max)
{
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 577:	8b 45 f4             	mov    -0xc(%ebp),%eax
 57a:	83 c0 01             	add    $0x1,%eax
 57d:	3b 45 0c             	cmp    0xc(%ebp),%eax
 580:	7c b3                	jl     535 <gets+0xf>
 582:	eb 01                	jmp    585 <gets+0x5f>
    cc = read(0, &c, 1);
    if(cc < 1)
      break;
 584:	90                   	nop
    buf[i++] = c;
    if(c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 585:	8b 55 f4             	mov    -0xc(%ebp),%edx
 588:	8b 45 08             	mov    0x8(%ebp),%eax
 58b:	01 d0                	add    %edx,%eax
 58d:	c6 00 00             	movb   $0x0,(%eax)
  return buf;
 590:	8b 45 08             	mov    0x8(%ebp),%eax
}
 593:	c9                   	leave  
 594:	c3                   	ret    

00000595 <stat>:

int
stat(char *n, struct stat *st)
{
 595:	55                   	push   %ebp
 596:	89 e5                	mov    %esp,%ebp
 598:	83 ec 18             	sub    $0x18,%esp
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 59b:	83 ec 08             	sub    $0x8,%esp
 59e:	6a 00                	push   $0x0
 5a0:	ff 75 08             	pushl  0x8(%ebp)
 5a3:	e8 39 01 00 00       	call   6e1 <open>
 5a8:	83 c4 10             	add    $0x10,%esp
 5ab:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(fd < 0)
 5ae:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 5b2:	79 07                	jns    5bb <stat+0x26>
    return -1;
 5b4:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 5b9:	eb 25                	jmp    5e0 <stat+0x4b>
  r = fstat(fd, st);
 5bb:	83 ec 08             	sub    $0x8,%esp
 5be:	ff 75 0c             	pushl  0xc(%ebp)
 5c1:	ff 75 f4             	pushl  -0xc(%ebp)
 5c4:	e8 30 01 00 00       	call   6f9 <fstat>
 5c9:	83 c4 10             	add    $0x10,%esp
 5cc:	89 45 f0             	mov    %eax,-0x10(%ebp)
  close(fd);
 5cf:	83 ec 0c             	sub    $0xc,%esp
 5d2:	ff 75 f4             	pushl  -0xc(%ebp)
 5d5:	e8 ef 00 00 00       	call   6c9 <close>
 5da:	83 c4 10             	add    $0x10,%esp
  return r;
 5dd:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
 5e0:	c9                   	leave  
 5e1:	c3                   	ret    

000005e2 <atoi>:

int
atoi(const char *s)
{
 5e2:	55                   	push   %ebp
 5e3:	89 e5                	mov    %esp,%ebp
 5e5:	83 ec 10             	sub    $0x10,%esp
  int n, sign;

  n = 0;
 5e8:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  while(*s == ' ') s++;
 5ef:	eb 04                	jmp    5f5 <atoi+0x13>
 5f1:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 5f5:	8b 45 08             	mov    0x8(%ebp),%eax
 5f8:	0f b6 00             	movzbl (%eax),%eax
 5fb:	3c 20                	cmp    $0x20,%al
 5fd:	74 f2                	je     5f1 <atoi+0xf>
  sign = (*s == '-') ? -1 : 1;
 5ff:	8b 45 08             	mov    0x8(%ebp),%eax
 602:	0f b6 00             	movzbl (%eax),%eax
 605:	3c 2d                	cmp    $0x2d,%al
 607:	75 07                	jne    610 <atoi+0x2e>
 609:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 60e:	eb 05                	jmp    615 <atoi+0x33>
 610:	b8 01 00 00 00       	mov    $0x1,%eax
 615:	89 45 f8             	mov    %eax,-0x8(%ebp)
  while('0' <= *s && *s <= '9')
 618:	eb 25                	jmp    63f <atoi+0x5d>
    n = n*10 + *s++ - '0';
 61a:	8b 55 fc             	mov    -0x4(%ebp),%edx
 61d:	89 d0                	mov    %edx,%eax
 61f:	c1 e0 02             	shl    $0x2,%eax
 622:	01 d0                	add    %edx,%eax
 624:	01 c0                	add    %eax,%eax
 626:	89 c1                	mov    %eax,%ecx
 628:	8b 45 08             	mov    0x8(%ebp),%eax
 62b:	8d 50 01             	lea    0x1(%eax),%edx
 62e:	89 55 08             	mov    %edx,0x8(%ebp)
 631:	0f b6 00             	movzbl (%eax),%eax
 634:	0f be c0             	movsbl %al,%eax
 637:	01 c8                	add    %ecx,%eax
 639:	83 e8 30             	sub    $0x30,%eax
 63c:	89 45 fc             	mov    %eax,-0x4(%ebp)
  int n, sign;

  n = 0;
  while(*s == ' ') s++;
  sign = (*s == '-') ? -1 : 1;
  while('0' <= *s && *s <= '9')
 63f:	8b 45 08             	mov    0x8(%ebp),%eax
 642:	0f b6 00             	movzbl (%eax),%eax
 645:	3c 2f                	cmp    $0x2f,%al
 647:	7e 0a                	jle    653 <atoi+0x71>
 649:	8b 45 08             	mov    0x8(%ebp),%eax
 64c:	0f b6 00             	movzbl (%eax),%eax
 64f:	3c 39                	cmp    $0x39,%al
 651:	7e c7                	jle    61a <atoi+0x38>
    n = n*10 + *s++ - '0';
  return sign*n;
 653:	8b 45 f8             	mov    -0x8(%ebp),%eax
 656:	0f af 45 fc          	imul   -0x4(%ebp),%eax
}
 65a:	c9                   	leave  
 65b:	c3                   	ret    

0000065c <memmove>:

void*
memmove(void *vdst, void *vsrc, int n)
{
 65c:	55                   	push   %ebp
 65d:	89 e5                	mov    %esp,%ebp
 65f:	83 ec 10             	sub    $0x10,%esp
  char *dst, *src;
  
  dst = vdst;
 662:	8b 45 08             	mov    0x8(%ebp),%eax
 665:	89 45 fc             	mov    %eax,-0x4(%ebp)
  src = vsrc;
 668:	8b 45 0c             	mov    0xc(%ebp),%eax
 66b:	89 45 f8             	mov    %eax,-0x8(%ebp)
  while(n-- > 0)
 66e:	eb 17                	jmp    687 <memmove+0x2b>
    *dst++ = *src++;
 670:	8b 45 fc             	mov    -0x4(%ebp),%eax
 673:	8d 50 01             	lea    0x1(%eax),%edx
 676:	89 55 fc             	mov    %edx,-0x4(%ebp)
 679:	8b 55 f8             	mov    -0x8(%ebp),%edx
 67c:	8d 4a 01             	lea    0x1(%edx),%ecx
 67f:	89 4d f8             	mov    %ecx,-0x8(%ebp)
 682:	0f b6 12             	movzbl (%edx),%edx
 685:	88 10                	mov    %dl,(%eax)
{
  char *dst, *src;
  
  dst = vdst;
  src = vsrc;
  while(n-- > 0)
 687:	8b 45 10             	mov    0x10(%ebp),%eax
 68a:	8d 50 ff             	lea    -0x1(%eax),%edx
 68d:	89 55 10             	mov    %edx,0x10(%ebp)
 690:	85 c0                	test   %eax,%eax
 692:	7f dc                	jg     670 <memmove+0x14>
    *dst++ = *src++;
  return vdst;
 694:	8b 45 08             	mov    0x8(%ebp),%eax
}
 697:	c9                   	leave  
 698:	c3                   	ret    

00000699 <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
 699:	b8 01 00 00 00       	mov    $0x1,%eax
 69e:	cd 40                	int    $0x40
 6a0:	c3                   	ret    

000006a1 <exit>:
SYSCALL(exit)
 6a1:	b8 02 00 00 00       	mov    $0x2,%eax
 6a6:	cd 40                	int    $0x40
 6a8:	c3                   	ret    

000006a9 <wait>:
SYSCALL(wait)
 6a9:	b8 03 00 00 00       	mov    $0x3,%eax
 6ae:	cd 40                	int    $0x40
 6b0:	c3                   	ret    

000006b1 <pipe>:
SYSCALL(pipe)
 6b1:	b8 04 00 00 00       	mov    $0x4,%eax
 6b6:	cd 40                	int    $0x40
 6b8:	c3                   	ret    

000006b9 <read>:
SYSCALL(read)
 6b9:	b8 05 00 00 00       	mov    $0x5,%eax
 6be:	cd 40                	int    $0x40
 6c0:	c3                   	ret    

000006c1 <write>:
SYSCALL(write)
 6c1:	b8 10 00 00 00       	mov    $0x10,%eax
 6c6:	cd 40                	int    $0x40
 6c8:	c3                   	ret    

000006c9 <close>:
SYSCALL(close)
 6c9:	b8 15 00 00 00       	mov    $0x15,%eax
 6ce:	cd 40                	int    $0x40
 6d0:	c3                   	ret    

000006d1 <kill>:
SYSCALL(kill)
 6d1:	b8 06 00 00 00       	mov    $0x6,%eax
 6d6:	cd 40                	int    $0x40
 6d8:	c3                   	ret    

000006d9 <exec>:
SYSCALL(exec)
 6d9:	b8 07 00 00 00       	mov    $0x7,%eax
 6de:	cd 40                	int    $0x40
 6e0:	c3                   	ret    

000006e1 <open>:
SYSCALL(open)
 6e1:	b8 0f 00 00 00       	mov    $0xf,%eax
 6e6:	cd 40                	int    $0x40
 6e8:	c3                   	ret    

000006e9 <mknod>:
SYSCALL(mknod)
 6e9:	b8 11 00 00 00       	mov    $0x11,%eax
 6ee:	cd 40                	int    $0x40
 6f0:	c3                   	ret    

000006f1 <unlink>:
SYSCALL(unlink)
 6f1:	b8 12 00 00 00       	mov    $0x12,%eax
 6f6:	cd 40                	int    $0x40
 6f8:	c3                   	ret    

000006f9 <fstat>:
SYSCALL(fstat)
 6f9:	b8 08 00 00 00       	mov    $0x8,%eax
 6fe:	cd 40                	int    $0x40
 700:	c3                   	ret    

00000701 <link>:
SYSCALL(link)
 701:	b8 13 00 00 00       	mov    $0x13,%eax
 706:	cd 40                	int    $0x40
 708:	c3                   	ret    

00000709 <mkdir>:
SYSCALL(mkdir)
 709:	b8 14 00 00 00       	mov    $0x14,%eax
 70e:	cd 40                	int    $0x40
 710:	c3                   	ret    

00000711 <chdir>:
SYSCALL(chdir)
 711:	b8 09 00 00 00       	mov    $0x9,%eax
 716:	cd 40                	int    $0x40
 718:	c3                   	ret    

00000719 <dup>:
SYSCALL(dup)
 719:	b8 0a 00 00 00       	mov    $0xa,%eax
 71e:	cd 40                	int    $0x40
 720:	c3                   	ret    

00000721 <getpid>:
SYSCALL(getpid)
 721:	b8 0b 00 00 00       	mov    $0xb,%eax
 726:	cd 40                	int    $0x40
 728:	c3                   	ret    

00000729 <sbrk>:
SYSCALL(sbrk)
 729:	b8 0c 00 00 00       	mov    $0xc,%eax
 72e:	cd 40                	int    $0x40
 730:	c3                   	ret    

00000731 <sleep>:
SYSCALL(sleep)
 731:	b8 0d 00 00 00       	mov    $0xd,%eax
 736:	cd 40                	int    $0x40
 738:	c3                   	ret    

00000739 <uptime>:
SYSCALL(uptime)
 739:	b8 0e 00 00 00       	mov    $0xe,%eax
 73e:	cd 40                	int    $0x40
 740:	c3                   	ret    

00000741 <halt>:
SYSCALL(halt)
 741:	b8 16 00 00 00       	mov    $0x16,%eax
 746:	cd 40                	int    $0x40
 748:	c3                   	ret    

00000749 <date>:
SYSCALL(date)    #added the date system call
 749:	b8 17 00 00 00       	mov    $0x17,%eax
 74e:	cd 40                	int    $0x40
 750:	c3                   	ret    

00000751 <getuid>:
SYSCALL(getuid)
 751:	b8 18 00 00 00       	mov    $0x18,%eax
 756:	cd 40                	int    $0x40
 758:	c3                   	ret    

00000759 <getgid>:
SYSCALL(getgid)
 759:	b8 19 00 00 00       	mov    $0x19,%eax
 75e:	cd 40                	int    $0x40
 760:	c3                   	ret    

00000761 <getppid>:
SYSCALL(getppid)
 761:	b8 1a 00 00 00       	mov    $0x1a,%eax
 766:	cd 40                	int    $0x40
 768:	c3                   	ret    

00000769 <setuid>:
SYSCALL(setuid)
 769:	b8 1b 00 00 00       	mov    $0x1b,%eax
 76e:	cd 40                	int    $0x40
 770:	c3                   	ret    

00000771 <setgid>:
SYSCALL(setgid)
 771:	b8 1c 00 00 00       	mov    $0x1c,%eax
 776:	cd 40                	int    $0x40
 778:	c3                   	ret    

00000779 <getprocs>:
SYSCALL(getprocs)
 779:	b8 1d 00 00 00       	mov    $0x1d,%eax
 77e:	cd 40                	int    $0x40
 780:	c3                   	ret    

00000781 <setpriority>:
SYSCALL(setpriority)
 781:	b8 1e 00 00 00       	mov    $0x1e,%eax
 786:	cd 40                	int    $0x40
 788:	c3                   	ret    

00000789 <putc>:
#include "stat.h"
#include "user.h"

static void
putc(int fd, char c)
{
 789:	55                   	push   %ebp
 78a:	89 e5                	mov    %esp,%ebp
 78c:	83 ec 18             	sub    $0x18,%esp
 78f:	8b 45 0c             	mov    0xc(%ebp),%eax
 792:	88 45 f4             	mov    %al,-0xc(%ebp)
  write(fd, &c, 1);
 795:	83 ec 04             	sub    $0x4,%esp
 798:	6a 01                	push   $0x1
 79a:	8d 45 f4             	lea    -0xc(%ebp),%eax
 79d:	50                   	push   %eax
 79e:	ff 75 08             	pushl  0x8(%ebp)
 7a1:	e8 1b ff ff ff       	call   6c1 <write>
 7a6:	83 c4 10             	add    $0x10,%esp
}
 7a9:	90                   	nop
 7aa:	c9                   	leave  
 7ab:	c3                   	ret    

000007ac <printint>:

static void
printint(int fd, int xx, int base, int sgn)
{
 7ac:	55                   	push   %ebp
 7ad:	89 e5                	mov    %esp,%ebp
 7af:	53                   	push   %ebx
 7b0:	83 ec 24             	sub    $0x24,%esp
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 7b3:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  if(sgn && xx < 0){
 7ba:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
 7be:	74 17                	je     7d7 <printint+0x2b>
 7c0:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
 7c4:	79 11                	jns    7d7 <printint+0x2b>
    neg = 1;
 7c6:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
    x = -xx;
 7cd:	8b 45 0c             	mov    0xc(%ebp),%eax
 7d0:	f7 d8                	neg    %eax
 7d2:	89 45 ec             	mov    %eax,-0x14(%ebp)
 7d5:	eb 06                	jmp    7dd <printint+0x31>
  } else {
    x = xx;
 7d7:	8b 45 0c             	mov    0xc(%ebp),%eax
 7da:	89 45 ec             	mov    %eax,-0x14(%ebp)
  }

  i = 0;
 7dd:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  do{
    buf[i++] = digits[x % base];
 7e4:	8b 4d f4             	mov    -0xc(%ebp),%ecx
 7e7:	8d 41 01             	lea    0x1(%ecx),%eax
 7ea:	89 45 f4             	mov    %eax,-0xc(%ebp)
 7ed:	8b 5d 10             	mov    0x10(%ebp),%ebx
 7f0:	8b 45 ec             	mov    -0x14(%ebp),%eax
 7f3:	ba 00 00 00 00       	mov    $0x0,%edx
 7f8:	f7 f3                	div    %ebx
 7fa:	89 d0                	mov    %edx,%eax
 7fc:	0f b6 80 80 11 00 00 	movzbl 0x1180(%eax),%eax
 803:	88 44 0d dc          	mov    %al,-0x24(%ebp,%ecx,1)
  }while((x /= base) != 0);
 807:	8b 5d 10             	mov    0x10(%ebp),%ebx
 80a:	8b 45 ec             	mov    -0x14(%ebp),%eax
 80d:	ba 00 00 00 00       	mov    $0x0,%edx
 812:	f7 f3                	div    %ebx
 814:	89 45 ec             	mov    %eax,-0x14(%ebp)
 817:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 81b:	75 c7                	jne    7e4 <printint+0x38>
  if(neg)
 81d:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 821:	74 2d                	je     850 <printint+0xa4>
    buf[i++] = '-';
 823:	8b 45 f4             	mov    -0xc(%ebp),%eax
 826:	8d 50 01             	lea    0x1(%eax),%edx
 829:	89 55 f4             	mov    %edx,-0xc(%ebp)
 82c:	c6 44 05 dc 2d       	movb   $0x2d,-0x24(%ebp,%eax,1)

  while(--i >= 0)
 831:	eb 1d                	jmp    850 <printint+0xa4>
    putc(fd, buf[i]);
 833:	8d 55 dc             	lea    -0x24(%ebp),%edx
 836:	8b 45 f4             	mov    -0xc(%ebp),%eax
 839:	01 d0                	add    %edx,%eax
 83b:	0f b6 00             	movzbl (%eax),%eax
 83e:	0f be c0             	movsbl %al,%eax
 841:	83 ec 08             	sub    $0x8,%esp
 844:	50                   	push   %eax
 845:	ff 75 08             	pushl  0x8(%ebp)
 848:	e8 3c ff ff ff       	call   789 <putc>
 84d:	83 c4 10             	add    $0x10,%esp
    buf[i++] = digits[x % base];
  }while((x /= base) != 0);
  if(neg)
    buf[i++] = '-';

  while(--i >= 0)
 850:	83 6d f4 01          	subl   $0x1,-0xc(%ebp)
 854:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 858:	79 d9                	jns    833 <printint+0x87>
    putc(fd, buf[i]);
}
 85a:	90                   	nop
 85b:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 85e:	c9                   	leave  
 85f:	c3                   	ret    

00000860 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, char *fmt, ...)
{
 860:	55                   	push   %ebp
 861:	89 e5                	mov    %esp,%ebp
 863:	83 ec 28             	sub    $0x28,%esp
  char *s;
  int c, i, state;
  uint *ap;

  state = 0;
 866:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  ap = (uint*)(void*)&fmt + 1;
 86d:	8d 45 0c             	lea    0xc(%ebp),%eax
 870:	83 c0 04             	add    $0x4,%eax
 873:	89 45 e8             	mov    %eax,-0x18(%ebp)
  for(i = 0; fmt[i]; i++){
 876:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 87d:	e9 59 01 00 00       	jmp    9db <printf+0x17b>
    c = fmt[i] & 0xff;
 882:	8b 55 0c             	mov    0xc(%ebp),%edx
 885:	8b 45 f0             	mov    -0x10(%ebp),%eax
 888:	01 d0                	add    %edx,%eax
 88a:	0f b6 00             	movzbl (%eax),%eax
 88d:	0f be c0             	movsbl %al,%eax
 890:	25 ff 00 00 00       	and    $0xff,%eax
 895:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    if(state == 0){
 898:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 89c:	75 2c                	jne    8ca <printf+0x6a>
      if(c == '%'){
 89e:	83 7d e4 25          	cmpl   $0x25,-0x1c(%ebp)
 8a2:	75 0c                	jne    8b0 <printf+0x50>
        state = '%';
 8a4:	c7 45 ec 25 00 00 00 	movl   $0x25,-0x14(%ebp)
 8ab:	e9 27 01 00 00       	jmp    9d7 <printf+0x177>
      } else {
        putc(fd, c);
 8b0:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 8b3:	0f be c0             	movsbl %al,%eax
 8b6:	83 ec 08             	sub    $0x8,%esp
 8b9:	50                   	push   %eax
 8ba:	ff 75 08             	pushl  0x8(%ebp)
 8bd:	e8 c7 fe ff ff       	call   789 <putc>
 8c2:	83 c4 10             	add    $0x10,%esp
 8c5:	e9 0d 01 00 00       	jmp    9d7 <printf+0x177>
      }
    } else if(state == '%'){
 8ca:	83 7d ec 25          	cmpl   $0x25,-0x14(%ebp)
 8ce:	0f 85 03 01 00 00    	jne    9d7 <printf+0x177>
      if(c == 'd'){
 8d4:	83 7d e4 64          	cmpl   $0x64,-0x1c(%ebp)
 8d8:	75 1e                	jne    8f8 <printf+0x98>
        printint(fd, *ap, 10, 1);
 8da:	8b 45 e8             	mov    -0x18(%ebp),%eax
 8dd:	8b 00                	mov    (%eax),%eax
 8df:	6a 01                	push   $0x1
 8e1:	6a 0a                	push   $0xa
 8e3:	50                   	push   %eax
 8e4:	ff 75 08             	pushl  0x8(%ebp)
 8e7:	e8 c0 fe ff ff       	call   7ac <printint>
 8ec:	83 c4 10             	add    $0x10,%esp
        ap++;
 8ef:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 8f3:	e9 d8 00 00 00       	jmp    9d0 <printf+0x170>
      } else if(c == 'x' || c == 'p'){
 8f8:	83 7d e4 78          	cmpl   $0x78,-0x1c(%ebp)
 8fc:	74 06                	je     904 <printf+0xa4>
 8fe:	83 7d e4 70          	cmpl   $0x70,-0x1c(%ebp)
 902:	75 1e                	jne    922 <printf+0xc2>
        printint(fd, *ap, 16, 0);
 904:	8b 45 e8             	mov    -0x18(%ebp),%eax
 907:	8b 00                	mov    (%eax),%eax
 909:	6a 00                	push   $0x0
 90b:	6a 10                	push   $0x10
 90d:	50                   	push   %eax
 90e:	ff 75 08             	pushl  0x8(%ebp)
 911:	e8 96 fe ff ff       	call   7ac <printint>
 916:	83 c4 10             	add    $0x10,%esp
        ap++;
 919:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 91d:	e9 ae 00 00 00       	jmp    9d0 <printf+0x170>
      } else if(c == 's'){
 922:	83 7d e4 73          	cmpl   $0x73,-0x1c(%ebp)
 926:	75 43                	jne    96b <printf+0x10b>
        s = (char*)*ap;
 928:	8b 45 e8             	mov    -0x18(%ebp),%eax
 92b:	8b 00                	mov    (%eax),%eax
 92d:	89 45 f4             	mov    %eax,-0xc(%ebp)
        ap++;
 930:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
        if(s == 0)
 934:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 938:	75 25                	jne    95f <printf+0xff>
          s = "(null)";
 93a:	c7 45 f4 6c 0e 00 00 	movl   $0xe6c,-0xc(%ebp)
        while(*s != 0){
 941:	eb 1c                	jmp    95f <printf+0xff>
          putc(fd, *s);
 943:	8b 45 f4             	mov    -0xc(%ebp),%eax
 946:	0f b6 00             	movzbl (%eax),%eax
 949:	0f be c0             	movsbl %al,%eax
 94c:	83 ec 08             	sub    $0x8,%esp
 94f:	50                   	push   %eax
 950:	ff 75 08             	pushl  0x8(%ebp)
 953:	e8 31 fe ff ff       	call   789 <putc>
 958:	83 c4 10             	add    $0x10,%esp
          s++;
 95b:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
      } else if(c == 's'){
        s = (char*)*ap;
        ap++;
        if(s == 0)
          s = "(null)";
        while(*s != 0){
 95f:	8b 45 f4             	mov    -0xc(%ebp),%eax
 962:	0f b6 00             	movzbl (%eax),%eax
 965:	84 c0                	test   %al,%al
 967:	75 da                	jne    943 <printf+0xe3>
 969:	eb 65                	jmp    9d0 <printf+0x170>
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 96b:	83 7d e4 63          	cmpl   $0x63,-0x1c(%ebp)
 96f:	75 1d                	jne    98e <printf+0x12e>
        putc(fd, *ap);
 971:	8b 45 e8             	mov    -0x18(%ebp),%eax
 974:	8b 00                	mov    (%eax),%eax
 976:	0f be c0             	movsbl %al,%eax
 979:	83 ec 08             	sub    $0x8,%esp
 97c:	50                   	push   %eax
 97d:	ff 75 08             	pushl  0x8(%ebp)
 980:	e8 04 fe ff ff       	call   789 <putc>
 985:	83 c4 10             	add    $0x10,%esp
        ap++;
 988:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 98c:	eb 42                	jmp    9d0 <printf+0x170>
      } else if(c == '%'){
 98e:	83 7d e4 25          	cmpl   $0x25,-0x1c(%ebp)
 992:	75 17                	jne    9ab <printf+0x14b>
        putc(fd, c);
 994:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 997:	0f be c0             	movsbl %al,%eax
 99a:	83 ec 08             	sub    $0x8,%esp
 99d:	50                   	push   %eax
 99e:	ff 75 08             	pushl  0x8(%ebp)
 9a1:	e8 e3 fd ff ff       	call   789 <putc>
 9a6:	83 c4 10             	add    $0x10,%esp
 9a9:	eb 25                	jmp    9d0 <printf+0x170>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 9ab:	83 ec 08             	sub    $0x8,%esp
 9ae:	6a 25                	push   $0x25
 9b0:	ff 75 08             	pushl  0x8(%ebp)
 9b3:	e8 d1 fd ff ff       	call   789 <putc>
 9b8:	83 c4 10             	add    $0x10,%esp
        putc(fd, c);
 9bb:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 9be:	0f be c0             	movsbl %al,%eax
 9c1:	83 ec 08             	sub    $0x8,%esp
 9c4:	50                   	push   %eax
 9c5:	ff 75 08             	pushl  0x8(%ebp)
 9c8:	e8 bc fd ff ff       	call   789 <putc>
 9cd:	83 c4 10             	add    $0x10,%esp
      }
      state = 0;
 9d0:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
 9d7:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
 9db:	8b 55 0c             	mov    0xc(%ebp),%edx
 9de:	8b 45 f0             	mov    -0x10(%ebp),%eax
 9e1:	01 d0                	add    %edx,%eax
 9e3:	0f b6 00             	movzbl (%eax),%eax
 9e6:	84 c0                	test   %al,%al
 9e8:	0f 85 94 fe ff ff    	jne    882 <printf+0x22>
        putc(fd, c);
      }
      state = 0;
    }
  }
}
 9ee:	90                   	nop
 9ef:	c9                   	leave  
 9f0:	c3                   	ret    

000009f1 <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 9f1:	55                   	push   %ebp
 9f2:	89 e5                	mov    %esp,%ebp
 9f4:	83 ec 10             	sub    $0x10,%esp
  Header *bp, *p;

  bp = (Header*)ap - 1;
 9f7:	8b 45 08             	mov    0x8(%ebp),%eax
 9fa:	83 e8 08             	sub    $0x8,%eax
 9fd:	89 45 f8             	mov    %eax,-0x8(%ebp)
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 a00:	a1 9c 11 00 00       	mov    0x119c,%eax
 a05:	89 45 fc             	mov    %eax,-0x4(%ebp)
 a08:	eb 24                	jmp    a2e <free+0x3d>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 a0a:	8b 45 fc             	mov    -0x4(%ebp),%eax
 a0d:	8b 00                	mov    (%eax),%eax
 a0f:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 a12:	77 12                	ja     a26 <free+0x35>
 a14:	8b 45 f8             	mov    -0x8(%ebp),%eax
 a17:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 a1a:	77 24                	ja     a40 <free+0x4f>
 a1c:	8b 45 fc             	mov    -0x4(%ebp),%eax
 a1f:	8b 00                	mov    (%eax),%eax
 a21:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 a24:	77 1a                	ja     a40 <free+0x4f>
free(void *ap)
{
  Header *bp, *p;

  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 a26:	8b 45 fc             	mov    -0x4(%ebp),%eax
 a29:	8b 00                	mov    (%eax),%eax
 a2b:	89 45 fc             	mov    %eax,-0x4(%ebp)
 a2e:	8b 45 f8             	mov    -0x8(%ebp),%eax
 a31:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 a34:	76 d4                	jbe    a0a <free+0x19>
 a36:	8b 45 fc             	mov    -0x4(%ebp),%eax
 a39:	8b 00                	mov    (%eax),%eax
 a3b:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 a3e:	76 ca                	jbe    a0a <free+0x19>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if(bp + bp->s.size == p->s.ptr){
 a40:	8b 45 f8             	mov    -0x8(%ebp),%eax
 a43:	8b 40 04             	mov    0x4(%eax),%eax
 a46:	8d 14 c5 00 00 00 00 	lea    0x0(,%eax,8),%edx
 a4d:	8b 45 f8             	mov    -0x8(%ebp),%eax
 a50:	01 c2                	add    %eax,%edx
 a52:	8b 45 fc             	mov    -0x4(%ebp),%eax
 a55:	8b 00                	mov    (%eax),%eax
 a57:	39 c2                	cmp    %eax,%edx
 a59:	75 24                	jne    a7f <free+0x8e>
    bp->s.size += p->s.ptr->s.size;
 a5b:	8b 45 f8             	mov    -0x8(%ebp),%eax
 a5e:	8b 50 04             	mov    0x4(%eax),%edx
 a61:	8b 45 fc             	mov    -0x4(%ebp),%eax
 a64:	8b 00                	mov    (%eax),%eax
 a66:	8b 40 04             	mov    0x4(%eax),%eax
 a69:	01 c2                	add    %eax,%edx
 a6b:	8b 45 f8             	mov    -0x8(%ebp),%eax
 a6e:	89 50 04             	mov    %edx,0x4(%eax)
    bp->s.ptr = p->s.ptr->s.ptr;
 a71:	8b 45 fc             	mov    -0x4(%ebp),%eax
 a74:	8b 00                	mov    (%eax),%eax
 a76:	8b 10                	mov    (%eax),%edx
 a78:	8b 45 f8             	mov    -0x8(%ebp),%eax
 a7b:	89 10                	mov    %edx,(%eax)
 a7d:	eb 0a                	jmp    a89 <free+0x98>
  } else
    bp->s.ptr = p->s.ptr;
 a7f:	8b 45 fc             	mov    -0x4(%ebp),%eax
 a82:	8b 10                	mov    (%eax),%edx
 a84:	8b 45 f8             	mov    -0x8(%ebp),%eax
 a87:	89 10                	mov    %edx,(%eax)
  if(p + p->s.size == bp){
 a89:	8b 45 fc             	mov    -0x4(%ebp),%eax
 a8c:	8b 40 04             	mov    0x4(%eax),%eax
 a8f:	8d 14 c5 00 00 00 00 	lea    0x0(,%eax,8),%edx
 a96:	8b 45 fc             	mov    -0x4(%ebp),%eax
 a99:	01 d0                	add    %edx,%eax
 a9b:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 a9e:	75 20                	jne    ac0 <free+0xcf>
    p->s.size += bp->s.size;
 aa0:	8b 45 fc             	mov    -0x4(%ebp),%eax
 aa3:	8b 50 04             	mov    0x4(%eax),%edx
 aa6:	8b 45 f8             	mov    -0x8(%ebp),%eax
 aa9:	8b 40 04             	mov    0x4(%eax),%eax
 aac:	01 c2                	add    %eax,%edx
 aae:	8b 45 fc             	mov    -0x4(%ebp),%eax
 ab1:	89 50 04             	mov    %edx,0x4(%eax)
    p->s.ptr = bp->s.ptr;
 ab4:	8b 45 f8             	mov    -0x8(%ebp),%eax
 ab7:	8b 10                	mov    (%eax),%edx
 ab9:	8b 45 fc             	mov    -0x4(%ebp),%eax
 abc:	89 10                	mov    %edx,(%eax)
 abe:	eb 08                	jmp    ac8 <free+0xd7>
  } else
    p->s.ptr = bp;
 ac0:	8b 45 fc             	mov    -0x4(%ebp),%eax
 ac3:	8b 55 f8             	mov    -0x8(%ebp),%edx
 ac6:	89 10                	mov    %edx,(%eax)
  freep = p;
 ac8:	8b 45 fc             	mov    -0x4(%ebp),%eax
 acb:	a3 9c 11 00 00       	mov    %eax,0x119c
}
 ad0:	90                   	nop
 ad1:	c9                   	leave  
 ad2:	c3                   	ret    

00000ad3 <morecore>:

static Header*
morecore(uint nu)
{
 ad3:	55                   	push   %ebp
 ad4:	89 e5                	mov    %esp,%ebp
 ad6:	83 ec 18             	sub    $0x18,%esp
  char *p;
  Header *hp;

  if(nu < 4096)
 ad9:	81 7d 08 ff 0f 00 00 	cmpl   $0xfff,0x8(%ebp)
 ae0:	77 07                	ja     ae9 <morecore+0x16>
    nu = 4096;
 ae2:	c7 45 08 00 10 00 00 	movl   $0x1000,0x8(%ebp)
  p = sbrk(nu * sizeof(Header));
 ae9:	8b 45 08             	mov    0x8(%ebp),%eax
 aec:	c1 e0 03             	shl    $0x3,%eax
 aef:	83 ec 0c             	sub    $0xc,%esp
 af2:	50                   	push   %eax
 af3:	e8 31 fc ff ff       	call   729 <sbrk>
 af8:	83 c4 10             	add    $0x10,%esp
 afb:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(p == (char*)-1)
 afe:	83 7d f4 ff          	cmpl   $0xffffffff,-0xc(%ebp)
 b02:	75 07                	jne    b0b <morecore+0x38>
    return 0;
 b04:	b8 00 00 00 00       	mov    $0x0,%eax
 b09:	eb 26                	jmp    b31 <morecore+0x5e>
  hp = (Header*)p;
 b0b:	8b 45 f4             	mov    -0xc(%ebp),%eax
 b0e:	89 45 f0             	mov    %eax,-0x10(%ebp)
  hp->s.size = nu;
 b11:	8b 45 f0             	mov    -0x10(%ebp),%eax
 b14:	8b 55 08             	mov    0x8(%ebp),%edx
 b17:	89 50 04             	mov    %edx,0x4(%eax)
  free((void*)(hp + 1));
 b1a:	8b 45 f0             	mov    -0x10(%ebp),%eax
 b1d:	83 c0 08             	add    $0x8,%eax
 b20:	83 ec 0c             	sub    $0xc,%esp
 b23:	50                   	push   %eax
 b24:	e8 c8 fe ff ff       	call   9f1 <free>
 b29:	83 c4 10             	add    $0x10,%esp
  return freep;
 b2c:	a1 9c 11 00 00       	mov    0x119c,%eax
}
 b31:	c9                   	leave  
 b32:	c3                   	ret    

00000b33 <malloc>:

void*
malloc(uint nbytes)
{
 b33:	55                   	push   %ebp
 b34:	89 e5                	mov    %esp,%ebp
 b36:	83 ec 18             	sub    $0x18,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 b39:	8b 45 08             	mov    0x8(%ebp),%eax
 b3c:	83 c0 07             	add    $0x7,%eax
 b3f:	c1 e8 03             	shr    $0x3,%eax
 b42:	83 c0 01             	add    $0x1,%eax
 b45:	89 45 ec             	mov    %eax,-0x14(%ebp)
  if((prevp = freep) == 0){
 b48:	a1 9c 11 00 00       	mov    0x119c,%eax
 b4d:	89 45 f0             	mov    %eax,-0x10(%ebp)
 b50:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 b54:	75 23                	jne    b79 <malloc+0x46>
    base.s.ptr = freep = prevp = &base;
 b56:	c7 45 f0 94 11 00 00 	movl   $0x1194,-0x10(%ebp)
 b5d:	8b 45 f0             	mov    -0x10(%ebp),%eax
 b60:	a3 9c 11 00 00       	mov    %eax,0x119c
 b65:	a1 9c 11 00 00       	mov    0x119c,%eax
 b6a:	a3 94 11 00 00       	mov    %eax,0x1194
    base.s.size = 0;
 b6f:	c7 05 98 11 00 00 00 	movl   $0x0,0x1198
 b76:	00 00 00 
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 b79:	8b 45 f0             	mov    -0x10(%ebp),%eax
 b7c:	8b 00                	mov    (%eax),%eax
 b7e:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if(p->s.size >= nunits){
 b81:	8b 45 f4             	mov    -0xc(%ebp),%eax
 b84:	8b 40 04             	mov    0x4(%eax),%eax
 b87:	3b 45 ec             	cmp    -0x14(%ebp),%eax
 b8a:	72 4d                	jb     bd9 <malloc+0xa6>
      if(p->s.size == nunits)
 b8c:	8b 45 f4             	mov    -0xc(%ebp),%eax
 b8f:	8b 40 04             	mov    0x4(%eax),%eax
 b92:	3b 45 ec             	cmp    -0x14(%ebp),%eax
 b95:	75 0c                	jne    ba3 <malloc+0x70>
        prevp->s.ptr = p->s.ptr;
 b97:	8b 45 f4             	mov    -0xc(%ebp),%eax
 b9a:	8b 10                	mov    (%eax),%edx
 b9c:	8b 45 f0             	mov    -0x10(%ebp),%eax
 b9f:	89 10                	mov    %edx,(%eax)
 ba1:	eb 26                	jmp    bc9 <malloc+0x96>
      else {
        p->s.size -= nunits;
 ba3:	8b 45 f4             	mov    -0xc(%ebp),%eax
 ba6:	8b 40 04             	mov    0x4(%eax),%eax
 ba9:	2b 45 ec             	sub    -0x14(%ebp),%eax
 bac:	89 c2                	mov    %eax,%edx
 bae:	8b 45 f4             	mov    -0xc(%ebp),%eax
 bb1:	89 50 04             	mov    %edx,0x4(%eax)
        p += p->s.size;
 bb4:	8b 45 f4             	mov    -0xc(%ebp),%eax
 bb7:	8b 40 04             	mov    0x4(%eax),%eax
 bba:	c1 e0 03             	shl    $0x3,%eax
 bbd:	01 45 f4             	add    %eax,-0xc(%ebp)
        p->s.size = nunits;
 bc0:	8b 45 f4             	mov    -0xc(%ebp),%eax
 bc3:	8b 55 ec             	mov    -0x14(%ebp),%edx
 bc6:	89 50 04             	mov    %edx,0x4(%eax)
      }
      freep = prevp;
 bc9:	8b 45 f0             	mov    -0x10(%ebp),%eax
 bcc:	a3 9c 11 00 00       	mov    %eax,0x119c
      return (void*)(p + 1);
 bd1:	8b 45 f4             	mov    -0xc(%ebp),%eax
 bd4:	83 c0 08             	add    $0x8,%eax
 bd7:	eb 3b                	jmp    c14 <malloc+0xe1>
    }
    if(p == freep)
 bd9:	a1 9c 11 00 00       	mov    0x119c,%eax
 bde:	39 45 f4             	cmp    %eax,-0xc(%ebp)
 be1:	75 1e                	jne    c01 <malloc+0xce>
      if((p = morecore(nunits)) == 0)
 be3:	83 ec 0c             	sub    $0xc,%esp
 be6:	ff 75 ec             	pushl  -0x14(%ebp)
 be9:	e8 e5 fe ff ff       	call   ad3 <morecore>
 bee:	83 c4 10             	add    $0x10,%esp
 bf1:	89 45 f4             	mov    %eax,-0xc(%ebp)
 bf4:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 bf8:	75 07                	jne    c01 <malloc+0xce>
        return 0;
 bfa:	b8 00 00 00 00       	mov    $0x0,%eax
 bff:	eb 13                	jmp    c14 <malloc+0xe1>
  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
  if((prevp = freep) == 0){
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 c01:	8b 45 f4             	mov    -0xc(%ebp),%eax
 c04:	89 45 f0             	mov    %eax,-0x10(%ebp)
 c07:	8b 45 f4             	mov    -0xc(%ebp),%eax
 c0a:	8b 00                	mov    (%eax),%eax
 c0c:	89 45 f4             	mov    %eax,-0xc(%ebp)
      return (void*)(p + 1);
    }
    if(p == freep)
      if((p = morecore(nunits)) == 0)
        return 0;
  }
 c0f:	e9 6d ff ff ff       	jmp    b81 <malloc+0x4e>
}
 c14:	c9                   	leave  
 c15:	c3                   	ret    
