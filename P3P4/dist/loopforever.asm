
_loopforever:     file format elf32-i386


Disassembly of section .text:

00000000 <main>:
#include "types.h"
#include "user.h"
#define TPS 100
int
main(void)
{
   0:	8d 4c 24 04          	lea    0x4(%esp),%ecx
   4:	83 e4 f0             	and    $0xfffffff0,%esp
   7:	ff 71 fc             	pushl  -0x4(%ecx)
   a:	55                   	push   %ebp
   b:	89 e5                	mov    %esp,%ebp
   d:	51                   	push   %ecx
   e:	83 ec 14             	sub    $0x14,%esp
  int pid, max = 7;
  11:	c7 45 ec 07 00 00 00 	movl   $0x7,-0x14(%ebp)
  unsigned long x = 0;
  18:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

  for (int i=0; i<max; i++) {
  1f:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  26:	eb 59                	jmp    81 <main+0x81>
    sleep(5*TPS);  // pause before each child starts
  28:	83 ec 0c             	sub    $0xc,%esp
  2b:	68 f4 01 00 00       	push   $0x1f4
  30:	e8 b5 03 00 00       	call   3ea <sleep>
  35:	83 c4 10             	add    $0x10,%esp
    pid = fork();
  38:	e8 15 03 00 00       	call   352 <fork>
  3d:	89 45 e8             	mov    %eax,-0x18(%ebp)
    if (pid < 0) {
  40:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
  44:	79 17                	jns    5d <main+0x5d>
      printf(2, "fork failed!\n");
  46:	83 ec 08             	sub    $0x8,%esp
  49:	68 cf 08 00 00       	push   $0x8cf
  4e:	6a 02                	push   $0x2
  50:	e8 c4 04 00 00       	call   519 <printf>
  55:	83 c4 10             	add    $0x10,%esp
      exit();
  58:	e8 fd 02 00 00       	call   35a <exit>
    }

    if (pid == 0) { // child
  5d:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
  61:	75 1a                	jne    7d <main+0x7d>
      sleep(getpid()*100); // stagger start
  63:	e8 72 03 00 00       	call   3da <getpid>
  68:	6b c0 64             	imul   $0x64,%eax,%eax
  6b:	83 ec 0c             	sub    $0xc,%esp
  6e:	50                   	push   %eax
  6f:	e8 76 03 00 00       	call   3ea <sleep>
  74:	83 c4 10             	add    $0x10,%esp
      do {
	x += 1;
  77:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
      } while (1);
  7b:	eb fa                	jmp    77 <main+0x77>
main(void)
{
  int pid, max = 7;
  unsigned long x = 0;

  for (int i=0; i<max; i++) {
  7d:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
  81:	8b 45 f0             	mov    -0x10(%ebp),%eax
  84:	3b 45 ec             	cmp    -0x14(%ebp),%eax
  87:	7c 9f                	jl     28 <main+0x28>
      printf(1, "Child %d exiting\n", getpid());
      exit();
    }
  }

  pid = fork();
  89:	e8 c4 02 00 00       	call   352 <fork>
  8e:	89 45 e8             	mov    %eax,-0x18(%ebp)
  if (pid == 0) {
  91:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
  95:	75 13                	jne    aa <main+0xaa>
    sleep(20);
  97:	83 ec 0c             	sub    $0xc,%esp
  9a:	6a 14                	push   $0x14
  9c:	e8 49 03 00 00       	call   3ea <sleep>
  a1:	83 c4 10             	add    $0x10,%esp
    do {
      x = x+1;
  a4:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
    } while (1);
  a8:	eb fa                	jmp    a4 <main+0xa4>
  }

  sleep(15*TPS);
  aa:	83 ec 0c             	sub    $0xc,%esp
  ad:	68 dc 05 00 00       	push   $0x5dc
  b2:	e8 33 03 00 00       	call   3ea <sleep>
  b7:	83 c4 10             	add    $0x10,%esp
  wait();
  ba:	e8 a3 02 00 00       	call   362 <wait>
  printf(1, "Parent exiting\n");
  bf:	83 ec 08             	sub    $0x8,%esp
  c2:	68 dd 08 00 00       	push   $0x8dd
  c7:	6a 01                	push   $0x1
  c9:	e8 4b 04 00 00       	call   519 <printf>
  ce:	83 c4 10             	add    $0x10,%esp
  exit();
  d1:	e8 84 02 00 00       	call   35a <exit>

000000d6 <stosb>:
               "cc");
}

static inline void
stosb(void *addr, int data, int cnt)
{
  d6:	55                   	push   %ebp
  d7:	89 e5                	mov    %esp,%ebp
  d9:	57                   	push   %edi
  da:	53                   	push   %ebx
  asm volatile("cld; rep stosb" :
  db:	8b 4d 08             	mov    0x8(%ebp),%ecx
  de:	8b 55 10             	mov    0x10(%ebp),%edx
  e1:	8b 45 0c             	mov    0xc(%ebp),%eax
  e4:	89 cb                	mov    %ecx,%ebx
  e6:	89 df                	mov    %ebx,%edi
  e8:	89 d1                	mov    %edx,%ecx
  ea:	fc                   	cld    
  eb:	f3 aa                	rep stos %al,%es:(%edi)
  ed:	89 ca                	mov    %ecx,%edx
  ef:	89 fb                	mov    %edi,%ebx
  f1:	89 5d 08             	mov    %ebx,0x8(%ebp)
  f4:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
  f7:	90                   	nop
  f8:	5b                   	pop    %ebx
  f9:	5f                   	pop    %edi
  fa:	5d                   	pop    %ebp
  fb:	c3                   	ret    

000000fc <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, char *t)
{
  fc:	55                   	push   %ebp
  fd:	89 e5                	mov    %esp,%ebp
  ff:	83 ec 10             	sub    $0x10,%esp
  char *os;

  os = s;
 102:	8b 45 08             	mov    0x8(%ebp),%eax
 105:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while((*s++ = *t++) != 0)
 108:	90                   	nop
 109:	8b 45 08             	mov    0x8(%ebp),%eax
 10c:	8d 50 01             	lea    0x1(%eax),%edx
 10f:	89 55 08             	mov    %edx,0x8(%ebp)
 112:	8b 55 0c             	mov    0xc(%ebp),%edx
 115:	8d 4a 01             	lea    0x1(%edx),%ecx
 118:	89 4d 0c             	mov    %ecx,0xc(%ebp)
 11b:	0f b6 12             	movzbl (%edx),%edx
 11e:	88 10                	mov    %dl,(%eax)
 120:	0f b6 00             	movzbl (%eax),%eax
 123:	84 c0                	test   %al,%al
 125:	75 e2                	jne    109 <strcpy+0xd>
    ;
  return os;
 127:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 12a:	c9                   	leave  
 12b:	c3                   	ret    

0000012c <strcmp>:

int
strcmp(const char *p, const char *q)
{
 12c:	55                   	push   %ebp
 12d:	89 e5                	mov    %esp,%ebp
  while(*p && *p == *q)
 12f:	eb 08                	jmp    139 <strcmp+0xd>
    p++, q++;
 131:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 135:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
  while(*p && *p == *q)
 139:	8b 45 08             	mov    0x8(%ebp),%eax
 13c:	0f b6 00             	movzbl (%eax),%eax
 13f:	84 c0                	test   %al,%al
 141:	74 10                	je     153 <strcmp+0x27>
 143:	8b 45 08             	mov    0x8(%ebp),%eax
 146:	0f b6 10             	movzbl (%eax),%edx
 149:	8b 45 0c             	mov    0xc(%ebp),%eax
 14c:	0f b6 00             	movzbl (%eax),%eax
 14f:	38 c2                	cmp    %al,%dl
 151:	74 de                	je     131 <strcmp+0x5>
    p++, q++;
  return (uchar)*p - (uchar)*q;
 153:	8b 45 08             	mov    0x8(%ebp),%eax
 156:	0f b6 00             	movzbl (%eax),%eax
 159:	0f b6 d0             	movzbl %al,%edx
 15c:	8b 45 0c             	mov    0xc(%ebp),%eax
 15f:	0f b6 00             	movzbl (%eax),%eax
 162:	0f b6 c0             	movzbl %al,%eax
 165:	29 c2                	sub    %eax,%edx
 167:	89 d0                	mov    %edx,%eax
}
 169:	5d                   	pop    %ebp
 16a:	c3                   	ret    

0000016b <strlen>:

uint
strlen(char *s)
{
 16b:	55                   	push   %ebp
 16c:	89 e5                	mov    %esp,%ebp
 16e:	83 ec 10             	sub    $0x10,%esp
  int n;

  for(n = 0; s[n]; n++)
 171:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
 178:	eb 04                	jmp    17e <strlen+0x13>
 17a:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
 17e:	8b 55 fc             	mov    -0x4(%ebp),%edx
 181:	8b 45 08             	mov    0x8(%ebp),%eax
 184:	01 d0                	add    %edx,%eax
 186:	0f b6 00             	movzbl (%eax),%eax
 189:	84 c0                	test   %al,%al
 18b:	75 ed                	jne    17a <strlen+0xf>
    ;
  return n;
 18d:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 190:	c9                   	leave  
 191:	c3                   	ret    

00000192 <memset>:

void*
memset(void *dst, int c, uint n)
{
 192:	55                   	push   %ebp
 193:	89 e5                	mov    %esp,%ebp
  stosb(dst, c, n);
 195:	8b 45 10             	mov    0x10(%ebp),%eax
 198:	50                   	push   %eax
 199:	ff 75 0c             	pushl  0xc(%ebp)
 19c:	ff 75 08             	pushl  0x8(%ebp)
 19f:	e8 32 ff ff ff       	call   d6 <stosb>
 1a4:	83 c4 0c             	add    $0xc,%esp
  return dst;
 1a7:	8b 45 08             	mov    0x8(%ebp),%eax
}
 1aa:	c9                   	leave  
 1ab:	c3                   	ret    

000001ac <strchr>:

char*
strchr(const char *s, char c)
{
 1ac:	55                   	push   %ebp
 1ad:	89 e5                	mov    %esp,%ebp
 1af:	83 ec 04             	sub    $0x4,%esp
 1b2:	8b 45 0c             	mov    0xc(%ebp),%eax
 1b5:	88 45 fc             	mov    %al,-0x4(%ebp)
  for(; *s; s++)
 1b8:	eb 14                	jmp    1ce <strchr+0x22>
    if(*s == c)
 1ba:	8b 45 08             	mov    0x8(%ebp),%eax
 1bd:	0f b6 00             	movzbl (%eax),%eax
 1c0:	3a 45 fc             	cmp    -0x4(%ebp),%al
 1c3:	75 05                	jne    1ca <strchr+0x1e>
      return (char*)s;
 1c5:	8b 45 08             	mov    0x8(%ebp),%eax
 1c8:	eb 13                	jmp    1dd <strchr+0x31>
}

char*
strchr(const char *s, char c)
{
  for(; *s; s++)
 1ca:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 1ce:	8b 45 08             	mov    0x8(%ebp),%eax
 1d1:	0f b6 00             	movzbl (%eax),%eax
 1d4:	84 c0                	test   %al,%al
 1d6:	75 e2                	jne    1ba <strchr+0xe>
    if(*s == c)
      return (char*)s;
  return 0;
 1d8:	b8 00 00 00 00       	mov    $0x0,%eax
}
 1dd:	c9                   	leave  
 1de:	c3                   	ret    

000001df <gets>:

char*
gets(char *buf, int max)
{
 1df:	55                   	push   %ebp
 1e0:	89 e5                	mov    %esp,%ebp
 1e2:	83 ec 18             	sub    $0x18,%esp
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 1e5:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 1ec:	eb 42                	jmp    230 <gets+0x51>
    cc = read(0, &c, 1);
 1ee:	83 ec 04             	sub    $0x4,%esp
 1f1:	6a 01                	push   $0x1
 1f3:	8d 45 ef             	lea    -0x11(%ebp),%eax
 1f6:	50                   	push   %eax
 1f7:	6a 00                	push   $0x0
 1f9:	e8 74 01 00 00       	call   372 <read>
 1fe:	83 c4 10             	add    $0x10,%esp
 201:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if(cc < 1)
 204:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 208:	7e 33                	jle    23d <gets+0x5e>
      break;
    buf[i++] = c;
 20a:	8b 45 f4             	mov    -0xc(%ebp),%eax
 20d:	8d 50 01             	lea    0x1(%eax),%edx
 210:	89 55 f4             	mov    %edx,-0xc(%ebp)
 213:	89 c2                	mov    %eax,%edx
 215:	8b 45 08             	mov    0x8(%ebp),%eax
 218:	01 c2                	add    %eax,%edx
 21a:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 21e:	88 02                	mov    %al,(%edx)
    if(c == '\n' || c == '\r')
 220:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 224:	3c 0a                	cmp    $0xa,%al
 226:	74 16                	je     23e <gets+0x5f>
 228:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 22c:	3c 0d                	cmp    $0xd,%al
 22e:	74 0e                	je     23e <gets+0x5f>
gets(char *buf, int max)
{
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 230:	8b 45 f4             	mov    -0xc(%ebp),%eax
 233:	83 c0 01             	add    $0x1,%eax
 236:	3b 45 0c             	cmp    0xc(%ebp),%eax
 239:	7c b3                	jl     1ee <gets+0xf>
 23b:	eb 01                	jmp    23e <gets+0x5f>
    cc = read(0, &c, 1);
    if(cc < 1)
      break;
 23d:	90                   	nop
    buf[i++] = c;
    if(c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 23e:	8b 55 f4             	mov    -0xc(%ebp),%edx
 241:	8b 45 08             	mov    0x8(%ebp),%eax
 244:	01 d0                	add    %edx,%eax
 246:	c6 00 00             	movb   $0x0,(%eax)
  return buf;
 249:	8b 45 08             	mov    0x8(%ebp),%eax
}
 24c:	c9                   	leave  
 24d:	c3                   	ret    

0000024e <stat>:

int
stat(char *n, struct stat *st)
{
 24e:	55                   	push   %ebp
 24f:	89 e5                	mov    %esp,%ebp
 251:	83 ec 18             	sub    $0x18,%esp
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 254:	83 ec 08             	sub    $0x8,%esp
 257:	6a 00                	push   $0x0
 259:	ff 75 08             	pushl  0x8(%ebp)
 25c:	e8 39 01 00 00       	call   39a <open>
 261:	83 c4 10             	add    $0x10,%esp
 264:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(fd < 0)
 267:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 26b:	79 07                	jns    274 <stat+0x26>
    return -1;
 26d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 272:	eb 25                	jmp    299 <stat+0x4b>
  r = fstat(fd, st);
 274:	83 ec 08             	sub    $0x8,%esp
 277:	ff 75 0c             	pushl  0xc(%ebp)
 27a:	ff 75 f4             	pushl  -0xc(%ebp)
 27d:	e8 30 01 00 00       	call   3b2 <fstat>
 282:	83 c4 10             	add    $0x10,%esp
 285:	89 45 f0             	mov    %eax,-0x10(%ebp)
  close(fd);
 288:	83 ec 0c             	sub    $0xc,%esp
 28b:	ff 75 f4             	pushl  -0xc(%ebp)
 28e:	e8 ef 00 00 00       	call   382 <close>
 293:	83 c4 10             	add    $0x10,%esp
  return r;
 296:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
 299:	c9                   	leave  
 29a:	c3                   	ret    

0000029b <atoi>:

int
atoi(const char *s)
{
 29b:	55                   	push   %ebp
 29c:	89 e5                	mov    %esp,%ebp
 29e:	83 ec 10             	sub    $0x10,%esp
  int n, sign;

  n = 0;
 2a1:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  while(*s == ' ') s++;
 2a8:	eb 04                	jmp    2ae <atoi+0x13>
 2aa:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 2ae:	8b 45 08             	mov    0x8(%ebp),%eax
 2b1:	0f b6 00             	movzbl (%eax),%eax
 2b4:	3c 20                	cmp    $0x20,%al
 2b6:	74 f2                	je     2aa <atoi+0xf>
  sign = (*s == '-') ? -1 : 1;
 2b8:	8b 45 08             	mov    0x8(%ebp),%eax
 2bb:	0f b6 00             	movzbl (%eax),%eax
 2be:	3c 2d                	cmp    $0x2d,%al
 2c0:	75 07                	jne    2c9 <atoi+0x2e>
 2c2:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 2c7:	eb 05                	jmp    2ce <atoi+0x33>
 2c9:	b8 01 00 00 00       	mov    $0x1,%eax
 2ce:	89 45 f8             	mov    %eax,-0x8(%ebp)
  while('0' <= *s && *s <= '9')
 2d1:	eb 25                	jmp    2f8 <atoi+0x5d>
    n = n*10 + *s++ - '0';
 2d3:	8b 55 fc             	mov    -0x4(%ebp),%edx
 2d6:	89 d0                	mov    %edx,%eax
 2d8:	c1 e0 02             	shl    $0x2,%eax
 2db:	01 d0                	add    %edx,%eax
 2dd:	01 c0                	add    %eax,%eax
 2df:	89 c1                	mov    %eax,%ecx
 2e1:	8b 45 08             	mov    0x8(%ebp),%eax
 2e4:	8d 50 01             	lea    0x1(%eax),%edx
 2e7:	89 55 08             	mov    %edx,0x8(%ebp)
 2ea:	0f b6 00             	movzbl (%eax),%eax
 2ed:	0f be c0             	movsbl %al,%eax
 2f0:	01 c8                	add    %ecx,%eax
 2f2:	83 e8 30             	sub    $0x30,%eax
 2f5:	89 45 fc             	mov    %eax,-0x4(%ebp)
  int n, sign;

  n = 0;
  while(*s == ' ') s++;
  sign = (*s == '-') ? -1 : 1;
  while('0' <= *s && *s <= '9')
 2f8:	8b 45 08             	mov    0x8(%ebp),%eax
 2fb:	0f b6 00             	movzbl (%eax),%eax
 2fe:	3c 2f                	cmp    $0x2f,%al
 300:	7e 0a                	jle    30c <atoi+0x71>
 302:	8b 45 08             	mov    0x8(%ebp),%eax
 305:	0f b6 00             	movzbl (%eax),%eax
 308:	3c 39                	cmp    $0x39,%al
 30a:	7e c7                	jle    2d3 <atoi+0x38>
    n = n*10 + *s++ - '0';
  return sign*n;
 30c:	8b 45 f8             	mov    -0x8(%ebp),%eax
 30f:	0f af 45 fc          	imul   -0x4(%ebp),%eax
}
 313:	c9                   	leave  
 314:	c3                   	ret    

00000315 <memmove>:

void*
memmove(void *vdst, void *vsrc, int n)
{
 315:	55                   	push   %ebp
 316:	89 e5                	mov    %esp,%ebp
 318:	83 ec 10             	sub    $0x10,%esp
  char *dst, *src;
  
  dst = vdst;
 31b:	8b 45 08             	mov    0x8(%ebp),%eax
 31e:	89 45 fc             	mov    %eax,-0x4(%ebp)
  src = vsrc;
 321:	8b 45 0c             	mov    0xc(%ebp),%eax
 324:	89 45 f8             	mov    %eax,-0x8(%ebp)
  while(n-- > 0)
 327:	eb 17                	jmp    340 <memmove+0x2b>
    *dst++ = *src++;
 329:	8b 45 fc             	mov    -0x4(%ebp),%eax
 32c:	8d 50 01             	lea    0x1(%eax),%edx
 32f:	89 55 fc             	mov    %edx,-0x4(%ebp)
 332:	8b 55 f8             	mov    -0x8(%ebp),%edx
 335:	8d 4a 01             	lea    0x1(%edx),%ecx
 338:	89 4d f8             	mov    %ecx,-0x8(%ebp)
 33b:	0f b6 12             	movzbl (%edx),%edx
 33e:	88 10                	mov    %dl,(%eax)
{
  char *dst, *src;
  
  dst = vdst;
  src = vsrc;
  while(n-- > 0)
 340:	8b 45 10             	mov    0x10(%ebp),%eax
 343:	8d 50 ff             	lea    -0x1(%eax),%edx
 346:	89 55 10             	mov    %edx,0x10(%ebp)
 349:	85 c0                	test   %eax,%eax
 34b:	7f dc                	jg     329 <memmove+0x14>
    *dst++ = *src++;
  return vdst;
 34d:	8b 45 08             	mov    0x8(%ebp),%eax
}
 350:	c9                   	leave  
 351:	c3                   	ret    

00000352 <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
 352:	b8 01 00 00 00       	mov    $0x1,%eax
 357:	cd 40                	int    $0x40
 359:	c3                   	ret    

0000035a <exit>:
SYSCALL(exit)
 35a:	b8 02 00 00 00       	mov    $0x2,%eax
 35f:	cd 40                	int    $0x40
 361:	c3                   	ret    

00000362 <wait>:
SYSCALL(wait)
 362:	b8 03 00 00 00       	mov    $0x3,%eax
 367:	cd 40                	int    $0x40
 369:	c3                   	ret    

0000036a <pipe>:
SYSCALL(pipe)
 36a:	b8 04 00 00 00       	mov    $0x4,%eax
 36f:	cd 40                	int    $0x40
 371:	c3                   	ret    

00000372 <read>:
SYSCALL(read)
 372:	b8 05 00 00 00       	mov    $0x5,%eax
 377:	cd 40                	int    $0x40
 379:	c3                   	ret    

0000037a <write>:
SYSCALL(write)
 37a:	b8 10 00 00 00       	mov    $0x10,%eax
 37f:	cd 40                	int    $0x40
 381:	c3                   	ret    

00000382 <close>:
SYSCALL(close)
 382:	b8 15 00 00 00       	mov    $0x15,%eax
 387:	cd 40                	int    $0x40
 389:	c3                   	ret    

0000038a <kill>:
SYSCALL(kill)
 38a:	b8 06 00 00 00       	mov    $0x6,%eax
 38f:	cd 40                	int    $0x40
 391:	c3                   	ret    

00000392 <exec>:
SYSCALL(exec)
 392:	b8 07 00 00 00       	mov    $0x7,%eax
 397:	cd 40                	int    $0x40
 399:	c3                   	ret    

0000039a <open>:
SYSCALL(open)
 39a:	b8 0f 00 00 00       	mov    $0xf,%eax
 39f:	cd 40                	int    $0x40
 3a1:	c3                   	ret    

000003a2 <mknod>:
SYSCALL(mknod)
 3a2:	b8 11 00 00 00       	mov    $0x11,%eax
 3a7:	cd 40                	int    $0x40
 3a9:	c3                   	ret    

000003aa <unlink>:
SYSCALL(unlink)
 3aa:	b8 12 00 00 00       	mov    $0x12,%eax
 3af:	cd 40                	int    $0x40
 3b1:	c3                   	ret    

000003b2 <fstat>:
SYSCALL(fstat)
 3b2:	b8 08 00 00 00       	mov    $0x8,%eax
 3b7:	cd 40                	int    $0x40
 3b9:	c3                   	ret    

000003ba <link>:
SYSCALL(link)
 3ba:	b8 13 00 00 00       	mov    $0x13,%eax
 3bf:	cd 40                	int    $0x40
 3c1:	c3                   	ret    

000003c2 <mkdir>:
SYSCALL(mkdir)
 3c2:	b8 14 00 00 00       	mov    $0x14,%eax
 3c7:	cd 40                	int    $0x40
 3c9:	c3                   	ret    

000003ca <chdir>:
SYSCALL(chdir)
 3ca:	b8 09 00 00 00       	mov    $0x9,%eax
 3cf:	cd 40                	int    $0x40
 3d1:	c3                   	ret    

000003d2 <dup>:
SYSCALL(dup)
 3d2:	b8 0a 00 00 00       	mov    $0xa,%eax
 3d7:	cd 40                	int    $0x40
 3d9:	c3                   	ret    

000003da <getpid>:
SYSCALL(getpid)
 3da:	b8 0b 00 00 00       	mov    $0xb,%eax
 3df:	cd 40                	int    $0x40
 3e1:	c3                   	ret    

000003e2 <sbrk>:
SYSCALL(sbrk)
 3e2:	b8 0c 00 00 00       	mov    $0xc,%eax
 3e7:	cd 40                	int    $0x40
 3e9:	c3                   	ret    

000003ea <sleep>:
SYSCALL(sleep)
 3ea:	b8 0d 00 00 00       	mov    $0xd,%eax
 3ef:	cd 40                	int    $0x40
 3f1:	c3                   	ret    

000003f2 <uptime>:
SYSCALL(uptime)
 3f2:	b8 0e 00 00 00       	mov    $0xe,%eax
 3f7:	cd 40                	int    $0x40
 3f9:	c3                   	ret    

000003fa <halt>:
SYSCALL(halt)
 3fa:	b8 16 00 00 00       	mov    $0x16,%eax
 3ff:	cd 40                	int    $0x40
 401:	c3                   	ret    

00000402 <date>:
SYSCALL(date)    #added the date system call
 402:	b8 17 00 00 00       	mov    $0x17,%eax
 407:	cd 40                	int    $0x40
 409:	c3                   	ret    

0000040a <getuid>:
SYSCALL(getuid)
 40a:	b8 18 00 00 00       	mov    $0x18,%eax
 40f:	cd 40                	int    $0x40
 411:	c3                   	ret    

00000412 <getgid>:
SYSCALL(getgid)
 412:	b8 19 00 00 00       	mov    $0x19,%eax
 417:	cd 40                	int    $0x40
 419:	c3                   	ret    

0000041a <getppid>:
SYSCALL(getppid)
 41a:	b8 1a 00 00 00       	mov    $0x1a,%eax
 41f:	cd 40                	int    $0x40
 421:	c3                   	ret    

00000422 <setuid>:
SYSCALL(setuid)
 422:	b8 1b 00 00 00       	mov    $0x1b,%eax
 427:	cd 40                	int    $0x40
 429:	c3                   	ret    

0000042a <setgid>:
SYSCALL(setgid)
 42a:	b8 1c 00 00 00       	mov    $0x1c,%eax
 42f:	cd 40                	int    $0x40
 431:	c3                   	ret    

00000432 <getprocs>:
SYSCALL(getprocs)
 432:	b8 1d 00 00 00       	mov    $0x1d,%eax
 437:	cd 40                	int    $0x40
 439:	c3                   	ret    

0000043a <setpriority>:
SYSCALL(setpriority)
 43a:	b8 1e 00 00 00       	mov    $0x1e,%eax
 43f:	cd 40                	int    $0x40
 441:	c3                   	ret    

00000442 <putc>:
#include "stat.h"
#include "user.h"

static void
putc(int fd, char c)
{
 442:	55                   	push   %ebp
 443:	89 e5                	mov    %esp,%ebp
 445:	83 ec 18             	sub    $0x18,%esp
 448:	8b 45 0c             	mov    0xc(%ebp),%eax
 44b:	88 45 f4             	mov    %al,-0xc(%ebp)
  write(fd, &c, 1);
 44e:	83 ec 04             	sub    $0x4,%esp
 451:	6a 01                	push   $0x1
 453:	8d 45 f4             	lea    -0xc(%ebp),%eax
 456:	50                   	push   %eax
 457:	ff 75 08             	pushl  0x8(%ebp)
 45a:	e8 1b ff ff ff       	call   37a <write>
 45f:	83 c4 10             	add    $0x10,%esp
}
 462:	90                   	nop
 463:	c9                   	leave  
 464:	c3                   	ret    

00000465 <printint>:

static void
printint(int fd, int xx, int base, int sgn)
{
 465:	55                   	push   %ebp
 466:	89 e5                	mov    %esp,%ebp
 468:	53                   	push   %ebx
 469:	83 ec 24             	sub    $0x24,%esp
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 46c:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  if(sgn && xx < 0){
 473:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
 477:	74 17                	je     490 <printint+0x2b>
 479:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
 47d:	79 11                	jns    490 <printint+0x2b>
    neg = 1;
 47f:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
    x = -xx;
 486:	8b 45 0c             	mov    0xc(%ebp),%eax
 489:	f7 d8                	neg    %eax
 48b:	89 45 ec             	mov    %eax,-0x14(%ebp)
 48e:	eb 06                	jmp    496 <printint+0x31>
  } else {
    x = xx;
 490:	8b 45 0c             	mov    0xc(%ebp),%eax
 493:	89 45 ec             	mov    %eax,-0x14(%ebp)
  }

  i = 0;
 496:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  do{
    buf[i++] = digits[x % base];
 49d:	8b 4d f4             	mov    -0xc(%ebp),%ecx
 4a0:	8d 41 01             	lea    0x1(%ecx),%eax
 4a3:	89 45 f4             	mov    %eax,-0xc(%ebp)
 4a6:	8b 5d 10             	mov    0x10(%ebp),%ebx
 4a9:	8b 45 ec             	mov    -0x14(%ebp),%eax
 4ac:	ba 00 00 00 00       	mov    $0x0,%edx
 4b1:	f7 f3                	div    %ebx
 4b3:	89 d0                	mov    %edx,%eax
 4b5:	0f b6 80 3c 0b 00 00 	movzbl 0xb3c(%eax),%eax
 4bc:	88 44 0d dc          	mov    %al,-0x24(%ebp,%ecx,1)
  }while((x /= base) != 0);
 4c0:	8b 5d 10             	mov    0x10(%ebp),%ebx
 4c3:	8b 45 ec             	mov    -0x14(%ebp),%eax
 4c6:	ba 00 00 00 00       	mov    $0x0,%edx
 4cb:	f7 f3                	div    %ebx
 4cd:	89 45 ec             	mov    %eax,-0x14(%ebp)
 4d0:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 4d4:	75 c7                	jne    49d <printint+0x38>
  if(neg)
 4d6:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 4da:	74 2d                	je     509 <printint+0xa4>
    buf[i++] = '-';
 4dc:	8b 45 f4             	mov    -0xc(%ebp),%eax
 4df:	8d 50 01             	lea    0x1(%eax),%edx
 4e2:	89 55 f4             	mov    %edx,-0xc(%ebp)
 4e5:	c6 44 05 dc 2d       	movb   $0x2d,-0x24(%ebp,%eax,1)

  while(--i >= 0)
 4ea:	eb 1d                	jmp    509 <printint+0xa4>
    putc(fd, buf[i]);
 4ec:	8d 55 dc             	lea    -0x24(%ebp),%edx
 4ef:	8b 45 f4             	mov    -0xc(%ebp),%eax
 4f2:	01 d0                	add    %edx,%eax
 4f4:	0f b6 00             	movzbl (%eax),%eax
 4f7:	0f be c0             	movsbl %al,%eax
 4fa:	83 ec 08             	sub    $0x8,%esp
 4fd:	50                   	push   %eax
 4fe:	ff 75 08             	pushl  0x8(%ebp)
 501:	e8 3c ff ff ff       	call   442 <putc>
 506:	83 c4 10             	add    $0x10,%esp
    buf[i++] = digits[x % base];
  }while((x /= base) != 0);
  if(neg)
    buf[i++] = '-';

  while(--i >= 0)
 509:	83 6d f4 01          	subl   $0x1,-0xc(%ebp)
 50d:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 511:	79 d9                	jns    4ec <printint+0x87>
    putc(fd, buf[i]);
}
 513:	90                   	nop
 514:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 517:	c9                   	leave  
 518:	c3                   	ret    

00000519 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, char *fmt, ...)
{
 519:	55                   	push   %ebp
 51a:	89 e5                	mov    %esp,%ebp
 51c:	83 ec 28             	sub    $0x28,%esp
  char *s;
  int c, i, state;
  uint *ap;

  state = 0;
 51f:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  ap = (uint*)(void*)&fmt + 1;
 526:	8d 45 0c             	lea    0xc(%ebp),%eax
 529:	83 c0 04             	add    $0x4,%eax
 52c:	89 45 e8             	mov    %eax,-0x18(%ebp)
  for(i = 0; fmt[i]; i++){
 52f:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 536:	e9 59 01 00 00       	jmp    694 <printf+0x17b>
    c = fmt[i] & 0xff;
 53b:	8b 55 0c             	mov    0xc(%ebp),%edx
 53e:	8b 45 f0             	mov    -0x10(%ebp),%eax
 541:	01 d0                	add    %edx,%eax
 543:	0f b6 00             	movzbl (%eax),%eax
 546:	0f be c0             	movsbl %al,%eax
 549:	25 ff 00 00 00       	and    $0xff,%eax
 54e:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    if(state == 0){
 551:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 555:	75 2c                	jne    583 <printf+0x6a>
      if(c == '%'){
 557:	83 7d e4 25          	cmpl   $0x25,-0x1c(%ebp)
 55b:	75 0c                	jne    569 <printf+0x50>
        state = '%';
 55d:	c7 45 ec 25 00 00 00 	movl   $0x25,-0x14(%ebp)
 564:	e9 27 01 00 00       	jmp    690 <printf+0x177>
      } else {
        putc(fd, c);
 569:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 56c:	0f be c0             	movsbl %al,%eax
 56f:	83 ec 08             	sub    $0x8,%esp
 572:	50                   	push   %eax
 573:	ff 75 08             	pushl  0x8(%ebp)
 576:	e8 c7 fe ff ff       	call   442 <putc>
 57b:	83 c4 10             	add    $0x10,%esp
 57e:	e9 0d 01 00 00       	jmp    690 <printf+0x177>
      }
    } else if(state == '%'){
 583:	83 7d ec 25          	cmpl   $0x25,-0x14(%ebp)
 587:	0f 85 03 01 00 00    	jne    690 <printf+0x177>
      if(c == 'd'){
 58d:	83 7d e4 64          	cmpl   $0x64,-0x1c(%ebp)
 591:	75 1e                	jne    5b1 <printf+0x98>
        printint(fd, *ap, 10, 1);
 593:	8b 45 e8             	mov    -0x18(%ebp),%eax
 596:	8b 00                	mov    (%eax),%eax
 598:	6a 01                	push   $0x1
 59a:	6a 0a                	push   $0xa
 59c:	50                   	push   %eax
 59d:	ff 75 08             	pushl  0x8(%ebp)
 5a0:	e8 c0 fe ff ff       	call   465 <printint>
 5a5:	83 c4 10             	add    $0x10,%esp
        ap++;
 5a8:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 5ac:	e9 d8 00 00 00       	jmp    689 <printf+0x170>
      } else if(c == 'x' || c == 'p'){
 5b1:	83 7d e4 78          	cmpl   $0x78,-0x1c(%ebp)
 5b5:	74 06                	je     5bd <printf+0xa4>
 5b7:	83 7d e4 70          	cmpl   $0x70,-0x1c(%ebp)
 5bb:	75 1e                	jne    5db <printf+0xc2>
        printint(fd, *ap, 16, 0);
 5bd:	8b 45 e8             	mov    -0x18(%ebp),%eax
 5c0:	8b 00                	mov    (%eax),%eax
 5c2:	6a 00                	push   $0x0
 5c4:	6a 10                	push   $0x10
 5c6:	50                   	push   %eax
 5c7:	ff 75 08             	pushl  0x8(%ebp)
 5ca:	e8 96 fe ff ff       	call   465 <printint>
 5cf:	83 c4 10             	add    $0x10,%esp
        ap++;
 5d2:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 5d6:	e9 ae 00 00 00       	jmp    689 <printf+0x170>
      } else if(c == 's'){
 5db:	83 7d e4 73          	cmpl   $0x73,-0x1c(%ebp)
 5df:	75 43                	jne    624 <printf+0x10b>
        s = (char*)*ap;
 5e1:	8b 45 e8             	mov    -0x18(%ebp),%eax
 5e4:	8b 00                	mov    (%eax),%eax
 5e6:	89 45 f4             	mov    %eax,-0xc(%ebp)
        ap++;
 5e9:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
        if(s == 0)
 5ed:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 5f1:	75 25                	jne    618 <printf+0xff>
          s = "(null)";
 5f3:	c7 45 f4 ed 08 00 00 	movl   $0x8ed,-0xc(%ebp)
        while(*s != 0){
 5fa:	eb 1c                	jmp    618 <printf+0xff>
          putc(fd, *s);
 5fc:	8b 45 f4             	mov    -0xc(%ebp),%eax
 5ff:	0f b6 00             	movzbl (%eax),%eax
 602:	0f be c0             	movsbl %al,%eax
 605:	83 ec 08             	sub    $0x8,%esp
 608:	50                   	push   %eax
 609:	ff 75 08             	pushl  0x8(%ebp)
 60c:	e8 31 fe ff ff       	call   442 <putc>
 611:	83 c4 10             	add    $0x10,%esp
          s++;
 614:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
      } else if(c == 's'){
        s = (char*)*ap;
        ap++;
        if(s == 0)
          s = "(null)";
        while(*s != 0){
 618:	8b 45 f4             	mov    -0xc(%ebp),%eax
 61b:	0f b6 00             	movzbl (%eax),%eax
 61e:	84 c0                	test   %al,%al
 620:	75 da                	jne    5fc <printf+0xe3>
 622:	eb 65                	jmp    689 <printf+0x170>
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 624:	83 7d e4 63          	cmpl   $0x63,-0x1c(%ebp)
 628:	75 1d                	jne    647 <printf+0x12e>
        putc(fd, *ap);
 62a:	8b 45 e8             	mov    -0x18(%ebp),%eax
 62d:	8b 00                	mov    (%eax),%eax
 62f:	0f be c0             	movsbl %al,%eax
 632:	83 ec 08             	sub    $0x8,%esp
 635:	50                   	push   %eax
 636:	ff 75 08             	pushl  0x8(%ebp)
 639:	e8 04 fe ff ff       	call   442 <putc>
 63e:	83 c4 10             	add    $0x10,%esp
        ap++;
 641:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 645:	eb 42                	jmp    689 <printf+0x170>
      } else if(c == '%'){
 647:	83 7d e4 25          	cmpl   $0x25,-0x1c(%ebp)
 64b:	75 17                	jne    664 <printf+0x14b>
        putc(fd, c);
 64d:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 650:	0f be c0             	movsbl %al,%eax
 653:	83 ec 08             	sub    $0x8,%esp
 656:	50                   	push   %eax
 657:	ff 75 08             	pushl  0x8(%ebp)
 65a:	e8 e3 fd ff ff       	call   442 <putc>
 65f:	83 c4 10             	add    $0x10,%esp
 662:	eb 25                	jmp    689 <printf+0x170>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 664:	83 ec 08             	sub    $0x8,%esp
 667:	6a 25                	push   $0x25
 669:	ff 75 08             	pushl  0x8(%ebp)
 66c:	e8 d1 fd ff ff       	call   442 <putc>
 671:	83 c4 10             	add    $0x10,%esp
        putc(fd, c);
 674:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 677:	0f be c0             	movsbl %al,%eax
 67a:	83 ec 08             	sub    $0x8,%esp
 67d:	50                   	push   %eax
 67e:	ff 75 08             	pushl  0x8(%ebp)
 681:	e8 bc fd ff ff       	call   442 <putc>
 686:	83 c4 10             	add    $0x10,%esp
      }
      state = 0;
 689:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
 690:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
 694:	8b 55 0c             	mov    0xc(%ebp),%edx
 697:	8b 45 f0             	mov    -0x10(%ebp),%eax
 69a:	01 d0                	add    %edx,%eax
 69c:	0f b6 00             	movzbl (%eax),%eax
 69f:	84 c0                	test   %al,%al
 6a1:	0f 85 94 fe ff ff    	jne    53b <printf+0x22>
        putc(fd, c);
      }
      state = 0;
    }
  }
}
 6a7:	90                   	nop
 6a8:	c9                   	leave  
 6a9:	c3                   	ret    

000006aa <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 6aa:	55                   	push   %ebp
 6ab:	89 e5                	mov    %esp,%ebp
 6ad:	83 ec 10             	sub    $0x10,%esp
  Header *bp, *p;

  bp = (Header*)ap - 1;
 6b0:	8b 45 08             	mov    0x8(%ebp),%eax
 6b3:	83 e8 08             	sub    $0x8,%eax
 6b6:	89 45 f8             	mov    %eax,-0x8(%ebp)
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 6b9:	a1 58 0b 00 00       	mov    0xb58,%eax
 6be:	89 45 fc             	mov    %eax,-0x4(%ebp)
 6c1:	eb 24                	jmp    6e7 <free+0x3d>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 6c3:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6c6:	8b 00                	mov    (%eax),%eax
 6c8:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 6cb:	77 12                	ja     6df <free+0x35>
 6cd:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6d0:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 6d3:	77 24                	ja     6f9 <free+0x4f>
 6d5:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6d8:	8b 00                	mov    (%eax),%eax
 6da:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 6dd:	77 1a                	ja     6f9 <free+0x4f>
free(void *ap)
{
  Header *bp, *p;

  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 6df:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6e2:	8b 00                	mov    (%eax),%eax
 6e4:	89 45 fc             	mov    %eax,-0x4(%ebp)
 6e7:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6ea:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 6ed:	76 d4                	jbe    6c3 <free+0x19>
 6ef:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6f2:	8b 00                	mov    (%eax),%eax
 6f4:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 6f7:	76 ca                	jbe    6c3 <free+0x19>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if(bp + bp->s.size == p->s.ptr){
 6f9:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6fc:	8b 40 04             	mov    0x4(%eax),%eax
 6ff:	8d 14 c5 00 00 00 00 	lea    0x0(,%eax,8),%edx
 706:	8b 45 f8             	mov    -0x8(%ebp),%eax
 709:	01 c2                	add    %eax,%edx
 70b:	8b 45 fc             	mov    -0x4(%ebp),%eax
 70e:	8b 00                	mov    (%eax),%eax
 710:	39 c2                	cmp    %eax,%edx
 712:	75 24                	jne    738 <free+0x8e>
    bp->s.size += p->s.ptr->s.size;
 714:	8b 45 f8             	mov    -0x8(%ebp),%eax
 717:	8b 50 04             	mov    0x4(%eax),%edx
 71a:	8b 45 fc             	mov    -0x4(%ebp),%eax
 71d:	8b 00                	mov    (%eax),%eax
 71f:	8b 40 04             	mov    0x4(%eax),%eax
 722:	01 c2                	add    %eax,%edx
 724:	8b 45 f8             	mov    -0x8(%ebp),%eax
 727:	89 50 04             	mov    %edx,0x4(%eax)
    bp->s.ptr = p->s.ptr->s.ptr;
 72a:	8b 45 fc             	mov    -0x4(%ebp),%eax
 72d:	8b 00                	mov    (%eax),%eax
 72f:	8b 10                	mov    (%eax),%edx
 731:	8b 45 f8             	mov    -0x8(%ebp),%eax
 734:	89 10                	mov    %edx,(%eax)
 736:	eb 0a                	jmp    742 <free+0x98>
  } else
    bp->s.ptr = p->s.ptr;
 738:	8b 45 fc             	mov    -0x4(%ebp),%eax
 73b:	8b 10                	mov    (%eax),%edx
 73d:	8b 45 f8             	mov    -0x8(%ebp),%eax
 740:	89 10                	mov    %edx,(%eax)
  if(p + p->s.size == bp){
 742:	8b 45 fc             	mov    -0x4(%ebp),%eax
 745:	8b 40 04             	mov    0x4(%eax),%eax
 748:	8d 14 c5 00 00 00 00 	lea    0x0(,%eax,8),%edx
 74f:	8b 45 fc             	mov    -0x4(%ebp),%eax
 752:	01 d0                	add    %edx,%eax
 754:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 757:	75 20                	jne    779 <free+0xcf>
    p->s.size += bp->s.size;
 759:	8b 45 fc             	mov    -0x4(%ebp),%eax
 75c:	8b 50 04             	mov    0x4(%eax),%edx
 75f:	8b 45 f8             	mov    -0x8(%ebp),%eax
 762:	8b 40 04             	mov    0x4(%eax),%eax
 765:	01 c2                	add    %eax,%edx
 767:	8b 45 fc             	mov    -0x4(%ebp),%eax
 76a:	89 50 04             	mov    %edx,0x4(%eax)
    p->s.ptr = bp->s.ptr;
 76d:	8b 45 f8             	mov    -0x8(%ebp),%eax
 770:	8b 10                	mov    (%eax),%edx
 772:	8b 45 fc             	mov    -0x4(%ebp),%eax
 775:	89 10                	mov    %edx,(%eax)
 777:	eb 08                	jmp    781 <free+0xd7>
  } else
    p->s.ptr = bp;
 779:	8b 45 fc             	mov    -0x4(%ebp),%eax
 77c:	8b 55 f8             	mov    -0x8(%ebp),%edx
 77f:	89 10                	mov    %edx,(%eax)
  freep = p;
 781:	8b 45 fc             	mov    -0x4(%ebp),%eax
 784:	a3 58 0b 00 00       	mov    %eax,0xb58
}
 789:	90                   	nop
 78a:	c9                   	leave  
 78b:	c3                   	ret    

0000078c <morecore>:

static Header*
morecore(uint nu)
{
 78c:	55                   	push   %ebp
 78d:	89 e5                	mov    %esp,%ebp
 78f:	83 ec 18             	sub    $0x18,%esp
  char *p;
  Header *hp;

  if(nu < 4096)
 792:	81 7d 08 ff 0f 00 00 	cmpl   $0xfff,0x8(%ebp)
 799:	77 07                	ja     7a2 <morecore+0x16>
    nu = 4096;
 79b:	c7 45 08 00 10 00 00 	movl   $0x1000,0x8(%ebp)
  p = sbrk(nu * sizeof(Header));
 7a2:	8b 45 08             	mov    0x8(%ebp),%eax
 7a5:	c1 e0 03             	shl    $0x3,%eax
 7a8:	83 ec 0c             	sub    $0xc,%esp
 7ab:	50                   	push   %eax
 7ac:	e8 31 fc ff ff       	call   3e2 <sbrk>
 7b1:	83 c4 10             	add    $0x10,%esp
 7b4:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(p == (char*)-1)
 7b7:	83 7d f4 ff          	cmpl   $0xffffffff,-0xc(%ebp)
 7bb:	75 07                	jne    7c4 <morecore+0x38>
    return 0;
 7bd:	b8 00 00 00 00       	mov    $0x0,%eax
 7c2:	eb 26                	jmp    7ea <morecore+0x5e>
  hp = (Header*)p;
 7c4:	8b 45 f4             	mov    -0xc(%ebp),%eax
 7c7:	89 45 f0             	mov    %eax,-0x10(%ebp)
  hp->s.size = nu;
 7ca:	8b 45 f0             	mov    -0x10(%ebp),%eax
 7cd:	8b 55 08             	mov    0x8(%ebp),%edx
 7d0:	89 50 04             	mov    %edx,0x4(%eax)
  free((void*)(hp + 1));
 7d3:	8b 45 f0             	mov    -0x10(%ebp),%eax
 7d6:	83 c0 08             	add    $0x8,%eax
 7d9:	83 ec 0c             	sub    $0xc,%esp
 7dc:	50                   	push   %eax
 7dd:	e8 c8 fe ff ff       	call   6aa <free>
 7e2:	83 c4 10             	add    $0x10,%esp
  return freep;
 7e5:	a1 58 0b 00 00       	mov    0xb58,%eax
}
 7ea:	c9                   	leave  
 7eb:	c3                   	ret    

000007ec <malloc>:

void*
malloc(uint nbytes)
{
 7ec:	55                   	push   %ebp
 7ed:	89 e5                	mov    %esp,%ebp
 7ef:	83 ec 18             	sub    $0x18,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 7f2:	8b 45 08             	mov    0x8(%ebp),%eax
 7f5:	83 c0 07             	add    $0x7,%eax
 7f8:	c1 e8 03             	shr    $0x3,%eax
 7fb:	83 c0 01             	add    $0x1,%eax
 7fe:	89 45 ec             	mov    %eax,-0x14(%ebp)
  if((prevp = freep) == 0){
 801:	a1 58 0b 00 00       	mov    0xb58,%eax
 806:	89 45 f0             	mov    %eax,-0x10(%ebp)
 809:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 80d:	75 23                	jne    832 <malloc+0x46>
    base.s.ptr = freep = prevp = &base;
 80f:	c7 45 f0 50 0b 00 00 	movl   $0xb50,-0x10(%ebp)
 816:	8b 45 f0             	mov    -0x10(%ebp),%eax
 819:	a3 58 0b 00 00       	mov    %eax,0xb58
 81e:	a1 58 0b 00 00       	mov    0xb58,%eax
 823:	a3 50 0b 00 00       	mov    %eax,0xb50
    base.s.size = 0;
 828:	c7 05 54 0b 00 00 00 	movl   $0x0,0xb54
 82f:	00 00 00 
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 832:	8b 45 f0             	mov    -0x10(%ebp),%eax
 835:	8b 00                	mov    (%eax),%eax
 837:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if(p->s.size >= nunits){
 83a:	8b 45 f4             	mov    -0xc(%ebp),%eax
 83d:	8b 40 04             	mov    0x4(%eax),%eax
 840:	3b 45 ec             	cmp    -0x14(%ebp),%eax
 843:	72 4d                	jb     892 <malloc+0xa6>
      if(p->s.size == nunits)
 845:	8b 45 f4             	mov    -0xc(%ebp),%eax
 848:	8b 40 04             	mov    0x4(%eax),%eax
 84b:	3b 45 ec             	cmp    -0x14(%ebp),%eax
 84e:	75 0c                	jne    85c <malloc+0x70>
        prevp->s.ptr = p->s.ptr;
 850:	8b 45 f4             	mov    -0xc(%ebp),%eax
 853:	8b 10                	mov    (%eax),%edx
 855:	8b 45 f0             	mov    -0x10(%ebp),%eax
 858:	89 10                	mov    %edx,(%eax)
 85a:	eb 26                	jmp    882 <malloc+0x96>
      else {
        p->s.size -= nunits;
 85c:	8b 45 f4             	mov    -0xc(%ebp),%eax
 85f:	8b 40 04             	mov    0x4(%eax),%eax
 862:	2b 45 ec             	sub    -0x14(%ebp),%eax
 865:	89 c2                	mov    %eax,%edx
 867:	8b 45 f4             	mov    -0xc(%ebp),%eax
 86a:	89 50 04             	mov    %edx,0x4(%eax)
        p += p->s.size;
 86d:	8b 45 f4             	mov    -0xc(%ebp),%eax
 870:	8b 40 04             	mov    0x4(%eax),%eax
 873:	c1 e0 03             	shl    $0x3,%eax
 876:	01 45 f4             	add    %eax,-0xc(%ebp)
        p->s.size = nunits;
 879:	8b 45 f4             	mov    -0xc(%ebp),%eax
 87c:	8b 55 ec             	mov    -0x14(%ebp),%edx
 87f:	89 50 04             	mov    %edx,0x4(%eax)
      }
      freep = prevp;
 882:	8b 45 f0             	mov    -0x10(%ebp),%eax
 885:	a3 58 0b 00 00       	mov    %eax,0xb58
      return (void*)(p + 1);
 88a:	8b 45 f4             	mov    -0xc(%ebp),%eax
 88d:	83 c0 08             	add    $0x8,%eax
 890:	eb 3b                	jmp    8cd <malloc+0xe1>
    }
    if(p == freep)
 892:	a1 58 0b 00 00       	mov    0xb58,%eax
 897:	39 45 f4             	cmp    %eax,-0xc(%ebp)
 89a:	75 1e                	jne    8ba <malloc+0xce>
      if((p = morecore(nunits)) == 0)
 89c:	83 ec 0c             	sub    $0xc,%esp
 89f:	ff 75 ec             	pushl  -0x14(%ebp)
 8a2:	e8 e5 fe ff ff       	call   78c <morecore>
 8a7:	83 c4 10             	add    $0x10,%esp
 8aa:	89 45 f4             	mov    %eax,-0xc(%ebp)
 8ad:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 8b1:	75 07                	jne    8ba <malloc+0xce>
        return 0;
 8b3:	b8 00 00 00 00       	mov    $0x0,%eax
 8b8:	eb 13                	jmp    8cd <malloc+0xe1>
  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
  if((prevp = freep) == 0){
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 8ba:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8bd:	89 45 f0             	mov    %eax,-0x10(%ebp)
 8c0:	8b 45 f4             	mov    -0xc(%ebp),%eax
 8c3:	8b 00                	mov    (%eax),%eax
 8c5:	89 45 f4             	mov    %eax,-0xc(%ebp)
      return (void*)(p + 1);
    }
    if(p == freep)
      if((p = morecore(nunits)) == 0)
        return 0;
  }
 8c8:	e9 6d ff ff ff       	jmp    83a <malloc+0x4e>
}
 8cd:	c9                   	leave  
 8ce:	c3                   	ret    
