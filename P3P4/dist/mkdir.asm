
_mkdir:     file format elf32-i386


Disassembly of section .text:

00000000 <main>:
#include "stat.h"
#include "user.h"

int
main(int argc, char *argv[])
{
   0:	8d 4c 24 04          	lea    0x4(%esp),%ecx
   4:	83 e4 f0             	and    $0xfffffff0,%esp
   7:	ff 71 fc             	pushl  -0x4(%ecx)
   a:	55                   	push   %ebp
   b:	89 e5                	mov    %esp,%ebp
   d:	53                   	push   %ebx
   e:	51                   	push   %ecx
   f:	83 ec 10             	sub    $0x10,%esp
  12:	89 cb                	mov    %ecx,%ebx
  int i;

  if(argc < 2){
  14:	83 3b 01             	cmpl   $0x1,(%ebx)
  17:	7f 17                	jg     30 <main+0x30>
    printf(2, "Usage: mkdir files...\n");
  19:	83 ec 08             	sub    $0x8,%esp
  1c:	68 89 08 00 00       	push   $0x889
  21:	6a 02                	push   $0x2
  23:	e8 ab 04 00 00       	call   4d3 <printf>
  28:	83 c4 10             	add    $0x10,%esp
    exit();
  2b:	e8 e4 02 00 00       	call   314 <exit>
  }

  for(i = 1; i < argc; i++){
  30:	c7 45 f4 01 00 00 00 	movl   $0x1,-0xc(%ebp)
  37:	eb 4b                	jmp    84 <main+0x84>
    if(mkdir(argv[i]) < 0){
  39:	8b 45 f4             	mov    -0xc(%ebp),%eax
  3c:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  43:	8b 43 04             	mov    0x4(%ebx),%eax
  46:	01 d0                	add    %edx,%eax
  48:	8b 00                	mov    (%eax),%eax
  4a:	83 ec 0c             	sub    $0xc,%esp
  4d:	50                   	push   %eax
  4e:	e8 29 03 00 00       	call   37c <mkdir>
  53:	83 c4 10             	add    $0x10,%esp
  56:	85 c0                	test   %eax,%eax
  58:	79 26                	jns    80 <main+0x80>
      printf(2, "mkdir: %s failed to create\n", argv[i]);
  5a:	8b 45 f4             	mov    -0xc(%ebp),%eax
  5d:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  64:	8b 43 04             	mov    0x4(%ebx),%eax
  67:	01 d0                	add    %edx,%eax
  69:	8b 00                	mov    (%eax),%eax
  6b:	83 ec 04             	sub    $0x4,%esp
  6e:	50                   	push   %eax
  6f:	68 a0 08 00 00       	push   $0x8a0
  74:	6a 02                	push   $0x2
  76:	e8 58 04 00 00       	call   4d3 <printf>
  7b:	83 c4 10             	add    $0x10,%esp
      break;
  7e:	eb 0b                	jmp    8b <main+0x8b>
  if(argc < 2){
    printf(2, "Usage: mkdir files...\n");
    exit();
  }

  for(i = 1; i < argc; i++){
  80:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
  84:	8b 45 f4             	mov    -0xc(%ebp),%eax
  87:	3b 03                	cmp    (%ebx),%eax
  89:	7c ae                	jl     39 <main+0x39>
      printf(2, "mkdir: %s failed to create\n", argv[i]);
      break;
    }
  }

  exit();
  8b:	e8 84 02 00 00       	call   314 <exit>

00000090 <stosb>:
               "cc");
}

static inline void
stosb(void *addr, int data, int cnt)
{
  90:	55                   	push   %ebp
  91:	89 e5                	mov    %esp,%ebp
  93:	57                   	push   %edi
  94:	53                   	push   %ebx
  asm volatile("cld; rep stosb" :
  95:	8b 4d 08             	mov    0x8(%ebp),%ecx
  98:	8b 55 10             	mov    0x10(%ebp),%edx
  9b:	8b 45 0c             	mov    0xc(%ebp),%eax
  9e:	89 cb                	mov    %ecx,%ebx
  a0:	89 df                	mov    %ebx,%edi
  a2:	89 d1                	mov    %edx,%ecx
  a4:	fc                   	cld    
  a5:	f3 aa                	rep stos %al,%es:(%edi)
  a7:	89 ca                	mov    %ecx,%edx
  a9:	89 fb                	mov    %edi,%ebx
  ab:	89 5d 08             	mov    %ebx,0x8(%ebp)
  ae:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
  b1:	90                   	nop
  b2:	5b                   	pop    %ebx
  b3:	5f                   	pop    %edi
  b4:	5d                   	pop    %ebp
  b5:	c3                   	ret    

000000b6 <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, char *t)
{
  b6:	55                   	push   %ebp
  b7:	89 e5                	mov    %esp,%ebp
  b9:	83 ec 10             	sub    $0x10,%esp
  char *os;

  os = s;
  bc:	8b 45 08             	mov    0x8(%ebp),%eax
  bf:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while((*s++ = *t++) != 0)
  c2:	90                   	nop
  c3:	8b 45 08             	mov    0x8(%ebp),%eax
  c6:	8d 50 01             	lea    0x1(%eax),%edx
  c9:	89 55 08             	mov    %edx,0x8(%ebp)
  cc:	8b 55 0c             	mov    0xc(%ebp),%edx
  cf:	8d 4a 01             	lea    0x1(%edx),%ecx
  d2:	89 4d 0c             	mov    %ecx,0xc(%ebp)
  d5:	0f b6 12             	movzbl (%edx),%edx
  d8:	88 10                	mov    %dl,(%eax)
  da:	0f b6 00             	movzbl (%eax),%eax
  dd:	84 c0                	test   %al,%al
  df:	75 e2                	jne    c3 <strcpy+0xd>
    ;
  return os;
  e1:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  e4:	c9                   	leave  
  e5:	c3                   	ret    

000000e6 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  e6:	55                   	push   %ebp
  e7:	89 e5                	mov    %esp,%ebp
  while(*p && *p == *q)
  e9:	eb 08                	jmp    f3 <strcmp+0xd>
    p++, q++;
  eb:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  ef:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
  while(*p && *p == *q)
  f3:	8b 45 08             	mov    0x8(%ebp),%eax
  f6:	0f b6 00             	movzbl (%eax),%eax
  f9:	84 c0                	test   %al,%al
  fb:	74 10                	je     10d <strcmp+0x27>
  fd:	8b 45 08             	mov    0x8(%ebp),%eax
 100:	0f b6 10             	movzbl (%eax),%edx
 103:	8b 45 0c             	mov    0xc(%ebp),%eax
 106:	0f b6 00             	movzbl (%eax),%eax
 109:	38 c2                	cmp    %al,%dl
 10b:	74 de                	je     eb <strcmp+0x5>
    p++, q++;
  return (uchar)*p - (uchar)*q;
 10d:	8b 45 08             	mov    0x8(%ebp),%eax
 110:	0f b6 00             	movzbl (%eax),%eax
 113:	0f b6 d0             	movzbl %al,%edx
 116:	8b 45 0c             	mov    0xc(%ebp),%eax
 119:	0f b6 00             	movzbl (%eax),%eax
 11c:	0f b6 c0             	movzbl %al,%eax
 11f:	29 c2                	sub    %eax,%edx
 121:	89 d0                	mov    %edx,%eax
}
 123:	5d                   	pop    %ebp
 124:	c3                   	ret    

00000125 <strlen>:

uint
strlen(char *s)
{
 125:	55                   	push   %ebp
 126:	89 e5                	mov    %esp,%ebp
 128:	83 ec 10             	sub    $0x10,%esp
  int n;

  for(n = 0; s[n]; n++)
 12b:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
 132:	eb 04                	jmp    138 <strlen+0x13>
 134:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
 138:	8b 55 fc             	mov    -0x4(%ebp),%edx
 13b:	8b 45 08             	mov    0x8(%ebp),%eax
 13e:	01 d0                	add    %edx,%eax
 140:	0f b6 00             	movzbl (%eax),%eax
 143:	84 c0                	test   %al,%al
 145:	75 ed                	jne    134 <strlen+0xf>
    ;
  return n;
 147:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
 14a:	c9                   	leave  
 14b:	c3                   	ret    

0000014c <memset>:

void*
memset(void *dst, int c, uint n)
{
 14c:	55                   	push   %ebp
 14d:	89 e5                	mov    %esp,%ebp
  stosb(dst, c, n);
 14f:	8b 45 10             	mov    0x10(%ebp),%eax
 152:	50                   	push   %eax
 153:	ff 75 0c             	pushl  0xc(%ebp)
 156:	ff 75 08             	pushl  0x8(%ebp)
 159:	e8 32 ff ff ff       	call   90 <stosb>
 15e:	83 c4 0c             	add    $0xc,%esp
  return dst;
 161:	8b 45 08             	mov    0x8(%ebp),%eax
}
 164:	c9                   	leave  
 165:	c3                   	ret    

00000166 <strchr>:

char*
strchr(const char *s, char c)
{
 166:	55                   	push   %ebp
 167:	89 e5                	mov    %esp,%ebp
 169:	83 ec 04             	sub    $0x4,%esp
 16c:	8b 45 0c             	mov    0xc(%ebp),%eax
 16f:	88 45 fc             	mov    %al,-0x4(%ebp)
  for(; *s; s++)
 172:	eb 14                	jmp    188 <strchr+0x22>
    if(*s == c)
 174:	8b 45 08             	mov    0x8(%ebp),%eax
 177:	0f b6 00             	movzbl (%eax),%eax
 17a:	3a 45 fc             	cmp    -0x4(%ebp),%al
 17d:	75 05                	jne    184 <strchr+0x1e>
      return (char*)s;
 17f:	8b 45 08             	mov    0x8(%ebp),%eax
 182:	eb 13                	jmp    197 <strchr+0x31>
}

char*
strchr(const char *s, char c)
{
  for(; *s; s++)
 184:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 188:	8b 45 08             	mov    0x8(%ebp),%eax
 18b:	0f b6 00             	movzbl (%eax),%eax
 18e:	84 c0                	test   %al,%al
 190:	75 e2                	jne    174 <strchr+0xe>
    if(*s == c)
      return (char*)s;
  return 0;
 192:	b8 00 00 00 00       	mov    $0x0,%eax
}
 197:	c9                   	leave  
 198:	c3                   	ret    

00000199 <gets>:

char*
gets(char *buf, int max)
{
 199:	55                   	push   %ebp
 19a:	89 e5                	mov    %esp,%ebp
 19c:	83 ec 18             	sub    $0x18,%esp
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 19f:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
 1a6:	eb 42                	jmp    1ea <gets+0x51>
    cc = read(0, &c, 1);
 1a8:	83 ec 04             	sub    $0x4,%esp
 1ab:	6a 01                	push   $0x1
 1ad:	8d 45 ef             	lea    -0x11(%ebp),%eax
 1b0:	50                   	push   %eax
 1b1:	6a 00                	push   $0x0
 1b3:	e8 74 01 00 00       	call   32c <read>
 1b8:	83 c4 10             	add    $0x10,%esp
 1bb:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if(cc < 1)
 1be:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 1c2:	7e 33                	jle    1f7 <gets+0x5e>
      break;
    buf[i++] = c;
 1c4:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1c7:	8d 50 01             	lea    0x1(%eax),%edx
 1ca:	89 55 f4             	mov    %edx,-0xc(%ebp)
 1cd:	89 c2                	mov    %eax,%edx
 1cf:	8b 45 08             	mov    0x8(%ebp),%eax
 1d2:	01 c2                	add    %eax,%edx
 1d4:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 1d8:	88 02                	mov    %al,(%edx)
    if(c == '\n' || c == '\r')
 1da:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 1de:	3c 0a                	cmp    $0xa,%al
 1e0:	74 16                	je     1f8 <gets+0x5f>
 1e2:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
 1e6:	3c 0d                	cmp    $0xd,%al
 1e8:	74 0e                	je     1f8 <gets+0x5f>
gets(char *buf, int max)
{
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 1ea:	8b 45 f4             	mov    -0xc(%ebp),%eax
 1ed:	83 c0 01             	add    $0x1,%eax
 1f0:	3b 45 0c             	cmp    0xc(%ebp),%eax
 1f3:	7c b3                	jl     1a8 <gets+0xf>
 1f5:	eb 01                	jmp    1f8 <gets+0x5f>
    cc = read(0, &c, 1);
    if(cc < 1)
      break;
 1f7:	90                   	nop
    buf[i++] = c;
    if(c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 1f8:	8b 55 f4             	mov    -0xc(%ebp),%edx
 1fb:	8b 45 08             	mov    0x8(%ebp),%eax
 1fe:	01 d0                	add    %edx,%eax
 200:	c6 00 00             	movb   $0x0,(%eax)
  return buf;
 203:	8b 45 08             	mov    0x8(%ebp),%eax
}
 206:	c9                   	leave  
 207:	c3                   	ret    

00000208 <stat>:

int
stat(char *n, struct stat *st)
{
 208:	55                   	push   %ebp
 209:	89 e5                	mov    %esp,%ebp
 20b:	83 ec 18             	sub    $0x18,%esp
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 20e:	83 ec 08             	sub    $0x8,%esp
 211:	6a 00                	push   $0x0
 213:	ff 75 08             	pushl  0x8(%ebp)
 216:	e8 39 01 00 00       	call   354 <open>
 21b:	83 c4 10             	add    $0x10,%esp
 21e:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(fd < 0)
 221:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 225:	79 07                	jns    22e <stat+0x26>
    return -1;
 227:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 22c:	eb 25                	jmp    253 <stat+0x4b>
  r = fstat(fd, st);
 22e:	83 ec 08             	sub    $0x8,%esp
 231:	ff 75 0c             	pushl  0xc(%ebp)
 234:	ff 75 f4             	pushl  -0xc(%ebp)
 237:	e8 30 01 00 00       	call   36c <fstat>
 23c:	83 c4 10             	add    $0x10,%esp
 23f:	89 45 f0             	mov    %eax,-0x10(%ebp)
  close(fd);
 242:	83 ec 0c             	sub    $0xc,%esp
 245:	ff 75 f4             	pushl  -0xc(%ebp)
 248:	e8 ef 00 00 00       	call   33c <close>
 24d:	83 c4 10             	add    $0x10,%esp
  return r;
 250:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
 253:	c9                   	leave  
 254:	c3                   	ret    

00000255 <atoi>:

int
atoi(const char *s)
{
 255:	55                   	push   %ebp
 256:	89 e5                	mov    %esp,%ebp
 258:	83 ec 10             	sub    $0x10,%esp
  int n, sign;

  n = 0;
 25b:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  while(*s == ' ') s++;
 262:	eb 04                	jmp    268 <atoi+0x13>
 264:	83 45 08 01          	addl   $0x1,0x8(%ebp)
 268:	8b 45 08             	mov    0x8(%ebp),%eax
 26b:	0f b6 00             	movzbl (%eax),%eax
 26e:	3c 20                	cmp    $0x20,%al
 270:	74 f2                	je     264 <atoi+0xf>
  sign = (*s == '-') ? -1 : 1;
 272:	8b 45 08             	mov    0x8(%ebp),%eax
 275:	0f b6 00             	movzbl (%eax),%eax
 278:	3c 2d                	cmp    $0x2d,%al
 27a:	75 07                	jne    283 <atoi+0x2e>
 27c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 281:	eb 05                	jmp    288 <atoi+0x33>
 283:	b8 01 00 00 00       	mov    $0x1,%eax
 288:	89 45 f8             	mov    %eax,-0x8(%ebp)
  while('0' <= *s && *s <= '9')
 28b:	eb 25                	jmp    2b2 <atoi+0x5d>
    n = n*10 + *s++ - '0';
 28d:	8b 55 fc             	mov    -0x4(%ebp),%edx
 290:	89 d0                	mov    %edx,%eax
 292:	c1 e0 02             	shl    $0x2,%eax
 295:	01 d0                	add    %edx,%eax
 297:	01 c0                	add    %eax,%eax
 299:	89 c1                	mov    %eax,%ecx
 29b:	8b 45 08             	mov    0x8(%ebp),%eax
 29e:	8d 50 01             	lea    0x1(%eax),%edx
 2a1:	89 55 08             	mov    %edx,0x8(%ebp)
 2a4:	0f b6 00             	movzbl (%eax),%eax
 2a7:	0f be c0             	movsbl %al,%eax
 2aa:	01 c8                	add    %ecx,%eax
 2ac:	83 e8 30             	sub    $0x30,%eax
 2af:	89 45 fc             	mov    %eax,-0x4(%ebp)
  int n, sign;

  n = 0;
  while(*s == ' ') s++;
  sign = (*s == '-') ? -1 : 1;
  while('0' <= *s && *s <= '9')
 2b2:	8b 45 08             	mov    0x8(%ebp),%eax
 2b5:	0f b6 00             	movzbl (%eax),%eax
 2b8:	3c 2f                	cmp    $0x2f,%al
 2ba:	7e 0a                	jle    2c6 <atoi+0x71>
 2bc:	8b 45 08             	mov    0x8(%ebp),%eax
 2bf:	0f b6 00             	movzbl (%eax),%eax
 2c2:	3c 39                	cmp    $0x39,%al
 2c4:	7e c7                	jle    28d <atoi+0x38>
    n = n*10 + *s++ - '0';
  return sign*n;
 2c6:	8b 45 f8             	mov    -0x8(%ebp),%eax
 2c9:	0f af 45 fc          	imul   -0x4(%ebp),%eax
}
 2cd:	c9                   	leave  
 2ce:	c3                   	ret    

000002cf <memmove>:

void*
memmove(void *vdst, void *vsrc, int n)
{
 2cf:	55                   	push   %ebp
 2d0:	89 e5                	mov    %esp,%ebp
 2d2:	83 ec 10             	sub    $0x10,%esp
  char *dst, *src;
  
  dst = vdst;
 2d5:	8b 45 08             	mov    0x8(%ebp),%eax
 2d8:	89 45 fc             	mov    %eax,-0x4(%ebp)
  src = vsrc;
 2db:	8b 45 0c             	mov    0xc(%ebp),%eax
 2de:	89 45 f8             	mov    %eax,-0x8(%ebp)
  while(n-- > 0)
 2e1:	eb 17                	jmp    2fa <memmove+0x2b>
    *dst++ = *src++;
 2e3:	8b 45 fc             	mov    -0x4(%ebp),%eax
 2e6:	8d 50 01             	lea    0x1(%eax),%edx
 2e9:	89 55 fc             	mov    %edx,-0x4(%ebp)
 2ec:	8b 55 f8             	mov    -0x8(%ebp),%edx
 2ef:	8d 4a 01             	lea    0x1(%edx),%ecx
 2f2:	89 4d f8             	mov    %ecx,-0x8(%ebp)
 2f5:	0f b6 12             	movzbl (%edx),%edx
 2f8:	88 10                	mov    %dl,(%eax)
{
  char *dst, *src;
  
  dst = vdst;
  src = vsrc;
  while(n-- > 0)
 2fa:	8b 45 10             	mov    0x10(%ebp),%eax
 2fd:	8d 50 ff             	lea    -0x1(%eax),%edx
 300:	89 55 10             	mov    %edx,0x10(%ebp)
 303:	85 c0                	test   %eax,%eax
 305:	7f dc                	jg     2e3 <memmove+0x14>
    *dst++ = *src++;
  return vdst;
 307:	8b 45 08             	mov    0x8(%ebp),%eax
}
 30a:	c9                   	leave  
 30b:	c3                   	ret    

0000030c <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
 30c:	b8 01 00 00 00       	mov    $0x1,%eax
 311:	cd 40                	int    $0x40
 313:	c3                   	ret    

00000314 <exit>:
SYSCALL(exit)
 314:	b8 02 00 00 00       	mov    $0x2,%eax
 319:	cd 40                	int    $0x40
 31b:	c3                   	ret    

0000031c <wait>:
SYSCALL(wait)
 31c:	b8 03 00 00 00       	mov    $0x3,%eax
 321:	cd 40                	int    $0x40
 323:	c3                   	ret    

00000324 <pipe>:
SYSCALL(pipe)
 324:	b8 04 00 00 00       	mov    $0x4,%eax
 329:	cd 40                	int    $0x40
 32b:	c3                   	ret    

0000032c <read>:
SYSCALL(read)
 32c:	b8 05 00 00 00       	mov    $0x5,%eax
 331:	cd 40                	int    $0x40
 333:	c3                   	ret    

00000334 <write>:
SYSCALL(write)
 334:	b8 10 00 00 00       	mov    $0x10,%eax
 339:	cd 40                	int    $0x40
 33b:	c3                   	ret    

0000033c <close>:
SYSCALL(close)
 33c:	b8 15 00 00 00       	mov    $0x15,%eax
 341:	cd 40                	int    $0x40
 343:	c3                   	ret    

00000344 <kill>:
SYSCALL(kill)
 344:	b8 06 00 00 00       	mov    $0x6,%eax
 349:	cd 40                	int    $0x40
 34b:	c3                   	ret    

0000034c <exec>:
SYSCALL(exec)
 34c:	b8 07 00 00 00       	mov    $0x7,%eax
 351:	cd 40                	int    $0x40
 353:	c3                   	ret    

00000354 <open>:
SYSCALL(open)
 354:	b8 0f 00 00 00       	mov    $0xf,%eax
 359:	cd 40                	int    $0x40
 35b:	c3                   	ret    

0000035c <mknod>:
SYSCALL(mknod)
 35c:	b8 11 00 00 00       	mov    $0x11,%eax
 361:	cd 40                	int    $0x40
 363:	c3                   	ret    

00000364 <unlink>:
SYSCALL(unlink)
 364:	b8 12 00 00 00       	mov    $0x12,%eax
 369:	cd 40                	int    $0x40
 36b:	c3                   	ret    

0000036c <fstat>:
SYSCALL(fstat)
 36c:	b8 08 00 00 00       	mov    $0x8,%eax
 371:	cd 40                	int    $0x40
 373:	c3                   	ret    

00000374 <link>:
SYSCALL(link)
 374:	b8 13 00 00 00       	mov    $0x13,%eax
 379:	cd 40                	int    $0x40
 37b:	c3                   	ret    

0000037c <mkdir>:
SYSCALL(mkdir)
 37c:	b8 14 00 00 00       	mov    $0x14,%eax
 381:	cd 40                	int    $0x40
 383:	c3                   	ret    

00000384 <chdir>:
SYSCALL(chdir)
 384:	b8 09 00 00 00       	mov    $0x9,%eax
 389:	cd 40                	int    $0x40
 38b:	c3                   	ret    

0000038c <dup>:
SYSCALL(dup)
 38c:	b8 0a 00 00 00       	mov    $0xa,%eax
 391:	cd 40                	int    $0x40
 393:	c3                   	ret    

00000394 <getpid>:
SYSCALL(getpid)
 394:	b8 0b 00 00 00       	mov    $0xb,%eax
 399:	cd 40                	int    $0x40
 39b:	c3                   	ret    

0000039c <sbrk>:
SYSCALL(sbrk)
 39c:	b8 0c 00 00 00       	mov    $0xc,%eax
 3a1:	cd 40                	int    $0x40
 3a3:	c3                   	ret    

000003a4 <sleep>:
SYSCALL(sleep)
 3a4:	b8 0d 00 00 00       	mov    $0xd,%eax
 3a9:	cd 40                	int    $0x40
 3ab:	c3                   	ret    

000003ac <uptime>:
SYSCALL(uptime)
 3ac:	b8 0e 00 00 00       	mov    $0xe,%eax
 3b1:	cd 40                	int    $0x40
 3b3:	c3                   	ret    

000003b4 <halt>:
SYSCALL(halt)
 3b4:	b8 16 00 00 00       	mov    $0x16,%eax
 3b9:	cd 40                	int    $0x40
 3bb:	c3                   	ret    

000003bc <date>:
SYSCALL(date)    #added the date system call
 3bc:	b8 17 00 00 00       	mov    $0x17,%eax
 3c1:	cd 40                	int    $0x40
 3c3:	c3                   	ret    

000003c4 <getuid>:
SYSCALL(getuid)
 3c4:	b8 18 00 00 00       	mov    $0x18,%eax
 3c9:	cd 40                	int    $0x40
 3cb:	c3                   	ret    

000003cc <getgid>:
SYSCALL(getgid)
 3cc:	b8 19 00 00 00       	mov    $0x19,%eax
 3d1:	cd 40                	int    $0x40
 3d3:	c3                   	ret    

000003d4 <getppid>:
SYSCALL(getppid)
 3d4:	b8 1a 00 00 00       	mov    $0x1a,%eax
 3d9:	cd 40                	int    $0x40
 3db:	c3                   	ret    

000003dc <setuid>:
SYSCALL(setuid)
 3dc:	b8 1b 00 00 00       	mov    $0x1b,%eax
 3e1:	cd 40                	int    $0x40
 3e3:	c3                   	ret    

000003e4 <setgid>:
SYSCALL(setgid)
 3e4:	b8 1c 00 00 00       	mov    $0x1c,%eax
 3e9:	cd 40                	int    $0x40
 3eb:	c3                   	ret    

000003ec <getprocs>:
SYSCALL(getprocs)
 3ec:	b8 1d 00 00 00       	mov    $0x1d,%eax
 3f1:	cd 40                	int    $0x40
 3f3:	c3                   	ret    

000003f4 <setpriority>:
SYSCALL(setpriority)
 3f4:	b8 1e 00 00 00       	mov    $0x1e,%eax
 3f9:	cd 40                	int    $0x40
 3fb:	c3                   	ret    

000003fc <putc>:
#include "stat.h"
#include "user.h"

static void
putc(int fd, char c)
{
 3fc:	55                   	push   %ebp
 3fd:	89 e5                	mov    %esp,%ebp
 3ff:	83 ec 18             	sub    $0x18,%esp
 402:	8b 45 0c             	mov    0xc(%ebp),%eax
 405:	88 45 f4             	mov    %al,-0xc(%ebp)
  write(fd, &c, 1);
 408:	83 ec 04             	sub    $0x4,%esp
 40b:	6a 01                	push   $0x1
 40d:	8d 45 f4             	lea    -0xc(%ebp),%eax
 410:	50                   	push   %eax
 411:	ff 75 08             	pushl  0x8(%ebp)
 414:	e8 1b ff ff ff       	call   334 <write>
 419:	83 c4 10             	add    $0x10,%esp
}
 41c:	90                   	nop
 41d:	c9                   	leave  
 41e:	c3                   	ret    

0000041f <printint>:

static void
printint(int fd, int xx, int base, int sgn)
{
 41f:	55                   	push   %ebp
 420:	89 e5                	mov    %esp,%ebp
 422:	53                   	push   %ebx
 423:	83 ec 24             	sub    $0x24,%esp
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 426:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  if(sgn && xx < 0){
 42d:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
 431:	74 17                	je     44a <printint+0x2b>
 433:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
 437:	79 11                	jns    44a <printint+0x2b>
    neg = 1;
 439:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
    x = -xx;
 440:	8b 45 0c             	mov    0xc(%ebp),%eax
 443:	f7 d8                	neg    %eax
 445:	89 45 ec             	mov    %eax,-0x14(%ebp)
 448:	eb 06                	jmp    450 <printint+0x31>
  } else {
    x = xx;
 44a:	8b 45 0c             	mov    0xc(%ebp),%eax
 44d:	89 45 ec             	mov    %eax,-0x14(%ebp)
  }

  i = 0;
 450:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  do{
    buf[i++] = digits[x % base];
 457:	8b 4d f4             	mov    -0xc(%ebp),%ecx
 45a:	8d 41 01             	lea    0x1(%ecx),%eax
 45d:	89 45 f4             	mov    %eax,-0xc(%ebp)
 460:	8b 5d 10             	mov    0x10(%ebp),%ebx
 463:	8b 45 ec             	mov    -0x14(%ebp),%eax
 466:	ba 00 00 00 00       	mov    $0x0,%edx
 46b:	f7 f3                	div    %ebx
 46d:	89 d0                	mov    %edx,%eax
 46f:	0f b6 80 10 0b 00 00 	movzbl 0xb10(%eax),%eax
 476:	88 44 0d dc          	mov    %al,-0x24(%ebp,%ecx,1)
  }while((x /= base) != 0);
 47a:	8b 5d 10             	mov    0x10(%ebp),%ebx
 47d:	8b 45 ec             	mov    -0x14(%ebp),%eax
 480:	ba 00 00 00 00       	mov    $0x0,%edx
 485:	f7 f3                	div    %ebx
 487:	89 45 ec             	mov    %eax,-0x14(%ebp)
 48a:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 48e:	75 c7                	jne    457 <printint+0x38>
  if(neg)
 490:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 494:	74 2d                	je     4c3 <printint+0xa4>
    buf[i++] = '-';
 496:	8b 45 f4             	mov    -0xc(%ebp),%eax
 499:	8d 50 01             	lea    0x1(%eax),%edx
 49c:	89 55 f4             	mov    %edx,-0xc(%ebp)
 49f:	c6 44 05 dc 2d       	movb   $0x2d,-0x24(%ebp,%eax,1)

  while(--i >= 0)
 4a4:	eb 1d                	jmp    4c3 <printint+0xa4>
    putc(fd, buf[i]);
 4a6:	8d 55 dc             	lea    -0x24(%ebp),%edx
 4a9:	8b 45 f4             	mov    -0xc(%ebp),%eax
 4ac:	01 d0                	add    %edx,%eax
 4ae:	0f b6 00             	movzbl (%eax),%eax
 4b1:	0f be c0             	movsbl %al,%eax
 4b4:	83 ec 08             	sub    $0x8,%esp
 4b7:	50                   	push   %eax
 4b8:	ff 75 08             	pushl  0x8(%ebp)
 4bb:	e8 3c ff ff ff       	call   3fc <putc>
 4c0:	83 c4 10             	add    $0x10,%esp
    buf[i++] = digits[x % base];
  }while((x /= base) != 0);
  if(neg)
    buf[i++] = '-';

  while(--i >= 0)
 4c3:	83 6d f4 01          	subl   $0x1,-0xc(%ebp)
 4c7:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 4cb:	79 d9                	jns    4a6 <printint+0x87>
    putc(fd, buf[i]);
}
 4cd:	90                   	nop
 4ce:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 4d1:	c9                   	leave  
 4d2:	c3                   	ret    

000004d3 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, char *fmt, ...)
{
 4d3:	55                   	push   %ebp
 4d4:	89 e5                	mov    %esp,%ebp
 4d6:	83 ec 28             	sub    $0x28,%esp
  char *s;
  int c, i, state;
  uint *ap;

  state = 0;
 4d9:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  ap = (uint*)(void*)&fmt + 1;
 4e0:	8d 45 0c             	lea    0xc(%ebp),%eax
 4e3:	83 c0 04             	add    $0x4,%eax
 4e6:	89 45 e8             	mov    %eax,-0x18(%ebp)
  for(i = 0; fmt[i]; i++){
 4e9:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
 4f0:	e9 59 01 00 00       	jmp    64e <printf+0x17b>
    c = fmt[i] & 0xff;
 4f5:	8b 55 0c             	mov    0xc(%ebp),%edx
 4f8:	8b 45 f0             	mov    -0x10(%ebp),%eax
 4fb:	01 d0                	add    %edx,%eax
 4fd:	0f b6 00             	movzbl (%eax),%eax
 500:	0f be c0             	movsbl %al,%eax
 503:	25 ff 00 00 00       	and    $0xff,%eax
 508:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    if(state == 0){
 50b:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
 50f:	75 2c                	jne    53d <printf+0x6a>
      if(c == '%'){
 511:	83 7d e4 25          	cmpl   $0x25,-0x1c(%ebp)
 515:	75 0c                	jne    523 <printf+0x50>
        state = '%';
 517:	c7 45 ec 25 00 00 00 	movl   $0x25,-0x14(%ebp)
 51e:	e9 27 01 00 00       	jmp    64a <printf+0x177>
      } else {
        putc(fd, c);
 523:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 526:	0f be c0             	movsbl %al,%eax
 529:	83 ec 08             	sub    $0x8,%esp
 52c:	50                   	push   %eax
 52d:	ff 75 08             	pushl  0x8(%ebp)
 530:	e8 c7 fe ff ff       	call   3fc <putc>
 535:	83 c4 10             	add    $0x10,%esp
 538:	e9 0d 01 00 00       	jmp    64a <printf+0x177>
      }
    } else if(state == '%'){
 53d:	83 7d ec 25          	cmpl   $0x25,-0x14(%ebp)
 541:	0f 85 03 01 00 00    	jne    64a <printf+0x177>
      if(c == 'd'){
 547:	83 7d e4 64          	cmpl   $0x64,-0x1c(%ebp)
 54b:	75 1e                	jne    56b <printf+0x98>
        printint(fd, *ap, 10, 1);
 54d:	8b 45 e8             	mov    -0x18(%ebp),%eax
 550:	8b 00                	mov    (%eax),%eax
 552:	6a 01                	push   $0x1
 554:	6a 0a                	push   $0xa
 556:	50                   	push   %eax
 557:	ff 75 08             	pushl  0x8(%ebp)
 55a:	e8 c0 fe ff ff       	call   41f <printint>
 55f:	83 c4 10             	add    $0x10,%esp
        ap++;
 562:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 566:	e9 d8 00 00 00       	jmp    643 <printf+0x170>
      } else if(c == 'x' || c == 'p'){
 56b:	83 7d e4 78          	cmpl   $0x78,-0x1c(%ebp)
 56f:	74 06                	je     577 <printf+0xa4>
 571:	83 7d e4 70          	cmpl   $0x70,-0x1c(%ebp)
 575:	75 1e                	jne    595 <printf+0xc2>
        printint(fd, *ap, 16, 0);
 577:	8b 45 e8             	mov    -0x18(%ebp),%eax
 57a:	8b 00                	mov    (%eax),%eax
 57c:	6a 00                	push   $0x0
 57e:	6a 10                	push   $0x10
 580:	50                   	push   %eax
 581:	ff 75 08             	pushl  0x8(%ebp)
 584:	e8 96 fe ff ff       	call   41f <printint>
 589:	83 c4 10             	add    $0x10,%esp
        ap++;
 58c:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 590:	e9 ae 00 00 00       	jmp    643 <printf+0x170>
      } else if(c == 's'){
 595:	83 7d e4 73          	cmpl   $0x73,-0x1c(%ebp)
 599:	75 43                	jne    5de <printf+0x10b>
        s = (char*)*ap;
 59b:	8b 45 e8             	mov    -0x18(%ebp),%eax
 59e:	8b 00                	mov    (%eax),%eax
 5a0:	89 45 f4             	mov    %eax,-0xc(%ebp)
        ap++;
 5a3:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
        if(s == 0)
 5a7:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 5ab:	75 25                	jne    5d2 <printf+0xff>
          s = "(null)";
 5ad:	c7 45 f4 bc 08 00 00 	movl   $0x8bc,-0xc(%ebp)
        while(*s != 0){
 5b4:	eb 1c                	jmp    5d2 <printf+0xff>
          putc(fd, *s);
 5b6:	8b 45 f4             	mov    -0xc(%ebp),%eax
 5b9:	0f b6 00             	movzbl (%eax),%eax
 5bc:	0f be c0             	movsbl %al,%eax
 5bf:	83 ec 08             	sub    $0x8,%esp
 5c2:	50                   	push   %eax
 5c3:	ff 75 08             	pushl  0x8(%ebp)
 5c6:	e8 31 fe ff ff       	call   3fc <putc>
 5cb:	83 c4 10             	add    $0x10,%esp
          s++;
 5ce:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
      } else if(c == 's'){
        s = (char*)*ap;
        ap++;
        if(s == 0)
          s = "(null)";
        while(*s != 0){
 5d2:	8b 45 f4             	mov    -0xc(%ebp),%eax
 5d5:	0f b6 00             	movzbl (%eax),%eax
 5d8:	84 c0                	test   %al,%al
 5da:	75 da                	jne    5b6 <printf+0xe3>
 5dc:	eb 65                	jmp    643 <printf+0x170>
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 5de:	83 7d e4 63          	cmpl   $0x63,-0x1c(%ebp)
 5e2:	75 1d                	jne    601 <printf+0x12e>
        putc(fd, *ap);
 5e4:	8b 45 e8             	mov    -0x18(%ebp),%eax
 5e7:	8b 00                	mov    (%eax),%eax
 5e9:	0f be c0             	movsbl %al,%eax
 5ec:	83 ec 08             	sub    $0x8,%esp
 5ef:	50                   	push   %eax
 5f0:	ff 75 08             	pushl  0x8(%ebp)
 5f3:	e8 04 fe ff ff       	call   3fc <putc>
 5f8:	83 c4 10             	add    $0x10,%esp
        ap++;
 5fb:	83 45 e8 04          	addl   $0x4,-0x18(%ebp)
 5ff:	eb 42                	jmp    643 <printf+0x170>
      } else if(c == '%'){
 601:	83 7d e4 25          	cmpl   $0x25,-0x1c(%ebp)
 605:	75 17                	jne    61e <printf+0x14b>
        putc(fd, c);
 607:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 60a:	0f be c0             	movsbl %al,%eax
 60d:	83 ec 08             	sub    $0x8,%esp
 610:	50                   	push   %eax
 611:	ff 75 08             	pushl  0x8(%ebp)
 614:	e8 e3 fd ff ff       	call   3fc <putc>
 619:	83 c4 10             	add    $0x10,%esp
 61c:	eb 25                	jmp    643 <printf+0x170>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 61e:	83 ec 08             	sub    $0x8,%esp
 621:	6a 25                	push   $0x25
 623:	ff 75 08             	pushl  0x8(%ebp)
 626:	e8 d1 fd ff ff       	call   3fc <putc>
 62b:	83 c4 10             	add    $0x10,%esp
        putc(fd, c);
 62e:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 631:	0f be c0             	movsbl %al,%eax
 634:	83 ec 08             	sub    $0x8,%esp
 637:	50                   	push   %eax
 638:	ff 75 08             	pushl  0x8(%ebp)
 63b:	e8 bc fd ff ff       	call   3fc <putc>
 640:	83 c4 10             	add    $0x10,%esp
      }
      state = 0;
 643:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
  for(i = 0; fmt[i]; i++){
 64a:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
 64e:	8b 55 0c             	mov    0xc(%ebp),%edx
 651:	8b 45 f0             	mov    -0x10(%ebp),%eax
 654:	01 d0                	add    %edx,%eax
 656:	0f b6 00             	movzbl (%eax),%eax
 659:	84 c0                	test   %al,%al
 65b:	0f 85 94 fe ff ff    	jne    4f5 <printf+0x22>
        putc(fd, c);
      }
      state = 0;
    }
  }
}
 661:	90                   	nop
 662:	c9                   	leave  
 663:	c3                   	ret    

00000664 <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 664:	55                   	push   %ebp
 665:	89 e5                	mov    %esp,%ebp
 667:	83 ec 10             	sub    $0x10,%esp
  Header *bp, *p;

  bp = (Header*)ap - 1;
 66a:	8b 45 08             	mov    0x8(%ebp),%eax
 66d:	83 e8 08             	sub    $0x8,%eax
 670:	89 45 f8             	mov    %eax,-0x8(%ebp)
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 673:	a1 2c 0b 00 00       	mov    0xb2c,%eax
 678:	89 45 fc             	mov    %eax,-0x4(%ebp)
 67b:	eb 24                	jmp    6a1 <free+0x3d>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 67d:	8b 45 fc             	mov    -0x4(%ebp),%eax
 680:	8b 00                	mov    (%eax),%eax
 682:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 685:	77 12                	ja     699 <free+0x35>
 687:	8b 45 f8             	mov    -0x8(%ebp),%eax
 68a:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 68d:	77 24                	ja     6b3 <free+0x4f>
 68f:	8b 45 fc             	mov    -0x4(%ebp),%eax
 692:	8b 00                	mov    (%eax),%eax
 694:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 697:	77 1a                	ja     6b3 <free+0x4f>
free(void *ap)
{
  Header *bp, *p;

  bp = (Header*)ap - 1;
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 699:	8b 45 fc             	mov    -0x4(%ebp),%eax
 69c:	8b 00                	mov    (%eax),%eax
 69e:	89 45 fc             	mov    %eax,-0x4(%ebp)
 6a1:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6a4:	3b 45 fc             	cmp    -0x4(%ebp),%eax
 6a7:	76 d4                	jbe    67d <free+0x19>
 6a9:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6ac:	8b 00                	mov    (%eax),%eax
 6ae:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 6b1:	76 ca                	jbe    67d <free+0x19>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if(bp + bp->s.size == p->s.ptr){
 6b3:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6b6:	8b 40 04             	mov    0x4(%eax),%eax
 6b9:	8d 14 c5 00 00 00 00 	lea    0x0(,%eax,8),%edx
 6c0:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6c3:	01 c2                	add    %eax,%edx
 6c5:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6c8:	8b 00                	mov    (%eax),%eax
 6ca:	39 c2                	cmp    %eax,%edx
 6cc:	75 24                	jne    6f2 <free+0x8e>
    bp->s.size += p->s.ptr->s.size;
 6ce:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6d1:	8b 50 04             	mov    0x4(%eax),%edx
 6d4:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6d7:	8b 00                	mov    (%eax),%eax
 6d9:	8b 40 04             	mov    0x4(%eax),%eax
 6dc:	01 c2                	add    %eax,%edx
 6de:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6e1:	89 50 04             	mov    %edx,0x4(%eax)
    bp->s.ptr = p->s.ptr->s.ptr;
 6e4:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6e7:	8b 00                	mov    (%eax),%eax
 6e9:	8b 10                	mov    (%eax),%edx
 6eb:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6ee:	89 10                	mov    %edx,(%eax)
 6f0:	eb 0a                	jmp    6fc <free+0x98>
  } else
    bp->s.ptr = p->s.ptr;
 6f2:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6f5:	8b 10                	mov    (%eax),%edx
 6f7:	8b 45 f8             	mov    -0x8(%ebp),%eax
 6fa:	89 10                	mov    %edx,(%eax)
  if(p + p->s.size == bp){
 6fc:	8b 45 fc             	mov    -0x4(%ebp),%eax
 6ff:	8b 40 04             	mov    0x4(%eax),%eax
 702:	8d 14 c5 00 00 00 00 	lea    0x0(,%eax,8),%edx
 709:	8b 45 fc             	mov    -0x4(%ebp),%eax
 70c:	01 d0                	add    %edx,%eax
 70e:	3b 45 f8             	cmp    -0x8(%ebp),%eax
 711:	75 20                	jne    733 <free+0xcf>
    p->s.size += bp->s.size;
 713:	8b 45 fc             	mov    -0x4(%ebp),%eax
 716:	8b 50 04             	mov    0x4(%eax),%edx
 719:	8b 45 f8             	mov    -0x8(%ebp),%eax
 71c:	8b 40 04             	mov    0x4(%eax),%eax
 71f:	01 c2                	add    %eax,%edx
 721:	8b 45 fc             	mov    -0x4(%ebp),%eax
 724:	89 50 04             	mov    %edx,0x4(%eax)
    p->s.ptr = bp->s.ptr;
 727:	8b 45 f8             	mov    -0x8(%ebp),%eax
 72a:	8b 10                	mov    (%eax),%edx
 72c:	8b 45 fc             	mov    -0x4(%ebp),%eax
 72f:	89 10                	mov    %edx,(%eax)
 731:	eb 08                	jmp    73b <free+0xd7>
  } else
    p->s.ptr = bp;
 733:	8b 45 fc             	mov    -0x4(%ebp),%eax
 736:	8b 55 f8             	mov    -0x8(%ebp),%edx
 739:	89 10                	mov    %edx,(%eax)
  freep = p;
 73b:	8b 45 fc             	mov    -0x4(%ebp),%eax
 73e:	a3 2c 0b 00 00       	mov    %eax,0xb2c
}
 743:	90                   	nop
 744:	c9                   	leave  
 745:	c3                   	ret    

00000746 <morecore>:

static Header*
morecore(uint nu)
{
 746:	55                   	push   %ebp
 747:	89 e5                	mov    %esp,%ebp
 749:	83 ec 18             	sub    $0x18,%esp
  char *p;
  Header *hp;

  if(nu < 4096)
 74c:	81 7d 08 ff 0f 00 00 	cmpl   $0xfff,0x8(%ebp)
 753:	77 07                	ja     75c <morecore+0x16>
    nu = 4096;
 755:	c7 45 08 00 10 00 00 	movl   $0x1000,0x8(%ebp)
  p = sbrk(nu * sizeof(Header));
 75c:	8b 45 08             	mov    0x8(%ebp),%eax
 75f:	c1 e0 03             	shl    $0x3,%eax
 762:	83 ec 0c             	sub    $0xc,%esp
 765:	50                   	push   %eax
 766:	e8 31 fc ff ff       	call   39c <sbrk>
 76b:	83 c4 10             	add    $0x10,%esp
 76e:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(p == (char*)-1)
 771:	83 7d f4 ff          	cmpl   $0xffffffff,-0xc(%ebp)
 775:	75 07                	jne    77e <morecore+0x38>
    return 0;
 777:	b8 00 00 00 00       	mov    $0x0,%eax
 77c:	eb 26                	jmp    7a4 <morecore+0x5e>
  hp = (Header*)p;
 77e:	8b 45 f4             	mov    -0xc(%ebp),%eax
 781:	89 45 f0             	mov    %eax,-0x10(%ebp)
  hp->s.size = nu;
 784:	8b 45 f0             	mov    -0x10(%ebp),%eax
 787:	8b 55 08             	mov    0x8(%ebp),%edx
 78a:	89 50 04             	mov    %edx,0x4(%eax)
  free((void*)(hp + 1));
 78d:	8b 45 f0             	mov    -0x10(%ebp),%eax
 790:	83 c0 08             	add    $0x8,%eax
 793:	83 ec 0c             	sub    $0xc,%esp
 796:	50                   	push   %eax
 797:	e8 c8 fe ff ff       	call   664 <free>
 79c:	83 c4 10             	add    $0x10,%esp
  return freep;
 79f:	a1 2c 0b 00 00       	mov    0xb2c,%eax
}
 7a4:	c9                   	leave  
 7a5:	c3                   	ret    

000007a6 <malloc>:

void*
malloc(uint nbytes)
{
 7a6:	55                   	push   %ebp
 7a7:	89 e5                	mov    %esp,%ebp
 7a9:	83 ec 18             	sub    $0x18,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 7ac:	8b 45 08             	mov    0x8(%ebp),%eax
 7af:	83 c0 07             	add    $0x7,%eax
 7b2:	c1 e8 03             	shr    $0x3,%eax
 7b5:	83 c0 01             	add    $0x1,%eax
 7b8:	89 45 ec             	mov    %eax,-0x14(%ebp)
  if((prevp = freep) == 0){
 7bb:	a1 2c 0b 00 00       	mov    0xb2c,%eax
 7c0:	89 45 f0             	mov    %eax,-0x10(%ebp)
 7c3:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
 7c7:	75 23                	jne    7ec <malloc+0x46>
    base.s.ptr = freep = prevp = &base;
 7c9:	c7 45 f0 24 0b 00 00 	movl   $0xb24,-0x10(%ebp)
 7d0:	8b 45 f0             	mov    -0x10(%ebp),%eax
 7d3:	a3 2c 0b 00 00       	mov    %eax,0xb2c
 7d8:	a1 2c 0b 00 00       	mov    0xb2c,%eax
 7dd:	a3 24 0b 00 00       	mov    %eax,0xb24
    base.s.size = 0;
 7e2:	c7 05 28 0b 00 00 00 	movl   $0x0,0xb28
 7e9:	00 00 00 
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 7ec:	8b 45 f0             	mov    -0x10(%ebp),%eax
 7ef:	8b 00                	mov    (%eax),%eax
 7f1:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if(p->s.size >= nunits){
 7f4:	8b 45 f4             	mov    -0xc(%ebp),%eax
 7f7:	8b 40 04             	mov    0x4(%eax),%eax
 7fa:	3b 45 ec             	cmp    -0x14(%ebp),%eax
 7fd:	72 4d                	jb     84c <malloc+0xa6>
      if(p->s.size == nunits)
 7ff:	8b 45 f4             	mov    -0xc(%ebp),%eax
 802:	8b 40 04             	mov    0x4(%eax),%eax
 805:	3b 45 ec             	cmp    -0x14(%ebp),%eax
 808:	75 0c                	jne    816 <malloc+0x70>
        prevp->s.ptr = p->s.ptr;
 80a:	8b 45 f4             	mov    -0xc(%ebp),%eax
 80d:	8b 10                	mov    (%eax),%edx
 80f:	8b 45 f0             	mov    -0x10(%ebp),%eax
 812:	89 10                	mov    %edx,(%eax)
 814:	eb 26                	jmp    83c <malloc+0x96>
      else {
        p->s.size -= nunits;
 816:	8b 45 f4             	mov    -0xc(%ebp),%eax
 819:	8b 40 04             	mov    0x4(%eax),%eax
 81c:	2b 45 ec             	sub    -0x14(%ebp),%eax
 81f:	89 c2                	mov    %eax,%edx
 821:	8b 45 f4             	mov    -0xc(%ebp),%eax
 824:	89 50 04             	mov    %edx,0x4(%eax)
        p += p->s.size;
 827:	8b 45 f4             	mov    -0xc(%ebp),%eax
 82a:	8b 40 04             	mov    0x4(%eax),%eax
 82d:	c1 e0 03             	shl    $0x3,%eax
 830:	01 45 f4             	add    %eax,-0xc(%ebp)
        p->s.size = nunits;
 833:	8b 45 f4             	mov    -0xc(%ebp),%eax
 836:	8b 55 ec             	mov    -0x14(%ebp),%edx
 839:	89 50 04             	mov    %edx,0x4(%eax)
      }
      freep = prevp;
 83c:	8b 45 f0             	mov    -0x10(%ebp),%eax
 83f:	a3 2c 0b 00 00       	mov    %eax,0xb2c
      return (void*)(p + 1);
 844:	8b 45 f4             	mov    -0xc(%ebp),%eax
 847:	83 c0 08             	add    $0x8,%eax
 84a:	eb 3b                	jmp    887 <malloc+0xe1>
    }
    if(p == freep)
 84c:	a1 2c 0b 00 00       	mov    0xb2c,%eax
 851:	39 45 f4             	cmp    %eax,-0xc(%ebp)
 854:	75 1e                	jne    874 <malloc+0xce>
      if((p = morecore(nunits)) == 0)
 856:	83 ec 0c             	sub    $0xc,%esp
 859:	ff 75 ec             	pushl  -0x14(%ebp)
 85c:	e8 e5 fe ff ff       	call   746 <morecore>
 861:	83 c4 10             	add    $0x10,%esp
 864:	89 45 f4             	mov    %eax,-0xc(%ebp)
 867:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
 86b:	75 07                	jne    874 <malloc+0xce>
        return 0;
 86d:	b8 00 00 00 00       	mov    $0x0,%eax
 872:	eb 13                	jmp    887 <malloc+0xe1>
  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
  if((prevp = freep) == 0){
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 874:	8b 45 f4             	mov    -0xc(%ebp),%eax
 877:	89 45 f0             	mov    %eax,-0x10(%ebp)
 87a:	8b 45 f4             	mov    -0xc(%ebp),%eax
 87d:	8b 00                	mov    (%eax),%eax
 87f:	89 45 f4             	mov    %eax,-0xc(%ebp)
      return (void*)(p + 1);
    }
    if(p == freep)
      if((p = morecore(nunits)) == 0)
        return 0;
  }
 882:	e9 6d ff ff ff       	jmp    7f4 <malloc+0x4e>
}
 887:	c9                   	leave  
 888:	c3                   	ret    
