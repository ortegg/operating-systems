#include "types.h"
#include "user.h"

int
main(int argc, char * argv[])
{  
  uint start;
  int pid, time;
  
  pid = fork();
  start = uptime();

  if(pid < 0){
    printf(2, "time_fail: Invalid PID.\n");
    exit();
  }
  if(pid > 0)	//parent process wait for child to finish and then tell us time to run
    wait();
  if(pid == 0){ //child process
    if(exec(argv[1], argv + 1)){   
      exit();
    }
  }
  time = uptime() - start;
  if(argc == 1)
    printf(1, "%s ran in %d.%d seconds.\n", argv[0], time / 100, time % 100);
  else
    printf(1, "%s ran in %d.%d seconds.\n", argv[1], time / 100, time % 100);
  exit();
}
