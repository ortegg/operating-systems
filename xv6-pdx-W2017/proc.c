#include "types.h"
#include "defs.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "x86.h"
#include "proc.h"
#include "spinlock.h"
#include "uproc.h"

struct StateLists {
  struct proc* ready;
  struct proc* free;
  struct proc* sleep;
  struct proc* zombie;
  struct proc* running;
  struct proc* embryo;
  struct proc* rtail;
  struct proc* ftail;
  struct proc* stail;
  struct proc* ztail;
  struct proc* runtail;
  struct proc* etail;
};

struct {
  struct spinlock lock;
  struct proc proc[NPROC];
  struct StateLists pLists;
} ptable;

static struct proc *initproc;

int nextpid = 1;
extern void forkret(void);
extern void trapret(void);

static void wakeup1(void *chan);
//*******************************************************************************************************************************
static int
addToStateList(struct proc ** head, struct proc * p){

  if(!*head){	// If the list is empty (tail == head) 
    *head = p;
    (*head)->next = 0;
    return 0;
  }
  else{
    p->next = *head;
    *head = p;
    return 0;
  }
  /*else{
    struct proc * temp = *head;
    while(temp->next != 0)
    {
      temp = temp->next;
    }
    temp->next = p;
    temp->next->next = 0;
    return 0;
  }*/ 
  return -1; 
}

static int
removeFromStateList(struct proc ** head, struct proc * p){
  if(*head == 0)
    panic("Error: List is empty.");		// list empty can't remove anything
  if(*head == p){	// if p is at beggining of list
    if((*head)->next == 0){
      *head = 0;
      return 0;
    }
    else{
      *head = (*head)->next;
      return 0;
    }
  }
  else{
    struct proc * tmp = *head;
    while(tmp->next != 0 && tmp->next != p)
      tmp = tmp->next;
    // Check if node really exists in Linked List
    if(tmp->next == 0)
      panic("Error: p is not in list.");
    tmp->next = tmp->next->next;
    return 0;
  } 
  return -1;
}

static void
assertState(struct proc * p,  enum procstate state){
  if(p->state == state)
    return;  
  panic("Error: Process state incorrect in assertState()");
}

static void
fromToStateList(struct proc *p, enum procstate fromState, enum procstate toState, struct proc **from, struct proc **to)
{
  if(!holding(&ptable.lock))
    panic("In fromToStateList(), not holding lock\n");
  removeFromStateList(from, p);
  assertState(p, fromState);
  p->state = toState;
  addToStateList(to, p);
}
//******************************************************** CONSOLE COMMANDS HELPERS *******************************************************
//control-f
int
count(struct proc * head){
  if(head == 0)
    return 0;
  int count = 0;
  struct proc * current = head;
  while(current != 0){				//iterative count
    ++count;
    current = current->next;
  }
  return count;
  //return 1 + count(head->next);		//recursive count
}

void
readysleep(struct proc * head){
  if(head == 0){
    return;
  }
  cprintf("%d", head->pid);
  if(head->next != 0)
    cprintf(" -> ");
  else if(head->next == 0)
    cprintf("\n");
  readysleep(head->next);
}
//control-z
void
zombie(struct proc * head){
  if(head == 0)
    return;
  int ppid;
  if(head->pid == 1)
    ppid = 1;
  else
    ppid = head->parent->pid;  
  cprintf("(%d, PPID%d)", head->pid, ppid);
  if(head->next != 0)
    cprintf(" -> ");
  else if(head->next == 0)
    cprintf("\n");
  readysleep(head->next);
}
//********************************************************************************************************************************************
void
pinit(void)
{
  initlock(&ptable.lock, "ptable");
}

//PAGEBREAK: 32
// Look in the process table for an UNUSED proc.
// If found, change state to EMBRYO and initialize
// state required to run in the kernel.
// Otherwise return 0.
static struct proc*
allocproc(void)						//************************** ALLOCPROC ******************************************
{
  struct proc *p;
  char *sp;

  acquire(&ptable.lock);
#ifndef CS333_P3P4
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->state == UNUSED)
        goto found;
  release(&ptable.lock);
  return 0;
#endif

#ifdef CS333_P3P4
  p = ptable.pLists.free;
  if(p->state == UNUSED)
    goto found;
  release(&ptable.lock);
  return 0;
#endif

found:
#ifdef CS333_P3P4
  fromToStateList(p, UNUSED, EMBRYO, &ptable.pLists.free, &ptable.pLists.embryo);
#endif
#ifndef CS333_P3P4
  p->state = EMBRYO;
#endif
  p->pid = nextpid++;
  release(&ptable.lock);

  // Allocate kernel stack.
  if((p->kstack = kalloc()) == 0){
#ifdef CS333_P3P4
    fromToStateList(p, EMBRYO, UNUSED, &ptable.pLists.embryo, &ptable.pLists.free);
#endif
#ifndef CS333_P3P4
    p->state = UNUSED;
#endif
    return 0;
  }
  sp = p->kstack + KSTACKSIZE;
  
  // Leave room for trap frame.
  sp -= sizeof *p->tf;
  p->tf = (struct trapframe*)sp;
  
  // Set up new context to start executing at forkret,
  // which returns to trapret.
  sp -= 4;
  *(uint*)sp = (uint)trapret;

  sp -= sizeof *p->context;
  p->context = (struct context*)sp;
  memset(p->context, 0, sizeof *p->context);
  p->context->eip = (uint)forkret;

  p->start_ticks = ticks;	//initialization of start_ticks to global kernel variable
  p->cpu_ticks_total = 0;
  p->cpu_ticks_in = 0;

  return p;
}

//PAGEBREAK: 32
// Set up first user process.
void
userinit(void)					//********************** USERINIT *****************************
{
  struct proc *p;
  extern char _binary_initcode_start[], _binary_initcode_size[];

#ifdef CS333_P3P4
  struct proc *tmp;
  acquire(&ptable.lock);
  ptable.pLists.ready = 0;	//initialize ready list
  ptable.pLists.free = 0;	//initialize free list
  ptable.pLists.sleep = 0;	//initialize sleep list
  ptable.pLists.zombie = 0;	//initialize zombie list
  ptable.pLists.running = 0;	//initialize running list
  ptable.pLists.embryo = 0;	//initialize embryo list

  for(tmp = ptable.proc; tmp < &ptable.proc[NPROC]; tmp++)
    addToStateList(&ptable.pLists.free, tmp);
  release(&ptable.lock);
#endif
  p = allocproc();
  initproc = p;
  if((p->pgdir = setupkvm()) == 0)
    panic("userinit: out of memory?");
  inituvm(p->pgdir, _binary_initcode_start, (int)_binary_initcode_size);
  p->sz = PGSIZE;
  memset(p->tf, 0, sizeof(*p->tf));
  p->tf->cs = (SEG_UCODE << 3) | DPL_USER;
  p->tf->ds = (SEG_UDATA << 3) | DPL_USER;
  p->tf->es = p->tf->ds;
  p->tf->ss = p->tf->ds;
  p->tf->eflags = FL_IF;
  p->tf->esp = PGSIZE;
  p->tf->eip = 0;  // beginning of initcode.S

  safestrcpy(p->name, "initcode", sizeof(p->name));
  p->cwd = namei("/");
#ifdef CS333_P3P4
  acquire(&ptable.lock);
  fromToStateList(p, EMBRYO, RUNNABLE, &ptable.pLists.embryo, &ptable.pLists.ready);
  release(&ptable.lock);
#endif
  p->state = RUNNABLE;
  p->gid = NGID;
  p->uid = NUID;

#ifdef CS333_P3P4
//  p->next = 0;			//p next pointer is null
  ptable.pLists.ready = p;	//p is now in ready list
  //ptable.pLists.rtail = ptable.pLists.ready;		//initialize free tail
  //ptable.pLists.stail = ptable.pLists.sleep;		//initialize sleep tail
  //ptable.pLists.ztail = ptable.pLists.zombie;		//initialize zombie tail
  //ptable.pLists.runtail = ptable.pLists.running;	//initialize running tail
  //ptable.pLists.etail = ptable.pLists.embryo;		//initialize embry tail
#endif
}

// Grow current process's memory by n bytes.
// Return 0 on success, -1 on failure.
int
growproc(int n)
{
  uint sz;
  
  sz = proc->sz;
  if(n > 0){
    if((sz = allocuvm(proc->pgdir, sz, sz + n)) == 0)
      return -1;
  } else if(n < 0){
    if((sz = deallocuvm(proc->pgdir, sz, sz + n)) == 0)
      return -1;
  }
  proc->sz = sz;
  switchuvm(proc);
  return 0;
}

// Create a new process copying p as the parent.
// Sets up stack to return as if from system call.
// Caller must set state of returned proc to RUNNABLE.
int
fork(void)						//************** FORK *************************************
{
  int i, pid;
  struct proc *np;

  // Allocate process.
  if((np = allocproc()) == 0)
    return -1;

  // Copy process state from p.
  if((np->pgdir = copyuvm(proc->pgdir, proc->sz)) == 0){
    kfree(np->kstack);
    np->kstack = 0;
#ifdef CS333_P3P4
    //acquire(&ptable.lock);
    //fromToStateList(np, EMBRYO, UNUSED, &ptable.pLists.embryo, &ptable.pLists.free);
    //release(&ptable.lock);
#endif
    np->state = UNUSED;
    return -1;
  }
  np->sz = proc->sz;
  np->parent = proc;
  *np->tf = *proc->tf;
  np->uid = proc->uid;		//copy uid & gid to child
  np->gid = proc->gid;

  // Clear %eax so that fork returns 0 in the child.
  np->tf->eax = 0;

  for(i = 0; i < NOFILE; i++)
    if(proc->ofile[i])
      np->ofile[i] = filedup(proc->ofile[i]);
  np->cwd = idup(proc->cwd);

  safestrcpy(np->name, proc->name, sizeof(proc->name));
 
  pid = np->pid;

  // lock to force the compiler to emit the np->state write last.
  acquire(&ptable.lock);
#ifdef CS333_P3P4
  //fromToStateList(np, EMBRYO, RUNNABLE, &ptable.pLists.embryo, &ptable.pLists.ready);
#endif
  np->state = RUNNABLE;
  release(&ptable.lock);
  
  return pid;
}

// Exit the current process.  Does not return.
// An exited process remains in the zombie state
// until its parent calls wait() to find out it exited.
#ifndef CS333_P3P4
void
exit(void)
{
  struct proc *p;
  int fd;

  if(proc == initproc)
    panic("init exiting");

  // Close all open files.
  for(fd = 0; fd < NOFILE; fd++){
    if(proc->ofile[fd]){
      fileclose(proc->ofile[fd]);
      proc->ofile[fd] = 0;
    }
  }

  begin_op();
  iput(proc->cwd);
  end_op();
  proc->cwd = 0;

  acquire(&ptable.lock);

  // Parent might be sleeping in wait().
  wakeup1(proc->parent);

  // Pass abandoned children to init.
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->parent == proc){
      p->parent = initproc;
      if(p->state == ZOMBIE)
        wakeup1(initproc);
    }
  }

  // Jump into the scheduler, never to return.
  proc->state = ZOMBIE;
  sched();
  panic("zombie exit");
}
#else			//*************************************** EXIT *****************************
void
exit(void)
{
  struct proc *p;
  int fd;

  if(proc == initproc)
    panic("init exiting");

  // Close all open files.
  for(fd = 0; fd < NOFILE; fd++){
    if(proc->ofile[fd]){
      fileclose(proc->ofile[fd]);
      proc->ofile[fd] = 0;
    }
  }

  begin_op();
  iput(proc->cwd);
  end_op();
  proc->cwd = 0;

  acquire(&ptable.lock);

  // Parent might be sleeping in wait().
  wakeup1(proc->parent);

  // Pass abandoned children to init.
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->parent == proc){
      p->parent = initproc;
      if(p->state == ZOMBIE)
        wakeup1(initproc);
    }
  }

  // Jump into the scheduler, never to return.
  proc->state = ZOMBIE;
  sched();
  panic("zombie exit");
/*
  struct proc *p, *r;
  int fd;

  if(proc == initproc)
    panic("init exiting");

  // Close all open files.
  for(fd = 0; fd < NOFILE; fd++){
    if(proc->ofile[fd]){
      fileclose(proc->ofile[fd]);
      proc->ofile[fd] = 0;
    }
  }

  begin_op();
  iput(proc->cwd);
  end_op();
  proc->cwd = 0;

  acquire(&ptable.lock);

  // Parent might be sleeping in wait().
  wakeup1(proc->parent);

  // Pass abandoned children to init.
  r = ptable.pLists.running;				//search running list
  p = r;
  while(p->next != 0 && p->pid != pid){
    p = p->next;
  }
  if(p->parent == proc)
    p->parent = initproc;
  r = ptable.pLists.zombie;				//search zombie list
  p = r;
  while(p->next != 0 && p->pid != pid){
    p = p->next;
  }
  if(p->parent == proc){
    p->parent = initproc;	
    if(p->state == ZOMBIE)
      wakeup1(initproc);
  }
  r = ptable.pLists.ready;				//search ready list
  p = r;
  while(p->next != 0 && p->pid != pid){
    p = p->next;
  }
  if(p->parent == proc)
    p->parent = initproc;
  r = ptable.pLists.sleep;				//search running list
  p = r;
  while(p->next != 0 && p->pid != pid){
    p = p->next;
  }
  if(p->parent == proc)
    p->parent = initproc;

  // Jump into the scheduler, never to return.
  fromToStateList(proc, RUNNING, ZOMBIE, &ptable.pLists.running, &ptable.pLists.zombie);
  //proc->state = ZOMBIE;
  sched();
  panic("zombie exit");*/
}
#endif

// Wait for a child process to exit and return its pid.
// Return -1 if this process has no children.
#ifndef CS333_P3P4
int
wait(void)
{
  struct proc *p;
  int havekids, pid;

  acquire(&ptable.lock);
  for(;;){
    // Scan through table looking for zombie children.
    havekids = 0;
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
      if(p->parent != proc)
        continue;
      havekids = 1;
      if(p->state == ZOMBIE){
        // Found one.
        pid = p->pid;
        kfree(p->kstack);
        p->kstack = 0;
        freevm(p->pgdir);
        p->state = UNUSED;
        p->pid = 0;
        p->parent = 0;
        p->name[0] = 0;
        p->killed = 0;
        release(&ptable.lock);
        return pid;
      }
    }

    // No point waiting if we don't have any children.
    if(!havekids || proc->killed){
      release(&ptable.lock);
      return -1;
    }

    // Wait for children to exit.  (See wakeup1 call in proc_exit.)
    sleep(proc, &ptable.lock);  //DOC: wait-sleep
  }
}
#else				//************************************* WAIT *****************************
int
wait(void)
{
  struct proc *p;
  int havekids, pid;

  acquire(&ptable.lock);
  for(;;){
    // Scan through table looking for zombie children.
    havekids = 0;
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
      if(p->parent != proc)
        continue;
      havekids = 1;
      if(p->state == ZOMBIE){
        // Found one.
        pid = p->pid;
        kfree(p->kstack);
        p->kstack = 0;
        freevm(p->pgdir);
        p->state = UNUSED;
        p->pid = 0;
        p->parent = 0;
        p->name[0] = 0;
        p->killed = 0;
        release(&ptable.lock);
        return pid;
      }
    }

    // No point waiting if we don't have any children.
    if(!havekids || proc->killed){
      release(&ptable.lock);
      return -1;
    }

    // Wait for children to exit.  (See wakeup1 call in proc_exit.)
    sleep(proc, &ptable.lock);  //DOC: wait-sleep
  }

/*  struct proc *p, *tmp;
  int havekids, pid;

  acquire(&ptable.lock);
  for(;;){
    // Scan through table looking for zombie children.
    havekids = 0;
    //for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    tmp = ptable.pLists.zombie;
    p = tmp;
    if(p->parent != proc){
      p = p->next;		//traverse list if equal
      continue;
    }
    havekids = 1;
    if(p->state == ZOMBIE){
        // Found one.
      pid = p->pid;
      kfree(p->kstack);
      p->kstack = 0;
      freevm(p->pgdir);
      fromToStateList(p, ZOMBIE, UNUSED, &ptable.pLists.zombie, &ptable.pLists.free);
      //p->state = UNUSED;
      p->pid = 0;
      p->parent = 0;
      p->name[0] = 0;
      p->killed = 0;
      release(&ptable.lock);
      return pid;
    }
  }

    // No point waiting if we don't have any children.
    if(!havekids || proc->killed){
      release(&ptable.lock);
      return -1;
    }

    // Wait for children to exit.  (See wakeup1 call in proc_exit.)
    sleep(proc, &ptable.lock);  //DOC: wait-sleep
  }*/
}
#endif

//PAGEBREAK: 42
// Per-CPU process scheduler.
// Each CPU calls scheduler() after setting itself up.
// Scheduler never returns.  It loops, doing:
//  - choose a process to run
//  - swtch to start running that process
//  - eventually that process transfers control
//      via swtch back to the scheduler.
#ifndef CS333_P3P4
// original xv6 scheduler. Use if CS333_P3P4 NOT defined.
void
scheduler(void)
{
  struct proc *p;
  int idle;  // for checking if processor is idle

  for(;;){
    // Enable interrupts on this processor.
    sti();

    idle = 1;  // assume idle unless we schedule a process
    // Loop over process table looking for process to run.
    acquire(&ptable.lock);
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
      if(p->state != RUNNABLE)
        continue;

      // Switch to chosen process.  It is the process's job
      // to release ptable.lock and then reacquire it
      // before jumping back to us.
      idle = 0;  // not idle this timeslice
      proc = p;
      switchuvm(p);
      p->state = RUNNING;
      p->cpu_ticks_in = ticks;			//ticks in
      swtch(&cpu->scheduler, proc->context);
      switchkvm();

      // Process is done running for now.
      // It should have changed its p->state before coming back.
      proc = 0;
    }
    release(&ptable.lock);
    // if idle, wait for next interrupt
    if (idle) {
      sti();
      hlt();
    }
  }
}

#else					//************************ SCHEDULER ***********************************
void
scheduler(void)
{
  struct proc *p;
  int idle;  // for checking if processor is idle

  for(;;){
    // Enable interrupts on this processor.
    sti();

    idle = 1;  // assume idle unless we schedule a process
    // Loop over process table looking for process to run.
    acquire(&ptable.lock);
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
      if(p->state != RUNNABLE)
        continue;

      // Switch to chosen process.  It is the process's job
      // to release ptable.lock and then reacquire it
      // before jumping back to us.
      idle = 0;  // not idle this timeslice
      proc = p;
      switchuvm(p);
      p->state = RUNNING;
      p->cpu_ticks_in = ticks;			//ticks in
      swtch(&cpu->scheduler, proc->context);
      switchkvm();

      // Process is done running for now.
      // It should have changed its p->state before coming back.
      proc = 0;
    }
    release(&ptable.lock);
    // if idle, wait for next interrupt
    if (idle) {
      sti();
      hlt();
    }
  }

  /*struct proc *p, *tmp;
  int idle;  // for checking if processor is idle

  for(;;){
    // Enable interrupts on this processor.
    sti();

    idle = 1;  // assume idle unless we schedule a process
    // Loop over process table looking for process to run.
    acquire(&ptable.lock);
    //for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    tmp = ptable.pLists.ready;
    p = tmp;
    if(p->state != RUNNABLE){
      p = p->next;
    }

      // Switch to chosen process.  It is the process's job
      // to release ptable.lock and then reacquire it
      // before jumping back to us.
    idle = 0;  // not idle this timeslice
    proc = p;
    switchuvm(p);
    fromToStateList(p, RUNNABLE, RUNNING, &ptable.pLists.ready, &ptable.pLists.running);
    //p->state = RUNNING;
    p->cpu_ticks_in = ticks;			//ticks in
    swtch(&cpu->scheduler, proc->context);
    switchkvm();

      // Process is done running for now.
      // It should have changed its p->state before coming back.
    proc = 0;
  }
    release(&ptable.lock);
    // if idle, wait for next interrupt
    if (idle) {
      sti();
      hlt();
    }*/
}
#endif

// Enter scheduler.  Must hold only ptable.lock
// and have changed proc->state.
#ifndef CS333_P3P4
void
sched(void)
{
  int intena;

  if(!holding(&ptable.lock))
    panic("sched ptable.lock");
  if(cpu->ncli != 1)
    panic("sched locks");
  if(proc->state == RUNNING)
    panic("sched running");
  if(readeflags()&FL_IF)
    panic("sched interruptible");
  intena = cpu->intena;
  proc->cpu_ticks_total += (ticks - proc->cpu_ticks_in);
  swtch(&proc->context, cpu->scheduler);
  cpu->intena = intena; 
}
#else						//*************** SCHED ****************************
void
sched(void)
{
  int intena;

  if(!holding(&ptable.lock))
    panic("sched ptable.lock");
  if(cpu->ncli != 1)
    panic("sched locks");
  if(proc->state == RUNNING)
    panic("sched running");
  if(readeflags()&FL_IF)
    panic("sched interruptible");
  intena = cpu->intena;
  proc->cpu_ticks_total += (ticks - proc->cpu_ticks_in);
  swtch(&proc->context, cpu->scheduler);
  cpu->intena = intena;  
}
#endif

// Give up the CPU for one scheduling round.
void
yield(void)
{
  acquire(&ptable.lock);  //DOC: yieldlock
#ifdef CS333_P3P4
  //fromToStateList(proc, RUNNING, RUNNABLE, &ptable.pLists.running, &ptable.pLists.ready);
#endif
  proc->state = RUNNABLE;
  sched();
  release(&ptable.lock);
}

// A fork child's very first scheduling by scheduler()
// will swtch here.  "Return" to user space.
void
forkret(void)
{
  static int first = 1;
  // Still holding ptable.lock from scheduler.
  release(&ptable.lock);

  if (first) {
    // Some initialization functions must be run in the context
    // of a regular process (e.g., they call sleep), and thus cannot 
    // be run from main().
    first = 0;
    iinit(ROOTDEV);
    initlog(ROOTDEV);
  }  
  // Return to "caller", actually trapret (see allocproc).
}

// Atomically release lock and sleep on chan.
// Reacquires lock when awakened.
// 2016/12/28: ticklock removed from xv6. sleep() changed to
// accept a NULL lock to accommodate.
void							//*************** SLEEP *************************************
sleep(void *chan, struct spinlock *lk)
{
  if(proc == 0)
    panic("sleep");

  // Must acquire ptable.lock in order to
  // change p->state and then call sched.
  // Once we hold ptable.lock, we can be
  // guaranteed that we won't miss any wakeup
  // (wakeup runs with ptable.lock locked),
  // so it's okay to release lk.
  if(lk != &ptable.lock){
    acquire(&ptable.lock);
    if (lk) release(lk);
  }

  // Go to sleep.
  proc->chan = chan;
#ifdef CS333_P3P4
  //fromToStateList(chan, RUNNING, SLEEPING, &ptable.pLists.running, &ptable.pLists.sleep);
#endif
  proc->state = SLEEPING;
  sched();

  // Tidy up.
  proc->chan = 0;

  // Reacquire original lock.
  if(lk != &ptable.lock){ 
    release(&ptable.lock);
    if (lk) acquire(lk);
  }
}

//PAGEBREAK!
#ifndef CS333_P3P4
// Wake up all processes sleeping on chan.
// The ptable lock must be held.
static void
wakeup1(void *chan)
{
  struct proc *p;

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
    if(p->state == SLEEPING && p->chan == chan)
      p->state = RUNNABLE;
}
#else   	//********************************************************** WAKEUP *************************
static void
wakeup1(void *chan)
{
   struct proc *p;

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
    if(p->state == SLEEPING && p->chan == chan)
      p->state = RUNNABLE;
 
/*
 struct proc *p, *tmp;

  //for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
  tmp = ptable.pLists.sleep;
  p = tmp;
  if(p->chan != chan)
      p = p->next;
  else if(p->state == SLEEPING && p->chan == chan){
      fromToStateList(p, SLEEPING, RUNNABLE, &ptable.pLists.sleep, &ptable.pLists.ready);
      //p->state = RUNNABLE;
  }
*/
}
#endif		
// Wake up all processes sleeping on chan.
void
wakeup(void *chan)
{
  acquire(&ptable.lock);
  wakeup1(chan);
  release(&ptable.lock);
}

// Kill the process with the given pid.
// Process won't exit until it returns
// to user space (see trap in trap.c).
#ifndef CS333_P3P4
int
kill(int pid)
{
  struct proc *p;

  acquire(&ptable.lock);
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->pid == pid){
      p->killed = 1;
      // Wake process from sleep if necessary.
      if(p->state == SLEEPING)
        p->state = RUNNABLE;
      release(&ptable.lock);
      return 0;
    }
  }
  release(&ptable.lock);
  return -1;
}
#else						//***************************** KILL ***************
int
kill(int pid)
{
  struct proc *p;

  acquire(&ptable.lock);
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->pid == pid){
      p->killed = 1;
      // Wake process from sleep if necessary.
      if(p->state == SLEEPING)
        p->state = RUNNABLE;
      release(&ptable.lock);
      return 0;
    }
  }
  release(&ptable.lock);
  return -1;

/*
  struct proc *p, *r;

  acquire(&ptable.lock);
  r = ptable.pLists.running;				//search running list
  p = r;
  while(p->next != 0 && p->pid != pid){
    p = p->next;
  }
  if(p->pid == pid){
    p->killed = 1;
    release(&ptable.lock);
    return 0;
  }  							//search ready list
  r = ptable.pLists.ready;
  p = r;
  while(p->next != 0 && p->pid != pid){
    p = p->next;
  }
  if(p->pid == pid){
    p->killed = 1;
    release(&ptable.lock);
    return 0;
  } 							//search sleep list
  r = ptable.pLists.sleep;
  p = r;
  while(p->next != 0 && p->pid != pid)
    p = p->next;
  if(p->pid == pid){
    p->killed = 1;
    if(p->state == SLEEPING)    			// Wake process from sleep if necessary.
    fromToStateList(p, SLEEPING, RUNNABLE, &ptable.pLists.sleep, &ptable.pLists.ready);
    release(&ptable.lock);
    return 0;
  } 							//search zombie list
  r = ptable.pLists.zombie;
  p = r;
  while(p->next != 0 && p->pid != pid){
    p = p->next;
  }
  if(p->pid == pid){
    p->killed = 1;
    release(&ptable.lock);
    return 0;
  }

  release(&ptable.lock);
  return -1;
*/
}
#endif

static char *states[] = {
  [UNUSED]    "unused",
  [EMBRYO]    "embryo",
  [SLEEPING]  "sleep ",
  [RUNNABLE]  "runble",
  [RUNNING]   "run   ",
  [ZOMBIE]    "zombie"
};

//PAGEBREAK: 36
// Print a process listing to console.  For debugging.
// Runs when user types ^P on console.
// No lock to avoid wedging a stuck machine further.
void
procdump(void)
{
  int i;
  struct proc *p;
  char *state;
  uint sec;
  uint part;
  uint pc[10];
  int ppid;
 
  cprintf("\nPID\tName\tUID\tGID\tPPID\tElapsed\tCPU\tState\tSize\tPCs\n");
 
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->state == UNUSED)
      continue;
    if(p->state >= 0 && p->state < NELEM(states) && states[p->state]){
      state = states[p->state];

    }
    else
      state = "???";
    sec = ((ticks)-(p->start_ticks)) / 100;
    part = ((ticks)-(p->start_ticks)) % 100;
    if(p->pid == 1) ppid = 1;
    else ppid = p->parent->pid;  
    cprintf("%d\t%s\t%d\t%d\t%d\t%d.%d\t%d.%d\t%s\t%d\t", p->pid, p->name, p->uid, p->gid, ppid, sec, part, p->cpu_ticks_total / 100, p->cpu_ticks_total % 100, state, p->sz);
    if(p->state == SLEEPING){
      getcallerpcs((uint*)p->context->ebp+2, pc);
      for(i=0; i<10 && pc[i] != 0; i++){

        cprintf("%p  ", pc[i]);
      }
    }
    cprintf("\n");
  }
}
void
printready(void)
{
  struct proc *p;
  p = ptable.pLists.ready;
  cprintf("Ready List Processes:\n");
  readysleep(p);
}
void
printfree(void)
{
  struct proc *p;
  p = ptable.pLists.free; 
  cprintf("Free List Size: %d\n", count(p));
}
void
printsleep(void)
{
  struct proc *p;
  p = ptable.pLists.sleep;
  cprintf("Sleep List Processes:\n");
  readysleep(p);
}
void
printzombie(void)
{
  struct proc *p;
  p = ptable.pLists.zombie;
  cprintf("Zombie List Processes:\n");
  zombie(p);
}
int
setuid(int num)
{
  acquire(&ptable.lock);
  proc->uid = num;
  release(&ptable.lock);
  return 0;
}

int
setgid(int num)
{
  acquire(&ptable.lock);
  proc->gid = num;
  release(&ptable.lock);
  return 0;
}

int
getprocs(int max, struct uproc * table)
{
  int i;				//counter for # of processes in array
  struct proc *p;			//ptable will be here

  acquire(&ptable.lock);		//lock for mutual exclusion
  for(i = 0, p = ptable.proc; p < &ptable.proc[NPROC] && i < max; ++p)
  {
    if(p->state != UNUSED){		//look for active process to be copied until max amount of processes
      table[i].pid = p->pid;
      table[i].uid = p->uid;
      table[i].gid = p->gid;
      table[i].ppid = p->parent != 0 ? p->parent->pid : 1;
      table[i].size = p->sz;
      safestrcpy(table[i].name, p->name, sizeof(p->name));
      table[i].elapsed_ticks = ((ticks)-(p->start_ticks)) / 100;
      table[i].elapsed_ticks1 = ((ticks)-(p->start_ticks)) % 100;
      table[i].CPU_total_ticks = p->cpu_ticks_total /100;
      table[i].CPU_total_ticks1 = p->cpu_ticks_total %100;

      switch(p->state){			//switch statement for process state
        case UNUSED:
          break;
 	case EMBRYO:
	  safestrcpy(table[i].state, "embryo", sizeof("embryo")); break;
 	case SLEEPING:
	  safestrcpy(table[i].state, "sleep", sizeof("sleep")); break;
 	case RUNNABLE:
	  safestrcpy(table[i].state, "ready", sizeof("ready")); break;
 	case RUNNING:
	  safestrcpy(table[i].state, "run", sizeof("run")); break;
 	case ZOMBIE:
	  safestrcpy(table[i].state, "zombie", sizeof("zombie")); break;
      }
      ++i;
    }    
  }
  release(&ptable.lock);		//release lock
  return i;				//return the number of proccesses in the uproc struct
} 
