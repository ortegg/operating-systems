#include "types.h"
#define STRMAX 32

struct uproc {
  uint pid;
  uint uid;
  uint gid;
  uint ppid;
  uint elapsed_ticks;
  uint elapsed_ticks1;
  uint CPU_total_ticks;
  uint CPU_total_ticks1;
  char state[STRMAX];
  uint size;
  char name[STRMAX];
};

int getprocs(int max, struct uproc * table);
